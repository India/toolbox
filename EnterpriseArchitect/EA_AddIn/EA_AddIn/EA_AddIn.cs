﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EA_AddIn
{
    public class Main
    {
        public String EA_Connect(EA.Repository Repository)
        {
            // No special processing req'd
            return "";
        }
        bool IsProjectOpen(EA.Repository Repository)
        {
            try
            {
                EA.Collection c = Repository.Models;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public object EA_GetMenuItems(EA.Repository Repository, string Location, string MenuName)
        {
            /* nb example of out parameter:
			object item;
			EA.ObjectType t = Repository.GetTreeSelectedItem(out item);
			EA.Package r = (EA.Package) item;
			*/

            switch (MenuName)
            {
                case "":
                    return "Edit Tagged Values";
            }
            return "";
        }
        public void EA_MenuClick(EA.Repository Repository, string Location, string MenuName, string ItemName)
        {
            EA_AddInForm f;
            f = new EA_AddInForm();
            f.m_Repository = Repository;
            f.ShowDialog();
        }
        public void EA_Disconnect()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
