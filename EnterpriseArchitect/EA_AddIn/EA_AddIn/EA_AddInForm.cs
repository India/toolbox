﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EA;

namespace EA_AddIn
{
    public partial class EA_AddInForm : Form
    {
        public EA.Repository m_Repository;
        public Dictionary<string, List<elemIdName>> elemTypeNamePair = new Dictionary<string, List<elemIdName>>();
        public DataTable elementsTable = new DataTable("Elements Table");
        public EA_AddInForm()
        {
            InitializeComponent();
        }
        private void EA_AddInForm_Load(object sender, System.EventArgs e)
        {
            EA.ObjectType selectedObject = m_Repository.GetTreeSelectedItemType();
            switch (selectedObject)
            {
                case EA.ObjectType.otPackage:                    
                    EA.Package currentPackage = (EA.Package)m_Repository.GetTreeSelectedObject();
                    elementsTable.Columns.Add("Element Type");
                    elementsTable.Columns.Add("Element Name");
                    elementsTable.Columns.Add("Element ID");
                    walkThroughPackages(currentPackage);

                    EA.Collection props = m_Repository.PropertyTypes;
                    collectProjectProps(props);

                    DataSet elementsSet = new DataSet("Elements DataSet");
                    elementsSet.Tables.Add(elementsTable);
                    dataGridView2.AutoGenerateColumns = true;
                    dataGridView2.DataSource = elementsSet;
                    dataGridView2.DataMember = "Elements Table";
                    dataGridView2.AutoResizeColumns();
                    List<string> elemTypes = new List<string>();
                    for(int rows = 0; rows < dataGridView2.Rows.Count; rows++)
                    {
                        if(!elemTypes.Contains(dataGridView2.Rows[rows].Cells[0].Value.ToString()))
                            elemTypes.Add(dataGridView2.Rows[rows].Cells[0].Value.ToString());
                    }
                    for (int rows = 0; rows < dataGridView1.Rows.Count; rows++)
                    {
                        //MessageBox.Show("From grid: " + dataGridView1.Rows[rows].Cells[0].Value.ToString() + " " + dataGridView1.Rows[rows].Cells[1].Value.ToString());
                        string tagValue = dataGridView1.Rows[rows].Cells[1].Value.ToString();
                        if (tagValue.StartsWith("Type=Enum", StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (tagValue.Contains("Values="))
                            {
                                DataGridViewComboBoxCell cbCell = new DataGridViewComboBoxCell();
                                int inx = tagValue.IndexOf("Values=");
                                string TagValues = tagValue.Substring(inx + 7);
                                TagValues = TagValues.TrimEnd(';');
                                cbCell.Items.AddRange(TagValues.Split(','));
                                dataGridView1[1, rows] = cbCell;
                                dataGridView1[1, rows].Value = TagValues.Split(',')[0];
                            }
                            else if (tagValue.Contains("Default="))
                            {
                                DataGridViewTextBoxCell TextBoxCell = new DataGridViewTextBoxCell();
                                int inx = tagValue.IndexOf("Default=");
                                string TagValues = tagValue.Substring(inx + 8);
                                TagValues = TagValues.TrimEnd(';');
                                dataGridView1[1, rows] = TextBoxCell;
                                dataGridView1[1, rows].Value = TagValues;
                            }
                        }
                    }
                    dataGridView1.AutoResizeColumns();
                    break;
            }
        }
        public void walkThroughPackages(Package currentPackage)
        {
            EA.Collection allElements = currentPackage.Elements;
            if (elemTypeNamePair.Keys.Count == 0)
            {                
                elemTypeNamePair.Add("Feature", null);
                elemTypeNamePair.Add("UseCase", null);
                elemTypeNamePair.Add("Requirement", null);
            }
            List<elemIdName> tempListFeature = new List<elemIdName>();
            List<elemIdName> tempListUseCase = new List<elemIdName>();
            List<elemIdName> tempListRequirement = new List<elemIdName>();
            
            if (elemTypeNamePair["Feature"] != null)
                tempListFeature = elemTypeNamePair["Feature"];
            if (elemTypeNamePair["UseCase"] != null)
                tempListUseCase = elemTypeNamePair["UseCase"];
            if (elemTypeNamePair["Requirement"] != null)
                tempListRequirement = elemTypeNamePair["Requirement"];

            for (var i = 0; i < allElements.Count; i++)
            {
                Element curElement = (Element)allElements.GetAt((short)i);
                elementsTable.Rows.Add(curElement.Type, curElement.Name, curElement.ElementID);
                if (curElement.Type.Equals("Feature"))
                {
                    elemIdName tempAdd1 = new elemIdName();
                    tempAdd1.elemId = curElement.ElementID;
                    tempAdd1.elemName = curElement.Name;
                    tempListFeature.Add(tempAdd1);
                }
                else if (curElement.Type.Equals("UseCase"))
                {
                    elemIdName tempAdd2 = new elemIdName();
                    tempAdd2.elemId = curElement.ElementID;
                    tempAdd2.elemName = curElement.Name;
                    tempListUseCase.Add(tempAdd2);
                }
                else if (curElement.Type.Equals("Requirement"))
                {
                    elemIdName tempAdd3 = new elemIdName();
                    tempAdd3.elemId = curElement.ElementID;
                    tempAdd3.elemName = curElement.Name;
                    tempListRequirement.Add(tempAdd3);
                }
            }
            elemTypeNamePair["Feature"] = tempListFeature;
            elemTypeNamePair["UseCase"] = tempListUseCase;
            elemTypeNamePair["Requirement"] = tempListRequirement;
            EA.Collection childPackages = currentPackage.Packages;
            childPackages.GetEnumerator();
            if (childPackages.Count != 0)
            {
                foreach (Package p in childPackages)
                {
                    walkThroughPackages(p);
                }
            }
        }
        public void collectProjectProps(EA.Collection props)
        {
            DataSet sNameValue = new DataSet("TagNameAndValueSet");
            DataTable tNameValue = new DataTable("TagNameAndValueTable");
            tNameValue.Columns.Add("Tag Name");
            tNameValue.Columns.Add("Tag Value");
            for (var j = 0; j < props.Count; j++)
            {
                PropertyType ptype = (PropertyType)props.GetAt((short)j);
                //MessageBox.Show(ptype.Tag + " " + ptype.Detail);
                tNameValue.Rows.Add(ptype.Tag, ptype.Detail);
            }
            sNameValue.Tables.Add(tNameValue);
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.DataSource = sNameValue;
            dataGridView1.DataMember = "TagNameAndValueTable";
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            foreach(DataGridViewRow dvr in dataGridView2.SelectedRows)
            {
                //MessageBox.Show(dvr.Cells[2].Value.ToString());
                EA.Element selElement = m_Repository.GetElementByID(Int32.Parse(dvr.Cells[2].Value.ToString()));
                for (int rows = 0; rows < dataGridView1.Rows.Count; rows++)
                {
                    //MessageBox.Show(dataGridView1[0, rows].Value.ToString() + " " + dataGridView1[1, rows].Value.ToString());
                    EA.TaggedValue t = (TaggedValue)selElement.TaggedValues.GetByName(dataGridView1[0, rows].Value.ToString());
                    if (t == null)
                        t = (TaggedValue)selElement.TaggedValues.AddNew(dataGridView1[0, rows].Value.ToString(), dataGridView1[1, rows].Value.ToString());
                    else
                        t.Value = dataGridView1[1, rows].Value.ToString();
                    t.Update();
                }
            }
            MessageBox.Show("Tags for the selected Elements has been updated");
            //Application.Exit();
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //Application.Exit();
            Dispose();
        }
    }
}
