﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advntdg
{
    /// <summary>
    /// Structure to hold progress Update information
    /// Using the ctor with text and a max value resets the progress bar to "0" and shows the given text
    /// Using the ctor with text, subtext and optional performTask updates the Progressbar accordingly
    /// </summary>
    internal class ProgressInfo
    {
        /// <summary>
        /// Returns the "main" text for the progressbar
        /// </summary>
        public string Text { get; }
        /// <summary>
        /// Returns the subtext (below the progressbar)
        /// </summary>
        public string SubText { get; }
        /// <summary>
        /// Returns the maximum value. -1 means: not set (hint for "Update")
        /// </summary>
        public int Max { get; }
        /// <summary>
        /// Returns true, when the Progressbar should be reset
        /// </summary>
        public bool Reset { get; }
        /// <summary>
        /// Returns true, if the Progressbar should advance one step
        /// </summary>
        public bool PerformStep { get; }

        /// <summary>
        /// Ctor to create a Progressinfo which RESET the Progressbar
        /// </summary>
        /// <param name="text">Text to be shown in Progressbar</param>
        /// <param name="max">Maximum value for progressbar</param>
        public ProgressInfo(string text, int max)
        {
            this.Text = text;
            this.Max = max;
            SubText = "";
            this.Reset = true;
            this.PerformStep = false;
        }

        /// <summary>
        /// Ctor to create a ProgressInformation to UPDATE the ProgressBAr
        /// </summary>
        /// <param name="text">Main Text - can be null (leaves Main Text)</param>
        /// <param name="subText">Subtext (shown below ProgressBar) - can be null</param>
        /// <param name="performStep">Optional (default: true): flags whether the progressbar should advance one step</param>
        public ProgressInfo(string text, string subText, bool performStep = true)
        {
            this.Text = text;
            SubText = subText;
            this.Max = -1;
            this.Reset = false;
            this.PerformStep = performStep;
        }
    }
}
