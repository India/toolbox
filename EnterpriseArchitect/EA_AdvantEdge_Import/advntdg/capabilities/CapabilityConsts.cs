﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using advntdg;
using Microsoft.Office.Interop.Excel;

namespace Advntdg.capabilities
{
    internal class CapabilityConsts
    {
        public const string SheetName = @"Cap. & Req. Library";

        private static readonly CapabilityConsts[] ConstVersions =
        {
            new CapabilityConsts() // BEGIN / HTE
            {
                Id = "ID",
                Title = "Capability Title",
                Description = "Description (Req. Statement)",
                Subtype = "Subtype",
                Kind = "OOTB/Config/Custom",
                NoOfBuc = "No of BUC used in",
                Sofware = "Software",
                Category = "Category (e.g. module, application...)",
                FuncTitle = "Function Title",
                FuncDesc = "Function Description",
                Availability = "Availability",
                AltIfNotAvail = "Alternative if not available",
                PrioIfNotAvail = "Priority if not available",
                Difficulty = "Difficulty of execution currently",
                DocUrl = "Doc. URL or Other database xref",
                ReuseId = "Reuse ID",
                ReuseMeId = "Capability Subtype Reuse Me ID"
            },
            new CapabilityConsts() // Rehau
            {
                Id = "ID",
                Title = "Capability Title",
                Description = "Description (Req. Statement)",
                Subtype = null,
                Kind = null,
                NoOfBuc = "Used in Buc",
                Sofware = null,
                Category = null,
                FuncTitle = "Related Functions Ids",
                FuncDesc = null,
                Availability = null,
                AltIfNotAvail = null,
                PrioIfNotAvail = null,
                Difficulty = null,
                DocUrl = null,
                ReuseId = "ID",
                ReuseMeId = "ID"
            }
        };
        
        public static IReadOnlyList<string> ImportAsTaggedValue => new List<string>
        {
            Active.Subtype,
            Active.Kind,
            Active.NoOfBuc,
            Active.Sofware,
            Active.Category,
            Active.Availability,
            Active.AltIfNotAvail,
            Active.PrioIfNotAvail,
            Active.Difficulty,
            Active.DocUrl,
            Active.ReuseId,
            Active.ReuseMeId
        };
        public static CapabilityConsts Active { get; private set; }

        [AdvProp]
        public string Id { get; private set; } = "ID";
        [AdvProp]
        public string Title { get; private set; } = "Capability Title";
        [AdvProp]
        public string Description { get; private set; } = "Description (Req. Statement)";
        [AdvProp]
        public string Subtype { get; private set; } = "Subtype";
        [AdvProp]
        public string Kind { get; private set; } = "OOTB/Config/Custom";
        [AdvProp]
        public string NoOfBuc { get; private set; } = "No of BUC used in";
        [AdvProp]
        public string Sofware { get; private set; } = "Software";
        [AdvProp]
        public string Category { get; private set; } = "Category (e.g. module, application...)";
        [AdvProp]
        public string FuncTitle { get; private set; } = "Function Title";
        [AdvProp]
        public string FuncDesc { get; private set; } = "Function Description";
        [AdvProp]
        public string Availability { get; private set; } = "Availability";
        [AdvProp]
        public string AltIfNotAvail { get; private set; } = "Alternative if not available";
        [AdvProp]
        public string PrioIfNotAvail { get; private set; } = "Priority if not available";
        [AdvProp]
        public string Difficulty { get; private set; } = "Difficulty of execution currently";
        [AdvProp]
        public string DocUrl { get; private set; } = "Doc. URL or Other database xref";
        [AdvProp]
        public string ReuseId { get; private set; } = "ID";
        [AdvProp]
        public string ReuseMeId { get; private set; } = "ID";
        [AdvProp]
        
        public static bool Eval(Workbook workbook)
        {
            Active = ExcelReader.SelectConsts(workbook, SheetName, ConstVersions);

            return Active != null;
        }
        
        public static int UsedConsts()
        {
            for (var i = 0; i < ConstVersions.Length; i++)
                if (ConstVersions[i] == Active)
                    return i;

            return -1;
        }
    }
}
