﻿using System;
using System.Collections.Generic;
using Advntdg.data;

namespace Advntdg.capabilities
{
    internal partial class Capability: AdvData
    {
        public override string ToString()
        {
            return this[CapabilityConsts.Active.Id] + " - " + this[CapabilityConsts.Active.Title];
        }
    }
}