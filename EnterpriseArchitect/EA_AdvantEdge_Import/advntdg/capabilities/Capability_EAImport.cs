﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advntdg.data;
using EA;

namespace Advntdg.capabilities
{
    internal partial class Capability : AdvData 
    {
        private const int CAPWidth = 110;
        private const int CAPHeight = 60;

        public int Import(Package reqPackage, Diagram reqDiagram, ref int reqPos, Package capPackage,
            Diagram capDiagram, ref int capPos)
        {
            EA.Element element = this.Element;

            String subType = "UseCase"; // Default!
            try
            {
                if(CapabilityConsts.Active.Subtype != null)
                    subType = this[CapabilityConsts.Active.Subtype];
            }
            catch (ArgumentException)
            {
            }
            
            if (subType.Equals("Requirement"))
            {
                if (element == null)
                {
                    element = reqPackage.Elements.AddNew(this[CapabilityConsts.Active.Title], "Requirement");
                    element.Update();
                }
                EaImporter.AddToDiagram(reqDiagram, element, reqPos, reqPos + CAPWidth, reqPos, reqPos + CAPHeight);
                reqPos += 20;
            }
            else
            {
                if (element == null)
                {
                    element = capPackage.Elements.AddNew(this[CapabilityConsts.Active.Title], "UseCase");
                    element.Update();
                }
                EaImporter.AddToDiagram(capDiagram, element, capPos, capPos + CAPWidth, capPos, capPos + CAPHeight);
                capPos += 20;
            }


            EaImporter.SetTaggedValues(element, this, CapabilityConsts.ImportAsTaggedValue);
            element.Alias = this[CapabilityConsts.Active.Id];
            string description = this[CapabilityConsts.Active.Description];
            try
            {
                if (CapabilityConsts.Active.FuncDesc != null 
                    && !string.IsNullOrEmpty(this[CapabilityConsts.Active.FuncDesc]))
                {
                    description += "\r\n\r\n<b>Function</b> " + this[CapabilityConsts.Active.FuncTitle] + "\r\n\r\n<i>" +
                                   this[CapabilityConsts.Active.FuncDesc] + "</i>";
                }
            }
            catch (ArgumentException)
            {
            }
            element.Notes = description;

            element.Update();
            this.Element = element;
            return reqPos;
        }

        public static void ImportCapabilities(Capability[] caps, EaImporter importer)
        {
            if (caps == null)
                return;
            importer.Progress?.Report(new ProgressInfo("Create Capabilitiess", caps.Length));

            // Locate package to import Actors to
            importer.Progress?.Report(new ProgressInfo(null, "Locate Packages", false));
            var capPackage = importer.CreateOrGetPackage("Capabilities");
            var reqPackage = importer.CreateOrGetPackage("Requirements");

            EaImporter.MapEA(capPackage, caps, CapabilityConsts.Active.ReuseMeId);
            EaImporter.MapEA(reqPackage, caps, CapabilityConsts.Active.ReuseMeId);

            EA.Diagram capDiagram = importer.CreateOrGetDiagram(capPackage, "Capabilities", "Use Case");
            int capPos = 20;

            EA.Diagram reqDiagram = importer.CreateOrGetDiagram(reqPackage, "Requirements", "Requirements");
            int reqPos = 20;

            foreach (var cap in caps)
            {
                importer.Progress?.Report(new ProgressInfo(null, cap[CapabilityConsts.Active.Title]));
                cap.Import(reqPackage, reqDiagram, ref reqPos, capPackage, capDiagram, ref capPos);
            }
            capPackage.Update();
            reqPackage.Update();
            capDiagram.Update();
            reqDiagram.Update();

            importer.AutoLayout(new Diagram[] {capDiagram, reqDiagram});
        }

    }
}
