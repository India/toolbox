﻿
using System.Collections.Generic;
using Advntdg.data;
using EA;

namespace Advntdg.relations
{
    internal partial class Relation : AdvData
    {
        private Diagram Import(EaImporter importer, Relation relation)
        {
            Diagram result = null;
            var left = importer.GetElement(relation[RelationConsts.Active.Lid], relation[RelationConsts.Active.Ltype], true);
            var right = importer.GetElement(relation[RelationConsts.Active.Rid], relation[RelationConsts.Active.Rtype], false);
            if (left != null && right != null)
            {
                EA.Connector connector = null;
                switch (relation[RelationConsts.Active.Relation])
                {
                    case "extends":
                        connector = EaImporter.AddConnector(left, right, "", "UseCase", "extend", "Extends");
                        break;
                    case "next in process":
                        connector = EaImporter.AddConnector(left, right, "", "SequenceFlow");
                        importer.LinkBucs(left, right);
                        break;
                    case "is major cap. for":
                        connector = EaImporter.AddConnector(left, right, "Is Major Capability for", "trace");
                        break;
                    case "requires":
                        connector = EaImporter.AddConnector(left, right, "", "Realisation");
                        break;
                    case "uses":
                        connector = EaImporter.AddConnector(left, right, "", "invokes");
                        break;
                    default:
                        return null;
                }

                if (connector != null) {
                    if (relation[RelationConsts.Active.Ltype].Equals("BUC") && !relation[RelationConsts.Active.Rtype].Equals("BUC")) { 
                        result = importer.GetBucFromId(relation[RelationConsts.Active.Lid]).Diagram;
                        if (result != null)
                            EaImporter.AddToDiagram(result, right);
                    }
                    if (relation[RelationConsts.Active.Rtype].Equals("BUC") && !relation[RelationConsts.Active.Ltype].Equals("BUC"))
                    {
                        result = importer.GetBucFromId(relation[RelationConsts.Active.Rid]).Diagram;
                        if (result != null)
                            EaImporter.AddToDiagram(result, left);
                    }
                }
            }
            return result;
        }

        public static void ImportRelations(Relation[] rels, EaImporter importer)
        {
            var diagramsToLayout = new HashSet<Diagram>();
            if (rels == null)
                return;

            importer.Progress?.Report(new ProgressInfo("Create Relations", rels.Length));

            foreach (var relation in rels)
            {
                importer.Progress?.Report(new ProgressInfo(null, relation.ToString()));
                var diagram = relation.Import(importer, relation);
                if (diagram != null)
                    diagramsToLayout.Add(diagram);
            }

            importer.AutoLayout(diagramsToLayout);

        }
    }
}
