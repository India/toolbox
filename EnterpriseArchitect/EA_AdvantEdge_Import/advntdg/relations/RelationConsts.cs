﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using advntdg;
using Microsoft.Office.Interop.Excel;

namespace Advntdg.relations
{
    internal class RelationConsts
    {
        public const string SheetName = @"Relationship Table";

        private static readonly RelationConsts[] ConstVersions =
        {
            new RelationConsts() // BEGIN / HTE
            {
               Lid = "LeftID",
               Ltype = "LeftType",
               RelationType = "RelType",
               Rid = "RightID",
               Rtype = "RightType",
               BucContext = "BUCContext",
               ProcessContext = "ProcessContext",
               Ltitle = "Left Title",
               Relation = "Relation",
               Rtitle = "Right Title"
            }
        };
        
        public static RelationConsts Active { get; private set; }


        [AdvProp]
        public string Lid { get; private set; } = "LeftID";
        [AdvProp]
        public string Ltype { get; private set; } = "LeftType";
        [AdvProp]
        public string RelationType { get; private set; } = "RelType";
        [AdvProp]
        public string Rid { get; private set; } = "RightID";
        [AdvProp]
        public string Rtype { get; private set; } = "RightType";
        [AdvProp]
        public string BucContext { get; private set; } = "BUCContext";
        [AdvProp]
        public string ProcessContext { get; private set; } = "ProcessContext";
        [AdvProp]
        public string Ltitle { get; private set; } = "Left Title";
        [AdvProp]
        public string Relation { get; private set; } = "Relation";
        [AdvProp]
        public string Rtitle { get; private set; } = "Right Title";


        public static IReadOnlyList<string> ImportAsTaggedValue => new List<string>
        {
            Active.BucContext,
            Active.ProcessContext
        };
        
        public static bool Eval(Workbook workbook)
        {
            Active = ExcelReader.SelectConsts(workbook, SheetName, ConstVersions);

            return Active != null;
        }
        
        public static int UsedConsts()
        {
            for (var i = 0; i < ConstVersions.Length; i++)
                if (ConstVersions[i] == Active)
                    return i;

            return -1;
        }
    }
}
