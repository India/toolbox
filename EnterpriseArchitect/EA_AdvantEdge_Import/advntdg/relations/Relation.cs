﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advntdg.data;

namespace Advntdg.relations
{
    internal partial class Relation : AdvData
    {
        public override string ToString()
        {
            return this[RelationConsts.Active.Ltitle] + " -> " + this[RelationConsts.Active.Rtitle];
        }
    }

}
