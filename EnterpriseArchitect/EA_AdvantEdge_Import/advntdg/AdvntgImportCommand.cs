﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using EA;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Text;
using advntdg;
using Advntdg.actors;
using Advntdg.bucs;
using Advntdg.capabilities;
using Advntdg.relations;
using Task = System.Threading.Tasks.Task;

namespace Advntdg
{
    internal class AdvntgImportCommand
    {
        private ProgressForm _progressForm;
        private static Microsoft.Office.Interop.Excel.Application excelApp;

        private class ExcelData
        {
            public ExcelData(string file, string package)
            {
                File = file;
                Package = package;
            }
            internal string File { get; }
            internal string Package { get; }
            internal Workbook Workbook { get; set; }
        }

        internal static void Execute(Repository repository)
        {
            using (var dlg = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = @"Excel files (*.xlsx;*.xlsm)|*.xlsx;*.xlsm|All files (*.*)|*.*",
                Title = Resources.FileDialog_SelectAdvantEdge_Excel
            })
            {

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    object selected;
                    repository.GetTreeSelectedItem(out selected);

                    var rootPackage = selected as Package;
                    if (rootPackage != null)
                    {
                        var command = new AdvntgImportCommand();
                        command.ExecuteSingle(repository, dlg.FileName, rootPackage);
                    }
                }
            }

        }

        internal static void ExecuteMultiple(Repository repository)
        {
            using (var dlg = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = @"Excel files (*.xlsx;*.xlsm)|*.xlsx;*.xlsm|All files (*.*)|*.*",
                Title = Resources.FileDialog_MainExcelFileHeader
            })
            {
                var result = dlg.ShowDialog();
                if (result == DialogResult.OK)
                {
                    var selectedFile = dlg.FileName;
                    var path = selectedFile.Remove(selectedFile.LastIndexOf(Path.DirectorySeparatorChar));
                    List<ExcelData> importData = new List<ExcelData>();
            
                    foreach (var directory in Directory.EnumerateDirectories(path))
                    {
                        foreach (var file in Directory.EnumerateFiles(directory))
                        {
                            var parts = file.Split(Path.DirectorySeparatorChar);
                            var fileName = parts[parts.Length - 1];
                            if (fileName.StartsWith("~$")) // skip office temporary files
                                continue;
                          
                            if (file.ToLower().EndsWith(@".xlsx") || file.ToLower().EndsWith(@".xlsm"))
                            {
                                importData.Add(new ExcelData(file, parts[parts.Length - 2]));
                            }
                        }
                    }

                    object selected;
                    repository.GetTreeSelectedItem(out selected);

                    var rootPackage = selected as Package;
                    if (rootPackage != null)
                    {
                        var command = new AdvntgImportCommand();
                        command.ExecuteMultiple(repository, rootPackage, importData.ToArray());
                    }
                }
            }
        }

        private void ReportProgress(ProgressInfo info)
        {
            if (info.Reset)
            {
                this._progressForm.Max = info.Max;
                this._progressForm.MainJob = info.Text;
                this._progressForm.SubJob = info.SubText;
            }
            else
            {
                if (info.Text != null)
                    this._progressForm.MainJob = info.Text;
                if (info.SubText != null)
                    this._progressForm.SubJob = info.SubText;
                if(info.PerformStep)
                    this._progressForm.PerformStep();
            }
        }

        private static Workbook OpenWorkbook(string fileName)
        {
            return ExcelApp.Workbooks.Open(fileName, ReadOnly: true, UpdateLinks: false, AddToMru: false,
                Notify: false, IgnoreReadOnlyRecommended: true);
        }

        private async void ExecuteSingle(Repository repository, string fileName, Package rootPackage)
        {
            this._progressForm = new ProgressForm();
            _progressForm.Text = Resources.StartImport;
            _progressForm.Show();
            _progressForm.Visible = true;
            var progressIndicator = new Progress<ProgressInfo>(ReportProgress);

            var workbook = OpenWorkbook(fileName);
            await Execute(repository, workbook, rootPackage, progressIndicator);

            workbook.Close(false);
            Marshal.ReleaseComObject(workbook);

            ExcelApp = null;
            _progressForm.Visible = false;
            _progressForm.Close();
            _progressForm = null;

        }
        private async void ExecuteMultiple(Repository repository, Package rootPackage, ExcelData[] dataArray)
        {
            this._progressForm = new ProgressForm();
            _progressForm.Text = Resources.StartImport;
            _progressForm.Show();
            _progressForm.Visible = true;
            var progressIndicator = new Progress<ProgressInfo>(ReportProgress);

            foreach (var data in dataArray)
            {
                data.Workbook = OpenWorkbook(data.File);
            }

            foreach(var data in dataArray) { 
                await Execute(repository, data.Workbook, rootPackage, progressIndicator, data.Package);
            }

            foreach (var data in dataArray)
            {
                data.Workbook.Close(false);
                Marshal.ReleaseComObject(data.Workbook);
            }
            ExcelApp = null;
            _progressForm.Visible = false;
            _progressForm.Close();
            _progressForm = null;

        }

        private static Microsoft.Office.Interop.Excel.Application ExcelApp
        {
            get
            {
                if(excelApp == null)
                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                return excelApp;
            }
            set
            {
                if (value == null)
                {
                    if (excelApp != null)
                    {
                        excelApp.Quit();
                        Marshal.ReleaseComObject(excelApp);
                        excelApp = null;
                    }
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        private async Task Execute(Repository repository, Workbook workbook, Package rootPackage, IProgress<ProgressInfo> progressIndicator, string packageName = null)
        {
            var progress = progressIndicator;
            if (progressIndicator == null)
            {
                this._progressForm = new ProgressForm();
                _progressForm.Text = Resources.StartImport;
                _progressForm.Show();
                _progressForm.Visible = true;
                progress = new Progress<ProgressInfo>(ReportProgress);
            }

            if (workbook == null)
                return;

            string title = "Importing ";
            if (packageName != null)
            {
                title += packageName;
            }
           
            _progressForm.Text = title;

            var importData = await ReadData(workbook, progress);
            //workbook.Close(false);
            
            if(importData != null)
                await ApplyData(repository, importData, progressIndicator, rootPackage, packageName);

            if (progressIndicator == null)
            {
                _progressForm.Visible = false;
                _progressForm.Close();
                _progressForm = null;
            }
            
            if (BucConsts.Active == null ||
                ActorConsts.Active == null ||
                CapabilityConsts.Active == null ||
                RelationConsts.Active == null)
            {
                var msg = new StringBuilder();
                msg.AppendFormat("Used Constants: Bucs({0}), Actors({1}), Caps({2}), Rels({3})\n",
                    BucConsts.UsedConsts(), ActorConsts.UsedConsts(), CapabilityConsts.UsedConsts(),
                    RelationConsts.UsedConsts());

                if (BucConsts.Active == null)
                {
                    msg.AppendLine("BUC Headers: ");
                    ExcelReader.ReportHeaders(workbook, msg, BucConsts.SheetName);
                }
                    
                if (ActorConsts.Active == null)
                {
                    msg.AppendLine("ACT Headers: ");
                    ExcelReader.ReportHeaders(workbook, msg, ActorConsts.SheetName);
                }

                if (CapabilityConsts.Active == null)
                {
                    msg.AppendLine("CAP Headers: ");
                    ExcelReader.ReportHeaders(workbook, msg, CapabilityConsts.SheetName);
                }

                if (RelationConsts.Active == null)
                {
                    msg.AppendLine("REL Headers: ");
                    ExcelReader.ReportHeaders(workbook, msg, RelationConsts.SheetName);
                }

                MessageBox.Show(msg.ToString(), Resources.ErrorDialog_CouldNotFindImportCOnfig, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private static async Task<ImportData> ReadData(Workbook workbook, IProgress<ProgressInfo> progress)
        {
            return await Task.Run(() =>
            {
                var result = new ImportData();
                var importer = new ExcelReader();

                BucConsts.Eval(workbook);
                ActorConsts.Eval(workbook);
                CapabilityConsts.Eval(workbook);
                RelationConsts.Eval(workbook);

                if (BucConsts.Active == null ||
                    ActorConsts.Active == null ||
                    CapabilityConsts.Active == null ||
                    RelationConsts.Active == null)
                    return null;
                progress?.Report(new ProgressInfo("Reading Actors...", 5));
                result.Actors = importer.Read<Actor>(workbook, progress, ActorConsts.SheetName);

                progress?.Report(new ProgressInfo("Reading Bucs...", ""));
                result.Bucs = importer.Read<Buc>(workbook, progress, BucConsts.SheetName);

                progress?.Report(new ProgressInfo("Reading Capabilities & Requirements...", ""));
                result.Capabilities = importer.Read<Capability>(workbook, progress, CapabilityConsts.SheetName);

                progress?.Report(new ProgressInfo("Reading Relations...", ""));
                result.Relations = importer.Read<Relation>(workbook, progress, RelationConsts.SheetName);

                return result;
            });
        }

        private static async Task ApplyData(Repository repository, ImportData data, IProgress<ProgressInfo> progress, Package rootPackage,  string packageName = null)
        {
            var importer = new EaImporter(repository);
            var package = packageName != null ? importer.CreateOrGetPackage(packageName, rootPackage) : rootPackage;

            await importer.Import(package, data, progress);
        }
    }
}
