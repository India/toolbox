﻿using System;
using System.Collections.Generic;
using Advntdg.actors;
using Advntdg.bucs;
using Advntdg.capabilities;
using Advntdg.data;
using Advntdg.relations;
using EA;
using Task = System.Threading.Tasks.Task;

namespace Advntdg
{
    /// <summary>
    /// Central class to organize the upload the data to EA and provide helper methods to
    /// handle EA
    /// </summary>
    internal class EaImporter
    {

        /// <summary>
        /// ctor to create EA Importer
        /// </summary>
        /// <param name="repository">EA Repository</param>
        public EaImporter(Repository repository)
        {
            Repository = repository;
        }


        /// <summary>
        /// Main function to start the import to EA
        /// </summary>
        /// <param name="package">The selected EA package to import the data to</param>
        /// <param name="data">ImportData structure, containing the data to import</param>
        /// <param name="progress">Progress information to notify UI</param>
        /// <returns>Task to execute job</returns>
        public Task Import(Package package, ImportData data, IProgress<ProgressInfo> progress)
        {
            this.Progress = progress;
            this.Data = data;
            this.Package = package;
            return Task.Run(() =>
            {
                Actor.ImportActors(Data.Actors, this);
                var bucPackage = Buc.ImportBucs(Data.Bucs, this);
                Capability.ImportCapabilities(Data.Capabilities, this);
                
                // Link BUCs with Actors
                PreProcessRelations();

                Relation.ImportRelations(Data.Relations, this);

                Buc.LayoutBUCDiagram(this, bucPackage);
            });
        }

        /// <summary>
        /// Method to update relations before importing them to EA.
        /// Evaluates "Actor ID" attribute on BUCs and adds uses relations
        /// between this actor and the BUC
        /// </summary>
        private void PreProcessRelations()
        {
            var relations = new List<Relation>();
            relations.AddRange(Data.Relations);
            foreach (var buc in Data.Bucs)
            {
                var actorId = buc[BucConsts.Active.ActorId];
                if (string.IsNullOrEmpty(actorId) == false)
                {
                    var actor = GetActorFromId(actorId);
                    if (actor != null)
                    {
                        var newRelation = new Relation();
                        newRelation[RelationConsts.Active.Lid] = actorId;
                        newRelation[RelationConsts.Active.Ltype] = "Actor";
                        newRelation[RelationConsts.Active.Ltitle] = "Actor";
                        newRelation[RelationConsts.Active.Rid] = buc[BucConsts.Active.Id];
                        newRelation[RelationConsts.Active.Rtype] = "BUC";
                        newRelation[RelationConsts.Active.Rtitle] = "BUC";
                        newRelation[RelationConsts.Active.Relation] = "uses";

                        relations.Add(newRelation);
                    }
                }
            }
            Data.Relations = relations.ToArray();
        }

        /// <summary>
        /// Property to access the main EA package
        /// </summary>
        private Package Package { get; set; }

        /// <summary>
        /// Property to access all import data
        /// </summary>
        public ImportData Data { get; private set; }

        /// <summary>
        ///  Property to access progress info
        /// </summary>
        public IProgress<ProgressInfo> Progress { get; private set; }

        /// <summary>
        /// Property to access the EA Repository
        /// </summary>
        private Repository Repository { get; set; }

        /// <summary>
        /// Property to hold "special" End-Element (for BUC Diagram)
        /// </summary>
        public Buc EndElement { get; set; }

        /// <summary>
        /// Property to hold "special" Start-Element (for BUC Diagram)
        /// </summary>
        public Buc StartElement { get; set; }

        /// <summary>
        /// Generic method to fill the TaggedValues
        /// </summary>
        /// <param name="element">EA element to update the tagged values</param>
        /// <param name="data">Data object containing the values</param>
        /// <param name="taggedValues">List of attributes to store as tagged values</param>
        public static void SetTaggedValues(Element element, AdvData data, IReadOnlyList<string> taggedValues)
        {
            foreach (var key in taggedValues)
            {
                try
                {
                    if (key != null)
                    {
                        var stringValue = data[key];
                        if (stringValue.Length > 255)
                        {
                            stringValue = stringValue.Remove(250);
                            stringValue += " ...";
                            System.Console.Out.WriteLine("Value of Tagged Value <" + key +
                                                         "> is too long and got cut <" +
                                                         stringValue + ">");
                        }

                        EA.TaggedValue value = element.TaggedValues.AddNew(key, stringValue);
                        value.Update();
                    }
                }
                catch (ArgumentException ex)
                {
                    System.Console.Out.WriteLine("Tagged Value " + key + " not found");
                }
            }
        }

        /// <summary>
        /// Helper method to map EA elements to data objects, using the given tagged value
        /// </summary>
        /// <typeparam name="T">Type of dataobject</typeparam>
        /// <param name="package">Package to look for elements</param>
        /// <param name="data">List of data objects to map</param>
        /// <param name="key">Attribute key to use for mapping</param>
        internal static void MapEA<T>(Package package, IEnumerable<T> data, string key) where T : AdvData
        {
            var eaDict = new Dictionary<string, Element>();
            foreach (EA.Element element in package.Elements)
            {
                EA.TaggedValue value = element.TaggedValues.GetByName(key);
                if (value != null)
                    eaDict[value.Value] = element;
            }

            foreach (var obj in data)
            {
                if (eaDict.ContainsKey(obj[key]))
                    obj.Element = eaDict[obj[key]];
            }
        }



        /// <summary>
        /// Locates an Element by a given id and type and whether it is on left side of a relation.
        /// The "side" is only relevant for type "Start/End"
        /// </summary>
        /// <param name="id">ID to match to</param>
        /// <param name="type">Type name - can be "BUC", "Capability", "Requirement", "Start/End"</param>
        /// <param name="isLeft">Flag whether the requested element will be on the left side of a relation</param>
        /// <returns></returns>
        internal Element GetElement(string id, string type, bool isLeft)
        {
            Element result = null;
            switch (type)
            {
                case "BUC":
                    var buc = GetBucFromId(id);
                    result = buc?.Element;
                    break;
                case "Capability":
                case "Requirement":
                    Capability cap = null;
                    foreach (var capImport in this.Data.Capabilities)
                        if (capImport[CapabilityConsts.Active.Id].Equals(id))
                        {
                            cap = capImport;
                            break;
                        }
                    result = cap?.Element;
                    break;
                case "Start/End":
                    if (isLeft)
                        result = StartElement.Element;
                    else
                        result = EndElement.Element;
                    break;
                case "Actor":
                    var actor = GetActorFromId(id);
                    result = actor?.Element;
                    break;
                default:
                    break;
            }
            return result;
        }

        /// <summary>
        /// Scans the import data for a BUC with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Buc GetBucFromId(string id)
        {
            Buc result = null;
            foreach (var bucImport in this.Data.Bucs)
                if (bucImport[BucConsts.Active.Id].Equals(id))
                {
                    result = bucImport;
                    break;
                }
            return result;
        }

        /// <summary>
        /// Scans the import data for an Actor with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Actor GetActorFromId(string id)
        {
            Actor result = null;
            foreach (var actorImport in this.Data.Actors)
                if (actorImport[ActorConsts.Active.Id].Equals(id))
                {
                    result = actorImport;
                    break;
                }
            return result;
        }


        /// <summary>
        /// Gets or Creates a package with the given name below the given (optional)parent.
        /// If the parent is null, the main package will be sed
        /// </summary>
        /// <param name="name">Name of the package</param>
        /// <param name="parent">Optional: Parent package</param>
        /// <returns></returns>
        internal Package CreateOrGetPackage(string name, EA.Package parent = null)
        {
            if (parent == null)
                parent = Package;

            EA.Package result = null;

            foreach (EA.Package current in parent.Packages)
            {
                if (current.Name.Equals(name))
                {
                    result = current;
                    break;
                }
            }

            if (result == null)
            {
                result = parent.Packages.AddNew(name, "Package");
                result.Update();
            }
            return result;
        }

        /// <summary>
        /// Creates or Gets a diagram of the given name below a package.
        /// If a diagram of the given name already exists this will be returned.
        /// </summary>
        /// <param name="package">Package to look for or create the requested diagram</param>
        /// <param name="diagramName">Name of the diagram</param>
        /// <param name="diagramType">In case of create: name of the diagram type</param>
        /// <returns></returns>
        public Diagram CreateOrGetDiagram(Package package, string diagramName, string diagramType)
        {
            Diagram result = null;
            try
            {
                result = package.Diagrams.GetByName(diagramName);
            }
            catch (Exception)
            {
            }
            if (result == null)
            {
                result = package.Diagrams.AddNew(diagramName, diagramType);
                result.Update();
            }

            return result;
        }

        /// <summary>
        /// Adds an element to a diagram if it is not already linked there.
        /// Position data is optional
        /// </summary>
        /// <param name="diagram">Diagram to add the element to</param>
        /// <param name="element">The element to add</param>
        /// <param name="l">Left position (optional)</param>
        /// <param name="r">Right position (optional)</param>
        /// <param name="t">Top position (optional)</param>
        /// <param name="b">Bottom position (optional)</param>
        /// <returns>True if the element was newly added</returns>
        public static bool AddToDiagram(Diagram diagram, Element element, 
            int l = -1, int r = -1, int t = -1,int b = -1)
        {
            EA.DiagramObject dummy = null;
            return AddToDiagram(diagram, element, out dummy, l, r, t, b);
        }
        
        /// <summary>
        /// Adds an element to a diagram if it is not already linked there.
        /// Position data is optional
        /// </summary>
        /// <param name="diagram">Diagram to add the element to</param>
        /// <param name="element">The element to add</param>
        /// <<param name="diagramObject">Out: Diagram Object created/found</param>
        /// <param name="l">Left position (optional)</param>
        /// <param name="r">Right position (optional)</param>
        /// <param name="t">Top position (optional)</param>
        /// <param name="b">Bottom position (optional)</param>
        /// <returns>True if the element was newly added</returns>
        public static bool AddToDiagram(Diagram diagram, Element element, out DiagramObject diagramObject,
                                        int l = -1, int r = -1, int t = -1,int b = -1)
        {
            var create = true;
            diagramObject = null;
            foreach (DiagramObject obj in diagram.DiagramObjects)
            {
                if (obj.ElementID == element.ElementID)
                {
                    create = false;
                    diagramObject = obj;
                }
            }

            if (create)
            {
                
                if(l<0 || r<0 || t<0 || b<0)
                    diagramObject = diagram.DiagramObjects.AddNew("", "");
                else
                    diagramObject = diagram.DiagramObjects.AddNew("l=" + l + ";r=" + r + ";t=" + t + ";b=" + b + ";", "");
                diagramObject.ElementID = element.ElementID;
                try
                {
                    diagramObject.Update();
                }
                catch (Exception e)
                {
                    //???
                    System.Console.Out.WriteLine(e.ToString());
                }

                try
                {
                    diagram.Update();
                }
                catch (Exception e)
                {
                    //???
                    System.Console.Out.WriteLine(e.ToString());
                }
            }

            return create;
        }
        

        /// <summary>
        /// Adds a connector (relation) between two elements if it does not exist
        /// </summary>
        /// <param name="left">Left element</param>
        /// <param name="right">Right element</param>
        /// <param name="name">Name of connector</param>
        /// <param name="type">type of connector</param>
        /// <returns></returns>
        public static Connector AddConnector(Element left, Element right, string name, string type, string stereotype = null, string subtype = null)
        {
            EA.Connector result = null;
            string typeLwr = type.ToLower();
            foreach (EA.Connector c in left.Connectors)
            {
                if (c.SupplierID == right.ElementID)
                {
                    //TODO: Check Type evaluation - I hope there is a better way than that:
                    String type1 = c.Type.ToLower();
                    String type2 = c.ObjectType.ToString().ToLower();
                    String type3 = c.MetaType.ToLower();
                    String type4 = c.Stereotype.ToLower();
                    if (type1.Equals(typeLwr) || type2.Equals(typeLwr) || type3.Equals(typeLwr) || type4.Equals(typeLwr))
                    {
                        result = c;
                    }
                }

            }
            if (result == null)
            {
                result = left.Connectors.AddNew(name, type);
                if (stereotype == null)
                    result.Stereotype = type;
                else
                    result.Stereotype = stereotype;

                if (subtype != null)
                    result.Subtype = subtype;
                
                result.SupplierID = right.ElementID;
                result.Update();
                left.Connectors.Refresh();
            }
            return result;
        }

        /// <summary>
        /// Method to autolayout a diagram
        /// </summary>
        /// <param name="diagram"></param>
        public void AutoLayout(Diagram diagram)
        {
            AutoLayout(new Diagram[] {diagram});
        }

        /// <summary>
        /// Method to autolayout a set of diagrams
        /// </summary>
        /// <param name="diagrams"></param>
        public void AutoLayout(IEnumerable<Diagram> diagrams)
        {
            var project = Repository.GetProjectInterface();
            foreach (var diagram in diagrams)
            {
                project.LayoutDiagramEx(diagram.DiagramGUID, 0, 4, 20, 20, false);
                Repository.SaveDiagram(diagram.DiagramID);
                Repository.CloseDiagram(diagram.DiagramID);
            }
        }

        /// <summary>
        /// Helpermethis to link BUCs
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        public void LinkBucs(Element left, Element right)
        {
            Buc leftBuc = GetBucByElement(left);
            Buc rightBuc = GetBucByElement(right);

            if (leftBuc != null && rightBuc != null)
            {
                leftBuc.AddSuccessor(rightBuc);
            }
        }

        /// <summary>
        /// Helper method to find a BUC by EA Element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private Buc GetBucByElement(Element element)
        {
            if (element.Stereotype.Equals("StartEvent"))
                return StartElement;
            if (element.Stereotype.Equals("EndEvent"))
                return EndElement;
            foreach (var buc in Data.Bucs)
            {
                if (buc.Element == element)
                    return buc;
            }
            return null;
        }
    }
}
