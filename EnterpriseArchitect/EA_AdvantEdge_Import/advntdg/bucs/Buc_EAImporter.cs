﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Advntdg.actors;
using Advntdg.bucs;
using Advntdg.data;
using EA;

namespace Advntdg.bucs
{
    /// <summary>
    /// Partial class for Buc contains the EA Import logic for BUCs
    /// </summary>
    internal partial class Buc : AdvData
    {
        /// <summary>
        /// Imports this BUC to EA. Creates/Updates the BUC, a subpackage and a Diagram for this BUC
        /// </summary>
        /// <param name="importer">The importer</param>
        /// <param name="bucPackage">The main BUC package</param>
        private void Import(EaImporter importer, EA.Package bucPackage)
        {
            var eaBuc = this.Element;
            
            string title = this[BucConsts.Active.Title];
            if (string.IsNullOrEmpty(title))
                title = this[BucConsts.Active.Id] + ": ---";

            if (eaBuc == null)
            {
                eaBuc = bucPackage.Elements.AddNew(title, "Activity");
            }
            eaBuc.Stereotype = "BPMN2.0::Activity";
            EaImporter.SetTaggedValues(eaBuc, this, BucConsts.ImportAsTaggedValue);
            eaBuc.Alias = this[BucConsts.Active.Id];
            eaBuc.Notes = "<b>DESCRIPTION</b>\r\n\r\n" + this[BucConsts.Active.Description] +
                          "\r\n\r\n<b>REQUIREMENTS</b>\r\n\r\n" + this[BucConsts.Active.Requirements];
            eaBuc.Update();
            this.Element = eaBuc;


            Package subPackage = importer.CreateOrGetPackage(title, bucPackage);
            
            Diagram subDiagram = importer.CreateOrGetDiagram(subPackage, title, "BPMN2.0::Business Process");
            EaImporter.AddToDiagram(subDiagram, eaBuc);
            eaBuc.SetCompositeDiagram(subDiagram.DiagramGUID);
            eaBuc.Update();
            Diagram = subDiagram;
        }

        /// <summary>
        /// Imports all given BUCs
        /// </summary>
        /// <param name="bucs">Array of BUCs to import</param>
        /// <param name="importer">Importer</param>
        /// <returns></returns>
        public static Package ImportBucs(Buc[] bucs, EaImporter importer)
        {
            if (bucs == null)
                return null;

            importer.Progress?.Report(new ProgressInfo("Create BUCs", bucs.Length));

            // Locate package to import Actors to
            importer.Progress?.Report(new ProgressInfo(null, "Locate Package", false));
            var bucPackage = importer.CreateOrGetPackage("BUCs");
            EaImporter.MapEA(bucPackage, bucs, BucConsts.Active.BucRusemeId);

            
            // generate start/end elements
            EA.Element startElement = bucPackage.Elements.GetByName("Start");
            if (startElement == null)
            {
                startElement = bucPackage.Elements.AddNew("Start", "BPMN2.0::StartEvent");
                startElement.Stereotype = "StartEvent";
                startElement.Update();
            }

            EA.Element endElement = bucPackage.Elements.GetByName("End");
            if (endElement == null)
            {
                endElement = bucPackage.Elements.AddNew("End", "BPMN2.0::EndEvent");
                endElement.Stereotype = "EndEvent";
                endElement.Update();
            }

            importer.StartElement = new Buc();
            importer.StartElement.Element = startElement;
            importer.EndElement = new Buc();
            importer.EndElement.Element = endElement;
                
            foreach (var buc in bucs)
            {
                importer.Progress?.Report(new ProgressInfo(null, buc[BucConsts.Active.Title]));
                buc.Import(importer, bucPackage);
            }
            bucPackage.Update();

            return bucPackage;
        }
        
        

    }
}
