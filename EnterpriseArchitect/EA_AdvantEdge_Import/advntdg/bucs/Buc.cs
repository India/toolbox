﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using Advntdg.data;
using EA;

namespace Advntdg.bucs
{
    /// <summary>
    /// Partial (Main) class. The real functionality is defined in
    /// Other partial classes. 
    /// </summary>
    internal partial class Buc : AdvData
    {
        /// <summary>
        /// Property to store the BUC-local diagram
        /// </summary>
        internal Diagram Diagram { get; set; }
        
        /// <summary>
        /// ToString definition for BUCs
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this[BucConsts.Active.Id] + " - " + this[BucConsts.Active.Title];
        }
    }
}