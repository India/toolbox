﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Advntdg.actors;
using EA;

namespace Advntdg.bucs
{
    /// <summary>
    /// Partial class of BUC to define Layout logic for the main BUC Diagram
    /// </summary>
    internal partial class Buc
    {
        // constants to define sizes in EA
        private const int BUCWidth = 110;
        private const int BUCHeight = 60;

        private const int EVNWidth = 20;
        private const int EVNHeight = 20;
        private const int MARGIN = 20;
        
        /// <summary>
        /// Helper class used to pass arguments
        /// </summary>
        internal class LayoutData
        {
            /// <summary>
            /// The BUC-Main diagram
            /// </summary>
            public Diagram diagram;
            
            /// <summary>
            /// Dictionary to store horizontal offset information
            /// </summary>
            public Dictionary<string, int> laneOffsets;
            
            /// <summary>
            /// List of (reminaing) Bucs to add to the diagram
            /// </summary>
            public HashSet<Buc> unhandledBucs;
            
            /// <summary>
            /// The importer object
            /// </summary>
            public EaImporter importer;
        }

        /// <summary>
        /// Helper class to return information about the next BUCs in the sequence
        /// </summary>
        internal class NextBuc
        {
            public NextBuc(Buc previous, Buc next)
            {
                Previous = previous;
                Buc = next;
            }
            public Buc Buc { get; set; }
            public Buc Previous { get; set; }
        }
        
        /// <summary>
        /// Static function to be called after all relations were applied. This function 
        /// will create the main diagram for BUCs and will try to layout it.
        /// NOTE: currently this works only in case a new diagram will be created.
        /// </summary>
        /// <param name="importer">The importer object</param>
        /// <param name="bucPackage">The package of all BUCs</param>
        public static void LayoutBUCDiagram(EaImporter importer, Package bucPackage)
        {
            var data = new LayoutData();
            
            data.importer = importer;
            data.diagram = importer.CreateOrGetDiagram(bucPackage, "BUCs", "BPMN2.0::Business Process");
            data.laneOffsets = new Dictionary<string, int>();
            data.unhandledBucs = new HashSet<Buc>(importer.Data.Bucs);

            Layout(importer.StartElement, null, data);
    
        }

        /// <summary>
        /// Function to add a BUC and to the diagramm and returns the next level. The Start 
        /// Element will organise a "breadth-first search" kind of adding following elements.
        /// </summary>
        /// <param name="buc">Buc to add</param>
        /// <param name="previous">(optional) the previous BUC</param>
        /// <param name="data">The general data</param>
        /// <returns></returns>
        private static HashSet<NextBuc> Layout(Buc buc, Buc previous, LayoutData data)
        {
            if (buc.DiagramObject != null) return new HashSet<NextBuc>();

            if (buc == data.importer.StartElement)
            {
                EA.DiagramObject diagramObject;
                EaImporter.AddToDiagram(data.diagram, buc.Element, out diagramObject);
                buc.DiagramObject = diagramObject;
                buc.VerticalOffset = diagramObject.bottom;
                buc.HorizontalOffset = diagramObject.right;

                HashSet<NextBuc> nextLevel = GetNextData(buc);
                while (nextLevel != null && nextLevel.Count > 0)
                {
                    HashSet<NextBuc> nextNextLevel = new HashSet<NextBuc>();
                    foreach (var nextBuc in nextLevel)
                    {
                        if (data.unhandledBucs.Contains(nextBuc.Buc))
                        {
                            nextNextLevel.UnionWith(Layout(nextBuc.Buc, nextBuc.Previous, data));
                        }
                    }
                    nextNextLevel.RemoveWhere(b => data.unhandledBucs.Contains(b.Buc) == false);
                    if (nextNextLevel.Count == 0)
                    {
                        nextLevel = new HashSet<NextBuc>();                        
                        if(data.unhandledBucs.Count > 0)
                            nextLevel.Add(new NextBuc(null, data.unhandledBucs.First()));
                    }
                    else
                    {
                        nextLevel = nextNextLevel;
                    }
                }
                return null; 
            }
            else
            {
                int offset = previous != null ? previous.HorizontalOffset : 0;
                AddToDiagramWithSwimlane(buc, data, offset + MARGIN);
                data.unhandledBucs.Remove(buc);
                return GetNextData(buc);
            }
        }

        /// <summary>
        /// Helper method to create a HashSet for the successor BUCs
        /// </summary>
        /// <param name="buc"></param>
        /// <returns></returns>
        private static HashSet<NextBuc> GetNextData(Buc buc)
        {
            var result = new HashSet<NextBuc>();
            foreach (var nextBuc in buc.Successors)
                result.Add(new NextBuc(buc, nextBuc));
            return result;
        }
        
        /// <summary>
        /// Helper methid to add a buc to a swimlane which might be created, too
        /// </summary>
        /// <param name="buc">The buc top add</param>
        /// <param name="data">General data information</param>
        /// <param name="offset">Offset based on "Previous" might be corrected intern</param>
        /// <returns></returns>
        private static bool AddToDiagramWithSwimlane(Buc buc, LayoutData data, int offset)
        {
            var create = true;

            foreach (DiagramObject obj in data.diagram.DiagramObjects)
            {
                if (obj.ElementID == buc.Element.ElementID)
                {
                    create = false;
                }
            }

            if (create)
            {
                Actor swimlaneActor = data.importer.GetActorFromId(buc[BucConsts.Active.ActorId]);
                int verticalOffset = 0;
                int horizontalOffset = offset;
                if (swimlaneActor != null && swimlaneActor.Element != null)
                {
                    string guid = swimlaneActor.Element.ElementGUID;
                    if (data.laneOffsets.ContainsKey(guid))
                    {
                        int laneOffset = data.laneOffsets[guid];
                        horizontalOffset = laneOffset > offset ? laneOffset : offset;
                    }

                    EA.Swimlane useLane = null;
                    EA.SwimlaneDef swimLaneDef = data.diagram.SwimlaneDef;
                    foreach (EA.Swimlane lane in swimLaneDef.Swimlanes)
                    {
                        if (lane.ClassifierGuid.Equals(guid))
                        {
                            useLane = lane;
                            break;
                        }
                        else
                        {
                            verticalOffset += lane.Width;
                        }
                    }

                    if (useLane == null)
                    {
                        swimLaneDef.Orientation = SwimlaneOrientationType.soHorizontal;
                        useLane = swimLaneDef.Swimlanes.Add(swimlaneActor[ActorConsts.Active.Name], 200);
                        useLane.ClassifierGuid = guid;
                    }

                    horizontalOffset += MARGIN;
                    data.laneOffsets[guid] = horizontalOffset + BUCWidth;
                }

                var l = horizontalOffset;
                var r = l + BUCWidth;
                var t = verticalOffset + 100 - BUCHeight / 2;
                var b = t + BUCHeight;

                EA.DiagramObject diagramObject = data.diagram.DiagramObjects.AddNew("l=" + l + ";r=" + r + ";t=" + t + ";b=" + b + ";", "");
                diagramObject.ElementID = buc.Element.ElementID;
                
                diagramObject.Update();
                data.diagram.Update();

                buc.DiagramObject = diagramObject;
                buc.VerticalOffset = diagramObject.bottom;
                buc.HorizontalOffset = diagramObject.right;

            }
            

            return create;
        }
    }
}