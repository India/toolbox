﻿using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using advntdg;
using Microsoft.Office.Interop.Excel;

namespace Advntdg.bucs
{
    internal class BucConsts
    {
        public const string SheetName = @"BUC Library";


        private static readonly BucConsts[] ConstVersions =
        {
            new BucConsts() // BEGIN / HTE
            {
                Id = "ID",
                ActorId = "Actor ID",
                Actor = "Actor",
                Title = "Title",
                Prio = "Priority",
                Description = "Description",
                Requirements = "Requirements",
                Frequency = "Frequency of use",
                DataInput = "Data Input",
                DataOutput = "Data Output",
                ReuseId = "Reuse ID",
                BucRusemeId = "BUC Reuse Me ID"
            },
            new BucConsts() // Rehau
            {
                Id = "ID",
                ActorId = "Actors ID",
                Actor = "Actors",
                Title = "BUC Name",
                Prio = "Priority",
                Description = "Description",
                Requirements = "Requirements",
                Frequency = "Frequency of use",
                DataInput = "Data Input",
                DataOutput = "Data Output",
                ReuseId = "ID",
                BucRusemeId = "ID"
            },
            new BucConsts()
            {
                Id = "ID",
                ActorId = "Actors ID",
                Actor = "Actors Name",
                Title = "BUC Name",
                Prio = "Priority",
                Description = "Description",
                Requirements = "Requirements",
                Frequency = "Frequency of use",
                DataInput = "Data Input",
                DataOutput = "Data Output",
                ReuseId = "ID",
                BucRusemeId = "ID"
            }
        };


        public static BucConsts Active { get; private set; }

        [AdvProp]
        public string Id { get; private set; }
        [AdvProp]
        public string ActorId { get; private set; }
        [AdvProp]
        public string Actor { get; private set; }
        [AdvProp]
        public string Title { get; private set; }
        [AdvProp]
        public string Prio { get; private set; }
        [AdvProp]
        public string Description { get; private set; }
        [AdvProp]
        public string Requirements { get; private set; }
        [AdvProp]
        public string Frequency { get; private set; }
        [AdvProp]
        public string DataInput { get; private set; }
        [AdvProp()]
        public string DataOutput { get; private set; }
        [AdvProp()]
        public string ReuseId { get; private set; }
        [AdvProp()]
        public string BucRusemeId { get; private set; }

        public static IReadOnlyList<string> ImportAsTaggedValue => new List<string>
        {
            Active.ActorId,
            Active.Actor,
            Active.Prio,
            Active.Requirements,
            Active.Frequency,
            Active.DataInput,
            Active.DataOutput,
            Active.ReuseId,
            Active.BucRusemeId
        };

        public static bool Eval(Workbook workbook)
        {
            Active = ExcelReader.SelectConsts(workbook, SheetName, ConstVersions);

            return Active != null;
        }

        public static int UsedConsts()
        {
            for (var i = 0; i < ConstVersions.Length; i++)
                if (ConstVersions[i] == Active)
                    return i;

            return -1;
        }

    }
}