﻿using System.Collections.Generic;
using EA;

namespace Advntdg.bucs
{
    /// <summary>
    /// Partial class for BUCs special diagram data, holding the BUC sequence as well as offset information
    /// for the Layout process
    /// </summary>
    internal partial class Buc
    {
        /// <summary>
        /// List of successors
        /// </summary>
        private List<Buc> successors = new List<Buc>();
        
        /// <summary>
        ///  Property to return array of successorts
        /// </summary>
        public Buc[] Successors => successors.ToArray();
        
        /// <summary>
        /// The horizontal offset in the main BUC-Diagram 
        /// </summary>
        public int HorizontalOffset { get; set; }
        /// <summary>
        /// The vertical offset in the main BUC Diagram
        /// </summary>
        public int VerticalOffset { get; set; }
        /// <summary>
        ///  The EA-DiagramObject in the main BUC-Diagram for this buc 
        /// </summary>
        public DiagramObject DiagramObject { get; set; }
        
        /// <summary>
        /// Adds a new successor to this BUC
        /// </summary>
        /// <param name="buc"></param>
        public void AddSuccessor(Buc buc)
        {
            successors.Add(buc); 
        }


    }
}