using Advntdg.actors;
using Advntdg.bucs;
using Advntdg.capabilities;
using Advntdg.relations;
using EA;

namespace Advntdg
{
    /// <summary>
    /// Structure to hold the import data
    /// </summary>
    internal class ImportData
    {
        /// <summary>
        /// Property to hold all Actors
        /// </summary>
        public Actor[] Actors { get; set; }
        /// <summary>
        /// Property to hold all BUCs
        /// </summary>
        public Buc[] Bucs { get; set; }
        /// <summary>
        /// Property to hold all Capabilities
        /// </summary>
        public Capability[] Capabilities { get; set; }
        /// <summary>
        /// Property to hold all Relaions
        /// </summary>
        public Relation[] Relations { get; set; }
    }
}