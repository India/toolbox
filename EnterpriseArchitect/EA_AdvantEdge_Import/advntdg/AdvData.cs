﻿using System;
using System.Collections.Generic;

namespace Advntdg.data
{
    internal abstract class AdvData
    {
        private readonly Dictionary<string, string> _data = new Dictionary<string, string>();


        public string ReuseId { get; protected set; }
        public EA.Element Element { get; set; }

        public string this[string key]
        {
            get
            {
                string result = null;
                if (_data.ContainsKey(key))
                    result = _data[key];
                else
                {
                    throw new ArgumentException();
                }
                return result;
            }
            set { _data[key] = value; }
        }
    }
}