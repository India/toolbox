﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Text;
using advntdg;
using Advntdg.bucs;
using Advntdg.data;

namespace Advntdg
{
    /// <summary>
    /// This class manages the import of data from an Excel Workbook and contains 
    /// general helper methods
    /// </summary>
    internal class ExcelReader
    {
        /// <summary>
        /// Generic method to read in a range and creates the given data objects
        /// </summary>
        /// <typeparam name="T">Type of dataobject to generate. It needs to be a subclass of AdvData and be creatable</typeparam>
        /// <param name="workbook">the Excel workbook</param>
        /// <param name="progress">Progress interface</param>
        /// <param name="sheetName">The name of the worksheet to import</param>
        /// <returns>An array of T</returns>
        public T[] Read<T>(Workbook workbook, IProgress<ProgressInfo> progress, string sheetName) where T : AdvData, new()
        {
            var result = new List<T>();
            Worksheet sheet = workbook.Sheets[sheetName];
            if (sheet != null)
            {
                var range = sheet.UsedRange;
                var headerMap = GetHeaders(range);
                var row = 1;
                while(true)
                {
                    ++row;
                    if (string.IsNullOrEmpty(GetValue(range, row, 1)))
                        break;
                    var data = new T();
                    foreach (var entry in headerMap)
                    {
                        data[entry.Value] = GetValue(range, row, entry.Key);
                    }
                    progress?.Report(new ProgressInfo(null, data.ToString(), false));

                    result.Add(data);
                }
                Marshal.ReleaseComObject(range);
                Marshal.ReleaseComObject(sheet);
            }
            return result.ToArray();
        }

        /// <summary>
        /// Helper method to reads in the sheets headers assuming they are defined in 
        /// row 1 of the range
        /// </summary>
        /// <param name="range">The excel worksheet (range)</param>
        /// <returns></returns>
        public static Dictionary<int, string> GetHeaders(Range range)
                 {
            var result = new Dictionary<int, string>();
            var column = 0;
            while(true)
            {
                ++column;
                var cell = range[1, column];
                var header = cell.Value2;
                Marshal.ReleaseComObject(cell);
                if (header is string && !string.IsNullOrEmpty(header))
                {
                    result[column] = header;
                }
                else
                {
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Helper method to get the value of a specific cell
        /// </summary>
        /// <param name="range">The excel worksheet range</param>
        /// <param name="row">Row in range (1-based)</param>
        /// <param name="col">Column in range (1-based)</param>
        /// <returns></returns>
        private static string GetValue(Range range, int row, int col)
        {
            var result = "";
            var cell = range[row, col];
            if (cell != null)
            {
                if (cell.Value2 != null)
                    result = cell.Value2.ToString();
                Marshal.ReleaseComObject(cell);
            }
            return result;
        }

        public static T SelectConsts<T>(Workbook workbook, string sheetName, T[] constVersions) where T : class
        {
            Worksheet sheet = workbook.Sheets[sheetName];
            if (sheet != null)
            {
                var range = sheet.UsedRange;
                var headerMap = ExcelReader.GetHeaders(range);
                for (var i = 0; i < constVersions.Length; i++)
                {
                    if (CheckVersion(constVersions[i], headerMap))
                        return constVersions[i];
                }
            }

            return null;

        }

        private static bool CheckVersion<T>(T constVersion, Dictionary<int, string> headerMap) where T: class
        {
            var properties = typeof(T).GetProperties().Where(
                prop => Attribute.IsDefined(prop, typeof(AdvPropAttribute)));
            foreach(var prop in properties)
            {
                string propName = prop.Name;
                Console.WriteLine("Checking header name " + propName);
                string value = prop.GetValue(constVersion) as string;
                if(value != null && headerMap.ContainsValue(value) == false)
                    return false;
            }

            return true;
        }

        public static void ReportHeaders(Workbook workbook, StringBuilder msg, string sheetName)
        {
            Worksheet sheet = workbook.Sheets[sheetName];
            if (sheet != null)
            {
                var range = sheet.UsedRange;
                var headerMap = ExcelReader.GetHeaders(range);
                foreach (var entry in headerMap.Values)
                {
                    msg.AppendLine("\t" + entry);
                }

                msg.AppendLine();
            }
        }
    }
}
