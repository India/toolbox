﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Advntdg
{
    /// <summary>
    /// Form part to provide the accessors for the controller
    /// </summary>
    public partial class ProgressForm : Form
    {
        public ProgressForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Property for the main text (job) shown above the ProgressBar
        /// </summary>
        public string MainJob
        {
            get { return _lblText.Text; }
            set { _lblText.Text = value; }
        }

        /// <summary>
        /// Property for the sub text (job) shown below the ProgressBar
        /// </summary>
        public string SubJob
        {
            get { return _lblEntity.Text; }
            set { _lblEntity.Text = value; }
        }

        /// <summary>
        /// Advances the progress bar one step
        /// </summary>
        public void PerformStep()
        {
            _pbProgressBar.PerformStep();
        }

        /// <summary>
        /// Property to hold the MAX value of the progressbar. Setting the MAX value will reset the progress bar
        /// </summary>
        public int Max
        {
            get { return _pbProgressBar.Maximum; }
            set
            {
                _pbProgressBar.Maximum = value;
                _pbProgressBar.Minimum = 1;
                _pbProgressBar.Value = 1;
                _pbProgressBar.Step = 1;
            }
        }
    }
}
