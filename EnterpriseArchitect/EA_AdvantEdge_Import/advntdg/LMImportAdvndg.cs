﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EA;
using Microsoft.Office.Core;

namespace Advntdg
{
    /// <summary>
    /// Main AddinClass (COM). Acts as the main interface to be called by EA
    /// </summary>
    public class LMImportAdvndg
    {
        // define menu constants
        private const string MenuHeader = "-&LMtec";
        private const string MenuImportSingle = "Import &Single AdvantEdge Excel File...";
        private const string MenuImportFolders = "Import AdvantEdge &Folder...";
        private const string MenuTest = "&TEST";
        
        ///
        /// Called Before EA starts to check Add-In Exists
        /// Nothing is done here.
        /// This operation needs to exists for the addin to work
        ///
        /// <param name="repository" />the EA repository
        /// a string
        public String EA_Connect(EA.Repository repository)
        {
            //No special processing required.
            return "a string";
        }

        ///
        /// Called when user Clicks Add-Ins Menu item from within EA.
        /// Populates the Menu with our desired selections.
        /// Location can be "TreeView" "MainMenu" or "Diagram".
        ///
        /// <param name="repository" />the repository
        /// <param name="location" />the location of the menu
        /// <param name="menuName" />the name of the menu
        ///
        public object EA_GetMenuItems(EA.Repository repository, string location, string menuName)
        {

            switch (menuName)
            {

                case "":
                    return MenuHeader;

                case MenuHeader:
                    string[] subMenus = { MenuImportSingle, MenuImportFolders, "-", MenuTest };
                    return subMenus;
                default:
                    break;
            }

            return "";
        }

        ///
        /// Called once Menu has been opened to see what menu items should active.
        ///
        /// <param name="repository" />the repository
        /// <param name="location" />the location of the menu
        /// <param name="menuName" />the name of the menu
        /// <param name="itemName" />the name of the menu item
        /// <param name="isEnabled" />boolean indicating whethe the menu item is enabled
        /// <param name="isChecked" />boolean indicating whether the menu is checked
        public void EA_GetMenuState(EA.Repository repository, string location, string menuName, string itemName, ref bool isEnabled, ref bool isChecked)
        {
            if (IsProjectOpen(repository))
            {
                switch (itemName)
                {
                    // define the state of the import menu option
                    case MenuImportSingle:
                    case MenuImportFolders:
                        isEnabled = IsPackageSelected(repository);
                        break;
                    // if defined above - enable it (only for testing purposes)
                    case MenuTest:
                        isEnabled = true;
                        break;
                    default:
                        isEnabled = false;
                        break;
                }
            }
            else
            {
                // If no open project, disable all menu options
                isEnabled = false;
            }
        }

        

        ///
        /// Called when user makes a selection in the menu.
        /// This is your main exit point to the rest of your Add-in
        ///
        /// <param name="repository" />the repository
        /// <param name="location" />the location of the menu
        /// <param name="menuName" />the name of the menu
        /// <param name="itemName" />the name of the selected menu item
        public void EA_MenuClick(EA.Repository repository, string location, string menuName, string itemName)
        {
            switch (itemName)
            {
                // user has clicked the menuHello menu option
                case MenuImportSingle:
                    AdvntgImportCommand.Execute(repository);
                    break;
                case MenuImportFolders:
                    AdvntgImportCommand.ExecuteMultiple(repository);
                    break;
                case MenuTest:
                    Connector cn = repository.GetCurrentDiagram().SelectedConnector as Connector;

                    var thisID = cn.ConnectorID;
                    var client = cn.ClientID;
                    var sup = cn.SupplierID;
                    var type = cn.Type;
                    var cname = cn.Name;
                    var stype = cn.Subtype;
                    var stereotype = cn.Stereotype;
                    
                    

                    var src = repository.GetElementByID(client);
                    var ot3 = src.ObjectType;
                    var srcElem = src as Element;

                    var name = srcElem.Name;
                    var st = srcElem.Stereotype;
                    var clsId1 = srcElem.ClassfierID;
                    var clsId2 = srcElem.ClassifierID;
                    var clsId3 = srcElem.ClassifierName;
                    var clsId4 = srcElem.ClassifierType;

                    var elemid = cn.AssociationClass.ElementID;
                    
                    
                    ObjectType ot1 = cn.ClientEnd.ObjectType;             
                    ObjectType ot2 = cn.SupplierEnd.ObjectType;

                    string e1 = cn.ClientEnd.End;
                    string e2 = cn.SupplierEnd.End;
                    var test = cn.Direction;
                    /*string result = Repository.SQLQuery(
                        "select t_object.Object_ID " +
                        "from t_object inner join t_objectproperties " +
                        "on t_object.Object_ID = t_objectproperties.Object_ID " +
                        "where t_objectproperties.Property=\"Reuse ID\" and t_objectproperty.Value=\"139\"");*/
                   /* var result = repository.SQLQuery(
                        "select t_objectproperties.Object_ID from t_objectproperties where t_objectproperties.Property=\"Reuse ID\" and t_objectproperties.Value=\"REQ-000106000563\"");
                    System.Console.Out.WriteLine(result);#1#
                    Project project = repository.GetProjectInterface();
                    Diagram diagram = repository.GetTreeSelectedObject() as Diagram;
                    if (diagram != null)
                    {
                        int layout = 0;
                        foreach (var diagramObject in diagram.DiagramObjects)
                        {
                            
                        }
                        project.LayoutDiagramEx(diagram.DiagramGUID, layout, 4, 20, 20, false);
                    }*/
                    /*Element element = repository.GetTreeSelectedObject() as Element;
                    if (element != null)
                    {
                        foreach (EA.Connector relation in element.Connectors)
                        {
                            var type1 = relation.GetType().FullName;
                            var type2 = relation.MetaType;
                            var type3 = relation.ObjectType.ToString();
                            var type4 = relation.Stereotype;
                            var type5 = relation.StereotypeEx;
                            var type6 = relation.FQStereotype;
                            var type7 = relation.Type;

                            System.Console.Out.WriteLine(type1);
                            System.Console.Out.WriteLine(type2);
                            System.Console.Out.WriteLine(type3);
                            System.Console.Out.WriteLine(type4);
                            System.Console.Out.WriteLine(type5);
                            System.Console.Out.WriteLine(type6);
                            System.Console.Out.WriteLine(type7);
                        }
                    }*/
                    Diagram diagram = repository.GetTreeSelectedObject() as Diagram;
                    if (diagram != null)
                    {
                        int layout = 0;
                        var swimLane = diagram.Swimlanes;
                        diagram.DiagramObjects.AddNew("DEMO", "SwimLane");
                        EA.SwimlaneDef def = diagram.SwimlaneDef;
                        string classifier = "";
                        foreach (EA.Swimlane lane in def.Swimlanes)
                        {
                            var x = lane.Title;
                            var y = lane.Width;
                            var z = lane.ClassifierGuid;
                            System.Console.Out.WriteLine(x + y+ z);
                            classifier = z;
                        }
                        EA.Swimlane newLane = def.Swimlanes.Add("MY ACTOR", 100);
                        newLane.ClassifierGuid = classifier;
                        diagram.Update();

                    }
                    break;
                default:
                    break;
            }
        }

        ///
        /// EA calls this operation when it exists. Can be used to do some cleanup work.
        ///
        public void EA_Disconnect()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        ///
        /// returns true if a project is currently opened
        ///
        /// <param name="repository" />the repository
        /// true if a project is opened in EA
        private static bool IsProjectOpen(Repository repository)
        {
            try
            {
                var c = repository.Models;
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true, if the current selected element in the repository is a Package
        /// </summary>
        /// <param name="repository">The EA Repository</param>
        /// <returns>True if a pacage is selected</returns>
        private bool IsPackageSelected(Repository repository)
        {
            object selected;
            repository.GetTreeSelectedItem(out selected);

            var package = selected as Package;
            return package != null;
        }

    }
}
