﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advntdg.actors;
using Advntdg.data;

namespace Advntdg.actors
{
    internal partial class Actor : AdvData    {
        private const int ACTWidth = 45;
        private const int ACTHeight = 90;

        public void Import(EA.Package actorPackage, EA.Diagram diagram, int pos)
        {
            EA.Element eaActor = this.Element;

            if (eaActor == null)
            {
                eaActor = actorPackage.Elements.AddNew(this[ActorConsts.Active.Name], "Actor");
            }
            EaImporter.SetTaggedValues(eaActor, this, ActorConsts.ImportAsTaggedValue);
            eaActor.Notes = this[ActorConsts.Active.Description];
            eaActor.Alias = this[ActorConsts.Active.Id];

            eaActor.Update();
            this.Element = eaActor;

            EaImporter.AddToDiagram(diagram, eaActor, pos, pos + ACTWidth, pos, pos + ACTHeight);
        }

        public static void ImportActors(Actor[] actors, EaImporter importer)
        {
            if (actors == null)
                return;
            importer.Progress?.Report(new ProgressInfo("Create Actors", actors.Length));

            // Locate package to import Actors to
            importer.Progress?.Report(new ProgressInfo(null, "Locate Package", false));
            var actorPackage = importer.CreateOrGetPackage("Actors");
            EaImporter.MapEA(actorPackage, actors, ActorConsts.Active.ActorReuseIdMe);
            EA.Diagram diagram = importer.CreateOrGetDiagram(actorPackage, "Actors", "Use Case");

            int pos = 20;
            foreach (var actor in actors)
            {
                importer.Progress?.Report(new ProgressInfo(null, actor[ActorConsts.Active.Name]));
                actor.Import(actorPackage, diagram, pos);
                pos += 20;
            }
            actorPackage.Update();
            diagram.Update();
            importer.AutoLayout(diagram);
        }
    }
}
