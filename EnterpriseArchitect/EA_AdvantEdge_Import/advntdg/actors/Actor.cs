﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advntdg.data;

namespace Advntdg.actors
{
    internal partial class Actor : AdvData
    {
        public override string ToString()
        {
            return this[ActorConsts.Active.Id] + " - " + this[ActorConsts.Active.Name];
        }

    }
}
