﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using advntdg;
using Microsoft.Office.Interop.Excel;

namespace Advntdg.actors
{
    internal class ActorConsts
    {
        public const string SheetName = @"Actors";

        private static readonly ActorConsts[] ConstVersions =
        {
            new ActorConsts() 
            {
                Id  = "ID",
                Name = "Actor",
                Description = "Role Description and Responsibilities",
                Comments = "Comments",
                ReuseId = "Reuse ID",
                ActorReuseIdMe = "Actor Reuse Me ID"

            },
            new ActorConsts() 
            {
                Id = "ID",
                Name = "Actor",
                Description = "Role Description and Responsibilities",
                Comments = "Comments",
                ReuseId = "ID",
                ActorReuseIdMe = "ID"
            }
        };

        
        public static ActorConsts Active { get; private set; }

        [AdvProp]
        public string Id { get; private set; } = "ID";
        [AdvProp]
        public string Name { get; private set; } = "Actor";
        [AdvProp]
        public string Description { get; private set; } = "Role Description and Responsibilities";
        [AdvProp]
        public string Comments { get; private set; } = "Comments";
        [AdvProp]
        public string ReuseId { get; private set; } = "Reuse ID";
        [AdvProp]
        public string ActorReuseIdMe { get; private set; } = "Actor Reuse Me ID";   
                
        public static IReadOnlyList<string> ImportAsTaggedValue => new List<string>
        {
            Active.Comments,
            Active.ReuseId,
            Active.ActorReuseIdMe,
        };
        
        public static bool Eval(Workbook workbook)
        {
            Active = ExcelReader.SelectConsts(workbook, SheetName, ConstVersions);

            return Active != null;
        }
        
        public static int UsedConsts()
        {
            for (var i = 0; i < ConstVersions.Length; i++)
                if (ConstVersions[i] == Active)
                    return i;

            return -1;
        }
    }
}
