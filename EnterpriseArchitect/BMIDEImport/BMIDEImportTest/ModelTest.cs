﻿using BMIDEImport.Model;
using BMIDEImport.Template;
using BMIDEImport.UI.FileHandler;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMIDEImportTest
{
    [TestClass]
    public class ModelTest
    {
        private Model Model { get; set; }

        public ModelTest()
        {
            var bmideData = BmideData.ReadBmidePackage(StringResources.TestModelFile, StringResources.TestTemplateDirectory);
            this.Model = new Model(bmideData);  
        }
        
       /* [TestMethod]
        public void CompareZipVsFile()
        {
            FileHandler
            var bmideDataZip = BmideData.ReadBmidePackage(StringResources.TestModelZipFile, StringResources.TestTemplateDirectory);
            var zipModel = new Model(bmideDataZip);
            Assert.AreEqual(zipModel.Data.Includes.Guid, Model.Data.Includes.Guid);
            //TODO: Further checks!
        }*/

        [TestMethod]
        public void ModelBaseContentTest()
        {
            Assert.IsNotNull(Model.Data);
            Assert.IsNotNull(Model.MainTemplate);
        }

        [TestMethod]
        public void ModelDataBaseTest()
        {
            Assert.IsNotNull(Model.Data.Dependencies); 
            Assert.IsNotNull(Model.Data.BusinessData); 
            Assert.IsNotNull(Model.Data.Includes); 
            Assert.IsNotNull(Model.Data.Localizations); 
            Assert.IsNotNull(Model.Data.MergedData); 
        }

        [TestMethod]
        public void ModelDataAmountsTest()
        {
            Assert.AreEqual(39, Model.Data.Localizations.Count); // foundation
            Assert.AreEqual(16, Model.Data.MergedData.Count); // foundation 
        }

        [TestMethod]
        public void ModelDataIncludesTest()
        {
            var inc = Model.Data.Includes;
            Assert.AreEqual("BMIDE EA Iimportdemo", inc.DisplayName);
            Assert.AreEqual("lm4importdemo", inc.Name);
            Assert.AreEqual("LM4", inc.Prefixes);
            Assert.AreEqual(1, inc.Includes.Count);
        }
        
        [TestMethod]
        public void ModelDataIncludesImportsTest()
        {
            Assert.AreEqual(1, Model.Data.Includes.Includes.Count);
            var depInclude = Model.Data.Includes.Includes[0];
            Assert.AreEqual("foundation_template.xml", depInclude.Include);
        }
        
        [TestMethod]
        public void ModelDataDependenciesAmountTest()
        {
            Assert.AreEqual(1, Model.Data.Dependencies.Count); // foundation 
        }
        
        [TestMethod]
        public void ModelDataDependenciesIncludeTest()
        {
            var inc = Model.Data.Dependencies[0].Includes;
            Assert.AreEqual("Foundation", inc.DisplayName);
            Assert.AreEqual("foundation", inc.Name);
            Assert.AreEqual("Fnd0", inc.Prefixes);
            Assert.AreEqual(0, inc.Includes.Count);
        }

    }
}