﻿using System.Threading.Tasks;
using BMIDEImport.Model;
using BMIDEImport.Template;
using EAInterface.EaInterface.MockEa;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMIDEImportTest
{
    [TestClass]
    public class EaMockTest
    {
        private BmideData Data { get; set; }
        private Model ImportModel { get; set; }

       
        public EaMockTest()
        {
            Data = BmideData.ReadBmidePackage(StringResources.TestModelFile, StringResources.TestTemplateDirectory);
            ImportModel = new Model(Data);
        }


        [TestMethod]
        public void LoadTest()
        {
            var rootPackage = new MockEaPackage("TEST");
            
            var task = Task.Run( async () => { await ImportModel.Align(rootPackage, null); });
            task.Wait();
            
            Assert.IsFalse(rootPackage.Dirty);
        }
        
    }
}