﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using BMIDEImport.EaInterface;
using BMIDEImport.Model;
using BMIDEImport.Template;
using EAInterface.EaInterface.MockEa;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMIDEImportTest
{
    [TestClass]
    public class Lm4DatamodelTest
    {
        private MockEaPackage _rootPackage;

        public Lm4DatamodelTest()
        {
            try
            {
                var data = BmideData.ReadBmidePackage(StringResources.TestModelMasterFile,
                    StringResources.TestTemplateDirectory);
                var importModel = new Model(data);
                _rootPackage = new MockEaPackage("TEST");

                var task = Task.Run(async () => { await importModel.Align(_rootPackage, null); });
                task.Wait();
            }
            catch
            {
            }
        }

        [TestMethod]
        public void CheckRootPackages()
        {
            Assert.IsNotNull(_rootPackage);
            string[] packages = {"foundation", "lm4importdemo"};

            foreach (var pkgName in packages)
            {
                Assert.IsNotNull(_rootPackage.Packages.FirstOrDefault(x => x.Name.Equals(pkgName)), "Could not locate package " + pkgName);
            }
        }

        [TestMethod]
        public void CheckExternalAttrAttachment()
        {
            var itemRevPackage = GetPackage(new string[] {"foundation"});
            Assert.IsNotNull(itemRevPackage);

            var itemRev = itemRevPackage.GetElementByName("ItemRevision");
            Assert.IsNotNull(itemRev, "could not locate ItemRevision");

            var lm4Attr = itemRev.Attributes.FirstOrDefault(x => x.Name.Equals("lm4extString1"));
            Assert.IsNotNull(lm4Attr, "lm4extString1 does not exist"); // check attachment to cots class

            Assert.AreEqual(lm4Attr.Alias, "ItemRev Ext"); // check localisation for cots template
        }

        private IEaPackage GetPackage(string[] path)
        {
            IEaPackage result = _rootPackage;
            string name = "";
            for (var i = 0; i < path.Length; i++)
            {
                name += "/" + path[i];
                result = result.Packages.FirstOrDefault(x => x.Name.Equals(path[i]));
                if (result == null)
                    throw new PathNotFoundException(name);
            }

            return result;
        }
    }

    internal class PathNotFoundException : Exception
    {
        public string Path { get; private set; }
        public PathNotFoundException(string path)
        {
            Path = path;
        }
    }
}