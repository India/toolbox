﻿using BMIDEImport.UI.FileHandler;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMIDEImportTest
{
    [TestClass]
    public class FileHandlerTest
    {
        [TestMethod]
        public void TestUnpacked()
        {
            IFile file = FileHandler.GetFile(StringResources.TestModelFile);
            Assert.IsNotNull(file);
            var importFilePath = file.ImportFilePath;
            Assert.IsNotNull(file.ImportFilePath);
            Assert.AreEqual(file.ImportFilePath, StringResources.TestModelFile);
        }
        
        [TestMethod]
        public void TestZIP()
        {
            IFile file = FileHandler.GetFile(StringResources.TestModelZipFile);
            Assert.IsNotNull(file);
            var importFilePath = file.ImportFilePath;
            Assert.IsNotNull(importFilePath);
            Assert.IsTrue(importFilePath.EndsWith("_template.xml"));
        }
       
    }
}