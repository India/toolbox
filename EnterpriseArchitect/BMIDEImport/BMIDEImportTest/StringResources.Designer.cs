﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BMIDEImportTest {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class StringResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal StringResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("BMIDEImportTest.StringResources", typeof(StringResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ..\..\..\lm4importdemo\output\unzipped\lm4importdemo_template.xml.
        /// </summary>
        internal static string TestModelFile {
            get {
                return ResourceManager.GetString("TestModelFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ..\..\..\lm4importdemo\extensions\master.xml.
        /// </summary>
        internal static string TestModelMasterFile {
            get {
                return ResourceManager.GetString("TestModelMasterFile", resourceCulture);
            }
        }
               
        /// <summary>
        ///   Looks up a localized string similar to ..\..\..\lm4importdemo\extensions\default.xml.
        /// </summary>
        internal static string TestModelExtensionFile {
            get {
                return ResourceManager.GetString("TestModelExtensionFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ..\..\..\lm4importdemo\output\wntx64\packaging\full_update\lm4importdemo_template.zip.
        /// </summary>
        internal static string TestModelZipFile {
            get {
                return ResourceManager.GetString("TestModelZipFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to C:\Siemens\Teamcenter11\bmide\templates\.
        /// </summary>
        internal static string TestTemplateDirectory {
            get {
                return ResourceManager.GetString("TestTemplateDirectory", resourceCulture);
            }
        }
    }
}
