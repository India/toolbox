﻿namespace BMIDEImport
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows TcForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._pbProgressBar = new System.Windows.Forms.ProgressBar();
            this._lblText = new System.Windows.Forms.Label();
            this._lblEntity = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _pbProgressBar
            // 
            this._pbProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._pbProgressBar.Location = new System.Drawing.Point(12, 33);
            this._pbProgressBar.Name = "_pbProgressBar";
            this._pbProgressBar.Size = new System.Drawing.Size(419, 23);
            this._pbProgressBar.TabIndex = 0;
            this._pbProgressBar.UseWaitCursor = true;
            // 
            // _lblText
            // 
            this._lblText.AutoSize = true;
            this._lblText.Location = new System.Drawing.Point(12, 9);
            this._lblText.Name = "_lblText";
            this._lblText.Size = new System.Drawing.Size(0, 17);
            this._lblText.TabIndex = 1;
            this._lblText.UseWaitCursor = true;
            // 
            // _lblEntity
            // 
            this._lblEntity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._lblEntity.AutoSize = true;
            this._lblEntity.Location = new System.Drawing.Point(12, 59);
            this._lblEntity.Name = "_lblEntity";
            this._lblEntity.Size = new System.Drawing.Size(0, 17);
            this._lblEntity.TabIndex = 2;
            this._lblEntity.UseWaitCursor = true;
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 85);
            this.Controls.Add(this._lblEntity);
            this.Controls.Add(this._lblText);
            this.Controls.Add(this._pbProgressBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgressForm";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.UseWaitCursor = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar _pbProgressBar;
        private System.Windows.Forms.Label _lblText;
        private System.Windows.Forms.Label _lblEntity;



    }
}