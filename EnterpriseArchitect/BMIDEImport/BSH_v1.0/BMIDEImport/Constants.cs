﻿namespace BMIDEImport
{
    internal static class Constants
    {
        public const string PROP_string = "String";
        public const string PROP_date = "Date";
        public const string PROP_int = "Int";
        public const string PROP_logical = "Boolean";
        public const string POM_string = "String";
        public const string POM_int = "Int";
        public const string POM_date = "Date";
        public const string POM_logical = "Boolean";
        public const string POM_double = "Double";
    }
}