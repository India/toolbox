﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using BMIDEImport.Template.Dependencies;
using BMIDEImport.Template.Model;

namespace BMIDEImport.Template
{
    public static class ModelReader
    {
        
        public static TcBusinessData Parse(string filename)
        {
            TcBusinessData result = null;
            
            var fileonly = filename.Remove(0, filename.LastIndexOf(Path.DirectorySeparatorChar)+1);
            var path = filename.Remove(filename.LastIndexOf(Path.DirectorySeparatorChar));
            
            var filesToRead = new List<string>();
            if (fileonly.Equals("master.xml"))
            {
                var includes = (TcBusinessDataIncludes) TcBusinessDataIncludes.Parse(filename);

                if (includes != null)
                {
                    foreach (var include in includes.Includes)
                    {
                        var datafile = path + Path.DirectorySeparatorChar + include.Include;
                        filesToRead.Add(datafile);
                    }
                }
            }
            else
            {
                filesToRead.Add(filename);
            }

            foreach (var fileToRead in filesToRead)
            {
                var serializer = new XmlSerializer(typeof(TcBusinessData), "http://teamcenter.com/BusinessModel/TcBusinessData");

                // A FileStream is needed to read the XML document.
                var fs = new FileStream(fileToRead, FileMode.Open, FileAccess.Read);
                var reader = XmlReader.Create(fs);


                // Use the Deserialize method to restore the object's state.
                var partResult = (TcBusinessData) serializer.Deserialize(reader);
                
                var sourceFile = fileToRead.Remove(0, fileToRead.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                sourceFile = sourceFile.Replace(".xml", "");
                partResult.SetSource(sourceFile);
                
                if (result == null)
                    result = partResult;
                else
                    result.Merge(partResult);

                fs.Close();
                reader.Close();
            }

            return result;
        }
        
        public static Dictionary<string, MergedBusinessDefintions> Merge(BmideData data, Dictionary<string, BmideData> packages)
        {
            var mbd = new Dictionary<string, MergedBusinessDefintions>();
            var bd = data.BusinessData;
            
            foreach (var t in bd.Definitions.StandardTypes)
            {
                MergedBusinessDefintions m = CreateMbdFromType(t, data);
                
                if(m != null)
                    mbd.Add(m.className, m);
            }
            foreach (var t in bd.Definitions.RuntimeTypes)
            {
                MergedBusinessDefintions m = CreateMbdFromType(t, data);

                if (m != null)
                {
                    mbd.Add(m.className, m);
                }
            }
            
            foreach (var t in bd.Definitions.Classes)
            {
                if (t.Name.Contains("MasterS") == false)
                {
                    MergedBusinessDefintions results = mbd.ContainsKey(t.Name) ? mbd[t.Name] : null;
                    if(results != null)
                    {
                        if (results.classDescription == "")
                            results.classDescription = t.Description;
                       
                        var attrs = MapAttributes(t.Attributes);
                        MergeAttributes(attrs, results.Attributes);
                    }
                }
            }
            foreach(var t in bd.Definitions.Forms)
            {
                MergedBusinessDefintions tempData = new MergedBusinessDefintions(data)
                {
                    className = t.Name,
                    classDescription = t.Description,
                    parentClassName = t.ParentTypeName,
                    Attributes = new List<BusinessAttribute>(),
                    Abstract = t.isAbstract
                };
                
                foreach (var prop in t.Properties)
                {
                    BusinessAttribute tempBusinessAttr = new BusinessAttribute
                    {
                        Name = prop.Name,
                        Type = prop.ValueType,
                        InitialValue = "",
                        Description = prop.Description,
                        IsArray = prop.IsArray,
                        ProperyType = prop.TypeName
                    };

                    tempData.Attributes.Add(tempBusinessAttr);
                }
                if (t.formStorageClassName != null)
                {
                    TcClass storage = bd.Definitions.Classes.Find(x => x.Name.Equals(t.formStorageClassName));
                    if (storage != null)
                    {
                        foreach (var attr in storage.Attributes)
                        {
                            BusinessAttribute tempBusinessAttr = new BusinessAttribute
                            {
                                Name = attr.Name,
                                Type = attr.Type,
                                MaxStringValue = attr.MaxStringValue,
                                LowerBound = attr.LowerBound,
                                UpperBound = attr.UpperBound,
                                InitialValue = "",
                                Description = attr.Description,
                                IsArray = attr.IsArray,
                                ProperyType = "Runtime"
                            };
                            tempData.Attributes.Add(tempBusinessAttr);
                        }
                    }
                    tempData.StorageClassName = t.formStorageClassName;
                }
                mbd.Add(tempData.className, tempData);
            }
            foreach(var t in bd.Definitions.Datasets)
            {
                MergedBusinessDefintions tempData = new MergedBusinessDefintions(data)
                {
                    className = t.Name,
                    classDescription = t.Description,
                    parentClassName = t.ParentTypeName,
                    Abstract = t.isAbstract
                };
                foreach (var prop in t.Properties)
                {
                    BusinessAttribute tempBusinessAttr = new BusinessAttribute
                    {
                        Name = prop.Name,
                        Type = prop.ValueType,
                        InitialValue = "",
                        Description = prop.Description,
                        IsArray = prop.IsArray
                    };
                    tempData.Attributes.Add(tempBusinessAttr);
                }

                mbd.Add(tempData.className, tempData);
            }
            
            foreach (var att in bd.Definitions.CompoundRules)
            {
                var targetType = FindOrExternal(mbd, data, packages, att.desttypeName);
                var sourceType = FindOrExternal(mbd, data, packages, att.sourcetypeName);
                
                if (targetType != null && sourceType != null)
                {
                    var sourceAttr = sourceType.Attributes.Find(x => x.Name.Equals(att.sourcepropertyName));
                    if (sourceAttr != null)
                    {
                        BusinessAttribute tempBusinessAttr = new BusinessAttribute();
                        tempBusinessAttr.Name = att.destpropertyName;
                        tempBusinessAttr.Type = sourceAttr.Type;
                        tempBusinessAttr.MaxStringValue = sourceAttr.MaxStringValue;
                        tempBusinessAttr.LowerBound = sourceAttr.LowerBound;
                        tempBusinessAttr.UpperBound = sourceAttr.UpperBound;
                            
                        tempBusinessAttr.InitialValue = "";
                        tempBusinessAttr.Description =
                            "Compound Property " + att.sourcetypeName + "." + att.sourcepropertyName;
                        tempBusinessAttr.ProperyType = "Compound";

                        targetType.Attributes.Add(tempBusinessAttr);
                    }
                }
            }

            foreach (var attaches in bd.Definitions.AttributeAttaches)
            {
                var targetType = FindOrExternal(mbd, data, packages, attaches.ClassName);
    
                if (targetType != null)
                {
                    var mappedAttributes = MapAttributes(attaches.Attributes);
                    foreach (var att in mappedAttributes)
                    {
                        var localizationKey = "Property{::}" + targetType.className + "{::}" + att.Name;
                        if (data.Localizations.ContainsKey(localizationKey))
                            targetType.Container.Localizations[localizationKey] = data.Localizations[localizationKey];

                        var current = targetType.Attributes.Find(x => x.Name.Equals(att.Name));
                        if (current == null)
                        {
                            targetType.Attributes.Add(att);
                        }
                        else
                        {
                            targetType.Attributes.Remove(att);
                            targetType.Attributes.Add(att);
                        }
                    }
                }
            }

            if (data.IsPartialImport)
            {

                // ensures GRM types to be there at least as "externals"

                var copy = new Dictionary<string, MergedBusinessDefintions>(mbd);
                foreach (var obj in copy)
                {
                    if (string.IsNullOrEmpty(obj.Value.parentClassName) == false)
                        FindOrExternal(mbd, data, packages, obj.Value.parentClassName);

                    foreach (var attr in obj.Value.Attributes)
                        if (string.IsNullOrEmpty(attr.TypedRefClassName) == false)
                            FindOrExternal(mbd, data, packages, attr.TypedRefClassName);
                }
                
                foreach (var grm in bd.Definitions.GrmInfo)
                {
                    FindOrExternal(mbd, data, packages, grm.GRMprimaryTypeName);
                    FindOrExternal(mbd, data, packages, grm.GRMsecondaryTypeName);
                    FindOrExternal(mbd, data, packages, grm.GRMrelationTypeName);
                }

            }

            return mbd;
        }

        private static MergedBusinessDefintions CreateMbdFromType(TcStandardType t, BmideData data)
        {
            MergedBusinessDefintions tempData = null;
            
            if (t.Name.Contains("MasterS") != false) 
                return tempData;

            tempData = new MergedBusinessDefintions(data)
            {
                className = t.Name,
                classDescription = t.Description,
                parentClassName = t.ParentTypeName,
                Attributes = new List<BusinessAttribute>(),
                Abstract = t.isAbstract
            };
            
            foreach (var prop in t.Properties)
            {
                BusinessAttribute tempBusinessAttr = CreateBusinessAttribute(prop);

                tempData.Attributes.Add(tempBusinessAttr);
            }

            return tempData;
        }

        public static BusinessAttribute CreateBusinessAttribute(TcProperty property)
        {
            var tempBusinessAttr = new BusinessAttribute();
            tempBusinessAttr.Name = property.Name;
            tempBusinessAttr.Type = property.ValueType;
            tempBusinessAttr.InitialValue = "";
            tempBusinessAttr.Description = property.Description;
            tempBusinessAttr.ProperyType = property.TypeName;
            tempBusinessAttr.MaxStringValue = property.MaxStringLength;
            return tempBusinessAttr;
        }

        private static MergedBusinessDefintions FindOrExternal(Dictionary<string, MergedBusinessDefintions> mbd, 
            BmideData data,
            Dictionary<string, BmideData> packages, 
            string typeName)
        {
            if (string.IsNullOrEmpty(typeName))
                return null;
            
            if (mbd.ContainsKey(typeName))
                return mbd[typeName];

            foreach (var bmideData in packages.Values)
            {
                if (data == bmideData || bmideData.MergedData == null)
                    continue;

                if (bmideData.MergedData.ContainsKey(typeName))
                    return bmideData.MergedData[typeName];
            }

            return CreateExternalClass(mbd, data, typeName);
        }

        private static MergedBusinessDefintions CreateExternalClass(Dictionary<string, MergedBusinessDefintions> mbd, BmideData data, string typeName)
        {
            var result = new MergedBusinessDefintions(data)    
                {
                    className = typeName,
                    IsExternal = true
                };
            
            mbd.Add(typeName, result);
            return result;
        }

        private static void MergeAttributes(IEnumerable<BusinessAttribute> from, List<BusinessAttribute> to)
        {
            foreach (var fromAttr in from)
            {
                var found = to.Find(x => x.Name.Equals(fromAttr.Name));
                if(found == null)
                    to.Add(fromAttr);
            }
        }


        private static List<BusinessAttribute> MapAttributes(List<TcAttribute> attrs) {
            var result = new List<BusinessAttribute>();
            foreach (var prop in attrs)
            {
                BusinessAttribute tempBusinessAttr = new BusinessAttribute();
                tempBusinessAttr.Name = prop.Name;
                tempBusinessAttr.Type = prop.Type;
                tempBusinessAttr.InitialValue = prop.InitialValue;
                tempBusinessAttr.Description = prop.Description;
                tempBusinessAttr.MaxStringValue = prop.MaxStringValue;
                tempBusinessAttr.LowerBound = prop.LowerBound;
                tempBusinessAttr.UpperBound = prop.UpperBound;
                tempBusinessAttr.IsArray = prop.IsArray;
                tempBusinessAttr.TypedRefClassName = prop.TypedRefClassName;                    
                    
                result.Add(tempBusinessAttr);
            }
            return result;
        }
    }
}