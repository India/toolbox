﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using BMIDEImport.Template.Localization;

namespace BMIDEImport.Template.Dependencies
{
    [XmlRoot("TcBusinessDataIncludes")]
    public class TcBusinessDataIncludes
    {

        [XmlElement("include")]
        public List<TcInclude> Includes;

        [XmlAttribute("ActiveRelease")]
        public string ActiveRelease;

        [XmlAttribute("currentTemplateVersion")]
        public string CurrentTemplateVersion;

        [XmlAttribute("displayName")]
        public string DisplayName;

        [XmlAttribute("enableOpsDataDeploy")]
        public bool EnableOpsDataDeploy;
            
        [XmlAttribute("guid")]
        public string Guid;
        
        [XmlAttribute("isDescriptionMandatory")]
        public bool IsDescriptionMandatory;
        
        [XmlAttribute("name")]
        public string Name;
        
        [XmlAttribute("optional")]
        public bool Optional;
        
        [XmlAttribute("prefixes")]
        public string Prefixes;
        
        [XmlAttribute("teamcenterTemplate")]
        public bool TeamcenterTemplate;
        
        public static TcBusinessDataIncludes Parse(string filename)
        {
            var serializer = new XmlSerializer(typeof(TcBusinessDataIncludes));

            // A FileStream is needed to read the XML document.
            var fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            var reader = XmlReader.Create(fs);


            // Use the Deserialize method to restore the object's state.
            TcBusinessDataIncludes result = (TcBusinessDataIncludes)serializer.Deserialize(reader);
            fs.Close();
            reader.Close();
            return result;
        }
    }

}