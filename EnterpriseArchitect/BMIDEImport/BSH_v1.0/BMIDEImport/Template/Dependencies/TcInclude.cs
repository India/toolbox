﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Dependencies
{
    public class TcInclude
    {
        [XmlAttribute("file")]
        public string Include;
    }
}