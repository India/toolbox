﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using BMIDEImport.Model;
using BMIDEImport.Template.Dependencies;
using BMIDEImport.Template.Localization;
using BMIDEImport.Template.Model;

namespace BMIDEImport.Template
{
    /// <summary>
    /// Container for BMIDE information
    /// </summary>
    public class BmideData
    {

        // private static string BMIDE_TEMPLATES = @"c:\Siemens\Teamcenter11\bmide\templates\";
        
        /// <summary>
        /// Get raw bmide data
        /// </summary>
        public TcBusinessData BusinessData { get; private set; }
        /// <summary>
        /// Get merged bmide data
        /// </summary>
        public Dictionary<string, MergedBusinessDefintions> MergedData { get; private set; }


        private Dictionary<string, bool> ImportedAs { get; set; }
        public Dictionary<string, TcLocalizationKey> Localizations { get; } = new Dictionary<string, TcLocalizationKey>();
        public ModelDeepCopyRules DeepCopyRules { get; private set; }
        public List<BmideData> Dependencies { get; private set; }
        public TcBusinessDataIncludes Includes { get; private set; }


        public string Path { get; private set; }
        public bool IsPartialImport { get; private set; }

        private BmideData(bool isPartial)
        {
            ImportedAs = new Dictionary<string, bool>();
            IsPartialImport = isPartial;
        }
        
        /// <summary>
        /// Method to read in a BMIDE package and prepare data
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static BmideData ReadBmidePackage(string fileName, string templatePath)
        {
            Dictionary<string, BmideData> packages = new Dictionary<string, BmideData>();
            string rootGuid = ReadPackage(fileName, templatePath, packages);
            //result.MergedData = ModelReader.Merge(result.BusinessData);
            return packages[rootGuid];
        }

        private static string ReadPackage(string fileName, string templatePath, Dictionary<string, BmideData> packages)
        {
            var result = "";
            var modelFile = fileName;
            var isPartial = false;

            if (fileName.EndsWith(".xml") && !(fileName.EndsWith("_template.xml") || fileName.EndsWith("master.xml")))
                isPartial = true;
            
            var dependencyFile = GetDependencyFile(fileName);
            var localizationFile = GetLocalizationFile(fileName);
 
            TcBusinessDataIncludes includes = TcBusinessDataIncludes.Parse(dependencyFile);

            if (packages.ContainsKey(includes.Guid))
                result = includes.Guid;
            else
            {
                result = includes.Guid;
                packages[includes.Guid] = new BmideData(isPartial);
                
                var dependencies = new List<BmideData>();

                foreach (var depPkg in includes.Includes)
                {
                    string templateFile = templatePath + depPkg.Include;
                    try
                    {
                        string guid = ReadPackage(templateFile, templatePath, packages);
                        dependencies.Add(packages[guid]);
                    }
                    catch(Exception e)
                    {
                        MessageBox.Show("Unable to read dependent package <" + depPkg.Include + ">\nfrom " + templatePath + "\n\n" + e.Message + "\n\n" + e.Source + "\n\n" + e.StackTrace);
                    }
                }
                packages[includes.Guid].Includes = includes;
                packages[includes.Guid].Dependencies = dependencies;
                packages[includes.Guid].BusinessData = ModelReader.Parse(fileName);
                packages[includes.Guid].PrepareLocalizations(localizationFile);
                packages[includes.Guid].MergedData = ModelReader.Merge(packages[includes.Guid], packages);
                packages[includes.Guid].BuildParents();
                packages[includes.Guid].EvaluateDeepCopyRules();
                packages[includes.Guid].EvaluateExternalPropertyAttachments(dependencies);
                packages[includes.Guid].Path = fileName.Remove(fileName.LastIndexOf(System.IO.Path.DirectorySeparatorChar));
                packages[includes.Guid].FileName = fileName;

            }
            return result;
        }

        public string FileName { get; private set; }

        private void EvaluateExternalPropertyAttachments(List<BmideData> dependencies)
        {
            foreach (var propAttachment in this.BusinessData.Definitions.PropertyAttaches)
            {
                var cls = LocateType(propAttachment.TypeName);
                if(cls != null) // can happen for types we are not interested in (i.e. *CreI)
                    foreach(var prop in propAttachment.Properties)
                        cls.Attributes.Add(ModelReader.CreateBusinessAttribute(prop));
            } 
                
        }

        private void EvaluateDeepCopyRules()
        {
            if (DeepCopyRules == null)
            {
                DeepCopyRules = new ModelDeepCopyRules(BusinessData.Definitions.DeepCopyRules);
                foreach (var dep in Dependencies)
                {
                    dep.EvaluateDeepCopyRules();
                    DeepCopyRules.Merge(dep.DeepCopyRules);
                }
            }
        }

        private static string GetLocalizationFile(string fileName)
        {
            string result;
            var fileOnly = fileName.Remove(0, fileName.LastIndexOf(System.IO.Path.DirectorySeparatorChar)+1);

            if (fileOnly.Contains("_template."))
                result = fileName.Remove(fileName.LastIndexOf(System.IO.Path.DirectorySeparatorChar))
                         + @"\lang"
                         + fileName.Remove(0, fileName.LastIndexOf(System.IO.Path.DirectorySeparatorChar))
                             .Replace(".xml", "_en_US.xml");
            else
                result = fileName.Remove(fileName.LastIndexOf(System.IO.Path.DirectorySeparatorChar))
                      + @"\lang\en_US\";

            return result;
        }

        private static string GetDependencyFile(string fileName)
        {
            string result;
            var fileOnly = fileName.Remove(0, fileName.LastIndexOf(System.IO.Path.DirectorySeparatorChar)+1);

            if (fileOnly.Contains("_template."))
                result = fileName.Replace("_template.", "_dependency.");
            else
                result = fileName.Replace(fileOnly, "dependency.xml");

            return result;
        }

        private void BuildParents()
        {
            foreach (var data in MergedData)
            {
                data.Value.Parent = string.IsNullOrEmpty(data.Value.parentClassName) ? 
                    null: 
                    LocateType(data.Value.parentClassName);
                
                if (String.IsNullOrEmpty(data.Value.StorageClassName) == false)
                    data.Value.Storage = LocateType(data.Value.StorageClassName);
            }
        }

        private void PrepareLocalizations(string localizationPath)
        {
            TcBusinessDataLocalization localizations = null;
            if (localizationPath.EndsWith(".xml"))
                localizations = TcBusinessDataLocalization.Parse(localizationPath);
            else
            {
                var files = Directory.GetFiles(localizationPath, "*.xml");
                foreach (var file in files)
                {
                    var result = TcBusinessDataLocalization.Parse(file);
                    if (localizations == null)
                        localizations = result;
                    else
                        localizations.Merge(result);
                }
            }

            if (localizations != null && localizations.tcLocalizations != null &&
                localizations.tcLocalizations.Localizations != null)
            {
                foreach (var localization in localizations.tcLocalizations.Localizations)
                {
                    this.Localizations[localization.ID] = localization;
                }
            }
        }
        
        public MergedBusinessDefintions LocateType(string typeName)
        {
            MergedBusinessDefintions result = MergedData.ContainsKey(typeName) ? MergedData[typeName] : null;
    
            if (result == null)
            {
                foreach (var dep in Dependencies)
                {
                    result = dep.LocateType(typeName);

                    if (result != null)
                        break;
                }
            }

            return result;
        }

        public bool CheckImported(string key)
        {
            if (ImportedAs.ContainsKey(key) == false)
                ImportedAs[key] = false; 
            
            if (ImportedAs[key])
                return true;
            else
                ImportedAs[key] = true;
            return false;
        }
    }
}