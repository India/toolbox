﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Localization
{
    [XmlRoot("TcBusinessDataLocalization")]
    public class TcBusinessDataLocalization
    {
        [XmlElement("Add")]
        public TcLocalizations tcLocalizations;
        
        public static TcBusinessDataLocalization Parse(string filename)
        {
            var serializer = new XmlSerializer(typeof(TcBusinessDataLocalization), "http://teamcenter.com/BusinessModel/TcBusinessDataLocalization");

            // A FileStream is needed to read the XML document.
            var fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            var reader = XmlReader.Create(fs);

            // Declare an object variable of the type to be deserialized.
            TcBusinessDataLocalization result;

            // Use the Deserialize method to restore the object's state.
            result = (TcBusinessDataLocalization)serializer.Deserialize(reader);
            fs.Close();
            reader.Close();
            return result;
        }


        public void Merge(TcBusinessDataLocalization local)
        {
            if (tcLocalizations == null)
                tcLocalizations = local.tcLocalizations;
            else
                if(local != null)
                    tcLocalizations.Merge(local.tcLocalizations);
        }
    }
}