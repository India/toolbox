﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Localization
{
    public class TcLocalizations
    {
        [XmlElement("key")]
        public List<TcLocalizationKey> Localizations;

        public void Merge(TcLocalizations localizations)
        {
            if (Localizations == null)
                Localizations = localizations.Localizations;
            else
                if(localizations != null && localizations.Localizations != null)
                    Localizations.AddRange(localizations.Localizations);
        }
    }
}