using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcDeepCopyRule
    {
        [XmlAttribute("typeName")]
        public string Type;
        
        [XmlAttribute("relRefPropName")]
        public string PropertyName;
        
        [XmlAttribute("propertyType")]
        public string PropertyType;
 
        [XmlAttribute("objectTypeName")]
        public string ObjectTypeName;

        [XmlAttribute("conditionName")] 
        public string ConditionName;

        /* "ITEM_create_from_rev" or "ITEM_copy_rev" */
        [XmlAttribute("operationName")] 
        public string OperationName;

        [XmlAttribute("copyType")] 
        public string CopyType;
        
        [XmlAttribute("isTargetPrimary")] 
        public bool IsTargetPrimary;
        
        [XmlAttribute("secured")] 
        public bool Secured;

        [XmlAttribute("copyRelationAttributes")] 
        public bool CopyRelationAttributes;

        
    }
}