﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcGRMInfo
    {
        [XmlAttribute("attachability")]
        public string GRMattachability;

        [XmlAttribute("changeability")]
        public string GRMchangeability;

        [XmlAttribute("description")]
        public string GRMdescription;

        [XmlAttribute("detachability")]
        public string GRMdetachability;

        [XmlAttribute("primaryCardinality")]
        public string GRMprimaryCardinality;

        [XmlAttribute("primaryTypeName")]
        public string GRMprimaryTypeName;

        [XmlAttribute("relationTypeName")]
        public string GRMrelationTypeName;

        [XmlAttribute("secondaryCardinality")]
        public string GRMsecondaryCardinality;

        [XmlAttribute("secondaryTypeName")]
        public string GRMsecondaryTypeName;

    }
}