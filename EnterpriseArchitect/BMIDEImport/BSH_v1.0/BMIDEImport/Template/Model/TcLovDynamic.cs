using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcLovDynamic
    {
        
 /*
  * <LOVDynamic name="LM4DynLov"
  * description=""
  * usage="Exhaustive"
  * queryClause="SELECT qid FROM LM4Item1_1Revision WHERE &quot;object_name&quot; LIKE &quot;LM*&quot;"
  * dataType="PROP_string"
  * queryType="LM4Item1_1Revision"
  * lovValueAttribute="lm4extString1"
  * lovValueDescriptionAttribute=""/>
  */
        
    [XmlAttribute("description")]
    public string lovdescription;

    [XmlAttribute("dataType")]
    public string lovType;

    [XmlAttribute("name")]
    public string lovName;

    [XmlAttribute("usage")]
    public string lovUsage;

    [XmlAttribute("queryClause")]
    public string queryClause;
    
    [XmlAttribute("lovValueAttribute")]
    public string valueAttribute;
 

    public string Source { get; set; }

    }
}