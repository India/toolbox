﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcForm
    {
        [XmlAttribute("typeName")]
        public string Name;

        [XmlAttribute("parentTypeName")]
        public string ParentTypeName;

        [XmlAttribute("description")]
        public string Description;

        [XmlElement("artifactName")]
        public string artifactName;

        [XmlAttribute("formStorageClassName")]
        public string formStorageClassName;

        [XmlAttribute("isAbstract")]
        public bool isAbstract;

        [XmlElement("TcProperty")]
        public List<TcProperty> Properties;

        public string Source { get; set; }
    }
}