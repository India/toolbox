using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcSubLov
    {

        [XmlAttribute("targetLOVName")] 
        public string BaseLov;

        [XmlAttribute("targetValue")] 
        public string Value;
        
        [XmlAttribute("subLOVName")]
        public string SubLov;

        [XmlAttribute("conditionName")] 
        public string Condition;
    }
}