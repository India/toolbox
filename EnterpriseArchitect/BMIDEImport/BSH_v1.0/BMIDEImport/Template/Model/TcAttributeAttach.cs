﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcAttributeAttach
    {
        [XmlAttribute("className")] 
        public string ClassName;
        
        [XmlElement("TcAttribute")]
        public List<TcAttribute> Attributes;
    }
}
