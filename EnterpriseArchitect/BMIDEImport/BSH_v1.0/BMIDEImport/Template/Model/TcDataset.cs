﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{

    public class TcDataset
    {
        [XmlAttribute("typeName")]
        public string Name;

        [XmlAttribute("parentTypeName")]
        public string ParentTypeName;

        [XmlAttribute("description")]
        public string Description;

        [XmlAttribute("isAbstract")]
        public bool isAbstract;
        
        [XmlAttribute("artifactName")]
        public string artifactName;

        [XmlElement("TcDSViewTool")]
        public List<TcTool> ViewTool;
        
        [XmlElement("TcDSEditTool")]
        public List<TcTool> EditTool;
        
        [XmlElement("TcProperty")]
        public List<TcProperty> Properties;

        public string Source { get; set; }
    }
}