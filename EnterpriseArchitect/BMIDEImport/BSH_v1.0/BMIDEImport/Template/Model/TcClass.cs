﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcClass
    {
        [XmlAttribute("className")]
        public string Name;

        [XmlAttribute("description")]
        public string Description;

        [XmlAttribute("parentClassName")]
        public string ParentClassName;

        [XmlElement("TcAttribute")]
        public List<TcAttribute> Attributes;

        public string Source { get; set; }
    }
}