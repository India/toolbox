﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcBusinessDefinitions
    {
        [XmlElement("TcClass")]
        public List<TcClass> Classes;

        [XmlElement("TcStandardType")]
        public List<TcStandardType> StandardTypes;
        
        [XmlElement("TcRuntimeType")]
        public List<TcStandardType> RuntimeTypes;

        [XmlElement("TcUnitOfMeasure")]
        public List<TcUnitOfMeasure> UnitsOfMeasure;

        [XmlElement("TcCompoundPropertyRule")]
        public List<TcCompoundPropertyRule> CompoundRules;

        [XmlElement("TcLOV")]
        public List<TcLOV> ListofValues;

        [XmlElement("LOVDynamic")] 
        public List<TcLovDynamic> DynamicListOfValues;

        [XmlElement("TcLOVAttach")]
        public List<TcLOVAttach> TcLovAttach;

        [XmlElement("TcGRMRule")]
        public List<TcGRMInfo> GrmInfo;

        [XmlElement("TcForm")]
        public List<TcForm> Forms;
        
        [XmlElement("TcDataset")]
        public List<TcDataset> Datasets;

        [XmlElement("TcAttributeAttach")] 
        public List<TcAttributeAttach> AttributeAttaches;

        [XmlElement("TcDeepCopyRule")] 
        public List<TcDeepCopyRule> DeepCopyRules;
        
        [XmlElement("TcPropertyAttach")] 
        public List<TcPropertyAttach> PropertyAttaches;
        
        [XmlElement("TcLOVValueSubLOVAttach")] 
        public List<TcSubLov> SubLovs;
            
        public void Merge(TcBusinessDefinitions other)
        {
            if (other != null)
            {
                Classes.AddRange(other.Classes);
                StandardTypes.AddRange(other.StandardTypes);
                UnitsOfMeasure.AddRange(other.UnitsOfMeasure);
                CompoundRules.AddRange(other.CompoundRules);
                ListofValues.AddRange(other.ListofValues);
                TcLovAttach.AddRange(other.TcLovAttach);
                GrmInfo.AddRange(other.GrmInfo);
                Forms.AddRange(other.Forms);
                Datasets.AddRange(other.Datasets);
                AttributeAttaches.AddRange(other.AttributeAttaches);              
                PropertyAttaches.AddRange(other.PropertyAttaches);
                SubLovs.AddRange(other.SubLovs);
            }
        }

        public void SetSource(string sourceFile)
        {
            foreach (var c in Classes) c.Source = sourceFile;
            foreach (var c in StandardTypes) c.Source = sourceFile;
            foreach (var c in ListofValues) c.Source = sourceFile;
            foreach (var c in Forms) c.Source = sourceFile;
            foreach (var c in Datasets) c.Source = sourceFile;
        }
    }

}