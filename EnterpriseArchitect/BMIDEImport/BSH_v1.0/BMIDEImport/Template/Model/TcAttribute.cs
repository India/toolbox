﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcAttribute
    {
        [XmlAttribute("attributeName")]
        public string Name;

        [XmlAttribute("attributeType")]
        public string Type;

        [XmlAttribute("description")]
        public string Description;

        [XmlAttribute("initialValue")]
        public string InitialValue;

        [XmlAttribute("maxStringLength")]
        public string MaxStringValue;

        [XmlAttribute("isArray")]
        public bool IsArray;

        [XmlAttribute("lowerBoundValue")] 
        public string LowerBound;

        [XmlAttribute("upperBoundValue")] 
        public string UpperBound;

        [XmlAttribute("typedRefClassName")]
        public string TypedRefClassName;
    }
}