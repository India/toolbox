using System.Collections.Generic;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcPropertyAttach
    {
        [XmlAttribute("typeName")]
        public string TypeName;
        
        [XmlElement("TcProperty")]
        public List<TcProperty> Properties;
    }
}