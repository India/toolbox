﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcStandardType
    {
        [XmlAttribute("typeName")]
        public string Name;

        [XmlAttribute("parentTypeName")]
        public string ParentTypeName;

        [XmlAttribute("description")]
        public string Description;
        
        [XmlAttribute("isAbstract")]
        public bool isAbstract;

        [XmlElement("TcProperty")]
        public List<TcProperty> Properties;

        [XmlElement("artifactName")]
        public string artifactName;

        public string Source { get; set; }
    }
}