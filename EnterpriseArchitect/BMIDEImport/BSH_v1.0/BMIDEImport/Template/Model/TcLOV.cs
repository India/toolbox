﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcLOV
    {
        [XmlAttribute("description")]
        public string lovdescription;

        [XmlAttribute("lovType")]
        public string lovType;

        [XmlAttribute("name")]
        public string lovName;

        [XmlAttribute("usage")]
        public string lovUsage;
        
        [XmlAttribute("isManagedExternally")] 
        public bool isManagedExternally = false;

        [XmlElement("TcLOVValue")]
        public List<TcLOVValue> tcLOVValues;
        
        public string Source { get; set; }

        public override string ToString()
        {
            return lovName;
        }
    }
}