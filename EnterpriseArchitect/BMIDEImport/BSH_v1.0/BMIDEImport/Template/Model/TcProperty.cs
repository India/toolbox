﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcProperty
    {
        [XmlAttribute("propName")]
        public string Name;

        [XmlAttribute("propValueType")]
        public string ValueType;

        [XmlAttribute("propTypeName")]
        public string TypeName;

        [XmlAttribute("description")]
        public string Description;

        [XmlAttribute("isArray")]
        public bool IsArray;

        [XmlAttribute("propMaxStringLength")] 
        public string MaxStringLength;
    }
}