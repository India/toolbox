﻿namespace BMIDEImport.Template.Model
{
    //Definitions to collect from XML

    //Definition to merge classes, standard types and forms
    public class BusinessAttribute
    {
        public BusinessAttribute()
        {
            IsRuntime = false;
        }
        public bool IsArray { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string InitialValue { get; set; }
        public string MaxStringValue { get; set; }
        public string LowerBound { get; set; }
        public string UpperBound { get; set; }
        public string TypedRefClassName { get; set; }
        public bool IsRuntime { get; set; }
        public string ProperyType { get; set; }
    }
}
