﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BMIDEImport.Template.Model
{
    public class MergedBusinessDefintions
    {
        public MergedBusinessDefintions(BmideData container)
        {
            Container = container;
            Attributes = new List<BusinessAttribute>();
        }

        private string cls;

        public string className
        {
            get { return cls; }
            set
            {
                cls = value;
                cls = value;
            }
        }

        public string classDescription { get; set; }
        public string parentClassName { get; set; }
        
        public List<BusinessAttribute> Attributes { get; set; }
        public MergedBusinessDefintions Parent { get; set; }
        public BmideData Container { get; private set; }
        public MergedBusinessDefintions Storage { get; set; }
        public string StorageClassName { get; set; }
        public bool Abstract { get; set; }

        public bool IsExternal { get; set; }

        public override string ToString()
        {
            return className;
        }
    }
}