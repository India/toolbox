﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using BMIDEImport.EaInterface;
using BMIDEImport.EaInterface.impl;
using BMIDEImport.UI;
using EA;

namespace BMIDEImport
{
    public class Main
    {
        private const string MenuHeader = "-&BMIDE";
        // private const string MenuHeader = "-&LMtec BMIDE";
        private const string ImportBmideTemplate = "Import BMIDE Template..."; //

        private readonly IEaFactory eaFactory = new EaFactory();
        
        //Global Variables
        

        public String EA_Connect(EA.Repository Repository)
        {
            // No special processing req'd
            return "";
        }
        bool IsProjectOpen(EA.Repository Repository)
        {
            try
            {
                var c = Repository.Models;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public object EA_GetMenuItems(EA.Repository Repository, string Location, string MenuName)
        {
            /* nb example of out parameter:
			object item;
			EA.ObjectType t = Repository.GetTreeSelectedItem(out item);
			EA.Package r = (EA.Package) item;
			*/

            switch (MenuName)
            {
                case "":
                    return MenuHeader;
                case MenuHeader:
                    string[] subMenus = { ImportBmideTemplate };
                    return subMenus;
                default:
                    break;
            }
            return null;
        }
        public void EA_MenuClick(EA.Repository repository, string location, string menuName, string itemName)
        {
            switch (itemName)
            {
                case ImportBmideTemplate:
                    try
                    {
                        repository.EnableUIUpdates = false;

                        var repo = eaFactory.GetRepository(repository);

                        /* DEMO
                        
                        var package = repository.GetTreeSelectedPackage();
                        Element clsA = null;
                        try
                        {
                            clsA = (Element) package.Elements.GetByName("A");
                        }
                        catch (Exception e)
                        {
                        }

                        if (clsA == null)
                        {
                            
    
                            // create the three classes
                            clsA = (Element) package.Elements.AddNew("A", "Class");
                            var clsB = (Element) package.Elements.AddNew("B", "Class");
                            var clsC = (Element) package.Elements.AddNew("C", "Class");
                            clsA.Update();
                            clsB.Update();
                            clsC.Update();
                            
                            // create the assiciation between A and B
                            var conn = (Connector) clsA.Connectors.AddNew("demo", "Association");
                            conn.SupplierID = clsB.ElementID;
                            conn.Update();
                            
                            // create the ProxyClass for the connector
                            var clsPrx = (Element) package.Elements.AddNew("ProxyConnector", "ProxyConnector");
                            clsPrx.ClassifierID = conn.ConnectorID;
                            clsPrx.Update();
                            repository.Execute("UPDATE t_object SET Classifier_guid='" + conn.ConnectorGUID + "' WHERE Object_ID=" + clsPrx.ElementID + ";");
                            
                            // now try to "link" the ProxyConnector to class C
                            var link = (Connector) clsPrx.Connectors.AddNew("", "Dependency");
                            link.SupplierID = clsC.ElementID;
                            link.VirtualInheritance = "0";
                            link.Update();
                            link.SupplierEnd.Containment = "Unspecified"; // added by comparing values
                            link.SupplierEnd.IsChangeable = "none"; // added by comparing values
                            link.SupplierEnd.IsNavigable = true;
                            link.SupplierEnd.OwnedByClassifier = false;
                            link.SupplierEnd.Derived = false;
                            link.SupplierEnd.AllowDuplicates = false;
                            link.SupplierEnd.DerivedUnion = false;
                            link.SupplierEnd.Update();
                            
                            link.ClientEnd.Containment = "Unspecified"; // added by comparing values
                            link.ClientEnd.IsChangeable = "none"; // added by comparing values
                            link.ClientEnd.OwnedByClassifier = false;
                            link.ClientEnd.Derived = false;
                            link.ClientEnd.AllowDuplicates = false;
                            link.ClientEnd.DerivedUnion = false;
                            link.ClientEnd.Update();
                            
                            repository.Execute("UPDATE t_connector SET SourceTS='instance', DestTS='instance' WHERE Connector_ID=" + link.ConnectorID + ";");

                            
                        }
                        else
                        {
                            var conn = (Connector) clsA.Connectors.GetAt(0);
                            var bID = conn.SupplierID;
                            var aID = conn.ClientID;
                            var clsB = (Element) repository.GetElementByID(bID);
                            var clsC = (Element) package.Elements.GetByName("C");
                            var connectors = clsC.Connectors.Count;
                            var link = (Connector)clsC.Connectors.GetAt(0);
                            var cID = clsC.ElementID;
                            var lClient = link.ClientID;
                            var lSupp = link.SupplierID;
                                                        
                            var cls1 = (Element) repository.GetElementByID(lClient);
                            var cls2 = (Element) repository.GetElementByID(lSupp);

                            var cl1 = cls1.ClassfierID;
                            var cl2 = cls1.ClassifierID;
                            
                            var cl3 = cls2.ClassfierID;
                            var cl4 = cls2.ClassifierID;

                            var name1 = cls1.Name;
                            var name2 = cls2.Name;

                            var tp1 = cls1.Type;
                            var tp2 = cls2.Type;

                            var cAbs = cls1.Abstract;
                            var cal = cls1.Alias;
                            var cCompl = cls1.Complexity;
                            var cGenF = cls1.Genfile;
                            var cGenL = cls1.Genlinks;
                            var cGenT = cls1.Gentype;
                            var cLock = cls1.Locked;
                            var cMult = cls1.Multiplicity;
                            var cName = cls1.Name;
                            var cNot = cls1.Notes;
                            var cPers = cls1.Persistence;
                            var cPhas = cls1.Phase;
                            var cPrio = cls1.Priority;
                            var cStat = cls1.Status;
                            var cSte = cls1.Stereotype;
                            var cSubt = cls1.Subtype;
                            var cTabl = cls1.Tablespace;
                            var cTag = cls1.Tag;
                            var cType = cls1.Type;
                            var cVis = cls1.Visibility;
                            var cAf = cls1.ActionFlags;
                            var cClass = cls1.ClassifierName;
                            var cExt = cls1.ExtensionPoints;
                               
                            var alias = link.Alias;
                            var color = link.Color;
                            var constrCnt = link.Constraints.Count;
                            var dir = link.Direction;
                            var name = link.Name;
                            var notes = link.Notes;
                            var st = link.Stereotype;
                            var subtype = link.Subtype;
                            var type = link.Type;
                            var il = link.IsLeaf;
                            var ir = link.IsRoot;
                            var isp  = link.IsSpec;
                            var mt = link.MetaType;
                            var sf = link.StateFlags;
                            var stex = link.StereotypeEx;
                            var styleEx = link.StyleEx;
                            var fkey = link.ForeignKeyInformation;
                            var lfqst = link.FQStereotype;

                            var supplierEnd = link.SupplierEnd;
                            var agg = supplierEnd.Aggregation;
                            var sAlias = supplierEnd.Alias;
                            var card = supplierEnd.Cardinality;
                            var sConstr = supplierEnd.Constraint;
                            var sCont = supplierEnd.Containment;
                            var sDer = supplierEnd.Derived;
                            var sEnd = supplierEnd.End;
                            var sNav = supplierEnd.Navigable;
                            var sOrd = supplierEnd.Ordering;
                            var sQual = supplierEnd.Qualifier;
                            var sRole = supplierEnd.Role;
                            var sStereo = supplierEnd.Stereotype;
                            var sVis = supplierEnd.Visibility;
                            var sAllowDupl = supplierEnd.AllowDuplicates;
                            var sDU = supplierEnd.DerivedUnion;
                            var sNavi = supplierEnd.IsNavigable;
                            var sChg = supplierEnd.IsChangeable;
                            var sOT = supplierEnd.ObjectType;
                            var rN = supplierEnd.RoleNote;
                            var rT = supplierEnd.RoleType;
                            var sStEx = supplierEnd.StereotypeEx;
                            var ow = supplierEnd.OwnedByClassifier;

                            var clientEnd = link.ClientEnd;
                            var agg2 = clientEnd.Aggregation;
                            var sAlias2 = clientEnd.Alias;
                            var card2 = clientEnd.Cardinality;
                            var sConstr2 = clientEnd.Constraint;
                            var sCont2 = clientEnd.Containment;
                            var sDer2 = clientEnd.Derived;
                            var sEnd2 = clientEnd.End;
                            var sNav2 = clientEnd.Navigable;
                            var sOrd2 = clientEnd.Ordering;
                            var sQual2 = clientEnd.Qualifier;
                            var sRole2 = clientEnd.Role;
                            var sStereo2 = clientEnd.Stereotype;
                            var sVis2 = clientEnd.Visibility;
                            var sAllowDupl2 = clientEnd.AllowDuplicates;
                            var sDU2 = clientEnd.DerivedUnion;
                            var sNavi2 = clientEnd.IsNavigable;
                            var sChg2 = clientEnd.IsChangeable;
                            var sOT2 = clientEnd.ObjectType;
                            var rN2 = clientEnd.RoleNote;
                            var rT2 = clientEnd.RoleType;
                            var sStEx2 = clientEnd.StereotypeEx;
                            var ow2 = clientEnd.OwnedByClassifier;
                        }
                        */
                

                
                        var dlg = new ImportDialogView(repo);
                        var ctl = new ImportDialogController(repo, dlg);
                        dlg.ShowDialog();
              

                    }
                    finally
                    {
                        repository.EnableUIUpdates = true;
                    }
                    break;
                default:
                    break;
            }   
        }
        
        public void EA_Disconnect()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

    }
}
