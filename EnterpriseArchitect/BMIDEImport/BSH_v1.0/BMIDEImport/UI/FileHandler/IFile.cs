﻿using System;

namespace BMIDEImport.UI.FileHandler
{
    public interface IFile : IDisposable
    {
        string ImportFilePath { get; }
    }
}