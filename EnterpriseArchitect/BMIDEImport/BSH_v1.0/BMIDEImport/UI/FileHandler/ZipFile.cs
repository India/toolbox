﻿using System;
using System.IO;
using System.Linq;

namespace BMIDEImport.UI.FileHandler
{
    class ZipFile : IFile
    {
        private string _fileName;
        private string _extractedFileName;
        private string _path;

        public ZipFile(string fileName)
        {
            _fileName = fileName;
            _extractedFileName = null;
        }

        public string ImportFilePath
        {
            get
            {
                EnsureExtracted();
                return _extractedFileName;
            }
        }

        private void EnsureExtracted()
        {
            if (_extractedFileName == null)
            {
                var tempPath = Path.GetTempPath();
                var dirUID = System.Guid.NewGuid().ToString("B");

                _path = Path.Combine(tempPath + dirUID);
                
                System.IO.Directory.CreateDirectory(_path);
                System.IO.Compression.ZipFile.ExtractToDirectory(_fileName, _path);

                var pathToImport = Path.Combine(_path, "install");

                var dirs = Directory.EnumerateDirectories(pathToImport);
                if (dirs.Count() != 1)
                    throw new ArgumentException(
                        "Unexpected content of zip file. Expecting 'install\\<packagenams>\\...'");
                
                pathToImport = Path.Combine(pathToImport, dirs.ElementAt(0));

                var files = Directory.GetFiles(pathToImport);

                foreach (var file in files)
                {
                    if (file.EndsWith("_template.xml"))
                    {
                        _extractedFileName = Path.Combine(pathToImport, file);
                        break;
                    }
                }

            }
        }

        public void Dispose()
        {
            if (_extractedFileName != null)
            {
                System.IO.Directory.Delete(_path, true);
            }
        }

    }
}