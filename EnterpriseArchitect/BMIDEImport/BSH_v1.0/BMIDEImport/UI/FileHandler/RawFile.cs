﻿namespace BMIDEImport.UI.FileHandler
{
    class RawFile : IFile
    {
        private string _fileName;

        public RawFile(string fileName)
        {
            _fileName = fileName;
        }

        public string ImportFilePath
        {
            get { return _fileName; } }
        
        public void Dispose()
        {
            // Nopthing to do
        }

    }
}