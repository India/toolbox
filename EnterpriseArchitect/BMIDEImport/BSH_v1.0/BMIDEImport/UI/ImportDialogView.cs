﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMIDEImport.EaInterface;
using BMIDEImport.Template;
using EA;
using Microsoft.Win32;
using IFile = BMIDEImport.UI.FileHandler.IFile;

namespace BMIDEImport.UI
{
    public partial class ImportDialogView : Form
    {

        public ImportDialogView(IEaRepository repository)
        {
            InitializeComponent();
            var version = Assembly.GetAssembly(typeof(ImportDialogView)).GetName().Version;
            this.Text = this.Text + " (" + version.Major + "." + version.Minor + " [" + version.Build + "] )";
        }

        public bool Cancelled { private get; set; }


        #region View Events
        public event EventHandler SelectModel;

        private void OnSelectModel(object sender, EventArgs e)
        {
            SelectModel?.Invoke(sender, e);
        }

        public event EventHandler SelectTemplate;
        
        private void OnSelectTemplateDir(object sender, EventArgs e)
        {
            SelectTemplate?.Invoke(sender, e);
        }

        public event EventHandler ModelChanged;

        private void OnModelChanged(object sender, EventArgs e)
        {
            ModelChanged?.Invoke(sender, e);
        }
        
        public event EventHandler TemplateChanged;

        private void OnTemplateChanged(object sender, EventArgs e)
        {
            TemplateChanged?.Invoke(sender, e);
        }

        public event EventHandler Import;

        private void OnImport(object sender, EventArgs e)
        {
            Import?.Invoke(sender, e);
        }

        public event EventHandler ImportFinished;
        
        public void OnImportFinished()
        {
            ImportFinished?.Invoke(this, null);   
        }

        public event EventHandler Cancel;

        private void OnCancel(object sender, EventArgs e)
        {
            Cancel?.Invoke(sender, e);
        }
        
        #endregion


        public void CheckCancelled()
        {
            if (Cancelled)
                throw new ImportDialogController.CancelledException();
        }

        private void ImportDialog_Load(object sender, EventArgs e)
        {

        }
                
        public void EnableProgress(bool enable)
        {
            _lbEntity.Enabled = enable;
            _lblText.Enabled = enable;
            _pbProgressBar.Enabled = enable;
            _btCancel.Text = enable ? "Cancel" : "Close";
        }

        public void EnableEntries(bool enable)
        {
            _txModel.Enabled = enable;
            _txTemplateDir.Enabled = enable;
            _lbModel.Enabled = enable;
            _lbTemplates.Enabled = enable;
            _btImport.Enabled = enable;
        }

        /// <summary>
        /// Property for the main text (job) shown above the ProgressBar
        /// </summary>
        public string MainJob
        {
            get { return _lblText.Text; }
            set { _lblText.Text = value; }
        }

        /// <summary>
        /// Property for the sub text (job) shown below the ProgressBar
        /// </summary>
        public string SubJob
        {
            get { return _lbEntity.Text; }
            set { _lbEntity.Text = $"[{_pbProgressBar.Value}/{_pbProgressBar.Maximum}]: {value}"; }
        }

        /// <summary>
        /// Advances the progress bar one step
        /// </summary>
        public void PerformStep()
        {
            _pbProgressBar.PerformStep();
          
        }

        /// <summary>
        /// Property to hold the MAX value of the progressbar. Setting the MAX value will reset the progress bar
        /// </summary>
        public int Max
        {
            get { return _pbProgressBar.Maximum; }
            set
            {
                _pbProgressBar.Maximum = value;
                _pbProgressBar.Minimum = 1;
                _pbProgressBar.Value = 1;
                _pbProgressBar.Step = 1;
            }
        }

        public bool AllowImport
        {
            get { return _btImport.Enabled; }
            set { _btImport.Enabled = value; }
        }

        public ImportDialogController.ItemData SelectedModel
        {
            get { return _txModel.SelectedItem as ImportDialogController.ItemData; }
            set { _txModel.SelectedItem = value; }
        }

        public string TemplateDir
        {
            get { return _txTemplateDir.Text; }
            set { _txTemplateDir.Text = value; }
        }

        public string ModelText
        {
            get { return _txModel.Text; } set { _txModel.Text = value; }
        }

        public string TemplateText
        {
            get { return _txTemplateDir.Text; } set { _txTemplateDir.Text = value; }
        }

        public void AddMessage(string msg)
        {
            if (String.IsNullOrWhiteSpace(msg) == false)
                lstActions.Items.Add(msg);
        }


        public void AddModel(ImportDialogController.ItemData itemData)
        {
            _txModel.Items.Add(itemData);
        }

        public void ClearLogs()
        {
            this.lstActions.Items.Clear();
        }
    }
}
