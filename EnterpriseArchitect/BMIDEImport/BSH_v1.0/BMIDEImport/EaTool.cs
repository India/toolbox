﻿using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Linq;
using System.Runtime.InteropServices;
using BMIDEImport.EaInterface;
using BMIDEImport.Model;
using EA;
using Attribute = EA.Attribute;
using Package = EA.Package;

namespace BMIDEImport
{
    public class EaTool
    {

        public static EA.Package CreateOrGetPackage(EA.Package parent, string name)
        {
            EA.Package result = null;
            try
            {
                result = (EA.Package) parent.Packages.GetByName(name);
            }
            catch(Exception) {}
            
            if (result == null)
            {
                result = parent.Packages.AddNew(name, "Package") as Package;
                result.Update();
            }

            return result;
        }

        private static void UpdateDirection(Connector connector, ModelRelation data, IProgress<ProgressInfo> progressIndicator)
        {
            //NOTE: This is a workaround for a EA (API-) error:
            //      The direction is stored reversed, but reading is correct.
            //      i.e. when Direction is "Destination -> Source" - we need to write
            //      exact the same value to change direction! 

            
            /* That would be correct, but does not work:
            data.ReportUpdate(data.Update("Direction",
                connector.Direction,
                "Destination -> Source", x => { connector.Direction = x; }), progressIndicator);
            */
            
            // the following is more or less the same than data.ReportUpdate(data.Update..., except the 
            //    wrong value
            var result = "";
            
            if (connector.Direction.Equals("Source -> Destination") == false)
            {
                result = $"Updated Direction: {connector.Direction} => Source -> Destination\n";
                connector.Direction = "Destination -> Source"; // WORKAROUND!! EA will reverse that!
                if(data.Status != ModelPart.EStatus.New)
                    data.Status = ModelPart.EStatus.Updated;
            }

            data.ReportUpdate(result, progressIndicator);

        }

        private static void UpdateStereotype(Element element, ModelPart data, IProgress<ProgressInfo> progressIndicator)
        {
            if (String.IsNullOrEmpty(data.StereoType) == false)
            {
                data.ReportUpdate(data.Update("Stereotype",
                    element.Stereotype,
                    data.StereoType, x => { element.Stereotype = x; }), progressIndicator);
            }
        }


 
    }
}