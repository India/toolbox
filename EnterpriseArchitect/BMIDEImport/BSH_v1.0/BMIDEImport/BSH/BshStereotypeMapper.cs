﻿using System.IO;
using BMIDEImport.EaInterface;
using BMIDEImport.Model;

namespace BMIDEImport.BSH
{
    public static class BshStereotypeMapper
    {
        private static void OnPreImport(object sender, ImportEventArgs e)
        {
            var mappingFile = e.BmideData.Path + Path.DirectorySeparatorChar + "mapping.txt";

            if (File.Exists(mappingFile))
            {
                var contents = File.ReadAllLines(mappingFile);
                foreach (var mapping in contents)
                {
                    var values = mapping.Split(':');
                    if (values.Length != 2) 
                        continue;
                    
                    var cls = e.ImportModel.MainTemplate.GetClass(values[0]);
                    if (cls != null)
                        cls.StereoType = values[1];
                    else
                    {
                        e.ImportModel.MainTemplate.GetLov(values[0]);
                    }
                }
            }

            foreach (var lov in e.BmideData.BusinessData.Definitions.ListofValues)
            {
                var source = lov.Source;
                if (string.IsNullOrEmpty(source) == false && source.Contains("_template") == false)
                {
                    source = source.Replace("bsh_", "");
                    var modelLov = e.ImportModel.MainTemplate.GetLov(lov.lovName);
                    if (modelLov != null && string.IsNullOrEmpty(modelLov.StereoType))
                        modelLov.StereoType = source;
                }
            }

            foreach (var cls in e.BmideData.BusinessData.Definitions.Classes)
                UpdateStereotypeOfClass(e.ImportModel.MainTemplate, cls.Name, cls.Source);

            foreach (var cls in e.BmideData.BusinessData.Definitions.Datasets)
                UpdateStereotypeOfClass(e.ImportModel.MainTemplate, cls.Name, cls.Source);
            
            foreach (var cls in e.BmideData.BusinessData.Definitions.Forms)
                UpdateStereotypeOfClass(e.ImportModel.MainTemplate, cls.Name, cls.Source);
            
            foreach (var cls in e.BmideData.BusinessData.Definitions.StandardTypes)
                UpdateStereotypeOfClass(e.ImportModel.MainTemplate, cls.Name, cls.Source);
            
            foreach (var cls in e.BmideData.BusinessData.Definitions.RuntimeTypes)
                UpdateStereotypeOfClass(e.ImportModel.MainTemplate, cls.Name, "runtime");

            foreach (var mCls in e.ImportModel.MainTemplate.Classes)
            {
                UpdateAttributes(mCls);
            }

            foreach (var templ in e.ImportModel.MainTemplate.Dependencies)
            {
                foreach (var mCls in templ.Classes)
                    UpdateAttributes(mCls);
            }
        }

        private static void UpdateAttributes(ModelClassLike mCls)
        {
            foreach (var attr in mCls.Attributes.Values)
            {
                if (attr.Type == null) continue;
                
                if (attr.Type.Contains("PROP_untyped_relation"))
                    attr.Visibility = Scope.Protected;
                if (attr.Type.Contains("POM_untyped_relation"))
                    attr.Visibility = Scope.Protected;
                if (attr.Type.Contains("PROP_typed_reference"))
                    attr.Visibility = Scope.Protected;
                if (attr.Type.Contains("POM_typed_reference"))
                    attr.Visibility = Scope.Protected;
                if (attr.Type.Contains("PROP_untyped_reference"))
                    attr.Visibility = Scope.Protected;
                if (attr.Type.Contains("POM_untyped_reference"))
                    attr.Visibility = Scope.Protected;
            }
        }

        private static void UpdateStereotypeOfClass(Model.Template templ, string cls, string source)
        {
            if (string.IsNullOrEmpty(source)  || source.Contains("_template") ) return;
            
            source = source.Replace("bsh_", "");
            var modelCls = templ.GetClass(cls);
            if (modelCls != null && string.IsNullOrEmpty(modelCls.StereoType))
            {
                modelCls.StereoType = source;
                if (modelCls.Storage != null)
                    modelCls.Storage.StereoType = source;
            }
        }

        public static void RegisterEvents(BmideImportCommand command)
        {
            command.PreImport += OnPreImport;
        }
    }
}