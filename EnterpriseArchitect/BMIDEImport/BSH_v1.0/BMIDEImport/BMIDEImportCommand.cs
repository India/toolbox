﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BMIDEImport.EaInterface;
using BMIDEImport.Template;
using BMIDEImport.UI;
using EA;


namespace BMIDEImport.Template
{
}

namespace BMIDEImport
{
    public class BmideImportCommand
    {
        private ImportDialogView _progressForm;
        private StringBuilder _messages = null;

        public class EaData
        {
            public EaData(IEaRepository repository)
            {
                this.Repository = repository;
            }
            
            public IEaRepository Repository { get; private set; }
            
            public Dictionary<string, Package>  Packages { get; } = new Dictionary<string, Package>();
            public Dictionary<string, Element>  Elements { get; } = new Dictionary<string, Element>();
            public Dictionary<string, Element>  Lovs     { get; } = new Dictionary<string, Element>();
            public Dictionary<Package, Diagram> PackageDiagrams     { get; } = new Dictionary<Package, Diagram>();

            public IEaPackage RootPackage { get; set; }

        }

        public class ProgressReporter : Progress<ProgressInfo>
        {
            private readonly ImportDialogView _progressForm;

            public ProgressReporter(Action<ProgressInfo> handler, ImportDialogView dlg)
            : base(handler)
            {
                this._progressForm = dlg;
            }
            
            protected override void OnReport(ProgressInfo info)
            {
                _progressForm.CheckCancelled();
                base.OnReport(info);
            }
        }


        public delegate void ImportEventHandler(object sender, ImportEventArgs e);

        public event ImportEventHandler PreImport;
        public event ImportEventHandler PostImport;
        
        /// <summary>
        /// Method to import a BMIDE package
        /// </summary>
        /// <param name="repository">EA repository</param>
        /// <param name="fileName">File to import</param>
        public async void  Execute(EaData eaData, BmideData bmideData, ImportDialogView dlg)
        {
            try
            {
                //EA Objects
                EaObjectType selectedObject = eaData.Repository.GetTreeSelectedItemType();
                Hashtable elemNameId = new Hashtable();

                switch (selectedObject)
                {
                    case EaObjectType.OtPackage:
                        this._progressForm = dlg;
                        _progressForm.Text = "Start Import...";
                        _messages = new StringBuilder();
                        var progressIndicator = new ProgressReporter(ReportProgress, dlg);

                        eaData.RootPackage = eaData.Repository.GetTreeSelectedPackage();
                        
                        ReportProgress(new ProgressInfo("Import " + bmideData.FileName));

                        Model.Model model = new Model.Model(bmideData);

                        ImportEventArgs args = new ImportEventArgs() {ImportModel = model, EaData = eaData, BmideData = bmideData};
                        OnPreImport(args);
                        await model.Align(eaData.RootPackage, progressIndicator);
                        OnPostImport(args);

                        CreateProtocol(eaData.Repository, eaData.RootPackage);
                       
                        break;
                }
            }
            catch(ImportDialogController.CancelledException) {}
            finally
            {
                dlg.OnImportFinished();
            }
                
        }

        private void CreateProtocol(IEaRepository repository, IEaPackage rootPackage)
        {
            if (_messages != null)
            {
                IEaPackage logPackage = rootPackage.CreateOrGetPackage("Logs");
                var now = DateTime.Now;

                IEaElement doc = logPackage.AddNewElement("Import Log " +now.ToShortDateString() + " - " + now.ToShortTimeString(), "Artifact");
                if (_messages.Length > 0)
                {
                    doc.Notes = _messages.ToString();
                }
                else
                    doc.Notes = "Nothing changed!";
                doc.Update();
                logPackage.Update();
                logPackage.RefreshElements();
            }
        }

        private void ReportProgress(ProgressInfo info)
        {
            switch (info.ProgressType)
            {
                case ProgressInfo.eProgressType.Reset:
                    this._progressForm.Max = info.Max;
                    this._progressForm.MainJob = info.Text;
                    this._progressForm.SubJob = info.SubText;
                    break;
                case ProgressInfo.eProgressType.Step:
                    if (info.Text != null)
                        this._progressForm.MainJob = info.Text;
                    if (info.SubText != null)
                        this._progressForm.SubJob = info.SubText;
                    if(info.PerformStep)
                        this._progressForm.PerformStep();
                    break;
                case ProgressInfo.eProgressType.Message:
                    this._progressForm.AddMessage(info.Text);
                    this._messages.AppendLine(info.Text);
                    break;
            }
        }

        protected virtual void OnPreImport(ImportEventArgs e)
        {
            PreImport?.Invoke(this, e);
        }

        protected virtual void OnPostImport(ImportEventArgs e)
        {
            PostImport?.Invoke(this, e);
        }
    }

    // cls

} // Ns