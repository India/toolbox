﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMIDEImport
{
    /// <summary>
    /// Structure to hold progress Update information
    /// Using the ctor with text and a max value resets the progress bar to "0" and shows the given text
    /// Using the ctor with text, subtext and optional performTask updates the Progressbar accordingly
    /// </summary>
    public class ProgressInfo
    {
        /// <summary>
        /// Enum to describe type of progress:
        /// Reset: resets progress indication (Text, Max)
        /// Step: Trigger a progress step (Text, Subtext, PerformStep) 
        /// Message: Only report a message (Text)
        /// </summary>
        public enum eProgressType
        {
            Reset,
            Step,
            Message
        };
        
        /// <summary>
        /// Returns the "main" text for the progressbar
        /// </summary>
        public string Text { get; }
        /// <summary>
        /// Returns the subtext (below the progressbar)
        /// </summary>
        public string SubText { get; }
        /// <summary>
        /// Returns the maximum value. -1 means: not set (hint for "Update")
        /// </summary>
        public int Max { get; }  
        /// <summary>
        /// Return type of reported Progress
        /// </summary>
        public eProgressType ProgressType { get; }
        /// <summary>
        /// Return whether to perform a step
        /// </summary>
        public bool PerformStep { get; }

        /// <summary>
        /// Ctor to create a Progressinfo which RESET the Progressbar
        /// </summary>
        /// <param name="text">Text to be shown in Progressbar</param>
        /// <param name="max">Maximum value for progressbar</param>
        public ProgressInfo(string text, int max)
        {
            this.Text = text;
            this.Max = max;
            SubText = "";
            this.ProgressType = eProgressType.Reset;
        }

        /// <summary>
        /// Ctor to create a ProgressInformation to UPDATE the ProgressBAr
        /// </summary>
        /// <param name="text">Main Text - can be null (leaves Main Text)</param>
        /// <param name="subText">Subtext (shown below ProgressBar) - can be null</param>
        /// <param name="performStep">Optional (default: true): flags whether the progressbar should advance one step</param>
        public ProgressInfo(string text, string subText, bool performStep = true)
        {
            this.Text = text;
            SubText = subText;
            this.Max = -1;
            this.PerformStep = performStep;
            this.ProgressType = eProgressType.Step;
        }
        
        /// <summary>
        /// Ctor to create a ProgressInformation to log a message
        /// </summary>
        /// <param name="text">Main Text - can be null (leaves Main Text)</param>
        public ProgressInfo(string text)
        {
            this.Text = text;
            SubText = "";
            this.Max = -1;
            this.ProgressType = eProgressType.Message;
        }
    }
}
