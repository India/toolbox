﻿using System;
using System.Collections.Generic;
using System.Linq;
using BMIDEImport.EaInterface;
using Attribute = EA.Attribute;

namespace BMIDEImport.Model
{
    public abstract class ModelClassLike : ModelPart
    {
        
        public Template Template { get; protected set; }
        protected string Description { get; set; }
        public string Name { get; protected set; }
        public IEaElement EaClass { get; protected set; }
        public Dictionary<string, ModelAttribute> Attributes { get; } = new Dictionary<string, ModelAttribute>();
        public override string StereoType { get; set; }
        
        private readonly List<ModelRelation> _relations = new List<ModelRelation>();
        protected IEnumerable<ModelRelation> Relations => _relations;

        protected ModelClassLike(Model model) : base(model)
        {
        }

        protected Package CreateOrGetContainerPackage(ModelClass externalParent)
        {
            Package result = null;
            if (externalParent == null)
            {
                result = Template.MainPackage;
            }
            else
            {
                Package parentPackage = CreateOrGetContainerPackage(externalParent.Parent);
                result = parentPackage.CreateOrGetSubPackage(externalParent.Name);
            }
            return result;
        }

        protected void AlignAttributes(IProgress<ProgressInfo> progressIndicator)
        {
            var eaAttributes = EaClass.Attributes.ToDictionary(attr => attr.Name);

            foreach (var attr in Attributes.Values)
            {
                attr.Align(progressIndicator, eaAttributes);
            }
        }

        public abstract void Align(IEaPackage package, IProgress<ProgressInfo> progressIndicator);

        public void AddRelation(ModelRelation relation)
        {
            foreach(var r in _relations)
                if (r.Equals(relation))
                    return;
            _relations.Add(relation);
        }

        public ModelRelation FindRelation(ModelClass secondary, ModelClass relation)
        {
            foreach(var r in _relations)
                if (r.Secondary == secondary && r.Relation == relation)
                    return r;
            return null;
        }

        
        #region ProgressCounting

        private bool counted = false;

        public override int CountClasses()
        {
            int result = 0;
            if (counted == false)
            {
                counted = true;
                result += 1; // this
            }

            return result;
        }


        #endregion

    }
}