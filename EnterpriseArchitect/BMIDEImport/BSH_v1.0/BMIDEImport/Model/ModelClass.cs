﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using BMIDEImport.EaInterface;
using BMIDEImport.Template;
using BMIDEImport.Template.Model;
using EA;
using Attribute = EA.Attribute;

namespace BMIDEImport.Model
{
    public class ModelClass : ModelClassLike
    {
        public ModelClass(Model model, MergedBusinessDefintions classDef, BmideData data, Template container)
            : base(model)
        {
            Template = container;
            InitFromData(classDef, data);
            Aligned = false;
        }

        public ModelClass Parent      { get; private set; }
        public Package    Package     { get; private set; }
        public string     Alias       { get; private set; }
        public ModelClass Storage     { get; private set; }
        public bool Abstract          { get; set; }
        public bool Aligned { get; set; }
        
        protected override string UpdatePrefix => "Update class <" + ToString() + ">";
        
        // IsExternal marks classes which are not found in import files. I.e. when importing only a partial extension file.
        public bool IsExternal        { get; private set; }


        private void InitFromData(MergedBusinessDefintions classDef, BmideData data)
        {
            Name = classDef.className;
            Parent = Template.CreateOrGetClass(classDef.Parent, data);
            Storage = Template.CreateOrGetClass(classDef.Storage, data);
            if(classDef.IsExternal == false)
                Package = CreateOrGetContainerPackage(GetExternalParent());
            else
            {
                Package = null;
            }
            Description = classDef.classDescription;
            Abstract = classDef.Abstract;
            IsExternal = classDef.IsExternal;

            if (Parent != null)
            {
                new ModelGeneralization(Model, this, Parent);
            }

            if (Storage != null)
            {
                new ModelStorageRelation(this.Model, this, Storage);
            }

            var localizationKey = @"BusinessObject{::}" + classDef.className;
            if (data.Localizations.ContainsKey(localizationKey))
            {
                Alias = data.Localizations[localizationKey].Text;
            }

            foreach (var attr in classDef.Attributes)
            {
                CreateOrGetAttribute(attr.Name, data, attr);
            }


            foreach (var lovAttach in data.BusinessData.Definitions.TcLovAttach)
            {
                if (lovAttach.tcLOVAttachTypeName.Equals(Name))
                {
                    var attribute =
                        DuplicateOrGetAttribute(lovAttach.tcLOVAttachPropertyInfo.tcLOVAttachValuePropertyName);
                    var lov = Template.GetLov(lovAttach.tcLOVAttachLovName);

                    if (attribute != null && lov != null)
                        attribute.Lov = lov;
                }
            }
        }

        private ModelClass GetTopMostParent()
        {
            return Parent == null ? this : Parent.GetTopMostParent();
        }

        private ModelAttribute DuplicateOrGetAttribute(string name)
        {
            ModelAttribute result = Attributes.ContainsKey(name) ? Attributes[name] : null;
            if (result == null)
            {
                ModelAttribute inheritedAttribute = Parent?.GetAttributeFromHierarchy(name);
                if (inheritedAttribute != null)
                {
                    result = new ModelAttribute(Model, this,inheritedAttribute);
                    Attributes.Add(name, result);
                }
            }
            return result;
        }

        private ModelAttribute GetAttributeFromHierarchy(string name)
        {
            var result = Attributes.ContainsKey(name)
                ? Attributes[name]
                : Parent?.GetAttributeFromHierarchy(name);
            return result;
        }

        public ModelAttribute CreateOrGetAttribute(string name, BmideData data, BusinessAttribute attr)
        {
            ModelAttribute result = null;
            if (Attributes.ContainsKey(name))
                result = Attributes[name];
            else
            {
                result = new ModelAttribute(Model, data, this, attr);
                Attributes.Add(name, result);
            }
            return result;
        }

        private ModelClass GetExternalParent()
        {       
            ModelClass result = Parent;
            if (result != null)
            {
                if (result.Template == this.Template)
                    result = result.GetExternalParent();
            }
            return result;
        }

        public override string ToString()
        {
            return Name;
        }

        public override void Align(EaInterface.IEaPackage rootPackage, IProgress<ProgressInfo> progressIndicator)
        {
            if (EaClass != null) // || Aligned)
                return;

            Aligned = true;

            IEaPackage containerPackage = null;
            
            if (IsExternal)
            {
                EaClass = LocateExternalClass(rootPackage, Name);
                if (EaClass != null)
                {
                    Template = Template.LocateTemplate(EaClass.Package, rootPackage, progressIndicator);
                    Package = Template.MainPackage.LocatePackage(EaClass.Package, rootPackage, progressIndicator);
                }
                    
                progressIndicator?.Report(new ProgressInfo(Template.Name, Name));
            }
            else
            {
                if (Package == Template.MainPackage)
                {
                    if (Parent != null && Parent.IsExternal && Parent.Template == Template)
                    {
                        Parent.Align(rootPackage, progressIndicator);
                        if (Parent.Template == Template)
                        {
                            if (Parent.Package != null)
                                Package = Parent.Package;
                        }
                        else
                        {
                            CreateOrGetContainerPackage(GetExternalParent());
                        }

                        /*if(Parent.Package != null)
                            Package = Parent.Package;*/
                    }
                }
                Package.Align(rootPackage, progressIndicator);

                progressIndicator?.Report(new ProgressInfo(Template.Name, Name));
                containerPackage = Package.EaPackage;   

                EaClass = (EaInterface.IEaElement) containerPackage.GetElementByName(Name);
            }
           
            if (EaClass == null && IsExternal == false)
            {
                EaClass = containerPackage.AddNewElement(Name, "Class");            
                EaClass.Update();
                containerPackage.Update();
                containerPackage.RefreshElements();
                Model.logNew($"New Class: {Template.Name}::{Name}");
                Status = EStatus.New;
                progressIndicator?.Report(new ProgressInfo("Add class <" + Name + "> to " + Package.ToString(true)));
            }

            if (EaClass != null)
            {
                ReportUpdate(Update("Notes", EaClass.Notes, Description, x => { EaClass.Notes = x; }), progressIndicator);
                if (Alias != null)
                    ReportUpdate(Update("Alias", EaClass.Alias, Alias, x => { EaClass.Alias = x; }), progressIndicator);
                string abstractFlag = Abstract ? "1" : "0";
                ReportUpdate(Update("Abstract", EaClass.Abstract, abstractFlag, x => { EaClass.Abstract = x; }), progressIndicator);
                string stereotype = StereoType ?? "";
                if(string.IsNullOrEmpty(stereotype) == false)
                    ReportUpdate(Update("Stereotype", EaClass.Stereotype, stereotype, x => { EaClass.Stereotype = x; }), progressIndicator);

                EaClass.Update();
                
                Parent?.Align(rootPackage, progressIndicator);
                Storage?.Align(rootPackage, progressIndicator);

                AlignAttributes(progressIndicator);

                foreach (var relation in Relations)
                {
                    relation.Align(rootPackage, progressIndicator);
                }
            }          
        }

        private IEaElement LocateExternalClass(IEaPackage package, string name)
        {
            IEaElement result = null;

            result = package.GetElementByName(name);

            foreach (var subPkg in package.Packages)
            {
                var subResult = LocateExternalClass(subPkg, name);
                if (result == null)
                {
                    result = subResult;
                }
                else if (subResult != null)
                {
                    // not unique class name!? Do some logging here!
                    break;
                }
            }

            return result;
        }
        
        public void CreateDcrRelations(Model model, ModelDeepCopyRules dataDeepCopyRules)
        {
            var allRules = dataDeepCopyRules.FindRules(this);
            foreach (var rule in allRules)
            {
                ModelRelation.CreateRelation(model, model.Data, rule);
            }
        }
        
        #region ProgressCounting

        public override int CountClasses()
        {
            var result =  base.CountClasses();
            if (result > 0)
            {
                if(Parent != null)
                    result += Parent.CountClasses();
                
                if(Storage != null)
                    result += Storage.CountClasses();

                foreach (var rel in Relations)
                {
                    if (rel.Secondary != null && rel.Secondary != this)
                        result += rel.CountClasses();
                }
            }

            return result;
        }

        #endregion


    }
}