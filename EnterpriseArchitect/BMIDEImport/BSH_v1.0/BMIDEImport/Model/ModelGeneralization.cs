﻿using System;
using BMIDEImport.EaInterface;

namespace BMIDEImport.Model
{
    public class ModelGeneralization
        : ModelRelation
    {
        public ModelGeneralization(Model model, ModelClass primary, ModelClass secondary)
            : base(model, primary, secondary, 1, 1, ModelRelation.ConnectorType.Generalization)
        {
        }

        public override string StereoType => "Parent Relation";

        public override string ToString()
        {
            return GetName(Primary)
                   + " --Generalization-->"
                   + GetName(Secondary);
        }

        public override string Name => "";
    }
}