﻿using System;
using System.Collections.Generic;
using System.Text;
using BMIDEImport.EaInterface;
using BMIDEImport.Template;
using BMIDEImport.Template.Model;
using EA;
using Attribute = EA.Attribute;

namespace BMIDEImport.Model
{
    public class ModelAttribute : ModelPart
    {
        private ModelLov _lov;
        
        public const string TypeTypedref = "POM_typed_reference";
        public const string TypeUnTypedref = "POM_untyped_reference";

        public ModelAttribute(Model model, BmideData data, ModelClassLike container, BusinessAttribute attr) : base(model)
        {
            Container = container;
            InitFromData(attr, data);
        }

        public ModelAttribute(Model model, ModelClassLike container, ModelAttribute attr) : base(model)
        {
            Container = container;
            InitFromModel(attr);
        }

        public ModelAttribute(Model model, ModelClassLike lov, TcLOVValue value, BmideData data) : base(model)
        {
            InitFromData(lov, value, data);
        }

        public ModelClassLike Container { get; private set; }
        public string TypedRefClassName { get; private set; }
        public string Type { get; private set; }
        public string MaxStringLength { get; private set; }
        public string UpperBound { get; private set; }
        public string LowerBound { get; private set; }
        public bool IsArray { get; private set; }
        public string InitialValue { get; private set; }
        public string Description { get; private set; }
        public string Alias { get; private set; }
        public string Name { get; private set; }
        public string PropertyType { get; private set; }
        public Scope Visibility { get; set; } = Scope.Public;

        public ModelLov Lov
        {
            get { return _lov; }
            set { _lov = value; Model.RegisterTypedReference(this); }
        }

        public IEaAttribute EaAttribute { get; private set; }

        private void InitFromData(BusinessAttribute attr, BmideData data)
        {
            Name = attr.Name;
            Description = attr.Description;
            InitialValue = attr.InitialValue;
            IsArray = attr.IsArray;
            LowerBound = string.IsNullOrEmpty(attr.LowerBound) ? null : attr.LowerBound;
            UpperBound = string.IsNullOrEmpty(attr.UpperBound) ? null : attr.UpperBound;
            MaxStringLength = attr.MaxStringValue;
            Type = GetPropertyTypeName(attr);
            TypedRefClassName = attr.TypedRefClassName;
            PropertyType = attr.ProperyType;

            var localizationKey = "Property{::}" + Container.Name + "{::}" + Name;
            if (data.Localizations.ContainsKey(localizationKey))
                Alias = data.Localizations[localizationKey].Text;

            if (string.IsNullOrEmpty(TypedRefClassName) == false)
                Model.RegisterTypedReference(this);
        }

        private void InitFromData(ModelClassLike lov, TcLOVValue value, BmideData data)
        {
            Container = lov;
            var localizationKey = @"LOVValue{::}" + lov.Name + "{::}" + value.lovValue;
            Name = value.lovValue;
            if (data.Localizations.ContainsKey(localizationKey))
                Alias = data.Localizations[localizationKey].Text;
                
            localizationKey = @"LOVValueDescription{::}" + lov.Name + "{::}" + value.lovValue;
            Description = value.lovValueDescription;
            if (data.Localizations.ContainsKey(localizationKey))
                Description += " [" + data.Localizations[localizationKey].Text + "]";

            IsArray = false;
            Type = "String";
        }

        private void InitFromModel(ModelAttribute attr)
        {
            Name = attr.Name;
            Alias = attr.Alias;
            Description = attr.Description;
            InitialValue = attr.InitialValue;
            IsArray = attr.IsArray;
            LowerBound = attr.LowerBound;
            UpperBound = attr.UpperBound;
            MaxStringLength = attr.MaxStringLength;
            Type = attr.Type;
            TypedRefClassName = attr.TypedRefClassName;
            PropertyType = attr.PropertyType;
        }
        
        private string GetPropertyTypeName(BusinessAttribute attr)
        {
            string result;
            if (attr.Type.Equals("POM_string") || attr.Type.Equals("PROP_string"))
                result = "String[" + MaxStringLength + "]";
            else if (attr.Type.Equals("POM_date") || attr.Type.Equals("PROP_date"))
                result = "Date";
            else if (attr.Type.Equals("POM_int") || attr.Type.Equals("PROP_int"))
                result = "Int";
            else if (attr.Type.Equals("POM_double") || attr.Type.Equals("PROP_double"))
                result = "Double";
            else if (attr.Type.Equals("POM_logical") || attr.Type.Equals("PROP_logical"))
                result = "Boolean";
            else 
                result = attr.Type;

            if (attr.IsArray)
                result = "Array<" + result + ">";

            return result;
        }

        public override string ToString()
        {
            return Name + " [" + Type + "]";
        }

        public void Align(IProgress<ProgressInfo> progressIndicator, Dictionary<string, IEaAttribute> eaAttributes)
        {
            var template = Container.Template;
            var className = Container.Name;
            var eaClass = Container.EaClass;
            
            progressIndicator?.Report(new ProgressInfo(template.Name, $"{className}.{Name}", false));
            if (eaAttributes.ContainsKey(Name))
                EaAttribute = eaAttributes[Name];
            
            if (EaAttribute == null)
            {
                EaAttribute = eaClass.AddAttribute(Name, Type);
                EaAttribute.Update();
                eaClass.Update();
                
                Model.logNew($"New attribute: {template.Name}::{className}::{Name}::{Type}");
                Status = EStatus.New;
                Container.ReportUpdate("added attribute <" + Name + ">", progressIndicator);
            }

            if (EaAttribute != null)
            {
                ReportUpdate(Update( "Type", EaAttribute.Type, Type, x => { EaAttribute.Type = x; }), progressIndicator);
                ReportUpdate(Update( "Notes", EaAttribute.Notes, Description, x => { EaAttribute.Notes = x; }), progressIndicator);
                ReportUpdate(Update( "Default", EaAttribute.Default, InitialValue, x => {EaAttribute.Default = x;}), progressIndicator);
                ReportUpdate(Update( "Visibility", EaAttribute.Visibility, Visibility, x => {EaAttribute.Visibility = x;}), progressIndicator);
                if (Alias != null)
                    ReportUpdate(Update( "Alias", EaAttribute.Alias, Alias, x => {EaAttribute.Alias = x;}), progressIndicator);
                
                             
                if (Type.Equals("Int"))
                {
                    ReportUpdate(Update( "UpperBound", EaAttribute.UpperBound, UpperBound, x => {EaAttribute.UpperBound = x;}), progressIndicator);
                    ReportUpdate(Update( "LowerBound", EaAttribute.LowerBound, LowerBound, x => {EaAttribute.LowerBound = x;}), progressIndicator);
                }

                if (String.IsNullOrEmpty(PropertyType) == false)
                {    
                    ReportUpdate(Update( "Stereotype", EaAttribute.Stereotype, PropertyType, x => {EaAttribute.Stereotype = x;}), progressIndicator);
                }


                EaAttribute.Update();
            }
        }

        public override void ReportUpdate(string msg, IProgress<ProgressInfo> progressIndicator)
        {
            if (Status != EStatus.New && String.IsNullOrEmpty(msg) == false)
            {
                Container.ReportUpdate("attribute <" + ToString() + "> " + msg, progressIndicator);    
            }
        }

        public ModelRelation CreateRelation()
        {
            ModelRelation result = null;
            if(string.IsNullOrEmpty(TypedRefClassName) == false)
                result = new ModelRelation(Model, this);

            if (Lov != null)
            
                result = new ModelRelation(Model, this, Lov);
            return result;
        }
    }
}