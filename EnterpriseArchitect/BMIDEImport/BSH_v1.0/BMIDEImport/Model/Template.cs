﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Windows.Forms;
using BMIDEImport.EaInterface;
using BMIDEImport.Template;
using BMIDEImport.Template.Model;
using EA;
using Microsoft.Win32;

namespace BMIDEImport.Model
{
    /// <summary>
    /// TcClass to represent a bmide template
    /// </summary>
    public class Template : ModelPart
    {
        /// <summary>
        /// ctor: generate a Template from XML data
        /// </summary>
        /// <param name="model"></param>
        /// <param name="data"></param>
        public Template(Model model, BmideData data, bool master = false) : base(model)
        {
            InitFromData(data, master);
        }

        public Package LovPackage { get; set; }
        public Package ClassicPackage { get; set; }
        public Package BatchPackage { get; set; }
        public Package DynamicPackage { get; set; }
        
        private void InitFromData(BmideData data, bool master)
        {
            Name = data.Includes.Name;
            Model.AddTemplate(Name, this);

            Dependencies = new List<Template>();
            foreach (var dep in data.Dependencies)
                Dependencies.Add(Model.CreateOrGetTemplate(dep.Includes.Name, dep));

            MainPackage = new Package(Model, null, Name, this);
            LovPackage = MainPackage.CreateOrGetSubPackage("LOVs");
            ClassicPackage = LovPackage.CreateOrGetSubPackage("Classic");
            BatchPackage = LovPackage.CreateOrGetSubPackage("Batch");
            DynamicPackage = LovPackage.CreateOrGetSubPackage("Dynamic");
            
            if (master)
            {
                foreach (var lov in data.BusinessData.Definitions.ListofValues)
                    CreateOrGetLov(lov, data);
                foreach (var lov in data.BusinessData.Definitions.DynamicListOfValues)
                    CreateOrGetLov(lov, data);    
                foreach (var cls in data.MergedData)
                    if (cls.Value.className != null)
                        CreateOrGetClass(cls.Value, data);
            }    
        }



        public ModelLov CreateOrGetLov(TcLOV lov, BmideData data)
        {
            ModelLov result = null;
            if (lovs.ContainsKey(lov.lovName))
                result = lovs[lov.lovName];
            else
            {
                result = new ModelLov(Model, this, lov, data);
                lovs.Add(lov.lovName, result);
            }
            return result;
        }

        public ModelLov CreateOrGetLov(TcLovDynamic lov, BmideData data)
        {
            ModelLov result = null;
            if (lovs.ContainsKey(lov.lovName))
                result = lovs[lov.lovName];
            else
            {
                result = new ModelLov(Model, this, lov, data);
                lovs.Add(lov.lovName, result);
            }
            return result;
        }
        
        public ModelClass CreateOrGetClass(MergedBusinessDefintions cls, BmideData data)
        {
            ModelClass result = null;

            if (cls == null)
                return null;
            
            if (classes.ContainsKey(cls.className))
                result = classes[cls.className];
            else
            {
                if (cls.Container == data)
                {
                    result = new ModelClass(Model, cls, data, this);
                    classes.Add(result.Name, result);
                }
                else
                {
                    BmideData classContainer = cls.Container;
                    Template containerTemplate =
                        Model.CreateOrGetTemplate(classContainer.Includes.Name, classContainer);
                    result = containerTemplate.CreateOrGetClass(cls, classContainer);
                }
            }
            return result;
        }

        public IEnumerable<ModelLov> Lovs => lovs.Values;

        public ModelLov GetLov(string name)
        {
            ModelLov result = lovs.ContainsKey(name) ? lovs[name] : null;
            if (result == null)
            {
                foreach (var dep in Dependencies)
                {
                    result = dep.GetLov(name);
                    if (result != null)
                        break;
                }
            }
            return result;
        }

        public IEnumerable<ModelClass> Classes => classes.Values;

        public ModelClass CreateOrGetClass(string typeName, BmideData data)
        {
            if (string.IsNullOrEmpty(typeName) || typeName.Equals("*"))
                return null;
            var source = data.LocateType(typeName);
            if (source == null)
                return null;
            return CreateOrGetClass(source, data); 
        }
        
        
        public Package MainPackage { get; private set; }

        public string Name { get; private set; }
        public List<Template> Dependencies { get; private set; }
        
        private Dictionary<string, ModelClass> classes = new Dictionary<string, ModelClass>();
        private Dictionary<string, ModelClass> depClasses = new Dictionary<string, ModelClass>();
        private Dictionary<string, ModelLov>   lovs = new Dictionary<string, ModelLov>();


        public ModelClass GetClass(string name)
        {
            ModelClass result = classes.ContainsKey(name) ? classes[name] : null;
            if (result == null)
            {
                result = depClasses.ContainsKey(name) ? depClasses[name] : null;
                if (result == null)
                {
                    foreach (var dep in Dependencies)
                    {
                        result = dep.GetClass(name);
                        if (result != null)
                        {
                            depClasses[name] = result;
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public override string ToString()
        {
            return Name;
        }

        public void Align(EaInterface.IEaPackage package, IProgress<ProgressInfo> progressIndicator)
        {
            foreach (var lov in lovs.Values)
            {
                lov.Align(package, progressIndicator);
            }
            foreach (var cls in classes.Values)
            {
                cls.Align(package, progressIndicator);
            }
        }

        public int CountElements()
        {
            return CountClasses() + MainPackage.Count;
        }


        public override int CountClasses()
        {
            int n = 0;
            foreach (var cls in classes.Values)
                n += cls.CountClasses();
            foreach (var lov in lovs.Values)
                n += lov.CountClasses();
            return n;
        }

        public Template LocateTemplate(IEaPackage eaPackage, EaInterface.IEaPackage rootPackage, IProgress<ProgressInfo> progressIndicator)
        {
            while (true)
            {
                if (eaPackage == null) 
                    return null;

                if (eaPackage.Id == MainPackage.EaPackage.Id) return this;

                foreach (var dep in Dependencies)
                {
                    dep.MainPackage.Align(rootPackage, progressIndicator);
                    if (dep.MainPackage.EaPackage.Id == eaPackage.Id) return dep;
                }

                eaPackage = eaPackage.Parent;
            }
        }
    }
}