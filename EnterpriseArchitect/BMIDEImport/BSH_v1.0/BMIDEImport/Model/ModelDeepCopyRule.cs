
using System.Web.Services.Description;
using BMIDEImport.Template.Model;

namespace BMIDEImport.Model
{
    public class ModelDeepCopyRule
    {
        public enum RuleKind
        {
            Revise, 
            SaveAs
        }
        public ModelDeepCopyRule(TcDeepCopyRule rule)
        {
            Primary = rule.Type;
            Secondary = rule.ObjectTypeName;
            Relation = rule.PropertyName;
            
            Kind = rule.OperationName.Equals("ITEM_copy_rev") ? RuleKind.Revise : RuleKind.SaveAs;
            Operation = rule.CopyType;
        }

        public RuleKind Kind { get; set; }

        public string Relation { get; set; }

        public string Secondary { get; set; }

        public string Operation { get; set; }
    
        public string Primary { get; private set; }

        public override string ToString()
        {
            return Primary + " --[" + Relation + "]--> " + Secondary;
        }
    }
}