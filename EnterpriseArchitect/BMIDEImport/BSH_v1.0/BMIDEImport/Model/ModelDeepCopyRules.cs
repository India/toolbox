using System.Collections.Generic;
using System.Linq;
using BMIDEImport.Template.Model;

namespace BMIDEImport.Model
{
    public class ModelDeepCopyRules
    {
        private List<ModelDeepCopyRule> _rules = new List<ModelDeepCopyRule>();

        public ModelDeepCopyRules(IEnumerable<TcDeepCopyRule> bmideRules)
        {
            AddRules(bmideRules);
        }

        public void AddRules(IEnumerable<TcDeepCopyRule> bmideRules)
        {
            foreach (var rule in bmideRules)
            {
                _rules.Add(new ModelDeepCopyRule(rule));                
            }
        }

        public void Merge(ModelDeepCopyRules toMerge)
        {
            foreach (var modelDeepCopyRule in toMerge._rules) 
                _rules.Add(modelDeepCopyRule);
        }

        public ModelDeepCopyRule FindRule(ModelRelation relation, ModelDeepCopyRule.RuleKind kind)
        {
            ModelDeepCopyRule result = null;

            if (relation.Relation == null)
                return null;

            var primary = relation.Primary as ModelClass;
            var operationFiltered = _rules.Where(r => r.Kind == kind).ToList();

            if (operationFiltered.Any() == false) 
                return null;

            while (result == null)
            {
                var primaryFiltered = operationFiltered.Where(r => r.Primary.Equals(primary == null ? "*" : primary.Name)).ToList();

                if (primaryFiltered.Any())
                {
                    var sec = relation.Secondary as ModelClass;
                    while (result == null)
                    {
                        var secondaryFiltered =
                            primaryFiltered.Where(r => r.Secondary.Equals(sec == null ? "*" : sec.Name)).ToList();

                        if (secondaryFiltered.Any())
                        {
                            var rel = relation.Relation;
                            while (result == null)
                            {
                                var relationFiltered =
                                    secondaryFiltered.Where(r => r.Relation.Equals(rel == null ? "*" : rel.Name)).ToList();
                                if (relationFiltered.Any())
                                    result = relationFiltered.First();

                                if (result == null)
                                {
                                    if (rel == null)
                                        break;
                                    rel = rel.Parent;
                                }
                            }

                        }

                        if (result == null)
                        {
                            if (sec == null)
                                break;
                            sec = sec.Parent;
                        }
                    }
                }

                if (result == null)
                {
                    if (primary == null)
                        break;
                    primary = primary.Parent;
                }
            }

            return result;
        }

        public IEnumerable<ModelDeepCopyRule> FindRules(ModelClass primary)
        {
            return _rules.Where(r => r.Primary.Equals(primary == null ? "*" : primary.Name)).ToList();
        }
    }
}