﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using BMIDEImport.Template;

namespace BMIDEImport.Model
{
    /// <summary>
    /// Central Model class - will interpret core XML Data to class like structure.
    /// This will be mapped against EA to find out work to do
    /// </summary>
    public class Model
    {
        /// <summary>
        /// Returns the Main template to import
        /// </summary>
        public Template MainTemplate { get; private set; }

        public BmideData Data { get; private set; }

        private Dictionary<String, Template> templates = new Dictionary<string, Template>();
        private HashSet<ModelAttribute> typedReferences = new HashSet<ModelAttribute>();

        /// <summary>
        /// ctor - create central Model object based on BMDE Structure
        /// </summary>
        /// <param name="data"></param>
        public Model(BmideData data)
        {
            Data = data;
            MainTemplate = new Template(this, data, true);
            ReadRelations(data);
            CreateDCRRelations(data.DeepCopyRules);
        }

        private void CreateDCRRelations(ModelDeepCopyRules dataDeepCopyRules)
        {
            foreach (var cls in MainTemplate.Classes)
            {
                cls.CreateDcrRelations(this, dataDeepCopyRules);
            }
        }

        public void RegisterTypedReference(ModelAttribute attr)
        {
            typedReferences.Add(attr);
        }

        private void ReadRelations(BmideData data)
        {
            do
            {
                HashSet<ModelAttribute> typedRefsCopy = new HashSet<ModelAttribute>(typedReferences);
                typedReferences.Clear();
                foreach (var attrbute in typedRefsCopy)
                {
                    attrbute.CreateRelation();
                }
            } while (typedReferences.Count > 0);

            foreach (var grm in data.BusinessData.Definitions.GrmInfo)
            {
                if (grm.GRMprimaryTypeName.Equals("*") == false && grm.GRMsecondaryCardinality.Equals("0")==false)
                {
                    ModelRelation.CreateRelation(this, data, grm);
                }
            }
        }

        /// <summary>
        /// Method to find and optionally create a template object. Template classes will regsiter themselves 
        /// as soon as possible (when they know their name)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dep"></param>
        /// <returns></returns>
        public Template CreateOrGetTemplate(string name, BmideData dep)
        {
            Template result = null;
            if (templates.ContainsKey(name))
                result = templates[name];
            else
                result = new Template(this, dep);
            return result;
        }

        /// <summary>
        /// Called by Templates to register themselves
        /// </summary>
        /// <param name="name"></param>
        /// <param name="template"></param>
        /// <exception cref="InvalidEnumArgumentException"></exception>
        public void AddTemplate(string name, Template template)
        {
            if (MainTemplate == null)
                MainTemplate = template;
            
            if(templates.ContainsKey(name))
                throw new InvalidEnumArgumentException(name="template");
            else
                templates.Add(name, template);
        }

        public void logNew(string text)
        {
            //TODO: implement
        }
        
        public void logWarning(string format)
        {
            //TODO: implement
        }

        public async Task Align(EaInterface.IEaPackage package, IProgress<ProgressInfo> progressIndicator)
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                int count = MainTemplate.CountElements();

                progressIndicator?.Report(new ProgressInfo("Update Classes", count));
                MainTemplate.Align(package, progressIndicator);
                progressIndicator?.Report(new ProgressInfo("Finished!"));
            });
        }

        private int CountElements(Template template = null, HashSet<Template> templateSet = null)
        {
            int result = 0;
            if (template == null)
                template = MainTemplate;
            if(templateSet == null)
                templateSet = new HashSet<Template>();

            if (templateSet.Contains(template) == false)
            {
                templateSet.Add(template);
                result = template.CountElements();
                foreach (var dep in template.Dependencies)
                {
                    result += CountElements(dep, templateSet);
                }
            }

            return result;
        }

 
    }
}