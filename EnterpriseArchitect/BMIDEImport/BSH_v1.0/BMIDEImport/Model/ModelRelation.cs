﻿using System;
using System.Collections.Generic;
using BMIDEImport.EaInterface;
using BMIDEImport.Template;
using BMIDEImport.Template.Model;
using EA;

namespace BMIDEImport.Model
{
    
    public class ModelRelation : ModelPart
    {
        public enum ConnectorType
        {
            None,
            Aggregation,
            Composite,
            Generalization,
            DeepCopyRule
        }

        private const string StereotypeUntypedref = "Untyped Reference";
        private const string StereotypeTypedref = "Typed Reference";
        private const string StereotypeUntypedrel = "Untyped Relation";
        private const string StereotypeLov = "LOV Attachment";

        public virtual string Name
        {
            get
            {
                if (Attribute != null)
                {
                    return Attribute.Name;
                }
                else if (Relation != null)
                {
                    return Relation.Name;
                }
                else
                {
                    return "";
                }
            }
        }
        
        public ModelDeepCopyRule DeepCopyRuleRevise { get; private set; }
        public ModelDeepCopyRule DeepCopyRuleSaveAs { get; private set; }

        public int SecondaryCardinality { get; private set; }
        public int PrimaryCardinality { get; private set; }
        public string Changeability { get; private set; }
        public string Detachability { get; private set; }
        public string Attachability { get; private set; }
        public ModelClass Relation { get; set; }
        public ModelClassLike Secondary { get; private set; }
        public ModelClassLike Primary { get; private set; }
        public ModelAttribute Attribute { get; private set; }
        public bool IsDcr { get; private set; } = false;

        public static void CreateRelation(Model model, BmideData data, ModelDeepCopyRule dcr)
        {
            var primary = model.MainTemplate.CreateOrGetClass(dcr.Primary, data);
            if (primary == null)
                return;
            var secondary = model.MainTemplate.CreateOrGetClass(dcr.Secondary, data);
            if (secondary == null)
                return;
            var relation = model.MainTemplate.CreateOrGetClass(dcr.Relation, data);
            if (relation == null)
                return;

            var existingRelation = primary.FindRelation(secondary, relation);
            if (existingRelation == null)
                new ModelRelation(model, dcr, primary, secondary, relation);
            else
                existingRelation.addDcr(dcr);
        }

        public static void CreateRelation(Model model, BmideData data, TcGRMInfo tcGrm)
        {
            var primary = model.MainTemplate.CreateOrGetClass(tcGrm.GRMprimaryTypeName, data);
            if (primary == null)
                return;
            var secondary = model.MainTemplate.CreateOrGetClass(tcGrm.GRMsecondaryTypeName, data);
            if (secondary == null)
                return;
            var relation = model.MainTemplate.CreateOrGetClass(tcGrm.GRMrelationTypeName, data);
            if (relation == null)
                return;
            new ModelRelation(model, tcGrm, primary, secondary, relation);
        }
        
        
        public ModelRelation(Model model, ModelDeepCopyRule dcr, ModelClass primary, ModelClass secondary,
            ModelClass relation) : base(model)
        {
            
            InitFromData(dcr, primary, secondary, relation);
        }

        public ModelRelation(Model model, TcGRMInfo tcGrm, ModelClass primary, ModelClass secondary,
            ModelClass relation) : base(model)
        {
            
            InitFromData(tcGrm, model.Data.DeepCopyRules, primary, secondary, relation);
        }

        public ModelRelation(Model model, ModelAttribute attribute) : base(model)
        {
            InitFromModel(attribute, true);
        }

        public ModelRelation(Model model, ModelAttribute attribute, ModelLov primary) : base(model)
        {
            InitFromModel(attribute, false);
        }
        
        public ModelRelation(Model model, ModelLov primary, ModelAttribute attribute, ModelLov secondary) : base(model)
        {
            InitFromModel(primary, attribute, secondary);
        }

        protected ModelRelation(Model model, ModelClass primary, ModelClass secondary, int primaryCardinality,
            int secondaryCardinality, ConnectorType aggType) : base(model)
        {
            Primary = primary;
            Secondary = secondary;
            PrimaryCardinality = primaryCardinality;
            SecondaryCardinality = secondaryCardinality;
            Connector = aggType;
            Aligned = false;

            Primary?.AddRelation(this);
            (Secondary as ModelClass)?.AddRelation(this);
        }

        private void InitFromModel(ModelAttribute attribute, bool asTypedReference)
        {
            Aligned = false;
            if (attribute.Container is ModelClass == false)
                throw new ArgumentException();

            var primary = (ModelClass) attribute.Container;
            Primary = primary;
            if (asTypedReference)
                Secondary = Model.MainTemplate.CreateOrGetClass(attribute.TypedRefClassName, Model.Data);
            else
                Secondary = attribute.Lov;

            PrimaryCardinality = -1;
            SecondaryCardinality = attribute.IsArray ? -1 : 1;
            Attribute = attribute;
            Connector = asTypedReference ? ConnectorType.Aggregation : ConnectorType.None;
            Primary?.AddRelation(this);
        }
        
        
        private void InitFromModel(ModelLov primary, ModelAttribute attribute, ModelLov secondary)
        {
            Aligned = false;

            Primary = primary;
            Secondary = secondary;
            Attribute = attribute;

            PrimaryCardinality = -1;
            SecondaryCardinality = 1;
            Connector = ConnectorType.None;
            
            Primary?.AddRelation(this);
        }

        public ConnectorType Connector { get; private set; }
        
        private void InitFromData(ModelDeepCopyRule dcr,ModelClass primary, ModelClass secondary, ModelClass relation)
        {
            IsDcr = true;
            Aligned = false;
            Primary = primary;
            Secondary = secondary;
            Relation = relation;

            PrimaryCardinality = -1;
            SecondaryCardinality = -1;

            addDcr(dcr);
            
            this.Connector = ConnectorType.DeepCopyRule;
            primary?.AddRelation(this);
        }
        
        private void addDcr(ModelDeepCopyRule dcr)
        {
            if (dcr.Kind == ModelDeepCopyRule.RuleKind.Revise)
                DeepCopyRuleRevise = dcr;
            else
                DeepCopyRuleSaveAs = dcr;
        }
        
        private void InitFromData(TcGRMInfo tcGrm, ModelDeepCopyRules rules, ModelClass primary, ModelClass secondary, ModelClass relation)
        {
            Aligned = false;
            Primary = primary;
            Secondary = secondary;
            Relation = relation;

            Attachability = tcGrm.GRMattachability;
            Detachability = tcGrm.GRMdetachability;
            Changeability = tcGrm.GRMchangeability;
            PrimaryCardinality = int.Parse(tcGrm.GRMprimaryCardinality);
            SecondaryCardinality = int.Parse(tcGrm.GRMsecondaryCardinality);

            DeepCopyRuleRevise = rules.FindRule(this, ModelDeepCopyRule.RuleKind.Revise);
            DeepCopyRuleSaveAs = rules.FindRule(this, ModelDeepCopyRule.RuleKind.SaveAs);

            primary?.AddRelation(this);
        }



        public override string ToString()
        {
            var result = "";

            if (Primary is ModelLov)
                result = "Cascading LOV " + GetName(Primary) + " [" + Attribute?.Name + "] => " + GetName(Secondary);
            else
            if (Attribute != null)
            {
                result = GetName(Primary) + "." + Attribute.ToString();
            }
            else if (Relation != null)
            {
                result = GetName(Primary)
                         + "[" + PrimaryCardinality + "]"
                         + " --[" + GetName(Relation) + "]{" + StereoType + "} -->"
                         + GetName(Secondary)
                         + "[" + SecondaryCardinality + "]";
            }


            return result;
        }

        protected static string GetName(ModelClassLike cls)
        {
            return cls == null ? "*" : cls.Name;
        }

        protected override string UpdatePrefix => "Update Relation " + ToString();

        public virtual void Align(IEaPackage rootPackage, IProgress<ProgressInfo> progressIndicator)
        {
            if (Aligned)
                return;

            Aligned = true;
            if (Primary == null || Secondary == null)
                return;
            if (PrimaryCardinality == 0 || SecondaryCardinality == 0)
                return;

            Primary.Align(rootPackage, progressIndicator);
            Secondary.Align(rootPackage, progressIndicator);
            Relation?.Align(rootPackage, progressIndicator);

            this.EaConnector = CreateOrUpdateConnector(progressIndicator);
            Aligned = this.EaConnector != null;
        }

        public bool Aligned { get; protected set; }

        public string UpdateTaggedValues(string key, string value)
        {
            string result = "";

            var taggedValues = EaConnector.TaggedValues;

            EaInterface.IEaConnectorTag tval = null;
            foreach (var tag in taggedValues)
            {
                if (tag == null)
                    break;
                if (tag.Name.Equals(key))
                {
                    tval = tag;
                    break;
                }
            }


            if (tval == null)
            {
                var newVal = EaConnector.AddTaggedValue(key, value);
                EaConnector.Update();

                if (newVal != null)
                {
                    newVal.Update();
                    tval = newVal;

                    result = $"New {key}: {value}";
                    if (Status != EStatus.New)
                        Status = EStatus.Updated;
                }
                else
                {
                    newVal = null; // HÄH?
                }
            }
            else
            {
                var current = tval.Value;
                if (value == null)
                    value = "";

                if (current.Equals(value) == false)
                {
                    tval.Value = value;
                    tval.Update();

                    result = $"Update {key}: '{current}' -> '{value}'";
                    if (Status != EStatus.New)
                        Status = EStatus.Updated;
                }
            }


            return result;
        }
       
        public string UpdateConstraint(string key, string value, string type)
        {
            string result = "";

            var constaints = EaConnector.Constraints;

            EaInterface.IEaConnectorConstraint constraint = null;
            foreach (var cc in constaints)
            {
                if (cc == null)
                    break;
                if (cc.Name.Equals(key))
                {
                    constraint = cc;
                    break;
                }
            }


            if (constraint == null)
            {
                var newVal = EaConnector.AddConstraint(key, value, type);
                EaConnector.Update();

                if (newVal != null)
                {
                    newVal.Update();
                    constraint = newVal;

                    result = $"New {key}: {value}";
                    if (Status != EStatus.New)
                        Status = EStatus.Updated;
                }
                else
                {
                    newVal = null; // HÄH?
                }
            }
            else
            {
                var current = constraint.Notes;
                if (value == null)
                    value = "";

                if (current.Equals(value) == false)
                {
                    constraint.Notes = value;
                    constraint.Update();

                    result = $"Update {key}: '{current}' -> '{value}'";
                    if (Status != EStatus.New)
                        Status = EStatus.Updated;
                }
            }


            return result;
        }

        public IEaConnector EaConnector { get; set; }

        public override string StereoType
        {
            get
            {
                string result = null;

                if (IsDcr)
                    result = "DCR";
                else
                    if (Relation != null)
                        result = StereotypeUntypedrel;

                if(Primary is ModelLov)
                    result = StereotypeLov;
                else
                    if (Attribute != null)
                    {
                        if (Attribute.Type.Contains(ModelAttribute.TypeTypedref))
                        {
                            result = StereotypeTypedref;
                        }
                        else if (Attribute.Type.Contains(ModelAttribute.TypeUnTypedref))
                        {
                            result = StereotypeUntypedref;
                        }
                        else
                        {
                            result = StereotypeLov;
                        }
                    }

                return result;
            }
        }

        public override bool Equals(object obj)
        {
            var other = obj as ModelRelation;
            if (other != null)
            {
                return other == this ||
                    other.Primary == Primary && other.Secondary == Secondary && other.Relation == Relation;
            }
            return base.Equals(obj);
        }
        
        private IEaConnector CreateOrUpdateConnector(IProgress<ProgressInfo> progressIndicator)
        {
            var primary = Primary.EaClass;
            var secondary = Secondary.EaClass;
            var aggType = Connector;
            var name = Name;
            IEaConnector connector = null;

            if (primary != null && secondary != null)
            {
                connector = LocateConnector();
                if (connector == null)
                {
                    Status = ModelPart.EStatus.New;
                    connector = CreateConnector();
                    progressIndicator?.Report(new ProgressInfo("Add relation " + ToString()));
                }

                EaConnector = connector;

                UpdateConnector(progressIndicator);
            }

            return connector;
        }
        
        private void UpdateConnector(IProgress<ProgressInfo> progressIndicator)
        {
                           
            ReportUpdate(Update( "PrimaryCardinality", 
                EaConnector.ClientEnd.Cardinality, 
                TranslateCardinality(PrimaryCardinality), x => { EaConnector.ClientEnd.Cardinality = x; }), progressIndicator);
            ReportUpdate(Update( "SecondaryCardinality", 
                EaConnector.SupplierEnd.Cardinality, 
                TranslateCardinality(SecondaryCardinality), x => { EaConnector.SupplierEnd.Cardinality = x; }), progressIndicator);

            UpdateStereotype(EaConnector, this, progressIndicator);
            
            if (Attribute != null)
            {
                var style = "LFSP=" + Attribute.EaAttribute.AttributeGuid + "R;";
                EaConnector.StyleEx = style;

                EaConnector.Name = ""; // reset name, which might be set on creation
                ReportUpdate(
                    Update("Role", EaConnector.ClientEnd.Role, Attribute.Name, x => { EaConnector.ClientEnd.Role = x; }),
                    progressIndicator);

                if (string.IsNullOrEmpty(Attribute.Alias) == false)
                {
                    ReportUpdate(
                        Update("Alias", EaConnector.ClientEnd.Alias, Attribute.Alias, x => { EaConnector.ClientEnd.Alias = x; }),
                        progressIndicator);
                }
            }

            if (Relation != null)
            {
                EaConnector.Name = ""; // reset name, which might be set on creation
                ReportUpdate(
                    Update("Role", EaConnector.ClientEnd.Role, Relation.Name,
                        x => { EaConnector.ClientEnd.Role = x; }),
                    progressIndicator);
                if (string.IsNullOrEmpty(Relation.Alias) == false)
                {
                    ReportUpdate(
                        Update("Alias", EaConnector.ClientEnd.Alias, Relation.Alias,
                            x => { EaConnector.ClientEnd.Alias = x; }),
                        progressIndicator);
                }

                var created = EaConnector.LinkTo(Relation.EaClass);
                if(created)
                    ReportUpdate("Create Relation Link for " + this.ToString(), progressIndicator);
            }

            switch (Connector)
            {
                case ConnectorType.None:
                case ConnectorType.DeepCopyRule:
                    if(Attachability != null) {
                        ReportUpdate(UpdateConstraint("Att["+ Attachability + "]", "", "Pre-condition"), progressIndicator);
                        
                        foreach(var cons in EaConnector.Constraints )
                        {
                            var test = cons.Name;
                            var test2 = cons.Notes;
                            var test3 = cons.Type;
                            var x = test + test2;
                        }
                    }
                    if(Detachability != null)
                        ReportUpdate(UpdateConstraint("Det[" + Detachability + "]", "", "Pre-condition"), progressIndicator);
                    if(Changeability != null)
                        ReportUpdate(UpdateConstraint("Chg[" + Changeability + "]", "", "Pre-condition"), progressIndicator);

                    if (DeepCopyRuleRevise != null)
                    {
                        ReportUpdate(UpdateConstraint("Revise[" + DeepCopyRuleRevise.Operation + "]", "OnRevise = " + DeepCopyRuleRevise.ToString(), "Post-condition"), progressIndicator);
                    }
                    if (DeepCopyRuleSaveAs != null)
                    {
                        ReportUpdate(UpdateConstraint("SaveAs[" + DeepCopyRuleSaveAs.Operation + "]", "OnSaveAs = " + DeepCopyRuleSaveAs.ToString(), "Post-condition"), progressIndicator);
                    }
                    //UpdateDirection(connector, data, progressIndicator);
                    ReportUpdate(Update("Primary Navigability",
                        EaConnector.ClientEnd.IsNavigable,
                        false, x => { EaConnector.ClientEnd.IsNavigable = x;}), progressIndicator);
                    ReportUpdate(Update("Secondary Navigability",
                        EaConnector.SupplierEnd.IsNavigable,
                        true, x => { EaConnector.SupplierEnd.IsNavigable = x;}), progressIndicator);
                    EaConnector.ClientEnd.Update();
                    EaConnector.SupplierEnd.Update();
                    EaConnector.Update(); 
                    break;
                case ConnectorType.Aggregation:
                    ReportUpdate(Update("Connector",
                        EaConnector.ClientEnd.Aggregation,
                        1, x => { EaConnector.ClientEnd.Aggregation = x; }), progressIndicator);
                    ReportUpdate(Update("Primary Navigability",
                        EaConnector.ClientEnd.Navigable,
                        "Unspecified", x => { EaConnector.ClientEnd.Navigable = x;}), progressIndicator);
                    ReportUpdate(Update("Secondary Navigability",
                        EaConnector.SupplierEnd.Navigable,
                        "Navigable", x => { EaConnector.SupplierEnd.Navigable = x;}), progressIndicator);
                    EaConnector.ClientEnd.Update();
                    EaConnector.SupplierEnd.Update();
                    EaConnector.Update();  
                    break;
                case ConnectorType.Composite:
                    ReportUpdate(Update("Connector",
                        EaConnector.ClientEnd.Aggregation,
                        2, x => { EaConnector.ClientEnd.Aggregation = x; }), progressIndicator);
                    ReportUpdate(Update("Primary Navigability",
                        EaConnector.ClientEnd.Navigable,
                        "Unspecified", x => { EaConnector.ClientEnd.Navigable = x;}), progressIndicator);
                    ReportUpdate(Update("Secondary Navigability",
                        EaConnector.SupplierEnd.Navigable,
                        "Navigable", x => { EaConnector.SupplierEnd.Navigable = x;}), progressIndicator);
                    EaConnector.ClientEnd.Update();
                    EaConnector.SupplierEnd.Update();
                    EaConnector.Update();  
                    break;
                case ConnectorType.Generalization:
                    break;        
                default:
                    throw new ArgumentOutOfRangeException();
            }
            EaConnector.ClientEnd.Update();
            EaConnector.SupplierEnd.Update();
            EaConnector.Update();  
        }

        private  IEaConnector CreateConnector( )
        {
            var result = Primary.EaClass.AddNewConnector(Name, ConnType(Connector));

            result.SupplierId = Secondary.EaClass.ElementId;
            result.Update();

            return result;
        }
        
        private IEaConnector LocateConnector( )
        {
            EaInterface.IEaConnector result = null;
            var toDelete = new List<IEaConnector>();
            
            var connectorType = ConnType(Connector);
            var altConnectorType = AltConnType(Connector); // for DCRs may be attached to existing relations

            foreach (var conn in Primary.EaClass.Connectors)
            {
                var t1 = conn.Type;
                var t2 = conn.SupplierId;
                var t3 = Secondary.EaClass.ElementId;
                var t4 = conn.Name;
                var t5 = conn.ClientEnd.Role;
                
                if ( (conn.Type.Equals(connectorType) || conn.Type.Equals(altConnectorType))
                    && conn.SupplierId == Secondary.EaClass.ElementId
                    && (Connector == ConnectorType.Generalization || conn.Name.Equals(Name) || conn.ClientEnd.Role.Equals(Name)
                        || (string.IsNullOrEmpty(conn.Name) && string.IsNullOrEmpty(conn.ClientEnd.Role))))
                {
                    if (result != null)
                    {
                        // Duplicate detection
                        if (conn.ConnectorId < result.ConnectorId)
                        {
                            toDelete.Add(result);
                            result = conn;
                        }
                        else
                        {
                            toDelete.Add(conn);
                        }
                    }
                    else
                        result = conn;
//                    break;
                }
            }

            foreach (var conn in toDelete)
            {
                Primary.EaClass.DeleteConnector(conn);
            }

            return result;
        }

        private static string ConnType(ConnectorType type)
        {
            switch (type)
            {
                case ConnectorType.None:
                    return "Association";
                case ConnectorType.Aggregation:
                    return "Aggregation";
                case ConnectorType.Composite:
                    return "Aggregation";
                case ConnectorType.Generalization:
                    return "Generalization";
                case ConnectorType.DeepCopyRule:
                    return "CommunicationPath";
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
        
        private static string AltConnType(ConnectorType type)
        {
            switch (type)
            {
                case ConnectorType.DeepCopyRule:
                    return "Association";
                default:
                    return "";
            }
            
        }
        private static string TranslateCardinality(int cardinality)
        {
            if (cardinality == -1)
                return "*";
            return cardinality.ToString();
        }
        
        private static void UpdateStereotype(IEaConnector connector, ModelPart data, IProgress<ProgressInfo> progressIndicator)
        {
            if (string.IsNullOrEmpty(data.StereoType) == false)
            {
                data.ReportUpdate(data.Update("Stereotype",
                    connector.Stereotype,
                    data.StereoType, x => { connector.Stereotype = x; }), progressIndicator);
            }
        }
        
        #region ProgressCounting

        public override int CountClasses()
        {
            int result = 0;
            if(Secondary != null)
                result += Secondary.CountClasses();
            if (Relation != null)
                result += Relation.CountClasses();
            return result;
        }

        #endregion

    }
}