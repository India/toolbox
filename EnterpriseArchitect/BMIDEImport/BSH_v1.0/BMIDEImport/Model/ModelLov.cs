﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMIDEImport.EaInterface;
using BMIDEImport.Template;
using BMIDEImport.Template.Model;

namespace BMIDEImport.Model
{
    public enum LovKind
    {
        Classic,
        Dynamic,
        Batch
    }
    public class ModelLov : ModelClassLike
    {
        private List<ModelAttribute> entries = new List<ModelAttribute>();

        private LovKind LovKind { get; set; }
    
        public ModelLov(Model model, Template template, TcLOV lov, BmideData data) : base(model)
        {
            InitFromData(model, template, lov, data);
        }
        public ModelLov(Model model, Template template, TcLovDynamic lov, BmideData data) : base(model)
        {
            InitFromData(template, lov, data);
        }

        private void InitFromData(Model model, Template template, TcLOV lov, BmideData data)
        {
            Template = template;
            Name = lov.lovName;
            Description = lov.lovdescription;
            
            foreach (var value in lov.tcLOVValues)
            {
                entries.Add(new ModelAttribute(Model, this, value, data));
            }

            var subLovs = data.BusinessData.Definitions.SubLovs.Where(x => x.BaseLov.Equals(Name));

            foreach (var subLovData in subLovs)
            {
                var tcLov = data.BusinessData.Definitions.ListofValues
                    .First(x => x.lovName.Equals(subLovData.SubLov));
                var attr = entries.First(x => x.Name.Equals(subLovData.Value));
                    
                if (tcLov != null)
                {
                    var secondary = template.CreateOrGetLov(tcLov, data);
                    
                    new ModelRelation(model, this, attr, secondary);
                }
            }

            LovKind = lov.isManagedExternally ? LovKind.Batch : LovKind.Classic;                
        }
        
        private void InitFromData(Template template, TcLovDynamic lov, BmideData data)
        {
            Template = template;
            Name = lov.lovName;
            Description = lov.lovdescription + "\n" + "Query: " + lov.queryClause;

            LovKind = LovKind.Dynamic;
        }

        public override string ToString()
        {
            return Name + " - " + Description;
        }

        public override void Align(EaInterface.IEaPackage package, IProgress<ProgressInfo> progressIndicator)
        {
            if (EaClass != null)
                return;

            Template.LovPackage.Align(package, progressIndicator);

            progressIndicator?.Report(new ProgressInfo(Template.Name, "LOV: " + Name));
            
            IEaPackage container = null;
            switch (LovKind)
            {
                case LovKind.Classic:
                    Template.ClassicPackage.Align(package, progressIndicator);
                    container = Template.ClassicPackage.EaPackage;
                    break;
                case LovKind.Dynamic:
                    Template.DynamicPackage.Align(package, progressIndicator);
                    container = Template.DynamicPackage.EaPackage;
                    break;
                case LovKind.Batch:
                    Template.BatchPackage.Align(package, progressIndicator);
                    container = Template.BatchPackage.EaPackage;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            try
            {
                EaClass = (EaInterface.IEaElement) container.GetElementByName(Name); // Elements.GetByName(Name);
            }
            catch (Exception)
            {}
            
            if (EaClass == null)
            {
                EaClass = container.AddNewElement(Name, "Enumeration");            
                EaClass.Update();
                Model.logNew($"New Lov: {Template.Name}::{Name}");
                Status = EStatus.New;
                progressIndicator?.Report(new ProgressInfo("Add class(lov) <" + Name + "> to " + Template.LovPackage.ToString(true)));
            }

            if (EaClass != null)
            {
                var updateText = new StringBuilder();

                updateText.Append(Update( "Notes", EaClass.Notes, Description, x => { EaClass.Notes = x; }));
                
                string stereotype = StereoType ?? "";
                if(string.IsNullOrEmpty(stereotype) == false)
                    ReportUpdate(Update("Stereotype", EaClass.Stereotype, stereotype, x => { EaClass.Stereotype = x; }), progressIndicator);

                EaClass.Update();

                AlignAttributes(progressIndicator);
            }
            
            foreach (var relation in Relations)
            {
                relation.Align(package, progressIndicator);
            }

        }

        private new void AlignAttributes(IProgress<ProgressInfo> progressIndicator)
        {
            Dictionary<string, EaInterface.IEaAttribute> eaAttributes = EaClass.Attributes.ToDictionary(attr => attr.Name);

            foreach (var attr in entries)
            {
                attr.Align(progressIndicator, eaAttributes);
            }
        }

    }
}