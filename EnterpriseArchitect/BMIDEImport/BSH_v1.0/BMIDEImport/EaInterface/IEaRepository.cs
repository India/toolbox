﻿
namespace BMIDEImport.EaInterface
{
    public interface IEaRepository
    {
        EaObjectType GetTreeSelectedItemType();
        IEaPackage GetTreeSelectedPackage();
        IEaPackage GetPackageById(int eaElementPackageId);
        void Execute(string sql);
        string SqlQuery(string sql);
        IEaElement GetElementById(int eaConnectorClientId);
    }
}