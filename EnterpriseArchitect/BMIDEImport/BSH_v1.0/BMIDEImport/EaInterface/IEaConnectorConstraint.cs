namespace BMIDEImport.EaInterface
{
    public interface IEaConnectorConstraint : IEaObject
    {
        string Type { get;  }
        string Name { get; set; }
        string Notes { get; set; }
    }
}