﻿using System.Collections.Generic;

namespace BMIDEImport.EaInterface
{
    public interface IEaElement : IEaObject
    {
        string Notes { get; set; }
        
        int ElementId { get; }
        int ClassifierID { get; set; }
        string Alias { get; set; }
        string Abstract { get; set; }
        string Name { get; }

        IEnumerable<IEaAttribute> Attributes { get; }
        IEnumerable<IEaConnector> Connectors { get; }
        string Stereotype { get; set; }

        IEaAttribute AddAttribute(string name, string type);
        IEaConnector AddNewConnector(string name, string connType);
        void DeleteConnector(IEaConnector conn);

        IEaPackage Package { get; }
    
    }
}