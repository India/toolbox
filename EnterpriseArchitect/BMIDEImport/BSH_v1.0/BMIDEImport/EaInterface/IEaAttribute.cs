﻿
namespace BMIDEImport.EaInterface
{
    public enum Scope
    {
        Public,
        Protected,
        Private,
        Package
    }

    public interface IEaAttribute : IEaObject
    {
        string Name { get; set; }
        string Type { get; set; }
        string Notes { get; set; }
        string Default { get; set; }
        string Alias { get; set; }
        string UpperBound { get; set; }
        string LowerBound { get; set; }
        string Stereotype { get; set; }
        string AttributeGuid { get; set; }
        Scope Visibility { get; set; }
    }
}