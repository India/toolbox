﻿using System.Collections.Generic;

namespace BMIDEImport.EaInterface
{
    public interface IEaConnector : IEaObject
    {
        string Type { get; }
        int SupplierId { get; set; }
        string Name { get; set; }
        string Stereotype { get; set; }
        IEaConnectorEnd ClientEnd { get; }
        IEaConnectorEnd SupplierEnd { get; }
        string StyleEx { get; set; }
        IEnumerable<IEaConnectorTag> TaggedValues { get;}
        string Alias { get; set; }
        IEaConnectorTag AddTaggedValue(string key, string value);
        int ConnectorId { get; }
        bool LinkTo(IEaElement relationClass);
        
        IEnumerable<IEaConnectorConstraint> Constraints { get; }
        IEaElement Primary { get; set; }
        IEaConnectorConstraint AddConstraint(string name, string value, string type);
    }
}