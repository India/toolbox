﻿using EA;

namespace BMIDEImport.EaInterface
{
    public interface IEaFactory
    {
        IEaRepository GetRepository(Repository repository);
    }
}