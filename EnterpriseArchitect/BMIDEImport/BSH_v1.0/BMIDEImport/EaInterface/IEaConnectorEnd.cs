﻿
namespace BMIDEImport.EaInterface
{
    public interface IEaConnectorEnd : IEaObject
    {
        string Cardinality { get; set; }
        bool IsNavigable { get; set; }
        int Aggregation { get; set; }
        string Navigable { get; set; }
        string Alias { get; set; }
        string Role { get; set; }
    }
}