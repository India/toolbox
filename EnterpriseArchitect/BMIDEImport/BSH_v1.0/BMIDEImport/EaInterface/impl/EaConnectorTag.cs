﻿using EA;

namespace BMIDEImport.EaInterface.impl
{
    public class EaConnectorTag : IEaConnectorTag
    {
        private readonly ConnectorTag _eaConnectorTag;
        
        public EaConnectorTag(ConnectorTag tv)
        {
            _eaConnectorTag = tv;
        }

        public bool Update()
        {
            return _eaConnectorTag.Update();
        }

        public string Name => _eaConnectorTag.Name;
        public string Value
        {
            get { return _eaConnectorTag.Value; }
            set { _eaConnectorTag.Value = value; }
        }
    }
}