using EA;

namespace BMIDEImport.EaInterface.impl
{
    public class EaConnectorConstraint : IEaConnectorConstraint
    {
        private readonly EaFactory _eaFactory; 
        private readonly ConnectorConstraint _eaConstraint;
        
        public EaConnectorConstraint(EaFactory factory, ConnectorConstraint connector)
        {
            _eaFactory = factory;
            _eaConstraint = connector;
        }

        public bool Update()
        {
            return _eaConstraint.Update();
        }

        public string Type => _eaConstraint.Type;
        public string Name
        {
            get { return _eaConstraint.Name; }
            set { _eaConstraint.Name = value; }
        }

        public string Notes {
            get { return _eaConstraint.Notes; }
            set { _eaConstraint.Notes = value; }
            
        }
    }
}