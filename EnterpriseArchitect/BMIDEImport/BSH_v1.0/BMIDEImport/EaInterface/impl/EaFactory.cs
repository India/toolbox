﻿using System;
using System.Collections.Generic;
using System.Linq;
using EA;
using Attribute = EA.Attribute;

namespace BMIDEImport.EaInterface.impl
{
    public class EaFactory : IEaFactory
    {
        private class EaObjectCache<TEaClass, TIntfClass> where TIntfClass : class
        {
            private readonly Dictionary<TEaClass, TIntfClass> _map = new Dictionary<TEaClass, TIntfClass>();
            
            public TIntfClass GetObject(TEaClass obj, Func<TEaClass, TIntfClass> creator)
            {
                TIntfClass result = null;
                if (obj != null)
                {
                    if (_map.ContainsKey(obj) == false)
                    {
                        _map[obj] = creator(obj);
                    }
                        
                        
                    result = _map[obj];
                }
                return result;
            }

            public TIntfClass First()
            {
                return _map.First().Value;
            }
        }

        private readonly EaObjectCache<Repository, IEaRepository> _repositoryMap = new EaObjectCache<Repository, IEaRepository>();
        public IEaRepository GetRepository(Repository repository)
        {
            return _repositoryMap.GetObject(repository, o => new EaRepository(this, o));
        }

        public IEaRepository GetRepository()
        {
            return _repositoryMap.First();
        }

        private readonly EaObjectCache<Package, IEaPackage> _packageMap = new EaObjectCache<Package, IEaPackage>();
        public IEaPackage GetPackage(Package package)
        {
            return _packageMap.GetObject(package, o => new EaPackage(this, o));
        }

        private readonly EaObjectCache<Element, IEaElement> _elementMap = new EaObjectCache<Element, IEaElement>();        
        public IEaElement GetElement(Element element)
        {
            return _elementMap.GetObject(element, o => new EaElement(this, o));
        }

        private readonly EaObjectCache<Attribute, IEaAttribute> _attributeMap = new EaObjectCache<Attribute, IEaAttribute>();        
        public IEaAttribute GetAttribute(Attribute attr)
        {
            return _attributeMap.GetObject(attr, o => new EaAttribute(o));
        }

        private readonly EaObjectCache<Connector, IEaConnector> _connectorMap = new EaObjectCache<Connector, IEaConnector>();        
        public IEaConnector GetConnector(Connector connector)
        {
            return _connectorMap.GetObject(connector, o => new EaConnector(this, o));
        }

        private readonly EaObjectCache<ConnectorEnd, IEaConnectorEnd> _connectorEndMap = new EaObjectCache<ConnectorEnd, IEaConnectorEnd>();
        public IEaConnectorEnd GetConnectorEnd(ConnectorEnd connEnd)
        {
            return _connectorEndMap.GetObject(connEnd, o => new EaConnectorEnd(o));
        }

        private readonly EaObjectCache<ConnectorTag, IEaConnectorTag> _connectorTagMap = new EaObjectCache<ConnectorTag, IEaConnectorTag>();
        public IEaConnectorTag GetConnectorTag(ConnectorTag tv)
        {
            return _connectorTagMap.GetObject(tv, o => new EaConnectorTag(o));
        }

        private readonly EaObjectCache<ConnectorConstraint, IEaConnectorConstraint> _constraints = new EaObjectCache<ConnectorConstraint, IEaConnectorConstraint>();
        public IEaConnectorConstraint GetConstraint(ConnectorConstraint cons)
        {
            return _constraints.GetObject(cons, o => new EaConnectorConstraint(this, o));
        }
    }
}