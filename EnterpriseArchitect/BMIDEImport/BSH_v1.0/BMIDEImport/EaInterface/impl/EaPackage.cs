﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BMIDEImport.EaInterface.impl
{
    public class EaPackage : IEaPackage
    {
        private readonly EaFactory _eaFactory; 
        private readonly EA.Package   _eaPackage;
     
        public EaPackage(EaFactory factory, EA.Package package)
        {
            _eaFactory = factory;
            _eaPackage = package;
        }
        
        public IEaPackage CreateOrGetPackage(string name)
        {
            EA.Package result = null;
            try
            {
                result = (EA.Package) _eaPackage.Packages.GetByName(name);
            }
            catch (Exception)
            {
                // ignored
            }

            if (result == null)
            {
                result = _eaPackage.Packages.AddNew(name, "Package") as EA.Package;
                result?.Update();
            }

            return _eaFactory.GetPackage(result);
        }
        
        public int Id
        {
            get
            {
                return _eaPackage.PackageID; 
            }
        }

        public IEnumerable<IEaPackage> Packages
        {
            get
            {
                return Iterate<EA.Package>(_eaPackage.Packages.GetEnumerator()).Select(x => _eaFactory.GetPackage(x));
            }
        }

        public IEaPackage Parent => _eaFactory.GetRepository().GetPackageById(_eaPackage.ParentID);

        public bool IsModel => _eaPackage.IsModel;

        private IEnumerable<T> Iterate<T>(IEnumerator e) where T : class
        {
            while (e.MoveNext())
                yield return e.Current as T;
        }
     

        public string Name => _eaPackage.Name;

        public IEaElement AddNewElement(string name, string type)
        {
            var result = _eaPackage.Elements.AddNew(name, type) as EA.Element;
            return _eaFactory.GetElement(result);
        }

        public IEaElement GetElementByName(string name)
        {
            EA.Element result = null;
            try
            {
                result = _eaPackage.Elements.GetByName(name) as EA.Element;
            }
            catch (Exception)
            {
                // ignored
            }

            return _eaFactory.GetElement(result);
        }

        public void RefreshElements()
        {
            _eaPackage.Elements.Refresh();

        }

        public bool Update()
        {
            return _eaPackage.Update();
        }

    }
}