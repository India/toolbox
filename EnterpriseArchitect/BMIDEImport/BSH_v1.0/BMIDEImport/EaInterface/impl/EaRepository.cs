﻿using System;
using EA;

namespace BMIDEImport.EaInterface.impl
{
    public class EaRepository: IEaRepository
    {
        private readonly EaFactory  _eaFactory; 
        private readonly Repository _eaRepository;
     
        public EaRepository(EaFactory factory, Repository repository)
        {
            _eaFactory = factory;
            _eaRepository = repository;
        }

        public EaObjectType GetTreeSelectedItemType()
        {
            EaObjectType result = EaObjectType.OtElement;
            
            switch (_eaRepository.GetTreeSelectedItemType())
            {
                case ObjectType.otProject: result = EaObjectType.OtProject; break;
                case ObjectType.otRepository: result = EaObjectType.OtRepository; break;
                case ObjectType.otCollection: result = EaObjectType.OtCollection; break;
                case ObjectType.otElement: result = EaObjectType.OtElement; break;
                case ObjectType.otPackage: result = EaObjectType.OtPackage; break;
                case ObjectType.otModel: result = EaObjectType.OtModel; break;
                case ObjectType.otConnector: result = EaObjectType.OtConnector; break;
                case ObjectType.otDiagram: result = EaObjectType.OtDiagram; break;
                case ObjectType.otRequirement: result = EaObjectType.OtRequirement; break;
                case ObjectType.otScenario: result = EaObjectType.OtScenario; break;
                case ObjectType.otConstraint: result = EaObjectType.OtConstraint; break;
                case ObjectType.otTaggedValue: result = EaObjectType.OtTaggedValue; break;
                case ObjectType.otFile: result = EaObjectType.OtFile; break;
                case ObjectType.otEffort: result = EaObjectType.OtEffort; break;
                case ObjectType.otMetric: result = EaObjectType.OtMetric; break;
                case ObjectType.otIssue: result = EaObjectType.OtIssue; break;
                case ObjectType.otRisk: result = EaObjectType.OtRisk; break;
                case ObjectType.otTest: result = EaObjectType.OtTest; break;
                case ObjectType.otDiagramObject: result = EaObjectType.OtDiagramObject; break;
                case ObjectType.otDiagramLink: result = EaObjectType.OtDiagramLink; break;
                case ObjectType.otResource: result = EaObjectType.OtResource; break;
                case ObjectType.otConnectorEnd: result = EaObjectType.OtConnectorEnd; break;
                case ObjectType.otAttribute: result = EaObjectType.OtAttribute; break;
                case ObjectType.otMethod: result = EaObjectType.OtMethod; break;
                case ObjectType.otParameter: result = EaObjectType.OtParameter; break;
                case ObjectType.otClient: result = EaObjectType.OtClient; break;
                case ObjectType.otAuthor: result = EaObjectType.OtAuthor; break;
                case ObjectType.otDatatype: result = EaObjectType.OtDatatype; break;
                case ObjectType.otStereotype: result = EaObjectType.OtStereotype; break;
                case ObjectType.otTask: result = EaObjectType.OtTask; break;
                case ObjectType.otTerm: result = EaObjectType.OtTerm; break;
                case ObjectType.otProjectIssues: result = EaObjectType.OtProjectIssues; break;
                case ObjectType.otAttributeConstraint: result = EaObjectType.OtAttributeConstraint; break;
                case ObjectType.otAttributeTag: result = EaObjectType.OtAttributeTag; break;
                case ObjectType.otMethodConstraint: result = EaObjectType.OtMethodConstraint; break;
                case ObjectType.otMethodTag: result = EaObjectType.OtMethodTag; break;
                case ObjectType.otConnectorConstraint: result = EaObjectType.OtConnectorConstraint; break;
                case ObjectType.otConnectorTag: result = EaObjectType.OtConnectorTag; break;
                case ObjectType.otProjectResource: result = EaObjectType.OtProjectResource; break;
                case ObjectType.otReference: result = EaObjectType.OtReference; break;
                case ObjectType.otRoleTag: result = EaObjectType.OtRoleTag; break;
                case ObjectType.otCustomProperty: result = EaObjectType.OtCustomProperty; break;
                case ObjectType.otPartition: result = EaObjectType.OtPartition; break;
                case ObjectType.otTransition: result = EaObjectType.OtTransition; break;
                case ObjectType.otEventProperty: result = EaObjectType.OtEventProperty; break;
                case ObjectType.otEventProperties: result = EaObjectType.OtEventProperties; break;
                case ObjectType.otPropertyType: result = EaObjectType.OtPropertyType; break;
                case ObjectType.otProperties: result = EaObjectType.OtProperties; break;
                case ObjectType.otProperty: result = EaObjectType.OtProperty; break;
                case ObjectType.otSwimlaneDef: result = EaObjectType.OtSwimlaneDef; break;
                case ObjectType.otSwimlanes: result = EaObjectType.OtSwimlanes; break;
                case ObjectType.otSwimlane: result = EaObjectType.OtSwimlane; break;
                case ObjectType.otModelWatcher: result = EaObjectType.OtModelWatcher; break;
                case ObjectType.otScenarioStep: result = EaObjectType.OtScenarioStep; break;
                case ObjectType.otScenarioExtension: result = EaObjectType.OtScenarioExtension; break;
                case ObjectType.otParamTag: result = EaObjectType.OtParamTag; break;
                case ObjectType.otProjectRole: result = EaObjectType.OtProjectRole; break;
                case ObjectType.otDocumentGenerator: result = EaObjectType.OtDocumentGenerator; break;
                case ObjectType.otMailInterface: result = EaObjectType.OtMailInterface; break;
                case ObjectType.otSimulation: result = EaObjectType.OtSimulation; break;
                case ObjectType.otTemplateBinding: result = EaObjectType.OtTemplateBinding; break;
                case ObjectType.otTemplateParameter: result = EaObjectType.OtTemplateParameter; break;
            }

            return result;
        }

        public IEaPackage GetTreeSelectedPackage()
        {
            var package = (Package) _eaRepository.GetTreeSelectedObject();
            return _eaFactory.GetPackage(package);
         
        }

        public IEaPackage GetPackageById(int id)
        {
            IEaPackage result = null;
            
            try
            {
                var package = _eaRepository.GetPackageByID(id);
                result = _eaFactory.GetPackage(package);
            }
            catch (Exception e)
            {
                var data = e.Message;
            }

            return result;
        }

        public void Execute(string sql)
        {
            _eaRepository.Execute(sql);
        }

        public string SqlQuery(string sql)
        {
            return _eaRepository.SQLQuery(sql);
        }

        public IEaElement GetElementById(int id)
        {
            return _eaFactory.GetElement(_eaRepository.GetElementByID(id));
        }
    }
}