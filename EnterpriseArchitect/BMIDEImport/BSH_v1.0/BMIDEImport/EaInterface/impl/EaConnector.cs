﻿using System.Activities;
using System.Collections.Generic;
using EA;

namespace BMIDEImport.EaInterface.impl
{
    public class EaConnector : IEaConnector
    {
        private readonly EaFactory _eaFactory; 
        private readonly Connector _eaConnector;
        
        public EaConnector(EaFactory factory, Connector connector)
        {
            _eaFactory = factory;
            _eaConnector = connector;
        }

        public bool Update()
        {
            return _eaConnector.Update();
        }

        public string Type => _eaConnector.Type;
        public string Name
        {
            get { return _eaConnector.Name; }
            set { _eaConnector.Name = value; }
        }

        public int SupplierId
        {
            get { return _eaConnector.SupplierID; }
            set { _eaConnector.SupplierID = value; }
        }

        public string Stereotype
        {
            get { return _eaConnector.Stereotype; }
            set { _eaConnector.Stereotype = value; }
        }

        public string StyleEx
        {
            get { return _eaConnector.StyleEx; }
            set { _eaConnector.StyleEx = value; }
        }

        public string Alias
        {
            get { return _eaConnector.Alias;}
            set { _eaConnector.Alias = value; }
        }

        public IEnumerable<IEaConnectorTag> TaggedValues
        {
            get
            {
                var result = new List<IEaConnectorTag>();
                foreach (EA.ConnectorTag conn in _eaConnector.TaggedValues)
                {
                    result.Add(_eaFactory.GetConnectorTag(conn));
                }
                return result;
            }
        }
       
        public IEaConnectorTag AddTaggedValue(string key, string value)
        {
            return _eaFactory.GetConnectorTag(_eaConnector.TaggedValues.AddNew(key, value) as ConnectorTag);
        }

        public int ConnectorId => _eaConnector.ConnectorID;
        
        public bool LinkTo(IEaElement relationClass)
        {
            var found = false;
            foreach (var connectorToCheck in relationClass.Connectors)
            {
                if (connectorToCheck.SupplierId != relationClass.ElementId)
                    continue;
                var primary = connectorToCheck.Primary;

                var classifierGuid = _eaFactory.GetRepository()
                    .SqlQuery("SELECT Classifier_guid FROM t_object WHERE Object_ID=" + primary.ElementId);

                if (classifierGuid.IndexOf("<Classifier_guid>") > 0)
                    classifierGuid = classifierGuid.Substring(classifierGuid.IndexOf("<Classifier_guid>") + 17);
                if (classifierGuid.IndexOf("</Classifier_guid>") > 0)
                    classifierGuid = classifierGuid.Substring(0, classifierGuid.IndexOf("</Classifier_guid>"));


                var connectorGuid = _eaConnector.ConnectorGUID;
                if (classifierGuid.Equals(connectorGuid))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                var proxy = relationClass.Package.AddNewElement("ProxyConnector", "ProxyConnector");
                // proxy.ClassifierID = conn.ConnectorID;  // BUG: this is NOT stored 
                proxy.Update();
                _eaFactory.GetRepository().Execute("UPDATE t_object SET "
                                        + "Classifier_guid='" +_eaConnector.ConnectorGUID + "', "
                                        + "Classifier = " + _eaConnector.ConnectorID 
                                        + " WHERE Object_ID=" + proxy.ElementId + ";");
                proxy.Update();
                var test = proxy.ClassifierID;

                // now try to "link" the ProxyConnector to class C
                var link = proxy.AddNewConnector("", "Dependency");
                link.SupplierId = relationClass.ElementId;
                link.Update();
                link.SupplierEnd.IsNavigable = true;
                link.SupplierEnd.Update();
                link.ClientEnd.Update();
            }

            return !found;
        }

        public IEaElement Primary
        {
            get
            {
                return _eaFactory.GetRepository().GetElementById(_eaConnector.ClientID);
                
            }
            set { _eaConnector.ClientID = value.ElementId; }
        }

        public IEnumerable<IEaConnectorConstraint> Constraints {  
            get
            {
                var result = new List<IEaConnectorConstraint>();
                foreach (EA.ConnectorConstraint cons in _eaConnector.Constraints)
                {
                    result.Add(_eaFactory.GetConstraint(cons));
                }
                return result;
            } 
        }

        public IEaConnectorConstraint AddConstraint(string name, string value, string type)
        {
            var constraint = _eaFactory.GetConstraint(_eaConnector.Constraints.AddNew(name, type) as ConnectorConstraint) as EaConnectorConstraint;
                        
            constraint.Notes = value;
            constraint.Update();

            return constraint;
        }


        public IEaConnectorEnd ClientEnd => _eaFactory.GetConnectorEnd(_eaConnector.ClientEnd);
        public IEaConnectorEnd SupplierEnd => _eaFactory.GetConnectorEnd(_eaConnector.SupplierEnd);
    }
}