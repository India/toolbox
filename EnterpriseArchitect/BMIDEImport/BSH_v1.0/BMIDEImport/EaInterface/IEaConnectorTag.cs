﻿namespace BMIDEImport.EaInterface
{
    public interface IEaConnectorTag : IEaObject
    {
        string Name { get; }
        string Value { get; set; }
    }
}