﻿namespace BMIDEImport.EaInterface
{
    public enum EaObjectType
    {
        OtProject,
        OtRepository,
        OtCollection,
        OtElement,
        OtPackage,
        OtModel,
        OtConnector,
        OtDiagram,
        OtRequirement,
        OtScenario,
        OtConstraint,
        OtTaggedValue,
        OtFile,
        OtEffort,
        OtMetric,
        OtIssue,
        OtRisk,
        OtTest,
        OtDiagramObject,
        OtDiagramLink,
        OtResource,
        OtConnectorEnd,
        OtAttribute,
        OtMethod,
        OtParameter,
        OtClient,
        OtAuthor,
        OtDatatype,
        OtStereotype,
        OtTask,
        OtTerm,
        OtProjectIssues,
        OtAttributeConstraint,
        OtAttributeTag,
        OtMethodConstraint,
        OtMethodTag,
        OtConnectorConstraint,
        OtConnectorTag,
        OtProjectResource,
        OtReference,
        OtRoleTag,
        OtCustomProperty,
        OtPartition,
        OtTransition,
        OtEventProperty,
        OtEventProperties,
        OtPropertyType,
        OtProperties,
        OtProperty,
        OtSwimlaneDef,
        OtSwimlanes,
        OtSwimlane,
        OtModelWatcher,
        OtScenarioStep,
        OtScenarioExtension,
        OtParamTag,
        OtProjectRole,
        OtDocumentGenerator,
        OtMailInterface,
        OtSimulation,
        OtTemplateBinding,
        OtTemplateParameter            
    }
}