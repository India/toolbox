﻿using System;
using BMIDEImport.UI.FileHandler;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMIDEImportTest
{
    [TestClass]
    public class FileHandlerTest
    {
        [TestMethod]
        public void TestUnpacked()
        {
            using (var file = FileHandler.GetFile(StringResources.TestModelFile))
            {
                Assert.IsNotNull(file);
                var importFilePath = file.ImportFilePath;
                Assert.IsNotNull(file.ImportFilePath);
                Assert.AreEqual(file.ImportFilePath, StringResources.TestModelFile);
            }
        }
        
        [TestMethod]
        public void TestZIP()
        {
            using (var file = FileHandler.GetFile(StringResources.TestModelZipFile))
            {

                Assert.IsNotNull(file);
                var importFilePath = file.ImportFilePath;
                Assert.IsNotNull(importFilePath);
                Assert.IsTrue(importFilePath.EndsWith("_template.xml"));
            }
        }

        [TestMethod]
        public void TestError()
        {
            try
            {
                var file = FileHandler.GetFile("a");
                Assert.Fail("Expected Exception when giving an invalid file");
            }
            catch (Exception exception)
            {
                Assert.IsInstanceOfType(exception, typeof(ArgumentException));
            }
        }
       
    }
}