﻿using System;
using BMIDEImport.Model;
using BMIDEImport.Template;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMIDEImportTest
{
    [TestClass]
    public class FormTests
    {
        public Model Model { get; set; }
        public ModelClass Form { get; set; }

        public FormTests()
        {
            var bmideData =
                BmideData.ReadBmidePackage(StringResources.TestModelFile, StringResources.TestTemplateDirectory);
            this.Model = new Model(bmideData);
            this.Form = Model.MainTemplate.GetClass("LM4Form1");
        }

        [TestMethod]
        public void FormExistTest()
        {
            Assert.IsNotNull(Form);
        }

        [TestMethod]
        public void FormBaseTest()
        {
            Assert.AreEqual("LM4Form1", Form.Name);
            Assert.AreEqual(false, Form.Abstract);
            Assert.AreEqual("Form1", Form.Alias);
            Assert.AreEqual("Form", Form.Parent.Name);
            Assert.AreEqual("LM4Form1Storage", Form.Storage.Name);
            Assert.AreEqual("", Form.Description);
            //Assert.IsNull(Form.EaClass); // not yet aligned TODO: EA Test seperately
            Assert.AreEqual(Model, Form.Model);
            Assert.AreEqual(ModelPart.EStatus.UpToDate, Form.Status); // initial state
            Assert.IsNull(Form.StereoType);
            Assert.AreEqual(Model.MainTemplate, Form.Template);

        }

        [TestMethod]
        public void FormPackageTest()
        {
            Assert.AreEqual("Form",                   Form.Package.Name);
            Assert.AreEqual("WorkspaceObject",        Form.Package.Parent.Name);
            Assert.AreEqual("POM_application_object", Form.Package.Parent.Parent.Name);
            Assert.AreEqual("POM_object",             Form.Package.Parent.Parent.Parent.Name);
            Assert.AreEqual("lm4importdemo",          Form.Package.Parent.Parent.Parent.Parent.Name);
            Assert.IsNull(                            Form.Package.Parent.Parent.Parent.Parent.Parent);
        }

        [TestMethod]
        public void FormAttributesTest()
        {
            var attrs = Form.Attributes;
            string[] expectedAttrs = {"lm4compound", "lm4lovstring", "LM4Rel1", "lm4string1"};
            
            Assert.AreEqual(4, attrs.Count);
            foreach(var expected in expectedAttrs)
            {
                Assert.IsTrue(attrs.ContainsKey(expected));
            }
        }

        [TestMethod]
        public void FormStorageAttributesTest()
        {
            var attrs = Form.Attributes;
            var storageAttrs = Form.Storage.Attributes;
            
            Assert.IsTrue(attrs.Count > storageAttrs.Count);

            foreach (var storageAttr in storageAttrs.Values)
            {
                Assert.IsTrue(attrs.ContainsKey(storageAttr.Name));
                var formAttr = attrs[storageAttr.Name];
                Assert.AreEqual("Runtime", formAttr.PropertyType);
                Assert.AreEqual(storageAttr.Type, formAttr.Type, "Type Storag/Form-Attribute are different");
                Assert.AreEqual(storageAttr.Alias, formAttr.Alias, "Alias Storage/Form-Attribute are differnt");
            }
            
            Assert.IsNotNull(attrs["lm4lovstring"].Lov);
            Assert.IsNull(storageAttrs["lm4lovstring"].Lov);
        }
    }
}