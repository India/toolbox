﻿using System;
using System.ComponentModel.Design;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using BMIDEImport.EaInterface;
using BMIDEImport.Model;
using BMIDEImport.Template;
using BMIDEImportTest.EaImpl;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMIDEImportTest
{
    [TestClass]
    public class EaMockTest
    {
        private BmideData Data { get; set; }
        private Model ImportModel { get; set; }

       
        public EaMockTest()
        {
            Data = BmideData.ReadBmidePackage(StringResources.TestModelFile, StringResources.TestTemplateDirectory);
            ImportModel = new Model(Data);
        }

        #region Package Testing
        
        [TestMethod]
        public void TestPackageCreation()
        {
            var rootPackage = new MockEaPackage("Root");

            var demoPackage = rootPackage.CreateOrGetPackage("DEMO");
            Assert.IsNotNull(demoPackage);
            Assert.AreEqual(demoPackage.Name, "DEMO");
            Assert.IsNull(demoPackage.Packages.GetEnumerator().Current);

            var demo2Package = rootPackage.CreateOrGetPackage("DEMO");
            Assert.AreSame(demoPackage, demo2Package);

            var demo3Package = rootPackage.CreateOrGetPackage("DEMO2");
            Assert.IsNotNull(demo3Package);
            Assert.AreEqual(demo3Package.Name, "DEMO2");
            Assert.IsNull(demo3Package.Packages.GetEnumerator().Current);
            Assert.AreNotSame(demoPackage, demo3Package);

            using (var pkgEnumerator = rootPackage.Packages.GetEnumerator())
            {   
                while (pkgEnumerator.MoveNext())
                {
                    if (pkgEnumerator.Current == demoPackage)
                        continue;
                    if (pkgEnumerator.Current == demo3Package)
                        continue;
                    Assert.Fail("Unexpected Package Found");
                }
            }
        }

        [TestMethod]
        public void TestPackageElementHandling()
        {
            var rootPackage = new MockEaPackage("Root");
            var result = rootPackage.GetElementByName("foo");

            Assert.IsNull(result);

            result = rootPackage.AddNewElement("foo", "class");
            Assert.IsNotNull(result);
            Assert.IsTrue(1 <= result.ElementId);

            try
            {
                var result2 = rootPackage.AddNewElement("foo", "dummy");
                Assert.IsNull(result2);
                Assert.Fail("Adding two elements with same name should raise an exception");
            }
            catch (ArgumentException)
            {
            }
            

            var result3 = rootPackage.GetElementByName("foo");
            Assert.IsNotNull(result3, "After adding element \"foo\" it should be returned, when asked");

            // check whether there is only one element in the package
            using (var clsEnumerator = rootPackage.Elements.GetEnumerator())
            {
                while (clsEnumerator.MoveNext())
                {
                    if (clsEnumerator.Current == result)
                        continue;

                    Assert.Fail("Unexpected Element Found");
                }
            }
        }

        [TestMethod]
        public void TestPackageDirtyFlag()
        {
            var rootPackage = new MockEaPackage("ROOT");
            Assert.IsFalse(rootPackage.Dirty);

            rootPackage.AddNewElement("Hello", "test2");
            Assert.IsTrue(rootPackage.Dirty);

            rootPackage.Update();
            Assert.IsFalse(rootPackage.Dirty);

            rootPackage.CreateOrGetPackage("foo");
            Assert.IsTrue(rootPackage.Dirty);

            rootPackage.Update();
            Assert.IsFalse(rootPackage.Dirty);
            
        }

        [TestMethod]
        public void TestPackageRefresh()
        {
            var rootPackage = new MockEaPackage("ROOT");
            rootPackage.RefreshElements();            
        }
        
        #endregion
        
        #region Element Testing

        [TestMethod]
        public void TestCreateElement()
        {
            var rootPackage = new MockEaPackage("ROOT");

            var elem = rootPackage.AddNewElement("foo", "class");
            Assert.IsTrue(elem.ElementId >= 1, "Unexpected Element ID of new Class");

            Assert.IsTrue(((MockEaElement) elem).Dirty);
            elem.Update();
            Assert.IsFalse(((MockEaElement) elem).Dirty);
 
            CheckPropertyDirtyFlag<MockEaElement, string>((MockEaElement) elem, "dummy", delegate(string val) { elem.Notes = val; }, (e) => e.Notes, "Notes");
            CheckPropertyDirtyFlag<MockEaElement, string>((MockEaElement) elem, "dummy", delegate(string val) { elem.Abstract = val; }, (e) => e.Abstract, "Abstract");
            CheckPropertyDirtyFlag<MockEaElement, string>((MockEaElement) elem, "dummy", delegate(string val) { elem.Alias = val; }, (e) => e.Alias, "Alias");
            CheckPropertyDirtyFlag<MockEaElement, string>((MockEaElement) elem, "dummy", delegate(string val) { elem.Stereotype = val; }, (e) => e.Stereotype, "Stereotype");
            
        }

        [TestMethod]
        public void testElementAttributes()
        {
            var root = new MockEaPackage("ROOT");
            var elem = root.AddNewElement("Foo", "Bar");

            Assert.IsTrue(((MockEaElement) elem).Dirty);
            elem.Update();
            Assert.IsFalse(((MockEaElement) elem).Dirty);

            var attrEnum = elem.Attributes.GetEnumerator();
            while(attrEnum.MoveNext())
                Assert.Fail("Element has Attributes after creation!");

            var attr = elem.AddAttribute("demo", "int");
            Assert.IsNotNull(attr);

            Assert.IsTrue(((MockEaElement) elem).Dirty);
            elem.Update();
            Assert.IsFalse(((MockEaElement) elem).Dirty);

            attrEnum = elem.Attributes.GetEnumerator();
            while (attrEnum.MoveNext())
            {
                if (attrEnum.Current == attr)
                    continue;
                if (attrEnum.Current != null) 
                    Assert.Fail($"Unexpected Attribute {attrEnum.Current.Name}");
            }
        }

        [TestMethod]
        public void testElementConnectors()
        {
            var root = new MockEaPackage("ROOT");
            var elem = root.AddNewElement("Foo", "Bar");

            Assert.IsTrue(((MockEaElement) elem).Dirty);
            elem.Update();
            Assert.IsFalse(((MockEaElement) elem).Dirty);

            IEaConnector conn = null;
            using (var connEnum = elem.Connectors.GetEnumerator())
            {
                while (connEnum.MoveNext())
                    Assert.Fail("Element has Connectors after creation!");

                conn = elem.AddNewConnector("demo", "relation");
                Assert.IsNotNull(conn);
            }
            
            Assert.IsTrue(((MockEaElement) elem).Dirty);
            elem.Update();
            Assert.IsFalse(((MockEaElement) elem).Dirty);

            using (var connEnum = elem.Connectors.GetEnumerator())
            {
                while (connEnum.MoveNext())
                {
                    if (connEnum.Current == conn)
                        continue;
                    if (connEnum.Current != null)
                        Assert.Fail($"Unexpected Connector {connEnum.Current.Name}");
                }
            }

            elem.DeleteConnector(conn);

            Assert.IsTrue(((MockEaElement) elem).Dirty);
            elem.Update();
            Assert.IsFalse(((MockEaElement) elem).Dirty);

            using (var connEnum = elem.Connectors.GetEnumerator())
            {
                while (connEnum.MoveNext())
                    Assert.Fail("Element has Connectors after deleting the last one creation!");
            }

        }

        #endregion

        #region Attribute Testing

        [TestMethod]
        public void TestAttributeDirty()
        {
            var package = new MockEaPackage("ROOT");
            var elem = package.AddNewElement("TEST", "class");
            IEaAttribute attr = (MockEaAttribute) elem.AddAttribute("test", "string");
            
            Assert.IsTrue(((MockEaAttribute)attr).Dirty);
            attr.Update();
            Assert.IsFalse(((MockEaAttribute)attr).Dirty);
            CheckPropertyDirtyFlag<MockEaAttribute, string>( ((MockEaAttribute)attr), "test", delegate(string val) { attr.Name = val; }, (e) => e.Name, "Name", false);
            CheckPropertyDirtyFlag<MockEaAttribute, string>( ((MockEaAttribute)attr), "test", delegate(string val) { attr.Type = val; }, (e) => e.Type, "Type", false);
            CheckPropertyDirtyFlag<MockEaAttribute, string>( ((MockEaAttribute)attr), "test", delegate(string val) { attr.Notes = val; }, (e) => e.Notes, "Notes");
            CheckPropertyDirtyFlag<MockEaAttribute, string>( ((MockEaAttribute)attr), "test", delegate(string val) { attr.Default = val; }, (e) => e.Default, "Default");
            CheckPropertyDirtyFlag<MockEaAttribute, string>( ((MockEaAttribute)attr), "test", delegate(string val) { attr.Alias = val; }, (e) => e.Alias, "Alias");
            CheckPropertyDirtyFlag<MockEaAttribute, string>( ((MockEaAttribute)attr), "test", delegate(string val) { attr.UpperBound = val; }, (e) => e.UpperBound, "UpperBound");
            CheckPropertyDirtyFlag<MockEaAttribute, string>( ((MockEaAttribute)attr), "test", delegate(string val) { attr.LowerBound = val; }, (e) => e.LowerBound, "LowerBound");
            CheckPropertyDirtyFlag<MockEaAttribute, string>( ((MockEaAttribute)attr), "test", delegate(string val) { attr.Stereotype = val; }, (e) => e.Stereotype, "Stereotype");
            CheckPropertyDirtyFlag<MockEaAttribute, string>( ((MockEaAttribute)attr), "test", delegate(string val) { attr.AttributeGuid = val; }, (e) => e.AttributeGuid, "AttributeGuid");
            CheckPropertyDirtyFlag<MockEaAttribute, Scope>( ((MockEaAttribute)attr), Scope.Private, delegate(Scope val) { attr.Visibility = val; }, (e) => e.Visibility, "Visibility", false);
        }

        #endregion
        
        #region Connector Testing

        [TestMethod]
        public void TestConnectorCreation()
        {
            var package = new MockEaPackage("ROOT");
            var elem = package.AddNewElement("TEST", "class");
            var connector = elem.AddNewConnector("connector", "relation");
            
            Assert.AreEqual("relation", connector.Type);
            Assert.AreEqual("connector", connector.Name);
            Assert.IsNotNull(connector.SupplierEnd);
            Assert.IsNotNull(connector.ClientEnd);
            
            Assert.IsTrue(connector.ConnectorId >= 1);
        }

        [TestMethod]
        public void TestConnectorDirty()
        {
            var package = new MockEaPackage("ROOT");
            var elem = package.AddNewElement("TEST", "class");
            var connector = elem.AddNewConnector("connector", "relation");
            
            Assert.IsTrue(((MockEaConnector)connector).Dirty);
            connector.Update();
            Assert.IsFalse(((MockEaConnector)connector).Dirty);

            
            CheckPropertyDirtyFlag<MockEaConnector, string>( ((MockEaConnector)connector), "test", delegate(string val) { connector.Name = val; }, (e) => e.Name, "Name", false);
            CheckPropertyDirtyFlag<MockEaConnector, int>( ((MockEaConnector)connector), 42, delegate(int val) { connector.SupplierId = val; }, (e) => e.SupplierId, "SupplierId", false);
            CheckPropertyDirtyFlag<MockEaConnector, string>( ((MockEaConnector)connector), "test", delegate(string val) { connector.Stereotype = val; }, (e) => e.Stereotype, "Stereotype");
            CheckPropertyDirtyFlag<MockEaConnector, string>( ((MockEaConnector)connector), "test", delegate(string val) { connector.StyleEx = val; }, (e) => e.StyleEx, "StyleEx");
            CheckPropertyDirtyFlag<MockEaConnector, string>( ((MockEaConnector)connector), "test", delegate(string val) { connector.Alias = val; }, (e) => e.Alias, "Alias");

            CheckOtherDirtyFlag(((MockEaConnector) connector), delegate() {connector.AddTaggedValue("a", "b");}, "AddTaggedValue");
        }

        [TestMethod]
        public void TestConnectorTaggedValues()
        {
            var package = new MockEaPackage("ROOT");
            var elem = package.AddNewElement("TEST", "class");
            var connector = elem.AddNewConnector("connector", "relation");

            using (var tvEnum = connector.TaggedValues.GetEnumerator())
            {
                while(tvEnum.MoveNext())
                    Assert.Fail("Unexpected TaggedValues after Connector creation");
            }

            var tv = connector.AddTaggedValue("a", "b");
            Assert.IsNotNull(tv);
            Assert.AreEqual("a", tv.Name);
            Assert.AreEqual("b", tv.Value);

            using (var tvEnum = connector.TaggedValues.GetEnumerator())
            {
                while (tvEnum.MoveNext())
                {
                    if (tvEnum.Current == tv)
                        continue;
                    Assert.Fail("Unexpected TaggedValues after Connector creation");
                }
            }

        }
        #endregion
        
        #region Connector Tagged Value Tests

        [TestMethod]
        public void TestConnectorTaggedDirty()
        {
            var package = new MockEaPackage("ROOT");
            var elem = package.AddNewElement("TEST", "class");
            var connector = elem.AddNewConnector("connector", "relation");
            var tv = connector.AddTaggedValue("a", "b");
            
            Assert.IsNotNull(tv);
            Assert.AreEqual("a", tv.Name);
            Assert.AreEqual("b", tv.Value);
            Assert.IsTrue(((MockEaConnectorTag)tv).Dirty);
            tv.Update();
            Assert.IsFalse(((MockEaConnectorTag)tv).Dirty);
            
            CheckPropertyDirtyFlag<MockEaConnectorTag, string>( ((MockEaConnectorTag)tv), "test", delegate(string val) { tv.Value = val; }, (e) => e.Value, "Value", false);

        }
        #endregion
        
        #region Connector End Tests

        [TestMethod]
        public void TestConnectorEndCreation()
        {
            var package = new MockEaPackage("ROOT");
            var elem = package.AddNewElement("TEST", "class");
            var connector = elem.AddNewConnector("connector", "relation");
            
            Assert.IsNotNull(connector.SupplierEnd);
            Assert.IsNotNull(connector.ClientEnd);
        }

        [TestMethod]
        public void TestConnectorEndDirtyFlag()
        {
            var package = new MockEaPackage("ROOT");
            var elem = package.AddNewElement("TEST", "class");
            var connector = elem.AddNewConnector("connector", "relation");

            var connectorEnd = connector.SupplierEnd;
            CheckPropertyDirtyFlag<MockEaConnectorEnd, string>( ((MockEaConnectorEnd)connectorEnd), "test", delegate(string val) { connectorEnd.Cardinality = val; }, (e) => e.Cardinality, "Cardinality");
            CheckPropertyDirtyFlag<MockEaConnectorEnd, bool>( ((MockEaConnectorEnd)connectorEnd), true, delegate(bool val) { connectorEnd.IsNavigable = val; }, (e) => e.IsNavigable, "IsNavigable", false);
            CheckPropertyDirtyFlag<MockEaConnectorEnd, bool>( ((MockEaConnectorEnd)connectorEnd), false, delegate(bool val) { connectorEnd.IsNavigable = val; }, (e) => e.IsNavigable, "IsNavigable", false);
            CheckPropertyDirtyFlag<MockEaConnectorEnd, int>( ((MockEaConnectorEnd)connectorEnd), 1, delegate(int val) { connectorEnd.Aggregation = val; }, (e) => e.Aggregation, "Aggregation", false);
            CheckPropertyDirtyFlag<MockEaConnectorEnd, string>( ((MockEaConnectorEnd)connectorEnd), "test", delegate(string val) { connectorEnd.Navigable = val; }, (e) => e.Navigable, "Navigable");
            CheckPropertyDirtyFlag<MockEaConnectorEnd, string>( ((MockEaConnectorEnd)connectorEnd), "test", delegate(string val) { connectorEnd.Alias = val; }, (e) => e.Alias, "Alias");
            CheckPropertyDirtyFlag<MockEaConnectorEnd, string>( ((MockEaConnectorEnd)connectorEnd), "test", delegate(string val) { connectorEnd.Role = val; }, (e) => e.Role, "Role");
            

        }
        #endregion
        
        #region Helper

        private static void CheckPropertyDirtyFlag<T, TValue>(T obj, TValue value, Action<TValue> setter,
            Func<T, TValue> func, string property, bool checkInitialNull = true) where T : IMockInterface
        {
            if(checkInitialNull)
                Assert.IsNull(func(obj), $"Property {property} initially is not null");
            Assert.IsFalse(((IMockInterface)obj).Dirty, $"Property {property} unexpected initial Dirty-flag");

            setter(value);
            Assert.AreEqual(value, func(obj), $"Property {property} does not return set value");
            Assert.IsTrue(((IMockInterface)obj).Dirty,  $"Property {property} has not switched Dirty-flag");
            obj.Update();
            Assert.IsFalse(((IMockInterface)obj).Dirty, $"Property {property} has not switched Dirty-flag to false after Update");
        }

        private static void CheckOtherDirtyFlag<T>(T obj, Action action, string method) where T : IMockInterface
        {
            Assert.IsFalse(((IMockInterface)obj).Dirty, $"Method {method} unexpected initial Dirty-flag");

            action();
            
            Assert.IsTrue(((IMockInterface)obj).Dirty,  $"Method {method} has not switched Dirty-flag");
            obj.Update();
            Assert.IsFalse(((IMockInterface)obj).Dirty, $"Method {method} has not switched Dirty-flag to false after Update");
        }

        
        #endregion

        

/*        [TestMethod]
        public void LoadTest()
        {
            MockEaPackage rootPackage = new MockEaPackage("TEST");
            
            var task = Task.Run( async () => { await ImportModel.Align(rootPackage, null); });
            task.Wait();
            rootPackage.Update();            
            Assert.IsFalse(rootPackage.Dirty);
        }
  */
    }
}