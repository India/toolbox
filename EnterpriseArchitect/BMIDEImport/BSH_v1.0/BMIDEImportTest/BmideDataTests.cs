﻿using System;
using BMIDEImport.Model;
using BMIDEImport.Template;
using BMIDEImport.UI.FileHandler;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BMIDEImportTest
{
    [TestClass]
    public class BmideDataTests
    {
        [TestMethod]
        public void LoadModelUnzippedTemplate()
        {
            var bmideData = BmideData.ReadBmidePackage(StringResources.TestModelFile, StringResources.TestTemplateDirectory);
            Assert.IsNotNull(bmideData);
        }

        [TestMethod]
        public void LoadModelZipTemplate()
        {
            IFile file = FileHandler.GetFile(StringResources.TestModelZipFile);
            Assert.IsNotNull(file);
            var importFilePath = file.ImportFilePath;
            var bmideData = BmideData.ReadBmidePackage(file.ImportFilePath, StringResources.TestTemplateDirectory);
            Assert.IsNotNull(bmideData);
        }
        
        [TestMethod]
        public void LoadModelMasterFile()
        {
            var bmideData = BmideData.ReadBmidePackage(StringResources.TestModelMasterFile, StringResources.TestTemplateDirectory);
            Assert.IsNotNull(bmideData);
        }
        
        [TestMethod]
        public void LoadModelExtensionFile()
        {
            var bmideData = BmideData.ReadBmidePackage(StringResources.TestModelExtensionFile, StringResources.TestTemplateDirectory);
            Assert.IsNotNull(bmideData);
        }

        
        [TestMethod]
        [ExpectedException(typeof(System.IO.FileNotFoundException))]
        public void LoadTemplateFailed()
        {
            var bmideData = BmideData.ReadBmidePackage(StringResources.TestModelFile + "-not-exists", StringResources.TestTemplateDirectory);
        }

        [TestMethod]
        public void CreateModelFromBmideData()
        {
            var bmideData = BmideData.ReadBmidePackage(StringResources.TestModelFile, StringResources.TestTemplateDirectory);
            var model = new Model(bmideData);
            
            Assert.IsNotNull(model);
        }

    }
}
