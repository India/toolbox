﻿using System.Collections.Generic;
using BMIDEImport.EaInterface;
using BMIDEImport.EaInterface.impl;

namespace BMIDEImportTest.EaImpl
{
    public class MockEaConnector : IEaConnector, IMockInterface
    {
        public bool Dirty { get; private set; }
        private static int _ConnectorID = 1;

        public bool Update()
        {
            Dirty = false;
            return true;
        }

        private string _type;
        private int _supplierId;
        private int _connectorId;
        private string _name;
        private string _stereotype;
        private IEaConnectorEnd _clientEnd;
        private IEaConnectorEnd _supplierEnd;
        private string _styleEx;
        readonly List<IEaConnectorTag> _taggedValues = new List<IEaConnectorTag>();
        private string _alias;

        public MockEaConnector(string name, string type)
        {
            Name = name;
            Type = type;
            ClientEnd = new MockEaConnectorEnd();
            SupplierEnd = new MockEaConnectorEnd();
            ConnectorId = _ConnectorID++;
        }

        public string Type
        {
            get { return _type; }
            set
            {
                _type = value; 
                Dirty = true;
            }
        }

        public int SupplierId
        {
            get { return _supplierId; }
            set
            {
                _supplierId = value;
                Dirty = true;
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value; 
                Dirty = true;
            }
        }

        public string Stereotype
        {
            get { return _stereotype; }
            set
            {
                _stereotype = value;
                Dirty = true;
            }
        }

        public IEaConnectorEnd ClientEnd
        {
            get { return _clientEnd; }
            set
            {
                _clientEnd = value;
                Dirty = true;
            }
        }

        public IEaConnectorEnd SupplierEnd
        {
            get { return _supplierEnd; }
            set
            {
                _supplierEnd = value; 
                Dirty = true;
            }
        }

        public string StyleEx
        {
            get { return _styleEx; }
            set { 
                _styleEx = value;
                Dirty = true;
            }
        }

        public IEnumerable<IEaConnectorTag> TaggedValues => _taggedValues;
        public string Alias
        {
            get { return _alias;}
            set { _alias = value;
                Dirty = true;
            }
        }

        public IEaConnectorTag AddTaggedValue(string key, string value)
        {
            var result = new MockEaConnectorTag(key, value);
            _taggedValues.Add(result);
            Dirty = true;
            return result;
        }

        public int ConnectorId {             
            get { return _connectorId; }
            set
            {
                _connectorId = value;
                Dirty = true;
            } }
    }
}