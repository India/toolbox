﻿using BMIDEImport.EaInterface;

namespace BMIDEImportTest.EaImpl
{
    public class MockEaConnectorTag : IEaConnectorTag, IMockInterface
    {
        public bool Dirty { get; private set; }

        public bool Update()
        {
            Dirty = false;
            return true;
        }

        public MockEaConnectorTag(string name, string value)
        {
            Name = name;
            Value = value;
        }
        
        private string _name;
        private string _value;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                Dirty = true;
            }
        }

        public string Value
        {
            get { return _value; }
            set
            {
                _value = value; 
                Dirty = true;
            }
        }
    }
}