﻿using System;
using System.Collections.Generic;

using BMIDEImport.EaInterface;

namespace BMIDEImportTest.EaImpl
{
    public class MockEaPackage : IEaPackage
    {
        public bool Dirty { get; private set; }

        public bool Update()
        {
            Dirty = false;
            return true;
        }

        private readonly Dictionary<string, IEaPackage> _packages = new Dictionary<string, IEaPackage>();
        private readonly Dictionary<string, IEaElement> _elements = new Dictionary<string, IEaElement>();

        public IEnumerable<IEaPackage> Packages => _packages.Values;
        public IEnumerable<IEaElement> Elements => _elements.Values;

        public MockEaPackage(string name)
        {
            Name = name;
        }

        public IEaPackage CreateOrGetPackage(string name)
        {
            IEaPackage result;

            if (_packages.ContainsKey(name))
                result = _packages[name];
            else
            {
                result = new MockEaPackage(name);
                _packages[name] = result;
            }

            Dirty = true;

            return result;
        }

        public string Name { get; private set; }

        public IEaElement AddNewElement(string name, string type)
        {
            if(_elements.ContainsKey(name))
                throw new ArgumentException($"Element of name {name} already exist", nameof(name));
            var result = new MockEaElement(name, type);
            _elements[name] = result;

            Dirty = true;
            
            return result;
        }

        public IEaElement GetElementByName(string name)
        {
            return _elements.ContainsKey(name) ? _elements[name] : null;
        }

        public void RefreshElements()
        {
            // Nothing to do
        }
    }
}