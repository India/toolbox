namespace BMIDEImportTest.EaImpl
{
    public interface IMockInterface
    {
        bool Dirty { get; }
        bool Update();
    }
}