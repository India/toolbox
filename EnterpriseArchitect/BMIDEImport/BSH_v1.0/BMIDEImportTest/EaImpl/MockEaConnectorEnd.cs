﻿using BMIDEImport.EaInterface;

namespace BMIDEImportTest.EaImpl
{
    public class MockEaConnectorEnd : IEaConnectorEnd, IMockInterface
    {
        public bool Dirty { get; private set; }

        public bool Update()
        {
            Dirty = false;
            return true;
        }
        
        string _cardinality;
        bool _isNavigable;
        int _aggregation;
        string _navigable;
        private string _alias;
        private string _role;

        public string Cardinality
        {
            get { return _cardinality; }
            set
            {
                _cardinality = value;
                Dirty = true;
            }
        }

        public bool IsNavigable
        {
            get { return _isNavigable; }
            set
            {
                _isNavigable = value; 
                Dirty = true;
            }
        }

        public int Aggregation
        {
            get { return _aggregation; }
            set
            {
                Dirty = true;
                _aggregation = value;
            }
        }

        public string Navigable
        {
            get { return _navigable; }
            set
            {
                _navigable = value; 
                Dirty = true;
            }
        }

        public string Alias {       
            get { return _alias; }
            set
            {
                _alias = value; 
                Dirty = true;
            }
        }
        public string Role { 
            get { return _role; }
            set
            {
                _role = value; 
                Dirty = true;
            }}
    }
}