﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using BMIDEImport.EaInterface;
using BMIDEImport.EaInterface.impl;
using BMIDEImport.UI;
using EA;

namespace BMIDEImport
{
    public class Main
    {
        private const string MenuHeader = "-&BMIDE";
        // private const string MenuHeader = "-&LMtec BMIDE";
        private const string ImportBmideTemplate = "Import BMIDE Template..."; //

        private readonly IEaFactory eaFactory = new EaFactory();
        
        //Global Variables
        

        public String EA_Connect(EA.Repository Repository)
        {
            // No special processing req'd
            return "";
        }
        bool IsProjectOpen(EA.Repository Repository)
        {
            try
            {
                var c = Repository.Models;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public object EA_GetMenuItems(EA.Repository Repository, string Location, string MenuName)
        {
            /* nb example of out parameter:
			object item;
			EA.ObjectType t = Repository.GetTreeSelectedItem(out item);
			EA.Package r = (EA.Package) item;
			*/

            switch (MenuName)
            {
                case "":
                    return MenuHeader;
                case MenuHeader:
                    string[] subMenus = { ImportBmideTemplate };
                    return subMenus;
                default:
                    break;
            }
            return null;
        }
        public void EA_MenuClick(EA.Repository repository, string location, string menuName, string itemName)
        {
            switch (itemName)
            {
                case ImportBmideTemplate:
                    try
                    {
                        repository.EnableUIUpdates = false;

                        var repo = eaFactory.GetRepository(repository);
                        var dlg = new ImportDialogView(repo);
                        var ctl = new ImportDialogController(repo, dlg);
                        dlg.ShowDialog();

                    }
                    finally
                    {
                        repository.EnableUIUpdates = true;
                    }
                    break;
                default:
                    break;
            }   
        }
        
        public void EA_Disconnect()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

    }
}
