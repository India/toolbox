﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Localization
{
    public class TcLocalizationKey {
        [XmlAttribute("id")]
        public string ID;

        [XmlAttribute("locale")]
        public string Locale;

        [XmlAttribute("status")]
        public string Status;

        [XmlText]
        public string Text;
    }
}