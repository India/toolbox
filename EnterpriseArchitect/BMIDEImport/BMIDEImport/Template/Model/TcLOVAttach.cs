﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcLOVAttach
    {
        [XmlAttribute("typeName")]
        public string tcLOVAttachTypeName;

        [XmlAttribute("lovName")]
        public string tcLOVAttachLovName;

        [XmlElement("TcLOVAttachPropertyInfo")]
        public TcLOVAttachPropertyInfo tcLOVAttachPropertyInfo;
    }
}