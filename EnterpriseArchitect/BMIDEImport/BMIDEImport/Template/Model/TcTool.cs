﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcTool
    {
        [XmlAttribute("name")]
        public string Name;
    }
}