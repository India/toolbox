﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcUnitOfMeasure
    {
        [XmlAttribute("unitOfMeasureName")]
        public string Name;

        [XmlAttribute("description")]
        public string Description;

    }
}