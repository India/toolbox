﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcLOVValue
    {
        [XmlAttribute("conditionName")]
        public string lovValueCondition;

        [XmlAttribute("description")]
        public string lovValueDescription;

        [XmlAttribute("value")]
        public string lovValue;
    }
}