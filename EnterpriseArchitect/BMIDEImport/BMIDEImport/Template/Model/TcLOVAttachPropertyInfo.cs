﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcLOVAttachPropertyInfo
    {
        [XmlAttribute("valuePropertyName")]
        public string tcLOVAttachValuePropertyName;
    }
}