﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BMIDEImport.Template.Model
{
    public class MergedBusinessDefintions
    {
        public MergedBusinessDefintions(BmideData container)
        {
            Container = container;
        }

        public string className { get; set; }
        public string classDescription { get; set; }
        public string parentClassName { get; set; }
        
        public List<BusinessAttribute> Attributes { get; set; }
        public MergedBusinessDefintions Parent { get; set; }
        public BmideData Container { get; private set; }
        public MergedBusinessDefintions Storage { get; set; }
        public string StorageClassName { get; set; }
        public bool Abstract { get; set; }
    }
}