﻿
using System.Xml.Serialization;
using BMIDEImport.Model;

namespace BMIDEImport.Template.Model
{
    [XmlRoot("TcBusinessData")]
    public class TcBusinessData
    {
        [XmlElement("Add")]
        public TcBusinessDefinitions Definitions;


        public void SetSource(string sourceFile)
        {
            Definitions?.SetSource(sourceFile);
        }
        
        public void Merge(TcBusinessData other)
        {
            Definitions?.Merge(other.Definitions);
        }
    }
}