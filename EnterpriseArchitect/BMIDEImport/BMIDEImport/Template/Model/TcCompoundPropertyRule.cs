﻿using System.Xml.Serialization;

namespace BMIDEImport.Template.Model
{
    public class TcCompoundPropertyRule
    {
        [XmlAttribute("destPropertyName")]
        public string destpropertyName;

        [XmlAttribute("destTypeName")]
        public string desttypeName;

        [XmlAttribute("sourcePropertyName")]
        public string sourcepropertyName;

        [XmlAttribute("sourceTypeName")]
        public string sourcetypeName;
    }
}