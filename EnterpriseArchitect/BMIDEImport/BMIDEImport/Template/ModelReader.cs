﻿using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using BMIDEImport.Template.Dependencies;
using BMIDEImport.Template.Model;

namespace BMIDEImport.Template
{
    public class ModelReader
    {
        
        public static TcBusinessData Parse(string filename)
        {
            TcBusinessData result = null;
            
            var fileonly = filename.Remove(0, filename.LastIndexOf(Path.DirectorySeparatorChar)+1);
            var path = filename.Remove(filename.LastIndexOf(Path.DirectorySeparatorChar));
            
            var filesToRead = new List<string>();
            if (fileonly.Equals("master.xml"))
            {
                var includes = (TcBusinessDataIncludes) TcBusinessDataIncludes.Parse(filename);

                if (includes != null)
                {
                    foreach (var include in includes.Includes)
                    {
                        var datafile = path + Path.DirectorySeparatorChar + include.Include;
                        filesToRead.Add(datafile);
                    }
                }
            }
            else
            {
                filesToRead.Add(filename);
            }

            foreach (var fileToRead in filesToRead)
            {
                var serializer = new XmlSerializer(typeof(TcBusinessData), "http://teamcenter.com/BusinessModel/TcBusinessData");

                // A FileStream is needed to read the XML document.
                var fs = new FileStream(fileToRead, FileMode.Open, FileAccess.Read);
                var reader = XmlReader.Create(fs);


                // Use the Deserialize method to restore the object's state.
                var partResult = (TcBusinessData) serializer.Deserialize(reader);
                
                var sourceFile = fileToRead.Remove(0, fileToRead.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                sourceFile = sourceFile.Replace(".xml", "");
                partResult.SetSource(sourceFile);
                
                if (result == null)
                    result = partResult;
                else
                    result.Merge(partResult);

                fs.Close();
                reader.Close();
            }

            return result;
        }
        
        public static List<MergedBusinessDefintions> Merge(BmideData data, Dictionary<string, BmideData> packages)
        {
            var mbd = new List<MergedBusinessDefintions>();
            var bd = data.BusinessData;
            
            foreach (var t in bd.Definitions.StandardTypes)
            {

                if (t.Name.Contains("MasterS") == false)
                {
                    MergedBusinessDefintions tempData = new MergedBusinessDefintions(data);
                    tempData.className = t.Name;
                    tempData.classDescription = t.Description;
                    tempData.parentClassName = t.ParentTypeName;
                    tempData.Attributes = new List<BusinessAttribute>();
                    tempData.Abstract = t.isAbstract;
                    foreach(var prop in t.Properties)
                    {
                        BusinessAttribute tempBusinessAttr = new BusinessAttribute();
                        tempBusinessAttr.Name = prop.Name;
                        tempBusinessAttr.Type = prop.ValueType;
                        tempBusinessAttr.InitialValue = "";
                        tempBusinessAttr.Description = prop.Description;
                        tempBusinessAttr.ProperyType = prop.TypeName;
                       
                        tempData.Attributes.Add(tempBusinessAttr);
                    }

                    mbd.Add(tempData);
                }
            }
            foreach (var t in bd.Definitions.Classes)
            {
                if (t.Name.Contains("MasterS") == false)
                {
                    MergedBusinessDefintions results = mbd.Find(x => x.className == t.Name);
                    if(results != null)
                    {
                        if (results.classDescription == "")
                            results.classDescription = t.Description;
                       
                        var attrs = MapAttributes(t.Attributes);
                        MergeAttributes(attrs, results.Attributes);
                    }
                }
            }
            foreach(var t in bd.Definitions.Forms)
            {
                MergedBusinessDefintions tempData = new MergedBusinessDefintions(data);
                tempData.className = t.Name;
                tempData.classDescription = t.Description;
                tempData.parentClassName = t.ParentTypeName;
                tempData.Attributes = new List<BusinessAttribute>();
                tempData.Abstract = t.isAbstract;
                foreach (var prop in t.Properties)
                {
                    BusinessAttribute tempBusinessAttr = new BusinessAttribute();
                    tempBusinessAttr.Name = prop.Name;
                    tempBusinessAttr.Type = prop.ValueType;
                    tempBusinessAttr.InitialValue = "";
                    tempBusinessAttr.Description = prop.Description;
                    tempBusinessAttr.IsArray = prop.IsArray;
                    tempBusinessAttr.ProperyType = prop.TypeName;
                    
                    tempData.Attributes.Add(tempBusinessAttr);
                }
                if (t.formStorageClassName != null)
                {
                    TcClass storage = bd.Definitions.Classes.Find(x => x.Name.Equals(t.formStorageClassName));
                    if (storage != null)
                    {
                        foreach (var attr in storage.Attributes)
                        {
                            BusinessAttribute tempBusinessAttr = new BusinessAttribute();
                            tempBusinessAttr.Name = attr.Name;
                            tempBusinessAttr.Type = attr.Type;
                            tempBusinessAttr.MaxStringValue = attr.MaxStringValue;
                            tempBusinessAttr.LowerBound = attr.LowerBound;
                            tempBusinessAttr.UpperBound = attr.UpperBound;
                            tempBusinessAttr.InitialValue = "";
                            tempBusinessAttr.Description = attr.Description;
                            tempBusinessAttr.IsArray = attr.IsArray;
                            tempBusinessAttr.ProperyType = "Runtime";
                            tempData.Attributes.Add(tempBusinessAttr);
                        }
                    }
                    tempData.StorageClassName = t.formStorageClassName;
                }
                mbd.Add(tempData);
            }
            foreach(var t in bd.Definitions.Datasets)
            {
                MergedBusinessDefintions tempData = new MergedBusinessDefintions(data);
                tempData.className = t.Name;
                tempData.classDescription = t.Description;
                tempData.parentClassName = t.ParentTypeName;
                tempData.Attributes = new List<BusinessAttribute>();
                tempData.Abstract = t.isAbstract;
                foreach (var prop in t.Properties)
                {
                    BusinessAttribute tempBusinessAttr = new BusinessAttribute();
                    tempBusinessAttr.Name = prop.Name;
                    tempBusinessAttr.Type = prop.ValueType;
                    tempBusinessAttr.InitialValue = "";
                    tempBusinessAttr.Description = prop.Description;
                    tempBusinessAttr.IsArray = prop.IsArray;
                    tempData.Attributes.Add(tempBusinessAttr);
                }

                mbd.Add(tempData);
            }
            
            foreach (var att in bd.Definitions.CompoundRules)
            {
                var targetType = mbd.Find(x => x.className.Equals(att.desttypeName) );
                var sourceType = mbd.Find(x => x.className.Equals(att.sourcetypeName) );
                if (targetType != null && sourceType != null)
                {
                    var sourceAttr = sourceType.Attributes.Find(x => x.Name.Equals(att.sourcepropertyName));
                    if (sourceAttr != null)
                    {
                        BusinessAttribute tempBusinessAttr = new BusinessAttribute();
                        tempBusinessAttr.Name = att.destpropertyName;
                        tempBusinessAttr.Type = sourceAttr.Type;
                        tempBusinessAttr.MaxStringValue = sourceAttr.MaxStringValue;
                        tempBusinessAttr.LowerBound = sourceAttr.LowerBound;
                        tempBusinessAttr.UpperBound = sourceAttr.UpperBound;
                            
                        tempBusinessAttr.InitialValue = "";
                        tempBusinessAttr.Description =
                            "Compound Property " + att.sourcetypeName + "." + att.sourcepropertyName;
                        tempBusinessAttr.ProperyType = "Compound";

                        targetType.Attributes.Add(tempBusinessAttr);
                    }
                }
            }

            foreach (var attaches in bd.Definitions.AttributeAttaches)
            {
                var targetType = mbd.Find(x => x.className.Equals(attaches.ClassName) );
                if (targetType == null)
                {
                    foreach (var bmideData in packages.Values)
                    {
                        if (data == bmideData || bmideData.MergedData == null)
                            continue;
                        targetType = bmideData.MergedData.Find(x => x.className.Equals(attaches.ClassName) );
                        if (targetType != null)
                            break;
                    }
                }
                if (targetType != null)
                {
                    var mappedAttributes = MapAttributes(attaches.Attributes);
                    foreach (var att in mappedAttributes)
                    {
                        var localizationKey = "Property{::}" + targetType.className + "{::}" + att.Name;
                        if (data.Localizations.ContainsKey(localizationKey))
                            targetType.Container.Localizations[localizationKey] = data.Localizations[localizationKey];

                        var current = targetType.Attributes.Find(x => x.Name.Equals(att.Name));
                        if (current == null)
                        {
                            targetType.Attributes.Add(att);
                        }
                        else
                        {
                            targetType.Attributes.Remove(att);
                            targetType.Attributes.Add(att);
                        }
                    }
                }
            }
                        
            return mbd;
        }

        private static void MergeAttributes(IEnumerable<BusinessAttribute> from, List<BusinessAttribute> to)
        {
            foreach (var fromAttr in from)
            {
                var found = to.Find(x => x.Name.Equals(fromAttr.Name));
                if(found == null)
                    to.Add(fromAttr);
            }
        }


        private static List<BusinessAttribute> MapAttributes(List<TcAttribute> attrs) {
            var result = new List<BusinessAttribute>();
            foreach (var prop in attrs)
            {
                BusinessAttribute tempBusinessAttr = new BusinessAttribute();
                tempBusinessAttr.Name = prop.Name;
                tempBusinessAttr.Type = prop.Type;
                tempBusinessAttr.InitialValue = prop.InitialValue;
                tempBusinessAttr.Description = prop.Description;
                tempBusinessAttr.MaxStringValue = prop.MaxStringValue;
                tempBusinessAttr.LowerBound = prop.LowerBound;
                tempBusinessAttr.UpperBound = prop.UpperBound;
                tempBusinessAttr.IsArray = prop.IsArray;
                tempBusinessAttr.TypedRefClassName = prop.TypedRefClassName;                    
                    
                result.Add(tempBusinessAttr);
            }
            return result;
        }
    }
}