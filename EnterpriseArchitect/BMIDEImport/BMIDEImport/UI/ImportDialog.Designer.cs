﻿using System.Windows.Forms;

namespace BMIDEImport.UI
{
    partial class ImportDialogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows TcForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._lbModel = new System.Windows.Forms.Label();
            this._txModel = new System.Windows.Forms.ComboBox();
            this._btSelectModel = new System.Windows.Forms.Button();
            this._btSelectTemplateDir = new System.Windows.Forms.Button();
            this._txTemplateDir = new System.Windows.Forms.TextBox();
            this._lbTemplates = new System.Windows.Forms.Label();
            this._btImport = new System.Windows.Forms.Button();
            this.lstActions = new System.Windows.Forms.ListBox();
            this._lblText = new System.Windows.Forms.Label();
            this._pbProgressBar = new System.Windows.Forms.ProgressBar();
            this._lbEntity = new System.Windows.Forms.Label();
            this._btCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _lbModel
            // 
            this._lbModel.AutoSize = true;
            this._lbModel.Location = new System.Drawing.Point(6, 21);
            this._lbModel.Name = "_lbModel";
            this._lbModel.Size = new System.Drawing.Size(83, 13);
            this._lbModel.TabIndex = 0;
            this._lbModel.Text = "Model to Import:";
            // 
            // _txModel
            // 
            this._txModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txModel.Location = new System.Drawing.Point(111, 18);
            this._txModel.Name = "_txModel";
            this._txModel.Size = new System.Drawing.Size(451, 20);
            this._txModel.TabIndex = 1;
            this._txModel.TextChanged += new System.EventHandler(this.OnModelChanged);
            // 
            // _btSelectModel
            // 
            this._btSelectModel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btSelectModel.Location = new System.Drawing.Point(568, 17);
            this._btSelectModel.Name = "_btSelectModel";
            this._btSelectModel.Size = new System.Drawing.Size(33, 21);
            this._btSelectModel.TabIndex = 2;
            this._btSelectModel.Text = "...";
            this._btSelectModel.UseVisualStyleBackColor = true;
            this._btSelectModel.Click += new System.EventHandler(this.OnSelectModel);
            // 
            // _btSelectTemplateDir
            // 
            this._btSelectTemplateDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btSelectTemplateDir.Location = new System.Drawing.Point(568, 43);
            this._btSelectTemplateDir.Name = "_btSelectTemplateDir";
            this._btSelectTemplateDir.Size = new System.Drawing.Size(33, 21);
            this._btSelectTemplateDir.TabIndex = 5;
            this._btSelectTemplateDir.Text = "...";
            this._btSelectTemplateDir.UseVisualStyleBackColor = true;
            this._btSelectTemplateDir.Click += new System.EventHandler(this.OnSelectTemplateDir);
            // 
            // _txTemplateDir
            // 
            this._txTemplateDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txTemplateDir.Location = new System.Drawing.Point(111, 44);
            this._txTemplateDir.Name = "_txTemplateDir";
            this._txTemplateDir.Size = new System.Drawing.Size(451, 20);
            this._txTemplateDir.TabIndex = 4;
            this._txTemplateDir.TextChanged += new System.EventHandler(this.OnTemplateChanged);
            // 
            // _lbTemplates
            // 
            this._lbTemplates.AutoSize = true;
            this._lbTemplates.Location = new System.Drawing.Point(6, 47);
            this._lbTemplates.Name = "_lbTemplates";
            this._lbTemplates.Size = new System.Drawing.Size(107, 13);
            this._lbTemplates.TabIndex = 3;
            this._lbTemplates.Text = "BMIDE Template Dir:";
            // 
            // _btImport
            // 
            this._btImport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._btImport.Location = new System.Drawing.Point(304, 70);
            this._btImport.Name = "_btImport";
            this._btImport.Size = new System.Drawing.Size(126, 28);
            this._btImport.TabIndex = 6;
            this._btImport.Text = "Start Import";
            this._btImport.UseVisualStyleBackColor = true;
            this._btImport.Click += new System.EventHandler(this.OnImport);
            // 
            // lstActions
            // 
            this.lstActions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstActions.FormattingEnabled = true;
            this.lstActions.Location = new System.Drawing.Point(9, 169);
            this.lstActions.Name = "lstActions";
            this.lstActions.Size = new System.Drawing.Size(590, 433);
            this.lstActions.TabIndex = 7;
            // 
            // _lblText
            // 
            this._lblText.AutoSize = true;
            this._lblText.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this._lblText.Location = new System.Drawing.Point(6, 108);
            this._lblText.Name = "_lblText";
            this._lblText.Size = new System.Drawing.Size(37, 13);
            this._lblText.TabIndex = 8;
            this._lblText.Text = "Status";
            this._lblText.UseMnemonic = false;
            // 
            // _pbProgressBar
            // 
            this._pbProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._pbProgressBar.Location = new System.Drawing.Point(9, 124);
            this._pbProgressBar.Name = "_pbProgressBar";
            this._pbProgressBar.Size = new System.Drawing.Size(590, 22);
            this._pbProgressBar.TabIndex = 9;
            // 
            // _lbEntity
            // 
            this._lbEntity.AutoSize = true;
            this._lbEntity.Location = new System.Drawing.Point(12, 149);
            this._lbEntity.Name = "_lbEntity";
            this._lbEntity.Size = new System.Drawing.Size(16, 13);
            this._lbEntity.TabIndex = 10;
            this._lbEntity.Text = "...";
            // 
            // _btCancel
            // 
            this._btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btCancel.Location = new System.Drawing.Point(436, 70);
            this._btCancel.Name = "_btCancel";
            this._btCancel.Size = new System.Drawing.Size(126, 28);
            this._btCancel.TabIndex = 11;
            this._btCancel.Text = "Close";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this.OnCancel);
            // 
            // ImportDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 610);
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._lbEntity);
            this.Controls.Add(this._pbProgressBar);
            this.Controls.Add(this._lblText);
            this.Controls.Add(this.lstActions);
            this.Controls.Add(this._btImport);
            this.Controls.Add(this._btSelectTemplateDir);
            this.Controls.Add(this._txTemplateDir);
            this.Controls.Add(this._lbTemplates);
            this.Controls.Add(this._btSelectModel);
            this.Controls.Add(this._txModel);
            this.Controls.Add(this._lbModel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportDialogView";
            this.ShowIcon = false;
            this.Text = "Import BMIDE Model";
            this.Load += new System.EventHandler(this.ImportDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _lbModel;
        private ComboBox _txModel;
        private System.Windows.Forms.Button _btSelectModel;
        private System.Windows.Forms.Button _btSelectTemplateDir;
        private System.Windows.Forms.TextBox _txTemplateDir;
        private System.Windows.Forms.Label _lbTemplates;
        private System.Windows.Forms.Button _btImport;
        private System.Windows.Forms.ListBox lstActions;
        private System.Windows.Forms.Label _lblText;
        private System.Windows.Forms.ProgressBar _pbProgressBar;
        private System.Windows.Forms.Label _lbEntity;
        private System.Windows.Forms.Button _btCancel;
    }
}