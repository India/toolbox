﻿using System;

namespace BMIDEImport.UI.FileHandler
{
    public class FileHandler
    {
        public static IFile GetFile(string fileName)
        {
            if (fileName.EndsWith(".xml"))
            {
                return new RawFile(fileName);
            }

            if (fileName.EndsWith("_template.zip"))
            {
                return new ZipFile(fileName);
            }
            
            throw new ArgumentException();
        }
    }
}