﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using BMIDEImport.EaInterface;
using BMIDEImport.Template;
using Microsoft.Win32;

namespace BMIDEImport.UI
{
    public class ImportDialogController
    {

        private const string RegPath = @"SOFTWARE\Sparx Systems\EAAddins\BMIDE\";
        private const string ModelAttr = "PreviousModelFile";
        private const string TemplateAttr = "BMIDETemplateDir";
        private const int    History = 10;
        private const string FileFilter = @"Datamodel File (*_template.xml)|*_template.xml|BMIDE Template Package|*_template.zip|BMIDE Master Exctension|master.xml|BMIDE (Part) Extension File|*.xml|All files (*.*)|*.*";

        private bool _running;
        private List<ItemData> _models = new List<ItemData>();
        private ItemData _newSelected = null;

        private IEaRepository Repository { get; set; }
        private ImportDialogView View { get; set; }

        public class ItemData
        {
            public ItemData(string path)
            {
                this.Path = path;
                var idx = path.LastIndexOf(System.IO.Path.PathSeparator);
                if (idx > 0)
                {
                    this.Display = path.Substring(idx + 1) + " - (" + path.Substring(0, idx) + ")";
                }
                else
                {
                    this.Display = path;
                }
            }

            public string Display { get; private set; }
            public string Path { get; }

            public override string ToString()
            {
                return Display;
            }

            public override bool Equals(object obj)
            {
                var other = obj as ItemData;
                return other != null && ((ItemData)obj).Path.Equals(Path);
            }

            public override int GetHashCode()
            {
                return Path.GetHashCode();
            }
        }
       
        public class CancelledException : Exception
        {
        }

        public ImportDialogController(IEaRepository repository, ImportDialogView view)
        {
            Repository = repository;
            View = view;
            InitEvents();
            InitFromRegistry();
        }

        private void InitEvents()
        {
            View.ModelChanged += OnCheckImportAllowed;
            View.TemplateChanged += OnCheckImportAllowed;
            View.SelectModel += OnSelectModel;
            View.SelectTemplate += OnSelectTemplateDir;
            View.Import += OnImport;
            View.ImportFinished += OnImportFinished;
            View.Cancel += OnCancel;
        }

        private void OnCheckImportAllowed(object sender, EventArgs e)
        {
            var selectedItem = View.SelectedModel ?? this._newSelected;

            if (string.IsNullOrEmpty(View.TemplateDir) == false &&
                selectedItem != null &&
                Directory.Exists(View.TemplateDir) &&
                System.IO.File.Exists(selectedItem.Path))
            {
                View.AllowImport = true;
            }
            else
            {
                View.AllowImport = false;
            }
        }
        private void OnSelectModel(object sender, EventArgs e)
        {
            using (var dlg = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = FileFilter,
                Title = "Select BMIDE Package File to Import"
            })
            {
                dlg.FileName = View.SelectedModel != null ? ((ImportDialogController.ItemData) View.SelectedModel).Path : View.ModelText;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    this._newSelected = new ImportDialogController.ItemData(dlg.FileName);
                    View.SelectedModel = null;
                    View.ModelText = _newSelected.Display;
                }
            }
        }
        private void OnSelectTemplateDir(object sender, EventArgs e)
        {
            using (var dlg = new FolderBrowserDialog())
            {
                dlg.SelectedPath = View.TemplateText;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    View.TemplateText = dlg.SelectedPath;
                }
            }
        }
        
        private void OnImport(object sender, EventArgs e)
        {
            View.EnableEntries(false);
            View.EnableProgress(true);

            var selectedItem = View.SelectedModel;
            if (selectedItem == null)
            {
                selectedItem = this._newSelected;
            }
            else
            {
                this._newSelected = null;
            }

            var registry = Registry.CurrentUser;
            
            SaveHistory(registry, selectedItem, History);

            registry.SetValue(RegPath + TemplateAttr, View.TemplateText);
            var templ = View.TemplateText;
            if (templ.EndsWith(@"\") == false)
                templ += @"\";
            var command = new BmideImportCommand();
            
            var file = FileHandler.FileHandler.GetFile(selectedItem.Path);
            using (file)
            {
                var bmideData = BmideData.ReadBmidePackage(file.ImportFilePath, templ);

                View.Cancelled = false;
                this._running = true;
                View.ClearLogs();
                
                // TODO: generalize extensions!
                RegisterExternalEvents(command);
                command.Execute(new BmideImportCommand.EaData(Repository), bmideData, View);
            }
        }
        
        private void OnImportFinished(object sender, EventArgs e)
        {
            this._running = false;
            View.EnableEntries(true);
            View.EnableProgress(false);
        }
        
        private void OnCancel(object sender, EventArgs e)
        {
            if (_running)
            {
                View.Cancelled = true;
                this._running = false;
                View.EnableEntries(true);
                View.EnableProgress(false);
            }
            else
                View.Close();
        }


        private static void RegisterExternalEvents(BmideImportCommand command)
        {
            new BSH.BshStereotypeMapper().RegisterEvents(command);
        }


        private void InitFromRegistry()
        {
            var registry = Registry.CurrentUser;
            _models = GetStringsFromRegistry(registry, ModelAttr, History);
            if (_models.Count > 0)
            {
                //_txModel.Text = (string) registry.GetValue(cRegPath + cModelAttr, "");
                for (var i = 0; i < _models.Count; i++)
                {
                    View.AddModel(_models[i]);
                }

                View.SelectedModel = _models[0];

            }

            View.TemplateDir = (string) registry.GetValue(RegPath + TemplateAttr, "");
            OnCheckImportAllowed(this, null);
        }
        
        private static List<ItemData> GetStringsFromRegistry(RegistryKey registry, string previousmodelfile, int max)
        {
            var result = new List<ItemData>();
            
            for(int i=0; i<max; i++)
            {
                var regKey = RegPath + ModelAttr;
                var value = "";
                if(i>0)
                {
                    regKey += i.ToString();
                }
                
                value = (string) registry.GetValue(regKey, "");
                
                if (string.IsNullOrEmpty(value) == false)
                {
                    result.Add(new ItemData(value));
                }
            }
            return result;
        }

        private void SaveHistory(RegistryKey registry, ItemData selected, int max)
        {
            var foundIdx = -1;
            for (var i = 0; i < _models.Count; i++)
            {
                if (selected.Equals(_models[i]))
                {
                    foundIdx = i;
                }               
            }
            
            if(foundIdx >= 0)
                _models.RemoveAt(foundIdx);
            _models.Insert(0, selected);

            var maxEntries = _models.Count > max ? max : _models.Count;
            for (var i = 0; i < maxEntries; i++)
            {
                var regKey = RegPath + ModelAttr;
                if(i>0)
                    regKey += i.ToString();
                registry.SetValue(regKey, _models[i].Path);
            }
        }
    }
}