﻿using BMIDEImport.Template;

namespace BMIDEImport
{
    public class ImportEventArgs
    {
        public BmideImportCommand.EaData EaData { get; set; }
        public Model.Model ImportModel { get; set; }
        public BmideData BmideData { get; set; }
    }
}