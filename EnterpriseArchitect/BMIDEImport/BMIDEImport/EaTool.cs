﻿using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Linq;
using System.Runtime.InteropServices;
using BMIDEImport.EaInterface;
using BMIDEImport.Model;

namespace BMIDEImport
{
    public class EaTool
    {
        public enum ConnectorType
        {
            None,
            Aggregation,
            Composite,
            Generalization
        }

        private static string ConnType(ConnectorType type)
        {
            switch (type)
            {
                case ConnectorType.None:
                    return "Association";
                case ConnectorType.Aggregation:
                    return "Aggregation";
                case ConnectorType.Composite:
                    return "Aggregation";
                case ConnectorType.Generalization:
                    return "Generalization";
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

        }

        public static IEaConnector CreateOrUpdateConnector(ModelRelation relation,
            IProgress<ProgressInfo> progressIndicator)
        {
            var primary = relation.Primary.EaClass;
            var secondary = relation.Secondary.EaClass;
            var aggType = relation.Connector;
            var name = relation.Name;

            IEaConnector connector = LocateConnector(primary, secondary, aggType, name);
            if (connector == null)
            {
                relation.Status = ModelPart.EStatus.New;
                connector = CreateConnector(primary, secondary, name, aggType);
                progressIndicator?.Report(new ProgressInfo("Add relation " + relation.ToString()));
            }

            relation.EaConnector = connector;

            UpdateConnector(connector, relation, progressIndicator);

            return connector;
        }
        
        private static void UpdateConnector(IEaConnector connector, ModelRelation data, IProgress<ProgressInfo> progressIndicator)
        {
                           
            data.ReportUpdate(data.Update( "PrimaryCardinality", 
                connector.ClientEnd.Cardinality, 
                TranslateCardinality(data.PrimaryCardinality), x => { connector.ClientEnd.Cardinality = x; }), progressIndicator);
            data.ReportUpdate(data.Update( "SecondaryCardinality", 
                connector.SupplierEnd.Cardinality, 
                TranslateCardinality(data.SecondaryCardinality), x => { connector.SupplierEnd.Cardinality = x; }), progressIndicator);

            UpdateStereotype(connector, data, progressIndicator);
            
            if (data.Attribute != null)
            {
                string style = "LFSP=" + data.Attribute.EaAttribute.AttributeGUID + "R;";
                connector.StyleEx = style;

                connector.Name = ""; // reset name, which might be set on creation
                data.ReportUpdate(
                    data.Update("Role", connector.ClientEnd.Role, data.Attribute.Name, x => { connector.ClientEnd.Role = x; }),
                    progressIndicator);

                if (string.IsNullOrEmpty(data.Attribute.Alias) == false)
                {
                    data.ReportUpdate(
                        data.Update("Alias", connector.ClientEnd.Alias, data.Attribute.Alias, x => { connector.ClientEnd.Alias = x; }),
                        progressIndicator);
                }
            }

            if (data.Relation != null)
            {
                connector.Name = ""; // reset name, which might be set on creation
                data.ReportUpdate(
                    data.Update("Role", connector.ClientEnd.Role, data.Relation.Name,
                        x => { connector.ClientEnd.Role = x; }),
                    progressIndicator);
                if (string.IsNullOrEmpty(data.Relation.Alias) == false)
                {
                    data.ReportUpdate(
                        data.Update("Alias", connector.ClientEnd.Alias, data.Relation.Alias,
                            x => { connector.ClientEnd.Alias = x; }),
                        progressIndicator);
                }
            }

            switch (data.Connector)
            {
                case ConnectorType.None:
                    if(data.Attachability != null)
                        data.ReportUpdate(data.UpdateTaggedValues("Attachability", data.Attachability), progressIndicator);
                    if(data.Detachability != null)
                        data.ReportUpdate(data.UpdateTaggedValues("Detachability", data.Detachability), progressIndicator);
                    if(data.Changeability != null)
                        data.ReportUpdate(data.UpdateTaggedValues("Changeability", data.Changeability), progressIndicator);
                    //UpdateDirection(connector, data, progressIndicator);
                    data.ReportUpdate(data.Update("Primary Navigability",
                        connector.ClientEnd.IsNavigable,
                        false, x => { connector.ClientEnd.IsNavigable = x;}), progressIndicator);
                    data.ReportUpdate(data.Update("Secondary Navigability",
                        connector.SupplierEnd.IsNavigable,
                        true, x => { connector.SupplierEnd.IsNavigable = x;}), progressIndicator);
                    connector.ClientEnd.Update();
                    connector.SupplierEnd.Update();
                    connector.Update(); 
                    break;
                case ConnectorType.Aggregation:
                    data.ReportUpdate(data.Update("Connector",
                        connector.ClientEnd.Aggregation,
                        1, x => { connector.ClientEnd.Aggregation = x; }), progressIndicator);
                    data.ReportUpdate(data.Update("Primary Navigability",
                        connector.ClientEnd.Navigable,
                        "Unspecified", x => { connector.ClientEnd.Navigable = x;}), progressIndicator);
                    data.ReportUpdate(data.Update("Secondary Navigability",
                        connector.SupplierEnd.Navigable,
                        "Navigable", x => { connector.SupplierEnd.Navigable = x;}), progressIndicator);
                    connector.ClientEnd.Update();
                    connector.SupplierEnd.Update();
                    connector.Update();  
                    break;
                case ConnectorType.Composite:
                    data.ReportUpdate(data.Update("Connector",
                        connector.ClientEnd.Aggregation,
                        2, x => { connector.ClientEnd.Aggregation = x; }), progressIndicator);
                    data.ReportUpdate(data.Update("Primary Navigability",
                        connector.ClientEnd.Navigable,
                        "Unspecified", x => { connector.ClientEnd.Navigable = x;}), progressIndicator);
                    data.ReportUpdate(data.Update("Secondary Navigability",
                        connector.SupplierEnd.Navigable,
                        "Navigable", x => { connector.SupplierEnd.Navigable = x;}), progressIndicator);
                    connector.ClientEnd.Update();
                    connector.SupplierEnd.Update();
                    connector.Update();  
                    break;
                case ConnectorType.Generalization:
                    break;        
                default:
                    throw new ArgumentOutOfRangeException();
            }
            connector.ClientEnd.Update();
            connector.SupplierEnd.Update();
            connector.Update();  
        }

        private static void UpdateStereotype(IEaConnector connector, ModelPart data, IProgress<ProgressInfo> progressIndicator)
        {
            if (string.IsNullOrEmpty(data.StereoType) == false)
            {
                data.ReportUpdate(data.Update("Stereotype",
                    connector.Stereotype,
                    data.StereoType, x => { connector.Stereotype = x; }), progressIndicator);
            }
        }

        private static IEaConnector CreateConnector(IEaElement primary, IEaElement secondary, string name, ConnectorType aggType)
        {
            var result = primary.AddNewConnector(name, ConnType(aggType));

            result.SupplierID = secondary.ElementID;
            result.Update();

            return result;
        }

        private static IEaConnector LocateConnector(IEaElement primary, IEaElement secondary, ConnectorType aggType, string name )
        {
            EaInterface.IEaConnector result = null;
            var toDelete = new List<IEaConnector>();
            
            var connectorType = ConnType(aggType);

            foreach (var conn in primary.Connectors)
            {
                var t1 = conn.Type;
                var t2 = conn.SupplierID;
                var t3 = secondary.ElementID;
                var t4 = conn.Name;
                var t5 = conn.ClientEnd.Role;
                
                if (conn.Type.Equals(connectorType)
                    && conn.SupplierID == secondary.ElementID
                    && (aggType == ConnectorType.Generalization || conn.Name.Equals(name) || conn.ClientEnd.Role.Equals(name)
                        || (string.IsNullOrEmpty(conn.Name) && string.IsNullOrEmpty(conn.ClientEnd.Role))))
                {
                    if (result != null)
                    {
                        // Duplicat detection
                        if (conn.ConnectorId < result.ConnectorId)
                        {
                            toDelete.Add(result);
                            result = conn;
                        }
                        else
                        {
                            toDelete.Add(conn);
                        }
                    }
                    else
                      result = conn;
//                    break;
                }
            }

            foreach (var conn in toDelete)
            {
                primary.DeleteConnector(conn);
            }

            return result;
        }
        
        private static string TranslateCardinality(int cardinality)
        {
            if (cardinality == -1)
                return "*";
            return cardinality.ToString();
        }


    }
}