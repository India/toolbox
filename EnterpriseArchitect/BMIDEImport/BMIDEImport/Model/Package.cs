﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Security;

namespace BMIDEImport.Model
{
    public class Package : ModelPart
    {
        public Package(Model model, Package parent, string name, Template template) : base(model)
        {
            Name = name;
            Template = template;
            Parent = parent;
        }

        public Package Parent { get; private set; }

        public string Name { get; private set; }
        public Template Template { get; private set; }

        private Boolean counted = false;
        public int Count
        {
            get
            {
                int result = 0;
                if (counted == false)
                {
                    result += 1;
                    foreach (var pkg in subPackages.Values)
                        result += pkg.Count;
                }

                return result;
            }
        }

        public Package CreateOrGetSubPackage(string packageName)
        {
            Package result = null;
            if (subPackages.ContainsKey(packageName))
                result = subPackages[packageName];
            else
            {
                result = new Package(Model, this, packageName, Template);
                subPackages.Add(packageName, result);
            }
            
            return result;
        }
        
        private Dictionary<string, Package> subPackages = new Dictionary<string, Package>();

        /// <summary>
        /// Returns name of package
        /// </summary>
        /// <returns>name of package</returns>
        public override string ToString()
        {
            return Name;
        }
        
        /// <summary>
        /// Returns name of package, if requested including parent path
        /// </summary>
        /// <param name="fullPath">switch whether return full path or only package name</param>
        /// <returns>name of package</returns>
        public string ToString(bool fullPath)
        {
            var result = "";
            if (fullPath && Parent != null)
                result = Parent.ToString(true) + ".";
            return result + ToString();
        }

        public void Align(EaInterface.IEaPackage rootPackage, IProgress<ProgressInfo> progressIndicator)
        {
            if (EaPackage != null)
                return;

            EaInterface.IEaPackage containerPackage = rootPackage;
            if (Parent != null)
            {
                Parent.Align(rootPackage, progressIndicator);
                containerPackage = Parent.EaPackage;
            }

            progressIndicator?.Report(new ProgressInfo(Template.Name, Name));

            EaPackage = containerPackage.CreateOrGetPackage(Name);
        }

        public EaInterface.IEaPackage EaPackage { get; set; }
    }
}