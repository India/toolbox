﻿using System;
using System.Collections.Generic;
using BMIDEImport.Template;
using BMIDEImport.Template.Model;


namespace BMIDEImport.Model
{
    public class ModelClass : ModelClassLike
    {
        public ModelClass(Model model, MergedBusinessDefintions classDef, BmideData data, Template container)
            : base(model)
        {
            Template = container;
            InitFromData(classDef, data);
        }
        
        private readonly List<ModelRelation> _relations = new List<ModelRelation>();

        public ModelClass Parent      { get; private set; }
        public Package    Package     { get; private set; }
        public string     Alias       { get; private set; }
        public ModelClass Storage     { get; private set; }
        public bool Abstract          { get; set; }


        private void InitFromData(MergedBusinessDefintions classDef, BmideData data)
        {
            Name = classDef.className;
            Parent = Template.CreateOrGetClass(classDef.Parent, data);
            Storage = Template.CreateOrGetClass(classDef.Storage, data);
            Package = CreateOrGetContainerPackage(GetExternalParent());
            Description = classDef.classDescription;
            Abstract = classDef.Abstract;

            if (Parent != null)
            {
                new ModelGeneralization(Model, this, Parent);
            }

            if (Storage != null)
            {
                new ModelStorageRelation(this.Model, this, Storage);
            }

            var localizationKey = @"BusinessObject{::}" + classDef.className;
            if (data.Localizations.ContainsKey(localizationKey))
            {
                Alias = data.Localizations[localizationKey].Text;
            }

            foreach (var attr in classDef.Attributes)
            {
                CreateOrGetAttribute(attr.Name, data, attr);
            }


            foreach (var lovAttach in data.BusinessData.Definitions.TcLovAttach)
            {
                if (lovAttach.tcLOVAttachTypeName.Equals(Name))
                {
                    var attribute =
                        DuplicateOrGetAttribute(lovAttach.tcLOVAttachPropertyInfo.tcLOVAttachValuePropertyName);
                    var lov = Template.GetLov(lovAttach.tcLOVAttachLovName);

                    if (attribute != null && lov != null)
                        attribute.Lov = lov;
                }
            }
        }

        private ModelClass GetTopMostParent()
        {
            return Parent == null ? this : Parent.GetTopMostParent();
        }

        private ModelAttribute DuplicateOrGetAttribute(string name)
        {
            ModelAttribute result = Attributes.ContainsKey(name) ? Attributes[name] : null;
            if (result == null)
            {
                ModelAttribute inheritedAttribute = Parent?.GetAttributeFromHierarchy(name);
                if (inheritedAttribute != null)
                {
                    result = new ModelAttribute(Model, this,inheritedAttribute);
                    Attributes.Add(name, result);
                }
            }
            return result;
        }

        private ModelAttribute GetAttributeFromHierarchy(string name)
        {
            var result = Attributes.ContainsKey(name)
                ? Attributes[name]
                : Parent?.GetAttributeFromHierarchy(name);
            return result;
        }

        private ModelAttribute CreateOrGetAttribute(string name, BmideData data, BusinessAttribute attr)
        {
            ModelAttribute result = null;
            if (Attributes.ContainsKey(name))
                result = Attributes[name];
            else
            {
                result = new ModelAttribute(Model, data, this, attr);
                Attributes.Add(name, result);
            }
            return result;
        }

        private ModelClass GetExternalParent()
        {
            ModelClass result = Parent;
            if (result != null)
            {
                if (result.Template == this.Template)
                    result = result.GetExternalParent();
            }
            return result;
        }

        public override string ToString()
        {
            return Name;
        }

        protected override string UpdatePrefix => "Update class <" + ToString() + ">";

        public override void Align(EaInterface.IEaPackage rootPackage, IProgress<ProgressInfo> progressIndicator)
        {
            if (EaClass != null)
                return;
            
            Parent?.Align(rootPackage, progressIndicator);
            Storage?.Align(rootPackage, progressIndicator);
            Package.Align(rootPackage, progressIndicator);

            progressIndicator?.Report(new ProgressInfo(Template.Name, Name));
            var containerPackage = Package.EaPackage;

            EaClass = (EaInterface.IEaElement) containerPackage.GetElementByName(Name); 
            
            if (EaClass == null)
            {
                EaClass = containerPackage.AddNewElement(Name, "Class");            
                EaClass.Update();
                containerPackage.Update();
                containerPackage.RefreshElements();
                Model.logNew($"New Class: {Template.Name}::{Name}");
                Status = EStatus.New;
                progressIndicator?.Report(new ProgressInfo("Add class <" + Name + "> to " + Package.ToString(true)));
            }

            if (EaClass != null)
            {
                ReportUpdate(Update("Notes", EaClass.Notes, Description, x => { EaClass.Notes = x; }), progressIndicator);
                if (Alias != null)
                    ReportUpdate(Update("Alias", EaClass.Alias, Alias, x => { EaClass.Alias = x; }), progressIndicator);
                string abstractFlag = Abstract ? "1" : "0";
                ReportUpdate(Update("Abstract", EaClass.Abstract, abstractFlag, x => { EaClass.Abstract = x; }), progressIndicator);
                string stereotype = StereoType ?? "";
                if(string.IsNullOrEmpty(stereotype) == false)
                    ReportUpdate(Update("Stereotype", EaClass.Stereotype, stereotype, x => { EaClass.Stereotype = x; }), progressIndicator);

                EaClass.Update();

            }

            AlignAttributes(progressIndicator);
          
            foreach (var relation in _relations)
            {
                relation.Align(rootPackage, progressIndicator);
            }

        }

        public void AddRelation(ModelRelation relation)
        {
            _relations.Add(relation);
        }
        
        #region ProgressCounting

        public override int CountClasses()
        {
            var result =  base.CountClasses();
            if (result > 0)
            {
                if(Parent != null)
                    result += Parent.CountClasses();
                
                if(Storage != null)
                    result += Storage.CountClasses();

                foreach (var rel in _relations)
                {
                    if (rel.Secondary != null)
                        result += rel.CountClasses();
                }
            }

            return result;
        }

        #endregion
    }
}