﻿using System;
using BMIDEImport.EaInterface;

namespace BMIDEImport.Model
{
    public class ModelGeneralization
        : ModelRelation
    {
        public ModelGeneralization(Model model, ModelClass primary, ModelClass secondary)
            : base(model, primary, secondary, 1, 1, EaTool.ConnectorType.Generalization)
        {
        }

        public override void Align(IEaPackage rootPackage, IProgress<ProgressInfo> progressIndicator)
        {
            base.Align(rootPackage, progressIndicator);
        }

        public override string StereoType { get { return "Parent Relation"; } }

        public override string ToString()
        {
            return GetName(Primary)
                   + " --Genralization-->"
                   + GetName(Secondary);
        }

        public override string Name { get { return "";  } }
    }
}