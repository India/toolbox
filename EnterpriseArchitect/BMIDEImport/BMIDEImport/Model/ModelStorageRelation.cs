﻿using System;
using BMIDEImport.EaInterface;

namespace BMIDEImport.Model
{
    public class ModelStorageRelation : ModelRelation
    {
        public ModelStorageRelation(Model model, ModelClass primary, ModelClass secondary)
            : base(model, primary, secondary, 1, 1, EaTool.ConnectorType.Composite)
        {
        }

        public override void Align(IEaPackage rootPackage, IProgress<ProgressInfo> progressIndicator)
        {
            base.Align(rootPackage, progressIndicator);
        }

        public override string StereoType { get { return "Storage TcClass"; } }

        public override string ToString()
        {
            return GetName(Primary)
                            + "[" + PrimaryCardinality + "]"
                            + " --STORAGECLASS-->"
                            + GetName(Secondary)
                            + "[" + SecondaryCardinality + "]";
        }

        public override string Name { get { return "Storage";  } }
    }
}