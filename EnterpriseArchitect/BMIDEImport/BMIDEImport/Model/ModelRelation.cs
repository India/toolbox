﻿using System;
using BMIDEImport.EaInterface;
using BMIDEImport.Template;
using BMIDEImport.Template.Model;

namespace BMIDEImport.Model
{
    public class ModelRelation : ModelPart
    {
        private const string StereotypeUntypedref = "Untyped Reference";
        private const string StereotypeTypedref = "Typed Reference";
        private const string StereotypeUntypedrel = "Untyped Relation";
        private const string StereotypeLov = "LOV Attachment";

        public virtual string Name
        {
            get
            {
                if (Attribute != null)
                {
                    return Attribute.Name;
                }
                else if (Relation != null)
                {
                    return Relation.Name;
                }
                else
                {
                    return "";
                }
            }
        }

        public static void CreateRelation(Model model, BmideData data, TcGRMInfo tcGrm)
        {
            var primary = model.MainTemplate.GetClass(tcGrm.GRMprimaryTypeName);
            if (primary == null)
                return;
            var secondary = model.MainTemplate.CreateOrGetClass(tcGrm.GRMsecondaryTypeName, data);
            if (secondary == null)
                return;
            var relation = model.MainTemplate.CreateOrGetClass(tcGrm.GRMrelationTypeName, data);
            if (relation == null)
                return;
            new ModelRelation(model, tcGrm, primary, secondary, relation);
        }

        public ModelRelation(Model model, TcGRMInfo tcGrm, ModelClass primary, ModelClass secondary,
            ModelClass relation) : base(model)
        {
            InitFromData(tcGrm, primary, secondary, relation);
        }

        public ModelRelation(Model model, ModelAttribute attribute) : base(model)
        {
            InitFromModel(attribute, true);
        }

        public ModelRelation(Model model, ModelAttribute attribute, ModelLov primary) : base(model)
        {
            InitFromModel(attribute, false);
        }

        protected ModelRelation(Model model, ModelClass primary, ModelClass secondary, int primaryCardinality,
            int secondaryCardinality, EaTool.ConnectorType aggType) : base(model)
        {
            Primary = primary;
            Secondary = secondary;
            PrimaryCardinality = primaryCardinality;
            SecondaryCardinality = secondaryCardinality;
            Connector = aggType;

            Primary?.AddRelation(this);
        }

        private void InitFromModel(ModelAttribute attribute, bool asTypedReference)
        {
            if (attribute.Container is ModelClass == false)
                throw new ArgumentException();

            Primary = (ModelClass) attribute.Container;
            if (asTypedReference)
                Secondary = Model.MainTemplate.CreateOrGetClass(attribute.TypedRefClassName, Model.Data);
            else
                Secondary = attribute.Lov;

            PrimaryCardinality = -1;
            SecondaryCardinality = attribute.IsArray ? -1 : 1;
            Attribute = attribute;
            Connector = asTypedReference ? EaTool.ConnectorType.Aggregation : EaTool.ConnectorType.None;
            Primary?.AddRelation(this);
        }

        public EaTool.ConnectorType Connector { get; private set; }

        private void InitFromData(TcGRMInfo tcGrm, ModelClass primary, ModelClass secondary, ModelClass relation)
        {
            Primary = primary;
            Secondary = secondary;
            Relation = relation;

            Attachability = tcGrm.GRMattachability;
            Detachability = tcGrm.GRMdetachability;
            Changeability = tcGrm.GRMchangeability;
            PrimaryCardinality = int.Parse(tcGrm.GRMprimaryCardinality);
            SecondaryCardinality = int.Parse(tcGrm.GRMsecondaryCardinality);

            Primary?.AddRelation(this);
        }

        public int SecondaryCardinality { get; private set; }
        public int PrimaryCardinality { get; private set; }
        public string Changeability { get; private set; }
        public string Detachability { get; private set; }
        public string Attachability { get; private set; }
        public ModelClass Relation { get; set; }
        public ModelClassLike Secondary { get; private set; }
        public ModelClass Primary { get; private set; }
        public ModelAttribute Attribute { get; private set; }

        public override string ToString()
        {
            var result = "";

            if (Attribute != null)
            {
                result = GetName(Primary) + "." + Attribute.ToString();
            }
            else if (Relation != null)
            {
                result = GetName(Primary)
                         + "[" + PrimaryCardinality + "]"
                         + " --[" + GetName(Relation) + "]-->"
                         + GetName(Secondary)
                         + "[" + SecondaryCardinality + "]";
            }

            return result;
        }

        protected static string GetName(ModelClassLike cls)
        {
            return cls == null ? "*" : cls.Name;
        }

        protected override string UpdatePrefix => "Update Relation " + ToString();

        public virtual void Align(IEaPackage rootPackage, IProgress<ProgressInfo> progressIndicator)
        {
            if (Primary == null || Secondary == null)
                return;
            if (PrimaryCardinality == 0 || SecondaryCardinality == 0)
                return;

            Primary.Align(rootPackage, progressIndicator);
            Secondary.Align(rootPackage, progressIndicator);
            Relation?.Align(rootPackage, progressIndicator);

            this.EaConnector = EaTool.CreateOrUpdateConnector(this, progressIndicator);
        }

        public string UpdateTaggedValues(string key, string value)
        {
            string result = "";

            var taggedValues = EaConnector.TaggedValues;

            EaInterface.IEaConnectorTag tval = null;
            foreach (var tag in taggedValues)
            {
                if (tag == null)
                    break;
                if (tag.Name.Equals(key))
                {
                    tval = tag;
                    break;
                }
            }


            if (tval == null)
            {
                var newVal = EaConnector.AddTaggedValue(key, value);
                EaConnector.Update();

                if (newVal != null)
                {
                    newVal.Update();
                    tval = newVal;

                    result = $"New {key}: {value}";
                    if (Status != EStatus.New)
                        Status = EStatus.Updated;
                }
                else
                {
                    newVal = null; // HÄH?
                }
            }
            else
            {
                var current = tval.Value;
                if (value == null)
                    value = "";

                if (current.Equals(value) == false)
                {
                    tval.Value = value;
                    tval.Update();

                    result = $"Update {key}: '{current}' -> '{value}'";
                    if (Status != EStatus.New)
                        Status = EStatus.Updated;
                }
            }


            return result;
        }

        public IEaConnector EaConnector { get; set; }

        public override string StereoType
        {
            get
            {
                string result = null;
                if (Relation != null)
                    result = StereotypeUntypedrel;

                if (Attribute != null)
                {
                    if (Attribute.Type.Contains(ModelAttribute.TypeTypedref))
                    {
                        result = StereotypeTypedref;
                    }
                    else if (Attribute.Type.Contains(ModelAttribute.TypeUnTypedref))
                    {
                        result = StereotypeUntypedref;
                    }
                    else
                    {
                        result = StereotypeLov;
                    }
                }

                return result;
            }
        }
        
        #region ProgressCounting

        public override int CountClasses()
        {
            int result = 0;
            if(Secondary != null)
                result += Secondary.CountClasses();
            if (Relation != null)
                result += Relation.CountClasses();
            return result;
        }

        #endregion
    }
}