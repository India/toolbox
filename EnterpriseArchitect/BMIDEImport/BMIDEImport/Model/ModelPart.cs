﻿using System;

namespace BMIDEImport.Model
{
    public abstract class ModelPart
    {
        public enum EStatus
        {
            New,
            Updated,
            UpToDate
        }
        
        public Model Model { get; private set; }
        public EStatus Status { get;  set; }
        
        protected ModelPart(Model model)
        {
            Model = model;
            Status = EStatus.UpToDate;
        }
        
        public string Update<T>(string eaProp, T current, T value, Action<T> property)
        {
            var result = "";
            var update = true;

            if (current == null)
                update = value != null;
            else 
                update = current.Equals(value) == false && value != null;

            if(update)
            {
                result = $"Updated {eaProp}: {current} => {value}\n";
                property(value);
                if(Status != EStatus.New)
                    Status = EStatus.Updated;
            }

            return result;
        }
                
        public virtual void ReportUpdate(string msg, IProgress<ProgressInfo> progressIndicator)
        {
            if (Status != EStatus.New && String.IsNullOrEmpty(msg) == false)
                progressIndicator?.Report(new ProgressInfo(UpdatePrefix + " : " + msg));
        }

        protected virtual string UpdatePrefix => "Update " + ToString();
        
        public virtual string StereoType
        {
            get { return null; }
            set { throw new NotImplementedException(); }
        }

        #region ProgressCounting


        public virtual int CountClasses()
        {
            return 0;
        }


        #endregion

    }
}