﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMIDEImport.Template;
using BMIDEImport.Template.Model;

namespace BMIDEImport.Model
{
    public class ModelLov : ModelClassLike
    {
        private List<ModelAttribute> entries = new List<ModelAttribute>();
    
        public ModelLov(Model model, Template template, TcLOV lov, BmideData data) : base(model)
        {
            InitFromData(template, lov, data);
        }

        private void InitFromData(Template template, TcLOV lov, BmideData data)
        {
            Template = template;
            Name = lov.lovName;
            Description = lov.lovdescription;
            
            foreach (var value in lov.tcLOVValues)
            {
                entries.Add(new ModelAttribute(Model, this, value, data));
            }

        }

        public override string ToString()
        {
            return Name + " - " + Description;
        }

        public override void Align(EaInterface.IEaPackage package, IProgress<ProgressInfo> progressIndicator)
        {
            if (EaClass != null)
                return;
            progressIndicator?.Report(new ProgressInfo(Template.Name, "LOV: " + Name));

            Template.LovPackage.Align(package, progressIndicator);
            
            var container = Template.LovPackage.EaPackage;
            try
            {
                EaClass = (EaInterface.IEaElement) container.GetElementByName(Name); // Elements.GetByName(Name);
            }
            catch (Exception)
            {}
            
            if (EaClass == null)
            {
                EaClass = container.AddNewElement(Name, "Enumeration");            
                EaClass.Update();
                Model.logNew($"New Lov: {Template.Name}::{Name}");
                Status = EStatus.New;
                progressIndicator?.Report(new ProgressInfo("Add class(lov) <" + Name + "> to " + Template.LovPackage.ToString(true)));
            }

            if (EaClass != null)
            {
                var updateText = new StringBuilder();

                updateText.Append(Update( "Notes", EaClass.Notes, Description, x => { EaClass.Notes = x; }));
                
                string stereotype = StereoType ?? "";
                if(string.IsNullOrEmpty(stereotype) == false)
                    ReportUpdate(Update("Stereotype", EaClass.Stereotype, stereotype, x => { EaClass.Stereotype = x; }), progressIndicator);

                EaClass.Update();

                AlignAttributes(progressIndicator);
            }
        }

        private new void AlignAttributes(IProgress<ProgressInfo> progressIndicator)
        {
            Dictionary<string, EaInterface.IEaAttribute> eaAttributes = EaClass.Attributes.ToDictionary(attr => attr.Name);

            foreach (var attr in entries)
            {
                attr.Align(progressIndicator, eaAttributes);
            }
        }

    }
}