﻿using System.Collections.Generic;

namespace BMIDEImport.EaInterface
{
    public interface IEaPackage : IEaObject
    {
        IEaPackage CreateOrGetPackage(string name);

        string Name { get; }
        IEnumerable<IEaPackage> Packages { get; }

        IEaElement AddNewElement(string name, string type);
        IEaElement GetElementByName(string name);
        
        void RefreshElements();
    }
}