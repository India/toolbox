﻿using BMIDEImport.EaInterface;

namespace EAInterface.EaInterface.MockEa
{
    public class MockEaAttribute : IEaAttribute
    {
        public bool Dirty { get; private set; }

        public bool Update()
        {
            Dirty = false;
            return true;
        }

        private string _name;
        private string _type;
        private string _notes;
        private string _default;
        private string _alias;
        private string _upperBound;
        private string _lowerBound;
        private string _stereotype;
        private string _attributeGuid;
        private Scope _visibility;

        public MockEaAttribute(string name, string type)
        {
            _name = name;
            _type = type;
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                Dirty = true;
            }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string Notes
        {
            get { return _notes; }
            set { 
                _notes = value;
                Dirty = true;
            }
        }

        public string Default
        {
            get { return _default; }
            set
            {
                _default = value;
                Dirty = true;
            }
        }

        public string Alias
        {
            get { return _alias; }
            set
            {
                _alias = value;
                Dirty = true;

            }
        }

        public string UpperBound
        {
            get { return _upperBound; }
            set
            {
                _upperBound = value;
                Dirty = true;

            }
        }

        public string LowerBound
        {
            get { return _lowerBound; }
            set
            {
                _lowerBound = value;
                Dirty = true;

            }
        }

        public string Stereotype
        {
            get { return _stereotype; }
            set
            {
                _stereotype = value;
                Dirty = true;

            }
        }

        public string AttributeGUID
        {
            get { return _attributeGuid; }
            set
            {
                _attributeGuid = value;
                Dirty = true;

            }
        }

        public Scope Visibility {
            get { return _visibility; }
            set
            {
                _visibility = value;
                Dirty = true;

            }
        }
    }
}