﻿using BMIDEImport.EaInterface;

// ReSharper disable once CheckNamespace
namespace EAInterface.EaInterface.MockEa
{
    public class MockEaConnectorTag : IEaConnectorTag
    {
        public bool Dirty { get; private set; }

        public bool Update()
        {
            Dirty = false;
            return true;
        }

        public MockEaConnectorTag(string name, string value)
        {
            _name = name;
            _value = value;
        }
        
        private string _name;
        private string _value;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                Dirty = true;
            }
        }

        public string Value
        {
            get { return _value; }
            set
            {
                _value = value; 
                Dirty = true;
            }
        }
    }
}