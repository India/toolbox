﻿using BMIDEImport.EaInterface;

namespace EAInterface.EaInterface.MockEa
{
    public class MockEaConnectorEnd : IEaConnectorEnd
    {
        public bool Dirty { get; private set; }

        public bool Update()
        {
            Dirty = false;
            return true;
        }
        
        private string _cardinality;
        private bool _isNavigable;
        private int _aggregation;
        private string _navigable;
        private string _alias;

        public string Cardinality
        {
            get { return _cardinality; }
            set
            {
                _cardinality = value;
                Dirty = true;
            }
        }

        public bool IsNavigable
        {
            get { return _isNavigable; }
            set
            {
                _isNavigable = value; 
                Dirty = true;
            }
        }

        public int Aggregation
        {
            get { return _aggregation; }
            set
            {
                _aggregation = value; 
                Dirty = true;
            }
        }

        public string Navigable
        {
            get { return _navigable; }
            set
            {
                _navigable = value; 
                Dirty = true;
            }
        }

        public string Alias
        {
            get { return _alias;}
            set
            {
                _alias = value;
                Dirty = true;
            }
        }

        public string Role { get; set; }
    }
}