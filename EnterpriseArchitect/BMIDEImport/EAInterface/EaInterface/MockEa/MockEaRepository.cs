﻿using BMIDEImport.EaInterface;

namespace EAInterface.EaInterface.MockEa
{
    public class MockEaRepository : IEaRepository
    {
        private EaObjectType _type;
        private IEaPackage _package;

        public MockEaRepository(EaObjectType type, IEaPackage package)
        {
            _type = type;
            _package = package;
        }
        
        public EaObjectType GetTreeSelectedItemType()
        {
            return _type;
        }

        public IEaPackage GetTreeSelectedPackage()
        {
            return _package;
        }
    }
}