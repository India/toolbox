﻿using System.Collections.Generic;
using BMIDEImport.EaInterface;

namespace EAInterface.EaInterface.MockEa
{
    public class MockEaElement : IEaElement
    {
        private static int IdCounter = 1;
        public bool Dirty { get; private set; }

        public bool Update()
        {
            Dirty = false;
            return true;
        }

        private string _notes;
        private int _elementId;
        private string _alias;
        private string _abstract;
        private List<IEaAttribute> _attributes = new List<IEaAttribute>();
        private List<IEaConnector> _connectors = new List<IEaConnector>();
        private string _stereotype;

        public MockEaElement(string name, string type)
        {
            _elementId = IdCounter++;
        }

        public string Notes
        {
            get { return _notes; }
            set
            {
                _notes = value;
                Dirty = true;
            }
        }

        public int ElementID
        {
            get { return _elementId; }
            set
            {
                _elementId = value;
                Dirty = true;
            }
        }

        public string Alias
        {
            get { return _alias; }
            set
            {
                _alias = value;
                Dirty = true;
            }
        }

        public string Abstract
        {
            get { return _abstract; }
            set
            {
                _abstract = value; 
                Dirty = true;
            }
        }

        public IEnumerable<IEaAttribute> Attributes => _attributes;

        public IEnumerable<IEaConnector> Connectors => _connectors;

        public string Stereotype
        {
            get { return _stereotype; }
            set
            {
                _stereotype = value; 
                Dirty = true;
            }
        }
        
        public IEaAttribute AddAttribute(string name, string type)
        {
            var result = new MockEaAttribute(name, type);
            _attributes.Add(result);
            Dirty = true;
            return result;
        }

        public IEaConnector AddNewConnector(string name, string connType)
        {
            var result = new MockEaConnector(name, connType);
            _connectors.Add(result);
            Dirty = true;
            return result;
        }

        public void DeleteConnector(IEaConnector conn)
        {
            throw new System.NotImplementedException();
        }
    }
}