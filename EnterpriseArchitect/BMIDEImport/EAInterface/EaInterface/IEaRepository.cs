﻿using EA;

namespace BMIDEImport.EaInterface
{
    public interface IEaRepository
    {
        EaObjectType GetTreeSelectedItemType();
        IEaPackage GetTreeSelectedPackage();
    }
}