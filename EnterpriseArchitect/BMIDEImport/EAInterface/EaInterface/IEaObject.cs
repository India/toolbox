﻿using System.Data;

namespace BMIDEImport.EaInterface
{
    public interface IEaObject
    {
        bool Update();
    }
}