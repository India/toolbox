﻿using System.Collections.Generic;
using System.Linq;
using EA;

namespace BMIDEImport.EaInterface.impl
{
    public class EaConnector : IEaConnector
    {
        private readonly EaFactory _eaFactory; 
        private readonly Connector _eaConnector;
        
        public EaConnector(EaFactory factory, Connector connector)
        {
            _eaFactory = factory;
            _eaConnector = connector;
        }

        public bool Update()
        {
            return _eaConnector.Update();
        }

        public string Type => _eaConnector.Type;
        public string Name
        {
            get { return _eaConnector.Name; }
            set { _eaConnector.Name = value; }
        }

        public int SupplierID
        {
            get { return _eaConnector.SupplierID; }
            set { _eaConnector.SupplierID = value; }
        }

        public string Stereotype
        {
            get { return _eaConnector.Stereotype; }
            set { _eaConnector.Stereotype = value; }
        }

        public string StyleEx
        {
            get { return _eaConnector.StyleEx; }
            set { _eaConnector.StyleEx = value; }
        }

        public string Alias
        {
            get { return _eaConnector.Alias;}
            set { _eaConnector.Alias = value; }
        }

        public IEnumerable<IEaConnectorTag> TaggedValues
        {
            get
            {
                //var result = from TaggedValue tv in _eaConnector.TaggedValues select _eaFactory.GetConnectorTag(tv);
                var result = new List<IEaConnectorTag>();
                foreach (var conn in _eaConnector.TaggedValues)
                {
                    EA.TaggedValue tv = conn as EA.TaggedValue;
                    result.Add(_eaFactory.GetConnectorTag(tv));
                }
                return result;
            }
        }
       
        public IEaConnectorTag AddTaggedValue(string key, string value)
        {
            return _eaFactory.GetConnectorTag(_eaConnector.TaggedValues.AddNew(key, value) as TaggedValue);
        }

        public int ConnectorId => _eaConnector.ConnectorID;

        public IEaConnectorEnd ClientEnd => _eaFactory.GetConnectorEnd(_eaConnector.ClientEnd);
        public IEaConnectorEnd SupplierEnd => _eaFactory.GetConnectorEnd(_eaConnector.SupplierEnd);
    }
}