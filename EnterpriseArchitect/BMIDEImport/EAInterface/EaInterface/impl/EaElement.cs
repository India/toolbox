﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using EA;

namespace BMIDEImport.EaInterface.impl
{
    public class EaElement : IEaElement
    {
        private readonly EaFactory _eaFactory; 
        private readonly Element   _eaElement;
        
        public EaElement(EaFactory factory, Element element)
        {
            _eaFactory = factory;
            _eaElement = element;
        }

        public IEnumerable<IEaAttribute> Attributes
        {
            get
            {
                var result = from Attribute attr in _eaElement.Attributes select _eaFactory.GetAttribute(attr as Attribute);
                return result;
            }
        }

        public int ElementID => _eaElement.ElementID;

        public string Alias
        {
            get { return _eaElement.Alias;  }
            set { _eaElement.Alias = value; }
        }

        public string Abstract
        {
            get { return _eaElement.Abstract; }
            set { _eaElement.Abstract = value; }
        }

        public string Stereotype
        {
            get { return _eaElement.Stereotype; }
            set { _eaElement.Stereotype = value; }
        }

        public IEnumerable<IEaConnector> Connectors
        {
            get
            {
                var result = from Connector connector in _eaElement.Connectors
                    select _eaFactory.GetConnector(connector);
                return result;
            } 
        }


        public IEaAttribute AddAttribute(string name, string type)
        {
            var result = _eaElement.Attributes.AddNew(name, type) as Attribute;
            return _eaFactory.GetAttribute(result);
        }

        public IEaConnector AddNewConnector(string name, string type)
        {
            var result = _eaElement.Connectors.AddNew(name, type) as Connector;
            return _eaFactory.GetConnector(result);
        }

        public void DeleteConnector(IEaConnector conn)
        {
            int ID = conn.ConnectorId;
            for (short i = 0; i < _eaElement.Connectors.Count; i++)
            {
                Connector current = (Connector) _eaElement.Connectors.GetAt(i);
                if (current.ConnectorID == ID)
                {
                    _eaElement.Connectors.DeleteAt(i, true);
                    break;
                }
            }
        }

        public bool Update()
        {
            return _eaElement.Update();
        }

        public string Notes
        {
            get { return _eaElement.Notes; }
            set { _eaElement.Notes = value; }
        }
    }
}