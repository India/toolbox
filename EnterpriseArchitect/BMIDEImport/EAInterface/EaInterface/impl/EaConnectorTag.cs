﻿using EA;

namespace BMIDEImport.EaInterface.impl
{
    public class EaConnectorTag : IEaConnectorTag
    {
        private readonly EaFactory _eaFactory; 
        private readonly TaggedValue _eaConnectorTag;
        
        public EaConnectorTag(EaFactory factory, TaggedValue tv)
        {
            _eaFactory = factory;
            _eaConnectorTag = tv;
        }

        public bool Update()
        {
            return _eaConnectorTag.Update();
        }

        public string Name => _eaConnectorTag.Name;
        public string Value
        {
            get { return _eaConnectorTag.Value; }
            set { _eaConnectorTag.Value = value; }
        }
    }
}