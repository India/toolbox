﻿using System;
using EA;

namespace BMIDEImport.EaInterface.impl
{
    public class EaRepository: IEaRepository
    {
        private readonly EaFactory  _eaFactory; 
        private readonly Repository _eaRepository;
     
        public EaRepository(EaFactory factory, Repository repository)
        {
            _eaFactory = factory;
            _eaRepository = repository;
        }

        public EaObjectType GetTreeSelectedItemType()
        {
            EaObjectType result = EaObjectType.otElement;
            
            switch (_eaRepository.GetTreeSelectedItemType())
            {
                case ObjectType.otProject: result = EaObjectType.otProject; break;
                case ObjectType.otRepository: result = EaObjectType.otRepository; break;
                case ObjectType.otCollection: result = EaObjectType.otCollection; break;
                case ObjectType.otElement: result = EaObjectType.otElement; break;
                case ObjectType.otPackage: result = EaObjectType.otPackage; break;
                case ObjectType.otModel: result = EaObjectType.otModel; break;
                case ObjectType.otConnector: result = EaObjectType.otConnector; break;
                case ObjectType.otDiagram: result = EaObjectType.otDiagram; break;
                case ObjectType.otRequirement: result = EaObjectType.otRequirement; break;
                case ObjectType.otScenario: result = EaObjectType.otScenario; break;
                case ObjectType.otConstraint: result = EaObjectType.otConstraint; break;
                case ObjectType.otTaggedValue: result = EaObjectType.otTaggedValue; break;
                case ObjectType.otFile: result = EaObjectType.otFile; break;
                case ObjectType.otEffort: result = EaObjectType.otEffort; break;
                case ObjectType.otMetric: result = EaObjectType.otMetric; break;
                case ObjectType.otIssue: result = EaObjectType.otIssue; break;
                case ObjectType.otRisk: result = EaObjectType.otRisk; break;
                case ObjectType.otTest: result = EaObjectType.otTest; break;
                case ObjectType.otDiagramObject: result = EaObjectType.otDiagramObject; break;
                case ObjectType.otDiagramLink: result = EaObjectType.otDiagramLink; break;
                case ObjectType.otResource: result = EaObjectType.otResource; break;
                case ObjectType.otConnectorEnd: result = EaObjectType.otConnectorEnd; break;
                case ObjectType.otAttribute: result = EaObjectType.otAttribute; break;
                case ObjectType.otMethod: result = EaObjectType.otMethod; break;
                case ObjectType.otParameter: result = EaObjectType.otParameter; break;
                case ObjectType.otClient: result = EaObjectType.otClient; break;
                case ObjectType.otAuthor: result = EaObjectType.otAuthor; break;
                case ObjectType.otDatatype: result = EaObjectType.otDatatype; break;
                case ObjectType.otStereotype: result = EaObjectType.otStereotype; break;
                case ObjectType.otTask: result = EaObjectType.otTask; break;
                case ObjectType.otTerm: result = EaObjectType.otTerm; break;
                case ObjectType.otProjectIssues: result = EaObjectType.otProjectIssues; break;
                case ObjectType.otAttributeConstraint: result = EaObjectType.otAttributeConstraint; break;
                case ObjectType.otAttributeTag: result = EaObjectType.otAttributeTag; break;
                case ObjectType.otMethodConstraint: result = EaObjectType.otMethodConstraint; break;
                case ObjectType.otMethodTag: result = EaObjectType.otMethodTag; break;
                case ObjectType.otConnectorConstraint: result = EaObjectType.otConnectorConstraint; break;
                case ObjectType.otConnectorTag: result = EaObjectType.otConnectorTag; break;
                case ObjectType.otProjectResource: result = EaObjectType.otProjectResource; break;
                case ObjectType.otReference: result = EaObjectType.otReference; break;
                case ObjectType.otRoleTag: result = EaObjectType.otRoleTag; break;
                case ObjectType.otCustomProperty: result = EaObjectType.otCustomProperty; break;
                case ObjectType.otPartition: result = EaObjectType.otPartition; break;
                case ObjectType.otTransition: result = EaObjectType.otTransition; break;
                case ObjectType.otEventProperty: result = EaObjectType.otEventProperty; break;
                case ObjectType.otEventProperties: result = EaObjectType.otEventProperties; break;
                case ObjectType.otPropertyType: result = EaObjectType.otPropertyType; break;
                case ObjectType.otProperties: result = EaObjectType.otProperties; break;
                case ObjectType.otProperty: result = EaObjectType.otProperty; break;
                case ObjectType.otSwimlaneDef: result = EaObjectType.otSwimlaneDef; break;
                case ObjectType.otSwimlanes: result = EaObjectType.otSwimlanes; break;
                case ObjectType.otSwimlane: result = EaObjectType.otSwimlane; break;
                case ObjectType.otModelWatcher: result = EaObjectType.otModelWatcher; break;
                case ObjectType.otScenarioStep: result = EaObjectType.otScenarioStep; break;
                case ObjectType.otScenarioExtension: result = EaObjectType.otScenarioExtension; break;
                case ObjectType.otParamTag: result = EaObjectType.otParamTag; break;
                case ObjectType.otProjectRole: result = EaObjectType.otProjectRole; break;
                case ObjectType.otDocumentGenerator: result = EaObjectType.otDocumentGenerator; break;
                case ObjectType.otMailInterface: result = EaObjectType.otMailInterface; break;
                case ObjectType.otSimulation: result = EaObjectType.otSimulation; break;
                case ObjectType.otTemplateBinding: result = EaObjectType.otTemplateBinding; break;
                case ObjectType.otTemplateParameter: result = EaObjectType.otTemplateParameter; break;
            }

            return result;
        }

        public IEaPackage GetTreeSelectedPackage()
        {
            var package = (Package) _eaRepository.GetTreeSelectedObject();
            return _eaFactory.GetPackage(package);
        }
    }
}