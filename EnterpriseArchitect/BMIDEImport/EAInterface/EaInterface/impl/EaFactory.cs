﻿using System;
using System.Collections.Generic;
using EA;
using Attribute = EA.Attribute;

namespace BMIDEImport.EaInterface.impl
{
    public class EaFactory : IEaFactory
    {
        private class EaObjectCache<EaClass, IntfClass> where IntfClass : class
        {
            private Dictionary<EaClass, IntfClass> _map = new Dictionary<EaClass, IntfClass>();
            
            public IntfClass GetObject(EaClass obj, Func<EaClass, IntfClass> creator)
            {
                IntfClass result = null;
                if (obj != null)
                {
                    if (_map.ContainsKey(obj) == false)
                    {
                        _map[obj] = creator(obj);
                    }
                        
                        
                    result = _map[obj];
                }
                return result;
            }
        }

        private readonly EaObjectCache<Repository, IEaRepository> _repositoryMap = new EaObjectCache<Repository, IEaRepository>();
        public IEaRepository GetRepository(Repository repository)
        {
            return _repositoryMap.GetObject(repository, o => new EaRepository(this, o));
        }

        private readonly EaObjectCache<Package, IEaPackage> _packageMap = new EaObjectCache<Package, IEaPackage>();
        public IEaPackage GetPackage(Package package)
        {
            return _packageMap.GetObject(package, o => new EaPackage(this, o)); ;
        }

        private readonly EaObjectCache<Element, IEaElement> _elementMap = new EaObjectCache<Element, IEaElement>();        
        public IEaElement GetElement(Element element)
        {
            return _elementMap.GetObject(element, o => new EaElement(this, o));
        }

        private readonly EaObjectCache<Attribute, IEaAttribute> _attributeMap = new EaObjectCache<Attribute, IEaAttribute>();        
        public IEaAttribute GetAttribute(Attribute attr)
        {
            return _attributeMap.GetObject(attr, o => new EaAttribute(this, o));
        }

        private readonly EaObjectCache<Connector, IEaConnector> _connectorMap = new EaObjectCache<Connector, IEaConnector>();        
        public IEaConnector GetConnector(Connector connector)
        {
            return _connectorMap.GetObject(connector, o => new EaConnector(this, o));
        }

        private readonly EaObjectCache<ConnectorEnd, IEaConnectorEnd> _connectorEndMap = new EaObjectCache<ConnectorEnd, IEaConnectorEnd>();
        public IEaConnectorEnd GetConnectorEnd(ConnectorEnd connEnd)
        {
            return _connectorEndMap.GetObject(connEnd, o => new EaConnectorEnd(this, o));
        }

        private readonly EaObjectCache<TaggedValue, IEaConnectorTag> _connectorTagMap = new EaObjectCache<TaggedValue, IEaConnectorTag>();
        public IEaConnectorTag GetConnectorTag(TaggedValue tv)
        {
            return _connectorTagMap.GetObject(tv, o => new EaConnectorTag(this, o));
        }
    }
}