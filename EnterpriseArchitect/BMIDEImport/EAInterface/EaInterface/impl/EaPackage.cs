﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EA;

namespace BMIDEImport.EaInterface.impl
{
    public class EaPackage : IEaPackage
    {
        private readonly EaFactory _eaFactory; 
        private readonly Package   _eaPackage;
     
        public EaPackage(EaFactory factory, Package package)
        {
            _eaFactory = factory;
            _eaPackage = package;
        }
        
        public IEaPackage CreateOrGetPackage(string name)
        {
            EA.Package result = null;
            try
            {
                result = (EA.Package) _eaPackage.Packages.GetByName(name);
            }
            catch(Exception) {}
            
            if (result == null)
            {
                result = _eaPackage.Packages.AddNew(name, "Package") as Package;
                result.Update();
            }

            return _eaFactory.GetPackage(result);
        }

        public IEnumerable<IEaPackage> Packages
        {
            get
            {
                return Iterate<Package>(_eaPackage.Packages.GetEnumerator()).Select(x => _eaFactory.GetPackage(x));
            }
        }

        private IEnumerable<T> Iterate<T>(IEnumerator e) where T : class
        {
            while (e.MoveNext())
                yield return e.Current as T;
        }
     

        public string Name => _eaPackage.Name;

        public IEaElement AddNewElement(string name, string type)
        {
            var result = _eaPackage.Elements.AddNew(name, type) as Element;
            return _eaFactory.GetElement(result);
        }

        public IEaElement GetElementByName(string name)
        {
            EA.Element result = null;
            try
            {
                result = _eaPackage.Elements.GetByName(name) as Element;
            }
            catch(Exception) {}
            return _eaFactory.GetElement(result);
        }

        public void RefreshElements()
        {
            _eaPackage.Elements.Refresh();

        }

        public bool Update()
        {
            return _eaPackage.Update();
        }
    }
}