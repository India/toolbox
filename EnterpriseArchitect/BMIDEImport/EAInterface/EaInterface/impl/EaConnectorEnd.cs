﻿using EA;
 
 namespace BMIDEImport.EaInterface.impl
 {
     public class EaConnectorEnd : IEaConnectorEnd
     {
         private readonly EaFactory _eaFactory; 
         private readonly ConnectorEnd _eaConnectorEnd;
         
         public EaConnectorEnd(EaFactory factory, ConnectorEnd connectorEnd)
         {
             _eaFactory = factory;
             _eaConnectorEnd = connectorEnd;
         }

         public bool Update()
         {
             return _eaConnectorEnd.Update();
         }

         public string Cardinality
         {
             get { return _eaConnectorEnd.Cardinality; }
             set { _eaConnectorEnd.Cardinality = value; }
         }

         public bool IsNavigable
         {
             get { return _eaConnectorEnd.IsNavigable; }
             set { _eaConnectorEnd.IsNavigable = value; }
         }

         public int Aggregation
         {
             get { return _eaConnectorEnd.Aggregation; }
             set { _eaConnectorEnd.Aggregation = value; }
         }

         public string Navigable
         {
             get { return _eaConnectorEnd.Navigable; }
             set { _eaConnectorEnd.Navigable = value; }
         }

         public string Alias
         {
             get { return _eaConnectorEnd.Alias; }
             set { _eaConnectorEnd.Alias = value; }
         }

         public string Role
         {
             get { return _eaConnectorEnd.Role; }
             set { _eaConnectorEnd.Role = value; }
         }
     }
 }