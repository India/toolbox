﻿using System;
using Attribute = EA.Attribute;

namespace BMIDEImport.EaInterface.impl
{
    class EaAttribute : IEaAttribute
    {
        private EaFactory _eaFactory; 
        private readonly Attribute _eaAttribute;
        
        public EaAttribute(EaFactory factory, Attribute element)
        {
            _eaFactory = factory;
            _eaAttribute = element;
        }

        public bool Update()
        {
            return _eaAttribute.Update();
        }

        public string Name
        {
            get { return _eaAttribute.Name; }
            set { _eaAttribute.Name = value; }
        }

        public string Type
        {
            get { return _eaAttribute.Type;}
            set {_eaAttribute.Type = value;}
        }
        public string Notes
        {
            get { return _eaAttribute.Notes;}
            set {_eaAttribute.Notes = value;}
        }

        public string Default
        {
            get { return _eaAttribute.Default;}
            set {_eaAttribute.Default = value;}
        }

        public string Alias
        {
            get { return _eaAttribute.Alias;}
            set {_eaAttribute.Alias = value;}
        }

        public string UpperBound
        {
            get { return _eaAttribute.UpperBound;}
            set {_eaAttribute.UpperBound = value;}
        }

        public string LowerBound
        {
            get { return _eaAttribute.LowerBound;}
            set {_eaAttribute.LowerBound = value;}
        }

        public string Stereotype 
        {
            get { return _eaAttribute.Stereotype;}
            set {_eaAttribute.Stereotype = value;}
        }

        public string AttributeGUID
        {
            get { return _eaAttribute.AttributeGUID; }
            set { _eaAttribute.AttributeGUID = value; }
        }

        public Scope Visibility
        {
            get { return (Scope) Enum.Parse(typeof(Scope), _eaAttribute.Visibility); }
            set { _eaAttribute.Visibility = value.ToString(); }
        }
    }
}