﻿using System.Collections.Generic;
using EA;

namespace BMIDEImport.EaInterface
{
    public interface IEaElement : IEaObject
    {
        string Notes { get; set; }
        
        int ElementID { get; }
        string Alias { get; set; }
        string Abstract { get; set; }

        IEnumerable<IEaAttribute> Attributes { get; }
        IEnumerable<IEaConnector> Connectors { get; }
        string Stereotype { get; set; }

        IEaAttribute AddAttribute(string name, string type);
        IEaConnector AddNewConnector(string name, string connType);
        void DeleteConnector(IEaConnector conn);
    }
}