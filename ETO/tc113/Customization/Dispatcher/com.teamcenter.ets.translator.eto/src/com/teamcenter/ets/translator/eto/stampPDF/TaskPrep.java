/*==============================================================================

          Copyright (c) 2007 UGS The PLM Company
                   Unpublished - All rights reserved

================================================================================
File description:   See description below.

    Filename:   TaskPrep.java
    Module  :   com.teamcenter.ets.translator.eto

================================================================================
                     Modification Log
================================================================================
Date        User           Change Description
21-Feb-2012 glangenbacher  Initial Version
11-Dec-2012 glangenbacher  Upgraded code for Teamcenter 9.1
23-Apr-2013 glangenbacher  Fixed issue when stamping without release date

==============================================================================*/

package com.teamcenter.ets.translator.eto.stampPDF;

//==== Imports  ================================================================
import com.teamcenter.ets.soa.ConnectionManager;
import com.teamcenter.ets.translator.eto.util.DatasetHelper;
import com.teamcenter.ets.translator.eto.util.TranslatorHelper;

import java.io.File;

import com.teamcenter.ets.extract.DefaultTaskPrep;

import com.teamcenter.ets.request.TranslationRequest;
import com.teamcenter.ets.util.Registry;

import com.teamcenter.services.strong.core.DataManagementService;
import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.soa.client.model.strong.Dataset;
import com.teamcenter.soa.client.model.strong.ImanFile;
import com.teamcenter.soa.client.model.strong.ItemRevision;
import com.teamcenter.translationservice.task.TranslationTask;
import java.io.IOException;

//==== Class ====================================================================
/**
 * Custom TaskPrep class for stamp-pdf translator. This sub class prepares a
 * stamp-pdf translation task. This is a configuration specified class based on
 * provider name and translator name in DispatcherClient property file which
 * creates the stamp-pdf specific translation request by preparing the data for
 * translation and creating the Translation request object.
 * 
 * @author	Gabriel Langenbacher
 * @version	2.0, 23.04.2013
 * 
 */
public class TaskPrep extends DefaultTaskPrep {

	private static Registry registry = Registry
	.getRegistry("com.teamcenter.ets.translator.eto.util.translator");

	/**
	 * The ETS Extractor service invokes the prepareTask() class method to
	 * prepare input data for translation requests. This method does the
	 * following Validates that all data necessary to translate the primary
	 * object(s) is available. Primary object(s) that are data incomplete are
	 * generally excluded from the translator input with appropriate logging.
	 * Collects all input files and creates TranslationTask object
	 * 
	 * @see com.teamcenter.ets.extract.TaskPrep#prepareTask()
	 * 
	 * @return Returns the Translation Task object.
	 */
	@Override
	public TranslationTask prepareTask() throws Exception {

		TranslationTask zTransTask = new TranslationTask();
		boolean dsProcessed = false; // process only the first Dataset 

		m_zTaskLogger.debug("TaskPrep -> TRANSLATOR (lowercase): "
				+ request.getPropertyObject(TranslationRequest.TRANSLATOR_NAME)
				.getDisplayableValue().toLowerCase());

		// get primary objects and secondary objects
		ModelObject primary_objs[] = request.getPropertyObject(
				TranslationRequest.PRIMARY_OBJS).getModelObjectArrayValue();
		ModelObject secondary_objs[] = request.getPropertyObject(
				TranslationRequest.SECONDARY_OBJS).getModelObjectArrayValue();

		com.teamcenter.soa.client.Connection connection = ConnectionManager.getActiveConnection();

		DataManagementService dmService = DataManagementService.getService(connection);

		for (int i = 0; i < primary_objs.length && !dsProcessed; i++) {

			// TranslationRequest PRIMARY Object must be a dataset
			Dataset transPrimaryDS = (Dataset) primary_objs[i];

			// TranslationRequest secondary object must be an ItemRevision
			ItemRevision transSecondaryIR = (ItemRevision) secondary_objs[i];

			m_zTaskLogger.debug("TaskPrep -> PRIMARY OBJECT TYPE IS: "
					+ transPrimaryDS.get_object_type());
			m_zTaskLogger.debug("TaskPrep -> SECONDARY OBJECT TYPE IS: "
					+ transSecondaryIR.get_object_type());

			// Skip if it is not pdf dataset
			if ( !transPrimaryDS.get_object_type().equalsIgnoreCase("PDF") )
			{
				continue;
			}				

			dmService.getProperties(
					new ModelObject[]{transSecondaryIR,transPrimaryDS}, 
					new String[]
					           {	
							"ref_list",
							"item_id",
							"item_revision_id"
					           }
			);			

			ModelObject[] contexts = transPrimaryDS.get_ref_list();
			ImanFile zIFile = null;

			// get named reference list array 
			for ( int j = 0; j < contexts.length; j++ )
			{
				// get file extension and compare.
				if ( contexts[j] instanceof ImanFile )
				{
					zIFile = ( ImanFile ) contexts[j];
					dmService.getProperties(new ModelObject[]{zIFile}, new String[]{"file_ext"});
					String scFileExt = zIFile.get_file_ext();

					m_zTaskLogger.debug("TaskPrep -> DATASET FILE EXTENSION IS: "
							+ scFileExt);                    

					if ( scFileExt.equalsIgnoreCase("pdf") )
					{
						break;
					}
					else
					{
						zIFile = null ;
					}
				}
			}

			if (zIFile == null)//(fileNames.length == 0)
			{
				throw new Exception ("No named reference found for " + transPrimaryDS.get_object_string());
			}

			File zFile1 = TranslationRequest.getFileToStaging( zIFile, stagingLoc );            

			//
			// - Get the source file name without the .prt extension.
			// - Sanitize the file name by replacing special characters with "_"
			// -    ProE To Jt translator has an option autoNameSanitize that
			// -    converts special charecters to "_" on the results jt file.
			// -    To avoid File mapping ( mapping between source and results
			// -    file) failures, the sanitization is done before sending for
			// -    translation.
			//
			String dsFname = zFile1.getName();
			String dsFnameExt = dsFname.substring(dsFname.lastIndexOf( "." )+1);
			int index = dsFname.lastIndexOf( dsFnameExt );
			String dsFnameExtTrim = dsFname.substring(0, index-1);

			String sanitizedFname = getSanitizedFilename(
					registry.getString("TaskPrep.sanitize.BadChars"),
					dsFnameExtTrim);
			String expFname = null;
			String impFname = null;

			if ( sanitizedFname != null )
			{
				//To have unique file names after extraction for files like
				// a b.prt, a-b.prt add a number to the sanitized filename
				// Sanitized file names will look like a_b0.prt, a_b1.prt
				expFname = sanitizedFname + i + "." + dsFnameExt;

				// - If the file name has been sanitized, rename the file in the staging
				// - directory to the sanitized name.
				zFile1.renameTo(new File (stagingLoc + File.separator + expFname));


				impFname = sanitizedFname + i + "." + DatasetHelper.BINARY_FILE_EXT_PDF;
			}
			else
			{
				expFname = dsFname;
				impFname = dsFnameExtTrim + "." + DatasetHelper.BINARY_FILE_EXT_PDF;
			}

			m_zTaskLogger.debug("TaskPrep -> EXPORTED DATASET FILE NAME IS: "
					+ expFname);             

			// get translation request arguments
			String[]  translatorArgsKeys = 	request.
			getPropertyObject(TranslationRequest.TRANSLATION_ARGS_KEYS).getStringArrayValue();   

			String[]  translatorArgsData = 	request.
			getPropertyObject(TranslationRequest.TRANSLATION_ARGS_DATA).getStringArrayValue();

			TranslatorHelper translatorHelper = new TranslatorHelper(m_zTaskLogger);

			// create strings for task options
			String expFullPathTmp 	= stagingLoc + "\\" + expFname;
			String expFullPath		= expFullPathTmp.replace('/', '\\');
			String impFullPathTmp 	= stagingLoc + "\\result\\" + impFname;
			String impFullPath		= impFullPathTmp.replace('/', '\\');
			String stampText 		= translatorHelper.getTranslationArgValue("text", translatorArgsKeys, translatorArgsData);
			String stampDate 		= translatorHelper.getTranslationArgValue("date_released", translatorArgsKeys, translatorArgsData);
			String stampReviewers 	= translatorHelper.getTranslationArgValue("reviewers", translatorArgsKeys, translatorArgsData);
			String stampApprovers	= translatorHelper.getTranslationArgValue("approvers", translatorArgsKeys, translatorArgsData);

			zTransTask = this.addOptions(zTransTask, "SRC_DIR_PATH", 	expFullPath);
			zTransTask = this.addOptions(zTransTask, "DST_DIR_PATH", 	impFullPath);
			zTransTask = this.addOptions(zTransTask, "STAGE_DIR_PATH", 	stagingLoc);
			zTransTask = this.addOptions(zTransTask, "STAMP_TEXT", 		stampText);
			zTransTask = this.addOptions(zTransTask, "STAMP_DATE", 		( stampDate.isEmpty() ? "(null)" : stampDate) );
			zTransTask = this.addOptions(zTransTask, "STAMP_REVIEWERS", "\"" + (stampReviewers.isEmpty() ? "(null)" : stampReviewers) + "\"");
			zTransTask = this.addOptions(zTransTask, "STAMP_APPROVERS", "\"" + (stampApprovers.isEmpty() ? "(null)" : stampApprovers) + "\"");

			m_zTaskLogger.debug("TaskPrep -> Translation arg 'SRC_DIR_PATH' with value '" + expFullPath + "'"); 
			m_zTaskLogger.debug("TaskPrep -> Translation arg 'DST_DIR_PATH' with value '" + impFullPath + "'"); 
			m_zTaskLogger.debug("TaskPrep -> Translation arg 'STAGE_DIR_PATH' with value '" + stagingLoc + "'"); 
			m_zTaskLogger.debug("TaskPrep -> Translation arg 'STAMP_TEXT' with value '" + stampText + "'"); 
			m_zTaskLogger.debug("TaskPrep -> Translation arg 'STAMP_DATE' with value '" + stampDate + "'"); 
			m_zTaskLogger.debug("TaskPrep -> Translation arg 'STAMP_REVIEWERS' with value '" + stampReviewers + "'"); 
			m_zTaskLogger.debug("TaskPrep -> Translation arg 'STAMP_APPROVERS' with value '" + stampApprovers + "'");  		        

			zTransTask = prepTransTask( 	zTransTask, 
					transPrimaryDS, 
					transSecondaryIR, 
					expFname,
					true, 
					true, 
					".pdf", 
					0, 
					null 
			);
			dsProcessed = true;
		}        	

		return addRefIdToTask(zTransTask, 0);
	}

	@Override
	protected String getSanitizedFilename(String scBadString, String scFileName) {
		this.m_zTaskLogger.debug("TaskPrep ->  -> Sanitizing: "
				+ scFileName);
		boolean qSanitized = false;
		char[] scBadChar = scBadString.toCharArray();

		for (int j = 0; j < scBadChar.length; j++) {
			this.m_zTaskLogger
			.debug("TaskPrep ->  -> Searching Bad Character: "
					+ scBadChar[j]);
			if (scFileName.indexOf(scBadChar[j]) == -1) {
				continue;
			}
			this.m_zTaskLogger
			.debug("TaskPrep ->  ->       - Replacing by: "
					+ registry
					.getString("TaskPrep.sanitize.replacementChar"));
			scFileName = scFileName.replace(scBadChar[j], (registry
					.getString("TaskPrep.sanitize.replacementChar"))
					.charAt(0));
			qSanitized = true;
		}

		if (!qSanitized) {
			return null;
		}

		this.m_zTaskLogger.debug("TaskPrep ->  -> Sanitized FileName: "
				+ scFileName);
		return scFileName;
	}	

	public static void renameFile(String oldName, String newName)
	throws IOException {
		File srcFile = new File(oldName);
		boolean bSucceeded = false;
		try {
			File destFile = new File(newName);
			if (destFile.exists()) {
				if (!destFile.delete()) {
					throw new IOException(oldName
							+ " was not successfully renamed to " + newName);
				}
			}
			if (!srcFile.renameTo(destFile)) {
				throw new IOException(oldName
						+ " was not successfully renamed to " + newName);
			} else {
				bSucceeded = true;
			}
		} finally {
			if (bSucceeded) {
				srcFile.delete();
			}
		}
	}

}