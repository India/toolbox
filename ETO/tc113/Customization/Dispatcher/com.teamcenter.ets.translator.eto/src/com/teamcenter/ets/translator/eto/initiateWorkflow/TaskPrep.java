/*==============================================================================

          Copyright (c) 2007 UGS The PLM Company
                   Unpublished - All rights reserved

================================================================================
File description:   See description below.

    Filename:   TaskPrep.java
    Module  :   com.teamcenter.ets.translator.eto

================================================================================
                     Modification Log
================================================================================
Date        User           Change Description
20-Feb-2013 glangenbacher  Initial Version

==============================================================================*/

package com.teamcenter.ets.translator.eto.initiateWorkflow;

//==== Imports  ================================================================
import com.teamcenter.ets.extract.DefaultTaskPrep;
import com.teamcenter.ets.request.TranslationRequest;
import com.teamcenter.ets.soa.ConnectionManager;
import com.teamcenter.ets.soa.SoaHelper;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.strong.workflow.WorkflowService;
import com.teamcenter.services.strong.workflow._2008_06.Workflow.ContextData;
import com.teamcenter.services.strong.workflow._2008_06.Workflow.InstanceInfo;
import com.teamcenter.soa.client.model.ErrorStack;
import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.soa.client.model.strong.ItemRevision;
import com.teamcenter.soa.client.model.strong.User;
import com.teamcenter.translationservice.task.TranslationTask;
import com.teamcenter.ets.translator.eto.util.DatasetHelper;
import com.teamcenter.ets.translator.eto.util.TranslatorHelper;

//==== Class ====================================================================
/**
 * Custom TaskPrep class for assignToProject translator. This sub class prepares a
 * assignToProject translation task. This is a configuration specified class based on
 * provider name and translator name in DispatcherClient property file which
 * creates the assignToProject specific translation request by preparing the data for
 * translation and creating the Translation request object.
 * 
 * @author	Gabriel Langenbacher
 * @version	1.0, 20.02.2013
 * 
 */
public class TaskPrep extends DefaultTaskPrep
{    
	/** 
	 * The service user who is running this process.
	 * @serial
	 * */
	private static String serviceUser;

	/**
	 * The ETS Extractor service invokes the prepareTask() class method to
	 * prepare input data for translation requests. This method does the following
	 *    Validates that all data necessary to translate the primary object(s) is
	 *        available. Primary object(s) that are data incomplete are generally
	 *        excluded from the translator input with appropriate logging.
	 *    Collects all input files and creates TranslationTask object
	 *
	 * @see com.teamcenter.ets.extract.TaskPrep#prepareTask()
	 * @throws If any exception has been thrown by any function used here.
	 *
	 * @return Returns the Translation Task object.
	 */
	public TranslationTask prepareTask() throws Exception
	{
		TranslationTask zTransTask = new TranslationTask();

		m_zTaskLogger.debug("TaskPrep -> TRANSLATOR (lowercase): "
				+ request.getPropertyObject(TranslationRequest.TRANSLATOR_NAME)
				.getStringValue().toLowerCase());    	

		// get primary objects and secondary objects
		//ModelObject primary_objs[]   = 	request.getPropertyObject( TranslationRequest.PRIMARY_OBJS ).getModelObjectArrayValue();

		ModelObject secondary_objs[] = 	request.getPropertyObject( TranslationRequest.SECONDARY_OBJS ).getModelObjectArrayValue();      

		// *****************************
		// Get connection for initiating services
		// *****************************   
		com.teamcenter.soa.client.Connection connection = ConnectionManager.getActiveConnection();

		// *****************************
		// Get current users projects
		// *****************************
		User currentUser = ConnectionManager.getLoggedInUser();
		serviceUser = currentUser.get_user_id();

		m_zTaskLogger.debug("TaskPrep -> CURRENT USER IS: " + serviceUser);  	

		// get translation request arguments
		String[]  translatorArgsKeys = 	request.getPropertyObject(TranslationRequest.TRANSLATION_ARGS_KEYS).getStringArrayValue();   
		String[]  translatorArgsData = 	request.getPropertyObject(TranslationRequest.TRANSLATION_ARGS_DATA).getStringArrayValue();
		TranslatorHelper translatorHelper = new TranslatorHelper(m_zTaskLogger);
		
		String templateName = translatorHelper.getTranslationArgValue("templateName", translatorArgsKeys, translatorArgsData);
		m_zTaskLogger.debug("TaskPrep -> SELECTED WORKFLOW TEMPLATE IS: " + templateName);
		if (templateName.isEmpty())
		{
			templateName = DatasetHelper.WORKFLOW_REQ_PHASE;
			m_zTaskLogger.warn("TaskPrep -> TEMPLATE NOT GIVEN, USE DEFAULT: " + templateName);
		}
		
		for (int i = 0; i < secondary_objs.length; i++)
		{
			// *****************************
			// Get primary and secondary objects of current translation request
			// *****************************         	
			//Item item  				= (Item)primary_objs[i];
			ItemRevision itemRev	= (ItemRevision)secondary_objs[i];

			itemRev = ( ItemRevision ) SoaHelper.getProperties( itemRev, new String[]{"item_id", "item_revision_id"} );

			String itemId	= itemRev.get_item_id();

			m_zTaskLogger.debug("TaskPrep -> CURRENT ITEM ID IS: " + itemId);

			// *****************************
			// Do status assignment for revision
			// ***************************** 
			WorkflowService workflowService = WorkflowService.getService(connection);
			this.triggerWorkflow(workflowService, templateName, secondary_objs[i]);
			
			zTransTask = this.addOptions(zTransTask, "workflow", templateName);

			zTransTask = prepTransTask( 	zTransTask, 
					null, 
					null, 
					"",
					false, 
					false, 
					".txt", 
					0, 
					null 
			);               							  	
		}       

		return addRefIdToTask( zTransTask, 0 );
	}

	private void triggerWorkflow(WorkflowService workflowService, String workflowName, ModelObject object) throws ServiceException
	{
		
		ContextData ctxData = new ContextData();
		String[] attach = new String[]{object.getUid()};
		int[] atttyp = new int[]{1};

		ctxData.attachmentCount = 1;
		ctxData.attachments = attach ;
        ctxData.attachmentTypes = atttyp;
        ctxData.processTemplate = workflowName;		
        
        InstanceInfo instInfo = workflowService.createInstance( true,
                "",
                "BlockDocProc",
                "BlockDocSub",
                "BlockProcItem",
                ctxData );     
        
        if( instInfo.serviceData.sizeOfPartialErrors() > 0 ) 
        {
            ErrorStack e = instInfo.serviceData.getPartialError(0);
            String[] msg=e.getMessages();
            //errorText=msg[0];

            //ServiceException se = new ServiceException("Error Occured");
            ServiceException se = new ServiceException(msg[0]);

            throw se;
        }        
	}
}