/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_library.hxx

    Description:  Header File containing declarations of function spread across all ETO4_<UTILITY NAME>_utilities.cxx 
                  For e.g. eto4_dataset_utilities.cxx/eto4_form_utilities.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_LIBRARY_HXX
#define ETO4_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <epm/signoff.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me.h>
#include <form.h>
#include <project.h>
#include <folder.h>
#include <pom.h>
#include <tctype.h>
#include <fclasses/tc_date.h>
#include <eto4_const.hxx>
#include <eto4_itk_errors.hxx>
#include <eto4_itk_mem_free.hxx>
#include <eto4_string_macros.hxx>

/*        Utility Functions For Object       */
int ETO4_obj_ask_class
(
    tag_t    tObject,
    char**   ppszClassName
);

int ETO4_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
);

int ETO4_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
);

int ETO4_obj_ask_status_tag
(
    tag_t      tObject,
    char*      pszStatusName,
    tag_t*     ptStatusTag
);

int ETO4_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
);

/*        Utility Functions For Error Logging        */
int ETO4_err_write_to_logfile
(
    char*       pszErrorFunction,
    int         iErrorNumber,
    char*       pszFileName,
    int         iLineNumber
);

int ETO4_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
);


/*        Utility Functions For Date & Time        */
int ETO4_get_date
(
    date_t *dReleaseDate
);

int ETO4_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
);



/*        Argument Utility Functions        */
int ETO4_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
);

int ETO4_arg_get_attached_obj_of_input_type_list
(
    tag_t   task,
    int     iAttachmentType,
    char**  ppszObjTypeList,
    int     iTypeCnt,
    int*    piCount,
    tag_t** pptObject
);

int ETO4_arg_get_attached_objects_of_type
(
    tag_t   task,
    int     iAttachmentType,
    char*   pszAttachedObjType,
    int*    piCount,
    tag_t** pptObject
);

int ETO4_arg_get_arguments
(
    TC_argument_list_t*   pArgList,
    tag_t                 tTask,
    const char*           pszArgument,
    char                  cSep,
    logical               bOptional,
    int*                  piValCount,
    char***               pppszArgValue,
    char**                ppszOriginalArgVal
);

int ETO4_arg_get_wf_argument_value
(
    TC_argument_list_t*   pArgList,
    tag_t                 tTask,
    const char*           pszArgument,
    logical               bOptional,
    char**                ppszArgValue
);


/*        String Utility Functions        */
int ETO4_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
);

int ETO4_contains_string_value
(
    char*  pszStrVal,
    int    iTotalStrVals,
    char** pppszStrVals
);

void  ETO4_get_string_tokens
(
    const char* pszInputStr,
    char*       pszDelim, 
    int*        iTokenCnt,
    char***     pppszAllTokens
);


/*        Utility Functions For GRM        */
int ETO4_grm_get_secondary_obj
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
);



/*        Utility Functions For Preferences        */
int ETO4_pref_get_string_values
(
    char*   pszPrefName, 
    int*    iPrefValCnt, 
    char*** pppszPrefVals
);
    
int ETO4_pref_get_integer_value
(
    char*   pszPrefName, 
    int*    piPrefValue
);



#endif




