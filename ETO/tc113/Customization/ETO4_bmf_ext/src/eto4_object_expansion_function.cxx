/*======================================================================================================================================================================
                                                      Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_object_expansion_function.cxx

    Description: This File contains custom functions that are used by BMF extension functions defined in file 'a4_bmf_extension.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>
#include <eto4_object_expansion_function.hxx>

/**
 * L2R relations: "L2R" means "Left to Right" expansion, and describes an expansion starting from primary object to secondary object. The symbol ">" describes the direction from
 *                left to right as shown above. Expansion form L2R can use any GRM Relation
 * R2L relations: "R2L" means �Right to Left� expansion, and describes an expansion starting from secondary object to primary object. The symbol �<� describes the direction from
 *                left to right.
 * Error:
 *       Preference values "%1$" is incorrect. Preference value of object expansion information follows pattern which looks like: " Relation Type > Secondary Type > Source Attribute Info On Primary > Destination Attribute Info On Secondary > Value "
 *
 * @param[in]  iObjDefCnt         
 * @param[in]  ppszObjDefinition 
 * @param[out] pmObjExpanPrefInfo 
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_collect_obj_expansion_info
(
    int    iObjDefCnt, 
    char** ppszObjDefinition,
    map<string, sPrefLineInfo>* pmObjExpanPrefInfo
)
{
    int iRetCode  = ITK_ok;

    for(int iDx = 0; iDx < iObjDefCnt && iRetCode == ITK_ok; iDx++)
    {
        logical L2RExpan = false;
        logical R2LExpan = false;

        ETO4_ITKCALL(iRetCode, ETO4_string_find_sub_str(ppszObjDefinition[iDx], ETO4_STRING_GREATER_THAN, &L2RExpan));

        ETO4_ITKCALL(iRetCode, ETO4_string_find_sub_str(ppszObjDefinition[iDx], ETO4_STRING_LESS_THAN, &R2LExpan));

        if(iRetCode == ITK_ok)
        {
            if((L2RExpan == true && R2LExpan == true) || (L2RExpan == false && R2LExpan == false))
            {
                iRetCode = OBJ_EXPANSION_INFO_NOT_DEFINED_PROPERLY;
                EMH_store_error_s1(EMH_severity_error, iRetCode, ppszObjDefinition[iDx]);
            }
            else
            {
                int    iCount            = 0;
                char*  pszExpanDirection = NULL;
                char** ppszInfo          = NULL;

                if(L2RExpan == true)
                {
                    ETO4_STRCPY(pszExpanDirection, ETO4_STRING_GREATER_THAN);

                    ETO4_ITKCALL(iRetCode, ETO4_str_parse_string(ppszObjDefinition[iDx], ETO4_STRING_GREATER_THAN, &iCount, &ppszInfo));
                }
                else
                {
                    ETO4_STRCPY(pszExpanDirection, ETO4_STRING_LESS_THAN);

                    ETO4_ITKCALL(iRetCode, ETO4_str_parse_string(ppszObjDefinition[iDx], ETO4_STRING_LESS_THAN, &iCount, &ppszInfo));
                }

                if(iCount == 5)
                {
                    ETO4_ITKCALL(iRetCode, ETO4_add_obj_expansion_info_to_map(ppszObjDefinition[iDx], pszExpanDirection, iCount, ppszInfo, pmObjExpanPrefInfo));
                }
                else
                {
                    iRetCode = OBJ_EXPANSION_INFO_NOT_DEFINED_PROPERLY;
                    EMH_store_error_s1(EMH_severity_error, iRetCode, ppszObjDefinition[iDx]);
                }

                ETO4_MEM_TCFREE(pszExpanDirection);
                ETO4_MEM_TCFREE(ppszInfo);
            }
        }
    }

    return iRetCode;
}

/**
 * L2R relations: "L2R" means "Left to Right" expansion, and describes an expansion starting from primary object to secondary object. The symbol ">" describes the direction from
 *                left to right as shown above. Expansion form L2R can use any GRM Relation
 * R2L relations: "R2L" means �Right to Left� expansion, and describes an expansion starting from secondary object to primary object. The symbol �<� describes the direction from
 *                left to right.
 * Error:
 *       Preference values "%1$" is incorrect. Preference value of object expansion information follows pattern which looks like: " Relation Type > Secondary Type > Source Attribute Info On Primary > Destination Attribute Info On Secondary > Value "
 *
 * @param[in]  pszObjDefinition    
 * @param[in]  pszExpanDirection  
 * @param[in]  iValCnt
 * @param[in]  ppszObjExpValInfo
 * @param[out] pmObjExpanPrefInfo
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_add_obj_expansion_info_to_map
(
    char*  pszObjDefinition,
    char*  pszExpanDirection,
    int    iValCnt, 
    char** ppszObjExpValInfo,
    map<string, sPrefLineInfo>* pmObjExpanPrefInfo
)
{
    int    iRetCode  = ITK_ok;
    string strMapKey;
    vector<string> vEachPrefLine;

    for(int iDx = 0; iDx < iValCnt && iRetCode == ITK_ok; iDx++)
    {
        if(iDx == 0)
        {
            if((ppszObjExpValInfo[iDx]) != NULL && tc_strlen(ppszObjExpValInfo[iDx]) > 0)
            {
                strMapKey.append(ppszObjExpValInfo[iDx]);
            }
            else
            {
                iRetCode = OBJ_EXPANSION_INFO_NOT_DEFINED_PROPERLY;
                EMH_store_error_s1(EMH_severity_error, iRetCode, pszObjDefinition);
            }
        }
        else
        {
            if((ppszObjExpValInfo[iDx]) != NULL && tc_strlen(ppszObjExpValInfo[iDx]) > 0)
            {
                vEachPrefLine.push_back(ppszObjExpValInfo[iDx]);
            }
            else
            {
                vEachPrefLine.push_back(ETO4_NO_VAL);
            }
        }
    }

    if(iRetCode == ITK_ok)
    {
        map<string, sPrefLineInfo>::iterator iterObjExpanPref;
        
        /* Find the Key */
        iterObjExpanPref = pmObjExpanPrefInfo->find(strMapKey);

        if(iterObjExpanPref != pmObjExpanPrefInfo->end())
        {
            iterObjExpanPref->second.vExpansionInfo.push_back(pszExpanDirection);
            iterObjExpanPref->second.vAttrNameVal.push_back(vEachPrefLine);
        }
        else
        {
            sPrefLineInfo sRelGroupPrefVal;
                       
            sRelGroupPrefVal.vExpansionInfo.push_back(pszExpanDirection);

            sRelGroupPrefVal.vAttrNameVal.push_back(vEachPrefLine);

            pmObjExpanPrefInfo->insert(std::pair<string, sPrefLineInfo>(strMapKey, sRelGroupPrefVal));
        }
    }

    return iRetCode;
}

/**
 * This function 
 *
 * @param[in]  vAttrNameValInfo
 * @param[out] pstrDestObjType
 * @param[out] psrtSrcAttr
 * @param[out] pstrDestAttr
 * @param[out] pstrValue
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_get_obj_expansion_info
(
    vector<string> vAttrNameValInfo,
    string* pstrDestObjType,
    string* psrtSrcAttr,
    string* pstrDestAttr,
    string* pstrValue
)
{
    int iRetCode  = ITK_ok;
    int iPosition = 0;
    std::vector<string>::const_iterator iterExpanInfo;

    for(iterExpanInfo = vAttrNameValInfo.begin(); iterExpanInfo != vAttrNameValInfo.end() && iRetCode == ITK_ok; iterExpanInfo++)
    {
        
        if(iPosition == 0)
        {
            (*pstrDestObjType).append(*iterExpanInfo);
        }

        if(iPosition == 1)
        {
            (*psrtSrcAttr).append(*iterExpanInfo);
        }

        if(iPosition == 2)
        {
            (*pstrDestAttr).append(*iterExpanInfo);
        }

        if(iPosition == 3)
        {
            (*pstrValue).append(*iterExpanInfo);
        }

        iPosition++;
    }

    return iRetCode;
}

/**
 * This function will retrieve actual attribute name of and list of object tags on which actual attribute exist. Attribute can be directly present on input object "tObject" or may exist on related
 * objects whose information is described using attribute "strAttr". Attribute "strAttr" will define information using object path notation. 
 * For example if value of "strAttr" = "items_tag.a4_oem" 
 * Above mentioned attribute value can be supplied as an input argument. This information implies that actual attribute is "a4_oem" which is present on object that is associated with input
 * object "tObject" through reference attribute "items_tag". So the actual object will not be input object "tObject".
 *
 * @param[in]  tObject        Tag of object. 
 * @param[in]  strAttr        Attribute information string.
 * @param[out] piActualObjCnt Count of actual attributes.
 * @param[out] pptActualObj   List of actual attributes.
 * @param[out] pstrActualAttr Name of actual attributes.
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_get_actual_attr_name_and_obj
(
    tag_t   tObject,
    string  strAttr,
    int*    piActualObjCnt,
    tag_t** pptActualObj,
    string* pstrActualAttr
)
{
    int    iRetCode = ITK_ok;
    int    iCount   = 0;
    char** ppszInfo = NULL;

    ETO4_ITKCALL(iRetCode, ETO4_str_parse_string((char*)(strAttr.c_str()), ETO4_STRING_DOT, &iCount, &ppszInfo));

    if(iCount == 1)
    {
        ETO4_UPDATE_TAG_ARRAY((*piActualObjCnt), (*pptActualObj), tObject);
        (*pstrActualAttr).append(strAttr);
    }
    else
    {
        ETO4_UPDATE_TAG_ARRAY((*piActualObjCnt), (*pptActualObj), tObject);

        for(int iDx = 0; iDx < iCount && iRetCode == ITK_ok; iDx++)
        {
            /* This check ensures that attribute currently being processed is not the last attribute in the object expansion notation array. */
            if(iDx < (iCount - 1))
            {
                int     iNewObjCnt = 0;
                tag_t*  ptNewObj   = NULL;
                logical bIsRefAttr = false;

                ETO4_ITKCALL(iRetCode, ETO4_get_obj_using_ref_property((*piActualObjCnt), (*pptActualObj), ppszInfo[iDx], &iNewObjCnt, &ptNewObj, &bIsRefAttr));

                if(iRetCode == ITK_ok)
                {
                    if(bIsRefAttr == false )
                    {
                        iRetCode = ATTR_INFO_NOT_DEFINED_PROPERLY_USING_OBJ_EXPANSION_NOTATION;
                        EMH_store_error_s1(EMH_severity_error, iRetCode, strAttr.c_str());
                    }
                    else
                    {
                        (*piActualObjCnt) = 0;

                        ETO4_MEM_TCFREE(*pptActualObj);

                        ETO4_CLONE_TAG_ARRAY((*piActualObjCnt), (*pptActualObj), iNewObjCnt, ptNewObj);
                    }
                }

                ETO4_MEM_TCFREE(ptNewObj);
            }
            else
            {
                /* Do not traverse the last attribute even if last attribute in array 'ppszInfo' is also of type reference. */
                (*pstrActualAttr).append(ppszInfo[iDx]);
            }
        }
   }

    ETO4_MEM_TCFREE(ppszInfo);
    return iRetCode;
}

/**
 * This function process input list of object "ptObject" and tries retrieve objects that are associated with input object "ptObject" if property "pszAttrName" is a reference property. If input property
 * is not of type reference then function will set "pbIsRefAttr" as "false". Property can also be of type reference array i.e. property can store list of "tags" as its value.
 * This function assumes that input attribute type is same for all input objects "ptObject" i.e. if input attribute is of type reference then this attribute is of type reference for all input objects.
 * If input attribute is not of type reference then processing of input object "ptObject" is stopped.
 *
 * @param[in]  iObjCnt      Count of objects to be processed.
 * @param[in]  ptObject     List of objects.
 * @param[in]  pszAttrName  Name of attribute
 * @param[out] piNewObjCnt  Count of objects retrieved from using reference property.
 * @param[out] pptNewObj    List of objects retrieved from using reference property.
 * @param[out] pbIsRefAttr  "true" if input attribute is of type reference else "false".
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_get_obj_using_ref_property
(
    int      iObjCnt,
    tag_t*   ptObject,
    char*    pszAttrName,
    int*     piNewObjCnt,
    tag_t**  pptNewObj,
    logical* pbIsRefAttr
)
{
    int iRetCode = ITK_ok;
    
    /* Initializing out parameter of this function. */
    (*piNewObjCnt) = 0;
    (*pptNewObj)   = NULL;
    (*pbIsRefAttr) = false;

    for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
    {
        char* pszValType = NULL;
        PROP_value_type_t ValueTypeOfProp = PROP_untyped;

        ETO4_ITKCALL(iRetCode, AOM_ask_value_type(ptObject[iDx], pszAttrName, &ValueTypeOfProp, &pszValType));

        if(ValueTypeOfProp == PROP_typed_reference || ValueTypeOfProp == PROP_untyped_reference)
        {
            logical bIsPropTypeArray = false;

            /* Setting the output argument to "true" because property is of type reference. */
            (*pbIsRefAttr) = true;

            ETO4_ITKCALL(iRetCode, ETO4_obj_is_property_type_array(ptObject[iDx], pszAttrName, &bIsPropTypeArray));

            if(iRetCode == ITK_ok)
            {
                if(bIsPropTypeArray == true)
                {
                    int    iCount   = 0;
                    tag_t* ptRefObj = NULL;

                    ETO4_ITKCALL(iRetCode, AOM_ask_value_tags(ptObject[iDx], pszAttrName, &iCount, &ptRefObj));

                    for(int iNx = 0; iNx < iCount && iRetCode == ITK_ok; iNx++)
                    {
                        ETO4_UPDATE_TAG_ARRAY((*piNewObjCnt), (*pptNewObj), ptRefObj[iNx]);
                    }

                    ETO4_MEM_TCFREE(ptRefObj);
                }
                else
                {
                    tag_t tAttrVal = NULLTAG;

                    ETO4_ITKCALL(iRetCode, AOM_ask_value_tag(ptObject[iDx], pszAttrName, &tAttrVal));

                    if(iRetCode == ITK_ok)
                    {
                        ETO4_UPDATE_TAG_ARRAY((*piNewObjCnt), (*pptNewObj), tAttrVal);
                    }
                }
            }
        }
        else
        {
            (*pbIsRefAttr) = false;
            break;
        }

        ETO4_MEM_TCFREE(pszValType);
    }

    return iRetCode;
}






