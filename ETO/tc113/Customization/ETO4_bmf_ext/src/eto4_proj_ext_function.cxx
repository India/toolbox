/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_proj_ext_function.cxx

    Description: This File contains custom functions that are used by BMF extension functions defined in file 'eto4_bmf_extension.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>
#include <eto4_object_expansion_function.hxx>
#include <eto4_miscellaneous_ext_function.hxx>
#include <eto4_proj_ext_function.hxx>

/**
 * This function will propagate project to related objects. Function will construct preference name using type of "tSrcObject" i.e. "<Source Object Type>_Propagate_Project"
 * Input source and destination object are validate using preference. If input source and destination object are associated with appropriate relation type and this information is present
 * in the preference then the project propagation will be performed else it is skipped. Each line of above mentioned preference value follows specific notation: 
 *                 Relation Type>Secondary Type>Source Attribute Info On Primary>Destination Attribute Info On Secondary>Value
 * Example:
 *                  TC_Is_Represented_By>A4_Design_CADRevision>items_tag.project_list>items_tag.project_list>
 *
 * Above line defined that propagation can happen from primary object (here: defined on its item) to the secondary object of type A4_Design_CADRevision, attached with relation type "TC_Is_Represented_By"
 * to its property project_list (here defined on its item).Relation expansion can have value "<" or ">".
 *
 * ">" Implies: L2R relations: "L2R" means "Left to Right" expansion, and describes an expansion starting from primary object to secondary object. The symbol ">" describes the direction from 
 *              left to right. Expansion form L2R can use any GRM Relation.
 * "<" Implies: R2L relations: "R2L" means "Right to Left" expansion, and describes an expansion starting from secondary object to primary object. The symbol "<" describes the direction from
 *              right to left. Expansion form R2L can use any GRM Relation.
 * Error:
 *       Preference values "%1$" is incorrect. Preference value for object expansion information follows pattern which looks like: 
 *         " Relation Type > Secondary Type > Source Attribute Info On Primary > Destination Attribute Info On Secondary > Value "
 *
 * @param[in] tSrcObject   Tag of source object.
 * @param[in] pszAttrName  Attribute name. This argument will not be NULL is this function is invoked from property set extension. If function is invoked from property set extension
 *                         then only process those line of project propagation preference which has same source attribute as property set extension attribute. 
 *                         If input attribute 'pszAttrName' is empty (NULLTAG) then it means that project propagation is evoked during GRM_create operation and so in this case we have
 *                         to find the value which has to be propagated using source object.
 * @param[in] iProjCount   Count of project to be assigned.
 * @param[in] ptProject    List of Project tags which need to be assigned.
 * @param[in] tDestObject  Tag of destination object.
 * @param[in] pszRelType   Relation type.
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_propagate_project_info
(
    tag_t  tSrcObject,
    char*  pszAttrName,
    int    iProjCount,
    tag_t* ptProject,
    tag_t  tDestObject,
    char*  pszRelType
)
{
    int    iRetCode     = ITK_ok;
    int    iPrefLineCnt = 0;
    char** ppszPrefVals = NULL;

    ETO4_ITKCALL(iRetCode, ETO4_get_propagation_pref_info(tSrcObject, ETO4_PREF_PROPAGATE_PROJECT_VALUE_POSTFIX, &iPrefLineCnt, &ppszPrefVals));
    
    if(iPrefLineCnt > 0)
    {
        map<string, sPrefLineInfo> mObjExpanPrefInfo;
        std::map<string, sPrefLineInfo>::iterator iterObExPrefMap;

        ETO4_ITKCALL(iRetCode, ETO4_collect_obj_expansion_info(iPrefLineCnt, ppszPrefVals, &mObjExpanPrefInfo));

        for(iterObExPrefMap = mObjExpanPrefInfo.begin(); iterObExPrefMap != mObjExpanPrefInfo.end() && iRetCode == ITK_ok; iterObExPrefMap++)
        {
            logical bSkip = false;

            /* This check determines if the function is getting invoked through 'GRM_create'. In this scenario only those line of object expansion
               preference are use full which starts with the same relation type 'pszRelType' which is input argument to this function.
            */
            if(tDestObject != NULLTAG)
            {
                if(tc_strcmp(pszRelType, iterObExPrefMap->first.c_str()) != 0)
                {
                    bSkip = true;
                }
            }

            for(int iDx = 0; iDx < iterObExPrefMap->second.vAttrNameVal.size() && bSkip == false && iRetCode == ITK_ok; iDx++)
            {
                logical bIsValidType = false;
                string  strDestObjType;
                string  srtSrcAttr;   
                string  strDestAttr;
                string  strValue;

                ETO4_ITKCALL(iRetCode, ETO4_get_obj_expansion_info(iterObExPrefMap->second.vAttrNameVal.at(iDx), &strDestObjType, &srtSrcAttr, &strDestAttr, &strValue));
                /* This check ensures that if destination object tag is not null then the type of destination must be part of preference which is currently being processed. */
                ETO4_ITKCALL(iRetCode, ETO4_validate_destination_obj(tDestObject, strDestObjType, &bIsValidType));

                if(bIsValidType == true && iRetCode == ITK_ok)
                {
                    ETO4_ITKCALL(iRetCode, ETO4_verify_project_attr(srtSrcAttr, strDestAttr));

                    ETO4_ITKCALL(iRetCode, ETO4_process_project_propagation(tSrcObject, tDestObject, strDestObjType, srtSrcAttr, iterObExPrefMap->first.c_str(), 
                                                                        iterObExPrefMap->second.vExpansionInfo.at(iDx), strDestAttr, pszAttrName, iProjCount, ptProject));
                }
            }
            
            /* Exit the loop if destination object is not null and preference corresponding to the relation type has been processed. */
            if(tDestObject != NULLTAG && bSkip == false)
            {
                break;
            }
        }
    }

    ETO4_MEM_TCFREE(ppszPrefVals);
    return iRetCode;
}

/**
 * This function validates if input strings contains information about attribute name. Function throws error if input strings "srtSrcAttr" and "strDestAttr" does not contains attribute "project_list"
 * as sub string.
 *
 * @param[in] srtSrcAttr  Information about attribute which is present on source object and will be used to extract information for propagation.
 * @param[in] strDestAttr Information about attribute which is present on destination object and will be used for setting the information that was extracted from source object.
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_verify_project_attr
(
    string srtSrcAttr,
    string strDestAttr
)
{
    int     iRetCode    = ITK_ok;
    logical bFoundAttr = false;

    ETO4_ITKCALL(iRetCode, ETO4_string_find_sub_str((char*)(srtSrcAttr.c_str()), ETO4_ATTR_PROJECT_LIST, &bFoundAttr));
    
    if(iRetCode == ITK_ok)
    {
        if(bFoundAttr == true)
        {
            bFoundAttr = false;

			ETO4_ITKCALL(iRetCode, ETO4_string_find_sub_str((char*)(strDestAttr.c_str()), ETO4_ATTR_PROJECT_LIST, &bFoundAttr));

            if(bFoundAttr == false && iRetCode == ITK_ok)
            {
                iRetCode = ATTR_INFO_NOT_DEFINED_PROPERLY_USING_OBJ_EXPANSION_NOTATION;
                EMH_store_error_s1(EMH_severity_error, iRetCode, strDestAttr.c_str());
            }
        }
        else
        {
            iRetCode = ATTR_INFO_NOT_DEFINED_PROPERLY_USING_OBJ_EXPANSION_NOTATION;
            EMH_store_error_s1(EMH_severity_error, iRetCode, srtSrcAttr.c_str());
        }
    }

    return iRetCode;
}

/**
 * This function assigns input project information to the destination object. This function can be invoked during GRM_create operation or set property operation. Destination object "tDestObject" can
 * be NULLTAG then function will extract related to "tSrcObject" (source object) associated with relation "strRelType" and consider then as destination objects. Source object can be primary or
 * secondary object based on relation expansion information which is taken as input argument "strExpDir". Relation expansion can have value "<" or ">".
 *
 * ">" Implies: L2R relations: "L2R" means "Left to Right" expansion, and describes an expansion starting from primary object to secondary object. The symbol ">" describes the direction from 
 *              left to right. Expansion form L2R can use any GRM Relation.
 * "<" Implies: R2L relations: "R2L" means "Right to Left" expansion, and describes an expansion starting from secondary object to primary object. The symbol "<" describes the direction from
 *              right to left. Expansion form R2L can use any GRM Relation.
 *
 * If value of "strExpDir" is ">" then "tSrcObj" is primary object we need to find secondary objects which are connected to primary object with relation "strRelType".
 * If value of "strExpDir" is "<" then "tSrcObj" is secondary object we need to find primary objects which are connected to secondary object with relation "strRelType".
 *
 * If input attribute 'pszAttrName' is empty then it means that project propagation is evoked during GRM_create operation and so in this case we have to find the value which has to be propagated
 * using source object.
 *
 * Attribute information supplied through argument "srtSrcAttr" and "strDestAttr" can be defined using object expansion definition.
 *
 * @param[in] tSrcObject     Tag of source object.
 * @param[in] tDestObject    Tag of destination object.
 * @param[in] strDestObjType Destination object type.
 * @param[in] srtSrcAttr     Information about attribute which may or may not be directly present on source object. 
 *                           For e.g. "items_tag.project_list" in this case the attribute to be set is "project_list". This attribute is present on Item of source object.
 * @param[in] strRelType     Relation type.
 * @param[in] strExpDir      Relation expansion direction.
 * @param[in] strDestAttr    Information about attribute which may or may not be directly present on destination object. 
 *                           For e.g. "items_tag.project_list" in this case the attribute to be set is "project_list". This attribute is present on Item of destination object.
 * @param[in] pszAttrName    Attribute name. This argument will not be NULL is this function is invoked from property set extension. If function is invoked from property set extension
 *                           then only process those line of project propagation preference which has same source attribute as property set extension attribute. 
 *                           If input attribute 'pszAttrName' is empty (NULLTAG) then it means that project propagation is evoked during GRM_create operation and so in this case we have
 *                           to find the value which has to be propagated using source object.
 * @param[in] iProjCount     Count of project to be assigned.
 * @param[in] ptProject      List of Project tags which need to be assigned.
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_process_project_propagation
(
    tag_t  tSrcObject,
    tag_t  tDestObject,
    string strDestObjType,
    string srtSrcAttr,
    string strRelType,
    string strExpDir,
    string strDestAttr,
    char*  pszAttrName,
    int    iProjCount,
    tag_t* ptProject
)
{
    int    iRetCode         = ITK_ok;
    int    iActualSrcObjCnt = 0;
    int    iTempProjCnt     = 0;
    tag_t* ptActualSrcObj   = NULL;
    tag_t* ptTempProjList   = NULL;
    string strActualSrcAttr;

    /* Make a copy of variable value. */
    ETO4_CLONE_TAG_ARRAY(iTempProjCnt, ptTempProjList, iProjCount, ptProject);

    ETO4_ITKCALL(iRetCode, ETO4_get_actual_attr_name_and_obj(tSrcObject, srtSrcAttr, &iActualSrcObjCnt, &ptActualSrcObj, &strActualSrcAttr));

    if(iRetCode == ITK_ok && iActualSrcObjCnt > 0)
    {
        logical bPerform  = false;
        /* If input attribute 'pszAttrName' is empty then it means that project propagation is evoked during GRM_create operation and so in this case we have to find the
            value which has to be propagated using source object.
        */
        if(pszAttrName == NULL || tc_strlen(pszAttrName) == 0)
        {
            iTempProjCnt = 0;

            ETO4_MEM_TCFREE(ptTempProjList);

            bPerform = true;

            if(iActualSrcObjCnt > 0)
            {
                /* If more than one source object exist for retrieving attribute value then last source object in the array will be used. */
                ETO4_ITKCALL(iRetCode, AOM_ask_value_tags(ptActualSrcObj[iActualSrcObjCnt-1], strActualSrcAttr.c_str(), &iTempProjCnt, &ptTempProjList));
            }
        }
        else
        {   /* This check ensures that if function is invoked from property set extension then only process those line of project propagation preference which 
                has same source attribute as property set extension attribute.
            */
            if(tc_strcmp(pszAttrName, strActualSrcAttr.c_str()) == 0)
            {
                bPerform = true;
            }
        }

        if(bPerform == true && iRetCode == ITK_ok && iTempProjCnt > 0)
        {
            ETO4_ITKCALL(iRetCode, ETO4_assign_project_to_related_obj(tSrcObject, tDestObject, strDestObjType, strRelType, strExpDir, strDestAttr, iTempProjCnt, ptTempProjList));
        }
    }
    else
    {
        if(iActualSrcObjCnt == 0 && iRetCode == ITK_ok)
        {
            iRetCode = ATTR_INFO_NOT_DEFINED_PROPERLY_USING_OBJ_EXPANSION_NOTATION;
            EMH_store_error_s1(EMH_severity_error, iRetCode, srtSrcAttr.c_str());
        }
    }

    ETO4_MEM_TCFREE(ptTempProjList);
    ETO4_MEM_TCFREE(ptActualSrcObj);
    return iRetCode;
}

/**
 * This function will assign input project information to the destination object. Destination object "tDestObject" can be NULLTAG then function will extract related to "tSrcObject" (source object) 
 * associated with relation "strRelType" and consider then as destination objects. Source object can be primary or secondary object based on relation expansion information which is taken as input
 * argument "strExpDir". Relation expansion can have value "<" or ">". 
 *
 * ">" Implies: L2R relations: "L2R" means "Left to Right" expansion, and describes an expansion starting from primary object to secondary object. The symbol ">" describes the direction from
 *              left to right. Expansion form L2R can use any GRM Relation.
 * "<" Implies: R2L relations: "R2L" means "Right to Left" expansion, and describes an expansion starting from secondary object to primary object. The symbol "<" describes the direction from
 *              right to left. Expansion form R2L can use any GRM Relation.
 *
 * If value of "strExpDir" is ">" then "tSrcObj" is primary object we need to find secondary objects which are connected to primary object with relation "strRelType".
 * If value of "strExpDir" is "<" then "tSrcObj" is secondary object we need to find primary objects which are connected to secondary object with relation "strRelType".
 *
 * @param[in] tSrcObject     Tag of source object.
 * @param[in] tDestObject    Tag of destination object.
 * @param[in] strDestObjType Destination object type.
 * @param[in] strRelType     Relation type.
 * @param[in] strExpDir      Relation expansion direction.
 * @param[in] strDestAttr    Information about attribute which may or may not be directly present on destination object. 
 *                           For e.g. "items_tag.project_list" in this case the attribute to be set is "project_list". This attribute is present on Item of destination object.
 * @param[in] iProjCount     Count of project to be assigned.
 * @param[in] ptProject      List of Project tags which need to be assigned.
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_assign_project_to_related_obj
(
    tag_t  tSrcObject,
    tag_t  tDestObject,
    string strDestObjType,
    string strRelType,
    string strExpDir,
    string strDestAttr,
    int    iProjCount,
    tag_t* ptProject
)
{
    int    iRetCode  = ITK_ok;
    int    iObjCnt   = 0;
    tag_t* ptObjList = NULL;

    if(tDestObject == NULLTAG)
    {
        ETO4_ITKCALL(iRetCode, ETO4_get_related_obj_using_obj_expan(tSrcObject, strDestObjType, strRelType, strExpDir, &iObjCnt, &ptObjList));
    }
    else
    {
        ETO4_UPDATE_TAG_ARRAY(iObjCnt, ptObjList, tDestObject);
    }

    if(iObjCnt > 0)
    {
        for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
        {
            int    iActualDestObjCnt = 0;
            tag_t* ptActualDestObj   = NULL;
            string strActualDestAttr;

            ETO4_ITKCALL(iRetCode, ETO4_get_actual_attr_name_and_obj(ptObjList[iDx], strDestAttr, &iActualDestObjCnt, &ptActualDestObj, &strActualDestAttr));
        
            if(iActualDestObjCnt > 0)
            {
                ETO4_ITKCALL(iRetCode, ETO4_assign_project(iActualDestObjCnt, ptActualDestObj, iProjCount, ptProject));
            }

            ETO4_MEM_TCFREE(ptActualDestObj);
        }
    }

    ETO4_MEM_TCFREE(ptObjList);
    return iRetCode;
}

/**
 * This function assigns the projects to the input list of Objects. This function traverse the input project list and segregate the project which is not yet assigned to the 
 * input objects individually.
 *
 * @param[in] iObjCnt    Object Count
 * @param[in] ptObjects  Object tags to which project will be assigned 
 * @param[in] iProjCnt   Project Count
 * @param[in] ptProjects Tags of project to be assigned
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_assign_project
(
    int    iObjCnt,
    tag_t* ptObjects,
    int    iProjCnt,
    tag_t* ptProjects
)
{
    int iRetCode = ITK_ok;

    if(iObjCnt > 0 && iProjCnt > 0)
    {
        int    iValidPrjCnt    = 0;
        tag_t  tLoggedInUser   = NULLTAG;
        tag_t* ptValidProjects = NULL;
        char*  pszUserID       = NULL;
        
        ETO4_ITKCALL(iRetCode, ETO4_get_logged_in_user(&tLoggedInUser, &pszUserID));

        for(int iFx = 0; iFx < iProjCnt && iRetCode == ITK_ok; iFx++)
        {
            logical bIsPrivMember = false;

            ETO4_ITKCALL(iRetCode, PROJ_is_user_a_privileged_member(ptProjects[iFx], tLoggedInUser, &bIsPrivMember));

            if(bIsPrivMember == true)
            {
                ETO4_UPDATE_TAG_ARRAY(iValidPrjCnt, ptValidProjects, ptProjects[iFx]);
            }
            else
            {
                char* pszPrjUID = NULL;

                POM_tag_to_uid(ptProjects[iFx], &pszPrjUID);

                TC_write_syslog("INFO:libA4_bmf_ext: User %s is not a privileged member of the project %s.\n", pszUserID, pszPrjUID);

                ETO4_MEM_TCFREE(pszPrjUID);
            }
        }

        if(iValidPrjCnt > 0)
        {
            for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
            {
                int    iUnassignedProjCnt   = 0;
                tag_t* ptUnassignedProjects = NULL;

                ETO4_ITKCALL(iRetCode, ETO4_get_list_of_unassigned_project(ptObjects[iDx], iValidPrjCnt, ptValidProjects, &iUnassignedProjCnt, &ptUnassignedProjects));

                if(iUnassignedProjCnt != 0)
                {
                    ETO4_ITKCALL(iRetCode, PROJ_assign_objects(iUnassignedProjCnt, ptUnassignedProjects, 1, &(ptObjects[iDx])));
                }

                ETO4_MEM_TCFREE(ptUnassignedProjects);
            }
        }

        ETO4_MEM_TCFREE(ptValidProjects);
        ETO4_MEM_TCFREE(pszUserID);
    }

    return iRetCode;
}

/**
 * This function traverse the input project list and segregate the project which is not yet assigned to the input object "tObject".
 *
 * @param[in]  tObject               Object tag to which project will be assigned 
 * @param[in]  iProjCnt              Project Count
 * @param[in]  ptProjects            Tags of project to be assigned
 * @param[out] piUnassignedProjCnt   Count of projects which are not yet assigned to the input object "tObject"
 * @param[out] pptUnassignedProjects Unassigned list of projects
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_get_list_of_unassigned_project
(
    tag_t   tObject,
    int     iProjCnt,
    tag_t*  ptProjects,
    int*    piUnassignedProjCnt,
    tag_t** pptUnassignedProjects
)
{
    int    iRetCode        = ITK_ok;
    int    iAssignedPrjCnt = 0;
    tag_t* ptAssignedPrj   = NULL;

    /* Get the project assigned to the Object   */
    ETO4_ITKCALL(iRetCode, AOM_ask_value_tags(tObject, ETO4_ATTR_PROJECT_LIST, &iAssignedPrjCnt, &ptAssignedPrj));

    for(int iDx = 0; iDx < iProjCnt && iRetCode == ITK_ok; iDx++)
    {
        logical bFound = false;

        for(int iJx = 0; iJx < iAssignedPrjCnt; iJx++)
        {
            if(ptAssignedPrj[iJx] == ptProjects[iDx])
            {
                bFound = true;
                break;
            }
        }

        if(bFound == false)
        {
            ETO4_UPDATE_TAG_ARRAY((*piUnassignedProjCnt), (*pptUnassignedProjects), ptProjects[iDx]);
        }
    }

    ETO4_MEM_TCFREE(ptAssignedPrj);
    return iRetCode;
}

/**
 * Function is designed to evaluate input list "ptObject" and replace any item revision with its Item tag so that Project assignment is synchronized between Item and its Revision. This function will
 * traverse through the input list of objects and will construct new list of objects with one exception i.e. if function finds any Item Revision in the input list of object "ptObject" then function
 * will replace Item Revision with its Item tag in the new list "pptNewList". 
 *
 * @param[in]  tObject    Count Of Objects
 * @param[in]  ptObject   Tags of objects 
 * @param[out] pptNewList New Object List with Item tags instead if Item Revision.
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_construct_proj_asgmt_list
(
    int     iObjCnt,
    tag_t*  ptObject,
    tag_t** pptNewList
)
{
    int iRetCode = ITK_ok;
    int iCount   = 0;

    for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
    {
        tag_t   tObject   = NULLTAG;
        logical bIsIRType = false;

        ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(ptObject[iDx], ETO4_CLASS_ITEMREVISION, &bIsIRType));

        if(iRetCode == ITK_ok)
        {
            if(bIsIRType == true)
            {
				ETO4_ITKCALL(iRetCode, AOM_ask_value_tag(ptObject[iDx], ETO4_ATTR_ITEM_TAG, &tObject));
            }
            else
            {
                tObject = ptObject[iDx];
            }

            if(iRetCode == ITK_ok)
            {
                ETO4_UPDATE_TAG_ARRAY(iCount, (*pptNewList), tObject);
            }
        }
    }

    return iRetCode;
}


