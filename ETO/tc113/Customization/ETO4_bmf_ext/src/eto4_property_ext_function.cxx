/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_property_ext_function.cxx

    Description: This File contains custom functions that are used by BMF extension functions defined in file 'eto4_bmf_extension.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>
#include <eto4_object_expansion_function.hxx>
#include <eto4_miscellaneous_ext_function.hxx>
#include <eto4_property_ext_function.hxx>

/**
 *
 * @param[in] tSrcObject 
 * @param[in] tDestObject
 * @param[in] iProjCount
 * @param[in] ptProject
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_check_src_obj_for_rev_processing 
(
    tag_t   tSrcObject,
    int*    piRevCnt,
    tag_t** pptItemRev
)
{
    int    iRetCode   = ITK_ok;
    int    iPrefCnt   = 0;
    char*  pszObjType = NULL;
    char** ppsPrefVal = NULL;
    
    /* Initializing out parameter of this function. */
    (*piRevCnt)   = 0;
    (*pptItemRev) = NULL;

    ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(tSrcObject, &pszObjType));

    ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values(ETO4_PREF_PROPERTY_PROPAGATION_ITEM_TYPE_LIST, &iPrefCnt, &ppsPrefVal));

    for(int iDx = 0; iDx < iPrefCnt && iRetCode == ITK_ok; iDx++)
    {
        if(tc_strcmp(pszObjType, ppsPrefVal[iDx]) == 0)
        {
            ETO4_ITKCALL(iRetCode, ITEM_list_all_revs(tSrcObject, piRevCnt, pptItemRev));
            break;
        }
    }

    ETO4_MEM_TCFREE(pszObjType);
    ETO4_MEM_TCFREE(ppsPrefVal);
    return iRetCode;
}

/**
 * L2R relations: "L2R" means "Left to Right" expansion, and describes an expansion starting from primary object to secondary object. The symbol ">" describes the direction from
 *                left to right as shown above. Expansion form L2R can use any GRM Relation
 * R2L relations: "R2L" means �Right to Left� expansion, and describes an expansion starting from secondary object to primary object. The symbol �<� describes the direction from
 *                left to right.
 * Error:
 *       Preference values "%1$" is incorrect. Preference value of object expansion information follows pattern which looks like: " Relation Type > Secondary Type > Source Attribute Info On Primary > Destination Attribute Info On Secondary > Value "
 *
 * @param[in]  pszPrefName         Preference Name
 * @param[in]  ppszPrefValue       String value of preference
 * @param[out] pmObjExpanPrefInfo 
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_propagate_prop_val
(
    tag_t   tSrcObject,
    char*   pszAttrName,
    char*   pszAttrVal,
    tag_t   tDestObject,
    char*   pszRelType,
    logical bOnlyValidate
)
{
    int    iRetCode     = ITK_ok;
    int    iPrefLineCnt = 0;
    char** ppszPrefVals = NULL;
    
    ETO4_ITKCALL(iRetCode, ETO4_get_property_propagation_pref_info(tSrcObject, ETO4_PREF_PROPAGATE_ATTR_VALUE_POSTFIX, bOnlyValidate, &iPrefLineCnt, &ppszPrefVals));
    
    if(iPrefLineCnt > 0)
    {
        map<string, sPrefLineInfo> mObjExpanPrefInfo;
        std::map<string, sPrefLineInfo>::iterator iterObExPrefMap;

        ETO4_ITKCALL(iRetCode, ETO4_collect_obj_expansion_info(iPrefLineCnt, ppszPrefVals, &mObjExpanPrefInfo));

        for(iterObExPrefMap = mObjExpanPrefInfo.begin(); iterObExPrefMap != mObjExpanPrefInfo.end() && iRetCode == ITK_ok; iterObExPrefMap++)
        {
            logical bSkip = false;

            /* This check determines if the function is getting invoked through 'GRM_create'. In this scenario only those line of object expansion
               preference are use full which starts with the same relation type 'pszRelType' which is input argument to this function.
            */
            if(tDestObject != NULLTAG)
            {
                if(tc_strcmp(pszRelType, iterObExPrefMap->first.c_str()) != 0)
                {
                    bSkip = true;
                }
            }

            for(int iDx = 0; iDx < iterObExPrefMap->second.vAttrNameVal.size() && bSkip == false && iRetCode == ITK_ok; iDx++)
            {
                logical bIsValidType = false;
                string  strDestObjType;
                string  srtSrcAttr;   
                string  strDestAttr;
                string  strValue;

                ETO4_ITKCALL(iRetCode, ETO4_get_obj_expansion_info(iterObExPrefMap->second.vAttrNameVal.at(iDx), &strDestObjType, &srtSrcAttr, &strDestAttr, &strValue));
                
                /* This check ensures that if destination object tag is not null then the type of destination must be part of preference which is currently being processed. */
                ETO4_ITKCALL(iRetCode, ETO4_validate_destination_obj(tDestObject, strDestObjType, &bIsValidType));

                if(bIsValidType == true && iRetCode == ITK_ok)
                {
                    ETO4_ITKCALL(iRetCode, ETO4_process_property_propagation(tSrcObject, tDestObject, strDestObjType, srtSrcAttr, iterObExPrefMap->first.c_str(), iterObExPrefMap->second.vExpansionInfo.at(iDx),
                                                                         strDestAttr, strValue, pszAttrName, pszAttrVal, bOnlyValidate));
                }
            }

            /* Exit the loop if destination object is not null and preference corresponding to the relation type has been processed. */
            if(tDestObject != NULLTAG && bSkip == false)
            {
                break;
            }
        }
    }

    ETO4_MEM_TCFREE(ppszPrefVals);
    return iRetCode;
}

/**
 * 
 * @param[in]
 * @param[in]
 * @param[out]
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_get_property_propagation_pref_info
(
    tag_t   tPrimaryObj,
    char*   pszPrefPostFix,
    logical bOnlyValidate,
    int*    piPrefValCnt, 
    char*** pppszPrefVals    
)
{
    int    iRetCode        = ITK_ok;
    int    iValCount       = 0;
    int    iExcludeInfoCnt = 0;
    char*  pszObjType      = NULL;
    char** ppszValue       = NULL;
    char** ppszExcludeInfo = NULL;

    ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(tPrimaryObj, &pszObjType));

    ETO4_ITKCALL(iRetCode, ETO4_get_propagation_pref_info(tPrimaryObj, pszPrefPostFix, &iValCount, &ppszValue));

    if(bOnlyValidate == true && iValCount > 0)
    {
        ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values(ETO4_PREF_EXCLUDE_PROP_PROPAGATION_VALIDATION_ATTR_LIST, &iExcludeInfoCnt, &ppszExcludeInfo));  
    }

    for(int iDx = 0; iDx < iValCount && iRetCode == ITK_ok; iDx++)
    {
        char*   pszObjExpDir        = NULL;
        char*   pszPropPrefLineTemp = NULL;
        logical bExcludePrefLine    = false;
                
        ETO4_ITKCALL(iRetCode, ETO4_get_obj_exp_direction(ppszValue[iDx], &pszObjExpDir));

        if(iRetCode == ITK_ok && bOnlyValidate == true)
        {
            ETO4_STRCPY(pszPropPrefLineTemp, pszObjType);
            ETO4_STRCAT(pszPropPrefLineTemp, pszObjExpDir);
            ETO4_STRCAT(pszPropPrefLineTemp, ppszValue[iDx]);
            
            for(int iNx = 0; iNx < iExcludeInfoCnt; iNx++)
            {            
                if(tc_strcmp(pszPropPrefLineTemp, ppszExcludeInfo[iNx]) == 0)
                {
                    bExcludePrefLine = true;
                    break;
                }
            }
        }

        if(bExcludePrefLine == false  && iRetCode == ITK_ok)
        {
            ETO4_UPDATE_STRING_ARRAY((*piPrefValCnt), (*pppszPrefVals), ppszValue[iDx]);
        }

        ETO4_MEM_TCFREE(pszObjExpDir);
        ETO4_MEM_TCFREE(pszPropPrefLineTemp);
    }

    ETO4_MEM_TCFREE(pszObjType);
    ETO4_MEM_TCFREE(ppszValue);
    ETO4_MEM_TCFREE(ppszExcludeInfo);
    return iRetCode;
}

/**
 * This function will get the direction of expansion by analyzing input argument "ppszInfo" which contains attribute propagation information. If input argument "ppszInfo" contains character
 * ">" as sub string then it means the information will be expanded from "L2R". If input argument "ppszInfo" contains character "<" as sub string then it means the information will be expanded from
 * "R2L"
 *
 * L2R: "L2R" means "Left to Right" expansion, and describes an expansion starting from primary object to secondary object. The symbol ">" describes the direction from
 *       left to right as shown above. Expansion form L2R can use any GRM Relation
 * R2L: "R2L" means �Right to Left� expansion, and describes an expansion starting from secondary object to primary object. The symbol �<� describes the direction from
 *       left to right.
 *
 * Input argument "ppszInfo" cannot contain character "<" and ">" at the same time.
 * 
 * @param[in]  ppszInfo      String containing attribute propagation information.
 * @param[out] ppszObjExpDir String which can have value "<" or ">" depending upon direction of expansion.
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_get_obj_exp_direction
(
    char*  ppszInfo,
    char** ppszObjExpDir
)
{
    int     iRetCode = ITK_ok;
    logical L2RExpan = false;
    logical R2LExpan = false;

    ETO4_ITKCALL(iRetCode, ETO4_string_find_sub_str(ppszInfo, ETO4_STRING_GREATER_THAN, &L2RExpan));

    ETO4_ITKCALL(iRetCode, ETO4_string_find_sub_str(ppszInfo, ETO4_STRING_LESS_THAN, &R2LExpan));

    if(iRetCode == ITK_ok)
    {
        if((L2RExpan == true && R2LExpan == true) || (L2RExpan == false && R2LExpan == false))
        {
            iRetCode = OBJ_EXPANSION_INFO_NOT_DEFINED_PROPERLY;
            EMH_store_error_s1(EMH_severity_error, iRetCode, ppszInfo);
        }
        else
        {
            if(L2RExpan == true)
            {
                ETO4_STRCPY((*ppszObjExpDir), ETO4_STRING_GREATER_THAN);
            }
            else
            {
                ETO4_STRCPY((*ppszObjExpDir), ETO4_STRING_LESS_THAN);
            }
        }
    }

    return iRetCode;
}

/**
 *
 * @param[in]  vAttrNameValInfo
 * @param[out] pstrDestObjType
 * @param[out] psrtSrcAttr
 * @param[out] pstrDestAttr
 * @param[out] pstrValue
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_process_property_propagation
(
    tag_t   tSrcObject,
    tag_t   tDestObject,
    string  strDestObjType,
    string  srtSrcAttr,
    string  strRelType,
    string  strExpDir,
    string  strDestAttr,
    string  strValue,
    char*   pszAttrName,
    char*   pszAttrValFromExt,
    logical bOnlyValidate
)
{
    int     iRetCode         = ITK_ok;
    int     iActualSrcObjCnt = 0;
    char*   pszTempAttrVal   = NULL;
    tag_t*  ptActualSrcObj   = NULL;
    logical bPerform         = false;
    string strActualSrcAttr;

    /* Make a copy of variable value. */
    ETO4_STRCPY(pszTempAttrVal, pszAttrValFromExt);

    if(tc_strcmp(srtSrcAttr.c_str(), ETO4_NO_VAL) == 0)
    {
        if(tc_strcmp(strDestAttr.c_str(), ETO4_NO_VAL) != 0 && tc_strcmp(strValue.c_str(), ETO4_NO_VAL) != 0)
        {
            bPerform = true;
            ETO4_MEM_TCFREE(pszTempAttrVal);
            ETO4_STRCPY(pszTempAttrVal, strValue.c_str());
        }
        else
        {
            iRetCode = ATTR_INFO_NOT_DEFINED_PROPERLY_USING_OBJ_EXPANSION_NOTATION;
            EMH_store_error_s1(EMH_severity_error, iRetCode, srtSrcAttr.c_str());
        }
    }
    else
    {
        ETO4_ITKCALL(iRetCode, ETO4_get_actual_attr_name_and_obj(tSrcObject, srtSrcAttr, &iActualSrcObjCnt, &ptActualSrcObj, &strActualSrcAttr));

        if(iRetCode == ITK_ok)
        {   /* If the input attribute 'pszAttrName' is empty then it means that property propagation is evoked during GRM_create operation and so in this case we have to find the
               value which has to be propagated using source object.
            */
            if(pszAttrName == NULL || tc_strlen(pszAttrName) == 0)
            {
                ETO4_MEM_TCFREE(pszTempAttrVal);

                bPerform = true;

                if(iActualSrcObjCnt > 0)
                {
                    /* If more than one source object exist for retrieving attribute value then last source object in the array will be used. */
                    ETO4_ITKCALL(iRetCode, AOM_ask_value_string(ptActualSrcObj[iActualSrcObjCnt-1], strActualSrcAttr.c_str(), &pszTempAttrVal));
                }
                else
                {
                    iRetCode = ATTR_INFO_NOT_DEFINED_PROPERLY_USING_OBJ_EXPANSION_NOTATION;
                    EMH_store_error_s1(EMH_severity_error, iRetCode, srtSrcAttr.c_str());
                }
            }
            else
            {   /* This check ensures that if function is invoked from property set extension then only process those line of project propagation preference which 
                    has same source attribute as property set extension attribute.
                */
                if(tc_strcmp(pszAttrName, strActualSrcAttr.c_str()) == 0)
                {
                    bPerform = true;
                }
            }
        }
    }

    if(bPerform == true && iRetCode == ITK_ok && tc_strlen(pszTempAttrVal) > 0)
    {
        if(bOnlyValidate == true)
        {
            ETO4_ITKCALL(iRetCode, ETO4_validate_prop_propagation_on_related_obj(tSrcObject, tDestObject, strDestObjType, strRelType, strExpDir, strDestAttr, pszTempAttrVal));
        }
        else
        {
            ETO4_ITKCALL(iRetCode, ETO4_set_prop_on_related_obj(tSrcObject, tDestObject, strDestObjType, strRelType, strExpDir, strDestAttr, pszTempAttrVal));
        }
    }
 
    ETO4_MEM_TCFREE(pszTempAttrVal);
    ETO4_MEM_TCFREE(ptActualSrcObj);
    return iRetCode;
}

/**
 *
 * @param[in] tSrcObject 
 * @param[in] tDestObject
 * @param[in] iProjCount
 * @param[in] ptProject
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_set_prop_on_related_obj
(
    tag_t  tSrcObject,
    tag_t  tDestObject,
    string strDestObjType,
    string strRelType,
    string strExpDir,
    string strDestAttr,
    char*  pszAttrValFromExt
)
{
    int    iRetCode  = ITK_ok;
    int    iObjCnt   = 0;
    tag_t* ptObjList = NULL;
    
    if(tDestObject == NULLTAG)
    {
        ETO4_ITKCALL(iRetCode, ETO4_get_related_obj_using_obj_expan(tSrcObject, strDestObjType, strRelType, strExpDir, &iObjCnt, &ptObjList));
    }
    else
    {
        ETO4_UPDATE_TAG_ARRAY(iObjCnt, ptObjList, tDestObject);
    }

    if(iObjCnt > 0)
    {
        for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
        {
            int    iActualDestObjCnt = 0;
            tag_t* ptActualDestObj   = NULL;
            string strActualDestAttr;

            ETO4_ITKCALL(iRetCode, ETO4_get_actual_attr_name_and_obj(ptObjList[iDx], strDestAttr, &iActualDestObjCnt, &ptActualDestObj, &strActualDestAttr));

            for(int iDx = 0; iDx < iActualDestObjCnt && iRetCode == ITK_ok; iDx++)
            {   
                logical bWriteAccessDenied = false;
                logical bObjCheckedOut     = false;
                logical bModifiable        = false;

                ETO4_ITKCALL(iRetCode, ETO4_check_privilege(ptActualDestObj[iDx], &bWriteAccessDenied, &bObjCheckedOut, &bModifiable));
            
                if(bModifiable == true)
                {
                    char* pszExistingVal = NULL;

                    ETO4_ITKCALL(iRetCode, ETO4_obj_aom_get_attribute_value(ptActualDestObj[iDx], (char*)(strActualDestAttr.c_str()), &pszExistingVal));

                    if(tc_strcmp(pszExistingVal, pszAttrValFromExt) != 0)
                    {
                        ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val(ptActualDestObj[iDx], (char*)(strActualDestAttr.c_str()), pszAttrValFromExt));
                    }

                    ETO4_MEM_TCFREE(pszExistingVal);
                }
            }

            ETO4_MEM_TCFREE(ptActualDestObj);
        }
    }

    ETO4_MEM_TCFREE(ptObjList);
    return iRetCode;
}


/**
 *
 * @param[in] tSrcObject 
 * @param[in] tDestObject
 * @param[in] iProjCount
 * @param[in] ptProject
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_validate_prop_propagation_on_related_obj
(
    tag_t  tSrcObject,
    tag_t  tDestObject,
    string strDestObjType,
    string strRelType,
    string strExpDir,
    string strDestAttr,
    char*  pszAttrValFromExt
)
{
    int    iRetCode  = ITK_ok;
    int    iObjCnt   = 0;
    tag_t* ptObjList = NULL;
    
    if(tDestObject == NULLTAG)
    {
        ETO4_ITKCALL(iRetCode, ETO4_get_related_obj_using_obj_expan(tSrcObject, strDestObjType, strRelType, strExpDir, &iObjCnt, &ptObjList));
    }
    else
    {
        ETO4_UPDATE_TAG_ARRAY(iObjCnt, ptObjList, tDestObject);
    }

    if(iObjCnt > 0)
    {
        for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
        {
            int    iActualDestObjCnt = 0;
            tag_t* ptActualDestObj   = NULL;
            string strActualDestAttr;

            ETO4_ITKCALL(iRetCode, ETO4_get_actual_attr_name_and_obj(ptObjList[iDx], strDestAttr, &iActualDestObjCnt, &ptActualDestObj, &strActualDestAttr));

            for(int iDx = 0; iDx < iActualDestObjCnt && iRetCode == ITK_ok; iDx++)
            {   
                logical bWriteAccessDenied = false;
                logical bObjCheckedOut     = false;
                logical bModifiable        = false;

                ETO4_ITKCALL(iRetCode, ETO4_check_privilege(ptActualDestObj[iDx], &bWriteAccessDenied, &bObjCheckedOut, &bModifiable));
            
                if(iRetCode == ITK_ok)
                {
                    if(bModifiable == true)
                    {
                        char* pszExistingVal = NULL;

                        ETO4_ITKCALL(iRetCode, ETO4_obj_aom_get_attribute_value(ptActualDestObj[iDx], (char*)(strActualDestAttr.c_str()), &pszExistingVal));

                        if(tc_strcmp(pszExistingVal, pszAttrValFromExt) != 0)
                        {
                            if(pszExistingVal != NULL && tc_strlen(pszExistingVal) > 0)
                            {
                                char* pszObjStrVal = NULL;

                                ETO4_ITKCALL(iRetCode, AOM_ask_value_string(ptActualDestObj[iDx], ETO4_ATTR_OBJECT_STRING, &pszObjStrVal));
                                
                                if(iRetCode == ITK_ok)
                                {
                                    iRetCode = REL_CREAT_PROHIBITED_PROP_ALREADY_SET;
                                    EMH_store_error_s3(EMH_severity_error, iRetCode, strActualDestAttr.c_str(), pszObjStrVal, pszExistingVal);
                                }

                                ETO4_MEM_TCFREE(pszObjStrVal);
                            }
                        }

                        ETO4_MEM_TCFREE(pszExistingVal);
                    }
                    else
                    {
                        if(bModifiable == false)
                        {
                            char* pszObjAttrVal = NULL;

                            ETO4_ITKCALL(iRetCode, AOM_ask_value_string(ptActualDestObj[iDx], ETO4_ATTR_OBJECT_STRING, &pszObjAttrVal));
                            /* Report custom error only if there are no OOTB error. */
                            if(iRetCode == ITK_ok)
                            {
                                if(bWriteAccessDenied == true)
                                {
                                    iRetCode = OBJECT_DOES_NOT_HAVE_WRITE_PRIVILEGE;
                                    EMH_store_error_s1(EMH_severity_error, iRetCode, pszObjAttrVal);
                                }
                                else
                                {
                                    if(bObjCheckedOut == true)
                                    {
                                        iRetCode = OBJECT_IS_CHECKED_OUT;
                                        EMH_store_error_s1(EMH_severity_error, iRetCode, pszObjAttrVal);
                                    }
                                }
                            }

                            ETO4_MEM_TCFREE(pszObjAttrVal);
                        }
                    }
                }
            }

            ETO4_MEM_TCFREE(ptActualDestObj);
        }
    }

    ETO4_MEM_TCFREE(ptObjList);
    return iRetCode;
}





