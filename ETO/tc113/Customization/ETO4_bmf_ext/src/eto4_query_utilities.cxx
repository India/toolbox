/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_query_utilities.cxx

    Description: This file contains functions related to POM queries.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/


#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>

static tag_t tTmplIRQryInit = NULLTAG;

/**
 * This function construct POM query the search database for "Template Base Revision" sub types whose "object_name" exactly matches with input string "pszIRName" and having released status 
 * of type "pszIRRelStatName". Function returns list of matched "Template Base Revision" sub types that exist in the database which fulfills the search criteria. 
 *
 * @param[in]  pszIRName        Name string with which "Template Base Revision" needs to be searched in the database
 * @param[in]  pszIRRelStatName Release status name 
 * @param[out] piIRCnt          Count of valid Template Revision
 * @param[out] pptItemRev       List of valid Template Revisions
 *
 * @retval ITK_ok if everything went okay, Else ITK return code.
 */
int ETO4_qry_template_ir_using_name_and_status
(
    char*   pszIRName,
    char*   pszIRRelStatName,
    int*    piIRCnt,
    tag_t** pptItemRev
)
{
    int         iRetCode          = ITK_ok;
    int         iRowsCnt          = 0;
    int         iColumnsCnt       = 0;
    const int   iActiveSeq        = 0;
    const char* pszUID            = "puid";
    const char* pszDateReleased   = "date_released";
    const char* pszIRBindId       = "itemRevBind";
    const char* pszIRExprId       = "itemRevExpr";
    const char* pszDateBindId     = "dateBind";
    const char* pszDateExprId     = "dateExpr";
    const char* pszJoinExprId     = "joinExpr";
    const char* pszWhereExprId1   = "whereExpr1";
    const char* pszWhereExprId2   = "whereExpr2";
    const char* pszWhereExprId3   = "whereExpr3";    
    const char* pszActiveSeqValue = "active sequence value";
    const char* pszActiveSeqExpr  = "active sequence expr";
    void***     pppQryRpt         = NULL;

    /* Initializing the Out Parameter to this function. */
    (*piIRCnt) = 0;
    (*pptItemRev) = NULL;

    /* Creating a enquiry with 'ASP_Find_IR_With_ObjeName_Status' which is a unique identifier within the user's current session. This identifier can be any name. */
    if(NULLTAG == tTmplIRQryInit)
    {
        ETO4_ITKCALL(iRetCode, POM_enquiry_create(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT)) ;

        ETO4_ITKCALL(iRetCode, POM_enquiry_add_select_attrs(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, ETO4_CLASS_TPL_BASEREVISION, 1, &pszUID));

        ETO4_ITKCALL(iRetCode, POM_enquiry_add_select_attrs(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, ETO4_CLASS_STATUS, 1, &pszDateReleased));

		ETO4_ITKCALL(iRetCode, POM_enquiry_add_select_attrs(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, ETO4_CLASS_STATUS, 1, &pszUID));

        ETO4_ITKCALL(iRetCode, POM_enquiry_set_string_value(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszIRBindId, 1, (const char**)&pszIRName, POM_enquiry_bind_value));

        ETO4_ITKCALL(iRetCode, POM_enquiry_set_attr_expr(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszIRExprId, ETO4_CLASS_TPL_BASEREVISION, ETO4_ATTR_OBJECT_NAME_ATTRIBUTE, POM_enquiry_equal, pszIRBindId));

        ETO4_ITKCALL(iRetCode, POM_enquiry_set_int_value(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszActiveSeqValue, 1, &iActiveSeq, POM_enquiry_const_value));

        ETO4_ITKCALL(iRetCode, POM_enquiry_set_attr_expr(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszActiveSeqExpr, ETO4_CLASS_TPL_BASEREVISION, ETO4_ATTR_ACTIVE_SEQ, POM_enquiry_not_equal, pszActiveSeqValue));

        ETO4_ITKCALL(iRetCode, POM_enquiry_set_join_expr(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszJoinExprId, ETO4_CLASS_TPL_BASEREVISION, ETO4_ATTR_RELEASE_STATUS_LIST, POM_enquiry_equal, ETO4_CLASS_STATUS, "puid")) ;
    
        ETO4_ITKCALL(iRetCode, POM_enquiry_set_string_value(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszDateBindId, 1, (const char**)&pszIRRelStatName, POM_enquiry_bind_value));
    
        ETO4_ITKCALL(iRetCode, POM_enquiry_set_attr_expr(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszDateExprId, ETO4_CLASS_STATUS, ETO4_ATTR_RELEASE_STATUS_NAME, POM_enquiry_equal, pszDateBindId));
    
        ETO4_ITKCALL(iRetCode, POM_enquiry_set_expr(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszWhereExprId1, pszIRExprId, POM_enquiry_and, pszActiveSeqExpr));
    
        ETO4_ITKCALL(iRetCode, POM_enquiry_set_expr(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszWhereExprId2 , pszJoinExprId, POM_enquiry_and, pszWhereExprId1));
    
        ETO4_ITKCALL(iRetCode, POM_enquiry_set_expr(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszWhereExprId3, pszDateExprId, POM_enquiry_and, pszWhereExprId2));
    
        ETO4_ITKCALL(iRetCode, POM_enquiry_set_where_expr(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszWhereExprId3));
    
        ETO4_ITKCALL(iRetCode, POM_enquiry_set_distinct(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, true));

        ETO4_ITKCALL(iRetCode, POM_enquiry_add_order_attr(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, ETO4_CLASS_TPL_BASEREVISION, ETO4_ATTR_DATE_RELEASED, POM_enquiry_desc_order));

        ETO4_ITKCALL(iRetCode, POM_cache_for_session(&tTmplIRQryInit));

        if(iRetCode == ITK_ok)
        {
            tTmplIRQryInit = 1;
        }
    }
    else
    {
        ETO4_ITKCALL(iRetCode, POM_enquiry_set_string_value(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszIRBindId, 1, (const char**)&pszIRName, POM_enquiry_bind_value));
        
        ETO4_ITKCALL(iRetCode, POM_enquiry_set_string_value(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, pszDateBindId, 1, (const char**)&pszIRRelStatName, POM_enquiry_bind_value));
    }
          
    ETO4_ITKCALL(iRetCode, POM_enquiry_execute(ETO4_QRY_ID_ETO_FIND_TMPLIR_WITH_OBJNAME_STAT, &iRowsCnt, &iColumnsCnt, &pppQryRpt)) ;

    if(iRetCode == ITK_ok)
    {
        for (int iNx = 0; iNx < iRowsCnt; iNx++)
        {
            ETO4_UPDATE_TAG_ARRAY((*piIRCnt), (*pptItemRev), (*(tag_t*)(pppQryRpt[iNx][0])));
        }
    }
    
    ETO4_MEM_TCFREE(pppQryRpt);
    return iRetCode;
}




