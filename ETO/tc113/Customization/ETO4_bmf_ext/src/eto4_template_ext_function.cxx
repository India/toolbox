/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_template_ext_function.cxx

    Description: This File contains custom functions that are used by BMF extension functions defined in file 'eto4_bmf_extension.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>
#include <eto4_template_ext_function.hxx>

/**
 * This function will find the Project Template Revision whose name (object_name) is stored in attribute "eto4_project_template" of input Item Revision "tItemRev". User has selected appropriate temple
 * name (stored in attribute "eto4_project_template") during creation of input revision "tItemRev" to be used as part of cloning process. Function will find approved Project Template with the name as 
 * mentioned in attribute "eto4_project_template". The Folder Hierarchy present under Project Template Revision will be replicated and attached to input Item revision "tItemRev".
 * Attribute "eto4_project_template" present on "ETO4_Proj_BaseRevision" has dynamic LOV attached which will query Approved "Project Template Revision" from the system to be displayed for user 
 * selection during creation of "A4_Project_BaseRevision" sub types.
 * Function will store tag of every valid folder that is present below "Project Template Revision" in a map "mFLRecord" as key and value for every such folder will be tag is newly cloned folder 
 * which will be associated with Project Revision.
 *  
 * @param[in] Item     Newly created Item
 * @param[in] tItemRev Tag of revision which contain Project Template name in attribute "eto4_project_template". Template with specified name will be queried and its folder hierarchy will be cloned
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_copy_template_content
(
    tag_t Item,
    tag_t tItemRev
)
{
    int   iRetCode     = ITK_ok;
    tag_t tTemplateRev = NULLTAG;
    
    ETO4_ITKCALL(iRetCode, ETO4_find_project_template(tItemRev, &tTemplateRev));

    if(tTemplateRev != NULLTAG)
    {
        int    iFLCnt          = 0;
        char** ppzsRelTypeList = NULL;
        tag_t* ptFolder        = NULL;
        map<tag_t, tag_t> mFLRecord;

        ETO4_ITKCALL(iRetCode, ETO4_grm_get_related_obj_2(tTemplateRev, ETO4_CLASS_FOLDER_BASE, NULL, false, &iFLCnt, &ptFolder, &ppzsRelTypeList));

        if(iFLCnt > 0)
        {
            for(int iDx = 0; iDx < iFLCnt && iRetCode == ITK_ok; iDx++)
            {
                tag_t tFolder = NULLTAG;
                tag_t tNewRel = NULLTAG;
                map<tag_t, tag_t>::iterator iterFLInfo = mFLRecord.begin();

                /* This check ensures that any folder which is created during a run of this loop is not created again if it occurs as child sub folder. This situation may appear during
                   cyclic Folder structures. If Folder is already created then it is directly related to Project Revision.
                */
                iterFLInfo = mFLRecord.find(ptFolder[iDx]);

                if(iterFLInfo == mFLRecord.end())
                {
                    ETO4_ITKCALL(iRetCode, ETO4_process_fl_create(ptFolder[iDx], &tFolder));

                    /* Relation creation between Main Project Revision and first level Folder is deliberately done before creation of nested folders. This is done to trigger attribute propagation 
                       from Main Project Revision to Folder on relation creation.
                    */
                    ETO4_ITKCALL(iRetCode, ETO4_grm_create_relation(tItemRev, tFolder, ppzsRelTypeList[iDx], &tNewRel));

                    if(iRetCode == ITK_ok)
                    {
                        mFLRecord.insert(PAIRFLINFO(ptFolder[iDx], tFolder));
                    }

                    ETO4_ITKCALL(iRetCode, ETO4_create_folder_structure(ptFolder[iDx], tFolder, mFLRecord));
                }
                else
                {
                    tFolder = iterFLInfo->second;
                    /* Relation creation between Main Project Revision and first level Folder is deliberately done before creation of nested folders. This is done to trigger attribute propagation 
                       from Main Project Revision to Folder on relation creation.
                    */
                    ETO4_ITKCALL(iRetCode, ETO4_grm_create_relation(tItemRev, tFolder, ppzsRelTypeList[iDx], &tNewRel));
                }
            }
        }

        ETO4_MEM_TCFREE(ptFolder);
        ETO4_MEM_TCFREE(ppzsRelTypeList);
    }

    return iRetCode;
}

/**
 * Function will query the database using value of attribute "eto4_project_template" present on input Project Revision "tItemRev" to retrieve approved "Template Base Revision" or its sub type. 
 * If multiple valid Template Revisions are present in the database then function will return only Template Revision.
 *
 * @param[in]  tItemRev      Main Project Revision tag     
 * @param[out] ptTemplateRev Valid Template Revision which might have been selected by user during creation of input Main Project Revision "tItemRev".
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_find_project_template
(
    tag_t  tItemRev,
    tag_t* ptTemplateRev
)
{
    int    iRetCode        = ITK_ok;
    int    iObjCnt         = 0;
    char*  pszTemplateName = NULL;
    tag_t* ptTemplateObj   = NULL;
    
    ETO4_ITKCALL(iRetCode, AOM_ask_value_string(tItemRev, ETO4_ATTR_PROJ_TEMPLATE, &pszTemplateName));

    if(tc_strlen(pszTemplateName) > 0)
    {
		ETO4_ITKCALL(iRetCode, ETO4_qry_template_ir_using_name_and_status(pszTemplateName, ETO4_STATUS_APPROVED, &iObjCnt, &ptTemplateObj));
    }

    if(iObjCnt > 0)
    {  
        (*ptTemplateRev) = (ptTemplateObj[iObjCnt-1]);
    }

    ETO4_MEM_TCFREE(pszTemplateName);
    ETO4_MEM_TCFREE(ptTemplateObj);
    return iRetCode;
}

/**
 * This function will create new Folder of same type as input source folder "tSrcFL" type. During creation function will use value for attributes "object_name" and "object_desc" which is same as that
 * of input source folder "tSrcFL".
 *
 * @param[in]  tSrcFL  Tag of Source Folder which will be used as reference during creation of new Folder     
 * @param[out] ptNewFL Tag of new custom folder
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_process_fl_create
(
    tag_t  tSrcFL,
    tag_t* ptNewFL
)
{
    int   iRetCode    = ITK_ok;
    char* pszTypeName = NULL;
    char* pszName     = NULL;
    char* pszDesc     = NULL;

	ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(tSrcFL, &pszTypeName));

    ETO4_ITKCALL(iRetCode, AOM_ask_value_string(tSrcFL, ETO4_ATTR_OBJECT_NAME_ATTRIBUTE, &pszName));

    if(tc_strlen(pszName) == 0 && iRetCode == ITK_ok)
    {
        ETO4_STRCPY(pszName, pszTypeName);
    }

    ETO4_ITKCALL(iRetCode, AOM_ask_value_string(tSrcFL, ETO4_ATTR_DESCRIPTION_ATTRIBUTE, &pszDesc));

    if(tc_strlen(pszDesc) == 0 && iRetCode == ITK_ok)
    {
        ETO4_STRCPY(pszDesc, pszTypeName);
    }
               
    ETO4_ITKCALL(iRetCode, ETO4_create_custom_folder2(pszTypeName, pszName, pszDesc, ptNewFL));

    ETO4_MEM_TCFREE(pszTypeName);
    ETO4_MEM_TCFREE(pszName);
    ETO4_MEM_TCFREE(pszDesc);
    return iRetCode;
}


/**
 * This function is called recursively to traverse the content of input source folder "tSrcFL" based on which function will clone the sub folder hierarchy of input source folder "tSrcFL". Function
 * will create cloned folder and these new cloned Folders will be inserted to input Parent Folder "tParentFL" one by one using depth first search algorithm. Function will only clone the content of
 * source folder "tSrcFL" which belongs to "A4_Folder_Base" sub type. Function traverses the input source folder "tSrcFL" to its entire depth as part of cloning process i.e. any content of input source
 * folder "tSrcFL" which is of valid Folder type will be traversed again in recursive format for cloning purpose. Function keeps track of every Source Folder and its content of type Folder which is
 * to be cloned as part of "key" in an input map "refmFLRecord" and corresponding cloned new folder is stored as map "value" corresponding to the stored key. Function is maintaining map with source
 * folder tag and corresponding new cloned Folder tag so that whenever function encounters situation in which same Folder is added as sub folder content it will not create new cloned folder every
 * time instead function will add already created cloned Folder as reference. This function also handler cyclic Folder structure with the help of validation which is performed based on information
 * stored in the map "refmFLRecord".
 * 
 * @param[in]     tSrcFL       Source Folder whose content needs to be cloned
 * @param[in]     tParentFL    Parent Folder in which the cloned content will be inserted
 * @param[in/out] refmFLRecord map containing source Folder tags as "key" and cloned new Folder tag as "value" for the corresponding  key.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_create_folder_structure
(
    tag_t tSrcFL,
    tag_t tParentFL,
    map<tag_t, tag_t>& refmFLRecord
)
{
    int    iRetCode  = ITK_ok;
    int    iFLCnt    = 0;
    char*  pszFLType = NULL;
    tag_t* ptFolders = NULL;

    ETO4_STRCPY(pszFLType, ETO4_CLASS_FOLDER_BASE);

    ETO4_ITKCALL(iRetCode, ETO4_ask_folder_references(tSrcFL, 1, &pszFLType, &iFLCnt, &ptFolders));
    
    for(int iDx = 0; iDx < iFLCnt && iRetCode == ITK_ok; iDx++)
    {
        map<tag_t, tag_t>::iterator iterFLInfo = refmFLRecord.begin();

        iterFLInfo = refmFLRecord.find(ptFolders[iDx]);

        /* If Folder to be cloned does not exist then create new Folder with similar details */
        if(iterFLInfo == refmFLRecord.end())
        {
            tag_t tNewFL = NULLTAG;

            ETO4_ITKCALL(iRetCode, ETO4_process_fl_create(ptFolders[iDx], &tNewFL));

            if(iRetCode == ITK_ok)
            {
                refmFLRecord.insert(PAIRFLINFO(ptFolders[iDx], tNewFL));

                ETO4_ITKCALL(iRetCode, ETO4_insert_content_to_folder(tParentFL, tNewFL, 999));

                ETO4_ITKCALL(iRetCode, ETO4_create_folder_structure(ptFolders[iDx], tNewFL, refmFLRecord));
            }
        }
        else
        {
            /* This section is also responsible for breaking the recursive loop which is initiated for creation of nested Folder structure.
               If Folder to be cloned is already created then this new Folder will be simply inserted in the parent cloned folder.
            */
            ETO4_ITKCALL(iRetCode, ETO4_insert_content_to_folder(tParentFL, (iterFLInfo->second), 999));

            break;
        }
    }

    ETO4_MEM_TCFREE(pszFLType);
    ETO4_MEM_TCFREE(ptFolders);
    return iRetCode;
}

