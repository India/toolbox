/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_string_utilities.cxx

    Description: This File contains functions for manipulating string. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>


/**
 * This function returns the substring when provided with start and end position on the the input string. Substring is the
 * character sequence that starts at character position 'iStrStartPosition' and ends at character position 'iStrEndPosition'
 *
 * For example If 'pszInputString' = 'Application'
 *                'iStrStartPosition' = 4
 *                'iStrEndPosition'   = 9
 *             then Output Will be: 'licati'
 *
 * Function will copy the characters starting 4th character to the 9th character of input string.
 * 
 * @param[in]  pszInputString    Actual input string
 * @param[in]  iStrStartPosition Position of a character in the current string to be used as starting character for the substring.
 * @param[in]  iStrEndPosition   Position of a character in the current string to be used as last character for the substring.
 * @param[out] ppszCopyedStr     Copied string.
 *
 * @retval INVALID_INPUT_TO_FUNCTION if input not correct
 * @retval ITK_ok on successful execution
 */
int ETO4_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
)
{
    int  iRetCode    = ITK_ok;
    int  iStrLen     = 0;

    /* Initializing out parameter of this function. */
    (*ppszCopyedStr) = NULL;

    iStrLen = (int) tc_strlen(pszInputString);
    
    if ((pszInputString != NULL) && (iStrStartPosition >= 0) && (iStrEndPosition <=  iStrLen) && 
        (iStrStartPosition <= iStrEndPosition))
    {
        if(tc_strcmp(pszInputString, "") != 0)
        {
            int iDx        = 0;
            int iNewStrLen = 0;

            iNewStrLen = iStrEndPosition - iStrStartPosition + 2;

            (*ppszCopyedStr) = (char*) MEM_alloc(sizeof(char*) * iNewStrLen);

            while(iStrStartPosition <= iStrEndPosition)
            {
                (*ppszCopyedStr)[iDx] = pszInputString[iStrStartPosition-1];
                iDx++;
                iStrStartPosition++;
            }

            (*ppszCopyedStr)[iDx] = '\0';
        }
    }
    else
    {
        iRetCode = INVALID_INPUT_TO_FUNCTION;
        EMH_store_error(EMH_severity_error, iRetCode);
    }

    return iRetCode;
}


/**
 * Function checks if given string exists in string of array or not 
 *
 * @param[in]  pszStrVal     String to be looked into string array
 * @param[in]  iTotalStrVals String Array Size 
 * @param[in]  pppszStrVals  String Array
 * @param[out] pbFound       'true' if input string 'pszStrVal' exist in the string array
 *
 * @return ITK_ok if string is found in the array else return ITK_ok
 */
int ETO4_contains_string_value
(
    char*    pszStrVal, 
    int      iTotalStrVals, 
    char**   pppszStrVals,
    logical* pbFound
)
{
    int iRetCode = ITK_ok;

    /* Initializing out parameter of this function. */
    (*pbFound) = false;

    for(int iNx = 0; iNx < iTotalStrVals; iNx++)
    {
        if(tc_strcmp( pszStrVal, pppszStrVals[iNx]) == 0)
        {
            (*pbFound) = true;
        }
    }
    
    return iRetCode;
}

/**
 * Parses a string containing values seperated by a given separator into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in]  pszList        Input string to be parsed
 * @param[in]  pszSeparator   Separator 
 * @param[out] iCount         Number of values in the returned string list
 * @param[out] pppszValuelist Returned list of values
 *
 * @note The returned array should be freed by ETO4_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 */
int ETO4_str_parse_string
(
    const char* pszList,
    char*       pszSeparator,
    int*        iCount,
    char***     pppszValuelist
)
{
    int   iRetCode     = ITK_ok;
    int   iCounter     = 0;
    char* pszToken     = NULL;
    char* pszTempList  = NULL;

     /* Validate input */
    if(pszList == NULL || pszSeparator == NULL)
    {
        TC_write_syslog( "ERROR: Missing input data: List passed or seprator is null\n");
    }
    else
    {
        /* Copying the input string to NULL terminated string */
        ETO4_STRCPY(pszTempList, pszList);

        /* Get the first token */
        pszToken = tc_strtok(pszTempList, pszSeparator);
        
        /* Tokenize the input string */
        while(pszToken != NULL)
        {
            ETO4_UPDATE_STRING_ARRAY((*iCount), (*pppszValuelist), pszToken);

            /* Get next token: */
            pszToken = tc_strtok(NULL, pszSeparator);
        }
    }

    /* Free the temporary memory used for tokenizing */
    ETO4_MEM_TCFREE(pszTempList);
    return iRetCode;
}


/**
 * Function split the input string based on very first occurrence of input Delimiter character. This function will return the remaining string after the Delimiter. 
 * For e.g. 
 * If input string is "ENT_01-Test_Data-1" and then the output string will be "Test_Data-1"
 *
 * @param[in]  pszInputStr    String to be checked for Delimiter occurrence
 * @param[in]  szDelim        Delimiter
 * @param[out] ppszTrimmedStr Trimmed String
 *
 * @return 
 */
int ETO4_get_str_after_delimiter
(
    char*      pszInputStr,
    const char szDelim, 
    char**     ppszTrimmedStr
)
{
    int iRetCode  = ITK_ok;

    pszInputStr = STRNG_find_first_char((const char*)pszInputStr, szDelim);
   
    if(pszInputStr != NULL)
    {   
        /* We are deliberating increasing the "pszInputStr" to 1 in the command below because we want to skip the delimiter and want to copy rest of the string. */
        ETO4_STRCPY((*ppszTrimmedStr), (pszInputStr+1));
    }

    return iRetCode;
}

/**
 * This function checks if input string contains sub-string which is provided as second input argument.
 *
 * @param[in]  pszInputString  Actual input string
 * @param[in]  pszSubString    Sub String to find
 * @param[out] pbSubStrFound   'true' if input string contains Sub String else 'false'
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_string_find_sub_str
(
    char*    pszInputString,
    char*    pszSubString,
    logical* pbSubStrFound
)
{
    int   iRetCode    = ITK_ok;
    char* pszPosition = NULL;  /* Never free this variable because u will actually be trying to free input variable */
    
    pszPosition = tc_strstr(pszInputString, pszSubString);

    if(pszPosition == NULL) 
    {
        (*pbSubStrFound) = false;
    }       
    else
    {
        (*pbSubStrFound) = true;        
    }

    return iRetCode;
}
