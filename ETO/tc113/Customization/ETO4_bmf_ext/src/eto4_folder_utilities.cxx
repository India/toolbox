/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_folder_utilities.hxx

    Description: This File contains custom Folder Manager (FL) functions to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>


/**
 * Function gets user Newstuff or Home folder depending on the input argument to the function
 *
 * @param[in]  pszUserFLType    Name of the folder. "newstuff" or "home"
 * @param[out] ptFolder         Tag to the Folder of input type
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_get_user_folder
(
    char*    pszUserFLType,
    tag_t*   ptFolder
)
{
    int    iRetCode              = ITK_ok;
    char*  pszCurrentUserName    = NULL;
    tag_t  tCurrentUser          = NULLTAG;
    tag_t  tUsrFolder            = NULLTAG;

    ETO4_ITKCALL(iRetCode, POM_get_user(&pszCurrentUserName, &tCurrentUser));

    if (iRetCode == ITK_ok && tCurrentUser != NULLTAG)
    {
        if ( tc_strcasecmp(pszUserFLType, ETO4_NEWSTUFF_FOLDER) == 0 )
        {
           if (iRetCode == ITK_ok)
           {          
                ETO4_ITKCALL(iRetCode, SA_ask_user_newstuff_folder(tCurrentUser, &tUsrFolder));
           }
        }          

        if ( tc_strcasecmp(pszUserFLType, ETO4_HOME_FOLDER) == 0 )
        {
               if (iRetCode == ITK_ok)
               {          
                    ETO4_ITKCALL(iRetCode, SA_ask_user_home_folder(tCurrentUser, &tUsrFolder));
               }
        }
    }

    if (iRetCode == ITK_ok)
    {
        *ptFolder = tUsrFolder;
    }

    ETO4_MEM_TCFREE(pszCurrentUserName);
    return iRetCode;
}

/**
 * Function creates custom folder and set attribute values as specified by input argument to this function.
 *
 * @param[in]  pszClassName  Class name of custom folder. e.g. ETO4_P01_Req_Phase
 * @param[in]  pszFolderName Value for attribute 'object_name'
 * @param[in]  pszFolderDesc Description to be set for new instance of folder
 * @param[in]  iAttrCount    Attribute Count to be set excluding Folder Name & Description.
 * @param[in]  ppszAttrNames Attribute Names
 * @param[in]  pszAttrValues Attribute Names
 * @param[out] ptFolder      Tag to instance of Folder.
 *
 * @note   Argument 'ppszAttrNames' should not contain attribute object_name and object_desc
 *         because these attribute are supplied separately to this function.
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_create_custom_folder
(
    char*    pszClassName,
    char*    pszFolderName,
    char*    pszFolderDesc,
    int      iAttrCount,
    char**   ppszAttrNames,
    char**   pszAttrValues,
    tag_t*   ptFolder
)
{
    int    iRetCode   = ITK_ok;
    tag_t  tClassId   = NULLTAG;

    /*  Get the tag for the class.  */
    ETO4_ITKCALL(iRetCode, POM_class_id_of_class( pszClassName, &tClassId ));

    /* Create an instance of the class. */
    ETO4_ITKCALL(iRetCode, POM_create_instance( tClassId, ptFolder));

    if(iAttrCount > 0)
    {
        ETO4_ITKCALL(iRetCode, ETO4_obj_pom_set_multiple_str_attr_without_saving( (*ptFolder), pszClassName, iAttrCount,
                                                                                  ppszAttrNames, pszAttrValues ) );
    }

    ETO4_ITKCALL(iRetCode, FL_initialize( (*ptFolder), pszFolderName, pszFolderDesc));

    ETO4_ITKCALL(iRetCode, POM_save_instances( 1, ptFolder, false));

    /*  Unlock the Folder as it is locked after creation */
    ETO4_ITKCALL(iRetCode, AOM_refresh( *ptFolder, false));
    return iRetCode;
}

/**
 * Function Inserts an object reference into a folder at a specified index position.
 *
 * @param[in]  tFolder   Tag to Folder
 * @param[in]  tObject   Object Tag which needs to be inserted in the Folder
 * @param[in]  iPosition It is zero based index, and that the input is larger than the number of objects in the folder,
 *                       or using 999 to indicate the last position.
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_insert_content_to_folder
(
    tag_t tFolder,
    tag_t tObject,
    int   iPosition
)
{
    int    iRetCode   = ITK_ok;

    ETO4_ITKCALL(iRetCode, AOM_refresh (tFolder, true));

    ETO4_ITKCALL(iRetCode, FL_insert(tFolder, tObject, iPosition));

    ETO4_ITKCALL(iRetCode, AOM_save( tFolder ));

    ETO4_ITKCALL(iRetCode, AOM_refresh( tFolder, false ));

    return iRetCode;
}

/**
 * Function creates custom folder and set "Name" and "Description" attribute  with values as specified by input argument to this function.
 *
 * @param[in]  pszFLType Class name of custom folder. e.g. A4_Project_Folder
 * @param[in]  pszFLName Value for attribute 'object_name'
 * @param[in]  pszFLDesc Description to be set for new instance of folder
 * @param[out] ptFolder  Tag to instance of Folder.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_create_custom_folder2
(
    char*  pszFLType,
    char*  pszFLName,
    char*  pszFLDesc,
    tag_t* ptFolder
)
{
    int   iRetCode       = ITK_ok;
    tag_t tFLType        = NULLTAG;
    tag_t tFLCreateInput = NULLTAG;

    ETO4_ITKCALL(iRetCode, TCTYPE_find_type(pszFLType, NULL, &tFLType));

    ETO4_ITKCALL(iRetCode, TCTYPE_construct_create_input(tFLType, &tFLCreateInput));

    ETO4_ITKCALL(iRetCode, AOM_set_value_string(tFLCreateInput, ETO4_ATTR_OBJECT_NAME_ATTRIBUTE, pszFLName));

    if(tc_strlen(pszFLDesc) > 0 && iRetCode == ITK_ok)
    {
        ETO4_ITKCALL(iRetCode, AOM_set_value_string(tFLCreateInput, ETO4_ATTR_DESCRIPTION_ATTRIBUTE, pszFLDesc));
    }

    ETO4_ITKCALL(iRetCode, TCTYPE_create_object(tFLCreateInput, ptFolder));

    ETO4_ITKCALL(iRetCode, AOM_save_with_extensions(*ptFolder));

    return iRetCode;
}

/**
 * Function will filter out the references of input Folder "tFolder" based on Object type list which is passed as input argument "ppszContTypes" to the function. If Object type list "ppszContTypes"
 * is empty then function will return all the content of input Folder "tFolder".
 *
 * @param[in]  tFolder   Tag to Folder
 * @param[in]  iTypCnt   Valid type count
 * @param[in]  ppszTypes Valid Type List
 * @param[out] piCnt     Valid Object Count
 * @param[out] pptObj    Valid Object List retrieved from Folder content
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_ask_folder_references
(
    tag_t   tFolder,
    int     iTypCnt,
    char**  ppszTypes,
    int*    piCnt,
    tag_t** pptObj
)
{
    int    iRetCode     = ITK_ok;
    int    iRefCnt      = 0;
    tag_t* ptReferences = NULL;

    if(iTypCnt > 0)
    {
        ETO4_ITKCALL(iRetCode, FL_ask_references(tFolder, FL_fsc_as_ordered, &iRefCnt, &ptReferences));
    }
    else
    {
        ETO4_ITKCALL(iRetCode, FL_ask_references(tFolder, FL_fsc_as_ordered, piCnt, pptObj));
    }

    if(iTypCnt > 0)
    {
        for(int iDx = 0; iDx < iRefCnt && iRetCode == ITK_ok; iDx++)
        {
            logical bIsValid = false;

            for(int iJx = 0; iJx < iTypCnt && iRetCode == ITK_ok; iJx++)
            {
                ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(ptReferences[iDx], ppszTypes[iJx], &bIsValid));

                if(bIsValid == true && iRetCode == ITK_ok)
                {
                    break;
                }
            }

            if(bIsValid == true && iRetCode == ITK_ok)
            {
                ETO4_UPDATE_TAG_ARRAY((*piCnt), (*pptObj), ptReferences[iDx]);
            }
        }
    }

    ETO4_MEM_TCFREE(ptReferences);
    return iRetCode;
}






