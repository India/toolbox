/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_miscellaneous_ext_function.cxx

    Description: This File contains custom functions that are used by BMF extension functions defined in file 'a4_bmf_extension.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>
#include <eto4_miscellaneous_ext_function.hxx>




/**
 * Function constructs the name of the preference using type of input "tPrimaryObj" and post-fixing with input string "pszPrefPostFix" and will retrieve the value of this site preference.
 * If preference constructed with exact type of input "tPrimaryObj" object does not exist then function will construct new preference using immediate parent type of "tPrimaryObj" object 
 * and try to retrieve the value of this preference from the system.
 *
 * @param[in]  tPrimaryObj    Tag of Object which will be used for constructing preference name.
 * @param[in]  pszPrefPostFix String which will be add as post-fixing for constructing preference name.
 * @param[out] piPrefValCnt   Count of preference value.
 * @param[out] pppszPrefVals  Preference values.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_get_propagation_pref_info
(
    tag_t   tPrimaryObj,
    char*   pszPrefPostFix,
    int*    piPrefValCnt, 
    char*** pppszPrefVals
)
{
    int   iRetCode    = ITK_ok;
    char* pszObjType  = NULL;
    char* pszPrefName = NULL;

    /* Initializing out parameter of this function. */
    (*piPrefValCnt)  = 0;
    (*pppszPrefVals) = NULL;

    ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(tPrimaryObj, &pszObjType));

    if(iRetCode == ITK_ok)
    {
        ETO4_STRCPY(pszPrefName, pszObjType);
        ETO4_STRCAT(pszPrefName, pszPrefPostFix);
    }

    ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values(pszPrefName, piPrefValCnt, pppszPrefVals));

    if((*piPrefValCnt) == 0)
    {
        /* If preference constructed with exact type of primary object does not exist then function will construct new preference
           using immediate parent type of primary object and try to retrieve the value from the system.
        */
        char* pszParentType = NULL;

        /* Free the variable containing preference name which was constructed using type name of primary object. Primary object is supplied as input argument to this function. */
        ETO4_MEM_TCFREE(pszPrefName);

        ETO4_ITKCALL(iRetCode, ETO4_obj_ask_parent_type(tPrimaryObj, &pszParentType));

        if(iRetCode == ITK_ok)
        {
            ETO4_STRCPY(pszPrefName, pszParentType);
            ETO4_STRCAT(pszPrefName, pszPrefPostFix);
        }

        ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values(pszPrefName, piPrefValCnt, pppszPrefVals));

        ETO4_MEM_TCFREE(pszParentType);
    }

    ETO4_MEM_TCFREE(pszObjType);
    ETO4_MEM_TCFREE(pszPrefName);
    return iRetCode;
}

/**
 * This function will retrieve the related destination object from source object. This function will decide if the source object "tSrcObj" is primary or secondary object based on relation expansion information which
 * is taken as input argument "strExpDir". Relation expansion can have value "<" or ">". 
 *
 * ">" Implies: L2R relations: "L2R" means "Left to Right" expansion, and describes an expansion starting from primary object to secondary object. The symbol ">" describes the direction from
 *              left to right. Expansion form L2R can use any GRM Relation.
 * "<" Implies: R2L relations: "R2L" means "Right to Left" expansion, and describes an expansion starting from secondary object to primary object. The symbol "<" describes the direction from
 *              right to left. Expansion form R2L can use any GRM Relation.
 *
 * If value of "strExpDir" is ">" then "tSrcObj" is primary object we need to find secondary objects which are connected to primary object with relation "strRelType".
 * If value of "strExpDir" is "<" then "tSrcObj" is secondary object we need to find primary objects which are connected to secondary object with relation "strRelType".
 *
 * @param[in]  tSrcObj    Tag of source object.
 * @param[in]  strDesType Destination object type.
 * @param[in]  strRelType Relation type.
 * @param[in]  strExpDir  Relation expansion direction.
 * @param[out] piObjCnt   Count of related objects.
 * @param[out] pptObjects List of related object to source.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_get_related_obj_using_obj_expan
(
    tag_t   tSrcObj,
    string  strDesType,
    string  strRelType,
    string  strExpDir,
    int*    piObjCnt,
    tag_t** pptObjects  
)
{
    int   iRetCode   = ITK_ok;
    char* pszDesType = NULL;

    if(tc_strcmp(strDesType.c_str(), ETO4_NO_VAL) != 0)
    {
        ETO4_STRCPY(pszDesType, strDesType.c_str());
    }

    if(tc_strcmp(strExpDir.c_str() ,ETO4_STRING_GREATER_THAN) == 0)
    {
        ETO4_ITKCALL(iRetCode, ETO4_grm_get_related_obj(tSrcObj, (char*)(strRelType.c_str()), pszDesType, NULL, false, pptObjects, piObjCnt));
    }
    else
    {
        ETO4_ITKCALL(iRetCode, ETO4_grm_get_related_obj(tSrcObj, (char*)(strRelType.c_str()), pszDesType, NULL, true, pptObjects, piObjCnt));
    }

    ETO4_MEM_TCFREE(pszDesType);
    return iRetCode;
}

/**
 * This function validates if input object "tDestObject" is of type "strDestObjType". If value of input argument "strDestObjType" is "NO_VAL" then the function will not compare the type
 * and will return argument "pbIsValidType" as "true"
 *
 * @param[in]  tObject       Tag of Object.
 * @param[in]  strObjType    Object type name.
 * @param[out] pbIsValidType "true" if input object "tObject" is exact type or sub type of object type name "strObjType".
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_validate_destination_obj
(
    tag_t    tObject,
    string   strObjType,
    logical* pbIsValidType
)
{
    int iRetCode = ITK_ok;

    if(tObject != NULLTAG)
    {
        if(tc_strcmp(strObjType.c_str(), ETO4_NO_VAL) == 0)
        {
            (*pbIsValidType) = true;
        }
        else
        {
            ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(tObject, (char*)(strObjType.c_str()), pbIsValidType));
        }
    }
    else
    {
        (*pbIsValidType) = true;
    }

    return iRetCode;
}





