/*======================================================================================================================================================================
                                                      Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_organization_utiities.cxx

    Description: This File contains custom functions for getting information about organization and system administration to perform generic operations.

========================================================================================================================================================================

Date          Name						 Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Feb-18    Akshay Kumar Singh          Initial Release

========================================================================================================================================================================*/
#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>

/**
 * This function gets the current logged in user tag and User ID
 *
 * @param[out] ptLoggedInUser Tag of current logged in user 
 * @param[out] ppszUserID     User ID for a user.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_get_logged_in_user
(
    tag_t* ptLoggedInUser,
    char** ppszUserID
)   
{
    int   iRetCode   = ITK_ok;
    tag_t tCurrentGM = NULLTAG;
    char* pszUserID  = NULL;

    ETO4_ITKCALL(iRetCode, SA_ask_current_groupmember(&tCurrentGM));

    ETO4_ITKCALL(iRetCode, SA_ask_groupmember_user(tCurrentGM, ptLoggedInUser));

    ETO4_ITKCALL(iRetCode, SA_ask_user_identifier2(*ptLoggedInUser, &pszUserID));

    if(iRetCode == ITK_ok)
    {
        ETO4_STRCPY(*ppszUserID, pszUserID);
    }

    ETO4_MEM_TCFREE(pszUserID);
    return iRetCode;
} 





