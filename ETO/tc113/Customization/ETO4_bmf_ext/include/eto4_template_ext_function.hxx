/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_template_ext_function.hxx

    Description:  Header File for 'eto4_template_ext_function.cxx'

========================================================================================================================================================================

Date          Name						Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_TEMPLATE_EXT_FUNCTION_HXX
#define ETO4_TEMPLATE_EXT_FUNCTION_HXX


typedef std::pair<tag_t, tag_t> PAIRFLINFO;

int ETO4_copy_template_content
(
    tag_t Item,
    tag_t tItemRev
);

int ETO4_find_project_template
(
    tag_t  tItemRev,
    tag_t* ptTemplateRev
);

int ETO4_process_fl_create
(
    tag_t  tSrcFL,
    tag_t* ptNewFL
);

int ETO4_create_folder_structure
(
    tag_t tSrcFL,
    tag_t tParentFL,
    map<tag_t, tag_t>& refmFLRecord
);

#endif


