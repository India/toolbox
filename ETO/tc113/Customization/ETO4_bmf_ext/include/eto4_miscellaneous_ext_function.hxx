/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_miscellaneous_ext_function.hxx

    Description: Header File for 'eto4_miscellaneous_ext_function.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_MISCELLANEOUS_EXT_FUNCTION_HXX
#define ETO4_MISCELLANEOUS_EXT_FUNCTION_HXX

int ETO4_get_propagation_pref_info
(
    tag_t   tPrimaryObj,
    char*   pszPrefPostFix,
    int*    piPrefValCnt, 
    char*** pppszPrefVals
);

int ETO4_get_related_obj_using_obj_expan
(
    tag_t   tSrcObj,
    string  strDesType,
    string  strRelType,
    string  strExpDir,
    int*    piObjCnt,
    tag_t** pptObjects  
);

int ETO4_validate_destination_obj
(
    tag_t    tObject,
    string   strObjType,
    logical* pbIsValidType
);


#endif





