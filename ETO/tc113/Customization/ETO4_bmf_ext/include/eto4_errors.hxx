/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_errors.hxx

    Description:  Header File for containing macros for custom errors

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_ERRORS_HXX
#define ETO4_ERRORS_HXX

#include <common/emh_const.h>


#define ERROR_ALLOCATING_MEMORY                                     (EMH_USER_error_base + 401)
#define ERROR_CODE_NOT_STORED_ON_STACK                              (EMH_USER_error_base + 402)
#define INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME                   (EMH_USER_error_base + 403)
#define INVALID_INPUT_TO_FUNCTION                                   (EMH_USER_error_base + 404)
#define INCORRECT_PREFERENCE_VALUE                                  (EMH_USER_error_base + 405)
#define INVALID_OBJ_TYPE_PASTED_TO_FOLDER                           (EMH_USER_error_base + 406)
#define ITEM_ALREADY_EXIST_WITH_GIVEN_ITEM_ID                       (EMH_USER_error_base + 407)
#define ATTR_INFO_NOT_DEFINED_PROPERLY_USING_OBJ_EXPANSION_NOTATION (EMH_USER_error_base + 408)
#define OBJ_EXPANSION_INFO_NOT_DEFINED_PROPERLY						(EMH_USER_error_base + 409)
#define REL_CREAT_PROHIBITED_PROP_ALREADY_SET                       (EMH_USER_error_base + 410)
#define OBJECT_DOES_NOT_HAVE_WRITE_PRIVILEGE                        (EMH_USER_error_base + 411)
#define OBJECT_IS_CHECKED_OUT                                       (EMH_USER_error_base + 412)

#endif


