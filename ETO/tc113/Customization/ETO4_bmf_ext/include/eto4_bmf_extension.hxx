/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_bmf_extension.hxx

    Description:  Header File for eto4_bmf_extension.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_BMF_EXTENSION_HXX
#define ETO4_BMF_EXTENSION_HXX

#include <tccore/tc_msg.h>
#include <itk/bmf.h>
#include <dispatcher/dispatcher_itk.h>
#include <bmf/libuserext_exports.h>

extern USER_EXT_DLL_API int ETO4_create_etoproject_proj_cre_post_action 
(
    METHOD_message_t* pmMessage,
    va_list   args
);

extern USER_EXT_DLL_API int ETO4_create_phase_folders_rev_cre_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);

extern USER_EXT_DLL_API int ETO4_create_phase_folders_iman_save_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);

extern USER_EXT_DLL_API int ETO4_create_phase_folders_item_cre_from_rev_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);

extern USER_EXT_DLL_API int ETO4_set_contents_prop_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);

extern USER_EXT_DLL_API int ETO4_paste_to_folder_pre_condition
(
    METHOD_message_t* pmMessage,
    va_list args
);

extern USER_EXT_DLL_API int ETO4_create_project_pre_condition
(
    METHOD_message_t *pmMessage,
    va_list args
);

extern USER_EXT_DLL_API int ETO4_copy_template_content_item_cre_rev_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);

extern USER_EXT_DLL_API int ETO4_set_object_name_prop_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);

extern USER_EXT_DLL_API int ETO4_set_project_grm_create_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);

extern USER_EXT_DLL_API int ETO4_set_project_contents_prop_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);

extern USER_EXT_DLL_API int ETO4_set_property_grm_create_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);


extern USER_EXT_DLL_API int ETO4_set_property_contents_prop_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
);


#include <bmf/libbmf_undef.h>

#endif





