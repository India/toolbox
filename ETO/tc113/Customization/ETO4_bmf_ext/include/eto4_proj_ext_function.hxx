/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_proj_ext_function.hxx

    Description:  Header File for 'eto4_proj_ext_function.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_PROJ_EXT_FUNCTION_HXX
#define ETO4_PROJ_EXT_FUNCTION_HXX

int ETO4_propagate_project_info
(
    tag_t  tSrcObject,
    char*  pszAttrName,
    int    iProjCount,
    tag_t* ptProject,
    tag_t  tDestObject,
    char*  pszRelType
);

int ETO4_verify_project_attr
(
    string srtSrcAttr,
    string strDestAttr
);

int ETO4_process_project_propagation
(
    tag_t  tSrcObject,
    tag_t  tDestObject,
    string strDestObjType,
    string srtSrcAttr,
    string strRelType,
    string strExpDir,
    string strDestAttr,
    char*  pszAttrName,
    int    iProjCount,
    tag_t* ptProject
);

int ETO4_assign_project_to_related_obj
(
    tag_t  tSrcObject,
    tag_t  tDestObject,
    string strDestObjType,
    string strRelType,
    string strExpDir,
    string strDestAttr,
    int    iProjCount,
    tag_t* ptProject
);

int ETO4_assign_project
(
    int    iObjCnt,
    tag_t* ptObjects,
    int    iProjCnt,
    tag_t* ptProjects
);

int ETO4_get_list_of_unassigned_project
(
    tag_t   tObject,
    int     iProjCnt,
    tag_t*  ptProjects,
    int*    piUnassignedProjCnt,
    tag_t** pptUnassignedProjects
);

int ETO4_construct_proj_asgmt_list
(
    int     iObjCnt,
    tag_t*  ptObject,
    tag_t** pptNewList
);

#endif

