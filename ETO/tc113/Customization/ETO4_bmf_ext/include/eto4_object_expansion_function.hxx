/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_object_expansion_function.hxx

    Description:  Header File for 'eto4_object_expansion_function.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_OBJECT_EXPANSION_FUNCTION_HXX
#define ETO4_OBJECT_EXPANSION_FUNCTION_HXX

struct sPrefLineInfo
{
    vector<string> vExpansionInfo;
    vector< vector<string> > vAttrNameVal;
};

int ETO4_collect_obj_expansion_info
(
    int    iObjDefCnt,  
    char** ppszObjDefinition,
    map<string, sPrefLineInfo>* pmObjExpanPrefInfo
);

int ETO4_add_obj_expansion_info_to_map
(
    char*  pszObjDefinition,
    char*  pszObjExpanSymbol,
    int    iValCnt,
    char** ppszValInfo,
    map<string, sPrefLineInfo>* pmObjExpanPrefInfo
);

int ETO4_get_obj_expansion_info
(
    vector<string> vAttrNameValInfo,
    string* pstrDestObjType,
    string* psrtSrcAttr,
    string* pstrDestAttr,
    string* pstrValue
);

int ETO4_get_actual_attr_name_and_obj
(
    tag_t   tObject,
    string  strAttr,
    int*    piActualObjCnt,
    tag_t** pptActualObj,
    string* pstrActualAttr
);

int ETO4_get_obj_using_ref_property
(
    int      iObjCnt,
    tag_t*   ptObject,
    char*    pszAttrName,
    int*     piNewObjCnt,
    tag_t**  pptNewObj,
    logical* pbIsRefAttr
);

#endif


