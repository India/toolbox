/*======================================================================================================================================================================
                                                    Copyright 2018  LMtec GmbH
                                                    Unpublished - All rights reserved
========================================================================================================================================================================

    File Description:

    Filename: eto4_property_ext_function.hxx

    Description:  Header File for 'eto4_property_ext_function.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7-Feb-18     Akshay Kr Singh            Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_PROPERTY_EXT_FUNCTION_HXX
#define ETO4_PROPERTY_EXT_FUNCTION_HXX

int ETO4_check_src_obj_for_rev_processing 
(
    tag_t   tSrcObject,
    int*    piRevCnt,
    tag_t** pptItemRev
);

int ETO4_propagate_prop_val
(
    tag_t   tSrcObject,
    char*   pszAttrName,
    char*   pszAttrVal,
    tag_t   tDestObject,
    char*   pszRelType,
    logical bOnlyValidate
);

int ETO4_get_property_propagation_pref_info
(
    tag_t   tPrimaryObj,
    char*   pszPrefPostFix,
    logical bOnlyValidate,
    int*    piPrefValCnt, 
    char*** pppszPrefVals    
);

int ETO4_get_obj_exp_direction
(
    char*  ppszInfo,
    char** ppszObjExpDir
);

int ETO4_process_property_propagation
(
    tag_t   tSrcObject,
    tag_t   tDestObject,
    string  strDestObjType,
    string  srtSrcAttr,
    string  strRelType,
    string  strExpDir,
    string  strDestAttr,
    string  strValue,
    char*   pszAttrName,
    char*   pszAttrValFromExt,
    logical bOnlyValidate
);

int ETO4_set_prop_on_related_obj
(
    tag_t  tSrcObject,
    tag_t  tDestObject,
    string strDestObjType,
    string strRelType,
    string strExpDir,
    string strDestAttr,
    char*  pszAttrValFromExt
);

int ETO4_validate_prop_propagation_on_related_obj
(
    tag_t  tSrcObject,
    tag_t  tDestObject,
    string strDestObjType,
    string strRelType,
    string strExpDir,
    string strDestAttr,
    char*  pszAttrValFromExt
);

#endif


