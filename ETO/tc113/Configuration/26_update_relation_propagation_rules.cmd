@echo off

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Update relation propagation rules and enable BOMView propagation
echo.
echo update_project_data -u=infodba -p=%DBAPASS_TMP% -g=dba -f=bomviewon
update_project_data -u=infodba -p=%DBAPASS% -g=dba -f=bomviewon

REM Update relation propagation rules and add/update further relation types, that are not set OOTB
echo.
echo update_project_data -u=infodba -p=%DBAPASS_TMP% -g=dba -f=add -t=IMAN_specification,IMAN_Rendering,IMAN_reference,TC_Attaches,IMAN_master_form
update_project_data -u=infodba -p=%DBAPASS% -g=dba -f=add -t=IMAN_specification,IMAN_Rendering,IMAN_reference,TC_Attaches,IMAN_master_form

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
