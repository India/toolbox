@echo off

set SUBFOLDER=%1
set QUERY=%2

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import Teamcenter Query

if exist "Queries\%SUBFOLDER%\%QUERY%.xml" (
    echo -----------------------------------------------------------------------
    echo plmxml_import -u=infodba -p=%DBAPASS_TMP% -g=dba -xml_file="Queries\%SUBFOLDER%\%QUERY%.xml" -import_mode=overwrite -log="..\logs\%COMPUTERNAME%\%QUERY%.log"
    echo.
    plmxml_import -u=infodba -p=%DBAPASS% -g=dba -xml_file="Queries\%SUBFOLDER%\%QUERY%.xml" -import_mode=overwrite -log="..\logs\%COMPUTERNAME%\%QUERY%.log"
    echo -----------------------------------------------------------------------
)

if not exist "Queries\%SUBFOLDER%\%QUERY%.xml" echo ERROR: File "Queries\%SUBFOLDER%\%QUERY%.xml" does not exist.
echo.

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
