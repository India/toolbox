@echo off

:: Call this script for full, client 2 tier or client 4 tier installation
:: Sample: "release_1.1.0.cmd VERSION 2_TIER_CLIENT" for 2 tier client
:: Sample: "release_1.1.0.cmd VERSION 4_TIER_CLIENT" for 4 tier client
:: Sample: "release_1.1.0.cmd VERSION" for full installation
setlocal
set SILENT_MODE=TRUE
set CURRENT_DIR=%~dp0
set MAX_FULL_STEP=3
set MAX_2T_STEP=2
set MAX_4T_STEP=1
if not defined TEAMCENTER_VERSION set TEAMCENTER_VERSION=V10000.1.0.21_20140818.00
set ETO_CURRENT_RELEASE=%1
set ETO_CURRENT_RELEASE_CANDIDATE=%ETO_CURRENT_RELEASE%-rc1
shift

if x%1==x-help goto HELP
if not x%1==x echo Current mode is "%1"
if x%1==x echo Current mode is "Full installation"

if x%SUPPRESS_PAUSE%==x (
    echo.
    echo ###########################################
    echo ##   PREREQUISITES
    echo ###########################################
    echo INFO: In case of using Windows 7 or Windows 2008 Server, make sure 
    echo       that you run this script with admin privileges!
    echo.
    color 2a
    pause
    color
)

echo.
echo Start process at %date% - %time%
echo.

cd /D %CURRENT_DIR%

:: IS_OBSOLETE is used to exclude some scripts, because they are obsolete and replaced by a later script
if x%2==xIS_OBSOLETE set IS_OBSOLETE=Y
if x%1==x4_TIER_CLIENT goto CLIENT_4TIER_INSTALLATION_PRE
if not defined TC_ROOT goto ENV_MISSING
if x%1==x2_TIER_CLIENT goto CLIENT_2TIER_INSTALLATION_PRE
if x%1==xSERVER_LIGHT goto SERVER_LIGHT_INSTALLATION_PRE

:ASK_CREDENTIALS
echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

REM Ask for the TC_ROOT if a different one wants to be used (e.g. 2-Tier / 4-Tier environment)
echo The following TC_ROOT will be used: %TC_ROOT%
echo Is this correct?
set /P TC_ROOT_C=(Y)es or (N)o:
if /I "%TC_ROOT_C%"=="Y" goto START_PROCESS
if /I "%TC_ROOT_C%"=="N" goto SET_TC_ROOT

:START_PROCESS
REM full installation (e.g. on server)

REM Import configuration package
echo.
echo ########################################
echo ## STEP 1 OF %MAX_FULL_STEP%  :::  Configuration    ##
echo ########################################
cd /D Configuration
:: ### Update / Import Preferences
if not defined IS_OBSOLETE call 00_update_version_preference.cmd
call 01_import_site_preferences.cmd
:: ### Copy lang files to textserver directory
call 22_copy_lang_files.cmd
:: ### Update project relation propagation rules
call 26_update_relation_propagation_rules.cmd
:: ### Import PROJECTS Queries
call 27_import_query.cmd PROJECTS ETO4_ProjectRevision
:: ### Install stylesheets
call 28_import_stylesheet.cmd GENERAL
:: ### Install workflows
call 29_import_workflow.cmd GENERAL SYSTEM_Assign-Project
cd /D %CURRENT_DIR%

REM Import tools package
echo.
echo ########################################
echo ## STEP 2 OF %MAX_FULL_STEP%  :::  Tools            ##
echo ########################################
cd /D Tools
:: ### Copy DLLs
call 01_copy_extensions.cmd
cd /D %CURRENT_DIR%

echo.
echo ########################################
echo ## STEP 3 OF %MAX_FULL_STEP%  :::  Regenerate Cache ##
echo ########################################
if defined IS_OBSOLETE goto FINISHED
echo Do you want to regenerate the server and client meta cache? All open Teamcenter sessions will be killed.
set /P CACHE_C=(Y)es or (N)o:
if /I "%CACHE_C%"=="Y" goto REGENERATE_CACHE
if /I "%CACHE_C%"=="N" goto FINISHED

:REGENERATE_CACHE
echo.
clearlocks -assert_all_dead -u=infodba -p=%DBAPASS% -g=dba
echo.
echo generate_metadata_cache -u=infodba -p=%DBAPASS_TMP% -g=dba -force
echo.
generate_metadata_cache -u=infodba -p=%DBAPASS% -g=dba -force
echo.
echo generate_client_meta_cache generate all -u=infodba -p=%DBAPASS_TMP% -g=dba
echo.
generate_client_meta_cache generate all -u=infodba -p=%DBAPASS% -g=dba
echo.

goto FINISHED

:CLIENT_2TIER_INSTALLATION
REM Import configuration package for 2-Tier server
echo.
echo ########################################
echo ## STEP 1 OF %MAX_2T_STEP%  :::  Configuration    ##
echo ########################################
cd /D Configuration
:: ### Copy lang files to textserver directory
call 22_copy_lang_files.cmd
cd /D %CURRENT_DIR%

REM Import tools package
echo.
echo ########################################
echo ## STEP 2 OF %MAX_2T_STEP%  :::  Tools            ##
echo ########################################
cd /D Tools
:: ### Copy DLLs
call 01_copy_extensions.cmd
cd /D %CURRENT_DIR%

echo.
goto FINISHED

:CLIENT_4TIER_INSTALLATION
REM Import Configuration package for client
echo.
echo ########################################
echo ## STEP 1 OF %MAX_4T_STEP%  :::  Configuration    ##
echo ########################################
cd /D Configuration
:: no client adjustment required
cd /D %CURRENT_DIR%

echo.
goto FINISHED

:SET_TC_ROOT
set /P TC_ROOT=Specify TC_ROOT: 
if not exist %TC_ROOT% (
    echo ERROR: The specified directory "%TC_ROOT%" does not exist!
    goto :EOF
)
echo.

goto START_PROCESS

:CLIENT_4TIER_INSTALLATION_PRE
if exist C:\plm\tc101_4t (
    set TC_ROOT=C:\plm\tc101_4t
    echo Set TC_ROOT to %TC_ROOT%
    echo.
    goto CLIENT_4TIER_INSTALLATION
)

if exist C:\plm\tc101 (
    set TC_ROOT=C:\plm\tc101
    echo Set TC_ROOT to %TC_ROOT%
    echo.
    goto CLIENT_4TIER_INSTALLATION
)

:: Default TC_ROOT does not exist
set /P TC_ROOT=Specify TC_ROOT: 

if not exist %TC_ROOT% (
    echo ERROR: The specified directory "%TC_ROOT%" does not exist!
    goto :EOF
)
echo.
goto CLIENT_4TIER_INSTALLATION

:CLIENT_2TIER_INSTALLATION_PRE
if exist C:\plm\tc101 (
    set TC_ROOT=C:\plm\tc101
    echo Set TC_ROOT to %TC_ROOT%
    echo.
    goto CLIENT_2TIER_INSTALLATION
)

:: Default TC_ROOT does not exist
set /P TC_ROOT=Specify TC_ROOT: 

if not exist %TC_ROOT% (
    echo ERROR: The specified directory "%TC_ROOT%" does not exist!
    goto :EOF
)
echo.
goto CLIENT_2TIER_INSTALLATION

:SERVER_LIGHT_INSTALLATION_PRE
if exist F:\plm\tc101 (
    set TC_ROOT=C:\plm\tc101
    echo Set TC_ROOT to %TC_ROOT%
    echo.
    goto CLIENT_2TIER_INSTALLATION
)

if exist C:\plm\tc101 (
    set TC_ROOT=C:\plm\tc101
    echo Set TC_ROOT to %TC_ROOT%
    echo.
    goto CLIENT_2TIER_INSTALLATION
)

:: Default TC_ROOT does not exist
set /P TC_ROOT=Specify TC_ROOT: 

if not exist %TC_ROOT% (
    echo ERROR: The specified directory "%TC_ROOT%" does not exist!
    goto :EOF
)
echo.
goto CLIENT_2TIER_INSTALLATION

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
echo    TC_ROOT = %TC_ROOT%
echo    TC_DATA = %TC_DATA%
endlocal
goto :EOF

:FINISHED
if not defined IS_OBSOLETE (
    REM Update version info file
    echo AutoUpd;%COMPUTERNAME%;%date%;%time%;ETOCust;%ETO_CURRENT_RELEASE_CANDIDATE%>%TC_ROOT%\eto_current_version.txt
    echo AutoUpd;%COMPUTERNAME%;%date%;%time%;ETOCust;%ETO_CURRENT_RELEASE_CANDIDATE%>>%TC_ROOT%\eto_history.txt
)

echo.
echo Process finished at %date% - %time%
echo.
endlocal
goto :EOF

:HELP
echo USAGE:
echo    install_release_%ETO_CURRENT_RELEASE% [INSTALLATION_MODE] [IS_OBSOLETE]
echo        If no option is used, the full installation will be performed
echo        Valid installation modes:
echo            FULL            Full installation mode (default)
echo            SERVER_LIGHT    Server installation mode without database modification
echo            2_TIER_CLIENT   Installation on a 2 Tier client
echo            4_TIER_CLIENT   Installation on a 4 Tier client
echo        IS_OBSOLETE         Use this flag if the next release script will run
echo                            as well. This option has to be the second argument  
endlocal
