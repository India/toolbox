@echo off
if not defined TC_ROOT goto ENV_MISSING

setlocal

echo Type in password of "infodba"
set /P DBAPASS=Password: 

echo Type target user name
set /P TARGET_USER=User: 

:START_PROCESS
echo.
echo INFO: Clearing user preferences of '%TARGET_USER%'...
preferences_manager -u=infodba -p=%DBAPASS% -g=dba -mode=clear -scope=USER -target=%TARGET_USER%

endlocal
:FINISHED
goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
