#!/usr/bin/perl

###############################################################################
# 
#   Cleanup_Workflow_Template.pl
#
#   This script removes all not needed content of exported workflow template xml file. 
#
#   Parameters:
#   -input_file     Input XML file
#   -input_list     Input TXT file, list that contains full path to template XML (one per line)
#
#   Example run:
#   > perl Cleanup_Workflow_Template.pl -input_file=<>
#
#=============================================================================#
# 1.0   03. November 2010    G. Langenbacher     initial version
###############################################################################

sub param($);
sub check_param($);

$|=1;

my $f_input         = param("-input_file");
my $f_input_list    = param("-input_list");

if (!-f "$f_input" && !-e "$f_input" && !-e "$f_input_list") 
{
    print ("\nERROR: Input file '$f_input' does not exist!\n");
    print ("\nUSAGE:\n   >perl Cleanup_Workflow_Template.pl -input_file=<fname>\n");
	print ("         >perl Cleanup_Workflow_Template.pl -input_list=<fname>\n");
    exit 0;
}

print "\nINFO: Start processing...";

if (-f "$f_input_list")
{
    open(LIST,"<$f_input_list") or die "\nERROR: Open input file '$f_input_list' failed";
    while (<LIST>)
    {    
        s/[\r\n]+//;   # better cho(m)p
        next if (/^\s*$/);
        next if (/^\#/);
        
        if (-f $_)
        {
            process_file($_);
        }
        else
        {
            print "WARNING: File '".$_."' does not exist!\n";
        }
    }
    close(LIST);
}
else
{
	process_file($f_input);
}

print "\nINFO: Process finished!\n";

############################
#  FUNC: process_file      #
# ------------------------ #
# Process XML file         #
############################
sub process_file($)
{
	my $fname = shift;
	my $site_tag_processing = 0;
	my $EPM_signoff_profile = 0;
	my $EPM_signoff_profile_cnt = 0;
	
	print "\nINFO: Processing file '$fname'";

	rename($fname, "$fname.bak") if (!-f "$fname.bak");
	open(INPUT,"<$fname.bak") or die "\nERROR: Open input file $fname.bak failed";
	open(OUTPUT,">$fname") or die "\nERROR: Open output file $fname failed";
	while (<INPUT>)
	{    
		s/[\r\n]+//;   # better cho(m)p
		next if (/^\s*$/);
		next if (/^\#/);
		
		my $string = $_;
		
		#<Site id="id4" name="PRION_DEV" siteId="-2121653886">
		#<ApplicationRef version="wxPxcluUYgqEoA" application="Teamcenter" label="wxPxcluUYgqEoA"></ApplicationRef>
		#<UserData id="id5">
		#<UserValue value="1" title="dbms"></UserValue></UserData></Site>
		if (($site_start) = ($_=~ m/<Site .*siteId=\"(.*)\">/i))
		{ 
			$site_tag_processing = 1;
		}

		if (!$site_tag_processing)
		{
			# schemaVersion="6" language="en-us" date="2011-10-08" time="22:58:25" author="Teamcenter V8000.3.0.41_20110707.00 - langenbacher@PRION_DEV(-2121653886)">
			$string =~ s/ time=".*" author=".*"//;
		
			$string =~ s/<ApplicationRef version=.*<\/ApplicationRef>//;
			
			#<AssociatedDataSet id="id84" dataSetRef="#id16" role="EPM_signoff_profile"></AssociatedDataSet>
			#<AssociatedDataSet id="id91" dataSetRef="#id16" role="EPM_signoff_profile"></AssociatedDataSet>
			if (($dataSetRef) = ($_=~ m/.*dataSetRef="(.*)" role="EPM_signoff_profile".*/i))
			{ 
				if ($EPM_signoff_profile eq $dataSetRef)
				{
					$EPM_signoff_profile_cnt++;
					print "\nWARNING: Duplicate EPM_signoff_profile found [$EPM_signoff_profile_cnt]. Removed!";
				}
				else
				{
					$EPM_signoff_profile = $dataSetRef;
					print OUTPUT "$string\n" if ($string ne "");
				}
			} 
			else
			{
				$EPM_signoff_profile = 0;
				print OUTPUT "$string\n" if ($string ne "");
			}
		}
		
		if (($site_end) = ($_=~ m/(.*)<\/Site>/i))
		{ 
			$site_tag_processing = 0;
		}    
	}
	close(INPUT);
	close(OUTPUT);
}


############################
#  FUNC: param             #
# ------------------------ #
# Get param by name        #
############################
sub param($)
{
	my $string = shift;
	my $ret = "<undefined>";
	foreach $parameter(@ARGV)
	{
		my ($para1, $para2) = split(/=+/, $parameter);
		if ( uc($string) eq uc($para1) )
		{
			$ret = $para2;
		}
	}
	return $ret;
}

######################################
# FUNCTION: CHECK OPTION             #
# INPUT: Argument option             #
# ---------------------------------- #
# RETURN: Boolean                    #
######################################
sub check_param($)
{
	my $string = shift;
	foreach $parameter(@ARGV)
	{
		my ($para1, $para2) = split(/=+/, $parameter);
		if ( uc($string) eq uc($para1) )
		{
			return 1;
		}
	}
	return 0;
}