/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_register_callbacks.hxx

    Description:  Header File for eto4_register_callbacks.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_REGISTER_CALLBACKS_HXX
#define ETO4_REGISTER_CALLBACKS_HXX
    
#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>


extern DLLAPI int ETO4_custom_register_callbacks(void);

extern DLLAPI int ETO4_register_custom_handlers
(
    int*    piDecision,
    va_list args
);


#endif

