/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_itk_mem_free.hxx

    Description:  This file contains macros for freeing portion of memory allocated with TCe 'MEM_' functions and by TCE itself.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release
    
========================================================================================================================================================================*/
#ifndef ETO4_ITK_MEM_FREE_HXX
#define ETO4_ITK_MEM_FREE_HXX

#include <eto4_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef ETO4_MEM_TCFREE
#undef ETO4_MEM_TCFREE
#endif

// Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc

#define ETO4_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }
// Macro to free memory allocated using alloc and malloc
#define ETO4_MEM_ARRAY_TCFREE(arraylen, stringarray){\
  for(int inx = 0; inx < arraylen && (stringarray != NULL); inx++){\
     MEM_free(stringarray[inx]);\
  }\
  MEM_free(stringarray);\
  stringarray = NULL;\
}

#endif








