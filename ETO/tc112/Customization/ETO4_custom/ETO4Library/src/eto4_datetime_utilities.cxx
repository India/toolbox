/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_datetime_utilities.cxx

    Description: This File contains custom functions on date & timeto perform generic operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Function gets current date in TC required Format. Get the current system date
 * and set it to TC Date structure.
 *
 * @param[out] dReleaseDate  Constructed Current date.
 *    
 * @return ITK_ok if function succedded.
 */
int ETO4_get_date(date_t *dReleaseDate)
{
    time_t  run_time;
    struct  tm* sConvTime =  NULL;

    /* Get time thru system calls. */
    run_time = time(NULL);
    sConvTime = localtime(&run_time);

    /* Convert the time into TC format */
    dReleaseDate->year   = 1900+ sConvTime->tm_year;
    dReleaseDate->month  = sConvTime->tm_mon;
    dReleaseDate->day    = sConvTime->tm_mday;
    dReleaseDate->hour   = sConvTime->tm_hour;
    dReleaseDate->minute = sConvTime->tm_min;
    dReleaseDate->second = sConvTime->tm_sec;

    return 0;
}


/**
 * This function gets the current date time.
 *
 * @param[in]  pszFormat   Expected date time format.
 * @param[out] szDateTime  current Date time in required format.
 *
 * @note 'szDateTime' must be a character array of at least [IMF_filename_size_c + 1] 
 *       characters 
 *
 * @retval ITK_ok on successful execution
 * @retval INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME if format resulted in
 *         resultant String length greater then IMF_filename_size_c + 1
 */
int ETO4_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
)
{
    int         iRetCode       = ITK_ok;
    int         iStrLength     = 0;
    struct tm   *clLocalTime   = NULL;
    time_t      tCurrentTime;

    /*Get system's local time*/
    tCurrentTime = time(NULL);
    clLocalTime  = localtime(&tCurrentTime);
        
    /*Get system time  in required format*/
    iStrLength = (int)strftime( szDateTime, (IMF_filename_size_c + 1), pszFormat, clLocalTime);
    if(iStrLength == 0) 
    {
        /* 'strftime'RETURN VALUE:
           If the total number of resulting bytes including the terminating null byte is not
           more than IMF_filename_size_c, strftime() shall return the number of bytes placed
           into the array pointed to by 'szDateTime', not including the terminating null byte. 
           Otherwise, 0 shall be returned and the contents of the array are unspecified.
        */
        iRetCode = INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME;
        EMH_store_error(EMH_severity_error,iRetCode);
    }
      
    return iRetCode;
}



