/*==============================================================================

          Copyright (c) 2007 UGS The PLM Company
                   Unpublished - All rights reserved

================================================================================
File description:   See description below.

    Filename:   TaskPrep.java
    Module  :   com.teamcenter.ets.translator.eto

================================================================================
                     Modification Log
================================================================================
Date        User           Change Description
21-Feb-2012 glangenbacher  Initial Version
11-Dec-2012 glangenbacher  Upgraded code for Teamcenter 9.1
19-Nov-2013 glangenbacher  Added error handling (missing NR)

==============================================================================*/

package com.teamcenter.ets.translator.eto.stampPDF;

import java.util.List;
import com.teamcenter.ets.load.DefaultDatabaseOperation;
import com.teamcenter.translationservice.task.TranslationDBMapInfo;
import com.teamcenter.ets.request.TranslationRequest;
import com.teamcenter.ets.translator.eto.util.DatasetHelper;
import com.teamcenter.ets.translator.eto.util.TranslatorHelper;

//==== Class ====================================================================
/**
 * This custom class is a sub class of the base DatabaseOperation
 * class which performs the loading operation to Tcg. This class stores
 * results for translation requests. This is a configuration specified class
 * based on provider name and translator name in ETS service property file.
 * 
 * @author	Gabriel Langenbacher
 * @version	1.0, 23.02.2012 * 
 */
public class DatabaseOperation extends DefaultDatabaseOperation
{
	/**
	 * Stores translation result data on the source dataset. If there are
	 * existing translation result data files, they may be replaced depending
	 * upon the value of the UpdateExistingVisualizationData TaskPrep instance
	 * variable.
	 *
	 * @param zDbMapInfo DB mapper info object.
	 * @param zFileList  List of files mapped to the source file.
	 *
	 * @throws Exception
	 */
	@Override
	protected void load( TranslationDBMapInfo zDbMapInfo,
			List<String>                 zFileList ) throws Exception
			{
		/*
    	// Switch based on dataset type
    	sourceDataset = (Dataset)TeamcenterServerProxy.getInstance().getProperties(sourceDataset, new String[] { "dataset_type", "object_name" });
        DatasetType zDsType = sourceDataset.get_dataset_type();
        zDsType = (DatasetType)TeamcenterServerProxy.getInstance().getProperties(zDsType, "datasettype_name");
        String dsType = zDsType.get_datasettype_name();
		 */

		loadPart( zFileList );
			} // end load()

	/**
	 * Loads part visualization data. This method calls the createInsetDataset
	 * which creates or inserts a dataset and/or dataset revision, relates newly created
	 * datasets to the item revision, and attaches the specified named reference files.
	 * If there are existing visualization data files, they may be replaced depending
	 * upon the setting of the UpdateExistingVisualizationData ETS Service property. This
	 * property is a boolean flag updateExistingVisData, which is read by the abstract
	 * DatabaseOperation class.
	 *
	 * @param zFileList  List of files mapped to the source file.
	 *
	 * @throws Exception
	 */
	protected void loadPart( List<String> zFileList )
	throws Exception
	{

		//m_zTaskLogger.debug("DatabaseOperation -> datasetType: " + primaryObj.getType().getName());
		//m_zTaskLogger.debug("\t - destinationObject: " + sourceDataset.get_current_name());
		//m_zTaskLogger.debug("\t - relationType: " + datasetBean.getTranslatedDatasetRelationWithIR());
		//m_zTaskLogger.debug("\t - namedReferenceType: " + datasetBean.getTranslatedFileReferenceType());

		// get translation request arguments
		String[]  translatorArgsKeys = 	request.
		getPropertyObject(TranslationRequest.TRANSLATION_ARGS_KEYS).getStringArrayValue();   

		String[]  translatorArgsData = 	request.
		getPropertyObject(TranslationRequest.TRANSLATION_ARGS_DATA).getStringArrayValue();

		TranslatorHelper translatorHelper = new TranslatorHelper(m_zTaskLogger);

		String dstRelType = translatorHelper.getTranslationArgValue("dst_relation_type", translatorArgsKeys, translatorArgsData);
		dstRelType = dstRelType.isEmpty() ? DatasetHelper.TC_REL_TYPE_PDF : dstRelType;

		m_zTaskLogger.debug("DatabaseOperation -> TRANSLATION ARGUMENT VALUE FOR \"dst_relation_type\" IS : "
				+ dstRelType); 		
		/*
        DatasetHelper pDtSetHelper = new DatasetHelper(this.m_zTaskLogger, this.updateExistingVisData, this.softFailureHandlingEnabled);
        pDtSetHelper.createInsertDataset ( sourceItemRev,
                                          sourceDataset,
                                          DatasetHelper.TC_DS_TYPE_PDF, // Type of Object that we have to create in TC to store the file
                                          dstRelType, // Relation to attach the new object to the ItemRevision
                                          DatasetHelper.TC_NR_TYPE_PDF, // NamedReference type to import the file into the new onject
                                          m_scResultDir,
                                          zFileList,
                                          false);

		 */
		// create result dataset only if valid result file found, otherwise dataset would have no named reference 
		if (zFileList.size() > 0)
		{		
			zDtSetHelper.createInsertDataset ( 	sourceItemRev,
					sourceDataset,
					DatasetHelper.TC_DS_TYPE_PDF,
					dstRelType,
					DatasetHelper.TC_NR_TYPE_PDF,
					m_scResultDir,
					zFileList,
					false);
		}

	} // loadPart
}
