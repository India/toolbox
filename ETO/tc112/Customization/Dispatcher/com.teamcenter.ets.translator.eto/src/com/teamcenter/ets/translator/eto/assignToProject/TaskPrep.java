/*==============================================================================

          Copyright (c) 2007 UGS The PLM Company
                   Unpublished - All rights reserved

================================================================================
File description:   See description below.

    Filename:   TaskPrep.java
    Module  :   com.teamcenter.ets.translator.eto

================================================================================
                     Modification Log
================================================================================
Date        User           Change Description
21-Feb-2012 glangenbacher  Initial Version
11-Dec-2012 glangenbacher  Upgraded code for Teamcenter 9.1

==============================================================================*/

package com.teamcenter.ets.translator.eto.assignToProject;

//==== Imports  ================================================================
import com.teamcenter.ets.extract.DefaultTaskPrep;
import com.teamcenter.ets.request.TranslationRequest;
import com.teamcenter.ets.soa.ConnectionManager;
import com.teamcenter.ets.soa.SoaHelper;
import com.teamcenter.ets.translator.eto.assignToProject.exceptions.ProjectNotFoundException;
import com.teamcenter.ets.translator.eto.assignToProject.exceptions.UserNotPrivilegedException;
import com.teamcenter.schemas.soa._2006_03.exceptions.ServiceException;
import com.teamcenter.services.strong.core.ProjectLevelSecurityService;
import com.teamcenter.services.strong.core._2007_09.ProjectLevelSecurity.AssignedOrRemovedObjects;
import com.teamcenter.services.strong.core._2009_10.ProjectLevelSecurity.ProjectInfo;
import com.teamcenter.services.strong.core._2009_10.ProjectLevelSecurity.UserProjectsInfoInput;
import com.teamcenter.services.strong.core._2009_10.ProjectLevelSecurity.UserProjectsInfoResponse;
import com.teamcenter.services.strong.workflow.WorkflowService;
import com.teamcenter.services.strong.workflow._2008_06.Workflow.ContextData;
import com.teamcenter.services.strong.workflow._2008_06.Workflow.InstanceInfo;
import com.teamcenter.soa.client.model.ErrorStack;
import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.soa.client.model.ServiceData;
import com.teamcenter.soa.client.model.strong.BusinessObject;
import com.teamcenter.soa.client.model.strong.Item;
import com.teamcenter.soa.client.model.strong.ItemRevision;
import com.teamcenter.soa.client.model.strong.TC_Project;
import com.teamcenter.soa.client.model.strong.User;
import com.teamcenter.soa.exceptions.NotLoadedException;
import com.teamcenter.translationservice.task.TranslationTask;

//==== Class ====================================================================
/**
 * Custom TaskPrep class for assignToProject translator. This sub class prepares a
 * assignToProject translation task. This is a configuration specified class based on
 * provider name and translator name in DispatcherClient property file which
 * creates the assignToProject specific translation request by preparing the data for
 * translation and creating the Translation request object.
 * 
 * @author	Gabriel Langenbacher
 * @version	1.1, 11.12.2012
 * 
 */
public class TaskPrep extends DefaultTaskPrep
{    
	/** 
	 * The service user who is running this process.
	 * @serial
	 * */
	private static String serviceUser;

	/**
	 * The ETS Extractor service invokes the prepareTask() class method to
	 * prepare input data for translation requests. This method does the following
	 *    Validates that all data necessary to translate the primary object(s) is
	 *        available. Primary object(s) that are data incomplete are generally
	 *        excluded from the translator input with appropriate logging.
	 *    Collects all input files and creates TranslationTask object
	 *
	 * @see com.teamcenter.ets.extract.TaskPrep#prepareTask()
	 * @throws If any exception has been thrown by any function used here.
	 *
	 * @return Returns the Translation Task object.
	 */
	public TranslationTask prepareTask() throws Exception
	{
		TranslationTask zTransTask = new TranslationTask();

		m_zTaskLogger.debug("TaskPrep -> TRANSLATOR (lowercase): "
				+ request.getPropertyObject(TranslationRequest.TRANSLATOR_NAME)
				.getStringValue().toLowerCase());    	

		// get primary objects and secondary objects
		ModelObject primary_objs[]   = 	request.
		getPropertyObject( TranslationRequest.PRIMARY_OBJS ).
		getModelObjectArrayValue();

		ModelObject secondary_objs[] = 	request.
		getPropertyObject( TranslationRequest.SECONDARY_OBJS ).
		getModelObjectArrayValue();      

		// *****************************
		// Get connection for initiating services
		// *****************************   
		com.teamcenter.soa.client.Connection connection = ConnectionManager.getActiveConnection();

		// *****************************
		// Get service stubs
		// *****************************    
		ProjectLevelSecurityService psService = ProjectLevelSecurityService.getService(connection);

		// *****************************
		// Get current users projects
		// *****************************
		User currentUser = ConnectionManager.getLoggedInUser();
		UserProjectsInfoInput userProjectsInfoInput[] = new UserProjectsInfoInput[1];
		userProjectsInfoInput[0] = new UserProjectsInfoInput();
		userProjectsInfoInput[0].user = currentUser;
		userProjectsInfoInput[0].activeProjectsOnly = true;
		userProjectsInfoInput[0].privilegedProjectsOnly = false; // maybe true
		userProjectsInfoInput[0].programsOnly = false;
		serviceUser = currentUser.get_user_id();

		m_zTaskLogger.debug("TaskPrep -> CURRENT USER IS: " + serviceUser);  	

		UserProjectsInfoResponse usersProjectList = psService.getUserProjects(userProjectsInfoInput);
		ProjectInfo usersProjects [] = usersProjectList.userProjectInfos[0].projectsInfo;

		for (int i = 0; i < secondary_objs.length; i++)
		{
			// *****************************
			// Get primary and secondary objects of current translation request
			// *****************************         	
			Item item  				= (Item)primary_objs[i];
			ItemRevision itemRev	= (ItemRevision)secondary_objs[i];

			itemRev = ( ItemRevision ) SoaHelper.getProperties( itemRev, new String[]{"item_id", "item_revision_id"} );

			String itemId	= itemRev.get_item_id();

			m_zTaskLogger.debug("TaskPrep -> CURRENT ITEM ID IS: " + itemId);

			// *****************************
			// Do status assignment for revision
			// ***************************** 
			//WorkflowService workflowService = WorkflowService.getService(connection);
			//this.triggerWorkflow(workflowService, DatasetHelper.WORKFLOW_REQ_PHASE, secondary_objs[i]);
			
			// *****************************
			// Do project assignment
			// *****************************              
			this.assignToProject(psService, usersProjects, itemId, new BusinessObject[]{item});

			zTransTask = this.addOptions(zTransTask, "project_id", itemId);

			zTransTask = prepTransTask( 	zTransTask, 
					null, 
					null, 
					"",
					false, 
					false, 
					".txt", 
					0, 
					null 
			);               							  	
		}       

		return addRefIdToTask( zTransTask, 0 );
	}

	/**
	 * Assigns objects to a project
	 * 
	 * @param service 				Service stub
	 * @param projectsList 			Project list of current user
	 * @param projectId 			ID of the project to be assigned
	 * @param objects 				Objects that have to be assigned to the project
	 * 
	 * @throws NotLoadedException			If project properties are not loaded
	 * @throws UserNotPrivilegedException	If user is not a privileged project member
	 * @throws ProjectNotFoundException		If user is not a project member
	 */    
	private void assignToProject (ProjectLevelSecurityService service, ProjectInfo[] projectsList, String projectId, BusinessObject[] objects) 
	throws NotLoadedException, UserNotPrivilegedException, ProjectNotFoundException
	{
		boolean projectFound = false;

		m_zTaskLogger.debug("TaskPrep -> FOUND USER PROJECTS: " + projectsList.length);

		for (int j = 0; j < projectsList.length; j++ )
		{
			m_zTaskLogger.debug("TaskPrep -> PROCESSING PROJECT: " + projectsList[j].project.get_project_id());

			if (projectsList[j].project.get_project_id().equalsIgnoreCase(projectId))
			{
				projectFound = true;
				// project found   

				// *****************************
				// Prepare data to assign to project
				// *****************************    
				AssignedOrRemovedObjects objectsToUpdate[] = new AssignedOrRemovedObjects[1];
				objectsToUpdate[0] = new AssignedOrRemovedObjects();
				objectsToUpdate[0].projects = new TC_Project[]{projectsList[j].project};
				objectsToUpdate[0].objectToAssign = objects;

				// *****************************
				// Execute the service operation (assign to project)
				// *****************************    	    	
				ServiceData serviceData = service.assignOrRemoveObjects(objectsToUpdate);

				if (serviceData.sizeOfPartialErrors() > 0)
					throw new UserNotPrivilegedException("ProjectLevelSecurityService.assignOrRemoveObjects returned a partial error. This is most likely because of the current service user \"" + serviceUser + "\" is not privileged in the project \"" + projectId + "\"");
			}        		
		} 

		if (!projectFound)
			throw new ProjectNotFoundException("Project \"" + projectId + "\" was not found in users projects. This is most likely because of the current service user \"" + serviceUser + "\" is not assigned to the project.");
	} 
	
	@SuppressWarnings("unused")
	private void triggerWorkflow(WorkflowService workflowService, String workflowName, ModelObject object) throws ServiceException
	{
		
		ContextData ctxData = new ContextData();
		String[] attach = new String[]{object.getUid()};
		int[] atttyp = new int[]{1};

		ctxData.attachmentCount = 1;
		ctxData.attachments = attach ;
        ctxData.attachmentTypes = atttyp;
        ctxData.processTemplate = workflowName;		
        
        InstanceInfo instInfo = workflowService.createInstance( true,
                "",
                "BlockDocProc",
                "BlockDocSub",
                "BlockProcItem",
                ctxData );     
        
        if( instInfo.serviceData.sizeOfPartialErrors() > 0 ) 
        {
            ErrorStack e = instInfo.serviceData.getPartialError(0);
            String[] msg=e.getMessages();
            //errorText=msg[0];

            //ServiceException se = new ServiceException("Error Occured");
            ServiceException se = new ServiceException(msg[0]);

            throw se;
        }        
	}
}