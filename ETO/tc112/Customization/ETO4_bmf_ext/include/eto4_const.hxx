/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_const.hxx

    Description:  Header File for constants used during scope of the DLL.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifndef ETO4_CONST_HXX
#define ETO4_CONST_HXX


/* Prefixes */
#define ETO4_PREF_PHASE_FOLDERS_PREFIX                            "ETO4_Phase_Folders_"

/* Postfixes */
#define ETO4_PREF_MANDATORY_ITEMS_POSTFIX                         "_mandatory_items"

/* Preferences */
#define ETO4_PREF_PHASE_FOLDERS_DEFAULT                           "ETO4_Phase_Folders_Default"
#define ETO4_PREF_ATTRIBUTE_FOR_PROJECT_NAME                      "ETO4_Attribute_For_Project_Name"
#define ETO4_PREF_IR_PHASE_FOLDER_REL                             "ETO4_ItemRev_Phase_Folder_Relation"
#define ETO4_PREF_PHASE_FOLDER_ALLOWED_OBJ_TYPES                  "ETO4_Phase_Folder_Allowed_Object_Types"
#define ETO4_PREF_FOLDER_CONTENT_PROPAGATE_OBJECTS                "ETO4_Folder_Content_Propagate_Objects"
#define ETO4_PREF_PROPAGATION_INFORMATION                         "ETO4_Propagation_Information"
#define ETO4_PREF_PROJECT_ID_PREFIX_LIST                          "ETO4_Project_ID_Prefix_List"

/* Relation    */
#define ETO4_IMAN_REFERENCE_RELATION                              "IMAN_reference"
#define ETO4_IMAN_BASED_ON                                        "IMAN_based_on"

/* Attributes  */
#define ETO4_ATTR_OBJECT_NAME_ATTRIBUTE                           "object_name"
#define ETO4_ATTR_DESCRIPTION_ATTRIBUTE                           "object_desc"
#define ETO4_ATTR_ITEM_ID                                         "item_id"
#define ETO4_ATTR_ITEM_TAG                                        "items_tag"
#define ETO4_ATTR_ITEM_REVISION_ID                                "item_revision_id"
#define ETO4_ATTR_ITEM_TAG                                        "items_tag"
#define ETO4_ATTR_OBJECT_TYPE_ATTRIBUTE                           "object_type"
#define ETO4_ATTR_OBJECT_CREATION_DATE                            "creation_date"                                 
#define ETO4_ATTR_CONTENTS                                        "contents"    
#define ETO4_ATTR_PROJECT_LIST                                    "project_list"
#define ETO4_ATTR_LICENSE_LIST                                    "license_list"
#define ETO4_ATTR_IP_CLASSIFICATION                               "ip_classification"
#define ETO4_ATTR_REVISION                                        "Revision"
#define ETO4_ATTR_ACTIVE_SEQ                                      "active_seq"
#define ETO4_ATTR_OWNING_USER                                     "owning_user" 

/* Class Name */
#define ETO4_CLASS_WORKSPACEOBJECT                                "WorkspaceObject"
#define ETO4_CLASS_ITEMREVISION                                   "ItemRevision"
#define ETO4_CLASS_PROJECT                                        "ETO4_Project"
#define ETO4_CLASS_PROJECT_REVISION                               "ETO4_ProjectRevision"
#define ETO4_CLASS_PHASE_FOLDER                                   "ETO4_Phase_Folder"
#define ETO4_CLASS_PHASE_SUB_FOLDER                               "ETO4_Phase_Sub_Folder"
#define ETO4_CLASS_FOLDER				                          "Folder"

/* Propagation Information */
#define ETO4_CONST_PROJECT                                        "Project"
#define ETO4_CONST_LICENSE                                        "License"
#define ETO4_CONST_IP_CLASSIFICATION                              "IP Classification"


/* Argument Names Passed Through BMF */
#define ETO4_BMF_ARG_WF_TEMPLATE_NAME                             "templateName"

/* Dispatcher constants */
#define ETO4_ETS_ARG_VALUE_PROVIDER                               "ETO"
#define ETO4_ETS_ARG_VALUE_PRIORITY                               2
#define ETO4_ETS_ARG_VALUE_TRIGGER                                "ETO-TRIGGER"

/* Dispatcher Service Name */
#define ETO4_ETS_SERVICE_INITIATE_WORKFLOW                        "eto4-initiate-workflow"

/* Folder Names String */
#define ETO4_NEWSTUFF_FOLDER                                      "newstuff"
#define ETO4_HOME_FOLDER                                          "home"

/* Separators */
#define ETO4_STRING_UNDERSCORE                                    "_"
#define ETO4_STRING_HIFEN                                         "-"
#define ETO4_STRING_SEMICOLON                                     ";"
#define ETO4_STRING_FORWARD_SLASH                                 "/"
#define ETO4_STRING_SINGLE_SPACE                                  " "
#define ETO4_STRING_COLON                                         ":"
#define ETO4_CHARACTER_COLON                                      ':'
#define ETO4_CHARACTER_SEMICOLON                                  ';'
#define ETO4_CHARACTER_HIFEN                                      '-'

#define ETO4_SEPARATOR_LENGTH                                     1
#define ETO4_NUMERIC_ERROR_LENGTH                                 12

#endif

