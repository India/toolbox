/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_extension_arguments.hxx

    Description:  Header File for eto4_extension_arguments.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_EXTENSION_ARGUMENTS_HXX
#define ETO4_EXTENSION_ARGUMENTS_HXX
    
#include <tccore/item_msg.h>
#include <tccore/tc_msg.h>
#include <itk/bmf.h>


int ETO4_bmf_get_arg_val
(
    METHOD_message_t* pmMessage, 
    char*             pszArgumentName,
    char**            ppszBMFArgVal
);

int ETO4_bmf_get_arg_val_list
(
    METHOD_message_t* pmMessage, 
    int               iArgCount,
    char**            ppszArgNamesList,    
    char***           pppszBMFArgValList,
    int*              piArgValCount
);

#endif



