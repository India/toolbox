/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_string_macros.hxx

    Description:  This file contains macros for string's basic operation such as copy, concatination etc

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_STRING_MACROS_HXX
#define ETO4_STRING_MACROS_HXX

#include <eto4_library.hxx>

#ifdef ETO4_STRCPY
#undef ETO4_STRCPY
#endif
#define ETO4_STRCPY(pszDestStr, pszSourceStr){\
                    int iSourceStrLen = (int) (tc_strlen(pszSourceStr)+1);\
                    pszDestStr = (char*)MEM_alloc((int)(iSourceStrLen * sizeof(char)));\
                    tc_strcpy(pszDestStr, pszSourceStr);\
}

#ifdef ETO4_STRCAT
#undef ETO4_STRCAT
#endif
#define ETO4_STRCAT(pszDestStr,pszSourceStr){\
                    int iSourceStrLen = (int) tc_strlen(pszSourceStr);\
                    int iDestStrLen = 0;\
                    if(pszDestStr != NULL){\
                        iDestStrLen = (int) tc_strlen (pszDestStr);\
                        pszDestStr = (char*)MEM_realloc(pszDestStr, (int)((iSourceStrLen + iDestStrLen + 1)* sizeof(char)));\
                    }\
                    else{\
                        pszDestStr = (char*)MEM_alloc((int)(sizeof(char) * (iSourceStrLen + 1)));\
                    }\
                    if(iDestStrLen == 0){\
                       tc_strcpy(pszDestStr, pszSourceStr);\
                    }\
                    else{\
                       tc_strcat(pszDestStr, pszSourceStr);\
                       tc_strcat(pszDestStr, '\0');\
                    }\
}

/**
 * This function adds input new string to the String Array. This also increases the value of the variable holding the 
 * original size of array
 *  
 */
#ifdef ETO4_UPDATE_STRING_ARRAY
#undef ETO4_UPDATE_STRING_ARRAY
#endif
#define ETO4_UPDATE_STRING_ARRAY(iCurrentArrayLen, ppszStrArray, pszNewValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ppszStrArray = (char**) MEM_alloc(iCurrentArrayLen * sizeof(char*));\
            }\
            else{\
                ppszStrArray = (char**) MEM_realloc(ppszStrArray, iCurrentArrayLen * sizeof(char*));\
            }\
            ppszStrArray[iCurrentArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszNewValue) + 1)* sizeof(char)));\
            tc_strcpy(ppszStrArray[iCurrentArrayLen - 1], pszNewValue);\
}


/* This macro add the new string to new index which is supplied to this macro as input argument. 
   This macro does not increments the variable 'iNewArrayLen' instead expects the calling function to should
   provide the incremented size of the string array.
*/
#ifdef ETO4_ADD_STRING_TO_ARRAY
#undef ETO4_ADD_STRING_TO_ARRAY
#endif
#define ETO4_ADD_STRING_TO_ARRAY(iNewArrayLen, ppszStrArray, pszNewValue){\
            if(iNewArrayLen == 1){\
                ppszStrArray = (char**) MEM_alloc(iNewArrayLen * sizeof(char*));\
            }\
            else{\
                ppszStrArray = (char**) MEM_realloc(ppszStrArray, iNewArrayLen * sizeof(char*));\
            }\
            ppszStrArray[iNewArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszNewValue) + 1)* sizeof(char)));\
            tc_strcpy(ppszStrArray[iNewArrayLen - 1], pszNewValue);\
}

/*  Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc for user defined data type. */
#ifdef ETO4_ALLOCATE_STR_STRUCT_MEM
#undef ETO4_ALLOCATE_STR_STRUCT_MEM
#endif
/* This macro is used to free the memory of structure. */
#define ETO4_ALLOCATE_STR_STRUCT_MEM(psStringInfoList, iStructSize){\
                                    iStructSize++;\
                                    if (psStringInfoList == NULL || iStructSize == 1){\
                                            psStringInfoList = (sStrArrayList*) MEM_alloc((int)(sizeof(sStrArrayList) * iStructSize));\
                                    }\
                                    else{\
                                            psStringInfoList = (sStrArrayList*) MEM_realloc(psStringInfoList, (int)((sizeof(sStrArrayList) * iStructSize)));\
                                    }\
                                    psStringInfoList[iStructSize-1].iStrCnt = 0;\
                                    psStringInfoList[iStructSize-1].ppszStrLines = NULL;\
}

/*  Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc for user defined data type. */
#ifdef ETO4_ALLOCATE_ATTR_VAL_STRUCT_MEM
#undef ETO4_ALLOCATE_ATTR_VAL_STRUCT_MEM
#endif
/* This macro is used to free the memory of structure. */
#define ETO4_ALLOCATE_ATTR_VAL_STRUCT_MEM(psCloneAttrValInfo, iAttrValListCnt){\
                                        iAttrValListCnt++;\
                                        if (psCloneAttrValInfo == NULL || iAttrValListCnt == 1){\
                                                psCloneAttrValInfo = (sCloneAttrValList*) MEM_alloc((int)(sizeof(sCloneAttrValList) * iAttrValListCnt));\
                                        }\
                                        else{\
                                                psCloneAttrValInfo = (sCloneAttrValList*) MEM_realloc(psCloneAttrValInfo, (int)((sizeof(sCloneAttrValList) * iAttrValListCnt)));\
                                        }\
                                        psCloneAttrValInfo[iAttrValListCnt-1].pszPartFileName = NULL;\
                                        psCloneAttrValInfo[iAttrValListCnt-1].tItem    = NULLTAG;\
                                        psCloneAttrValInfo[iAttrValListCnt-1].tItemRev = NULLTAG;\
                                        psCloneAttrValInfo[iAttrValListCnt-1].tBOMLine = NULLTAG;\
}

#endif //ETO4_STRING_MACROS_HXX









