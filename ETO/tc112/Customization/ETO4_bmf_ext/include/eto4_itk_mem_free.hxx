/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_itk_mem_free.hxx

    Description: This file contains macros for freeing portion of memory allocated with TCe 'MEM_' functions and by TCe itself.


========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifndef ETO4_ITK_MEM_FREE_HXX
#define ETO4_ITK_MEM_FREE_HXX

#include <eto4_library.hxx>

/* We want to use our macros. If some others are defined under same name, undefine them */

#ifdef ETO4_MEM_TCFREE
#undef ETO4_MEM_TCFREE
#endif

/* Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc */

#define ETO4_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }


#ifdef ETO4_UPDATE_TAG_ARRAY
#undef ETO4_UPDATE_TAG_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation if done for the very first time
*/
#define ETO4_UPDATE_TAG_ARRAY(iCurrentArrayLen, ptArray, tValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ptArray = (tag_t*) MEM_alloc(iCurrentArrayLen * sizeof(tag_t));\
            }\
            else{\
                ptArray = (tag_t*) MEM_realloc(ptArray, iCurrentArrayLen * sizeof(tag_t));\
            }\
            ptArray[iCurrentArrayLen-1]=  tValue;\
}

#ifdef ETO4_ADD_TAG_TO_ARRAY
#undef ETO4_ADD_TAG_TO_ARRAY
#endif
/* This macro add the new tag to new index which is supplied to this macro as input argument. 
   This macro does not increments the variable 'iNewArrayLen' instead expects the calling function to should
   provide the incremented size of the tag array.
*/
#define ETO4_ADD_TAG_TO_ARRAY(iNewArrayLen, ptArray, tValue){\
            if(iNewArrayLen == 1){\
                ptArray = (tag_t*) MEM_alloc(iNewArrayLen * sizeof(tag_t));\
            }\
            else{\
                ptArray = (tag_t*) MEM_realloc(ptArray, iNewArrayLen * sizeof(tag_t));\
            }\
            ptArray[iNewArrayLen-1] = tValue;\
}


#ifdef ETO4_ADD_INT_TO_ARRAY
#undef ETO4_ADD_INT_TO_ARRAY
#endif
/* This macro add the new tag to new index which is supplied to this macro as input argument. 
   This macro does not increments the variable 'iNewArrayLen' instead expects the calling function to should
   provide the incremented size of the tag array.
*/
#define ETO4_ADD_INT_TO_ARRAY(iNewArrayLen, piArray, iValue){\
            if(iNewArrayLen == 1){\
                piArray = (int*) MEM_alloc(iNewArrayLen * sizeof(int));\
            }\
            else{\
                piArray = (int*) MEM_realloc(piArray, iNewArrayLen * sizeof(int));\
            }\
            piArray[iNewArrayLen-1] = iValue;\
}

#ifdef ETO4_CLONE_INT_TO_ARRAY
#undef ETO4_CLONE_INT_TO_ARRAY
#endif
/* This macro creates replica of supplied integer array.
*/
#define ETO4_CLONE_INT_TO_ARRAY(piDestArray, piSrcArray, iArrayLen){\
            for(int iDx = 0; iDx < iArrayLen; iDx++) {\
                ETO4_ADD_INT_TO_ARRAY((iDx+1), piDestArray, piSrcArray[iDx]);\
            }\
}

/*  Macro to free memory for user defined data type. */
#ifdef ETO4_TC_FREE_STR_LIST_STRUCT
#undef ETO4_TC_FREE_STR_LIST_STRUCT
#endif
/* This macro is used to free the memory of structure. */
#define ETO4_TC_FREE_STR_LIST_STRUCT(psStringInfoList, iStructSize) {\
            for(int iDx = 0; iDx < iStructSize; iDx++) {\
                    if((psStringInfoList[iDx].iStrCnt) > 0) {\
                            ETO4_MEM_TCFREE(psStringInfoList[iDx].ppszStrLines);\
                        }\
            }\
            ETO4_MEM_TCFREE(psStringInfoList);\
}

/*  Macro to free memory for user defined data type. */
#ifdef ETO4_TC_FREE_ATTR_VAL_LIST_STRUCT
#undef ETO4_TC_FREE_ATTR_VAL_LIST_STRUCT
#endif
/* This macro is used to free the memory of structure. */
#define ETO4_TC_FREE_ATTR_VAL_LIST_STRUCT(psCloneAttrValInfo, iAttrValListCnt) {\
                                          for(int iDx = 0; iDx < iAttrValListCnt; iDx++) {\
                                                ETO4_MEM_TCFREE(psCloneAttrValInfo[iDx].pszPartFileName);\
                                          }\
                                          ETO4_MEM_TCFREE(psCloneAttrValInfo);\
}


#endif








