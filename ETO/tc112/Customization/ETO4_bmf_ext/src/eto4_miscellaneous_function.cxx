/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_miscellaneous_function.cxx

    Description: This File contains custom that are used by BMF extension functions defined in file 'eto4_bmf_extension.cxx'.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>
#include <eto4_miscellaneous_function.hxx>

/**
 * This function validate if 'TC_Project' attribute 'project_id' starts with any one of the string that are specified in the 
 * preference 'ETO4_Project_ID_Prefix_List'.
 *
 * @param[in]  pszProjectID     Project ID value of TC_Project
 * @param[out] pbHasValidPrefix 'true' if project_id has value that start's with the prefix as mentioned in the preference else 'false'
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_validate_project_id_attr_prefix
(
    char*    pszProjectID,
    logical* pbHasValidPrefix
)
{
    int    iRetCode       = ITK_ok;
    int    iPrefixCnt     = 0;
    char** ppszPrefixList = NULL;

    /* Initializing the Out Parameter to this function. */
    (*pbHasValidPrefix) = false;

    ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values(ETO4_PREF_PROJECT_ID_PREFIX_LIST, &iPrefixCnt, &ppszPrefixList));

    for(int iDx = 0; iDx < iPrefixCnt && iRetCode == ITK_ok; iDx++)
    {
        int iLen = 0;

        iLen = (int) tc_strlen(ppszPrefixList[iDx]);

        if(tc_strncasecmp(pszProjectID, ppszPrefixList[iDx], iLen) == 0)
        {
            (*pbHasValidPrefix) = true;
            break;
        }
    }

    if(iPrefixCnt == 0)
    {
        TC_write_syslog("\nWarning: \"Engineering To Order\": Preference %s is empty or might not exist in the system.\n", ETO4_PREF_PROJECT_ID_PREFIX_LIST);
    }

    ETO4_MEM_TCFREE( ppszPrefixList );
    return iRetCode;
}


/**
 * Function is responsible for creating Folders and its sub folder hierarchy and attaching it to input Item revision.
 * Input argument 'pszObjectName' contains object_name. 
 * The object name has prefix separated with a "-" E.g. "ENT-Weser" has the prefix "ENT" or "SW-Oder" has the prefix 
 * "SW". This prefix is retrieved from the object_name attribute. A multi value preference with the naming pattern
 * "ETO4_Phase_Folders_<PREFIX>" is checked in Teamcenter.  E.g. for the prefix "ENT" the preference 
 * "ETO4_Phase_Folders_ENT" should exist.
 * Each line of preference starts with parent folders type followed by sub folder name which will be of type 'ETO4_Phase_Sub_Folder'. 
 * Name are separated by colon ':'. Few Item revisions are also created and added to these Parent Folders. Information about the 
 * list of Item types are stored along with mandatory attribute name and value information are stored in the preference with the name
 * "<PHASE_FOLDER_TYPE>_mandatory_items". values have to follow the pattern: "<ITEM_TYPE>:<OBJECT_NAME>:<ATTR NAME 1>:<ATTR NAME 2>".
 * The items will to be created in the same order as specified in the preference. The revision of the document has to be pasted to 
 * the phase folder. The items created by the system will to be pasted to the Newstuff folder of the current logged in user.
 * The property "object_desc" of the sub folders created within this process has to get updated with the "object_type" value of their
 * parent folder (e.g. ETO4_P01_Req_Phase).
 *
 * @param[in] pszObjectName  ETO Project Item attribute that contains Project Name Info                    
 * @param[in] ptNewItemRev   Tag Pointer to New ETO Project revision
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_create_phase_folders
(
    char*   pszObjectName,
    tag_t*  ptNewItemRev,
    logical bCreateDoc
)
{
    int     iRetCode            = ITK_ok;
    int     iMarkPoint          = 0;
    int     iTokenCnt           = 0;
    int     iPrefValCnt         = 0;
    char*   pszPrefPhaseFolder  = NULL;
    char*   pszIRPhaseFLRel     = NULL;
    char**  ppszAllTokens       = NULL;
    char**  ppszPrefVals        = NULL;
    
    /* Get the Project Name (object name )prefix, */
    ETO4_ITKCALL(iRetCode, ETO4_str_parse_string( pszObjectName , ETO4_STRING_HIFEN, &iTokenCnt, &ppszAllTokens ));
 
    if( iTokenCnt > 1 )
    {
        ETO4_STRCAT( pszPrefPhaseFolder, ETO4_PREF_PHASE_FOLDERS_PREFIX );

        ETO4_STRCAT( pszPrefPhaseFolder, ppszAllTokens[0] );

        ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values( pszPrefPhaseFolder, &iPrefValCnt, &ppszPrefVals ));
    }

    /* If no such a preference exist or no prefix in project name use the default preference 'ETO4_Phase_Folders_Default' */
    if( iPrefValCnt == 0 )
    {
        ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values( ETO4_PREF_PHASE_FOLDERS_DEFAULT, &iPrefValCnt, &ppszPrefVals ));

        if(iPrefValCnt == 0)
        {
            TC_write_syslog("\nWarning: \"Engineering To Order\": Preference %s is empty or might not exist in the system.\n", ETO4_PREF_PHASE_FOLDERS_DEFAULT);
        }
    }

    if(iPrefValCnt > 0)
    {
        ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_value(ETO4_PREF_IR_PHASE_FOLDER_REL, &pszIRPhaseFLRel));
           
        if(pszIRPhaseFLRel == NULL)
        {
            ETO4_STRCPY(pszIRPhaseFLRel, ETO4_IMAN_REFERENCE_RELATION);
        }

        /* Define a markpoint for database transaction */
        ETO4_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));

        /* For every value, create folder of the type Preference value and folder name = preference value */
        for( int iNx = 0; iNx < iPrefValCnt && iRetCode == ITK_ok; iNx++ )
        {
            int    iTempPrefValCnt = 0;
            char** ppszTempPrefVal = NULL;

            ETO4_ITKCALL(iRetCode, ETO4_str_parse_string( ppszPrefVals[iNx], ETO4_STRING_SEMICOLON, &iTempPrefValCnt, &ppszTempPrefVal));

            if(iTempPrefValCnt > 0)
            {
                int    iFolderCnt     = 0;
                tag_t  tParentFolder  = NULLTAG;
                char** ppszFolderType = NULL;

                ETO4_ITKCALL(iRetCode, ETO4_str_parse_string( ppszTempPrefVal[0], ETO4_STRING_COLON, &iFolderCnt, &ppszFolderType));

                for( int iJx = 0; iJx < iFolderCnt && iRetCode== ITK_ok; iJx++)
                {
                    tag_t  tRelation  = NULLTAG;
                    tag_t  tFolder    = NULLTAG;

                    if(iJx == 0)
                    {
                        ETO4_ITKCALL(iRetCode, ETO4_create_custom_folder( ppszFolderType[iJx], ppszFolderType[iJx], NULL, 0, NULL, NULL, &tParentFolder ));
                        //tParentFolder = tFolder;

                        ETO4_ITKCALL(iRetCode, ETO4_grm_create_relation( *ptNewItemRev, tParentFolder, pszIRPhaseFLRel, &tRelation ));
                    }
                    else
                    {  
                        ETO4_ITKCALL(iRetCode, ETO4_create_custom_folder( ETO4_CLASS_PHASE_SUB_FOLDER, ppszFolderType[iJx], ppszFolderType[0], 0, NULL, NULL, &tFolder ));

                        ETO4_ITKCALL(iRetCode, ETO4_check_and_create_fl_sub_substruct(ppszFolderType[iJx], ppszFolderType[0], tFolder, iTempPrefValCnt, ppszTempPrefVal));

                        /* Insert New Document revision to Phase Folder */
                        ETO4_ITKCALL(iRetCode, ETO4_insert_content_to_folder(tParentFolder, tFolder, 999));
                    }
                }

                if(iFolderCnt > 0 && bCreateDoc == true)
                {
                    /* Creating mandatory items for each phase folder */
                    ETO4_ITKCALL(iRetCode, ETO4_create_items_for_phase_folder(tParentFolder, pszObjectName, ppszFolderType[0]));
                } 

                ETO4_MEM_TCFREE(ppszFolderType);
            }

            ETO4_MEM_TCFREE(ppszTempPrefVal);
        }

        /* Commit the information into the database. */
        ETO4_ITKCALL(iRetCode, POM_forget_markpoint(iMarkPoint));
            
        if(iRetCode != ITK_ok)
        {
            logical bStateChanged = false;
            
            /* Do the rollback of the database. */
            POM_roll_to_markpoint(iMarkPoint, &bStateChanged);
        }
    }

    ETO4_MEM_TCFREE( pszIRPhaseFLRel );
    ETO4_MEM_TCFREE( pszPrefPhaseFolder );
    ETO4_MEM_TCFREE( ppszAllTokens );
    ETO4_MEM_TCFREE( ppszPrefVals );
    return iRetCode;
}

/**
 * This function constructs the preference name which carries the mandatory item list and attribute values.
 * Function retrieves the Item Type specified in the preference. Preference value is the combination of Item
 * type name and the mandatory attribute value and name list needed during creation of Item type specified
 * in the preference.
 *
 * @param[in]   tParentFolder        Tag of Folder to which Item Revision will be added
 * @param[in]   pszETOPrjName        Name of the ETO Project
 * @param[in]   pszPhaseFolderType   Phase folder type 
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_create_items_for_phase_folder
(
    tag_t   tParentFolder,
    char*   pszETOPrjName,
    char*   pszPhaseFolderType
)
{
    int    iRetCode            = ITK_ok;
    int    iPrefValCnt         = 0;
    int    iAttrCount          = 0;
    char*  pszPrefItemTypeList = NULL;
    char** ppszPrefVals        = NULL;
    char** ppszItemAttrNames   = NULL;

    pszPrefItemTypeList = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(pszPhaseFolderType) + tc_strlen(ETO4_PREF_MANDATORY_ITEMS_POSTFIX) + 1)));

    tc_strcpy(pszPrefItemTypeList, pszPhaseFolderType);
    tc_strcat(pszPrefItemTypeList, ETO4_PREF_MANDATORY_ITEMS_POSTFIX);
    tc_strcat(pszPrefItemTypeList, '\0');

    ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values( pszPrefItemTypeList, &iPrefValCnt, &ppszPrefVals ));

    for(int iNx = 0; iNx < iPrefValCnt && iRetCode == ITK_ok; iNx++)
    {
        int    iDocAttrInfoCnt      = 0;
        tag_t  tItem                = NULLTAG;
        tag_t  tItemRev             = NULLTAG;
        char*  pszItemType          = NULL;
        char*  pszItemName          = NULL;
        char** ppszPrefItemAttrVal  = NULL;
        char** pszItemAttrValues    = NULL;

        if(iNx == 0)
        {
            int    iParsedValCnt = 0;
            char** ppszParsedVal = 0;

            ETO4_ITKCALL(iRetCode, ETO4_str_parse_string( ppszPrefVals[iNx], ETO4_STRING_COLON, &iParsedValCnt, &ppszParsedVal));

            if(iParsedValCnt > 2)
            {
                iAttrCount = iParsedValCnt - 2;

                for(int iJx = 0; iJx < iAttrCount && iRetCode == ITK_ok; iJx++)
                {
                    int   iCnt = iJx;
                    char* pszAttrName = NULL;

                    ETO4_STRCPY(pszAttrName, ppszParsedVal[iCnt+2]);

                    ETO4_UPDATE_STRING_ARRAY(iCnt, ppszItemAttrNames, pszAttrName);

                    ETO4_MEM_TCFREE( pszAttrName );
                }
            }

            ETO4_MEM_TCFREE( ppszParsedVal );
        }
        else
        {
            ETO4_ITKCALL(iRetCode, ETO4_str_parse_string( ppszPrefVals[iNx], ETO4_STRING_COLON, &iDocAttrInfoCnt, &ppszPrefItemAttrVal));

            if(iAttrCount == (iDocAttrInfoCnt -2))
            {
                pszItemAttrValues = (char**) MEM_alloc((int)(sizeof(char*) * iAttrCount));

                for(int iDx = 0; iDx < iDocAttrInfoCnt && iRetCode == ITK_ok; iDx++)
                {
                    if(iDx == 0)
                    {
                        pszItemType = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(ppszPrefItemAttrVal[iDx]) +1)));
                        tc_strcpy(pszItemType, ppszPrefItemAttrVal[iDx]);
                    }
                    else
                    {
                        if(iDx == 1)
                        {
                            pszItemName = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(ppszPrefItemAttrVal[iDx]) + tc_strlen(pszETOPrjName) + tc_strlen(ETO4_STRING_SINGLE_SPACE) + 1)));
                            tc_strcpy(pszItemName, ppszPrefItemAttrVal[iDx]);
                            tc_strcat(pszItemName, ETO4_STRING_SINGLE_SPACE);
                            tc_strcat(pszItemName, pszETOPrjName);
                            tc_strcat(pszItemName, '\0');
                        }
                        else
                        {
                            pszItemAttrValues[iDx-2] = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(ppszPrefItemAttrVal[iDx]) +1)));

                            tc_strcpy(pszItemAttrValues[iDx-2], ppszPrefItemAttrVal[iDx]);
                        }
                    }
                }
            }
            else
            {
                iRetCode = INCORRECT_PREFERENCE_VALUE;
                EMH_store_error_s1(EMH_severity_error, iRetCode, pszPrefItemTypeList);  
            }

            /* Create custom Item Revision */
            ETO4_ITKCALL(iRetCode, ETO4_create_item_type(pszItemType, pszItemName, iAttrCount, ppszItemAttrNames, pszItemAttrValues, &tItem, &tItemRev));

            ETO4_ITKCALL(iRetCode, AOM_refresh (tParentFolder, true));

            /* Insert New Document revision to Phase Folder */
            ETO4_ITKCALL(iRetCode, FL_insert(tParentFolder, tItemRev, 999 ));

            ETO4_ITKCALL(iRetCode, AOM_save( tParentFolder ));

            ETO4_ITKCALL(iRetCode, AOM_refresh( tParentFolder, false ));
        }

        ETO4_MEM_TCFREE( pszItemType );
        ETO4_MEM_TCFREE( pszItemName );
        ETO4_MEM_TCFREE( ppszPrefItemAttrVal );
        ETO4_MEM_TCFREE( pszItemAttrValues );
    }

    ETO4_MEM_TCFREE( pszPrefItemTypeList );
    ETO4_MEM_TCFREE( ppszPrefVals );
    ETO4_MEM_TCFREE( ppszItemAttrNames );
    return iRetCode;
}

/**
 * Function creates custom Item of type specified as argument to this function and sets item revision attributes.
 *
 * @param[in]  pszItemType      Type of Item
 * @param[in]  pszItemName      Item Name
 * @param[in]  iPropCnt         Count of properties to be set
 * @param[in]  ppszProperty     List of property names
 * @param[in]  ppszPropertyVal  List of property values
 * @param[out] ptItem           Tag to New Item 
 * @param[out] ptItemRev        Tag to New Item Revision
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_create_item_type 
(
    char*  pszItemType,
    char*  pszItemName,
    int    iPropCnt,
    char** ppszProperty,
    char** ppszPropertyVal,
    tag_t* ptItem,
    tag_t* ptItemRev
)
{
    int   iRetCode    = ITK_ok;
    int   iMarkPoint  = 0;
    tag_t tNewstuffFL = NULLTAG;

    if(iPropCnt != 0 && pszItemType != NULL && pszItemName != NULL)
    {
        /* Define a markpoint for database transaction */
        ETO4_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));

        /* Create ETO4_Phase_Plan   */
        ETO4_ITKCALL( iRetCode, ITEM_create_item ( NULL, pszItemName, pszItemType, NULL, ptItem, ptItemRev));

        for(int iDx = 0; iDx < iPropCnt && iRetCode == ITK_ok; iDx++)
        {
            ETO4_ITKCALL(iRetCode, AOM_set_value_string( (*ptItem), ppszProperty[iDx], ppszPropertyVal[iDx]));

            /*  Save the object.  */
            ETO4_ITKCALL(iRetCode, AOM_save( *ptItem ));
        }

        ETO4_ITKCALL(iRetCode, ETO4_get_user_folder(ETO4_NEWSTUFF_FOLDER, &tNewstuffFL));

        ETO4_ITKCALL(iRetCode, FL_insert(tNewstuffFL, (*ptItem), 999));

        ETO4_ITKCALL(iRetCode, AOM_save(tNewstuffFL)); 

        /* Commit the information into the database. */
        ETO4_ITKCALL(iRetCode, POM_forget_markpoint(iMarkPoint));
    
        /* Call AOM_refresh to unlock the item  with paramater FALSE*/
        ETO4_ITKCALL(iRetCode, AOM_refresh(*ptItem, FALSE));

        /* Call AOM_refresh to unlock the item revision with paramater FALSE*/
        ETO4_ITKCALL(iRetCode, AOM_refresh(*ptItemRev, FALSE));

        if (iRetCode != ITK_ok)
        {
            logical bStateChanged = false;
        
            /* Do the rollback of the database. */
            POM_roll_to_markpoint(iMarkPoint, &bStateChanged);
        }  
    }

    return iRetCode;
}

/**
 * Function validates the Business Object that is pasted in the Phase Folder and Phase Sub Folder by comparing
 * it with the valid object list specified in the preference. If preference is not specified then by default any Object
 * can to be pasted in Phase Folder or Phase Sub Folder. 
 * 
* @note: If folder of type is 'ETO4_Phase_Sub_Folder' is pasted by user in any of the 'ETO4_Phase_Folder' type then
 *       function will also verify the contests of 'ETO4_Phase_Sub_Folder'. The content of this folder should match the list
 *       of Item revision type that is valid for 'ETO4_Phase_Folder' type on which the BMF extension is invoked.
 *                   
 * @param[in] ptContents      Tag to all the Objects in Folder
 * @param[in] iTotalContents  Number of Objects
 * @param[in] tFolder         Tag to Phase Plan Folder
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_validate_object_for_past_action_on_phase_folder
(
    tag_t* ptContents,
    int    iTotalContents,
    tag_t  tFolder
)
{
    int      iRetCode           = ITK_ok;
    int      iPrefValCnt        = 0;
    char*    pszFolderType      = NULL;
    char*    pszParentClass     = NULL;
    char**   ppszPrefVals       = NULL;

    if(iTotalContents > 0)
    {
        /* Get Folder type */
        ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(tFolder, &pszFolderType));

        /* Get parent Folder type */
        ETO4_ITKCALL(iRetCode, ETO4_obj_ask_parent_class( tFolder, &pszParentClass ) );

        /* Validating if Folder is of the type ETO4_Phase_Folder or ETO4_Phase_Sub_Folder  */
        if( tc_strcmp( pszParentClass, ETO4_CLASS_PHASE_FOLDER ) == 0 || tc_strcmp( pszFolderType, ETO4_CLASS_PHASE_SUB_FOLDER ) == 0 )
        {
            char*    pszObjType            = NULL;
            char*    pszSubPhaseFolderDesc = NULL;
            logical  bISValidType          = false;

            ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values( ETO4_PREF_PHASE_FOLDER_ALLOWED_OBJ_TYPES, &iPrefValCnt, &ppszPrefVals ));

            ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(ptContents[iTotalContents-1], &pszObjType));

            if(tc_strcmp( pszFolderType, ETO4_CLASS_PHASE_SUB_FOLDER ) == 0)
            {
                ETO4_ITKCALL(iRetCode, AOM_ask_value_string( tFolder, ETO4_ATTR_DESCRIPTION_ATTRIBUTE, &pszSubPhaseFolderDesc ));

                ETO4_ITKCALL(iRetCode, ETO4_verify_object_pasted_to_phase_folder(iPrefValCnt, ppszPrefVals, pszObjType, pszSubPhaseFolderDesc));
            }
            else
            {
                if(tc_strcmp( pszObjType, ETO4_CLASS_PHASE_SUB_FOLDER ) == 0)
                {    
                    int    iNoOfRef      = 0;
                    tag_t* ptReferences  = NULL;

                    ETO4_ITKCALL(iRetCode, FL_ask_references(ptContents[iTotalContents-1], FL_fsc_as_ordered, &iNoOfRef, &ptReferences));

                    for(int iDx = 0; iDx < iNoOfRef && iRetCode == ITK_ok; iDx++)
                    {
                        char* pszFLContentType = NULL;

                        ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(ptReferences[iDx], &pszFLContentType));

                        ETO4_ITKCALL(iRetCode, ETO4_verify_object_pasted_to_phase_folder(iPrefValCnt, ppszPrefVals, pszFLContentType, pszFolderType));

                        ETO4_MEM_TCFREE( pszFLContentType );
                    }

                    ETO4_MEM_TCFREE( ptReferences );
                }
                else
                {
                    ETO4_ITKCALL(iRetCode, ETO4_verify_object_pasted_to_phase_folder(iPrefValCnt, ppszPrefVals, pszObjType,	pszFolderType));
                }
            }

            ETO4_MEM_TCFREE( pszObjType );
            ETO4_MEM_TCFREE( pszSubPhaseFolderDesc );
        }
    }

    ETO4_MEM_TCFREE( pszFolderType );
    ETO4_MEM_TCFREE( pszParentClass );
    ETO4_MEM_TCFREE( ppszPrefVals );
    return iRetCode;
}

/**
 * Function validates if input object Type matches the valid list of allowed Object that can be pasted in Phase Folders.
 * Valid list of Item revision and their parent folder type is taken as input argument to this function.
 *                   
 * @param[in] iPrefValCnt    Preference count
 * @param[in] ppszPrefVals   Preference Value
 * @param[in] pszObjType     Type name of object that user is trying to past in Phase Folder
 * @param[in] pszFolderType  Phase folder type name
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_verify_object_pasted_to_phase_folder
(
    int    iPrefValCnt,
    char** ppszPrefVals,
    char*  pszObjType,
    char*  pszFolderType
)
{
    int     iRetCode      = ITK_ok;
    logical bISValidType  = false;

    for(int iDx = 0; iDx < iPrefValCnt && iRetCode == ITK_ok && bISValidType == false; iDx++)
    {		
        int    iFLIRTypeCnt      = 0;
        char** ppszFLIRTypeNames = NULL;

        ETO4_ITKCALL(iRetCode, ETO4_str_parse_string(ppszPrefVals[iDx], ETO4_STRING_COLON, &iFLIRTypeCnt, &ppszFLIRTypeNames));

        if(iFLIRTypeCnt > 1)
        {
            if(tc_strcmp(ppszFLIRTypeNames[0], pszFolderType) == 0)
            {
                char* pszIRType = NULL;

                for(int iJx = 0; iJx < (iFLIRTypeCnt-1) && iRetCode == ITK_ok && bISValidType == false; iJx++)
                {
                    if(iJx == 0)
                    {
                        ETO4_STRCPY(pszIRType, ppszFLIRTypeNames[iJx+1]);
                    }
                    else
                    {
                        ETO4_STRCAT(pszIRType, ETO4_STRING_SEMICOLON);
                        ETO4_STRCAT(pszIRType, ppszFLIRTypeNames[iJx+1]);
                    } 					

                    if(tc_strcmp(pszObjType, ppszFLIRTypeNames[iJx+1]) == 0)
                    {
                        bISValidType = true;
                    }
                }

                if(bISValidType == false)
                {
                    iRetCode = INVALID_OBJ_TYPE_PASTED_TO_FOLDER;
                    EMH_store_error_s1(EMH_severity_error, iRetCode, pszIRType);
                }

                ETO4_MEM_TCFREE( pszIRType );
            }
        }
        else
        {
            iRetCode = INCORRECT_PREFERENCE_VALUE;
            EMH_store_error_s1(EMH_severity_error, iRetCode, ETO4_PREF_PHASE_FOLDER_ALLOWED_OBJ_TYPES);
        }

        ETO4_MEM_TCFREE( ppszFLIRTypeNames );
    }

    if(bISValidType == false && iRetCode == ITK_ok)
    {
        iRetCode = INCORRECT_PREFERENCE_VALUE;
        EMH_store_error_s1(EMH_severity_error, iRetCode, ETO4_PREF_PHASE_FOLDER_ALLOWED_OBJ_TYPES);
    }

    return iRetCode;
}

/**
 * This function is responsible for creating the Sub Phase Folder structure recursively if nested folder structure
 * is specified in the preference. 
 * Note : 
 *        1. First Level folder structure are separated by ':'.
 *        2. Any information about the Sub Folder structure start with Name of the Parent Folder and is separated
 *           from rest of the information using semicolon ';'
 *           For e.g.
 *           "ETO4_P01_Req_Phase:Requirement Profile Documentation:Q-Gate 0 Documentation;Requirement Profile Documentation:
 *             Kick-Off Documentation:Concept Documentation;Q-Gate 0 Documentation:Prototype Documentation;Kick-Off Documentation:
 *             Qualification Documentation"        
 *
 * @param[in] pszParentFLName  Name of the Parent Folder                    
 * @param[in] pszParentFLDesc  Parent Folder Description
 * @param[in] tParentFL        Tag to Parent Folder
 * @param[in] iTempPrefValCnt  Preference Count
 * @param[in] ppszTempPrefVal  Preference Values
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_check_and_create_fl_sub_substruct
(
    char*   pszParentFLName,
    char*   pszParentFLDesc,
    tag_t   tParentFL,
    int     iTempPrefValCnt,
    char**  ppszTempPrefVal
)
{
    int iRetCode   = ITK_ok;

    for(int iDx = 0; iDx < iTempPrefValCnt && iRetCode == ITK_ok; iDx++)
    {
        int iLen = 0;

        iLen = (int) tc_strlen(pszParentFLName);

        if(tc_strncasecmp(pszParentFLName, ppszTempPrefVal[iDx], iLen) == 0)
        {
            int    iFolderCnt     = 0;
            tag_t  tFolder        = NULLTAG;
            char** ppszFolderType = NULL;

            ETO4_ITKCALL(iRetCode, ETO4_str_parse_string( ppszTempPrefVal[iDx], ETO4_STRING_COLON, &iFolderCnt, &ppszFolderType));

            if(iFolderCnt > 1)
            {
                for(int iJx = 1 ; iJx < iFolderCnt && iRetCode == ITK_ok; iJx++)
                {
                    ETO4_ITKCALL(iRetCode, ETO4_create_custom_folder( ETO4_CLASS_PHASE_SUB_FOLDER, ppszFolderType[iJx], pszParentFLDesc, 0, NULL,
                                                                                    NULL, &tFolder ));

                    ETO4_ITKCALL(iRetCode, ETO4_check_and_create_fl_sub_substruct(ppszFolderType[iJx], pszParentFLDesc, tFolder, iTempPrefValCnt, ppszTempPrefVal));

                    ETO4_ITKCALL(iRetCode, AOM_refresh (tParentFL, true));

                    ETO4_ITKCALL(iRetCode, FL_insert(tParentFL, tFolder, 999 ));

                    ETO4_ITKCALL(iRetCode, AOM_save( tParentFL ));

                    ETO4_ITKCALL(iRetCode, AOM_refresh( tParentFL, false ));
                }
            }

            ETO4_MEM_TCFREE(ppszFolderType);
        }
    }

    return iRetCode;
}

/**
 * This function applies propagation information specified in the preference 'ETO4_Propagation_Information'        
 * from the input Source Object to the list of input objects of valid type. List of valid Object types that
 * will receive propagated information are specified in the preference 'ETO4_Folder_Content_Propagate_Objects'.
 *
 * @note  : This function will be called recursively for those input object which are of 
 *          type 'ETO4_Phase_Sub_Folder'
 *
 * @param[in] pszSourceFLType  Source Object Type
 * @param[in] tSourceObj       Source Object from which Propagation Information needs to be carry forwarded                   
 * @param[in] iPropInfoTypCnt  Type count of objects which are valid for Propagating Information
 * @param[in] ppszPropInfoType Object type list which are valid for Propagating Information
 * @param[in] iObjCnt          Number of Object for applying Propagation Information
 * @param[in] ptObjects        Tags of Object for applying Propagation Information
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_validate_and_propagation_info
(
    char*  pszSourceFLType,
    tag_t  tSourceObj,
    int    iPropInfoTypCnt,
    char** ppszPropInfoType,
    int    iObjCnt,
    tag_t* ptObjects
)
{
    int     iRetCode            = ITK_ok;
    int     iPropagationInfoCnt = 0;   
    char**  ppszPropagationInfo = NULL;
    logical bReadPropPref       = false;

    for(int iZx = 0; iZx < iObjCnt && iRetCode == ITK_ok; iZx++)
    {
        char*  pszObjectType   = NULL;

        /* Get Folder type */
        ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(ptObjects[iZx], &pszObjectType));

        for(int iDx = 0; iDx < iPropInfoTypCnt && iRetCode == ITK_ok; iDx++)
        {
            if(tc_strcmp(pszObjectType, ppszPropInfoType[iDx]) == 0)
            {
                tag_t   tDestinationObj = NULLTAG;
                logical bIsTypeOfIR     = false;

                if(tc_strcmp(pszObjectType, ETO4_CLASS_PHASE_SUB_FOLDER) == 0)
                {
                    int    iNoOfRef     = 0;
                    tag_t* ptReferences = NULL;
    
                    ETO4_ITKCALL(iRetCode, FL_ask_references(ptObjects[iZx], FL_fsc_as_ordered, &iNoOfRef, &ptReferences));

                    if (iNoOfRef > 0 && iRetCode == ITK_ok)
                    {
                        /*  Apply Propagation Information to contents of  ETO4_Phase_Sub_Folder which is being pasted to 'ETO4_Project_Phase_Folder' */
                        ETO4_ITKCALL(iRetCode, ETO4_validate_and_propagation_info( pszSourceFLType, tSourceObj, iPropInfoTypCnt, ppszPropInfoType,
                                                                                    iNoOfRef, ptReferences ));
                    }

                    ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val(ptObjects[iZx], ETO4_ATTR_DESCRIPTION_ATTRIBUTE,
                                                                                       pszSourceFLType));
                    ETO4_MEM_TCFREE(ptReferences);
                }
              
                if(bReadPropPref == false && iRetCode == ITK_ok)
                {
                    ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values( ETO4_PREF_PROPAGATION_INFORMATION, &iPropagationInfoCnt, &ppszPropagationInfo ));
                    
                    bReadPropPref = true;
                }

                if(iPropagationInfoCnt > 0 && iRetCode == ITK_ok)
                {
                    ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(ptObjects[iZx], ETO4_CLASS_ITEMREVISION, &bIsTypeOfIR));

                    if(bIsTypeOfIR == true)
                    {
                        ETO4_ITKCALL(iRetCode, ITEM_ask_item_of_rev ( ptObjects[iZx], &tDestinationObj )); 
                    }
                    else
                    {
                        tDestinationObj = ptObjects[iZx];
                    }
                }

                for(int iJx = 0; iJx < iPropagationInfoCnt && iRetCode == ITK_ok; iJx++)
                {
                    if(tc_strcmp(ETO4_CONST_PROJECT, ppszPropagationInfo[iJx]) == 0)
                    {
                        int    iProjectCount = 0;
                        tag_t* ptProjects    = NULL;

                        /* Get the project assigned to the Source Object   */

                        ETO4_ITKCALL(iRetCode, AOM_ask_value_tags(tSourceObj, ETO4_ATTR_PROJECT_LIST, &iProjectCount, &ptProjects) );

                         /*  Assign Project to Object.  */
                        ETO4_ITKCALL(iRetCode, PROJ_assign_objects ( iProjectCount, ptProjects, 1, &tDestinationObj));

                        ETO4_MEM_TCFREE(ptProjects);
                    }
                    else
                    {
                        if(tc_strcmp(ETO4_CONST_LICENSE, ppszPropagationInfo[iJx]) == 0)
                        {
                            int    iLicCount = 0;
                            tag_t* ptLicense  = NULL;

                            ETO4_ITKCALL(iRetCode, AOM_ask_value_tags(tSourceObj, ETO4_ATTR_LICENSE_LIST, &iLicCount, &ptLicense) );

                            for(int iFx = 0; iFx < iLicCount && iRetCode == ITK_ok; iFx++)
                            {
                                ETO4_ITKCALL(iRetCode, ADA_add_license_object(ptLicense[iFx], tDestinationObj));
                            }

                            ETO4_MEM_TCFREE(ptLicense);
                        }
                        else
                        {
                            if(tc_strcmp(ETO4_CONST_IP_CLASSIFICATION, ppszPropagationInfo[iJx]) == 0)
                            {
                                char* pszIPClassific = NULL;

                                ETO4_ITKCALL(iRetCode, AOM_ask_value_string(tSourceObj, ETO4_ATTR_IP_CLASSIFICATION, &pszIPClassific) );

                                if(pszIPClassific != NULL)
                                {
                                    ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val(tDestinationObj, ETO4_ATTR_IP_CLASSIFICATION, pszIPClassific) );
                                }

                                ETO4_MEM_TCFREE( pszIPClassific );
                            }
                        }
                    }
                }

                break;
                
            }
        }

        ETO4_MEM_TCFREE(pszObjectType);
    }

    ETO4_MEM_TCFREE(ppszPropagationInfo);
    return iRetCode;
}

/**
 * Function is responsible for cloning the Folder structure for new 'ETO4_Phase_PlanRevision' from the revision based on which 
 * new ETO4_Phase_Plan Item it is created. Only Folders are created new during cloning process rest of the objects present in the
 * source folder are referenced in the new folder hierarchy. The source Item revision has folders associated with 'IMAN_reference'
 * and these folder may have various sub folder or other business object. Clone process replicated the same behavior for new folders.
 *
 * @param[in] tItemRev	Tag to item revision for which new cloned structure needs to be created                    
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_clone_folder_structure 
(
    tag_t tItemRev
)
{
    int     iRetCode      = ITK_ok;
    int     iIRCount      = 0;	
    tag_t*  ptItemRev     = NULL;
    
    ETO4_ITKCALL(iRetCode, ETO4_grm_get_related_obj(tItemRev, ETO4_IMAN_BASED_ON, ETO4_CLASS_ITEMREVISION, NULL, false, &ptItemRev, &iIRCount));

	if(iIRCount > 0)
	{
        int     iFolderCnt    = 0;
        int     iMarkPoint    = 0;
        tag_t*  ptFolder      = NULL;
	
        ETO4_ITKCALL(iRetCode, ETO4_grm_get_related_obj(ptItemRev[0], ETO4_IMAN_REFERENCE_RELATION, ETO4_CLASS_PHASE_FOLDER, NULL, false, &ptFolder, &iFolderCnt));

        if(iFolderCnt > 0)
        {
            /* Define a markpoint for database transaction */
            ETO4_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));
        }

	    for(int iJx = 0; iJx < iFolderCnt && iRetCode == ITK_ok; iJx++)
        {
            int    iNoOfRef      = 0;
            tag_t  tFolder       = NULLTAG;
            tag_t  tFLType       = NULLTAG;
            tag_t  tRelation     = NULLTAG;
            tag_t* ptReferences  = NULL;
            char*  pszTypeName   = NULL;

            ETO4_ITKCALL(iRetCode, TCTYPE_ask_object_type( ptFolder[iJx], &tFLType));

            ETO4_ITKCALL(iRetCode, TCTYPE_ask_name2( tFLType, &pszTypeName));

            ETO4_ITKCALL(iRetCode, ETO4_create_custom_folder(pszTypeName, pszTypeName, NULL, 0, NULL, NULL, &tFolder ));

            ETO4_ITKCALL(iRetCode, ETO4_grm_create_relation( tItemRev, tFolder, ETO4_IMAN_REFERENCE_RELATION, &tRelation ));

            ETO4_ITKCALL(iRetCode, FL_ask_references(ptFolder[iJx], FL_fsc_as_ordered, &iNoOfRef, &ptReferences));

            for(int iNx = 0; iNx < iNoOfRef && iRetCode == ITK_ok; iNx++)
            {
                logical bIsTypeOfFL  = false;
        
                ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(ptReferences[iNx], ETO4_CLASS_FOLDER, &bIsTypeOfFL));

                if(bIsTypeOfFL == false)
                {
                    ETO4_ITKCALL(iRetCode, ETO4_insert_content_to_folder(tFolder, ptReferences[iNx], 999 ));
                }
                else
                {
                    ETO4_ITKCALL(iRetCode, ETO4_construct_sub_structure(tFolder, ptReferences[iNx]));
                }
            }

            ETO4_MEM_TCFREE( pszTypeName );
            ETO4_MEM_TCFREE( ptReferences );
        }

        if(iFolderCnt > 0)
        {
            /* Commit the information into the database. */
            ETO4_ITKCALL(iRetCode, POM_forget_markpoint(iMarkPoint));
        
            if (iRetCode != ITK_ok)
            {
                logical bStateChanged = false;
        
                /* Do the rollback of the database. */
                POM_roll_to_markpoint(iMarkPoint, &bStateChanged);
            }  
        }

        ETO4_MEM_TCFREE( ptFolder );
    }

    ETO4_MEM_TCFREE( ptItemRev );
    return iRetCode;
}

/**
 * Function is responsible for cloning the Sub Folder structure recursively for new 'ETO4_Phase_PlanRevision' based on input source
 * folder.
 *
 * @param[in] tParentFolder  Tag of Parent Folder                    
 * @param[in] tSourceFolder  Tag of Source Folders
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_construct_sub_structure 
(
    tag_t tParentFolder,
    tag_t tSourceFolder
)
{
    int    iRetCode      = ITK_ok;
    int    iNoOfRef      = 0;
    tag_t  tFLType       = NULLTAG;
    tag_t  tRelation     = NULLTAG;	
    tag_t  tSubFL        = NULLTAG;
    tag_t* ptReferences  = NULL;
    char*  pszDec        = NULL;
    char*  pszFLName     = NULL;
    char*  pszTypeName   = NULL;

    ETO4_ITKCALL(iRetCode, AOM_ask_value_string(tSourceFolder, ETO4_ATTR_DESCRIPTION_ATTRIBUTE, &pszDec));

    ETO4_ITKCALL(iRetCode, AOM_ask_value_string(tSourceFolder, ETO4_ATTR_OBJECT_NAME_ATTRIBUTE, &pszFLName));

    ETO4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tSourceFolder, &tFLType));

    ETO4_ITKCALL(iRetCode, TCTYPE_ask_name2( tFLType, &pszTypeName));

    ETO4_ITKCALL(iRetCode, ETO4_create_custom_folder(pszTypeName, pszFLName, pszDec, 0, NULL, NULL, &tSubFL ));

    ETO4_ITKCALL(iRetCode, ETO4_insert_content_to_folder(tParentFolder, tSubFL, 999 ));

    ETO4_ITKCALL(iRetCode, FL_ask_references(tSourceFolder, FL_fsc_as_ordered, &iNoOfRef, &ptReferences));

    for(int iNx = 0; iNx < iNoOfRef && iRetCode == ITK_ok; iNx++)
    {
        logical bIsTypeOfFL  = false;

        ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(ptReferences[iNx], ETO4_CLASS_FOLDER, &bIsTypeOfFL));

        ETO4_ITKCALL(iRetCode, AOM_refresh (tSubFL, true));

        if(bIsTypeOfFL == false)
        {
            ETO4_ITKCALL(iRetCode, FL_insert(tSubFL, ptReferences[iNx], 999 ));

            ETO4_ITKCALL(iRetCode, AOM_save( tSubFL ));
        }
        else
        {
            ETO4_ITKCALL(iRetCode, ETO4_construct_sub_structure(tSubFL, ptReferences[iNx]));
        }

        ETO4_ITKCALL(iRetCode, AOM_refresh( tSubFL, false ));
    }

    ETO4_MEM_TCFREE( pszDec );
    ETO4_MEM_TCFREE( pszFLName );
    ETO4_MEM_TCFREE( pszTypeName );
    ETO4_MEM_TCFREE( ptReferences );
    return iRetCode;
}
