/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_grm_utilities.hxx

    Description: This File contains custom functions for GRM to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>


/**
 * This function returns a list and count of all relation tags and the secondary objects with a specified relation for specified secondary
 * object Type to the specified primary_object.
 *
 * e.g. Suppose 'ETO4_Project' is a type of 'Item'. Then object type for 'ETO4_Project' is 'ETO4_Project'
 *
 * @note This function returns the value depending on Secondary Object Type as well as tags of the relation object that binds secondary objects to primary objects
 * 
 * @param[in] tPrimaryObj          The object.
 * @param[in] pszRelationTypeName  Name of relation type. Can be NULL.
 * @param[in] pszSecondaryObjType  Type of the primary object.
 * @param[out] pptSecondaryObjects List of Primary object of specified type.
 * @param[out] piObjCnt            Primary Object count.
 * @param[out] pptRelation         List of tags of the relation object that binds primary and secondary objects
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int ETO4_grm_get_secondary_obj_and_rel_info
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    char*   pszSecondaryObjType,
    tag_t** pptSecondaryObjects,
    int*    piObjCnt,
    tag_t** pptRelation
)
{
    int    iRetCode                 = ITK_ok;
    int    iSecondaryCount          = 0;
    tag_t  tRelationType            = NULLTAG;
    char*  pszObjectType            = NULL;
    GRM_relation_t *ptSecondaryObjs = NULL;
    
    /* Initializing out parameter of this function. */
    (*piObjCnt) = 0;
    (*pptSecondaryObjects) = NULL;

    /* get the relation type tag e.g. "IMAN_specification"  */
    ETO4_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    
    ETO4_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, tRelationType, &iSecondaryCount, &ptSecondaryObjs));
                                       
    for(int iNx = 0; iNx < iSecondaryCount && iRetCode == ITK_ok; iNx++)
    {        
        ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(ptSecondaryObjs[iNx].secondary, &pszObjectType));
            
        if (tc_strcmp(pszObjectType, pszSecondaryObjType) == 0)
        {
            ETO4_UPDATE_TAG_ARRAY((*piObjCnt), (*pptSecondaryObjects), (ptSecondaryObjs[iNx].secondary));
            ETO4_ADD_TAG_TO_ARRAY((*piObjCnt), (*pptRelation), (ptSecondaryObjs[iNx].the_relation));
        }
    }

    ETO4_MEM_TCFREE(pszObjectType);
    ETO4_MEM_TCFREE(ptSecondaryObjs);
    return iRetCode;
}

/**
 * This function get's the object's related to input business object 'tObject'. Related objects are retrieved based on various 
 * input criteria such as:
 *
 *   <b> Relation Type between business objects </b>
 *   <b> Type of related object </b>
 *   <b> Related objects can be excluded based on status </b>
 * 
 * @note: This function is generic function and is capable of retrieving Primary and Secondary relation objects based on value
 *        of input arguments 'bGetPrimary' If argument 'pszRelationTypeName' is NULL then all related objects irrespective
 *        of relation type will be retrieved. If 'pszObjType' is NOT NULL then related objects of type 'pszObjType' will be 
 *        retrieved.
 *
 * @param[in]  tObject              Tag of Object
 * @param[in]  pszRelationTypeName  Relation Type. NULLTAG means all relation types.
 * @param[in]  pszObjType           Specific type of object which is related to input object 'tObject'.
 *                                  NULL means any type of related object.
 * @param[in]  pszExcludeStatus     Related objects with this status will be excluded from valid related objects list
 * @param[in]  bGetPrimary          'true' if Primary related objects needs to be retrieved from input objects else  
 *                                  secondary objects will be retrieved. 
 * @param[out] pptValidRelatedObj   List of valid related objects
 * @param[out] piRelatedObjCnt      Count of valid related objects.
 *
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int ETO4_grm_get_related_obj
(
    tag_t   tObject,
    char*   pszRelationTypeName,
    char*   pszObjType,
    char*   pszExcludeStatus,
    logical bGetPrimary,
    tag_t** pptValidRelatedObj,
    int*    piRelatedObjCnt
)
{
    int     iRetCode              = ITK_ok;
    int     iCount                = 0;
    int     iTempCnt              = 0;
    int     iRelatedObjCnt        = 0;
    int     iLoadedObjCnt         = 0;    
    tag_t   tRelationType         = NULLTAG;
    tag_t*  ptTempObj             = NULL;
    tag_t*  ptLoadedObj           = NULL;
    GRM_relation_t *ptRelatedObj  = NULL;
    
    /* Initializing out parameter of this function. */
    (*piRelatedObjCnt)    = 0;
    (*pptValidRelatedObj) = NULL;

    if(pszRelationTypeName != NULL)
    {
        /* Get the relation type tag e.g. "IMAN_specification"  */
        ETO4_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    }
    
    if(bGetPrimary == true)
    {
        ETO4_ITKCALL(iRetCode, GRM_list_primary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }
    else
    {
        ETO4_ITKCALL(iRetCode, GRM_list_secondary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }

    /* Related objects are deliberately collected in this loop so that they can be loaded using POM API in one shot */                 
    for(int iNx = 0; iNx < iRelatedObjCnt && iRetCode == ITK_ok; iNx++)
    {
        int     iActiveSeq = 0;
        tag_t   tObject    = NULLTAG;
        logical bIsValid   = false;
        logical bIsWSObj   = false;
        
        if(bGetPrimary == true)
        {
            tObject = ptRelatedObj[iNx].primary;
        }
        else
        {
            tObject = ptRelatedObj[iNx].secondary;
        }

        ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(tObject, ETO4_CLASS_WORKSPACEOBJECT, &bIsWSObj));

        if(iRetCode == ITK_ok)
        {
            if(bIsWSObj == true)
            {
                ETO4_ITKCALL(iRetCode, AOM_ask_value_int(tObject, ETO4_ATTR_ACTIVE_SEQ, &iActiveSeq));
            }
            else
            {
                iActiveSeq = 1;
            }
        }
    
        /* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0 && iRetCode == ITK_ok)
        {
            logical bBelongsToClass = false;

            if(pszObjType != NULL && tc_strlen(pszObjType) > 0)
            {
                ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(tObject, pszObjType, &bBelongsToClass));
            }
            else
            {
                bBelongsToClass = true;
            }

            if( bBelongsToClass ==  true && iRetCode == ITK_ok)
            {
                tag_t  tStatus  = NULLTAG;

                if(pszExcludeStatus != NULL && tc_strlen(pszExcludeStatus) > 0)
                {
                    ETO4_ITKCALL(iRetCode, ETO4_obj_ask_status_tag(tObject, pszExcludeStatus, &tStatus));
                }

                if(tStatus == NULLTAG && iRetCode == ITK_ok)
                {
                    ETO4_UPDATE_TAG_ARRAY(iCount, (*pptValidRelatedObj), tObject);
                }
            }
        }
    }
  
    if(iRetCode == ITK_ok)
    {
        (*piRelatedObjCnt) = iCount;
    }

    ETO4_MEM_TCFREE(ptLoadedObj);
    ETO4_MEM_TCFREE(ptTempObj);
    ETO4_MEM_TCFREE(ptRelatedObj);
    return iRetCode;
}

/**
 * This function returns a list of secondary objects of requested types which are attached to input primary object with any of the input list of relations.
 *
 * @note This function returns the value depending on secondary Object Type list.
 * 
 * @param[in]  tPrimaryObj         The object.
 * @param[in]  iRelCount           Count of Relation.  Can be 0.
 * @param[in]  ppszRelTypeList     Name of relation types. Can be NULL.
 * @param[in]  iSecObjCnt          Count of secondary object type
 * @param[in]  ppszSecObjTypeList  List of secondary object type.
 * @param[out] pptSecondaryObjects List of Primary object of specified type.
 * @param[out] piObjCnt            Primary Object count.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int ETO4_grm_get_sec_obj_attach_with_rel_type
(
    tag_t   tPrimaryObj,
    int     iRelCount,
    char**  ppszRelTypeList,
    int     iSecObjCnt,
    char**  ppszSecObjTypeList,
    tag_t** pptSecondaryObjects,
    int*    piObjCnt  
)
{
    int     iRetCode             = ITK_ok;
    int     iSecondaryCount      = 0;
    tag_t   tRelationType        = NULLTAG;
    GRM_relation_t* ptSecObjects = NULL;
    
    /* Initializing out parameter of this function. */
    (*piObjCnt) = 0;
    (*pptSecondaryObjects) = NULL;

    /* Get the relation type tag e.g. "IMAN_specification". If there is only one input relation then find the secondary objects
       with this specific relation else find all the secondary objects and validate the relation type from the input list.
    */
    if(iRelCount == 1 && ppszRelTypeList != NULL)
    {
        ETO4_ITKCALL(iRetCode, GRM_find_relation_type(ppszRelTypeList[0], &tRelationType));
    }
    
    ETO4_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, tRelationType, &iSecondaryCount, &ptSecObjects));
                                       
    for(int iZx = 0; iZx < iSecondaryCount && iRetCode == ITK_ok; iZx++)
    {
        char*   pszRelTypeName = NULL;
        logical bIsValidRel    = false;

        ETO4_ITKCALL(iRetCode, TCTYPE_ask_name2(ptSecObjects[iZx].relation_type , &pszRelTypeName));

        for(int iDx = 0; iDx < iRelCount && iRetCode == ITK_ok; iDx++)
        {
            if(tc_strcmp(pszRelTypeName, ppszRelTypeList[iDx]) == 0)
            {
                bIsValidRel = true;
                break;
            }
        }

        if(bIsValidRel == true && iRetCode == ITK_ok)
        {
            char* pszType = false;

            ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(ptSecObjects[iZx].secondary, &pszType));

            for(int iJx = 0; iJx < iSecObjCnt && iRetCode == ITK_ok; iJx++)
            {
                if (tc_strcmp(pszType, ppszSecObjTypeList[iJx]) == 0)
                {
                    ETO4_UPDATE_TAG_ARRAY((*piObjCnt), (*pptSecondaryObjects), (ptSecObjects[iZx].secondary));
                }
            }

            ETO4_MEM_TCFREE(pszType);
        }

        ETO4_MEM_TCFREE(pszRelTypeName);
    }

    ETO4_MEM_TCFREE(ptSecObjects);
    return iRetCode;
}


/**
 * This function creates specification relationship between primary and secondary object.
 * 
 * @param[in]  tPrimaryObj     Tag of the primary object.
 * @param[in]  tSecondaryObj   Tag of the secondary object.
 * @param[in]  pszRelationType Name of relation type.
 * @param[out] ptRelation      Tag to newly created relation type.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int ETO4_grm_create_relation
(
    tag_t  tPrimaryObj,
    tag_t  tSecondaryObj,
    char*  pszRelationType,
    tag_t* ptRelation
)
{
    int   iRetCode      = ITK_ok;
    tag_t tRelationType = NULLTAG;
    tag_t tNewRelation  = NULLTAG;

    /* Initializing the Out Parameter to this function. */
    *ptRelation = NULLTAG;

    /* Get relation type */
    ETO4_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationType, &tRelationType));

    if(tRelationType == NULLTAG)
        iRetCode = GRM_invalid_relation_type;
    if(tPrimaryObj == NULLTAG)
        iRetCode = GRM_invalid_primary_object;
    if(tSecondaryObj == NULLTAG)
        iRetCode = GRM_invalid_secondary_object;

     /*     Find relation to check if relation already exists   */
    ETO4_ITKCALL(iRetCode, GRM_find_relation(tPrimaryObj, tSecondaryObj, tRelationType, &tNewRelation));

    if(tNewRelation == NULLTAG)
    {
        ETO4_ITKCALL(iRetCode, GRM_create_relation(tPrimaryObj, tSecondaryObj, tRelationType, NULLTAG, &tNewRelation));

        if(tNewRelation != NULLTAG)
        {
            ETO4_ITKCALL(iRetCode, GRM_save_relation(tNewRelation));
        }
    }

    if(iRetCode == ITK_ok)
    {
        (*ptRelation) = tNewRelation;
    }
    
    return iRetCode;
}

