REM Make sure you installed gli_kill_installer.exe once before running this script!

:: net stop "Teamcenter Dispatcher Module V8000.3.0.20120307.00"
:: net stop "Teamcenter Dispatcher Scheduler V8000.3.0.20120307.00"
net stop "Teamcenter FSC Service FSC_DEUSRV01_Administrator"
net stop "TeamcenterServerManager_PRION"
kill java.exe
del /F/Q/S %temp%
del /F/Q/S C:\temp
if exist "%USERPROFILE%\FSCCache" rmdir/S/Q  %USERPROFILE%\FSCCache
if exist "%USERPROFILE%\FCCCache" rmdir/S/Q  %USERPROFILE%\FCCCache
if exist "%USERPROFILE%\Teamcenter" rmdir/S/Q  %USERPROFILE%\Teamcenter
:: net start "Teamcenter Dispatcher Module V8000.3.0.20120307.00"
:: net start "Teamcenter Dispatcher Scheduler V8000.3.0.20120307.000"
net start "Teamcenter FSC Service FSC_DEUSRV01_Administrator"
net start "TeamcenterServerManager_PRION"
