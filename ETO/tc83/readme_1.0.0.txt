**********************
*       Readme       *
**********************

See change_log for the updates with the latest release.
The installation process always installs the full release. No installation of a previous release is required.