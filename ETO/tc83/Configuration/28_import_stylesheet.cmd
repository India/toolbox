@echo off

set SUBFOLDER=%1
set STYLESHEET=%2

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import Teamcenter stylesheet in bulk
if %STYLESHEET%x==x (
    echo install_xml_stylesheet_datasets -u=infodba -p=%DBAPASS_TMP% -g=dba -input=Stylesheets\%SUBFOLDER%\import_stylesheet.txt -filepath=Stylesheets\%SUBFOLDER% -replace
    echo.
    install_xml_stylesheet_datasets -u=infodba -p=%DBAPASS% -g=dba -input=Stylesheets\%SUBFOLDER%\import_stylesheet.txt -filepath=Stylesheets\%SUBFOLDER% -replace
)
REM goto finish after importing with install_xml_stylesheet_datasets
if %STYLESHEET%x==x goto :EOF

REM Import Teamcenter stylesheet by stylesheet
if exist "Stylesheets\%SUBFOLDER%\%STYLESHEET%.xml" (
    echo -----------------------------------------------------------------------
    echo import_file -u=infodba -p=%DBAPASS_TMP% -g=dba -f="Stylesheets\%SUBFOLDER%\%STYLESHEET%.xml" -d="%STYLESHEET%.xml" -type=XMLRenderingStylesheet -ref=XMLRendering -de=r -vb
    echo.
    import_file -u=infodba -p=%DBAPASS% -g=dba -f="Stylesheets\%SUBFOLDER%\%STYLESHEET%.xml" -d="%STYLESHEET%.xml" -type=XMLRenderingStylesheet -ref=XMLRendering -de=r -vb
    echo -----------------------------------------------------------------------
)

if not exist "Stylesheets\%SUBFOLDER%\%STYLESHEET%.xml" echo ERROR: File "Stylesheets\%SUBFOLDER%\%STYLESHEET%.xml" does not exist.
echo.

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
