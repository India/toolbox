/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_extension_arguments.hxx

    Description: This File contains the functions that performs operations on BMIDE Extensions

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <eto4_lib_library.hxx>
#include <eto4_lib_const.hxx>
#include <eto4_extension_arguments.hxx>
#ifdef __cplusplus
}
#endif

/**
 * Function extracting the user argument value from the message.
 *
 * @param[in] pmMessage          Received message
 * @param[in] pszArgumentName    Name of the argument for which value is required
 * @param[in] ppszBMFArgVal      Value of argument passed through extension
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_bmf_get_arg_val
(
    METHOD_message_t *pmMessage, 
    char*             pszArgumentName,
    char**            ppszBMFArgVal
)
{

    int iParamCount        = 0;
    int iRetCode           = ITK_ok;
    BMF_extension_arguments_t *pBMFInput_args    = NULL;

    /* Initializing the Out Parameter to this function. */
    (*ppszBMFArgVal) = NULL;

    /* Extract the user arguments from the message */
    ETO4_ITKCALL(iRetCode, BMF_get_user_params( pmMessage, &iParamCount,
                                                &pBMFInput_args )); 
        
    if( iRetCode == ITK_ok && iParamCount > 0 ) 
    {    
        logical bFoundArg    = false;

        for( int idx =0; (idx < iParamCount) && (bFoundArg == false) ; idx++ )
        {
            if( tc_strcmp(pszArgumentName, pBMFInput_args[idx].paramName) == 0 )
            {
                
                (*ppszBMFArgVal) = (char*)MEM_alloc( (((int)tc_strlen(pBMFInput_args[idx].arg_val.str_value)) +1) * sizeof(char));
                
                tc_strcpy((*ppszBMFArgVal), pBMFInput_args[idx].arg_val.str_value);
                
                bFoundArg = true;
                  
            }
        }
        
        ETO4_MEM_TCFREE(pBMFInput_args );
    }

   
    return iRetCode;
}

/**
 * Function extracting the user argument value from the message.
 *
 * @param[in] pmMessage           Received message
 * @param[in] iArgCount           No of Argument count
 * @param[in] ppszArgNamesList    Argument List for which values are required
 * @param[out] pppszBMFArgValList Values of list of arguments passed through extension
 * @param[out] piArgValCount      No of Argument value count
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */

int ETO4_bmf_get_arg_val_list
(
    METHOD_message_t *pmMessage, 
    int              iArgCount,
    char**           ppszArgNamesList,    
    char***          pppszBMFArgValList,
    int*             piArgValCount
)
{

    int iParamCount        = 0;
    int iRetCode           = ITK_ok;
    int iArgValCount       = 0;
    BMF_extension_arguments_t *pBMFInput_args    = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*pppszBMFArgValList) = NULL;
    (*piArgValCount)      = 0;


    /* Extract the user arguments from the message  */
    ETO4_ITKCALL( iRetCode, BMF_get_user_params( pmMessage, &iParamCount,
                                                 &pBMFInput_args ) ); 
    
    if( iRetCode == ITK_ok && iParamCount > 0 ) 
    {         
        
      for( int idx =0; idx < iParamCount ; idx++ )
      {
          logical bFound = false;

          for( int idxArgList = 0; (idxArgList < iArgCount) && (bFound == false ); idxArgList++ )
          {

              if( tc_strcmp(ppszArgNamesList[idxArgList], pBMFInput_args[idx].paramName) == 0 )
              {

                    iArgValCount++;
                    
                    /* Creating Array of Arguments. */
                    (*pppszBMFArgValList) = (char**)MEM_realloc(*pppszBMFArgValList, iArgValCount * sizeof(char*));

                    /* Storing Argument Values. */
                    (*pppszBMFArgValList)[iArgValCount-1] = (char*)MEM_alloc(
                                                                (((int)tc_strlen(pBMFInput_args[idx].arg_val.str_value))+ 1) * sizeof(char));

                    tc_strcpy((*pppszBMFArgValList)[iArgValCount-1], pBMFInput_args[idx].arg_val.str_value);

                    bFound = true;
                
                  
              }
          }
      }

      *piArgValCount = iArgValCount;

      ETO4_MEM_TCFREE(pBMFInput_args );
         
    }

    return iRetCode;
}





