/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_lib_object_utilities.cxx

    Description: This File contains custom functions for POM classes to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <eto4_lib_library.hxx>
#include <eto4_lib_const.hxx>
#include <eto4_errors.hxx>
#ifdef __cplusplus
}
#endif

/**
 * This function gets the name of type for the input business
 * objet.
 * e.g. Consider Item type 'P_KatalogteilRevision'. If we pass tag to
 *      an instance of 'P_KatalogteilRevision' this function will return string
 *      'P_KatalogteilRevision' and not 'ItemRevision'. Consider Dataset of type 'catia'.
 *      If we pass tag to an instance of 'catia' this function will
 *      return 'catia' and not 'Dataset'.
 *
 * @param[in]  tObject       Business Object tag
 * @param[out] ppszObjType   Object type
 *
 * @return ITK_ok is successful else ITK return code.
 */

int ETO4_obj_ask_type
(
    tag_t  tObject,
    char** ppszObjType
)
{
    int     iRetCode            = ITK_ok;
    tag_t   tObjectType         = NULLTAG;
    char    szObjectType[TCTYPE_name_size_c+1] = {""};

    /* Initializing the Out Parameter to this function. */
    (*ppszObjType) = NULL;
    
    ETO4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject,&tObjectType));
    
    ETO4_ITKCALL(iRetCode, TCTYPE_ask_name(tObjectType, szObjectType));
    
    if(iRetCode == ITK_ok)
    {
        *ppszObjType = (char*) MEM_alloc(sizeof(char) * (((int)tc_strlen(szObjectType)) +1));
        if(*ppszObjType == NULL)
        {
           /*Store error into error stack */
            iRetCode = ERROR_ALLOCATING_MEMORY;
            EMH_store_error(EMH_severity_error, iRetCode);
            ETO4_err_write_to_logfile("MEM_alloc", iRetCode,__FILE__, __LINE__);            
        }
        else
        {
            tc_strcpy(*ppszObjType, szObjectType);
        }

    }
    
    return iRetCode;
}

/**
 * This function gets the Parent Class of the input business objet.
 *
 * e.g. Consider Item type 'P_Katalogteil'. If we pass tag to an instance of 'P_Katalogteil' this function will return string
 *      'Item' and not 'P_Katalogteil'. Consider Dataset of type 'catia'.If we pass tag to an instance of 
 *      'Dataset'. 'Email' is a of type 'Document' and 'Document' is of type 'Item'. If we pass  tag to an instance of 'Email'
*        this function will return string 'Document'  not 'Item' or 'Email.
 *
 * @param[in]  tObject               Business Object tag
 * @param[out] ppszObjParentTypeName Object Class Name
 * 
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_obj_ask_parent_class
(
    tag_t       tObject,
    char**      ppszObjParentTypeName
)
{
    int     iRetCode            = ITK_ok;
    tag_t   tObjectType         = NULLTAG;
    tag_t   tParentType         = NULLTAG;
    char    szClassName[TCTYPE_class_name_size_c+1] = {""};


    /* Initializing the Out Parameter to this function. */
    (*ppszObjParentTypeName) = NULL;

    ETO4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tObjectType));

    ETO4_ITKCALL(iRetCode, TCTYPE_ask_parent_type(tObjectType, &tParentType));
        
    ETO4_ITKCALL(iRetCode, TCTYPE_ask_class_name(tParentType, szClassName));
    
       
    if(iRetCode == ITK_ok)
    {
        *ppszObjParentTypeName = (char*) MEM_alloc(sizeof(char) * (((int)tc_strlen(szClassName)) +1));
        if(*ppszObjParentTypeName == NULL)
        {
           /*Store error into error stack */
            iRetCode = ERROR_ALLOCATING_MEMORY;
            EMH_store_error(EMH_severity_error, iRetCode);
            ETO4_err_write_to_logfile("MEM_alloc", iRetCode,__FILE__, __LINE__);
            
        }
        else
        {
            tc_strcpy((*ppszObjParentTypeName), szClassName);
        }

    }
    
    return iRetCode;
}

/**
 * Set the string attribute value.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] pszPropVal      Value of the property to be set
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    ETO4_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute string value.
     */    
    ETO4_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszPropertyName, pszPropVal));

    /*  Save the object.  */
    ETO4_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     ETO4_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int ETO4_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
	tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    ETO4_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    ETO4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    ETO4_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}

/**
 * Function to initiate workflow with provided target object
 *
 * @param[in]  pszWorkflowTemplate  Workflow template name
 * @param[in]  tTargetObject        Target object
 * @return ITK_ok if no errors occurred, else ITK error code
 */
extern int ETO4_initiate_workflow
(
    char* pszWorkflowTemplate, 
    tag_t tTargetObject
)
{
    int iRetCode (ITK_ok);
    tag_t tProcessTemplate = NULLTAG;
    tag_t tNewProcess      = NULLTAG;
    int   iAttType         = EPM_target_attachment;
    char* pszProcessName = NULL;

    ETO4_ITKCALL(iRetCode, EPM_find_process_template( pszWorkflowTemplate, &tProcessTemplate ) );

    if( tProcessTemplate != NULLTAG )
    {
        ETO4_ITKCALL(iRetCode, WSOM_ask_object_id_string(tTargetObject, &pszProcessName));
        ETO4_ITKCALL(iRetCode, EPM_create_process(pszProcessName, "", tProcessTemplate, 1, 
            &tTargetObject, &iAttType, &tNewProcess));
        
        ETO4_MEM_TCFREE( pszProcessName );
    }

    return (iRetCode);
}

/**
 * Function sets the value of the given list of string attributes from the given object with given class
 *
 * @param[in] tObject 		 Object whose attributes are to be set.
 * @param[in] pszClassName 	 The name of class whose object is to be created.
 * @param[in] iAttrCount 	 Number of attributes to be set.
 * @param[in] ppszAttrNames  List of attribute names
 * @param[in] ppszAttrValues List of attribute values
 *
 * @note The number of names and values in the string lists must be same and equal to iAttrCount
 * @return ITK_ok is successful else ITK return code. 
 */
int ETO4_obj_pom_set_multiple_str_attr_without_saving
(
	tag_t   tObject,
    char*   pszClassName,
    int     iAttrCount,
    char**  ppszAttrNames,
    char**  ppszAttrValues
)
{
    int iRetCode = ITK_ok;
 
    /* Now fill the instance with values for each field. */
    for(int iDx = 0; iDx < iAttrCount && iRetCode == ITK_ok; iDx++)
    {
        tag_t tAttr  = NULLTAG;
        
        ETO4_ITKCALL(iRetCode, POM_attr_id_of_attr( ppszAttrNames[iDx], pszClassName, &tAttr));

        ETO4_ITKCALL(iRetCode, POM_set_attr_string( 1, &tObject, tAttr, ppszAttrValues[iDx]));
    }
    
    return iRetCode;
}



