/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_lib_string_utilities.cxx

    Description: This File contains functions for manipulating string. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif
#include <eto4_lib_library.hxx>
#include <eto4_lib_const.hxx>
#include <eto4_errors.hxx>
#ifdef __cplusplus
}
#endif

/**
 * This function returns the substring when provided with start and end position on the 
 * the input string. Substring is the character sequence that starts at character position 
 * 'iStrStartPosition' and ends at character position 'iStrEndPosition'
 *
 * For example If 'pszInputString' = 'Application'
 *                'iStrStartPosition' = 4
 *                'iStrEndPosition'   = 9
 *             then Output Will be: 'licati'
 *
 * Function will copy the characters starting 4th character to the 9th character of
 * input string.
 * 
 * @param[in]  pszInputString    Actual input string
 * @param[in]  iStrStartPosition Position of a character in the current string to be
 *                               used as starting character for the substring.
 * @param[in]  iStrEndPosition   Position of a character in the current string to be
 *                               used as last character for the substring.
 *
 * @param[out] ppszCopyedStr     Copied string.
 *
 * @retval INVALID_INPUT_TO_FUNCTION if input not correct
 * @retval ITK_ok on successful execution
 */
int ETO4_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
)
{
    int  iRetCode    = ITK_ok;
    int  iStrLen     = 0;

    /* Initializing the Out Parameter to this function. */
    (*ppszCopyedStr) = NULL;

    iStrLen = (int) tc_strlen(pszInputString);
    
    if ((pszInputString != NULL) && (iStrStartPosition >= 0) && (iStrEndPosition <=  iStrLen) && 
        (iStrStartPosition <= iStrEndPosition))
    {
        if(tc_strcmp(pszInputString, "") != 0)
        {
            int idx        = 0;
            int iNewStrLen = 0;

            iNewStrLen = iStrEndPosition - iStrStartPosition + 2;

            (*ppszCopyedStr) = (char*) MEM_alloc(sizeof(char*) * iNewStrLen);

            while(iStrStartPosition <= iStrEndPosition)
            {
                (*ppszCopyedStr)[idx] = pszInputString[iStrStartPosition-1];
                idx++;
                iStrStartPosition++;
            }

            (*ppszCopyedStr)[idx] = '\0';
        }
    }
    else
    {
        iRetCode = INVALID_INPUT_TO_FUNCTION;
    }

    return iRetCode;
}


/**
 * Function checks if given string exists in string of array or not 
 *
 * @param[in]  pszStrVal     String to be looked into string array
 * @param[in]  iTotalStrVals String Array Size 
 * @param[in]  pppszStrVals  String Array
 *
 * @return ITK_ok if string is found in the array else return 1
 */
int ETO4_contains_string_value
(
    char*  pszStrVal, 
    int    iTotalStrVals, 
    char** pppszStrVals
)
{
    int iRetCode (ITK_ok);

    if( iTotalStrVals > 0 )
    {
         for ( int inx = 0; inx < iTotalStrVals; inx++ )
         {
             if(tc_strcmp( pszStrVal, pppszStrVals[ inx ]) == 0)
             {
                return iRetCode;
             }
         } 
        
    }

    return 1;
}


/**
 * Function tokenize the given string with provided delimited
 *
 * @param[in]  pszInputStr      String to be tokenized
 * @param[in]  pszDelim         Delimiter
 * @param[out] iTokenCnt        Total number of tokens
 * @param[out] pppszAllTokens   String array of tokens
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
void  ETO4_get_string_tokens
(
    const char* pszInputStr,
    char*       pszDelim, 
    int*        piTokenCnt,
    char***     pppszAllTokens
)
{
    char* pszTempStr = NULL;
    char* pszToken = NULL;

    ETO4_STRCPY(pszTempStr, pszInputStr);
   
    if(NULL != (pszToken = tc_strtok(pszTempStr, pszDelim)))
    {
        ETO4_UPDATE_STRING_ARRAY(piTokenCnt, pppszAllTokens, pszToken);
    }
    while(NULL != (pszToken = tc_strtok(NULL, pszDelim)))
    {
        ETO4_UPDATE_STRING_ARRAY(piTokenCnt,pppszAllTokens, pszToken);
    }

    ETO4_MEM_TCFREE(pszTempStr);
}

/**
 * Function split the input string based on very first occurrence of input Delimiter character. This function will
 * return the remaining string after the Delimiter. For e.g. 
 * If input string is "ENT_01-Test_Data-1" and then the output string will be "Test_Data-1"
 *
 * @param[in]  pszInputStr    String to be checked for Delimiter occurrence
 * @param[in]  szDelim        Delimiter
 * @param[out] ppszOutputStr  String array of tokens
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
void ETO4_get_str_after_delimiter
(
    const char*   pszInputStr,
    const char    szDelim, 
    char**        ppszAllTokens
)
{
    pszInputStr = STRNG_find_first_char(pszInputStr, szDelim);
   
	if(pszInputStr != NULL)
	{
		(*ppszAllTokens) = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(pszInputStr))));

		tc_strcpy((*ppszAllTokens), (pszInputStr+1));
	}
}


