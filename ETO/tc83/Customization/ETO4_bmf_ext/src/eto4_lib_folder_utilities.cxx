/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_lib_folder_utilities.hxx

    Description: This File contains custom Folder Manager (FL) functions to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <eto4_lib_library.hxx>
#include <eto4_lib_const.hxx>
#include <eto4_errors.hxx>
#ifdef __cplusplus
}
#endif

/**
 * Function gets user Newstuff or Home folder depending on the input argument to the function
 *
 * @param[in]  pszUserFLType    Name of the folder. "newstuff" or "home"
 * @param[out] ptFolder         Tag to the Folder of input type
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_get_user_folder
(
    char*    pszUserFLType,
    tag_t*   ptFolder
)
{
    int    iRetCode              = ITK_ok;
    char*  pszCurrentUserName    = NULL;
    tag_t  tCurrentUser          = NULLTAG;
    tag_t  tUsrFolder            = NULLTAG;

    ETO4_ITKCALL(iRetCode, POM_get_user(&pszCurrentUserName, &tCurrentUser));

    if (iRetCode == ITK_ok && tCurrentUser != NULLTAG)
    {
        if ( tc_strcasecmp(pszUserFLType, ETO4_NEWSTUFF_FOLDER) == 0 )
        {
           if (iRetCode == ITK_ok)
           {          
                ETO4_ITKCALL(iRetCode, SA_ask_user_newstuff_folder(tCurrentUser, &tUsrFolder));
           }
        }          

        if ( tc_strcasecmp(pszUserFLType, ETO4_HOME_FOLDER) == 0 )
        {
               if (iRetCode == ITK_ok)
               {          
                    ETO4_ITKCALL(iRetCode, SA_ask_user_home_folder(tCurrentUser, &tUsrFolder));
               }
        }
    }

    if (iRetCode == ITK_ok)
    {
        *ptFolder = tUsrFolder;
    }

    ETO4_MEM_TCFREE(pszCurrentUserName);
    return iRetCode;
}

/**
 * Function creates custom folder and set attribute values as specified by input argument to this function.
 *
 * @param[in]  pszClassName  Class name of custom folder. e.g. ETO4_P01_Req_Phase
 * @param[in]  pszFolderName Value for attribute 'object_name'
 * @param[in]  pszFolderDesc Description to be set for new instance of folder
 * @param[in]  iAttrCount    Attribute Count to be set excluding Folder Name & Description.
 * @param[in]  ppszAttrNames Attribute Names
 * @param[in]  pszAttrValues Attribute Names
 * @param[out] ptFolder      Tag to instance of Folder.
 *
 * @note   Argument 'ppszAttrNames' should not contain attribute object_name and object_desc
 *         because these attribute are supplied separately to this function.
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_create_custom_folder
(
    char*    pszClassName,
    char*    pszFolderName,
    char*    pszFolderDesc,
    int      iAttrCount,
    char**   ppszAttrNames,
    char**   pszAttrValues,
    tag_t*   ptFolder
)
{
    int    iRetCode   = ITK_ok;
    tag_t  tClassId   = NULLTAG;

    /*  Get the tag for the class.  */
    ETO4_ITKCALL(iRetCode, POM_class_id_of_class( pszClassName, &tClassId ));

    /* Create an instance of the class. */
    ETO4_ITKCALL(iRetCode, POM_create_instance( tClassId, ptFolder));

    if(iAttrCount > 0)
    {
        ETO4_ITKCALL(iRetCode, ETO4_obj_pom_set_multiple_str_attr_without_saving( (*ptFolder), pszClassName, iAttrCount,
                                                                                  ppszAttrNames, pszAttrValues ) );
    }
	
	ETO4_ITKCALL(iRetCode, FL_initialize( (*ptFolder), pszFolderName, pszFolderDesc));

    ETO4_ITKCALL(iRetCode, POM_save_instances( 1, ptFolder, false));

    return iRetCode;
}

/**
 * Function Inserts an object reference into a folder at a specified index position.
 *
 * @param[in]  tFolder   Tag to Folder
 * @param[in]  tObject   Object Tag which needs to be inserted in the Folder
 * @param[in]  iPosition It is zero based index, and that the input is larger than the number of objects in the folder,
 *                       or using 999 to indicate the last position.
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_insert_content_to_folder
(
    tag_t tFolder,
	tag_t tObject,
	int   iPosition
)
{
    int    iRetCode   = ITK_ok;

	ETO4_ITKCALL(iRetCode, AOM_refresh (tFolder, true));

	ETO4_ITKCALL(iRetCode, FL_insert(tFolder, tObject, iPosition ));

	ETO4_ITKCALL(iRetCode, AOM_save( tFolder ));

	ETO4_ITKCALL(iRetCode, AOM_refresh( tFolder, false ));

    return iRetCode;
}
