/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_register_custom_handlers.cxx

    Description:  This file contains function registering custom handlers to be used by the workflow.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <eto4_library.hxx>
#include <eto4_register_callbacks.hxx>
#include <eto4_rule_handlers.hxx>
#include <eto4_action_handlers.hxx>
#include <eto4_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Registers custom handlers to be used by the workflow.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
extern DLLAPI int ETO4_register_custom_handlers(int* piDecision, va_list args)
{
    int iRetCode = ITK_ok;

    TC_write_syslog( "Registering custom handlers ...\n");
    *piDecision  = ALL_CUSTOMIZATIONS;

        
    iRetCode = ETO4_register_rule_handlers();
    if(iRetCode != ITK_ok)
    {
        printf("Unable to register: ETO4_register_rule_handlers\n");
    }
    
    iRetCode = ETO4_register_action_handlers();
    if(iRetCode != ITK_ok)
    {
        printf("Unable to register: ETO4_register_action_handlers\n");
    }
    
    return iRetCode;
}
