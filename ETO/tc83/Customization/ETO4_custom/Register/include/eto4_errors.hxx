/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_errors.hxx

    Description:  Header File for containing macros for custom errors

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifndef ETO4_ERRORS_HXX
#define ETO4_ERRORS_HXX

#include <tc/emh_const.h>

#define ERROR_ALLOCATING_MEMORY                                     (EMH_USER_error_base + 401)
#define ERROR_CODE_NOT_STORED_ON_STACK                              (EMH_USER_error_base + 402)
#define INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME                   (EMH_USER_error_base + 403)
#define ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND                       (EMH_USER_error_base + 404)
#define UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES                     (EMH_USER_error_base + 405)
#define INVALID_INPUT_TO_FUNCTION                                   (EMH_USER_error_base + 406)

#endif


