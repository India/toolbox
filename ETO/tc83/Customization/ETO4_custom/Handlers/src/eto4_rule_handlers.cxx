/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_rule_handlers.cxx

    Description: This file contains function that registers all Rule Handlers for ETO custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <eto4_library.hxx>
#include <eto4_rule_handlers.hxx>

#ifdef __cplusplus
}
#endif


/**
 * Registers the rule handlers for workflows.
 * 
 * @return Status of registration of rule handlers.
 */
int ETO4_register_rule_handlers
(
    void
)
{
    int iRetCode      = ITK_ok;
    
    //TC_write_syslog("In ETO Register Rule Handlers Operation\n");   
   
    return iRetCode;
    
}


