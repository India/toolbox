/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_action_handlerss.cxx

    Description: This file contains function that registers all Action Handlers for ETO custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <eto4_action_handlers.hxx>
#include <eto4_library.hxx>
#include <eto4_security_info.hxx>

#ifdef __cplusplus
}
#endif


/**
 * Functon to register the action handlers to be used in the workflows.
 *
 * @return Status of registration of action handlers. 
 */
int ETO4_register_action_handlers
(
    void
)
{
    int iRetCode = ITK_ok;
    
    TC_write_syslog("In ETO Register Action Handlers Operation\n");
    
    ETO4_ITKCALL( iRetCode, EPM_register_action_handler( "ETO4-propagate-security-info", "", ETO4_propagate_security_info));

        
    return iRetCode;
}
