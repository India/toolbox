/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_grm_utilities.hxx

    Description: This File contains custom functions for GRM to perform generic operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>

#ifdef __cplusplus
}
#endif


/**
 * This function returns a list and count of all secondary objects attached with a specified
 * relation to the specified primary_object.
 *
 * @param[in] tPrimaryObj              The object.
 * @param[in] pszRelationTypeName      Name of relation type. Can be NULL.
 * @param[out] pptSecondaryObjects     List of Primary object
 * @param[out] piObjectCount           Primary Object count.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int ETO4_grm_get_secondary_obj
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
)
{

    int     iRetCode                        = ITK_ok;
    int     iCount                          = 0;
    int     iIterator                       = 0;
    int     iSecondaryCount                 = 0;
    tag_t   tRelationType                   = NULLTAG;
    char*   pszObjectType                   = NULL;
    GRM_relation_t *ptSecondaryObjects      = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount) = 0;
    (*pptSecondaryObjects) = NULL;

    /* get the relation type tag e.g. "IMAN_specification"  */
    ETO4_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    
    ETO4_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, tRelationType, 
                                                       &iSecondaryCount, &ptSecondaryObjects));
                                       
    for(iIterator = 0; iIterator < iSecondaryCount && iRetCode == ITK_ok; iIterator++)
    {
        int  iActiveSeq   = 0;
      
        ETO4_ITKCALL(iRetCode, AOM_ask_value_int(ptSecondaryObjects[iIterator].secondary, ETO4_ATTR_ACTIVE_SEQ, &iActiveSeq));
	
		/* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0)
        {
            iCount++;
            (*pptSecondaryObjects) = (tag_t*)MEM_realloc ( (*pptSecondaryObjects) , (sizeof(tag_t) * iCount) );
            (*pptSecondaryObjects)[iCount -1] = ptSecondaryObjects[iIterator].secondary;
        }
    }

    if(iCount == 0)
    {
        TC_write_syslog("No Secondary Object associated with the relation to the object.\n");
    }
    else
    {
        (*piObjectCount ) = iCount;
    }

    ETO4_MEM_TCFREE(pszObjectType);
    ETO4_MEM_TCFREE(ptSecondaryObjects);
    return iRetCode;
}