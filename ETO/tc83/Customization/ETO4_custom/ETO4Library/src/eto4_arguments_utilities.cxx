/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_arguments_utilities.cxx

    Description:  This File contains functions for string parsing operations and functions for retrieving arguments passed in workflow action or rule handlers

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <eto4_library.hxx>
#include <eto4_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Parses a string containing values seperated by a given separator
 * into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in] pszList          Input list to be parsed
 * @param[in] cSep             Separator character
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by ETO4_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 * @retval INCORRECT_INPUT If list or seprator is Null.
 * @retval MEMORY_ALLOCATION_FAILED If cannot allocate memory.
 */
int ETO4_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
)
{
    int        iRetCode    = ITK_ok;
    int        iCounter    = 0;
    char*      pszToken    = "";
    char*      pszTempList = NULL;

    /* Convert separator (char) to char* - strtok(char* input, Char* sep) */
    char pszSeparator[ETO4_SEPARATOR_LENGTH+1];
    pszSeparator[0] = cSep;
    pszSeparator[1] = '\0';

    /* Validate input */
    if((pszList == NULL) || (cSep == '\0'))
    {
        TC_write_syslog( "ERROR: Missing input data:List passed or seprator is null\n");
    }
    else
    {

        /* Allocating memory to pszTempList */
        pszTempList  = (char*) MEM_alloc(sizeof(char)*((int)tc_strlen(pszList)+1));
        if (pszTempList == NULL)
        {
            TC_write_syslog( "ERROR: Cannot allocate memory\n");
        }
        else
        {
            /* Copying the input string to Null terminated string */
            tc_strcpy(pszTempList, pszList);

            /* Get the first token */
            pszToken = tc_strtok( pszTempList,pszSeparator );

            *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
            /* Tokenize the input string */
            while( pszToken != NULL )
            {


                /* Allocate memory for each string pointer */
                (*pppszValuelist)[iCounter] = (char*) MEM_alloc(sizeof(char)*(((int)tc_strlen(pszToken))+1));
                
                if ((*pppszValuelist)[iCounter] == NULL)
                {
                    /*Store error into error stack */
                    iRetCode = ERROR_ALLOCATING_MEMORY;
                    EMH_store_error(EMH_severity_error, iRetCode);
                    ETO4_err_write_to_logfile("MEM_alloc", iRetCode,  __FILE__, __LINE__);
                    break;
                }
                else
                {
                    /* Copy the token to the output string pointer */
                    tc_strcpy((*pppszValuelist)[iCounter],pszToken);

                    /* Count the number of elements in the string */
                    iCounter++;
                    
                    /* Allocating memory to pointer of string (output) as per the number of tokens found */
                    *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));
                    
                    if (*pppszValuelist == NULL)
                    {
                        /*Store error into error stack */
                        iRetCode = ERROR_ALLOCATING_MEMORY;
                        EMH_store_error(EMH_severity_error, iRetCode);
                        ETO4_err_write_to_logfile("MEM_realloc", iRetCode,  __FILE__, __LINE__);
                        break;                       
                    }
                    else
                    {
                        /* Get next token: */
                        pszToken = tc_strtok( NULL, pszSeparator );
                    }
                }
            }

            if(iRetCode == ITK_ok)
            {
                *iCount = iCounter;
            }

        }
            
    }

    /* Free the temporary memory used for tokenizing */
    ETO4_MEM_TCFREE(pszTempList);
    return iRetCode;
}


/**
 * This function retrieves tags of the attachments to the workflow whose types are 
 * one of the input Types which is given as an input parameter.
 *
 * @param[in]  task               Tag of the task on which action is triggered
 * @param[in]  iAttachmentType    Attachment type can be one of 'EPM_target_attachment' or 'EPM_reference_attachment'
 * @param[in]  ppszObjTypeList    List of Type of the Object according to which attachment is retrieved.
 * @param[in]  iTypeCnt           Count of input types.
 * @param[out] piCount            Number of attachment that are retrieved.
 * @param[out] pptObject          Array of attachment tags.
 * 
 * @retval ITK_ok is always returned.
 */
int ETO4_arg_get_attached_obj_of_input_type_list
(
    tag_t   task,
    int     iAttachmentType,
    char**  ppszObjTypeList,
    int     iTypeCnt,
    int*    piCount,
    tag_t** pptObject
)
{
    int      iRetCode                = ITK_ok;
    int      iTargetCount            = 0;
    int      iIterator               = 0;
    int      iObjCount               = 0;
    tag_t    tRootTask               = NULLTAG;
    tag_t*   ptTargets               = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*pptObject) = NULL;
    (*piCount)   = 0;

    /* Validate input */
    if(ppszObjTypeList != NULL && iTypeCnt > 0)
    {
        ETO4_ITKCALL(iRetCode, EPM_ask_root_task(task, &tRootTask));
          
        /* Get the target objects. */
        ETO4_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, iAttachmentType, &iTargetCount, &ptTargets));
        
        /* For each target, check if it is an Item Revision. If so, try to get the children.
           Check the status of the returned Item Revision and compare themn with the given list.
        */
        for (iIterator = 0; iIterator < iTargetCount && iRetCode == ITK_ok; iIterator++)
        {
            for(int idx = 0; idx < iTypeCnt && iRetCode == ITK_ok; idx++)
            {
                char*   pszObjType = NULL;
            
                ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(ptTargets[iIterator], &pszObjType));

                        
                if (tc_strcmp(ppszObjTypeList[idx], pszObjType) == 0)
                { 
                    iObjCount++;
                    (*pptObject)               = (tag_t *) MEM_realloc((*pptObject ), iObjCount * sizeof(tag_t));
                    (*pptObject )[iObjCount-1] = ptTargets[iIterator];
                }

                ETO4_MEM_TCFREE(pszObjType);
            }
        }

        *piCount   = iObjCount;
    }

    ETO4_MEM_TCFREE(ptTargets);    
    return iRetCode;
    
}

/**
 * This function retrieves tag of the attachment as per the "Type"  of the attachment.
 * Type of the attachment is given as an input parameter.
 *
 * e.g. Suppose 'P_KatalogteilRevision' has class type of 'Item'. If we pass 'pszAttachedObjType' as
 *      'P_KatalogteilRevision' function will specifically return all the attachments of type
 *      'P_KatalogteilRevision' only.
 *
 * @param[in]  task                Tag of the task on which action is triggered
 * @param[in]  iAttachmentType     Attachment type can be one of 'EPM_target_attachment' or 'EPM_reference_attachment'
 * @param[in]  pszAttachedObjType  Type of the Object according to which attachment is retrieved.
 * @param[out] piCount             Number of attachment that are retrieved.
 * @param[out] pptObject           Array of attachment tags.
 * 
 * @retval ITK_ok is always returned.
 */
int ETO4_arg_get_attached_objects_of_type
(
    tag_t   task,
    int     iAttachmentType,
    char*   pszAttachedObjType,
    int*    piCount,
    tag_t** pptObject
)
{
    int      iRetCode      = ITK_ok;
    int      iTargetCount  = 0;
    int      iIterator     = 0;
    int      iObjCount     = 0;
    tag_t    tRootTask     = NULLTAG;
    tag_t*   ptTargets     = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*pptObject) = NULL;
    (*piCount)   = 0;

    /* Validate input */
    if(pszAttachedObjType != NULL)
    {
        ETO4_ITKCALL(iRetCode, EPM_ask_root_task(task, &tRootTask));
          
        /* Get the target objects. */
        ETO4_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, iAttachmentType, &iTargetCount, &ptTargets));
        
        /* For each target, check if it is an Item Revision. If so, try to get the children.
           Check the status of the returned Item Revision and compare themn with the given list.
        */
        for (iIterator = 0; iIterator < iTargetCount && iRetCode == ITK_ok; iIterator++)
        {
            logical bIsTypeOf = false;
            
            ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(ptTargets[iIterator], pszAttachedObjType, &bIsTypeOf));
            
            if (bIsTypeOf == true)
            { 
                iObjCount++;
                (*pptObject ) = (tag_t *) MEM_realloc((*pptObject ), iObjCount * sizeof(tag_t));
                (*pptObject )[iObjCount-1] = ptTargets[iIterator];
            }
        }

        *piCount   = iObjCount;
    }

    ETO4_MEM_TCFREE(ptTargets);    
    return iRetCode;
}

/**
 * This function loops through the arguments given in a handler
 * definition list and searches for the given switch. It gives
 * back a list with values of the switch. \n
 * Depending on the lOptional the argument must be present in
 * the definition of the handler or not.
 *
 * @note Example: -type=item;document \n
 *       where -type is the switch and \n
 *       item and document are the two values which appear in the array.
 *
 * @note The separator for multiple values can be semi-colon (;) or coma(,). 
 *       This is generic function and takes input as character separator 
 *       on that basis function will process the value list.
 *       
 * @param[in]  pArgList           Argument list provided by the handler msg structure
 * @param[in]  tTask              Task tag of the workflow task
 * @param[in]  pszArgument        Name of the argument to be used
 * @param[in]  cSep               Separator character
 * @param[in]  bOptional          If FALSE the argument is required, else optional
 * @param[out] piValCount         Number of values returned
 * @param[out] pppszArgValue      Array of values returned
 * @param[out] ppszOriginalArgVal Value of the argument with separator as passed in the workflow 
 * @retval ITK_ok is successful else ITK return code.
 */
int ETO4_arg_get_arguments
(
    TC_argument_list_t*   pArgList,
    tag_t                 tTask,
    const char*           pszArgument,
    char                  cSep,
    logical               bOptional,
    int*                  piValCount,
    char***               pppszArgValue,
    char**                ppszOriginalArgVal
)
{
    /* Use TC_init_argument_list and TC_number_of_arguments to get the argument list
       find the right argument using ITK_ask_argument_named_value
       if found, split the value to an array of values using the separator
    */
    
    int  iRetCode                = 0;
    int  iIterator               = 0;
    int  iArgCount               = 0;
    char* pszSwitch              = NULL;
    char* pszSValue              = NULL;
    char* pszNextArgument        = NULL;
    logical bFoundArg            = false;

    /* Initializing the Out Parameter to this function. */
    (*pppszArgValue) = NULL;
    (*piValCount)   = 0;
    (*ppszOriginalArgVal) = NULL;

    /* Validate input */
    if((pszArgument == NULL) || (cSep == '\0'))
    {
        TC_write_syslog( "ERROR: Missing input data:Name of the argument or seprator is null\n");
    }
    else
    {
        TC_init_argument_list(pArgList);
        

        iArgCount = TC_number_of_arguments(pArgList);

        for(iIterator = 0; iIterator < iArgCount && iRetCode == ITK_ok; iIterator++)
        {
            pszNextArgument = TC_next_argument(pArgList);

            ETO4_ITKCALL(iRetCode, ITK_ask_argument_named_value(pszNextArgument, &pszSwitch, &pszSValue));
            
            if(tc_strcmp(pszSwitch, pszArgument) == 0)
            {
                int iLen  = 0;
                
                iLen = (int)tc_strlen(pszSValue);

                (*ppszOriginalArgVal) = (char*) MEM_alloc(sizeof(char)*(iLen +1));

                if(iLen > 0)
                {
                    tc_strcpy((*ppszOriginalArgVal), pszSValue);
                }

                ETO4_ITKCALL(iRetCode, ETO4_arg_parse_multipleVal(pszSValue, cSep, piValCount, pppszArgValue));
                if(iRetCode != ITK_ok)
                {
                    /*Store error into error stack */
                    iRetCode = UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES;
                    EMH_store_error(EMH_severity_error, iRetCode);
                    break;
                }
                else
                {
                    bFoundArg = true;
                    break;
                }
            }
            
            ETO4_MEM_TCFREE(pszSwitch);
            ETO4_MEM_TCFREE(pszSValue);
        }

        /*
         * When the requested argument is not found: If lOptional flag is set to TRUE, Do Nothing.
         * Else If lOptional flag is set to FALSE. Throw an Error.
         */
        if(bOptional == false && bFoundArg == false)
        {
            iRetCode = ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND;
            EMH_store_error(EMH_severity_error, iRetCode);
        }
    }

    ETO4_MEM_TCFREE(pszSwitch);
    ETO4_MEM_TCFREE(pszSValue);
    return iRetCode;
}

/**
 * This function loops through the arguments given in a handler definition list and searches for the given argument name.
 * It gives back single values of the input argument name. \n
 * Depending on the lOptional the argument must be present in the definition of the handler or not.
 *       
 * @param[in]  pArgList      Argument list provided by the handler msg structure
 * @param[in]  tTask         Task tag of the workflow task
 * @param[in]  pszArgument   Name of the argument whose value has to be retrieved
 * @param[in]  bOptional     If FALSE the argument is required, else optional
 * @param[out] ppszArgValue  Array of values returned
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int ETO4_arg_get_wf_argument_value
(
    TC_argument_list_t*   pArgList,
    tag_t                 tTask,
    const char*           pszArgument,
    logical               bOptional,
    char**                ppszArgValue
)
{
    /* Use TC_init_argument_list and TC_number_of_arguments to get the argument list
       find the right argument using ITK_ask_argument_named_value
       if found, split the value to an array of values using the separator
    */
    
    int  iRetCode                = 0;
    int  iIterator               = 0;
    int  iArgCount               = 0;
    char* pszSwitch              = NULL;
    char* pszSValue              = NULL;
    char* pszNextArgument        = NULL;
    logical bFoundArg            = false;

    /* Initializing the Out Parameter to this function. */
    (*ppszArgValue) = NULL;

    TC_init_argument_list(pArgList);

    iArgCount = TC_number_of_arguments(pArgList);

    for(iIterator = 0; iIterator < iArgCount && iRetCode == ITK_ok; iIterator++)
    {
        pszNextArgument = TC_next_argument(pArgList);

        ETO4_ITKCALL(iRetCode, ITK_ask_argument_named_value(pszNextArgument, &pszSwitch, &pszSValue));
        
        if(tc_strcmp(pszSwitch, pszArgument) == 0)
        {
            (*ppszArgValue) = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(pszSValue)+1)));
            tc_strcpy(*ppszArgValue, pszSValue);
            bFoundArg = true;
            break;
        }
        
        ETO4_MEM_TCFREE(pszSwitch);
        ETO4_MEM_TCFREE(pszSValue);
    }

    /* When the requested argument is not found: If lOptional flag is set to TRUE, Do Nothing.
       Else If lOptional flag is set to FALSE. Throw an Error.
     */
    if(bOptional == false && bFoundArg == false)
    {
        iRetCode = ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND;
        EMH_store_error(EMH_severity_error, iRetCode);
    }

    ETO4_MEM_TCFREE(pszSwitch);
    ETO4_MEM_TCFREE(pszSValue);
    return iRetCode;
}





