@echo off
setlocal
set CURRENT_DIR=%~dp0
REM e.g. 1.0.0
set CURRENT_RELEASE=%1

echo.
echo Please select the Teamcenter version for which you want to install the server side components of this solution:
echo   [A]Teamcenter 9.1 64 bit
echo   [B]Teamcenter 9.1 32 bit
echo   [C]Skip process
choice /c:ABC

if ERRORLEVEL 3 (
    echo Process skipped!
    goto :EOF
)
if ERRORLEVEL 2 (
    set BIT_VERSION=32x
    set ERRORLEVEL=0
    goto START_PROCESS
)
if ERRORLEVEL 1 (
    set BIT_VERSION=64x
    set ERRORLEVEL=0
    goto START_PROCESS
)
echo.
echo ERROR: Invalid Selection!
goto :EOF

:START_PROCESS

REM this script is used to start release_<CURRENT_RELEASE>.cmd and write a log file at the same time using tee utility.
if not exist tee.exe (
    echo WARNING: tee.exe was not found in this directory! Starting process wihtout writing a log file...
    echo.
    %CURRENT_DIR%release_%CURRENT_RELEASE%.cmd %*
    goto :EOF
)
if not exist %CURRENT_DIR%logs mkdir %CURRENT_DIR%logs
if not exist %CURRENT_DIR%logs\%COMPUTERNAME% mkdir %CURRENT_DIR%logs\%COMPUTERNAME%
set LOG_FILE=%CURRENT_DIR%logs\%COMPUTERNAME%\install_release_%CURRENT_RELEASE%_%computername%_%random%.log
%CURRENT_DIR%release_%CURRENT_RELEASE%.cmd %* | %CURRENT_DIR%tee -a %LOG_FILE%
endlocal