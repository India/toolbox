/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_register_callbacks.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <eto4_const.hxx>
#include <eto4_register_callbacks.hxx>
#include <eto4_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Description:
 *
 * Function to register Callback function. Entry point for custom T$ custom Code.
 *
 * Returns:
 *
 * Status of execution.
 */
extern DLLAPI int ETO4_custom_register_callbacks () 
{
    int  iRetCode   = ITK_ok;
        
    printf("INFO: Registered T4 Register Custom Exits. \n");

    iRetCode = CUSTOM_register_exit ("ETO4_custom", "USER_gs_shell_init_module", (CUSTOM_EXIT_ftn_t)  ETO4_register_custom_handlers);
    if(iRetCode!=ITK_ok)
    {
        printf("ERROR: Unable to register custom handlers.");
        return iRetCode;
    }

    return iRetCode;
}




