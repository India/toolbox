/*======================================================================================================================================================================
                Copyright 2013 Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_object_utilities.cxx

    Description:  This File contains custom functions for POM classes to perform generic operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <eto4_library.hxx>
#include <eto4_const.hxx>
#include <eto4_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Get the class name of the given object
 *
 * @note Get the class name with POM functions
 * 
 * @param[in] tObject        Object to get the class name from
 * @param[out] ppszClassName The name of the class of the object
 *
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_obj_ask_class
(
    tag_t    tObject,
    char**   ppszClassName
)
{
    int  iRetCode                = ITK_ok;
    char *pszNameOfClass         = NULL;
    tag_t tObjClass              = NULLTAG;

    /* Initializing the Out Parameter to this function. */
    (*ppszClassName) = NULL;


    ETO4_ITKCALL(iRetCode, POM_class_of_instance (tObject, &tObjClass));
    
    ETO4_ITKCALL(iRetCode, POM_name_of_class(tObjClass, &pszNameOfClass));
      
    if(iRetCode == ITK_ok)
    {
        (*ppszClassName) = (char*) MEM_alloc((((int)tc_strlen(pszNameOfClass))+1)* sizeof(char));

        tc_strcpy(*ppszClassName,pszNameOfClass);
    }


    ETO4_MEM_TCFREE(pszNameOfClass);

    return iRetCode;
}

/**
 * This function gets the name of type for the input business
 * objet.
 * e.g. Consider Item type 'P_KatalogteilRevision'. If we pass tag to
 *      an instance of 'P_KatalogteilRevision' this function will return string
 *      'P_KatalogteilRevision' and not 'ItemRevision'. Consider Dataset of type 'catia'.
 *      If we pass tag to an instance of 'catia' this function will
 *      return 'catia' and not 'Dataset'.
 *
 * @param[in]  tObject       Business Object tag
 * @param[out] ppszObjType   Object type
 *
 * @return ITK_ok is successful else ITK return code.
 */

int ETO4_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
)
{
    int     iRetCode            = ITK_ok;
    tag_t   tObjectType         = NULLTAG;
    char    szObjectType[TCTYPE_name_size_c+1] = {""};

    /* Initializing the Out Parameter to this function. */
    (*ppszObjType) = NULL;
    
    ETO4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject,&tObjectType));
    
    ETO4_ITKCALL(iRetCode, TCTYPE_ask_name(tObjectType, szObjectType));
    
    if(iRetCode == ITK_ok)
    {
        *ppszObjType = (char*) MEM_alloc(sizeof(char) * (((int)tc_strlen(szObjectType)) +1));
        if(*ppszObjType == NULL)
        {
           /*Store error into error stack */
            iRetCode = ERROR_ALLOCATING_MEMORY;
            EMH_store_error(EMH_severity_error, iRetCode);
            ETO4_err_write_to_logfile("MEM_alloc", iRetCode,__FILE__, __LINE__);            
        }
        else
        {
            tc_strcpy(*ppszObjType, szObjectType);
        }

    }
    
    return iRetCode;
}
/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int ETO4_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
	tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    ETO4_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    ETO4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    ETO4_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}


/**
 * This function get's the tag to status of input status type(pszStatusName) out 
 * of the list of status attached to input Workspace Object.
 *
 * @param[in]  tObject        The object to get the release status from
 * @param[in]  pszStatusName  Name of status whose tag is to be recovered
 * @param[out] ptStatusTag    Tag to status of input type
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_obj_ask_status_tag
(
    tag_t      tObject,
    char*      pszStatusName,
    tag_t*     ptStatusTag
)
{
    int     iRetCode              = ITK_ok;
    int     iNoOfStatuses         = 0;
    tag_t   item_rev              = NULLTAG;
    tag_t*  ptStatusTagList       = NULL;
    logical bFound                = false;
    char    szReleaseStatType[WSO_name_size_c+1] = {""};

    /* Initializing the Out Parameter to this function. */
    (*ptStatusTag) = NULLTAG;

    ETO4_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses, &ptStatusTagList));
    
    for (int iDx = 0; iDx < iNoOfStatuses && iRetCode == ITK_ok && bFound == false; iDx++)
    {
        ETO4_ITKCALL(iRetCode, CR_ask_release_status_type(ptStatusTagList[iDx], szReleaseStatType));
        
        if(tc_strcmp(pszStatusName, szReleaseStatType) == 0)
        {
              *ptStatusTag = ptStatusTagList[iDx];
              bFound = true;
        }       
    }
      
    ETO4_MEM_TCFREE(ptStatusTagList);
    return iRetCode;
}

/**
 * Set the string attribute value.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] pszPropVal      Value of the property to be set
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int ETO4_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    ETO4_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute string value.
     */    
    ETO4_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszPropertyName, pszPropVal));

    /*  Save the object.  */
    ETO4_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     ETO4_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}