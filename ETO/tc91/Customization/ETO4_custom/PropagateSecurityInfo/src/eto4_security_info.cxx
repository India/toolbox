/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_security_info.cxx

    Description: This File contains functions/handlers which are responsible for sending a dispatcher request depending on the
                 argument or parameter values.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <eto4_action_handlers.hxx>
#include <eto4_library.hxx>
#include <eto4_errors.hxx>
#include <eto4_security_info.hxx>

#ifdef __cplusplus
}
#endif

/**
 * The handler 
 *
 * @param[in] msg  Handler Message
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int ETO4_propagate_security_info
(
    EPM_action_message_t msg
)
{
    int    iRetCode            = ITK_ok;
    int    iIRCount            = 0;
    int    iPropInfoCnt        = 0;
    tag_t  tRootTask           = NULLTAG;
    tag_t* ptItemRev           = NULL;
    char*  pszIRType           = NULL;
    char** ppszPropagationInfo = NULL;

    /* Get the target attachments   */
    ETO4_ITKCALL(iRetCode, EPM_ask_root_task( msg.task, &tRootTask));
    
    /* Getting the "object_types" argument value. */
    ETO4_ITKCALL(iRetCode, ETO4_arg_get_wf_argument_value(msg.arguments, msg.task, ETO4_WF_ARG_OBJECT_TYPES,
                                                              false, &pszIRType));

    if(pszIRType == NULL)
    {
        ETO4_ITKCALL(iRetCode, ETO4_arg_get_attached_objects_of_type(tRootTask, EPM_target_attachment, pszIRType, 
                                                                        &iIRCount, &ptItemRev));
    }
    else
    {
        ETO4_ITKCALL(iRetCode, ETO4_arg_get_attached_objects_of_type(tRootTask, EPM_target_attachment, ETO4_CLASS_ITEMREVISION, 
                                                                        &iIRCount, &ptItemRev));
    }

    if(iIRCount > 0)
    {
        ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values( ETO4_PREF_PROPAGATION_INFORMATION, &iPropInfoCnt, &ppszPropagationInfo ));

        if(iPropInfoCnt == 0)
        {
            TC_write_syslog("Preference \"%s\" for Propagation Information not present in the system. \n",  ETO4_PREF_PROPAGATION_INFORMATION);
            return iRetCode;   
        }
    }

    for(int iDx = 0; iDx < iIRCount&& iRetCode == ITK_ok ; iDx++)
    {
        int    iObjCount      = 0;
        int    iProjectCount  = 0;
        int    iLicCount      = 0;
        tag_t* ptObject       = NULL;
        tag_t* ptProjects     = NULL;
        tag_t* ptLicense      = NULL;
        char*  pszIPClassific = NULL;

        ETO4_ITKCALL(iRetCode, ETO4_get_security_info(ptItemRev[iDx], iPropInfoCnt, ppszPropagationInfo, &iLicCount, &ptLicense, 
                                                        &iProjectCount, &ptProjects, &pszIPClassific));

        if(iLicCount > 0 || iProjectCount > 0 || pszIPClassific != NULL)
        {

        }

        for(int iZx = 0; iZx < iObjCount && iRetCode == ITK_ok; iZx++)
        {
            if(iProjectCount > 0)
            {
                /*  Assign Project to Object */
                ETO4_ITKCALL(iRetCode, PROJ_assign_objects (iProjectCount, ptProjects, 1, &(ptObject[iZx])));
            }

            /*  Assign License to Object */
            for(int iFx = 0; iFx < iLicCount && iRetCode == ITK_ok; iFx++)
            {
                ETO4_ITKCALL(iRetCode, ADA_add_license_object(ptLicense[iFx], ptObject[iZx]));
            }

            /*  Set IP Classification attribute of Object */
            if(iRetCode == ITK_ok && pszIPClassific != NULL)
            {
                ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val(ptObject[iZx], ETO4_ATTR_IP_CLASSIFICATION, pszIPClassific) );
            }
        }

        ETO4_MEM_TCFREE( ptProjects );
        ETO4_MEM_TCFREE( ptLicense );
        ETO4_MEM_TCFREE( pszIPClassific );
        ETO4_MEM_TCFREE( ptObject );
    }

    ETO4_MEM_TCFREE( pszIRType );
    ETO4_MEM_TCFREE( ptItemRev );
    ETO4_MEM_TCFREE( ppszPropagationInfo );
    return iRetCode;
}

/**
 * The function retrieves the security attribute information from the Item of the input revision. All the security information
 * follow the propagation rule hence this function assumes that security information is applied to Item and because of propagation
 * rules the information will get assigned to it associated objects as defined in propagation rule in Teamcenter.
 *
 * @param[in]  tItemRev          Tag of Item revision whose Item will be considered for retrieving security information
 * @param[in]  iSecurityAttrCnt  Security attribute count
 * @param[in]  ppszSecurityAttr  Names of Security attribute whose value needs to be fetched
 * @param[out] piLicCount        License Count
 * @param[out] pptLicense        Tag of License assigned to Item of input revision
 * @param[out] piProjectCount    Project count
 * @param[out] pptProjects       Tag of projects
 * @param[out] ppszIPClassific   IP Classification attribute value
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int ETO4_get_security_info
(
    tag_t   tItemRev,
    int     iSecurityAttrCnt,
    char**  ppszSecurityAttr,
    int*    piLicCount,
    tag_t** pptLicense,
    int*    piProjectCount,
    tag_t** pptProjects,
    char**  ppszIPClassific
)
{
    int   iRetCode     = ITK_ok;
    tag_t tItem        = NULLTAG;
 
    ETO4_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));

    for(int iJx = 0; iJx < iSecurityAttrCnt && iRetCode == ITK_ok; iJx++)
    {
        if(tc_strcmp(ETO4_CONST_PROJECT, ppszSecurityAttr[iJx]) == 0)
        {   
            /* Get the project assigned to Item */
            ETO4_ITKCALL(iRetCode, AOM_ask_value_tags(tItem, ETO4_ATTR_PROJECT_LIST, piProjectCount, pptProjects));
        }
        else
        {
            if(tc_strcmp(ETO4_CONST_LICENSE, ppszSecurityAttr[iJx]) == 0)
            {  
                ETO4_ITKCALL(iRetCode, AOM_ask_value_tags(tItem, ETO4_ATTR_LICENSE_LIST, piLicCount, pptLicense));
            }
            else
            {
                if(tc_strcmp(ETO4_CONST_IP_CLASSIFICATION, ppszSecurityAttr[iJx]) == 0)
                {
                    ETO4_ITKCALL(iRetCode, AOM_ask_value_string(tItem, ETO4_ATTR_IP_CLASSIFICATION, ppszIPClassific) );
                }
            }
        }
    }
    
    return iRetCode;
}

/**
 * The function retrieves the security attribute information from the Item of the input revision. All the security information
 * follow the propagation rule hence this function assumes that security information is applied to Item and because of propagation
 * rules the information will get assigned to it associated objects as defined in propagation rule in Teamcenter.
 *
 * @param[in]  tItemRev          Tag of Item revision whose Item will be considered for retrieving security information
 * @param[in]  iSecurityAttrCnt  Security attribute count
 * @param[in]  ppszSecurityAttr  Names of Security attribute whose value needs to be fetched
 * @param[out] piLicCount        License Count
 * @param[out] pptLicense        Tag of License assigned to Item of input revision
 * @param[out] piProjectCount    Project count
 * @param[out] pptProjects       Tag of projects
 * @param[out] ppszIPClassific   IP Classification attribute value
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int ETO4_get_obj_without_security_info
(
    tag_t   tItemRev,
    int     iLicCount,
    tag_t*  ptLicense,
    int     iProjectCount,
    tag_t*  ptProjects,
    char*   pszIPClassific,
    int*    iObjCnt,
    tag_t** pptObject
)
{
    int   iRetCode     = ITK_ok;

    return iRetCode;
}