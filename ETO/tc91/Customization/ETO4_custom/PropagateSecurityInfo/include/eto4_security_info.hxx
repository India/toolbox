/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_security_info.hxx

    Description:  Header File for eto4_security_info.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_SECURITY_INFO_HXX
#define ETO4_SECURITY_INFO_HXX

#include <tccore/license.h>

int ETO4_propagate_security_info
(
    EPM_action_message_t msg
);

int ETO4_get_security_info
(
    tag_t   tItemRev,
    int     iSecurityAttrCnt,
    char**  ppszSecurityAttr,
    int*    piLicCount,
    tag_t** pptLicense,
    int*    piProjectCount,
    tag_t** pptProjects,
    char**  ppszIPClassific
);

#endif

