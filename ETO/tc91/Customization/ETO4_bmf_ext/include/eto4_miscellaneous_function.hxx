/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_miscellaneous_function.hxx

    Description:  Header File for 'eto4_miscellaneous_function.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifndef ETO4_MISCELLANEOUS_FUNCTION_HXX
#define ETO4_MISCELLANEOUS_FUNCTION_HXX

#include <tccore/license.h>

int ETO4_validate_project_id_attr_prefix
(
    char*    pszProjectID,
    logical* pbHasValidPrefix
);

int ETO4_create_phase_folders
(
    char*   pszObjectName,
    tag_t*  ptNewItemRev,
    logical bCreateDoc
);

int ETO4_create_items_for_phase_folder
(
	tag_t   tParentFolder,
	char*   pszETOPrjName,
    char*   pszPhaseFolderType
);

int ETO4_create_item_type 
(
	char*  pszDocumentType,
	char*  pszDocumentName,
	int    iPropCnt,
	char** ppszProperty,
	char** ppszPropertyVal,
	tag_t* ptDocument,
	tag_t* ptDocumentRev
);

int ETO4_validate_object_for_past_action_on_phase_folder
(
	tag_t* ptContents,
	int    iTotalContents,
    tag_t  tFolder
);

int ETO4_verify_object_pasted_to_phase_folder
(
	int    iPrefValCnt,
	char** ppszPrefVals,
	char*  pszObjType,
	char*  pszFolderType
);

int ETO4_check_and_create_fl_sub_substruct
(
    char*   pszParentFLName,
    char*   pszParentFLDesc,
    tag_t   tParentFL,
    int     iTempPrefValCnt,
    char**  ppszTempPrefVal
);

int ETO4_validate_and_propagation_info
(
    char*  pszSourceFLType,
    tag_t  tSourceObj,
    int    iPropInfoTypCnt,
    char** ppszPropInfoType,
    int    iObjCnt,
    tag_t* ptObjects
);

int ETO4_clone_folder_structure 
(
    tag_t tItemRev
);

int ETO4_construct_sub_structure 
(
    tag_t tParentFolder,
	tag_t tSourceFolder
);

#endif
