/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_lib_library.hxx

    Description:  Header File containing declarations of function spread across all eto4_lib_<UTILITY NAME>_utilities.cxx 
                  For e.g. eto4_lib_dataset_utilities.cxx/eto4_lib_form_utilities.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifndef ETO4_LIB_LIBRARY_HXX
#define ETO4_LIB_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me.h>
#include <form.h>
#include <project.h>
#include <folder.h>
#include <pom.h>
#include <tctype.h>
#include <tc/tc_util.h>
#include <eto4_lib_const.hxx>
#include <eto4_lib_itk_errors.hxx>
#include <eto4_lib_itk_mem_free.hxx>
#include <eto4_lib_string_macros.hxx>

/*        Utility Functions For Item/Item Revision        */
int ETO4_itemrev_get_previous_rev
(
    tag_t     tItemRev,
    tag_t*    ptPreviousItemRev,
    logical*  pbIsOnlyRevision,
    logical*  pbIsFirstRevision
);

int ETO4_itemrev_get_all_previous_rev
(
    tag_t     tItemRev,
    int*      piPrevIRCnt,
    tag_t**   pptPreviousIR,
    logical*  pbIsOnlyRevision,
    logical*  pbIsFirstRevision
);

int ETO4_itemrev_construct_name
(
    tag_t   tItemRev,
    char**  ppszName
);

/*        Utility Functions For Folder        */
int ETO4_get_user_folder
(
    char*    pszUserFLType,
    tag_t*   ptFolder
);

int ETO4_create_custom_folder
(
    char*    pszClassName,
    char*    pszFolderName,
    char*    pszDesc,
    int      iAttrCount,
    char**   ppszAttrNames,
    char**   pszAttrValues,
    tag_t*   ptFolder
);

int ETO4_insert_content_to_folder
(
    tag_t tFolder,
	tag_t tObject,
	int   iPosition
);

/*        Utility Functions For GRM        */
int ETO4_grm_get_secondary_obj_of_type
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    char*   pszSecondaryObjType,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
);

int ETO4_grm_create_relation
(
    tag_t   tPrimaryObj,
    tag_t   tSecondaryObj,
    char*   pszRelationType,
    tag_t*  ptRelation
);

int ETO4_grm_get_secondary_obj
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
);

/*        Utility Functions For Objects        */
int ETO4_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
);

int ETO4_obj_ask_parent_class
(
    tag_t       tObject,
    char**      ppszObjParentTypeName
);

int ETO4_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
);

int ETO4_create_folder
( 
    const char* folder_name, 
    const char* folder_type,
    tag_t *folder_tag
);

int ETO4_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
);

int ETO4_initiate_workflow
(
    char* pszWorkflowTemplate,
    tag_t tTargetObject
);

int ETO4_obj_pom_set_multiple_str_attr_without_saving
(
	tag_t   tObject,
    char*   pszClassName,
    int     iAttrCount,
    char**  ppszAttrNames,
    char**  ppszAttrValues
);

/*        Utility Functions For Error Logging        */
int ETO4_err_write_to_logfile
(
    char*       pszErrorFunction,
    int         iErrorNumber,
    char*       pszFileName,
    int         iLineNumber
);

int ETO4_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
);

/*        Utility Functions For Date & Time        */
int ETO4_get_date
(
    date_t *dReleaseDate
);

int ETO4_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
);



/*        Argument Utility Functions        */
int ETO4_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
);

/*        String Utility Functions        */
int ETO4_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
);

int ETO4_contains_string_value
(
    char*  pszStrVal,
    int    iTotalStrVals,
    char** pppszStrVals
);

void  ETO4_get_string_tokens
(
    const char* pszInputStr, 
    char*       pszDelim,
    int*        piTokenCnt,
    char***     pppszAllTokens
);

void ETO4_get_str_after_delimiter
(
    const char*   pszInputStr,
    const char    szDelim, 
    char**        ppszAllTokens
);



/*        Utility Functions For Preferences        */
int ETO4_pref_get_string_value
(
    char*   pszPrefName, 
    char**  ppszPrefValue
);

int ETO4_pref_get_string_values
(
    char*   pszPrefName, 
    int*    iPrefValCnt, 
    char*** pppszPrefVals
);
    
int ETO4_pref_get_integer_value
(
    char*   pszPrefName, 
    int*    piPrefValue
);



/*        Utility Functions For Effectivity        */


/*        Utility Functions For BOM Line        */


/*       Utility Functions For Organisation */

#endif




