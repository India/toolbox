/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_lib_string_macros.hxx

    Description:  This file contains macros for string's basic operation such as copy, concatination etc

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef ETO4_LIB_STRING_MACROS_HXX
#define ETO4_LIB_STRING_MACROS_HXX

#include <eto4_lib_library.hxx>


#define ETO4_STRCPY(pszDestStr, pszSourceStr){\
    int iSourceStrLen = (int) (tc_strlen(pszSourceStr)+1);\
    pszDestStr = (char*)MEM_alloc((int)(iSourceStrLen * sizeof(char)));\
    tc_strcpy(pszDestStr, pszSourceStr);\
}

#define ETO4_STRCAT(pszDestStr,pszSourceStr){\
    int iSourceStrLen = (int) tc_strlen(pszSourceStr);\
    int iDestiStrLen = 0;\
    if(pszDestStr != NULL)\
       iDestiStrLen = (int) tc_strlen (pszDestStr);\
    pszDestStr = (char*)MEM_realloc(pszDestStr, (int)((iSourceStrLen + iDestiStrLen + 1)* sizeof(char)));\
    if(iDestiStrLen == 0)\
       tc_strcpy(pszDestStr, pszSourceStr);\
    else\
       tc_strcat(pszDestStr, pszSourceStr);\
       tc_strcat(pszDestStr, '\0');\
}


#define ETO4_UPDATE_STRING_ARRAY(piArrayLen,  pppszArrayOfStrings,  pszValue)\
{\
    (*piArrayLen)++;\
    *pppszArrayOfStrings = (char**) MEM_realloc(*pppszArrayOfStrings, (int)((*piArrayLen) * sizeof(char*)));\
    (*pppszArrayOfStrings)[*piArrayLen - 1] = (char*) MEM_alloc((int)((strlen(pszValue) + 1)* sizeof(char)));\
    tc_strcpy((*pppszArrayOfStrings)[*piArrayLen - 1], pszValue);\
}

#endif //ETO4_LIB_STRING_MACROS_HXX







