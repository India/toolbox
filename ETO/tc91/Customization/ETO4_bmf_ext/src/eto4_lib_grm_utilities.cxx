/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_lib_grm_utilities.hxx

    Description: This File contains custom functions for GRM to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <eto4_lib_library.hxx>
#include <eto4_lib_const.hxx>
#include <eto4_errors.hxx>
#ifdef __cplusplus
}
#endif

/**
 * This function returns a list and count of all secondary objects with a specified
 * relation for specified secondary object Type to the specified primary_object.
 *
 * e.g. Suppose 'P_Katalogteil' is a type of 'Item'. Then object type for 'P_Katalogteil'
 *      is 'P_Katalogteil'
 *
 * @note This function returns the value deplending on Secondary Object Type
 * 
 * @param[in] tPrimaryObj              The object.
 * @param[in] pszRelationTypeName      Name of relation type. Can be NULL.
 * @param[in] pszSecondaryObjType      Type of the primary object.
 * @param[out] pptSecondaryObjects     List of Primary object of specified type.
 * @param[out] piObjectCount           Primary Object count.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int ETO4_grm_get_secondary_obj_of_type
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    char*   pszSecondaryObjType,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
)
{

    int     iRetCode                        = ITK_ok;
    int     iCount                          = 0;
    int     iIterator                       = 0;
    int     iSecondaryCount                 = 0;
    tag_t   tRelationType                   = NULLTAG;
    char*   pszObjectType                   = NULL;
    GRM_relation_t *ptSecondaryObjects      = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount) = 0;
    (*pptSecondaryObjects) = NULL;

    /* get the relation type tag e.g. "IMAN_specification"  */
    ETO4_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName,
                                                   &tRelationType)); 
    
    ETO4_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, tRelationType, 
                                                       &iSecondaryCount,
                                                       &ptSecondaryObjects));
                                       
    for(iIterator = 0; iIterator < iSecondaryCount && iRetCode == ITK_ok; iIterator++)
    {

        
        ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(ptSecondaryObjects[iIterator].secondary,
                                                  &pszObjectType));
            
     
        if (tc_strcmp(pszObjectType, pszSecondaryObjType) == 0)
        {
            iCount++;
            (*pptSecondaryObjects) = (tag_t*)MEM_realloc ( (*pptSecondaryObjects) , (sizeof(tag_t) * iCount) );
            (*pptSecondaryObjects)[iCount -1] = ptSecondaryObjects[iIterator].secondary;
        }
    }

    if(iCount == 0)
    {
        TC_write_syslog("No secondary object of type %s is associated with the relation %s to the primary object.\n", pszSecondaryObjType, pszRelationTypeName);
    }
    else
    {
        (*piObjectCount ) = iCount;
    }

    ETO4_MEM_TCFREE(pszObjectType);
    ETO4_MEM_TCFREE(ptSecondaryObjects);
    return iRetCode;
}

/**
 * This function creates specification relationship between primary and secondary object.
 * 
 * @param[in] tPrimaryObj      Tag of the primary object.
 * @param[in] tSecondaryObj    Tag of the secondary object.
 * @param[in] pszRelationType  Name of relation type.
 * @param[out] ptRelation      Tag to newly created relation type.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int ETO4_grm_create_relation
(
    tag_t   tPrimaryObj,
    tag_t   tSecondaryObj,
    char*   pszRelationType,
    tag_t*  ptRelation
)
{
    int    iRetCode        = ITK_ok;
    tag_t  tRelationType   = NULLTAG;
    tag_t  tNewRelation    = NULLTAG;

    /* Initializing the Out Parameter to this function. */
    *ptRelation = NULLTAG;

    /* Get relation type */
    ETO4_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationType, &tRelationType));

    if(tRelationType == NULLTAG)
        iRetCode = GRM_invalid_relation_type;
    if(tPrimaryObj == NULLTAG)
        iRetCode = GRM_invalid_primary_object;
    if(tSecondaryObj == NULLTAG)
        iRetCode = GRM_invalid_secondary_object;

     /*     Find relation to check if relation already exists   */
    ETO4_ITKCALL(iRetCode, GRM_find_relation(tPrimaryObj, tSecondaryObj, tRelationType, &tNewRelation));

    if(tNewRelation == NULLTAG)
    {
        ETO4_ITKCALL(iRetCode, GRM_create_relation(tPrimaryObj, tSecondaryObj, tRelationType, NULLTAG, &tNewRelation));

        if(tNewRelation != NULLTAG)
        {
            ETO4_ITKCALL(iRetCode, GRM_save_relation(tNewRelation));
        }
    }

    if(iRetCode == ITK_ok)
    {
        ( *ptRelation )=tNewRelation;
    }
    
    return iRetCode;
}


/**
 * This function returns a list and count of all secondary objects attached with a specified
 * relation to the specified primary_object.
 *
 * @param[in] tPrimaryObj              The object.
 * @param[in] pszRelationTypeName      Name of relation type. Can be NULL.
 * @param[out] pptSecondaryObjects     List of Primary object
 * @param[out] piObjectCount           Primary Object count.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int ETO4_grm_get_secondary_obj
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
)
{

    int     iRetCode                        = ITK_ok;
    int     iCount                          = 0;
    int     iIterator                       = 0;
    int     iSecondaryCount                 = 0;
    tag_t   tRelationType                   = NULLTAG;
    char*   pszObjectType                   = NULL;
    GRM_relation_t *ptSecondaryObjects      = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount) = 0;
    (*pptSecondaryObjects) = NULL;

    /* get the relation type tag e.g. "IMAN_specification"  */
    ETO4_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    
    ETO4_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, tRelationType, 
                                                       &iSecondaryCount, &ptSecondaryObjects));
                                       
    for(iIterator = 0; iIterator < iSecondaryCount && iRetCode == ITK_ok; iIterator++)
    {
        int  iActiveSeq   = 0;
      
        ETO4_ITKCALL(iRetCode, AOM_ask_value_int(ptSecondaryObjects[iIterator].secondary, ETO4_ATTR_ACTIVE_SEQ, &iActiveSeq));
	
		/* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0)
        {
            iCount++;
            (*pptSecondaryObjects) = (tag_t*)MEM_realloc ( (*pptSecondaryObjects) , (sizeof(tag_t) * iCount) );
            (*pptSecondaryObjects)[iCount -1] = ptSecondaryObjects[iIterator].secondary;
        }
    }

    if(iCount == 0)
    {
        TC_write_syslog("No secondary object is associated with the relation %s to the primary object.\n", pszRelationTypeName);
    }
    else
    {
        (*piObjectCount ) = iCount;
    }

    ETO4_MEM_TCFREE(pszObjectType);
    ETO4_MEM_TCFREE(ptSecondaryObjects);
    return iRetCode;
}