/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_lib_arguments_utilities.hxx

    Description: This File contains functions for string parsing operations and functions for retrieving arguments passed in workflow action or rule handlers
                  These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <eto4_lib_library.hxx>
#include <eto4_errors.hxx>
#ifdef __cplusplus
}
#endif

/**
 * Parses a string containing values seperated by a given separator
 * into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in] pszList          Input list to be parsed
 * @param[in] cSep             Separator character
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by ETO4_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 * @retval INCORRECT_INPUT If list or seprator is Null.
 * @retval MEMORY_ALLOCATION_FAILED If cannot allocate memory.
 */
int ETO4_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
)
{
    int        iRetCode    = ITK_ok;
    int        iCounter    = 0;
    char*      pszToken    = "";
    char*      pszTempList = NULL;

    /* Convert separator (char) to char* - strtok(char* input, Char* sep) */
    char pszSeparator[ETO4_SEPARATOR_LENGTH+1];
    pszSeparator[0] = cSep;
    pszSeparator[1] = '\0';

    /* Validate input */
    if((pszList == NULL) || (cSep == '\0'))
    {
        TC_write_syslog( "ERROR: Missing input data:List passed or seprator is null\n");
    }
    else
    {

        /* Allocating memory to pszTempList */
        pszTempList  = (char*) MEM_alloc(sizeof(char)*((int)tc_strlen(pszList)+1));
        if (pszTempList == NULL)
        {
            TC_write_syslog( "ERROR: Cannot allocate memory\n");
        }
        else
        {
            /* Copying the input string to Null terminated string */
            tc_strcpy(pszTempList, pszList);

            /* Get the first token */
            pszToken = tc_strtok( pszTempList,pszSeparator );

            *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
            /* Tokenize the input string */
            while( pszToken != NULL )
            {


                /* Allocate memory for each string pointer */
                (*pppszValuelist)[iCounter] = (char*) MEM_alloc(sizeof(char)*(((int)tc_strlen(pszToken))+1));
                
                if ((*pppszValuelist)[iCounter] == NULL)
                {
                    /*Store error into error stack */
                    iRetCode = ERROR_ALLOCATING_MEMORY;
                    EMH_store_error(EMH_severity_error, iRetCode);
                    ETO4_err_write_to_logfile("MEM_alloc", iRetCode,  __FILE__, __LINE__);
                    break;
                }
                else
                {
                    /* Copy the token to the output string pointer */
                    tc_strcpy((*pppszValuelist)[iCounter],pszToken);

                    /* Count the number of elements in the string */
                    iCounter++;
                    
                    /* Allocating memory to pointer of string (output) as per the number of tokens found */
                    *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));
                    
                    if (*pppszValuelist == NULL)
                    {
                        /*Store error into error stack */
                        iRetCode = ERROR_ALLOCATING_MEMORY;
                        EMH_store_error(EMH_severity_error, iRetCode);
                        ETO4_err_write_to_logfile("MEM_realloc", iRetCode,  __FILE__, __LINE__);
                        break;                       
                    }
                    else
                    {
                        /* Get next token: */
                        pszToken = tc_strtok( NULL, pszSeparator );
                    }
                }
            }

            if(iRetCode == ITK_ok)
            {
                *iCount = iCounter;
            }

        }
            
    }

    /* Free the temporary memory used for tokenizing */
    ETO4_MEM_TCFREE(pszTempList);
    return iRetCode;
}





