/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_lib_preference_utilities.cxx

    Description: This File contains custom functions for Item & Item Revision to perform generic operations. These functions are part of library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <eto4_lib_library.hxx>
#include <eto4_lib_const.hxx>
#include <eto4_errors.hxx>
#ifdef __cplusplus
}
#endif

/**
 * Function to read Teamcenter preference with string value
 *
 * @param[in]  pszPrefName    Preference Name
 * @param[out] ppszPrefVals   String value of preference
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_pref_get_string_value
(
    char*   pszPrefName, 
    char**  ppszPrefValue
)
{
    int iRetCode   = ITK_ok;
    int iPrefCnt   = 0;

    TC_preference_search_scope_t prefOldScope;
    
    ETO4_ITKCALL(iRetCode, PREF_initialize());

    ETO4_ITKCALL(iRetCode, PREF_ask_search_scope( &prefOldScope));

    ETO4_ITKCALL(iRetCode, PREF_set_search_scope( TC_preference_site ));

    ETO4_ITKCALL(iRetCode, PREF_ask_value_count( pszPrefName, &iPrefCnt ));

    if( iPrefCnt != 0 )
        ETO4_ITKCALL(iRetCode, PREF_ask_char_value( pszPrefName, 0, ppszPrefValue ));

    ETO4_ITKCALL(iRetCode, PREF_set_search_scope (prefOldScope));

    return iRetCode;
}


/**
 * Function to read Teamcenter preference with Multiple string value
 *
 * @param[in]  pszPrefName    Preference Name
 * @param[out] piPrefValCnt   Total number of preference values
 * @param[out] pppszPrefVals  String array of preference values
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_pref_get_string_values
(
    char*   pszPrefName, 
    int*    piPrefValCnt, 
    char*** pppszPrefVals
)
{
    int iRetCode   = ITK_ok;
    int iPrefCnt   = 0;

    TC_preference_search_scope_t prefOldScope;
    
    ETO4_ITKCALL(iRetCode, PREF_initialize());

    ETO4_ITKCALL(iRetCode, PREF_ask_search_scope( &prefOldScope));

    ETO4_ITKCALL(iRetCode, PREF_set_search_scope( TC_preference_site ));

    ETO4_ITKCALL(iRetCode, PREF_ask_value_count( pszPrefName, &iPrefCnt ));

    if( iPrefCnt != 0 )
        ETO4_ITKCALL(iRetCode, PREF_ask_char_values( pszPrefName, piPrefValCnt, pppszPrefVals ));

	ETO4_ITKCALL(iRetCode, PREF_set_search_scope (prefOldScope));

    return iRetCode;
}

/**
 * Function to read Teamcenter Preference for Retrieving integer value
 *
 * @param[in]  pszPrefName  Preference Name
 * @param[out] piPrefValue  Integer value of preference            
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int ETO4_pref_get_integer_value
(
    char*   pszPrefName, 
    int*    piPrefValue
)
{
    int iRetCode  = ITK_ok;
	int iPrefCnt   = 0;

    TC_preference_search_scope_t prefOldScope;
    
    ETO4_ITKCALL(iRetCode, PREF_initialize());

    ETO4_ITKCALL(iRetCode, PREF_ask_search_scope( &prefOldScope));

    ETO4_ITKCALL(iRetCode, PREF_set_search_scope( TC_preference_site ));

    ETO4_ITKCALL(iRetCode, PREF_ask_value_count( pszPrefName, &iPrefCnt ));

    if(iPrefCnt != 0 )
        ETO4_ITKCALL(iRetCode, PREF_ask_int_value( pszPrefName, 0, piPrefValue ));

	ETO4_ITKCALL(iRetCode, PREF_set_search_scope (prefOldScope));

    return iRetCode;
}




