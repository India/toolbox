/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: eto4_bmf_extension.hxx

    Description: This File conatins is  wrapper class. This  is the entry point to create and maintain the ETFF.  Any other activities needed
                 to maintain the ETFF should be routed through this class.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <eto4_lib_library.hxx>
#include <eto4_lib_const.hxx>
#include <eto4_errors.hxx>
#include <eto4_extension_arguments.hxx>
#include <eto4_bmf_extension.hxx>
#include <eto4_miscellaneous_function.hxx>
#ifdef __cplusplus
}
#endif

/* This variable is used as part of project assignment operation while pasting contents to Phase and ML Folders. This variable is
   initialized of this variable will be performed by SMA3_paste_to_folder_pre_condition function to actual number of contents of
   folder before pasting or cutting from folder.
*/
static int iContentCnt = 0;

/**
 * Function performs post action on "PROJECT_create" event of "TC_Project".
 * Function creates 'ETO4_Project' Item and sets Item name same as project name. 'ETO4_Project' description will get 
 * same value as TC_Project description. Function also creates dispatcher request to assign Project to newly created 
 * 'ETO4_Project' Item. 'ETO4_Project' will be attached to users new stuff folder.
 *
 * @note: During creation of "ETO4_Project" Item another extension also gets invoked that will attach 
 *        custom folders to "ETO4_ProjectRevision".
 *
 * @param[in] pmMessage Received message
 * @param[in] args      List of arguments containing Project information
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_create_etoproject_proj_cre_post_action 
(
    METHOD_message_t* pmMessage,
    va_list   args
)
{
	/*  Message "PROJECT_create"  */
    int          iRetCode           = ITK_ok;
    logical      bHasValidPrefix    = false;
    char*        pszProjectID       = va_arg(args, char*);
    char*        pszProjectName     = va_arg(args, char*);
    char*        pszProjectDesc     = va_arg(args, char*);
    tag_t*       ptNewProject       = va_arg(args, tag_t*);

    ETO4_ITKCALL(iRetCode, ETO4_validate_project_id_attr_prefix(pszProjectID, &bHasValidPrefix));

	if(bHasValidPrefix == true)
	{
		int          iMarkPoint         = 0;
		tag_t        tETOProjItem       = NULLTAG;
		tag_t        tETOProjItemRev    = NULLTAG;
		tag_t        tOwningUser        = NULLTAG;
		tag_t        tRequest           = NULLTAG;
		tag_t        tNewstuffFL        = NULLTAG;
        char*        pszPropName        = NULL;

		/* Define a markpoint for database transaction */
		ETO4_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));

		/* Create ETO4_Phase_Plan   */
		ETO4_ITKCALL( iRetCode, ITEM_create_item ( pszProjectID, pszProjectName, ETO4_CLASS_PROJECT, NULL, 
												   &tETOProjItem, &tETOProjItemRev));

		ETO4_ITKCALL( iRetCode, AOM_ask_value_tag ( *ptNewProject, ETO4_ATTR_OWNING_USER, &tOwningUser));

		/*  Map Project attr to ETO4_Phase_Plan */
		ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val( tETOProjItem, ETO4_ATTR_DESCRIPTION_ATTRIBUTE, pszProjectDesc));

		ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val( tETOProjItemRev, ETO4_ATTR_DESCRIPTION_ATTRIBUTE, pszProjectDesc));

        ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_value(ETO4_PREF_ATTRIBUTE_FOR_PROJECT_NAME, &pszPropName));
       
        /* Setting persistent property value on ETO Project revision if it is considered for storing the project name */
        if(pszPropName!= NULL)
        {
            ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val( tETOProjItemRev, pszPropName, pszProjectName));
        }

		ETO4_ITKCALL(iRetCode, ETO4_get_user_folder(ETO4_NEWSTUFF_FOLDER, &tNewstuffFL));

		ETO4_ITKCALL(iRetCode, ETO4_insert_content_to_folder(tNewstuffFL, tETOProjItem, 999));

		/* Commit the information into the database. */
		ETO4_ITKCALL(iRetCode, POM_forget_markpoint(iMarkPoint));
	        
		if (iRetCode != ITK_ok)
		{
			logical bStateChanged = false;
	        
			/* Do the rollback of the database. */
			POM_roll_to_markpoint(iMarkPoint, &bStateChanged);
		} 

        ETO4_MEM_TCFREE( pszPropName );
	}

    return iRetCode;
}

/**
 * This extension is registered at "ETO4_ProjectRevision" object as a post-action for the operation "ITEM_create_rev". It is getting
 * fired when creating a new Item of the type "ETO4_Project" using the menu "File->New�->Item�".
 * 
 * The item name attribute (object_name) stores the project name value. Preference can also be created with the name 'ETO4_Attribute_For_Project_Name' specifying the item
 * revision persistent property name which will be considered for storing the project name. The prefix is separated with a "-" from the actual project name.
 * E.g. "ENT-Weser" has the prefix "ENT" or "SW-Oder" has the prefix "SW". This prefix has to be retrieved from the object_name attribute. 
 * A multi value preference with the naming pattern "ETO4_Phase_Folders_<PREFIX>" has to be checked in Teamcenter. It contains the list of folders that need to be created. 
 * E.g. for the prefix "ENT" the preference "ETO4_Phase_Folders_ENT" should exist. It has the values:
 *  -	ETO4_P00_Project_Management
 *  -	ETO4_P01_Req_Phase
 *  -	ETO4_P02_Concept_Phase
 *  -	ETO4_P03_Design_Phase
 *  -	ETO4_P04_Prototype_Phase
 *  -	ETO4_P05_Q_Phase
 *  -	ETO4_P06_Pre_Series_Phase
 *  -	ETO4_P07_Series_Phase
 *
 * Each of above values represents a folder type that needs to be created underneath the "ItemRevision" to which this extension is attached. The folders have to be attached
 * using the relation type "IMAN_reference". Preference can also be created with the name 'ETO4_ItemRev_Phase_Folder_Relation' to specify the relation name using which 
 * folders are attached to Item revision. Folder type name is used as a folder name (object_name). The folder definition might have sub folders, separated by a colon. 
 * If these exist the subfolders have to be created using the specified name (only one level).
 *
 * @note If Item revision attribute 'object_name' contains a Prefix that is mentioned in the preference 'ETO4_Attribute_For_Project_Name' then the 
 *       'object_name' attribute value is reset with the trimmed prefix value. For e.g. if 'object_name' value is "ENT-Weser" and preference 'ETO4_Attribute_For_Project_Name'
 *       has value 'ENT' then 'object_name' is set to 'Weser'
 *
 * @note If this prefix is not used then folder set defined by the preference "ETO4_Phase_Folders_Default" will be used.
 * 
 * For each phase folder a set of mandatory documents can be configured. The pattern for the multi-value preference name is "<PHASE_FOLDER_TYPE>_mandatory_items".
 * The values have to follow the pattern: "<ITEM_TYPE>:<OBJECT_NAME>:<ATTR NAME 1>:<ATTR NAME 2>". 

 * @param[in] pmMessage Received message
 * @param[in] args      List of arguments containing information
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_create_phase_folders_rev_cre_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
)
{
    /* Message "ITEM_create_rev" */
    int          iRetCode          = ITK_ok;
    char*        pszPropName       = NULL;
    char*        pszProjectName    = NULL;
    char*        pszTrimmedName    = NULL;
	char*        pszWFTemplateName = NULL;
    char**       ppszTransArgList  = NULL;
    tag_t        tTransReq         = NULLTAG;
    tag_t        tNewItem          = va_arg(args, tag_t);
    const char*  pszNewRevID       = va_arg(args, char*);
    tag_t*       ptNewItemRev      = va_arg(args, tag_t*);
	
	ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_value(ETO4_PREF_ATTRIBUTE_FOR_PROJECT_NAME, &pszPropName));

    if(pszPropName == NULL)
    {
        ETO4_ITKCALL(iRetCode, AOM_ask_value_string( tNewItem, ETO4_ATTR_OBJECT_NAME_ATTRIBUTE, &pszProjectName));
    }
    else
    {
        ETO4_ITKCALL(iRetCode, AOM_ask_value_string( tNewItem, pszPropName, &pszProjectName));
    }

    ETO4_get_str_after_delimiter(pszProjectName, ETO4_CHARACTER_HIFEN, &pszTrimmedName);

	if(pszTrimmedName == NULL)
	{
		pszTrimmedName = (char*) MEM_alloc((int)(sizeof(char) *(tc_strlen(pszProjectName)+1)));
		tc_strcpy(pszTrimmedName, pszProjectName);
	}
	else
	{
		/* Setting  ETO4_Project Item and revision name with string which is without prefix. */
		ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val(tNewItem, ETO4_ATTR_OBJECT_NAME_ATTRIBUTE, pszTrimmedName));

		ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val(*ptNewItemRev, ETO4_ATTR_OBJECT_NAME_ATTRIBUTE, pszTrimmedName));
	}
	
	/* Folder structure will be created based on Project Prefix hence we will send the actual name and not the trimmed name */
    ETO4_ITKCALL(iRetCode, ETO4_create_phase_folders(pszProjectName, ptNewItemRev, true));


    ETO4_ITKCALL(iRetCode, ETO4_bmf_get_arg_val( pmMessage, ETO4_BMF_ARG_WF_TEMPLATE_NAME, &pszWFTemplateName));

    /* Create dispatcher request. */
    if(iRetCode == ITK_ok)
    {
        ppszTransArgList =  (char**) MEM_alloc(1 * sizeof(char *));

        ppszTransArgList[0] = (char *)MEM_alloc((int)((tc_strlen( ETO4_BMF_ARG_WF_TEMPLATE_NAME ) + tc_strlen( pszWFTemplateName)) * sizeof(char) + 1));

        sprintf(ppszTransArgList[0], "%s=%s", ETO4_BMF_ARG_WF_TEMPLATE_NAME, pszWFTemplateName);
    }

    ETO4_ITKCALL(iRetCode, DISPATCHER_create_request( ETO4_ETS_ARG_VALUE_PROVIDER, ETO4_ETS_SERVICE_INITIATE_WORKFLOW, ETO4_ETS_ARG_VALUE_PRIORITY,
		                                              0, 0, 0, 1, &(tNewItem), &(*ptNewItemRev), 1, (const char**)ppszTransArgList, 
                                                      ETO4_ETS_ARG_VALUE_TRIGGER, 0, 0, 0, &tTransReq ));

    ETO4_MEM_TCFREE( pszPropName );
    ETO4_MEM_TCFREE( pszProjectName );
    ETO4_MEM_TCFREE( pszTrimmedName );
	ETO4_MEM_TCFREE( pszWFTemplateName );
	ETO4_MEM_TCFREE( ppszTransArgList );
    return iRetCode;
}

/**
 *
 * This extension is registered at "ETO4_ProjectRevision" object as a post-action for the operation "IMAN_save". Function is designed
 * mainly to clone folder hierarchy during 'Revise operation' although this function will get invoked during "Revise..." or when 
 * "ETO4_ProjectRevision" is saved to database or when performing a "Save as..." of a "ETO4_ProjectRevision".
 *
 * Function is responsible for cloning the Folder structure for new 'ETO4_ProjectRevision' from the revision based on
 * which new ETO4_Project Item it is created. Only Folders are created new during cloning process rest of the objects present in the
 * source folder are referenced in the new folder hierarchy.
 *
 *
 * @param[in] pmMessage Received message
 * @param[in] args      List of arguments containing information
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_create_phase_folders_iman_save_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
)
{
    /* Message "IMAN_save" */
    int    iRetCode           = ITK_ok;
    tag_t  tItemRev           = NULLTAG;
    int    iNoOfStatuses      = 0;
    tag_t* ptStatusTagList    = NULL;
    char*  pszItemRevName     = NULL;
    char*  pszWFTemplateName  = NULL;

    tItemRev = va_arg(args, tag_t);

    ETO4_ITKCALL(iRetCode, WSOM_ask_release_status_list(tItemRev, &iNoOfStatuses, &ptStatusTagList));

    if(iNoOfStatuses == 0)
    {
		int     iRevCnt    = 0;
		tag_t   tItem      = NULLTAG;
		tag_t*  ptItemRev  = NULL;
		logical bCreateDoc = false;

		ETO4_ITKCALL(iRetCode, AOM_ask_value_tag( tItemRev, ETO4_ATTR_ITEM_TAG, &tItem));

		ETO4_ITKCALL(iRetCode, ITEM_list_all_revs(tItem, &iRevCnt, &ptItemRev));

		ETO4_MEM_TCFREE( ptItemRev );

		if(iRevCnt > 1)
		{
			int     iFolderCnt    = 0;
			tag_t*  ptFolder      = NULL;
			
			ETO4_ITKCALL(iRetCode, ETO4_grm_get_secondary_obj_of_type(tItemRev, ETO4_IMAN_REFERENCE_RELATION, ETO4_CLASS_PHASE_FOLDER,
																		&ptFolder, &iFolderCnt));
			if(iFolderCnt == 0)
			{
				ETO4_ITKCALL(iRetCode, ETO4_clone_folder_structure(tItemRev));

				//ETO4_ITKCALL(iRetCode, ETO4_bmf_get_arg_val( pmMessage, ETO4_BMF_ARG_WF_TEMPLATE_NAME, &pszWFTemplateName));
		  //     
				///* Change Item revision has to get the status "ETO4_Requirements_Phase", use the workflow "SYSTEM_Set-Status-Requirements-Phase" */
				//ETO4_ITKCALL(iRetCode, ETO4_initiate_workflow(pszWFTemplateName, tItemRev ) );
			}

			ETO4_MEM_TCFREE( ptFolder );
		}
    }

    ETO4_MEM_TCFREE( pszItemRevName );
    ETO4_MEM_TCFREE( pszWFTemplateName );
    ETO4_MEM_TCFREE( ptStatusTagList );
    return iRetCode;
}

/**
 * Function is registered at "ETO4_Project" object as a post-action for the operation "ITEM_create_from_rev". This Extension is 
 * getting fired when doing a "Save as�" of a "ETO4_ProjectRevision".
 *
 * Function is responsible for cloning the Folder structure for new 'ETO4_ProjectRevision' from the revision based on
 * which new ETO4_Project Item it is created. Only Folders are created new during cloning process rest of the objects present in the
 * source folder are referenced in the new folder hierarchy.
 *
 *
 * @param[in] pmMessage Received message
 * @param[in] args      List of arguments containing information
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_create_phase_folders_item_cre_from_rev_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
)
{
    /* Message "ITEM_create_from_rev" */
    int         iRetCode       = ITK_ok;
    tag_t       tOldItem       = va_arg(args, tag_t);
    tag_t       tOldItemRev    = va_arg(args, tag_t);
    const char* pszItemID      = va_arg(args, char*);
    const char* pszItemRevID   = va_arg(args, char*);
    tag_t*      ptNewItem      = va_arg(args, tag_t*);
    tag_t*      ptNewItemRev   = va_arg(args, tag_t*);

    //ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_value(ETO4_PREF_PROJECT_NAME_PROP_NAME, &pszPropName));

    //if(pszPropName == NULL)
    //{
    //    ETO4_ITKCALL(iRetCode, AOM_ask_value_string( tOldItemRev, ETO4_ATTR_OBJECT_NAME_ATTRIBUTE, &pszProjectName));
    //}
    //else
    //{
    //    ETO4_ITKCALL(iRetCode, AOM_ask_value_string( tOldItemRev, pszPropName, &pszProjectName));
    //}

    //ETO4_ITKCALL(iRetCode, ETO4_create_phase_folders(pszProjectName, ptNewItemRev, true));

    ETO4_ITKCALL(iRetCode, ETO4_clone_folder_structure(*ptNewItemRev));

    return (iRetCode);
}

/**
 * Extenstion is registered at the property "contents" of the object "ETO4_Phase_Folder" as a post-action for the operation
 * "PROP_set_value_tags". It is getting fired when a new document revision is getting pasted to the "ETO4_Phase_Folder" object.
 *
 * Every time a item revision gets attached to a phase folder, the item has to get the project(s) of the folder
 * assigned. If no project is assigned to the folder the paste must not be allowed and error message is displayed to the user.
 *
 * @param[in] pmMessage Received message
 * @param[in] args      List of arguments containing information
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_set_contents_prop_post_action
(
    METHOD_message_t* pmMessage, 
    va_list args
)
{
    int         iRetCode          = ITK_ok;
    int         iTotalContents    = 0;
    int         iPrjCount         = 0;
    tag_t       tItem             = NULLTAG;
    tag_t       tPropTypeContent  = NULLTAG;
    tag_t       tETOFolder        = NULLTAG;
    tag_t*      ptContents        = NULL;
    tag_t*      ptProjects        = NULL;
    const char* pszPropName       = NULL;
    
    /*  Get values for property "contents" PROP_set_value_tags */
    tPropTypeContent   = va_arg(args, tag_t);
    iTotalContents     = va_arg(args, int);    
    ptContents         = va_arg(args, tag_t*);
 
    /* Get the property name */
    pszPropName = pmMessage->prop_name;
    if( pszPropName == NULL || tc_strcmp( pszPropName, ETO4_ATTR_CONTENTS ) != 0 )
    {
        return iRetCode;
    }

    /* Get the object tag from message : tag of Folder  */
    tETOFolder = pmMessage->object_tag;

    /* Get the project assigned to the Folder   */
    ETO4_ITKCALL(iRetCode, AOM_ask_value_tags(tETOFolder, ETO4_ATTR_PROJECT_LIST, &iPrjCount, &ptProjects));

    if(iTotalContents > 0 && iTotalContents > iContentCnt)
    {
        char*   pszFolderType    = NULL;
        logical bIsTypeOfPhaseFL = false;

        ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(tETOFolder, ETO4_CLASS_PHASE_FOLDER, &bIsTypeOfPhaseFL));

        if(bIsTypeOfPhaseFL == true)
        {
            ETO4_ITKCALL(iRetCode, ETO4_obj_ask_type(tETOFolder, &pszFolderType));
        }
        else
        {
            ETO4_ITKCALL(iRetCode, AOM_ask_value_string(tETOFolder, ETO4_ATTR_DESCRIPTION_ATTRIBUTE, &pszFolderType));
        }

        for(int iDx = iTotalContents; iDx > iContentCnt && iRetCode == ITK_ok; iDx--)
        {
            logical bIsPhaseSubFL = false;

		    ETO4_ITKCALL(iRetCode, ETO4_obj_is_type_of(ptContents[iDx-1], ETO4_CLASS_PHASE_SUB_FOLDER, &bIsPhaseSubFL));

            if(bIsPhaseSubFL == true)
            {
                ETO4_ITKCALL(iRetCode, ETO4_obj_set_str_attribute_val(ptContents[iDx-1], ETO4_ATTR_DESCRIPTION_ATTRIBUTE, pszFolderType));
            }

            if(iPrjCount > 0)
            {
                int    iPrefValCnt  = 0;
                char** ppszPrefVals = NULL;

                ETO4_ITKCALL(iRetCode, ETO4_pref_get_string_values( ETO4_PREF_FOLDER_CONTENT_PROPAGATE_OBJECTS, &iPrefValCnt, &ppszPrefVals ));

	            if( bIsPhaseSubFL == true)
	            {
		            int     iNoOfRef         = 0;
		            tag_t*  ptReferences     = NULL;
        			
		            ETO4_ITKCALL(iRetCode, FL_ask_references(ptContents[iDx-1], FL_fsc_as_ordered, &iNoOfRef, &ptReferences));

		            if (iNoOfRef > 0)
		            {
			            /*  Apply Propagation Information to contents of  ETO4_Phase_Sub_Folder which is being pasted to 'ETO4_Project_Phase_Folder' */
                        ETO4_ITKCALL(iRetCode, ETO4_validate_and_propagation_info(pszFolderType, tETOFolder, iPrefValCnt, ppszPrefVals,
                                                                                    iNoOfRef, ptReferences ));
                    }

		            /*   Apply Propagation Information to ETO4_Phase_Sub_Folder */
                    ETO4_ITKCALL(iRetCode, ETO4_validate_and_propagation_info(pszFolderType, tETOFolder, iPrefValCnt, ppszPrefVals, 
                                                                                1, &(ptContents[iDx-1])));
		            ETO4_MEM_TCFREE( ptReferences );
	            }
	            else
	            {
                    ETO4_ITKCALL(iRetCode, ETO4_validate_and_propagation_info(pszFolderType, tETOFolder, iPrefValCnt, ppszPrefVals, 
                                                                                1, &(ptContents[iDx-1])));
	            }
                
                ETO4_MEM_TCFREE( ppszPrefVals );
            }
        }
        
        ETO4_MEM_TCFREE( pszFolderType );
    }        

    ETO4_MEM_TCFREE( ptProjects );
    return iRetCode;
}

/**
 * Function validate the object type that is pasted to the folder. It made sure that only objects of the Type that are
 * specified in the preference 'ETO4_Phase_Folder_Allowed_Object_Types' are allowed to be  pasted to the phase folder. 
 * If this preference does not exist any Object can be pasted in these folders. This extension is registered at the property 
 * "contents" of the object "ETO4_Phase_Folder" and 'ETO4_Phase_Sub_Folder' as a pre-condition for the operation
 * "PROP_set_value_tags". It is getting fired when a TC Business Object is pasted to the sub folder type of "ETO4_Phase_Folder" 
 * or if TC Business Object is pasted directly to'ETO4_Phase_Sub_Folder' type.
 *
 * @param[in] pmMessage  Received message
 * @param[in] args       List of arguments containing Information.
 *
 * @retval INVALID_OBJ_TYPE_PASTED_TO_FOLDER if object pasted is incorrect
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_paste_to_folder_pre_condition
(
    METHOD_message_t *pmMessage,
    va_list args)
{
    int          iRetCode           = ITK_ok;
    int          iTotalContents     = 0;
    tag_t        tItem              = NULLTAG;
    tag_t        tPropTypeContent   = NULLTAG;
    tag_t        tFolder            = NULLTAG;
    tag_t*       ptContents         = NULL;
    const char*  pszPropName        = NULL;
    
    /*  Get values for property "contents" PROP_set_value_tags  */
    tPropTypeContent   = va_arg(args, tag_t);
    iTotalContents     = va_arg(args, int);    
    ptContents         = va_arg(args, tag_t*);
 
    /* Get the property name    */
    pszPropName = pmMessage->prop_name;
    if( pszPropName == NULL || tc_strcmp( pszPropName, ETO4_ATTR_CONTENTS ) != 0 )
    {
        return iRetCode;
    }

    /* Get the object tag from message : tag of Folder  */
    tFolder = pmMessage->object_tag;

	ETO4_ITKCALL(iRetCode, ETO4_validate_object_for_past_action_on_phase_folder(ptContents, iTotalContents, tFolder));

    /* This function will set iContentCnt variable to actual number of folder contents before pasting and cutting objects
       from folders.
    */
    ETO4_ITKCALL(iRetCode, FL_ask_size(tFolder, &iContentCnt));

    return iRetCode;
}


/**
 * This function performs pre-condition validation on "TC_Project" object for operation "PROJECT_create". It is getting 
 * fired when a new project is created using the "Project" application.
 * This extension checks that no change object of the type "ETO4_Phase_Plan" with the item ID same as the desired project ID 
 * already exists in Teamcenter before doing the project creation. Otherwise the change object cannot be created afterwards. 
 * The user will get a error message.
 *
 * @param[in] pmMessage  Received message
 * @param[in] args       List of arguments containing Information
 *
 * @retval ITEM_ALREADY_EXIST_WITH_GIVEN_ITEM_ID If ETO4_Plan_Phase exist with same Project ID.
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int ETO4_create_project_pre_condition
(
    METHOD_message_t *pmMessage,
    va_list args
)
{
    int         iRetCode       = ITK_ok;
    int         iItemCount     = 0;
    const char* pszProjectID   = va_arg(args, char*);
    const char* pszAttrs       = ETO4_ATTR_ITEM_ID;    
    tag_t*      ptItems        = NULL; 

    ETO4_ITKCALL(iRetCode, ITEM_find_items_by_key_attributes(1, &pszAttrs, &pszProjectID, &iItemCount, &ptItems));

    if( iItemCount > 0 )
    {
        iRetCode = ITEM_ALREADY_EXIST_WITH_GIVEN_ITEM_ID;
        EMH_store_error(EMH_severity_error, iRetCode);
    }

    ETO4_MEM_TCFREE( ptItems );

    return iRetCode;
}