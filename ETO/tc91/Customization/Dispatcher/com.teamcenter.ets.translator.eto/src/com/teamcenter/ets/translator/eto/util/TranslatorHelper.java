/*==============================================================================

          Copyright (c) 2007 UGS The PLM Company
                   Unpublished - All rights reserved

================================================================================
File description:   See description below.

    Filename:   TaskPrep.java
    Module  :   com.teamcenter.ets.translator.eto

================================================================================
                     Modification Log
================================================================================
Date        User           Change Description
21-Feb-2012 glangenbacher  Initial Version
11-Dec-2012 glangenbacher  Upgraded code for Teamcenter 9.1

==============================================================================*/

package com.teamcenter.ets.translator.eto.util;

import com.teamcenter.tstk.util.log.ITaskLogger;
import com.teamcenter.tstk.util.log.LoggerFactoryUtil;

public class TranslatorHelper 
{

	protected ITaskLogger m_zTaskLogger = LoggerFactoryUtil.getDefaultTaskLogger();

	public TranslatorHelper(ITaskLogger zTaskLogger)
	{
		this.m_zTaskLogger = zTaskLogger;
	}

	/**
	 * 
	 * Gets the argument value for the given argument name for the 
	 * current translation request
	 * 
	 * @param argName				Argument name
	 * @param translatorArgsKeys	List of argument keys
	 * @param translatorArgsData	List of argument data
	 * @return	Value for the given key
	 * @throws Exception If the number of keys and data differ 
	 */	
	public String getTranslationArgValue(String argName, String[] translatorArgsKeys, String[] translatorArgsData) throws Exception 
	{    	
		String value = "";

		if (translatorArgsKeys.length == translatorArgsData.length)
		{
			for (int j=0; j < translatorArgsKeys.length; j++)
			{
				if (translatorArgsKeys[j].equalsIgnoreCase(argName))
				{
					this.m_zTaskLogger.debug("TranslatorHelper -> Found key '" + argName + "' with value '" + translatorArgsData[j] + "'");

					// check if the argument has a proper value and is not null
					if (translatorArgsData[j] != null && !translatorArgsData[j].isEmpty() && !translatorArgsData[j].equalsIgnoreCase("(null)"))
					{
						value = translatorArgsData[j];
					}
					break;
				}
			} 			
		}
		else
		{
			new Exception("Misetotch in key value pairs of translator arguments. This is most likely because of some arguments do not have a value.");
		}

		return value;
	}
}
