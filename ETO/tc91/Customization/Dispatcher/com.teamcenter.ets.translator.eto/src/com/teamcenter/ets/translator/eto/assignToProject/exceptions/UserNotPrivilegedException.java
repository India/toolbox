/*==============================================================================

          Copyright (c) 2007 UGS The PLM Company
                   Unpublished - All rights reserved

================================================================================
File description:   See description below.

    Filename:   TaskPrep.java
    Module  :   com.teamcenter.ets.translator.eto

================================================================================
                     Modification Log
================================================================================
Date        User           Change Description
21-Feb-2012 glangenbacher  Initial Version

==============================================================================*/

package com.teamcenter.ets.translator.eto.assignToProject.exceptions;

/**
 * Custom Exception class used if user is not a privileged member of the
 * project. He has no project assignment privileges.
 * This is most likely if the user is an unprivileged project member.
 * 
 * @author	Gabriel Langenbacher
 * @version	1.0, 23.02.2012
 * 
 */
public class UserNotPrivilegedException extends Exception {

	/**
	 * Determines if a de-serialized file is compatible with this class.
	 * 
	 * Maintainers must change this value if and only if the new version
	 * of this class is not compatible with old versions. See Sun docs
	 * for <a href=http://java.sun.com/products/jdk/1.1/docs/guide
	 * /serialization/spec/version.doc.html> details. </a>
	 * 
	 * Not necessary to include in first version of the class, but
	 * included here as a reminder of its importance.
	 */
	private static final long serialVersionUID = 9091573542980502450L;

	public UserNotPrivilegedException(String string) {
		super(string);
	}	
}
