@echo off

set SUBFOLDER=%1
set WORKFLOW=%2

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import Teamcenter workflows

if exist "Workflows\%SUBFOLDER%\%WORKFLOW%.xml" (
    echo -----------------------------------------------------------------------
    echo plmxml_import -u=infodba -p=%DBAPASS_TMP% -g=dba -xml_file="Workflows\%SUBFOLDER%\%WORKFLOW%.xml" -transfermode=workflow_template_overwrite -log=..\logs\%COMPUTERNAME%\%WORKFLOW%.log
    echo.
    plmxml_import -u=infodba -p=%DBAPASS% -g=dba -xml_file="Workflows\%SUBFOLDER%\%WORKFLOW%.xml" -transfermode=workflow_template_overwrite -log=..\logs\%COMPUTERNAME%\%WORKFLOW%.log
    echo -----------------------------------------------------------------------
)

if not exist "Workflows\%SUBFOLDER%\%WORKFLOW%.xml" echo ERROR: File "Workflows\%SUBFOLDER%\%WORKFLOW%.xml" does not exist.
echo.

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
