/**
 * Copyright (c) 2016 LMtec GmbH
 * Unpublished - All rights reserved
 * 
 * Class that overwrites NewBOWizard
 * 
 * @file	PXCBOWizard.java
 * @author	E.Dizdarevic
 * @date	13.05.2016
 * 
 */

package com.teamcenter.rac.commands.newdoc;

import java.io.File;

import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.ui.commands.create.bo.NewBOWizard;
import com.teamcenter.rac.util.SWTUIUtilities;

/**
 * Class that overwrites NewBOWizard
 * @author E.Dizdarevic
 */
public class ASPBOWizard extends NewBOWizard {

	TCComponent dropComponent; // drop component (for example TC folder or Item revision)
	public TCComponent m_createdCmp;
	protected ASPBOOperation op ;
	protected File[] dropFiles ;
	protected TCComponent[] newComponents;
	boolean finished ;

	/**
	 * PXCBOWizard constructor
	 * Set dropComponent
	 * @param arg0
	 * @param c
	 */
	public ASPBOWizard(String arg0, TCComponent c, File[] df) {
		super(arg0);
		dropComponent = c;
		dropFiles = df ;
		finished = false ;
	}

	/* (non-Javadoc)
	 * @see com.teamcenter.rac.ui.commands.create.bo.NewBOWizard#postSuccessfulFinish()
	 * 
	 * After successful finish, assigns newComponent and sets finished=true
	 */
	protected void postSuccessfulFinish() {
		Runnable runnable = new Runnable() {
			public void run() {
				getWizardDialog().close();
			}
		};
		SWTUIUtilities.asyncExec(runnable);
		newComponents = op.newComponents ;
		finished = true ;
	}
	
    /* (non-Javadoc)
     * @see com.teamcenter.rac.ui.commands.create.bo.NewBOWizard#getOperationClass()
     * 
     * Overwrites getOperationClass to use PXCBOOperation
     */
    public ASPBOOperation getOperationClass()
    {
    	op = new ASPBOOperation( dropComponent, dropFiles ) ;
    	return op ;
    }
}
