/**
 * Copyright (c) 2016 LMtec GmbH
 * Unpublished - All rights reserved
 * 
 * Class that overwrites NewBOOperation
 * 
 * @file	PXCBOOperation.java
 * @author	E.Dizdarevic
 * @date	13.05.2016
 * 
 */

package com.teamcenter.rac.commands.newdoc;

import java.io.File;

import org.eclipse.jface.viewers.StructuredSelection;

import com.teamcenter.rac.aif.ICommandListener;
import com.teamcenter.rac.aif.common.AIFDataBean;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.ui.commands.RACUICommandsActivator;
import com.teamcenter.rac.ui.commands.create.bo.NewBOOperation;
import com.teamcenter.rac.ui.commands.paste.PasteDataBean;
import com.teamcenter.rac.ui.commands.paste.PasteJob;

/**
 * Class that overwrites NewBOOperation
 * @author E.Dizdarevic
 */
public class ASPBOOperation extends NewBOOperation {

	private TCComponent dropComponent;
	protected TCComponent[] newComponents;
	private File[] dropFiles ;

	/**
	 * PXCBOOperation constructor
	 * @param c
	 */
	public ASPBOOperation(TCComponent c, File[] df) {
		dropComponent = c; // store c in dropComponent
		dropFiles = df;
	}
	
	/* (non-Javadoc)
	 * @see com.teamcenter.rac.ui.commands.create.bo.NewBOOperation#pasteNewComponent()
	 * 
	 * Overwrite the function pasteNewComponent
	 * Paste newComponent in dropComponent
	 */
	protected void pasteNewComponent(String s, ICommandListener icommandlistener) throws Exception {

		if (dropFiles == null) {
			newComponents = new TCComponent[1];
			newComponents[0] = getNewComponent();
		} else {
			newComponents = new TCComponent[dropFiles.length];
			newComponents[0] = getNewComponent();

			for (int i = 1; i < dropFiles.length; i++) {
				try {
					this.createComponent(getMonitor());
				} catch (TCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				newComponents[i] = getNewComponent();
			}
		}
		
		TCSession session = (TCSession) RACUICommandsActivator.getDefault().getSession();

		StructuredSelection structuredselection = new StructuredSelection(dropComponent);
		PasteDataBean pastedatabean ;
        if(s != null)
            pastedatabean = new PasteDataBean(new AIFDataBean(session, structuredselection), newComponents, s);
        else
            pastedatabean = new PasteDataBean(new AIFDataBean(session, structuredselection), newComponents);
		PasteJob pastejob = new PasteJob(pastedatabean);
		pastejob.setFailBackFlag(true);
		pastejob.executeOperation();
	}
}
