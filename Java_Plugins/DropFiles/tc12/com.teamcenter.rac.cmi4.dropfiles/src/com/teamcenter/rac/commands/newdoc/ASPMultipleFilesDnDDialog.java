/**
 * Copyright (c) 2016 LMtec GmbH
 * Unpublished - All rights reserved
 * 
 * Class that overwrites MultipleFilesDnDDialog
 * 
 * @file	PXCMultipleFilesDnDDialog.java
 * @author	E.Dizdarevic
 * @date	13.05.2016
 * 
 */

package com.teamcenter.rac.commands.newdoc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.AIFUtility;
import com.teamcenter.rac.commands.newdataset.MultipleFilesNewDatasetDialog;
import com.teamcenter.rac.common.TCUtilities;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentFolderType;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.ui.commands.create.bo.NewBOModel;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.util.wizard.extension.BaseExternalWizardDialog;
import com.teamcenter.services.internal.rac.core.DataManagementService;
import com.teamcenter.services.internal.rac.core._2008_06.DataManagement.CreateRelationsResponse;
import com.teamcenter.services.internal.rac.core._2008_06.DataManagement.Relationship;


/**
 * Class that overwrites MultipleFilesDnDDialog
 * 
 * @author E.Dizdarevic
 */
public class ASPMultipleFilesDnDDialog extends Dialog implements IPropertyChangeListener {
	private TCSession tcSession = null;
	private File[] dropFiles;
	private TCComponent dropComponent;
	private Shell _parentShell;
	private InterfaceAIFComponent _c;
	private Button checkbox1;
	private Button radiobox1;
	private Button radiobox2;
	private Button radiobox3;
	TCComponent[] atccomponent;

	/**
	 * Constructor for class PXCMultipleFilesDnDDialog
	 * 
	 * @param parentShell
	 * @param theFiles
	 * @param c
	 */
	public ASPMultipleFilesDnDDialog(Shell parentShell, File[] theFiles, InterfaceAIFComponent c) {
		super(parentShell);
		dropFiles = theFiles;
		tcSession = (TCSession) AIFDesktop.getActiveDesktop().getCurrentApplication().getSession();
		dropComponent = (TCComponent) c;
		_parentShell = parentShell;
		_c = c;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 * 
	 * Overwrite Dialog configureShell
	 * Aim is to set the Dialog Title
	 */
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(Registry.getRegistry(this).getString("DialogTitle"));
	}

	private String getFileExtension( File file )
	{
		String filename = file.getName() ;
		int ind = filename.lastIndexOf('.');
		if (ind == 0)
		{
			return "" ;
		}
		else
		{
			String extension = filename.substring(ind+1);
			return extension ;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 * 
	 * Does not create any UI - If selected id folder, then starts
	 * importInFolder , else start OOTB drag and drop dialog
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		radiobox1 = null;
		radiobox2 = null;
		radiobox3 = null;
		checkbox1 = null;

		// EPEP-7014 - dialog redesign
		Composite composite = new Composite(parent, SWT.EMBEDDED);
		GridLayout gl = new GridLayout(1, false);
		gl.marginLeft = 10;
		gl.marginRight = 10;
		composite.setLayout(gl);

		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		composite.setLayoutData(data);

		String str = String.format(Registry.getRegistry(this).getString("Question"));
		final Label label1 = new Label(composite, SWT.NONE);
		label1.setText(str);

		boolean only_ids_file = true;
		for (int i = 0; i < dropFiles.length && only_ids_file == true; i++) {
			if (getFileExtension(dropFiles[i]).equals("ids") == false)
				only_ids_file = false;
		}

		if (only_ids_file == true)
			return composite;

		radiobox1 = new Button(composite, SWT.RADIO);
		radiobox1.setText(Registry.getRegistry(this).getString("Radiobox1Text"));
		radiobox1.setSelection(true);
		radiobox1.setEnabled(true);
		radiobox1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkbox1.setEnabled(radiobox3.getSelection());
			}
		});

		radiobox2 = new Button(composite, SWT.RADIO);
		radiobox2.setText(Registry.getRegistry(this).getString("Radiobox2Text"));
		radiobox2.setSelection(false);
		radiobox2.setEnabled(true);
		if (!(dropComponent instanceof TCComponentFolder) && !(dropComponent instanceof TCComponentItem))
			radiobox2.setEnabled(false);
		radiobox2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkbox1.setEnabled(radiobox3.getSelection());
			}
		});

		radiobox3 = new Button(composite, SWT.RADIO);
		radiobox3.setText(Registry.getRegistry(this).getString("Radiobox3Text"));
		radiobox3.setSelection(false);
		radiobox3.setEnabled(true);
		if (!(dropComponent instanceof TCComponentFolder))
			radiobox3.setEnabled(false);
		radiobox3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkbox1.setEnabled(radiobox3.getSelection());
			}
		});

		checkbox1 = new Button(composite, SWT.CHECK);
		checkbox1.setText(Registry.getRegistry(this).getString("Checkbox1Text"));
		checkbox1.setSelection(true);
		checkbox1.setEnabled(false);

		/*Display display = composite.getDisplay();
		Color red = display.getSystemColor(SWT.COLOR_RED);
		str = String.format(Registry.getRegistry(this).getString("Info"));
		Label label2 = new Label(composite, SWT.NONE);
		Font font = new Font(display, "Tahoma", 10, SWT.ITALIC);
		label2.setText(str);
		label2.setFont(font);
		label2.setForeground(red);*/

		return composite;
	}

	/**
	 * Function that starts PXCBOWizard + MultipleFilesNewDatasetDialog
	 * 
	 * @param createDoc
	 * 
	 * @param itemType
	 */
	private void importInFolder(String docBaseType, boolean one_document_per_file, boolean doc_name_equal_file_name) {
		try {
			String wizardId = "com.teamcenter.rac.ui.commands.create.bo.NewBOWizard";
			ASPBOWizard newbowizard;
			if ((dropComponent instanceof TCComponentFolder) && one_document_per_file == true)
				newbowizard = new ASPBOWizard(wizardId, dropComponent, dropFiles);
			else
				newbowizard = new ASPBOWizard(wizardId, dropComponent, null);
			NewBOModel m_boModel = new NewBOModel(this);
			m_boModel.setSession(tcSession);
			m_boModel.reInitializeTransientData();
			m_boModel.setFrame(AIFUtility.getActiveDesktop());
			m_boModel.setAllowUserToSelectRelation(false);
			newbowizard.setBOModel(m_boModel);
			newbowizard.setWindowTitle(Registry.getRegistry(this).getString("NewBOWizardTitle"));
			newbowizard.setRevisionFlag(false);
			newbowizard.setDefaultType(docBaseType);
			BaseExternalWizardDialog dialog = new BaseExternalWizardDialog(AIFUtility.getActiveDesktop().getShell(),
					newbowizard);
			dialog.create();
			newbowizard.retrievePersistedDialogSettings(dialog);
			newbowizard.setWizardDialog(dialog);
			dialog.open();
			dialog = null;
			m_boModel = null;

			// if newbowizard was cancelled, then finish=false
			// in this case, we don't want to import files
			if (newbowizard.finished == false)
				return;

			if ((dropComponent instanceof TCComponentFolder) && one_document_per_file == true) {
				if (newbowizard.newComponents.length != dropFiles.length)
					return;
				
				TCComponentFolderType folderTypeComponent = ( TCComponentFolderType ) tcSession.getTypeComponent( "Folder" );
				TCComponentFolder newFolder = folderTypeComponent.create( "drop", "drop", "Folder" );
				MultipleFilesNewDatasetDialog dlg = new MultipleFilesNewDatasetDialog(_parentShell, dropFiles, newFolder);
				dlg.open();
				AIFComponentContext[] children = newFolder.getChildren();
				if (children.length != dropFiles.length)
					return;
				
				for (int i = 0; i < children.length; i++) {
					TCComponentDataset dataset = (TCComponentDataset) children[i].getComponent();
					TCComponentItem item = (TCComponentItem) newbowizard.newComponents[i];
					if (doc_name_equal_file_name == true)
					{
						String dataset_name=dataset.getProperty("object_name");
						int ind = dataset_name.lastIndexOf('.') ;
						String item_name;
						if ( ind == -1 ) // in case of no extension
							item_name = dataset_name;
						else
							item_name = dataset_name.substring(0, dataset_name.lastIndexOf('.'));
						item.setStringProperty("object_name", item_name);
						item.getLatestItemRevision().setStringProperty("object_name", item_name);
					}

					TCComponentItemRevision revision = item.getLatestItemRevision();
					
					Relationship relation;
					relation = new Relationship();
					relation.primaryObject = revision;
					relation.secondaryObject = dataset;
					relation.relationType = "IMAN_specification";
					DataManagementService dmService = DataManagementService.getService(revision.getSession());
					CreateRelationsResponse response = dmService.createCachedRelations(new Relationship[] { relation });
					if (response.serviceData.sizeOfPartialErrors() > 0) {
						MessageBox.post("Unable to create IMAN_specification", "Error", MessageBox.ERROR);
						break;
					}
				}
				newFolder.delete();
			} else { // one document for all files
				if (newbowizard.newComponents.length != 1)
					return;
				TCComponentItem item = (TCComponentItem) newbowizard.newComponents[0];
				TCComponentItemRevision revision = item.getLatestItemRevision();
				MultipleFilesNewDatasetDialog dlg = new MultipleFilesNewDatasetDialog(_parentShell, dropFiles,
						revision);
				dlg.open();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 * 
	 * Action on pushbutton Ok was pressed If dropComponent is a TC folder, then
	 * start importInFolder Else start OOTB import MultipleFilesNewDatasetDialog
	 */
	@Override
	protected void okPressed() {
		if (radiobox1 != null) {
			if (radiobox1.getSelection() == true) {
				super.okPressed();
				MultipleFilesNewDatasetDialog dlg = new MultipleFilesNewDatasetDialog(_parentShell, dropFiles, _c);
				dlg.open();
			} else {
				boolean one_document_per_file = radiobox3.getSelection();
				boolean doc_name_equal_file_name = checkbox1.getSelection();
				super.okPressed();
				try {
					// EPEP-3681: check allowed to document types to be created
					String docBaseType = Registry.getRegistry(this).getString("PXCDocumentBaseType");
					String[] displayableDocTypes = TCUtilities.getDisplayableTypeNames(tcSession, docBaseType);
					List<String> tmp = new ArrayList<String>();
					for (int i = 0; i < displayableDocTypes.length; i++)
						tmp.add(displayableDocTypes[i]);
					System.out.println("displayableDocTypes=" + tmp.toString());
					if (displayableDocTypes.length == 0) {
						MessageBox.post("Your current login group and role do not allow you to create any document",
								"Information", MessageBox.INFORMATION);
					} else {
						importInFolder(docBaseType, one_document_per_file, doc_name_equal_file_name);
					}
				} catch (TCException e1) {
					MessageBox.post("Failed to get displayable doc type names", "Error", MessageBox.ERROR);
					e1.printStackTrace();
				}
			}
		} else {
			super.okPressed();
			MultipleFilesNewDatasetDialog dlg = new MultipleFilesNewDatasetDialog(_parentShell, dropFiles, _c);
			dlg.open();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.util.IPropertyChangeListener#propertyChange(org.eclipse
	 * .jface.util.PropertyChangeEvent)
	 * 
	 * Needed for compilation.
	 */
	public void propertyChange(PropertyChangeEvent event) {
	}
}
