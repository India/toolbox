package com.lmtec.rac.sma;


import java.net.Proxy.Type;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.supercsv.comment.CommentStartsWith;
import org.supercsv.prefs.CsvPreference;

import com.lmtec.rac.PopupWindow;
import com.lmtec.rac.wizards.importer.CsvPreferences;
import com.lmtec.rac.wizards.importer.FileColumn;
import com.lmtec.rac.wizards.importer.ImportWizard;
import com.lmtec.rac.wizards.importer.ParseBigDecimalToMili;
import com.lmtec.rac.wizards.importer.ParseDoubleToMili;
import com.lmtec.rac.wizards.importer.SelectFilePage;
import com.lmtec.rac.wizards.importer.ImportFilePage;
import com.lmtec.rac.wizards.importer.SelectFileColumnsPage;
import com.lmtec.rac.wizards.importer.bom.BomLineImport;
import com.lmtec.rac.wizards.importer.bom.BomLineService;
import com.lmtec.rac.wizards.importer.spec.IImportProvider;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;

public class UpdateBOM extends AbstractHandler {

	private static String MSG_TITLE = "Update BOM";
	private IWorkbenchWindow window = null;
	private Shell shell = null;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		//boolean debug = true;
		boolean debug = false;
		
		// get eclipse window and shell
		window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		shell = window.getShell();
		
		BomLineService rowService;
		try{
			rowService = new BomLineService();
			rowService.setRowClazz(SMABomLine.class);
			rowService.readBom();
		} catch (TCException e) {
			PopupWindow.errorMessage(shell, MSG_TITLE, "An error has occured, cannot read the ItemRevision from the server");
			e.printStackTrace();
			return null;
		}
		
		final IImportProvider importProvider = new BomLineImport(SMABomLine.class);
		
		// Construct the properties to update
		importProvider.addColumn(new FileColumn("Pos.-ID", "findNo", String.class, new String[]{"find no","findno","pos.-id","pos", "pos id", "posid", "pos_id"}, false, false));
		importProvider.addColumn(new FileColumn("Reference Designator", "refDes", String.class, new String[]{"refdes","ref des","ref.des.","ref_des"}, false, false));
		importProvider.addColumn(new FileColumn("Number", "itemId", String.class, new String[]{"item id","itemid","partnr.","number","nummer"}, false, true));
		importProvider.addColumn(new FileColumn("Quantity", "quantity", String.class, new String[]{"quantity","qty"}, false, false));
		importProvider.addColumn(new FileColumn("Matrix", "transformation", Double.class, null, false, false));
		importProvider.addColumn(new FileColumn("X Pos", "t0", Double.class, new String[]{"x_Cor"}, new ParseBigDecimalToMili(), false, false));
		importProvider.addColumn(new FileColumn("Y Pos", "t1", Double.class, new String[]{"y_Cor"}, new ParseBigDecimalToMili(), false, false));
		importProvider.addColumn(new FileColumn("Z Pos", "t2", Double.class, new String[]{"z_Cor"}, new ParseBigDecimalToMili(), false, false));
		importProvider.addColumn(new FileColumn("Rotation", "rotation", Integer.class, new String[]{"rotation"}, false, false));
		importProvider.addColumn(new FileColumn("Placement", "placement", String.class, new String[]{"placement side"}, false, false));
		importProvider.setFileRowService(rowService);
		
		// Check if the item is valid for BOM import
		TCComponentItemRevision itemRevision = null;
		String itemRevisionType = null;
		try {
			itemRevision = BomLineService.getItemRevision();
			itemRevisionType = itemRevision.getType();
		} catch (TCException e1) {
			e1.printStackTrace();
		}
	
		if (!debug && 
				( 
						itemRevision == null || 
						itemRevisionType == null || 
						!itemRevisionType.equals("SMA3_SMPRevision") 
				 )
			) 
		{
			PopupWindow.errorMessage(shell, MSG_TITLE, "The selected component is not valid for CSV BOM update. The import is only allowed for Semifinished Products.");
			return null;
		}
		
		// check read/write permission
        if (!debug && !rowService.getPermission()) {
        	PopupWindow.errorMessage(shell, MSG_TITLE, "You do not have the required access right to modify the item revision.");
        	return null;
        }
		
		// create the wizzard and the the title 
		ImportWizard wizzard = new ImportWizard("Update BOM");
		wizzard.setImportProvider(importProvider);
		
		// add specific file parsing settings to the preferences
		CsvPreferences.getInstance().put("CSV with single quotes", new CsvPreference.Builder('\'', ';', "\r\n").skipComments(new CommentStartsWith("#")).build());
		CsvPreferences.getInstance().put("CSV with double quotes", new CsvPreference.Builder('"', ';', "\r\n").skipComments(new CommentStartsWith("#")).build());
		
		// set the default file pasting setting to the importprovider
		importProvider.setCsvPreference("CSV with double quotes");
		
		// create the import file page
		SelectFilePage selectFilePage = new SelectFilePage(importProvider);
		selectFilePage.setDialogExt(new String[]{"*.csv"});
		selectFilePage.setTitle("Import File");
		selectFilePage.setDescription("The file must contain a header row. The column name 'Number' (Item ID) is mandatory (case insensitive). \nOther supported BOM attributes are: Pos.-ID, Reference Designator and Quantity");
		selectFilePage.setDebug(debug);
		//importProvider.setFilePath("C:\\Vollbest_PXC3.csv");
		importProvider.setFilePath("C:\\8000072773-C-Placementdata.csv");
		
		wizzard.addPage(selectFilePage);
		
		// create the validate file page
		IWizardPage validateFilePage = new SelectFileColumnsPage(importProvider);
		validateFilePage.setTitle("Validate Content");
		validateFilePage.setDescription("Select the columns to import or update");
		wizzard.addPage(validateFilePage);
		
		// create the Update BOM page
		ImportFilePage updateBOMPage = new ImportFilePage(importProvider);
		updateBOMPage.setTitle("Update BOM Lines");
		updateBOMPage.setDescription("Select lines to insert, update or remove");
		updateBOMPage.setEnableShowAllColums(false); // hide button to show all columns
		wizzard.addPage(updateBOMPage);
		
		WizardDialog dialog = new WizardDialog(shell, wizzard);
		dialog.open();
		return null;
	}
}
