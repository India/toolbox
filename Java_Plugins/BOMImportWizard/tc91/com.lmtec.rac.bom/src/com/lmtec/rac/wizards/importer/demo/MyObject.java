package com.lmtec.rac.wizards.importer.demo;

import com.lmtec.rac.wizards.importer.FileRow;
import com.lmtec.rac.wizards.importer.spec.IFileRow;

public class MyObject extends FileRow{
	
	private static final long serialVersionUID = 8081440821341654010L;
	private String id;
	private String name;
	
	@Override
	public String getId() {
		return id;
	}

	@Override
	public boolean equalId(Object obj) {
		if(obj == null || !(obj instanceof IFileRow))
			return false;
		
		FileRow row = (FileRow) obj;
			
		if(this.getId().equals(row.getId()))
			return true;
		
		return false;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
