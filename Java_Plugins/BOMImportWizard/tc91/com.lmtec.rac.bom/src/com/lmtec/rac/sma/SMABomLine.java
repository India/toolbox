package com.lmtec.rac.sma;

import java.math.BigDecimal;

import com.lmtec.rac.wizards.importer.bom.BomLine;


public class SMABomLine extends BomLine{
	
	private static final long serialVersionUID = 8444115278327807842L;
	private int rotation;
	private String placement;

	public SMABomLine() {
		this.setEnabled(true);
		//this.setQuantity("1");
		this.setFindNo("0");
	}
	
	@Override
	public String getId() {
		return ((getFindNo() == null) ? "" : getFindNo() ) + ((getRefDes() == null) ? "" : getRefDes());
	}

	@Override
	public void setT0(BigDecimal t0) {
		super.setT0(t0);
	}
	
	@Override
	public void setT1(BigDecimal t1) {
		super.setT1(t1);
	}
	
	@Override
	public void setT2(BigDecimal t2) {
		super.setT2(t2);
	}
	
	public int getRotation() {
		return rotation;
	}

	public void setRotation(int rotation) {
		this.rotation = rotation;

		switch (rotation) {
			case 0:
				
				break;
			
			case 90:
				
				break;
				
			case 180:
				
				break;
				
			case 270:
				
				break;
	
			default:
				
				break;
		}
	}

	public String getPlacement() {
		return placement;
	}

	public void setPlacement(String placement) {
		this.placement = placement;
		
		if(placement.equalsIgnoreCase("top")){
			
		}
		
		if(placement.equalsIgnoreCase("bottom")){
			
		}

	}

}
