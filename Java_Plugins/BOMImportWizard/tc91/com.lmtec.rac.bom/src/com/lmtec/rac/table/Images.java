package com.lmtec.rac.table;

import org.eclipse.swt.graphics.Image;

import com.lmtec.rac.Activator;

public enum Images {
	CHECKED(Activator.getImageDescriptor("icons/checked.gif").createImage()),
	UNCHECKED(Activator.getImageDescriptor("icons/unchecked.gif").createImage());
	
	private Image image;
	
	private Images(Image image){
        this.image = image;
    }
	
	public Image getImage() {
		return image;
	}
}
