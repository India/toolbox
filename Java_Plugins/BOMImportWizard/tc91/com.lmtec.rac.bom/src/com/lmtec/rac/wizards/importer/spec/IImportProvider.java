package com.lmtec.rac.wizards.importer.spec;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.supercsv.prefs.CsvPreference;

import com.lmtec.rac.wizards.importer.RowStatus;


public interface IImportProvider extends IRunnableWithProgress{
	
	/**
	 * Sets the {@link IFileRowService}
	 * @param rowService
	 */
	void setFileRowService(IFileRowService rowService);
	
	/**
	 * Sets the {@link IFileRowService}
	 * @return {@link IFileRowService}
	 */
	IFileRowService getFileRowService();
	
	/**
	 * Set the File Path
	 * @param String filePath
	 */
	void setFilePath(String filePath);
	
	/**
	 * Returns the File Path
	 * @return String
	 */
	String getFilePath();
	
	/**
	 * Returns the File Name
	 * @return String
	 */
	String getFileName();
	
	/**
	 * Get the {@link CsvPreference} used to parse the file
	 * @return {@link CsvPreference}
	 */
	CsvPreference getCsvPreference();
	
	/**
	 * Set the {@link CsvPreference} used to parse the file
	 * @param preference {@link CsvPreference} 
	 */
	void setCsvPreference(CsvPreference preference);
	
	/**
	 * Set the {@link CsvPreference} by String. <br />
	 * Calls the CsvPreferences.getInstance().get(String preferenceName)
	 * @param preferenceName {@link String} 
	 */
	void setCsvPreference(String preferenceName);
	
	/**
	 * Returns the header from the file
	 * @return String[]
	 */
	String[] getFileHeader() throws NullPointerException;
	
	/**
	 * Returns the header with the enabled columns. Disabled elements has null values.
	 * @return String[]
	 */
	String[] getFileHeaderEnabled() throws NullPointerException;
	
	/**
	 * Opens the file from the getFilePath()
	 * @throws IOException
	 */
	void readFile() throws IOException;
	
	/**
	 * Close the file if open
	 * @throws IOException
	 */
	void closeFile() throws IOException;
	
	/**
	 * Opens and close the file, reads each row in the opened file
	 * @throws IOException
	 */
	void readFileRows() throws IOException;
	
	/**
	 * Get the {@link IFileColumn}s in a collection
	 * @return {@link Set}&lt;{@link IFileColumn}&gt;
	 */
	Set<IFileColumn> getColumns();
	
	/**
	 * Get the {@link IFileRow}s in a collection
	 * @return {@link Set}&lt;{@link IFileRow}&gt;
	 */
	Set<IFileRow> getRows();
	
	/**
	 * Adds a {@link IFileColumn} to the column collection
	 * @param column {@link IFileColumn}
	 */
	void addColumn(IFileColumn column);
	
	/**
	 * Set the {@link RowStatus} to the collection of {@link IFileRow} 
	 * @param rows {@link Set}&lt;{@link IFileRow}&gt;
	 * @param status {@link RowStatus} 
	 */
	void setRowStatus(Set<IFileRow> rows, RowStatus status);
	
	/**
	 * Toggles all columns to disabled, ie. enabled = false
	 */
	void disableAllColumns();
	
	/**
	 * Enables the columns that are found in the file header
	 */
	void enableColumnsFromFileHeader() throws NullPointerException;
	
	/**
	 * Indicates if all madatory columns are available and set to enabled
	 * @return boolean
	 */
	boolean columnsAreComplete();
	
	/**
	 * By default reads and return all rows.
	 * @throws IOException
	 */
	void read() throws IOException, Exception;
	
	/**
	 * Derived from RunnableWithProgressMonitor
	 * @throws InvocationTargetException, InterruptedException
	 * @param monitor {@link IProgressMonitor}
	 */

	void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException;
	
	/**
	 * Executed when the import service has run
	 * ie. when the wizard has finished and all rows has been processed
	 */
	void done();
	 
}
