package com.lmtec.rac.wizards.importer;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.util.CsvContext;

public class ParseDoubleToMili extends CellProcessorAdaptor {

	@Override
	public Object execute(Object value, CsvContext context) {
		ParseDouble processor = new ParseDouble();
		Double milli = 1000.00D;
		return (Double) processor.execute(value, context) / milli;
	}
}
