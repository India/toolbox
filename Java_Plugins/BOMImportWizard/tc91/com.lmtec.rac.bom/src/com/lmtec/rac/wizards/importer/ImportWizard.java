package com.lmtec.rac.wizards.importer;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;

import com.lmtec.rac.wizards.AbstractWizard;
import com.lmtec.rac.wizards.importer.spec.IFileRowService;
import com.lmtec.rac.wizards.importer.spec.IImportProvider;


public class ImportWizard extends AbstractWizard{

	private IImportProvider importProvider;
	
	/**
	 * On finnished pressed calls the {@link IImportProvider}.run();
	 * and {@link IImportProvider}.done();
	 */
	public ImportWizard(String windowTitle) {
		super(windowTitle);
	}
	
	public IImportProvider getImportProvider() {
		return importProvider;
	}

	public void setImportProvider(IImportProvider importService) {
		this.importProvider = importService;
	}
	
	@Override
	public boolean performFinish() {

		final ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());

		// check permission
		IFileRowService rowService = importProvider.getFileRowService(); 
		if( rowService != null && ! rowService.getPermission()){
			MessageDialog.openError(getShell(), "An error has occured","You do not have the required access right to modify the item");
			return false;
		}
		
		try {
			if(importProvider != null)
				dialog.run(false, false, (IRunnableWithProgress) importProvider); 				
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if(importProvider != null)
			importProvider.done();
		
		getShell().redraw();
		
		return true;
	}
	

}
