package com.lmtec.rac.wizards.importer.bom;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IProgressMonitorWithBlocking;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import com.lmtec.rac.wizards.importer.AbstractImportProvider;
import com.lmtec.rac.wizards.importer.RowStatus;
import com.lmtec.rac.wizards.importer.spec.IFileRow;
import com.teamcenter.rac.kernel.TCException;

public class BomLineImport extends AbstractImportProvider{

	public BomLineImport(Class<? extends IFileRow> rowClazz) {
		super(rowClazz);
	}

	@Override
	public void read() throws IOException, Exception {
		
		((BomLineService) rowService).readBom();

		// clear the rows collection
		rows.clear();
		
		readFile();
		
		// get rows from file
		readFileRows();
		
		// Check line consistency
		for (IFileRow child : rows) {
			for (IFileRow parent : rows) {
				if(child != parent && child.equalId(parent) && !((BomLine) child).equalObject(parent)){
					parent.setStatus(RowStatus.ERROR);
					child.setStatus(RowStatus.ERROR);
					parent.setStatusDescription("Inconsistent duplicate");
					child.setStatusDescription("Inconsistent duplicate");
				}
			}
		}

		// remove duplicates and aggregate
		HashSet<BomLine> duplicates = new HashSet<BomLine>();
		for (IFileRow child : rows) {
			if(duplicates.contains(child) || child.getStatus() == RowStatus.ERROR)
				continue;
			
			for (IFileRow parent : rows) {
				if(child != parent && ((BomLine) child).equalObject(parent)){
					int pQ = ((BomLine) parent).getQuantityAsInteger();
					((BomLine) child).setQuantityAdd(pQ);
					rowService.setRowStatus(child);
					duplicates.add((BomLine) parent);
				}
			}
		}
		rows.removeAll(duplicates);
		
		// get remove rows from the server
		Set<IFileRow> bomChildrenToRemove = new HashSet<IFileRow>( ((BomLineService) rowService).getBomChildren() );
		for (BomLine bomLine : ((BomLineService) rowService).getBomChildren())
			for (IFileRow row : rows)
				if(row.equalId(bomLine))
					bomChildrenToRemove.remove(bomLine);
		
		setRowStatus(bomChildrenToRemove, RowStatus.REMOVE);
		rows.addAll(bomChildrenToRemove);
		
	}

	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
		IProgressMonitorWithBlocking blocking = (IProgressMonitorWithBlocking) monitor;
		
		int jobs = 0;
		for (IFileRow row : rows) {
			for (int i = 0; i < RowStatus.activeValues().length; i++) {
				if(row.getStatus() == RowStatus.activeValues()[i]){
					jobs++;
				}
			}
		}

		blocking.beginTask("Prepare import...", jobs);
		
		for (int i = 0; i < RowStatus.activeValues().length; i++) {
			RowStatus status = RowStatus.realValues()[i];
			
			for (IFileRow row : rows) {
				if(row.getStatus() == status){
					blocking.setTaskName(status.getLabel() + "..");
					try {
						if(rowService != null)
							rowService.execute(row);
					} catch (Exception e) {
						MessageDialog.openError(new Shell(), "An Error has occured", "There was an error processing itemID " + ((BomLine)row).getItemId());
						e.printStackTrace();
					}
					blocking.worked(1);
				}
			}
		}
		blocking.done();
	}

	@Override
	public void done() {
		
		try {
			if(rowService != null)
				rowService.save();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Shell shell = new Shell();
		StringBuilder message = new StringBuilder();
		boolean updatesDone = false;
		for (RowStatus status : RowStatus.activeValues()) {
			int count = 0;
			for (IFileRow row : rows) {
				if(row.isEnabled() && status == row.getStatus())
					count++;
			}
			message.append(status.getLabel() + "\t" + count + "\n");
			if(count != 0)
				updatesDone = true; 
		}
		String updatesNotDone = (!updatesDone) ? "No changes were done\n" : "";
		MessageDialog.openInformation(shell, "The import is complete", updatesNotDone + message.toString());
	}
	

}
