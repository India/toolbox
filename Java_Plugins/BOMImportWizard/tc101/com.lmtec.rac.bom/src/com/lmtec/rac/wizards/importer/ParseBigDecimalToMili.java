package com.lmtec.rac.wizards.importer;

import java.math.BigDecimal;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ParseBigDecimal;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.util.CsvContext;

public class ParseBigDecimalToMili extends CellProcessorAdaptor {

	@Override
	public Object execute(Object value, CsvContext context) {
		ParseBigDecimal processor = new ParseBigDecimal();
		BigDecimal parsedValue = (BigDecimal) processor.execute(value, context);
		BigDecimal milli = new BigDecimal(1000.0d);
		return  parsedValue.divide(milli);
	}
}
