package com.lmtec.rac.wizards.importer.spec;

import org.supercsv.cellprocessor.ift.CellProcessor;

public interface IFileColumn extends IEnabled, IMandatory{
	
	public abstract boolean hasMap(String name);

	public abstract String getLabel();

	public abstract void setLabel(String label);

	public abstract String getPropertyName();

	public abstract void setPropertyName(String propertyName);

	public abstract String[] getMapping();

	public abstract void setMapping(String[] mapping);

	public abstract void setEnabled(boolean enabled);

	public abstract String getExtName();

	public abstract void setExtName(String extName);

	public Class<?> getDataType();

	public void setDataType(Class<?> dataType);
	
	public CellProcessor getCellProcessor();
	
	public void setCellProcessor(CellProcessor processor);
}
