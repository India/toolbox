package com.siemens.modeltools.rac.menus.commands;

import java.awt.Frame;

import org.apache.log4j.Logger;

import com.siemens.modeltools.rac.menus.dialogs.RenameIpemDialog;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;

public class RenameIpemCommand extends AbstractAIFCommand 
{

	public RenameIpemCommand(Frame parent, TCSession session, TCComponent target, Logger logger) 
	{
		RenameIpemDialog myDlg = new RenameIpemDialog(parent, session, target, logger);
		myDlg.setRenameCustomLayout();
		
		this.setRunnable(myDlg);
	}

}
