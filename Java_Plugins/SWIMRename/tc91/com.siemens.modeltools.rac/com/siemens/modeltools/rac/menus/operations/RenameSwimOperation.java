package com.siemens.modeltools.rac.menus.operations;

import java.io.File;
import java.io.FileWriter;
import java.util.SortedSet;

import org.apache.log4j.Logger;

import com.siemens.modeltools.rac.RenamePropertyAccessor;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;

public class RenameSwimOperation extends AbstractAIFOperation 
{
	protected TCSession session = null;
    protected AIFDesktop desktop = null;
    protected Logger logger;
    protected TCComponent comp;
    protected String sNewName;
    protected String autorename_item_id = "false";
    protected String autorename_item_name = "false";
    protected String autorename_other = "false";
    protected String CurrentPwd = "";


	public RenameSwimOperation() 
	{
		// TODO Auto-generated constructor stub
	}

	public RenameSwimOperation(String arg0) 
	{
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	public RenameSwimOperation(TCSession theSession, AIFDesktop dt, TCComponent theComp, 
			               String newName, boolean itemIDFlag, boolean itemNameFlag,
						   boolean otherFlag, String thePassword, Logger theLogger)
	{
		super();
		session = theSession;
		desktop = dt;
		logger = theLogger;
		comp = theComp;
		sNewName = newName;
		CurrentPwd = thePassword;
		
		
		if(itemIDFlag)
		{
			autorename_item_id = "true";
		}
		
		if(itemNameFlag)
		{
			autorename_item_name = "true";
		}
		
		if(otherFlag)
		{
			autorename_other = "true";
		}

	}

	@Override
	public void executeOperation() throws Exception 
	{
	  logger.info("Executing the Rename Operation ...");
		
	  TCComponentItemRevision SelectedItemRevision = null;
	  String SelectedItemID = "";	
	  String CurrentUser = "";
	  String CurrentGroup = "";
	  String CurrentDatasetName = "";
	  String fileTypeStr = "";
	  String sourceType = "";
	  String swimUtilityPath = "";
	  String swimWorkDir = "";
	  String swimWorkDirName = "";
	  String swimWorkFileName = "";
	  String swimWorkFile = "";
	  String[] swimCommandLine = new String[1];
		
	  try
	  {
		
		// Initialize the RenamePropertyAccessor
		RenamePropertyAccessor swimProperties = new RenamePropertyAccessor("swimrename.cfg.xml", logger);
		
		TCComponentDataset SelectedDataset = (TCComponentDataset) comp;
		
		// Determined where the selected Item is used.
		AIFComponentContext[] parents = SelectedDataset.whereReferenced();
		
		for(int i = 0; i < parents.length; i++)
        {
        	TCComponent parent = (TCComponent) parents[i].getComponent();
        	if (parent instanceof TCComponentItemRevision)
        	{
				SelectedItemRevision = (TCComponentItemRevision) parent;
				break;
        	}
        }
        if (SelectedItemRevision != null)
        {
        	TCComponentItem SelectedItem = (TCComponentItem) SelectedItemRevision.getItem();
        	if (SelectedItem != null)
        	{
        		SelectedItemID = SelectedItem.getProperty("item_id");
        	}
        }
        
        // Get the current user and group.
        CurrentUser = session.getUserName();
        CurrentGroup = session.getGroup().getFullName();
        
        logger.info("User: " + CurrentUser + "  Group: " + CurrentGroup + "  Password: " + CurrentPwd);

        // Get the Dataset Name.
        CurrentDatasetName = SelectedDataset.getProperty("object_name");
        
        // Get the Dataset Type
        fileTypeStr = SelectedDataset.getType();
        
        // Make sure fileTypeStr is one of the configured dataset types.
        SortedSet datasetTypeSet = swimProperties.getDataSetTypes("swimrenamedatasettype");
		
		//Make sure Dataset Type is in the configured set.
		if(datasetTypeSet.contains(fileTypeStr))
		{
			// Return the Dataset Source Type
			sourceType = swimProperties.getDataSetTypeSource("swimrenamedatasettype", fileTypeStr);
			
			// Get the swimUtilityPath from the configuration file.
			swimUtilityPath = swimProperties.getSingleValueForNode("swimrenameutilitypath");
			
			// Make sure bat file  swimUtilityPath exists
			File ootbBatFile = new File(swimUtilityPath);
			
			if(!ootbBatFile.exists())
			{
				logger.error("OOTB Batch File: " + swimUtilityPath + " does not exist!");
				MessageBox.post("OOTB Batch File: " + swimUtilityPath + " does not exist!","The swimrename utility can not be found!",1);
				return;
			}
			
            // Get the swimWorkFileName from the configuration file.
			swimWorkFileName = swimProperties.getSingleValueForNode("swimrenameworkbatname");
			
			// Get the swimWorkDir from the configuration file.
			swimWorkDir = swimProperties.getSingleValueForNode("swimrenameworkpath");
			swimWorkDirName = (CurrentDatasetName + "_" + CurrentUser);
			
			// Strip any bad characters from this name.
			swimWorkDirName = validateDirectoryName(swimWorkDirName);			
			
			File SwimRenameDir = new File(swimWorkDir, swimWorkDirName);
			
			logger.info("Creating working directory " + SwimRenameDir.getPath());
			
			if(SwimRenameDir.mkdir())
			{
				// The Directory was created!
				SwimRenameDir.deleteOnExit();
				
				// Put the command line together.
				swimCommandLine[0] = new String("start /I /WAIT /Separate " + swimUtilityPath + " -u " + CurrentUser +
						                         " -p " + CurrentPwd + " -g " + CurrentGroup + " -item " + SelectedItemID +
						                         " -type " + sourceType + " -old_name " + CurrentDatasetName + 
						                         " -new_name " + sNewName + " -autorename_item_id " + autorename_item_id + 
						                         " -autorename_item_name " + autorename_item_name + " -autorename_other " + 
						                         autorename_other + " -work_dir " + SwimRenameDir.getPath());
				
				// Write the Command to the working bat file.
				swimWorkFile = swimWorkDir + "\\" + swimWorkFileName;
				FileWriter myWriter = new FileWriter(swimWorkFile);
				myWriter.write(swimCommandLine[0]);
				myWriter.close();
				
				// Execute the constructed bat file.
				Process myProc = Runtime.getRuntime().exec(swimWorkFile);
			}
			else
			{
				logger.error("Failed to create the swimrename work directory: " + swimWorkDirName + " at " + swimWorkDir);
				MessageBox.post("Failed to create the swimrename work directory: " + swimWorkDirName + " at " + swimWorkDir, "Work Directory could not be created!", 1);
				return;
			}
		}
	  }catch (Exception ex) 
	  {
          // error
          logger.error(ex);        
          MessageBox.post(ex);
                  
          throw ex;
      }
	}

	private String validateDirectoryName(String newName)
	{
		String returnVal = newName;
		char finalName[] = newName.toCharArray();
		int iSize = newName.length();
		boolean bIsGood = true;
		
        //	newName can not contain any of the following characters:
		//    \ / : * ? " < > |		
		for(int i=0;i<iSize;i++)
		{
			if(finalName[i] == '\\')
			{
				bIsGood = false;
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '/')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == ':')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '*')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '?')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '\"')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '>')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '<')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '|')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else
			{
				// Do nothing.
			}
		}
		
		if(!bIsGood)
		{
			logger.info("Setting Directory Name to: " + String.valueOf(finalName));
			// Set textField to finalName.
			returnVal = String.valueOf(finalName);
		}
		
		
		return returnVal;
	}
}
