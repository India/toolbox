This plug-in "ModelTools Plug-in" provides RAC Menu Items to Rename both Solidworks and ProE Models.

To deploy this plug-in:

1.  Copy file "com.siemens.modeltools.rac_9.1.0.0.jar" to your "%TC_ROOT%\portal\plugins" or your "%TPR%" directory.

2.  Modify files "ipemrename.cfg.xml" and "swimrename.cfg.xml" to match your environment.

3.  Copy files "ipemrename.cfg.xml" and "swimrename.cfg.xml" to your "%TC_ROOT%\portal" or your "%TPR%" directory.

4.  Run script "genregxml.bat" in "%TC_ROOT%\portal\registry"

5.  Verify that when you launch the RAC for Teamcenter 9.1 that the TPR environment variable is being set.
     Note: The ootb portal.bat file sets the TPR environment variable.