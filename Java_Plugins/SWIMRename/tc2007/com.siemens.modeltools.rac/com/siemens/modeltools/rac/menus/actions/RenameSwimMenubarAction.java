package com.siemens.modeltools.rac.menus.actions;

import java.awt.Frame;
import java.util.SortedSet;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import com.siemens.modeltools.rac.RenamePropertyAccessor;
import com.siemens.modeltools.rac.menus.commands.RenameSwimCommand;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aifrcp.Application;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;

public class RenameSwimMenubarAction implements IWorkbenchWindowActionDelegate 
{
	private static Logger logger = Logger.getLogger(RenameSwimMenubarAction.class);
	private RenameSwimCommand myCmd;
	private IWorkbenchWindow myWindow;
	private Frame myFrame;
	private TCSession mySession;

	public RenameSwimMenubarAction() 
	{
		// TODO Auto-generated constructor stub
	}

	public void dispose() 
	{
		// TODO Auto-generated method stub

	}

	public void init(IWorkbenchWindow window) 
	{
		myWindow = window;

	}

	public void run(IAction arg0) 
	{
		logger.info("Just pushed Menu Item - Rename Solidworks Model - ...");
		
		myFrame = Application.aif_portal.getDesktopManager().getActiveDesktop().getFrame();
		mySession = (TCSession) Application.aif_portal.getDesktopManager().getKernel().getSessionManager().getSession(Application.aif_portal.getDesktopManager().getCurrentDesktop().getCurrentApplication());
		
		// Get any TC components selected.
		InterfaceAIFComponent comps[] = Application.aif_portal.getDesktopManager().getCurrentDesktop().getCurrentApplication().getTargetComponents();
		
		if(comps != null)
		{
		  if(comps.length ==1)
		  {
			TCComponent theComp = (TCComponent)comps[0];
			
			// Make sure theComp is the correct Component Type, etc...  !
			if(isCorrectComponent(theComp))  
			{			  
		      try
		      {
		        myCmd = new RenameSwimCommand(myFrame, mySession, theComp, logger);
		        myCmd.executeModal();
		      }
		      catch(Exception ex)
		      {
			    MessageBox mb = new MessageBox (myWindow.getShell());
			    mb.setMessage(ex.toString());
			    mb.open();
		      }
			}
			else
			{
				MessageBox mb = new MessageBox (myWindow.getShell());
				mb.setMessage("Incorrect Selection: Please select a Solidworks Model.");
				mb.open();
			}
		  }
		  else
		  {
			  MessageBox mb = new MessageBox (myWindow.getShell());
			  mb.setMessage("Incorrect Selection: Please select only one Solidworks Model.");
			  mb.open(); 
		  }
		}
		else
		{
			MessageBox mb = new MessageBox (myWindow.getShell());
			mb.setMessage("Incorrect Selection: Please select one Solidworks Model.");
			mb.open();
		}


	}

	public void selectionChanged(IAction arg0, ISelection arg1) 
	{
		// TODO Auto-generated method stub

	}
	
    //Make sure theComp is the correct Component Type, etc...  !
	private boolean isCorrectComponent(TCComponent testComp)
	{
		boolean bReturn = false;		
		String sCompType = testComp.getType();
		logger.info("Checking for Type: " + sCompType);
		RenamePropertyAccessor swimProperties = new RenamePropertyAccessor("swimrename.cfg.xml", logger);
		
		// Get the configured Dataset Types.
		SortedSet itemTypeSet = swimProperties.getDataSetTypes("swimrenamedatasettype");
		
		//Make sure sCompType is in the set.
		if(itemTypeSet.contains(sCompType))
		{
			bReturn = true;
		}
		
		return bReturn;
	}

}
