package com.siemens.modeltools.rac.menus.commands;

import java.awt.Frame;

import org.apache.log4j.Logger;

import com.siemens.modeltools.rac.menus.dialogs.RenameSwimDialog;
import com.teamcenter.rac.aif.AbstractAIFCommand;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCSession;

public class RenameSwimCommand extends AbstractAIFCommand 
{

	public RenameSwimCommand(Frame parent, TCSession session, TCComponent target, Logger logger) 
	{
		RenameSwimDialog myDlg = new RenameSwimDialog(parent, session, target, logger);
		myDlg.setRenameCustomLayout();
		
		this.setRunnable(myDlg);
	}

}
