package com.siemens.modeltools.rac.menus.operations;

import java.io.File;
import java.io.FileWriter;
import java.util.SortedSet;

import org.apache.log4j.Logger;

import com.siemens.modeltools.rac.RenamePropertyAccessor;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;

public class RenameIpemOperation extends AbstractAIFOperation 
{
	protected TCSession session = null;
    protected AIFDesktop desktop = null;
    protected Logger logger;
    protected TCComponent comp;
    protected String sNewName;
    protected String autorename_item_id = "false";
    protected String autorename_item_name = "false";
    protected String autorename_other = "false";
    protected String CurrentPwd = "";


	public RenameIpemOperation()
	{
		// TODO Auto-generated constructor stub
	}

	public RenameIpemOperation(String arg0) 
	{
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	public RenameIpemOperation(TCSession theSession, AIFDesktop dt, TCComponent theComp, 
			               String newName, boolean itemIDFlag, boolean itemNameFlag,
						   boolean otherFlag, String thePassword, Logger theLogger)
	{
		super();
		session = theSession;
		desktop = dt;
		logger = theLogger;
		comp = theComp;
		sNewName = newName;
		CurrentPwd = thePassword;
		
		
		if(itemIDFlag)
		{
			autorename_item_id = "true";
		}
		
		if(itemNameFlag)
		{
			autorename_item_name = "true";
		}
		
		if(otherFlag)
		{
			autorename_other = "true";
		}

	}

	@Override
	public void executeOperation() throws Exception 
	{
	  logger.info("Executing the Rename Operation ...");
		
	  TCComponentItemRevision SelectedItemRevision = null;
	  String SelectedItemID = "";	
	  String CurrentUser = "";
	  String CurrentGroup = "";
	  String CurrentDatasetName = "";
	  String fileTypeStr = "";
	  String sourceType = "";
	  String ipemUtilityPath = "";
	  String ipemWorkDir = "";
	  String ipemWorkDirName = "";
	  String ipemWorkFileName = "";
	  String ipemWorkFile = "";
	  String[] ipemCommandLine = new String[1];
		
	  try
	  {
		
		// Initialize the RenamePropertyAccessor
		RenamePropertyAccessor ipemProperties = new RenamePropertyAccessor("ipemrename.cfg.xml", logger);
		
		TCComponentDataset SelectedDataset = (TCComponentDataset) comp;
		
		// Determined where the selected Item is used.
		AIFComponentContext[] parents = SelectedDataset.whereReferenced();
		
		for(int i = 0; i < parents.length; i++)
        {
        	TCComponent parent = (TCComponent) parents[i].getComponent();
        	if (parent instanceof TCComponentItemRevision)
        	{
				SelectedItemRevision = (TCComponentItemRevision) parent;
				break;
        	}
        }
        if (SelectedItemRevision != null)
        {
        	TCComponentItem SelectedItem = (TCComponentItem) SelectedItemRevision.getItem();
        	if (SelectedItem != null)
        	{
        		SelectedItemID = SelectedItem.getProperty("item_id");
        	}
        }
        
        // Get the current user and group.
        CurrentUser = session.getUserName();
        CurrentGroup = session.getGroup().getFullName();
        
        logger.info("User: " + CurrentUser + "  Group: " + CurrentGroup + "  Password: " + CurrentPwd);

        // Get the Dataset Name.
        CurrentDatasetName = SelectedDataset.getProperty("object_name");
        
        // Get the Dataset Type
        fileTypeStr = SelectedDataset.getType();
        
        // Make sure fileTypeStr is one of the configured dataset types.
        SortedSet datasetTypeSet = ipemProperties.getDataSetTypes("ipemrenamedatasettype");
		
		//Make sure Dataset Type is in the configured set.
		if(datasetTypeSet.contains(fileTypeStr))
		{
			// Return the Dataset Source Type
			sourceType = ipemProperties.getDataSetTypeSource("ipemrenamedatasettype", fileTypeStr);
			
			// Get the ipemUtilityPath from the configuration file.
			ipemUtilityPath = ipemProperties.getSingleValueForNode("ipemrenameutilitypath");
			
			// Make sure bat file  ipemUtilityPath exists
			File ootbBatFile = new File(ipemUtilityPath);
			
			if(!ootbBatFile.exists())
			{
				logger.error("OOTB Batch File: " + ipemUtilityPath + " does not exist!");
				MessageBox.post("OOTB Batch File: " + ipemUtilityPath + " does not exist!","The ipemrename utility can not be found!",1);
				return;
			}
			
            // Get the ipemWorkFileName from the configuration file.
			ipemWorkFileName = ipemProperties.getSingleValueForNode("ipemrenameworkbatname");
			
			// Get the ipemWorkDir from the configuration file.
			ipemWorkDir = ipemProperties.getSingleValueForNode("ipemrenameworkpath");
			ipemWorkDirName = (CurrentDatasetName + "_" + CurrentUser);
			
			// Strip any bad characters from this name.
			ipemWorkDirName = validateDirectoryName(ipemWorkDirName);			
			
			File ipemRenameDir = new File(ipemWorkDir, ipemWorkDirName);
			
			logger.info("Creating working directory " + ipemRenameDir.getPath());
			
			if(ipemRenameDir.mkdir())
			{
				// The Directory was created!
				ipemRenameDir.deleteOnExit();
				
				// Put the command line together.
				ipemCommandLine[0] = new String("start /I /WAIT /Separate " + ipemUtilityPath + " -u " + CurrentUser +
						                         " -p " + CurrentPwd + " -g " + CurrentGroup + " -item " + SelectedItemID +
						                         " -type " + sourceType + " -old_name " + CurrentDatasetName + 
						                         " -new_name " + sNewName + " -autorename_item_id " + autorename_item_id + 
						                         " -autorename_item_name " + autorename_item_name + " -autorename_other " + 
						                         autorename_other + " -work_dir " + ipemRenameDir.getPath());
				
				// Write the Command to the working bat file.
				ipemWorkFile = ipemWorkDir + "\\" + ipemWorkFileName;
				FileWriter myWriter = new FileWriter(ipemWorkFile);
				myWriter.write(ipemCommandLine[0]);
				myWriter.close();
				
				// Execute the constructed bat file.
				Process myProc = Runtime.getRuntime().exec(ipemWorkFile);
			}
			else
			{
				logger.error("Failed to create the ipemrename work directory: " + ipemWorkDirName + " at " + ipemWorkDir);
				MessageBox.post("Failed to create the ipemrename work directory: " + ipemWorkDirName + " at " + ipemWorkDir, "Work Directory could not be created!", 1);
				return;
			}
		}
	  }catch (Exception ex) 
	  {
          // error
          logger.error(ex);        
          MessageBox.post(ex);
                  
          throw ex;
      }
	}

	private String validateDirectoryName(String newName)
	{
		String returnVal = newName;
		char finalName[] = newName.toCharArray();
		int iSize = newName.length();
		boolean bIsGood = true;
		
        //	newName can not contain any of the following characters:
		//    \ / : * ? " < > |		
		for(int i=0;i<iSize;i++)
		{
			if(finalName[i] == '\\')
			{
				bIsGood = false;
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '/')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == ':')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '*')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '?')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '\"')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '>')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '<')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '|')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else
			{
				// Do nothing.
			}
		}
		
		if(!bIsGood)
		{
			logger.info("Setting Directory Name to: " + String.valueOf(finalName));
			// Set textField to finalName.
			returnVal = String.valueOf(finalName);
		}
		
		
		return returnVal;
	}
}
