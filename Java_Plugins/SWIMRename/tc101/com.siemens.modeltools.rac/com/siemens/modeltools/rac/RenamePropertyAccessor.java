package com.siemens.modeltools.rac;

import java.io.File;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class RenamePropertyAccessor 
{
	protected Logger logger;	
    protected String sPropertyFileName;
    protected Document propDocument;
    
    private String sTcProot = "";
    
	public RenamePropertyAccessor(String propFileName, Logger theLogger) 
	{
		logger = theLogger;
		sPropertyFileName = propFileName;
		sTcProot = System.getenv("TPR");
		
		logger.info("TPR is set to: " + sTcProot);
		
		if(sTcProot != null && sTcProot.length()>0)
		{
		  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			
		  String sFullPathToFile = sTcProot + "\\" + sPropertyFileName;
		  
		  File renameCfg = new File(sFullPathToFile);
		  
		  if(renameCfg != null)
		  {
			  try
		       {
		         DocumentBuilder builder = factory.newDocumentBuilder();
		         propDocument = builder.parse( renameCfg );
		        
		         logger.info("Root element of the swimrename properties file is " + 
		        		 propDocument.getDocumentElement().getNodeName());
		         
		         
		       }catch (SAXParseException err) 
		       {
		         logger.error("** Parsing error" + ", line " 
		             + err.getLineNumber () + ", uri " + err.getSystemId ());
		         logger.error(" " + err.getMessage ());

		        }catch (SAXException e) 
		        {
		          Exception x = e.getException ();
		          ((x == null) ? e : x).printStackTrace ();

		        }catch (Throwable t) 
		        {
		          t.printStackTrace ();
		        }
		  }
		}
	}
	
	public String getSingleValueForNode(String tagName)
    {
         String sOut = null;
         
         NodeList listOfNodes = propDocument.getElementsByTagName(tagName);
         
         // Get the Node
         Node theNode = listOfNodes.item(0);
         
         if(theNode.getNodeType() == Node.ELEMENT_NODE)
         {
             Element theElement = (Element)theNode;
           
             NodeList propertyList = theElement.getElementsByTagName("property");
             Element propertyElement = (Element)propertyList.item(0);
             
             NodeList textPList = propertyElement.getChildNodes();

             sOut = ((Node)textPList.item(0)).getNodeValue().trim();
             logger.info("Value for \"" + tagName + "\" is set to \"" + sOut + "\".");
             
         }
         else
         {
             logger.error("CONFIG ERROR: Node \"" + tagName + "\" is not configured!");
         }
        
        return sOut;
    }

	public SortedSet getDataSetTypes(String tagName)
    {
         SortedSet myDataSetTypes = new TreeSet();
         
         NodeList listOfTypes = propDocument.getElementsByTagName(tagName);
         int totalTypes = listOfTypes.getLength();
         
         // Loop accross the listOfTypes
         for(int i=0; i< totalTypes; i++)
         {
           // Get the Type
           Node typeNode = listOfTypes.item(i);
           
           if(typeNode.getNodeType() == Node.ELEMENT_NODE)
           {
             Element typeElement = (Element)typeNode;
           
             NodeList propertyList = typeElement.getElementsByTagName("property");
             
             for(int j=0;j<propertyList.getLength();j++)
             {
               Node propertyNode = propertyList.item(j);
               
               // Determine the name
               NamedNodeMap propMap = propertyNode.getAttributes();
               String propertyName = propMap.item(0).getNodeValue();
               
               if(propertyName.equals("typename"))
               {
                 Element propertyElement = (Element)propertyNode;
             
                 NodeList textPList = propertyElement.getChildNodes();

                 String sType = ((Node)textPList.item(0)).getNodeValue().trim();
                 logger.info("Dataset Type : " + sType);
             
                 myDataSetTypes.add(sType);
               }
             }
           }
           else
           {
        	 logger.error("CONFIG ERROR: Node \"" + tagName + "\" is not configured!");
             logger.error("Node Type Found is " + typeNode.getNodeType());
           }

         }       
        
        return myDataSetTypes;
    }
	
	public String getDataSetTypeSource(String tagName, String datasetType)
    {
         String datasetTypeSource = "";
         
         NodeList listOfTypes = propDocument.getElementsByTagName(tagName);
         int totalTypes = listOfTypes.getLength();
         
         // Loop accross the listOfTypes
         for(int i=0; i< totalTypes; i++)
         {
             boolean isGoodName = false;
           
             // Get the Type Node
             Node typeNode = listOfTypes.item(i);
           
             if(typeNode.getNodeType() == Node.ELEMENT_NODE)
             {
               Element typeElement = (Element)typeNode;
           
               NodeList propertyList = typeElement.getElementsByTagName("property");
               
               for(int j=0;j<propertyList.getLength();j++)
               {
                 Node propertyNode = propertyList.item(j);
                 
                 // Determine the name
                 NamedNodeMap propMap = propertyNode.getAttributes();
                 String propertyName = propMap.item(0).getNodeValue();
                 
                 if(propertyName.equals("typename"))
                 {
                   Element propertyElement = (Element)propertyNode;
             
                   NodeList textPList = propertyElement.getChildNodes();
                 
                   String sTypeName = ((Node)textPList.item(0)).getNodeValue().trim();
                   
                   if(sTypeName.equals(datasetType))
                   {
                     isGoodName = true;
                   }
                 
                 }
               } // END of first j loop.
               
               if(isGoodName)
               {
                 for(int j=0;j<propertyList.getLength();j++)
                 {
                   Node propertyNode = propertyList.item(j);
                 
                   // Determine the name
                   NamedNodeMap propMap = propertyNode.getAttributes();
                   String propertyName = propMap.item(0).getNodeValue();
                 
                   if(propertyName.equals("typesource"))
                   {
                     Element propertyElement = (Element)propertyNode;
             
                     NodeList textPList = propertyElement.getChildNodes();
                     
                     String sSourceName = ((Node)textPList.item(0)).getNodeValue().trim();
                     
                     datasetTypeSource = sSourceName;
                     
                   }
                 } // END of second j loop. 
               }
             }
             else
             {
          	   logger.error("CONFIG ERROR: Node \"" + tagName + "\" is not configured!");
               logger.error("Node Type Found is " + typeNode.getNodeType());
             }
           

         } // end of i loop.
        
        return datasetTypeSource;
    }

}
