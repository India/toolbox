package com.siemens.modeltools.rac.menus.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.apache.log4j.Logger;

import com.siemens.modeltools.rac.menus.operations.RenameIpemOperation;
import com.swtdesigner.SwingResourceManager;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.InterfaceAIFOperationListener;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentUser;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;

public class RenameIpemDialog extends AbstractAIFDialog implements
		InterfaceAIFOperationListener 
{
	// A Reference to the session	
	protected TCSession session = null;
	
	// A Reference to the desktop
	protected AIFDesktop desktop = null;
	
	// A Reference to the parent frame
	protected Frame parent = null;
	
	// A Reference to the Logger
	protected Logger logger;
	
	// A Reference to the Operation
	protected RenameIpemOperation myOp;
	
	// A Reference to the TC Component to rename.
	protected TCComponent myComp;
	
	// Dialog components.
	private JTextField textField;
	private JCheckBox renameTheItemCheckBox;
	private JCheckBox renameTheItemCheckBox_1;
	private JCheckBox renameOtherCheckBox;
	private JPasswordField passwordField;
	
	private void initRenameDialog()
	{
		String sItemName = "";
		try
		{
		  sItemName = myComp.getProperty("object_name");
		}
		catch(TCException tce)
		{
			logger.error(tce);
		}
		setTitle("Rename ProE Model.");
		
		if(sItemName != null && sItemName.length()>0)
		{
			setTitle("Rename Model \"" + sItemName + "\".");
		}
		
		getContentPane().setLayout(new BorderLayout());
		
		final JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.black, 1, false));
		panel.setBackground(Color.LIGHT_GRAY);
		getContentPane().add(panel, BorderLayout.SOUTH);

		final JButton renameButton = new JButton();
		renameButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent arg0) 
			{
				// Add Action for Rename.
				startRenameOperation();
				
			}
		});
		renameButton.setFont(new Font("", Font.BOLD, 12));
		renameButton.setText("Rename");
		panel.add(renameButton);

		final JButton cancelButton = new JButton();
		cancelButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent e) 
			{
				// Add Action for Cancel.
				closeRenameDialog();
					
			}
		});
		cancelButton.setFont(new Font("", Font.BOLD, 12));
		cancelButton.setText("Cancel");
		panel.add(cancelButton);

		final JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		final JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(Color.black, 1, false));
		panel_1.setLayout(null);
		scrollPane.setViewportView(panel_1);

		final JLabel newNameLabel = new JLabel();
		newNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		newNameLabel.setText("New Model Name:");
		newNameLabel.setBounds(10, 27, 123, 16);
		panel_1.add(newNameLabel);

		textField = new JTextField();
		textField.setBounds(139, 25, 265, 20);
		panel_1.add(textField);

		final JLabel selectAllThatLabel = new JLabel();
		selectAllThatLabel.setFont(new Font("", Font.BOLD, 12));
		selectAllThatLabel.setText("Select all that apply");
		selectAllThatLabel.setBounds(139, 56, 203, 16);
		panel_1.add(selectAllThatLabel);

		renameTheItemCheckBox = new JCheckBox();
		renameTheItemCheckBox.setToolTipText("Specifies whether the item name should be changed to match the model name.");
		renameTheItemCheckBox.setText("Apply to Item Name?");
		renameTheItemCheckBox.setBounds(139, 78, 203, 24);
		panel_1.add(renameTheItemCheckBox);

		renameTheItemCheckBox_1 = new JCheckBox();
		renameTheItemCheckBox_1.setToolTipText("Specifies whether the item ID should be changed to match the model name.");
		renameTheItemCheckBox_1.setText("Apply to Item ID?");
		renameTheItemCheckBox_1.setBounds(139, 108, 203, 24);
		panel_1.add(renameTheItemCheckBox_1);

		renameOtherCheckBox = new JCheckBox();
		renameOtherCheckBox.setToolTipText("Specifies whether the names of other objects within the item or item revisions should be changed to match the model name.");
		renameOtherCheckBox.setText("Apply to Other?");
		renameOtherCheckBox.setBounds(139, 138, 203, 24);
		panel_1.add(renameOtherCheckBox);

		final JLabel verifyPasswordLabel = new JLabel();
		verifyPasswordLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		verifyPasswordLabel.setText("Verify Password:");
		verifyPasswordLabel.setBounds(27, 186, 106, 16);
		panel_1.add(verifyPasswordLabel);

		passwordField = new JPasswordField();
		passwordField.setBounds(139, 184, 154, 20);
		panel_1.add(passwordField);

		final JLabel label = new JLabel();
		label.setForeground(new Color(255, 0, 0));
		label.setFont(new Font("", Font.BOLD, 16));
		label.setText("*");
		label.setBounds(410, 27, 18, 16);
		panel_1.add(label);

		final JLabel label_1 = new JLabel();
		label_1.setForeground(new Color(255, 0, 0));
		label_1.setFont(new Font("", Font.BOLD, 16));
		label_1.setText("*");
		label_1.setBounds(299, 186, 18, 16);
		panel_1.add(label_1);

		final JButton button = new JButton();
		button.setToolTipText("This action requires that you verify your Teamcenter Password!");
		button.setIcon(SwingResourceManager.getIcon(RenameIpemDialog.class, "HelpMe.png"));
		button.setBounds(323, 181, 26, 26);
		panel_1.add(button);

		final JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(Color.black, 1, false));
		panel_2.setBackground(Color.LIGHT_GRAY);
		getContentPane().add(panel_2, BorderLayout.NORTH);

		final JLabel renameItemXxxLabel = new JLabel();
		panel_2.add(renameItemXxxLabel);
		renameItemXxxLabel.setHorizontalAlignment(SwingConstants.CENTER);
		renameItemXxxLabel.setFont(new Font("", Font.BOLD, 12));
		renameItemXxxLabel.setText("Rename Model  xxx To:");
		if(sItemName != null && sItemName.length()>0)
		{
			renameItemXxxLabel.setText("Rename ProE Model \"" + sItemName + "\" To:");
		}

	}
	
	private boolean validateNewDatasetName(String newName)
	{
		char finalName[] = newName.toCharArray();
		int iSize = newName.length();
		boolean bIsGood = true;
		
        //	newName can not contain any of the following characters:
		//    \ : * ? " < > |		
		for(int i=0;i<iSize;i++)
		{
			if(finalName[i] == '\\')
			{
				bIsGood = false;
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == ':')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '*')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '?')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '\"')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '>')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '<')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else if(newName.charAt(i) == '|')
			{
				bIsGood = false;
				
				finalName[i] = '_';
			}
			else
			{
				// Do nothing.
			}
		}
		
		if(!bIsGood)
		{
			logger.info("Setting New Model Name to: " + String.valueOf(finalName));
			// Set textField to finalName.
			textField.setText(String.valueOf(finalName));
		}
		
		
		return bIsGood;
	}
	
	private boolean validatePassword(String thePass)
	{
		boolean bPass = false;
		
		try
		{
		  TCComponentUser myUser = session.getUser();
		  
		  // Now verify the password for the user.
		  if(myUser.checkPassword(thePass))
		  {
			  bPass = true;
		  }		  
		  
		}
		catch(TCException tce)
		{
			logger.error(tce);
		}
		
		
		return bPass;
	}
	
	private void startRenameOperation()
	{
		String myNewName = getNewName();
		char myPassword[] = passwordField.getPassword();
		String sPassword = String.valueOf(myPassword);
		
		if(myNewName != null && myNewName.length() > 0)
		{		
		  // The New Dataset Name can not contain any of the following characters:
		  //    \ : * ? " < > |
		  if(validateNewDatasetName(myNewName))	
		  {
		    if(sPassword != null && sPassword.length()>0)
		    {
		      if(validatePassword(sPassword))
		      {
		        myOp = new RenameIpemOperation(session, desktop, myComp, getNewName(),
				                                                     getIDflag(),
				                                                     getNameflag(),
				                                                     getOtherflag(),
				                                                     sPassword,
				                                                     logger);
		        myOp.addOperationListener(this);

		        session.queueOperation(myOp);
		  
		        closeRenameDialog();
		      }
		      else
		      {
		    	  MessageBox.post("Verification of Teamcenter Password Failed!", "Error on Rename Dialog!", 1);
		      }
		    }
		    else
		    {
		    	MessageBox.post("Verify Password field must contain a value!", "Error on Rename Dialog!", 1);
		    }
		  }
		  else
		  {
			  MessageBox.post("Invalid characters have been romoved from the new Model Name! Please verify the new Model Name...", "Model Name " + myNewName + " is not valid!", 1);
		  }
		}
		else
		{
			MessageBox.post("New Model Name field must contain a value!", "Error on Rename Dialog!", 1);
		}
		
	}
	
	private String getNewName()
	{
		String theNewName = textField.getText();
		
		return theNewName;
	}
	
	private boolean getIDflag()
	{
		boolean theFlag = false;
		
		if(renameTheItemCheckBox_1.isSelected())
		{
			theFlag = true;
		}
		
		return theFlag;
	}
	
	private boolean getNameflag()
	{
		boolean theFlag = false;
		
		if(renameTheItemCheckBox.isSelected())
		{
			theFlag = true;
		}
		
		return theFlag;
	}
	
	private boolean getOtherflag()
	{
		boolean theFlag = false;
		
		if(renameOtherCheckBox.isSelected())
		{
			theFlag = true;
		}
		
		return theFlag;
	}
	
	private void closeRenameDialog()
	{
		setVisible(false);
		dispose();
	}

	public RenameIpemDialog(Frame arg0) 
	{
		super(arg0);
			
		// TODO Auto-generated constructor stub
		initRenameDialog();		
		
	}

	public RenameIpemDialog(Dialog arg0) 
	{
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public RenameIpemDialog(boolean arg0) 
	{
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public RenameIpemDialog(Frame arg0, boolean arg1) 
	{
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public RenameIpemDialog(Dialog arg0, boolean arg1) 
	{
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public RenameIpemDialog(Frame arg0, String arg1) 
	{
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public RenameIpemDialog(Dialog arg0, String arg1) 
	{
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public RenameIpemDialog(Frame arg0, String arg1, boolean arg2) 
	{
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

	public RenameIpemDialog(Dialog arg0, String arg1, boolean arg2) 
	{
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}
	
	public RenameIpemDialog(Frame arg0, TCSession theSession, TCComponent theComponent, Logger theLogger)
	{
		super(arg0);
		
		parent = arg0;
		session = theSession;
		logger = theLogger;
		myComp = theComponent;
		
		if(parent instanceof AIFDesktop)
		{
			desktop = (AIFDesktop)parent;
		}
		
		initRenameDialog();
	}

	public void endOperation() 
	{
		// TODO Auto-generated method stub

	}

	public void startOperation(String arg0) 
	{
		// TODO Auto-generated method stub

	}
	
	public void setRenameCustomLayout()
	{
		// Set the size of the dialog
		Dimension theDim = new Dimension(500, 330);
		
		this.setPreferredSize(theDim);
		this.centerToScreen(1.0, 1.0);
		this.pack();
	}

}
