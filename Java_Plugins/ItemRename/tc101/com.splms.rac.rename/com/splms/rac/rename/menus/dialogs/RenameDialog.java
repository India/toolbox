package com.splms.rac.rename.menus.dialogs;
//eclipse importsimport org.apache.log4j.Logger;import org.eclipse.swt.SWT;import org.eclipse.swt.widgets.Shell;import java.lang.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.util.EventObject;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.text.JTextComponent;
import javax.swing.JComponent;
import javax.swing.table.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import org.eclipse.swt.SWT;import java.util.Vector;
import java.util.Vector.*;
import com.teamcenter.rac.aif.*;
import com.teamcenter.rac.aif.kernel.AbstractAIFSession;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.common.TCConstants;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.*;
public class RenameDialog extends AbstractAIFDialog //implements InterfaceAIFOperationListener
  {    private static final long serialVersionUID = 1L;  private static Logger logger = Logger.getLogger("com.splms.rac.rename");  private String caseSensitivity;  private boolean nameEditable;  private char[] invalidCharacters;  private int itemIdLength;  private int revIdLength;  private int nameLength;  private JComboBox combobox;  private JTextField old_idTextField;
  private JTextField new_idTextField;
  private JTextField old_nameTextField;
  private JTextField new_nameTextField;
  private JTextField old_revidTextField;
  private JTextField new_revidTextField;
  private JButton renameButton;
  private JButton cancelButton;
  private CustomTableModel myTableModel = null;
  private JTable jTable1 = new JTable();
  protected TCSession session;
  protected InterfaceAIFComponent comp;
  protected TCComponentItem item;
  protected TCComponentItemRevision rev;
  protected AIFDesktop desktop;
  protected String old_id;
  protected String new_id;
  protected String old_name;
  protected String new_name;
  protected String old_revid;
  protected String new_revid;
  protected Shell parent;  protected Vector<TCComponent> attachedObjects = null;
  protected Vector<String> oldItemIDS = null;
  protected Vector<String> oldRevIDS = null;
  protected Vector<String> oldNames = null;
  protected Vector<String> newIDS = null;
  protected Vector<String> newNames = null;
  protected Vector<String> displayStrings = null;
  protected LoadOperation loadOp;  protected RenameOperation renameOp;  private Registry appReg;

  public String getOldID() {
    return old_idTextField.getText();
  }
  public String getNewID() {
    return new_idTextField.getText();
  }
  public String getOldName() {
    return old_nameTextField.getText();
  }
  public String getNewName() {
    return new_nameTextField.getText();
  }
  public String getOldRevID() {
    return old_revidTextField.getText();
  }
  public String getNewRevID() {
    return new_revidTextField.getText();
  }    
  //End of Common Methods
  // Item context
  public RenameDialog(Shell shellParent, int iStyle, TCSession tcSession, AIFDesktop currentDesktop, TCComponentItem selectedPart, String theCaseSensitivity, boolean theItemNameEditable, char theInvalidCharacters[], int theItemIdLength, int theItemNameLength)
    {
    super(currentDesktop, true);
    logger.info("Inside RenameDialog");    appReg = Registry.getRegistry(this);
    session = null;
    item = null;
    rev = null;
    old_id = null;
    new_id = null;
    old_name = null;
    new_name = null;
    old_revid = null;
    new_revid = null;
    attachedObjects = new Vector<TCComponent>();
    oldItemIDS = new Vector<String>();
    oldRevIDS = new Vector<String>();
    oldNames = new Vector<String>();
    newIDS = new Vector<String>();
    newNames = new Vector<String>();
    displayStrings = new Vector<String>();
    parent = shellParent;
    desktop = currentDesktop;    if(desktop == null)    	logger.info("desktop is NULL at TOP of RenameDialog Constructor Itemcontext!!!");    session = tcSession;
    item = selectedPart;    caseSensitivity = theCaseSensitivity;    nameEditable = theItemNameEditable;    invalidCharacters = theInvalidCharacters;    itemIdLength = theItemIdLength;    revIdLength = 32;    nameLength = theItemNameLength;    logger.info("caseSensitivity = '" + caseSensitivity + "', nameEditable = " + nameEditable + ", itemIdLength = " + itemIdLength + ", revIdLength = " + revIdLength + ", nameLength = " + nameLength);    for(int i = 0; i < invalidCharacters.length; i++)      logger.info("invalidCharacters[" + i + "] = '" + invalidCharacters[i] + "'");    
    try
      {
      old_id = item.getTCProperty("item_id").toString();
      old_name = item.getTCProperty("object_name").toString();
      old_revid = "N/A";
      new_revid = "N/A";
      // get latest rev
      TCComponentItemRevision latestRev = item.getLatestItemRevision();
      // get drawing related rev(s)
      AIFComponentContext children[] = null;
      AIFComponentContext children2[] = null;
      children = null;
      children = latestRev.getChildren("IMAN_specification");
      for(int z = 0; z < children.length; z++)
        {
        if(children[z].getComponent().getType().equalsIgnoreCase("IdeasDrawing"))
          {
logger.info("Found IdeasDrawing");
          children2 = ((TCComponent)(children[z].getComponent())).getChildren("IDEAS_DRAWING");
          for(int a = 0; a < children2.length; a++)
            {
            if(children2[a].getComponent() instanceof TCComponentItemRevision)
              {
              String oldItemID = ((TCComponentItemRevision)(children2[a].getComponent())).
                               getTCProperty("item_id").toString();
              String oldRevID = ((TCComponentItemRevision)(children2[a].getComponent())).
                               getTCProperty("item_revision_id").toString();
              TCComponentItem thisItem = ((TCComponentItemRevision)(children2[a].getComponent())).getItem();
              String oldName = thisItem.getTCProperty("object_name").toString();
              // ignore if it matches the original
              if(oldItemID.equals(old_id))
                continue;
              // ignore if item or children are non-editable
              if(areItemsAndRevsModifiable(thisItem) != 0)
                {
                continue;
                }
              // add related data
              attachedObjects.addElement((TCComponent)thisItem);
              oldItemIDS.addElement(oldItemID);
              oldRevIDS.addElement(oldRevID);
              oldNames.addElement(oldName);
              newIDS.addElement(oldItemID);
              newNames.addElement(oldName);
              displayStrings.addElement(oldItemID + "  <>  " + oldRevID + "  <>  " + oldName);
              }
            }
          }
        }
      }
    catch(TCException tcException)
      {
      String s = tcException.getError();
      if (desktop != null)
        {
        MessageBox messagebox = new MessageBox(desktop, s, null,
                                  appReg.getString("error.TITLE"), MessageBox.ERROR, true);
        messagebox.setVisible(true);
        }
      else
        {
        logger.info(s);
        }
      }
    rev = null;
    parent = shellParent;
logger.info("oldID = " + old_id + ", oldName = " + old_name);    initializeDialog();

    }

  // ItemRevision context  public RenameDialog(Shell shellParent, int iStyle, TCSession tcSession, AIFDesktop currentDesktop, TCComponentItemRevision selectedPartRev, String theCaseSensitivity, boolean theRevNameEditable, char theInvalidCharacters[], int theRevIdLength, int theRevNameLength)
    {
    super(currentDesktop, true);
    logger.info("Inside RenameDialog itemRev context");
    appReg = Registry.getRegistry(this);
    session = null;
    item = null;
    rev = null;
    old_id = null;
    new_id = null;
    old_name = null;
    new_name = null;
    old_revid = null;
    new_revid = null;
    attachedObjects = new Vector<TCComponent>();
    oldItemIDS = new Vector<String>();
    oldRevIDS = new Vector<String>();
    oldNames = new Vector<String>();
    newIDS = new Vector<String>();
    newNames = new Vector<String>();
    displayStrings = new Vector<String>();
    parent = shellParent;    desktop = currentDesktop;    if(desktop == null)    	logger.info("desktop is NULL at RenameDialog Constructor itemRev context!!!");    session = tcSession;    rev = selectedPartRev;    caseSensitivity = theCaseSensitivity;    nameEditable = theRevNameEditable;    invalidCharacters = theInvalidCharacters;    revIdLength = theRevIdLength;    itemIdLength = 32;    nameLength = theRevNameLength;
    logger.info("caseSensitivity = '" + caseSensitivity + "', nameEditable = " + nameEditable + ", itemIdLength = " + itemIdLength + ", revIdLength = " + revIdLength + ", nameLength = " + nameLength);    for(int i = 0; i < invalidCharacters.length; i++)      logger.info("invalidCharacters[" + i + "] = '" + invalidCharacters[i] + "'");        try
      {
      old_revid = rev.getTCProperty("item_revision_id").toString();
      old_name = rev.getTCProperty("object_name").toString();
      item = rev.getItem();
      old_id = item.getTCProperty("item_id").toString();
      // get drawing related rev(s)
      AIFComponentContext children[] = null;
      AIFComponentContext children2[] = null;
      children = null;
      children = rev.getChildren("IMAN_specification");
      for(int z = 0; z < children.length; z++)
        {
        if(children[z].getComponent().getType().equalsIgnoreCase("IdeasDrawing"))
          {
//System.out.println("Found IdeasDrawing");
          children2 = ((TCComponent)(children[z].getComponent())).getChildren("IDEAS_DRAWING");
          for(int a = 0; a < children2.length; a++)
            {
            if(children2[a].getComponent() instanceof TCComponentItemRevision)
              {
              String oldItemID = ((TCComponentItemRevision)(children2[a].getComponent())).
                               getTCProperty("item_id").toString();
              String oldRevID = ((TCComponentItemRevision)(children2[a].getComponent())).
                               getTCProperty("item_revision_id").toString();
              String oldName = ((TCComponentItemRevision)(children2[a].getComponent())).
                               getTCProperty("object_name").toString();
              // ignore if it matches the original
              if(oldItemID.equals(old_id))
                continue;
              // ignore if rev or children are non-editable
              if(areRevsModifiable((TCComponentItemRevision)(children2[a].getComponent())) != 0)
                {
                continue;
                }

              // add related data
              attachedObjects.addElement((TCComponent)(children2[a].getComponent()));
              oldItemIDS.addElement(oldItemID);
              oldRevIDS.addElement(oldRevID);
              oldNames.addElement(oldName);
              newIDS.addElement(oldRevID);
              newNames.addElement(oldName);
              displayStrings.addElement(oldItemID + "  <>  " + oldRevID + "  <>  " + oldName);
              }
            }
          }
        }
      }

    catch(TCException tcException)
      {
      String s = tcException.getError();
      if (desktop != null)
        {
        MessageBox messagebox = new MessageBox(desktop, s, null,
                                  appReg.getString("error.TITLE"), MessageBox.ERROR, true);
        messagebox.setVisible(true);
        }
      else
        {
        System.out.println(s);
        }
      }
    parent = shellParent;
logger.info("oldID = " + old_id + ", oldName = " + old_name);
    initializeDialog();
    }


  public int areItemsAndRevsModifiable(TCComponentItem myItem)
    {
logger.info("Inside areItemAndRevsModifiable");
    try
      {
      AIFComponentContext compContext[] = myItem.getChildren();
      for (int i = 0; i < compContext.length; i++)
        {
        // see if children are modifiable
        if (! ( ( (TCComponent)(compContext[i].getComponent())).okToModify()))
          return (1);
        // if revision, check it's children
        if (compContext[i].getComponent() instanceof TCComponentItemRevision)
          {
          int rc = areRevsModifiable(((TCComponentItemRevision)(compContext[i].getComponent())));
          if(rc != 0)
             return(rc);
          }
        }
      }
    catch(TCException tcException)
      {
logger.info("Catch Exception Component = " + myItem.toString() + ", type = " + myItem.getType());
      String s = tcException.getError();
      MessageBox messagebox = new MessageBox(desktop, s,
                                  appReg.getString("error.TITLE"), MessageBox.ERROR);      messagebox.setModal(true);
      messagebox.setVisible(true);
      }
logger.info("Returning from areItemAndRevsModifiable");
    // return success
    return(0);
    }


  public int areRevsModifiable(TCComponentItemRevision myRev)
    {
logger.info("Inside areRevsModifiable");
    try
      {

      AIFComponentContext compContext[] = myRev.getChildren();

      for (int i = 0; i < compContext.length; i++)
        {
        if (  (compContext[i].getComponent() instanceof TCComponent) &&
           (! (compContext[i].getComponent() instanceof TCComponentFolder)) &&
           (! (compContext[i].getComponent() instanceof TCComponentPseudoFolder)))
          {
          // see if children are modifiable

          if (! ( ( (TCComponent)(compContext[i].getComponent())).okToModify()))
            return (1);

          }
        }
      }
    catch(TCException tcException)
      {

      String s = tcException.getError();
      MessageBox messagebox = new MessageBox(desktop, s,
                                  appReg.getString("error.TITLE"), 3);
      messagebox.setVisible(true);
      }
    // return success
    return(0);
    }

/*
  public void addPropertyChangeListener(PropertyChangeListener
                                        propertychangelistener) {
    propertySupport.addPropertyChangeListener(propertychangelistener);
  }*/

  public void endOperation()
    {    setVisible(false);    dispose();    setCursor(Cursor.getPredefinedCursor(0));	    	boolean suc = false;  
    suc = renameOp.getSuccessFlag();
//    renameOp.removeOperationListener(this);
    session.setReadyStatus();
    if (suc == true)
      {
      MessageBox messagebox = new MessageBox(desktop,
                                  appReg.getString("renameSuccess.info"),
                                  appReg.getString("info.TITLE"), MessageBox.INFORMATION);
      messagebox.setModal(true);
      messagebox.setVisible(true);
      }
    else
      {
      MessageBox messagebox = new MessageBox(desktop,
                                  appReg.getString("renameFailure.info"),
                                  appReg.getString("info.TITLE"), MessageBox.INFORMATION);
      messagebox.setModal(true);
      messagebox.setVisible(true);
      }
    }
/*
  public void fireStopOperation()
    {
    firePropertyChange("iMAN Stop Operation", null, null);
    }*/

  private void initializeDialog()
    {
logger.info("Inside RenameDialog::initializeDialog");    combobox = new JComboBox();
    jTable1 = new JTable()
      {
      public boolean isCellEditable(int row, int column)
        {
        if(column == 0)
          return false;
        else if((column == 1) && (rev == null))
          {
          try
            {
            if((((TCComponent)attachedObjects.elementAt(row)).getTCProperty("item_id").isModifiable()) == false)
              {

              return(false);
              }
            else
              return(true);
            }
          catch(TCException tcException)
            {
            String s = tcException.getError();
            if (desktop != null)
              {
              MessageBox messagebox = new MessageBox(desktop, s, null,
                                       appReg.getString("error.TITLE"), MessageBox.ERROR, true);
              messagebox.setVisible(true);
              }
            else
              {
              logger.info(s);
              }
            return(false);
            }
          }
        else
          return true;
        }
      };
    myTableModel = new CustomTableModel();

//    propertySupport = new PropertyChangeSupport(this);
    appReg = Registry.getRegistry(this);

    // old id
    old_idTextField = new JTextField(32);
    old_idTextField.setEditable(false);

    // new id
    new_idTextField = new JTextField(32);
    new_idTextField.addKeyListener(new KeyAdapter()
      {
      public void keyReleased(KeyEvent keyevent)
        {
logger.info("Inside new_idTextField keyReleased");
        JTextField jtextfield = (JTextField) keyevent.getSource();
        String s = jtextfield.getText();

        char keyChar = keyevent.getKeyChar();logger.info("keyChar = " + keyChar);
        for(int i = 0; i < invalidCharacters.length; i++)
          {logger.info("invalidCharacter[" + i + "] = " + invalidCharacters[i] + ", indexOf = " + s.indexOf(keyChar));
          if((keyChar == invalidCharacters[i])  && (s.indexOf(keyChar) != -1))
            {
            jtextfield.setText(s.substring(0, (s.length() - 1)));
            Toolkit.getDefaultToolkit().beep();
            break;
            }
          }        int byte0 = itemIdLength;
        if (s.length() > byte0)
          {
          jtextfield.setText(s.substring(0, byte0));
          Toolkit.getDefaultToolkit().beep();
          }
        validateFields();
        }
      });

    // old name
    old_nameTextField = new JTextField(32);
    old_nameTextField.setEditable(false);

    // new name
    new_nameTextField = new JTextField(32);
    if(nameEditable == true)      new_nameTextField.setEditable(true);    else	
      new_nameTextField.setEditable(false);
    new_nameTextField.addKeyListener(new KeyAdapter()
      {
      public void keyReleased(KeyEvent keyevent)
        {
        JTextField jtextfield = (JTextField) keyevent.getSource();
        String s = jtextfield.getText();
 
        int byte0 = nameLength; 
        if (s.length() > byte0)
          {
          jtextfield.setText(s.substring(0, byte0));
          Toolkit.getDefaultToolkit().beep();
          }
        validateFields();
        }
      });

    // old rev id
    old_revidTextField = new JTextField(32);
    old_revidTextField.setEditable(false);

    // new rev id
    new_revidTextField = new JTextField(32);
    new_revidTextField.addKeyListener(new KeyAdapter()
      {
       public void keyReleased(KeyEvent keyevent)
         {
         JTextField jtextfield = (JTextField) keyevent.getSource();
         String s = jtextfield.getText();
         char keyChar = keyevent.getKeyChar();         logger.info("keyChar = " + keyChar);                 for(int i = 0; i < invalidCharacters.length; i++)                   {         logger.info("invalidCharacter[" + i + "] = " + invalidCharacters[i] + ", indexOf = " + s.indexOf(keyChar));                   if((keyChar == invalidCharacters[i])  && (s.indexOf(keyChar) != -1))                     {                     jtextfield.setText(s.substring(0, (s.length() - 1)));                     Toolkit.getDefaultToolkit().beep();                     break;                     }                   }
         int byte0 = revIdLength;
         if (s.length() > byte0)
           {
           jtextfield.setText(s.substring(0, byte0));
           Toolkit.getDefaultToolkit().beep();
           }
         validateFields();
         }
      });

    // Rename Button definitions
    renameButton = new JButton("Rename");
    renameButton.setEnabled(false);
    renameButton.addActionListener(new ActionListener()
      {
      public void actionPerformed(ActionEvent actionevent)
        {
        renameButton.setEnabled(false);
        int checkID = 0;
        int doNotProceed = 0;
        // check to make sure that item id has not been used
        if(!(new_idTextField.getText().equalsIgnoreCase(old_id)))
          {
          try
            {
            TCComponentItemType itemType = (TCComponentItemType)session.getTypeComponent("Item");
            TCComponentItem item = itemType.find(new_idTextField.getText());
            if(item != null)
              {
              renameButton.setEnabled(false);
              MessageBox messagebox = new MessageBox(desktop, appReg.getString("existingItem.error"), appReg.getString("error.TITLE"), MessageBox.ERROR);
              messagebox.setModal(true);
              messagebox.setVisible(true);
              if(caseSensitivity.equalsIgnoreCase("UPPER"))              	new_idTextField.setText(old_id.toUpperCase());              else if(caseSensitivity.equalsIgnoreCase("LOWER"))              	new_idTextField.setText(old_id.toLowerCase());              else            	new_idTextField.setText(old_id);
              checkID = 1;
              }
            }
          catch(TCException tcException)
            {
            String s = tcException.getError();
            if (desktop != null)
              {
              MessageBox messagebox = new MessageBox(desktop, s, null,
                                      appReg.getString("error.TITLE"), MessageBox.ERROR, true);
              messagebox.setVisible(true);
              }
            else
              {
              System.out.println(s);
              }
            }
          }
        // check to make sure that item rev id has not been used
        if((rev != null) && (!(new_revidTextField.getText().equalsIgnoreCase(old_revid))))
          {
          try
            {
            TCComponentItemRevision findRev = rev.findRevision(new_revidTextField.getText());
            if(findRev != null)
              {
              renameButton.setEnabled(false);
              MessageBox messagebox = new MessageBox(desktop, appReg.getString("existingRev.error"), appReg.getString("error.TITLE"), MessageBox.ERROR);
              messagebox.setModal(true);
              messagebox.setVisible(true);
              if(caseSensitivity.equalsIgnoreCase("UPPER"))                new_revidTextField.setText(old_revid.toUpperCase());              else if(caseSensitivity.equalsIgnoreCase("LOWER"))                new_revidTextField.setText(old_revid.toLowerCase());              else                new_revidTextField.setText(old_revid);
              checkID = 1;
              }
            }
          catch(TCException tcException)
            {
            String s = tcException.getError();
            if (desktop != null)
              {
              MessageBox messagebox = new MessageBox(desktop, s, null,
                                       appReg.getString("error.TITLE"), MessageBox.ERROR, true);
              messagebox.setVisible(true);
              }
            else
              {
              System.out.println(s);
              }
            }
          }
        doNotProceed = checkID;
        if(doNotProceed == 0)
          startCreateOperation();
        }
      });

    //Cancel Button Defintions
    cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent actionevent)
      {
        session.setReadyStatus();
      setVisible(false);
      dispose();
      }
    });

    setDefaultCloseOperation(0);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowevent) {
          cancelButton.doClick();
      }
    });

    if(rev != null)
      setTitle(appReg.getString("command2.TITLE"));
    else
      setTitle(appReg.getString("command1.TITLE"));
    JPanel jpanel = new JPanel(new VerticalLayout(5, 2, 2, 2, 2));
    getContentPane().add(jpanel);
    JPanel jpanel1 = new JPanel(new ButtonLayout());
    JPanel jpanel4 = new JPanel(new VerticalLayout());
    JPanel jpanel5 = new JPanel(new PropertyLayout());

    JPanel jpanel6 = new JPanel(new PropertyLayout());

    JPanel tablePanel = new JPanel(new PropertyLayout());

    JLabel jLabel11 = new JLabel("             ");
    JLabel jLabel12 = new JLabel("                               Existing Value                               ");
    jLabel12.setHorizontalAlignment(0);
    JLabel jLabel13 = new JLabel("                               Desired Value                                ");
    jLabel13.setHorizontalAlignment(0);
    JLabel jLabel21 = new JLabel(" Item ID ");
    JLabel jLabel41 = new JLabel(" Item Name ");
    JLabel jLabel81 = new JLabel(" Revision ID ");

    // table model for matrix
    if(rev == null)
      {
      myTableModel.addColumn("Related Revision (ItemID  <>  RevID  <>  ITEM Name)", 300, JLabel.LEFT);
      myTableModel.addColumn("New Item ID", 200, JLabel.CENTER);
      myTableModel.addColumn("New ITEM Name", 200, JLabel.CENTER);
      }
    else
      {
      myTableModel.addColumn("Related Revision (ItemID  <>  RevID  <>  REVISION Name)", 300, JLabel.LEFT);
      myTableModel.addColumn("New Rev ID", 200, JLabel.CENTER);
      myTableModel.addColumn("New REVISION Name", 200, JLabel.CENTER);
      }
    // jtable for matrix
    jTable1.setAutoCreateColumnsFromModel(false);
    jTable1.setColumnSelectionAllowed(false);
    jTable1.setRowSelectionAllowed(false);
    jTable1.setDragEnabled(false);
    jTable1.setModel(myTableModel);
    jTable1.setShowGrid(true);
    jTable1.getTableHeader().setReorderingAllowed(false);
    jTable1.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

    // add columns to the table
    for (int k = 0; k < myTableModel.getColumnCount(); k++)
      {
      // cell renderer
      DefaultTableCellRenderer renderer;
      renderer = new myTableCellRenderer();
      // cell editor
      JTextField specialTextField = new JTextField();
      specialTextField.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
      specialTextField.setBackground(Color.WHITE);
      myTableCellEditor editor = null;
      editor = new myTableCellEditor(specialTextField);
      specialTextField.addKeyListener(new KeyAdapter()
        {
        public void keyReleased(KeyEvent keyevent)
          {
//System.out.println("keyReleased");
          JTextField jtextfield = (JTextField) keyevent.getSource();
          jtextfield.setBackground(Color.WHITE);
          String editValue = jtextfield.getText();
          int editRow = jTable1.getEditingRow();
          int editColumn = jTable1.getEditingColumn();
          if((editRow >= 0) && (editColumn > 0))
            editValue = jTable1.getCellEditor(editRow, editColumn).getCellEditorValue().toString();
          if(editValue != null)
            {            String oldID = null;
            if(rev == null)              {
              oldID = oldItemIDS.elementAt(editRow).toString();              }            
            else              {
              oldID = oldRevIDS.elementAt(editRow).toString();              }
            String oldName = oldNames.elementAt(editRow).toString();
            String newID = newIDS.elementAt(editRow).toString();
            String newName = newNames.elementAt(editRow).toString();
            jtextfield.setBackground(Color.WHITE);
            jtextfield.setForeground(Color.BLACK);
            if(editColumn == 1)
              {
              newID = editValue;
              newIDS.set(editRow, editValue);
              validateFields();
              }

            if(editColumn == 2)
              {
              newName = editValue;
              newNames.set(editRow, editValue);
              validateFields();
              }

            jtextfield.setBackground(Color.WHITE);
            if((editColumn == 1) && (! (editValue.equals(oldID))))
              jtextfield.setBackground(Color.YELLOW);
            if((editColumn == 2) && (! (editValue.equals(oldName))))
              jtextfield.setBackground(Color.YELLOW);

            if((editColumn == 1) && (newID.length() > 0) && (! (newID.equals(oldID))))
              jtextfield.setBackground(Color.YELLOW);
            if((editColumn == 2) && (newName.length() > 0) && (! (newName.equals(oldName))))
              jtextfield.setBackground(Color.YELLOW);

            }
          String s = jtextfield.getText();
          if((editColumn == 1) && (rev == null))
            {
            char keyChar = keyevent.getKeyChar();
            for(int i = 0; i < invalidCharacters.length; i++)
              {
              if((keyChar == invalidCharacters[i])  && (s.indexOf(keyChar) != -1))
                {
                jtextfield.setText(s.substring(0, (s.length() - 1)));
                Toolkit.getDefaultToolkit().beep();
                newIDS.set(editRow, s.substring(0, (s.length() - 1)));
                if(oldItemIDS.elementAt(editRow).equals(s.substring(0, (s.length() - 1))))
                  jtextfield.setBackground(Color.WHITE);
                else
                  jtextfield.setBackground(Color.YELLOW);
                validateFields();
                }
              }
            }
          int byte0 = 32;          if(rev == null)            {        	if(editColumn == 1)        	  byte0 = itemIdLength;        	else if(editColumn == 2)        	  byte0 = nameLength;            }          else            {          	if(editColumn == 1)          	  byte0 = revIdLength;          	else if(editColumn == 2)          	  byte0 = nameLength;            }
          if (s.length() > byte0)
            {
            jtextfield.setText(s.substring(0, byte0));
            Toolkit.getDefaultToolkit().beep();
            if(editColumn == 1)
              newIDS.set(editRow, s.substring(0, byte0));
            else if(editColumn == 2)
              newNames.set(editRow, s.substring(0, byte0));
            }
          validateFields();
          }
        });

      TableColumn column = new TableColumn( k,
                                            myTableModel.getColumnWidth(k),
                                            renderer,
                                            editor);
      jTable1.addColumn(column);
      }
    JScrollPane tableScrollPane = new JScrollPane();

    tableScrollPane.getViewport().add(jTable1);
    tableScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    tableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

    // Add the Dialog components
    jpanel1.add(renameButton);
    jpanel1.add(cancelButton);

    jpanel5.add("1.1.right.top", jLabel11);
    jpanel5.add("1.2.right.top", jLabel12);
    jpanel5.add("1.3.right.top", jLabel13);
    jpanel5.add("2.1.right.top", jLabel21);
    jpanel5.add("2.2.right.top", old_idTextField);
    jpanel5.add("2.3.right.top", new_idTextField);
    jpanel5.add("3.1.right.top", jLabel41);
    jpanel5.add("3.2.right.top", old_nameTextField);
    jpanel5.add("3.3.right.top", new_nameTextField);
    jpanel5.add("4.1.right.top", combobox);
    if(rev != null)
      {
      jLabel41.setText(" Rev Name ");
      jpanel5.add("4.1.right.top", jLabel81);
      jpanel5.add("4.2.right.top", old_revidTextField);
      jpanel5.add("4.3.right.top", new_revidTextField);      jpanel5.add("5.1.right.top", combobox);
      }

    // Add the Dialog components and pack
    jpanel.add("top.bind", jpanel4);
    jpanel.add("top.bind", new JLabel(""));
    jpanel.add("top.bind", new JLabel(""));
    jpanel.add("top.bind", jpanel5);
    jpanel.add("top.bind.resizable.resizable",jpanel6);
//System.out.println("attachedObjects.size = " + attachedObjects.size());
    if(attachedObjects.size() > 0)
      {
      if(rev == null)
        tablePanel.add("1.1", new JLabel("Attached Solid Model ITEM Attributes"));
      else
        tablePanel.add("1.1", new JLabel("Attached Solid Model ITEM REVISION Attributes"));
      tablePanel.add("2.1", tableScrollPane);
      jpanel.add("top.bind", tablePanel);
      tableScrollPane.getViewport().setPreferredSize(new Dimension(760, 110));
      int rh = jTable1.getRowHeight();
      jTable1.setRowHeight(rh+2);
      }
    jpanel.add("bottom.bind.center.top", jpanel1);
    jpanel.add("bottom.bind", new Separator());
    pack();    
    setModal(true);
    centerToScreen(1.0D, 1.0D);
    startLoadOperation();
    } // End of initializeDialog Class

  private int checkExistingIds(int context, TCComponentItemRevision thisRev)
    {
    int idCheck = 0;
    String myold_id = null;
    String mynew_id = null;
    String myold_revid = null;
    String mynew_revid = null;
    if(context == -1)
      {
      myold_id = old_idTextField.getText();
      mynew_id = new_idTextField.getText();
      myold_revid = old_idTextField.getText();
      mynew_revid = new_idTextField.getText();
      }
    else
      {
      if(rev == null)
        myold_id = oldItemIDS.elementAt(context).toString();
      else
        myold_id = oldRevIDS.elementAt(context).toString();
      mynew_id = jTable1.getValueAt(context, 1).toString();
      myold_revid = oldRevIDS.elementAt(context).toString();
      mynew_revid = jTable1.getValueAt(context, 2).toString();
      }
    // check to make sure that item id has not been used
    if(!(mynew_id.equalsIgnoreCase(myold_id)))
      {
      try
        {
        TCComponentItemType itemType = (TCComponentItemType)session.getTypeComponent("Item");
        TCComponentItem item = itemType.find(mynew_id);
        if(item != null)
          {
          renameButton.setEnabled(false);
          String msg = appReg.getString("existingItem.error") + "\nRev = " + thisRev.toString() + ", New Item ID = " + mynew_id;
          MessageBox messagebox = new MessageBox(desktop, msg, appReg.getString("error.TITLE"), MessageBox.ERROR);
          messagebox.setModal(true);
          messagebox.setVisible(true);
          idCheck = 1;
          }
        }
      catch(TCException tcException)
        {
        String s = tcException.getError();
        if (desktop != null)
          {
          MessageBox messagebox = new MessageBox(desktop, s, null,
                                  appReg.getString("error.TITLE"), MessageBox.ERROR, true);
          messagebox.setVisible(true);
          }
        else
          {
          System.out.println(s);
          }
        }
      }
    // check to make sure that item rev id has not been used
    if((thisRev != null) && (!(mynew_revid.equalsIgnoreCase(myold_revid))))
      {
      try
        {
        TCComponentItemRevision findRev = thisRev.findRevision(mynew_revid);
        if(findRev != null)
          {
          renameButton.setEnabled(false);
          String msg = appReg.getString("existingRev.error") + "\nRev = " + thisRev.toString() + ", New Rev ID = " + mynew_revid;
          MessageBox messagebox = new MessageBox(desktop, msg, appReg.getString("error.TITLE"), MessageBox.ERROR);
          messagebox.setVisible(true);
          idCheck = 1;
          }
        }
      catch(TCException tcException)
        {
        String s = tcException.getError();
        if (desktop != null)
          {
          MessageBox messagebox = new MessageBox(desktop, s, null,
                                   appReg.getString("error.TITLE"), MessageBox.ERROR, true);
          messagebox.setVisible(true);
          }
        else
          {
          System.out.println(s);
          }
        }
      }
    return idCheck;
    }


  private void populateFields()
    {logger.info("Inside populateFields");
    // init values
    if(caseSensitivity.equalsIgnoreCase("UPPER"))      new_id = old_id.toUpperCase();    else if(caseSensitivity.equalsIgnoreCase("LOWER"))      new_id = old_id.toLowerCase();    else      new_id = old_id;
    if(caseSensitivity.equalsIgnoreCase("UPPER"))      new_name = old_name.toUpperCase();    else if(caseSensitivity.equalsIgnoreCase("LOWER"))      new_name = old_name.toLowerCase();    else      new_name = old_name;
//System.out.println("new_name1 = " + new_name);
    if(caseSensitivity.equalsIgnoreCase("UPPER"))      new_revid = old_revid.toUpperCase();    else if(caseSensitivity.equalsIgnoreCase("LOWER"))      new_revid = old_revid.toLowerCase();    else      new_revid = old_revid;
    if(rev != null)
      {
      new_idTextField.setEnabled(true);
      new_idTextField.setEditable(false);
      }
    old_idTextField.setEnabled(true);
    old_idTextField.setEditable(false);
    old_nameTextField.setEnabled(true);
    old_nameTextField.setEditable(false);
    old_revidTextField.setEnabled(true);
    old_revidTextField.setEditable(false);
    // Places the info into the fields
    old_idTextField.setText(old_id);
    new_idTextField.setText(new_id);
    old_nameTextField.setText(old_name);
    new_nameTextField.setText(new_name);
    old_revidTextField.setText(old_revid);
    new_revidTextField.setText(new_revid);
    // populate jtable with rows
    for(int q = 0; q < attachedObjects.size(); q++)
      {
      if(rev == null)
        myTableModel.addRow(displayStrings.elementAt(q).toString(), oldItemIDS.elementAt(q).toString(), oldNames.elementAt(q).toString());
      else
        myTableModel.addRow(displayStrings.elementAt(q).toString(), oldRevIDS.elementAt(q).toString(), oldNames.elementAt(q).toString());
      }    if(nameEditable == false)      {
      for(int i = 0; i < jTable1.getRowCount(); i++)        {        TableCellEditor edit = jTable1.getCellEditor(i, 2);        Object value = edit.getCellEditorValue();        edit.getTableCellEditorComponent(jTable1, value, true, i, 2).setEnabled(false);        edit.getTableCellEditorComponent(jTable1, value, false, i, 2).setEnabled(false);        }      }        validateFields();        setVisible(true);
    } // End of PopulateFields Class

  private void validateFields()
    {
logger.info("Inside validateFields");
    String testString = null;
    if(caseSensitivity.equalsIgnoreCase("UPPER"))      testString = new_idTextField.getText().toUpperCase();    else if(caseSensitivity.equalsIgnoreCase("LOWER"))      testString = new_idTextField.getText().toLowerCase();    else      testString = new_idTextField.getText();
    if (! (new_idTextField.getText().equals(testString)))
      new_idTextField.setText(testString);
    if(rev == null)
      {
      if(caseSensitivity.equalsIgnoreCase("UPPER"))        testString = new_nameTextField.getText().toUpperCase();      else if(caseSensitivity.equalsIgnoreCase("LOWER"))        testString = new_nameTextField.getText().toLowerCase();      else        testString = new_nameTextField.getText();
      if (! (new_nameTextField.getText().equals(testString)))
        new_nameTextField.setText(testString);
      }
    if(caseSensitivity.equalsIgnoreCase("UPPER"))      testString = new_revidTextField.getText().toUpperCase();    else if(caseSensitivity.equalsIgnoreCase("LOWER"))      testString = new_revidTextField.getText().toLowerCase();    else      testString = new_revidTextField.getText();
    if(!(new_revidTextField.getText().equals(testString)))
      new_revidTextField.setText(testString);

    renameButton.setEnabled(false);    boolean emptyValue = false;
    int change = 0;
    // JTable matrix level
    for(int q = 0; q < attachedObjects.size(); q++)
      {
//System.out.println("Processing: " + attachedObjects.elementAt(q).toString());
      String oldID = null;
      if(rev == null)
        {
        oldID = oldItemIDS.elementAt(q).toString();
        // make id non-editable if it's not really modifiable
        try
          {
          if((((TCComponent)attachedObjects.elementAt(q)).getTCProperty("item_id").isModifiable()) == false)
            {
//System.out.println("I know that I need to grey out id for " + attachedObjects.elementAt(q).toString());
            jTable1.getCellEditor(q, 1).getTableCellEditorComponent(jTable1, oldID, false, q, 1).setEnabled(false);
            }
          }
        catch(TCException tcException)
          {
          String s = tcException.getError();
          if (desktop != null)
            {
            MessageBox messagebox = new MessageBox(desktop, s, null,
                                     appReg.getString("error.TITLE"), MessageBox.ERROR, true);
            messagebox.setVisible(true);
            }
          else
            {
            System.out.println(s);
            }
          }
        }
      else
        oldID = oldRevIDS.elementAt(q).toString();
      String oldName = oldNames.elementAt(q).toString();
      String newID = newIDS.elementAt(q).toString();
      String newName = newNames.elementAt(q).toString();      if((newID == null) || newID.length() == 0)    	  emptyValue = true;      if((newName == null) || newName.length() == 0)    	  emptyValue = true;      
//System.out.println(" oldID = " + oldID + ", newID = " + newID + ", newID2 = " + jTable1.getValueAt(q, 1) + ", newName = " + newName);
      if((! (newID.equals(oldID))))
        {
//System.out.println("ID Change");
        change++;
        }
      if((! (newName.equals(oldName))))
        {
//System.out.println("Name Change");
        change++;
        }
      }
    old_idTextField.setEnabled(true);
    old_idTextField.setEditable(false);
    old_nameTextField.setEnabled(true);
    old_nameTextField.setEditable(false);
    old_revidTextField.setEnabled(true);
    old_revidTextField.setEditable(false);

    // make id non-editable if it's not really modifiable
    try
      {
      if(item.getTCProperty("item_id").isModifiable() == false)
        new_idTextField.setEditable(false);
      }
    catch(TCException tcException)
      {
      String s = tcException.getError();
      if (desktop != null)
        {
        MessageBox messagebox = new MessageBox(desktop, s, null,
                                 appReg.getString("error.TITLE"), MessageBox.ERROR, true);
        messagebox.setVisible(true);
        }
      else
        {
        System.out.println(s);
        }
      }

    if(new_idTextField.isEditable())
      new_idTextField.setBackground(Color.WHITE);
    if(nameEditable == true)      new_nameTextField.setEditable(true);    else	      new_nameTextField.setEditable(false);
    if(new_nameTextField.isEditable())
      new_nameTextField.setBackground(Color.WHITE);
    if(new_revidTextField.isEditable())
      new_revidTextField.setBackground(Color.WHITE);
    if(rev != null)
      {
      new_id = old_id;
      new_idTextField.setEnabled(true);
      new_idTextField.setEditable(false);
      // make id non-editable if it's not really modifiable
      try
        {
        if(rev.getTCProperty("item_revision_id").isModifiable() == false)
          new_revidTextField.setEditable(false);
        }
      catch(TCException tcException)
        {
        String s = tcException.getError();
        if (desktop != null)
          {
          MessageBox messagebox = new MessageBox(desktop, s, null,
                                   appReg.getString("error.TITLE"), MessageBox.ERROR, true);
          messagebox.setVisible(true);
          }
        else
          {
          System.out.println(s);
          }
        }
      }
    // add up if we need a change    if((new_idTextField.getText() == null) || new_idTextField.getText().length() == 0)  	  emptyValue = true;
    if((!(new_idTextField.getText().equals(old_id))))
      {
      new_idTextField.setBackground(Color.YELLOW);
      change++;
      // JBH2 - auto upper case for Item ID field
      if(caseSensitivity.equalsIgnoreCase("UPPER"))        testString = new_idTextField.getText().toUpperCase();      else if(caseSensitivity.equalsIgnoreCase("LOWER"))        testString = new_idTextField.getText().toLowerCase();      else        testString = new_idTextField.getText();
      if (! (new_idTextField.getText().equals(testString)))
        {
        new_idTextField.setText(testString);
        if(new_idTextField.getText().equals(old_id))
          {
          change--;
          new_idTextField.setBackground(Color.WHITE);
          }
        }
      // JBH2
      }
    if((new_nameTextField.getText() == null) || new_nameTextField.getText().length() == 0)    	  emptyValue = true;        if((! (new_nameTextField.getText().equals(old_name))))
      {
      new_nameTextField.setBackground(Color.YELLOW);
      change++;
      // JBH2 - auto upper case for Name field
      if(caseSensitivity.equalsIgnoreCase("UPPER"))        testString = new_nameTextField.getText().toUpperCase();      else if(caseSensitivity.equalsIgnoreCase("LOWER"))        testString = new_nameTextField.getText().toLowerCase();      else        testString = new_nameTextField.getText();
      if(!(new_nameTextField.getText().equals(testString)))
        {
        new_nameTextField.setText(testString);
        if(new_nameTextField.getText().equals(old_name))
          {
          change--;
          new_nameTextField.setBackground(Color.WHITE);
          }
        }
      // JBH2
      }
    if((new_revidTextField.getText() == null) || new_revidTextField.getText().length() == 0)    	  emptyValue = true;
    if((!(new_revidTextField.getText().equals(old_revid))))
      {
      new_revidTextField.setBackground(Color.YELLOW);
      change++;
      // JBH2 - auto upper case for Rev ID field
      if(caseSensitivity.equalsIgnoreCase("UPPER"))        testString = new_revidTextField.getText().toUpperCase();      else if(caseSensitivity.equalsIgnoreCase("LOWER"))        testString = new_revidTextField.getText().toLowerCase();      else        testString = new_revidTextField.getText();
      if(!(new_revidTextField.getText().equals(testString)))
        {
        new_revidTextField.setText(testString);
        if(new_revidTextField.getText().equals(old_revid))
          {
          change--;
          new_revidTextField.setBackground(Color.WHITE);
          }
        }
      // JBH2
      }
//System.out.println("change = " + change);
    //Adding check to make sure rename button is only on when appropriate
        if((change != 0) && (emptyValue == false))
      {
      renameButton.setEnabled(true);
      renameButton.setFocusable(true);
      }
    else
      renameButton.setEnabled(false);
    }
/*
  public void removePropertyChangeListener(PropertyChangeListener
                                           propertychangelistener) {
    propertySupport.removePropertyChangeListener(propertychangelistener);
  }*/

  public void run() {logger.info("Inside RenameDialog::run");	  
    setVisible(true);
  }

  
  private void startCreateOperation()
    {
logger.info("Inside RenameDialog::startCreateOperation");if(desktop == null)	logger.info("desktop is NULL at RenameDialog::RenameOperation constructor call!!!"); 
    renameOp = new RenameOperation(session, desktop, getOldID(), getNewID(),
               getOldName(), getNewName(), getOldRevID(), getNewRevID(),
               item, rev, jTable1, attachedObjects);
//    renameOp.addOperationListener(this);
//    session.queueOperation(renameOp);    try      {      session.setStatus(appReg.getString("renamingOperation"));      setVisible(false);      dispose();      setCursor(Cursor.getPredefinedCursor(0));      renameOp.execute();      endOperation();      }        catch(Exception exception)      {      String s = exception.getMessage();      if (desktop != null)        {        MessageBox messagebox = new MessageBox(desktop, s, null,                                appReg.getString("error.TITLE"), MessageBox.ERROR, true);        messagebox.setVisible(true);        }      else        {        logger.info(s);        }      }
    }
  private void startLoadOperation()
    {logger.info("Inside RenameDialog::startLoadOperation");
    loadOp = new LoadOperation();
    session.queueOperation(loadOp);
    }

  private class LoadOperation extends AbstractAIFOperation  {	    public void executeOperation()    {	  logger.info("Inside RenameDialog::LoadOperation::executeOperation");    populateFields();    }/*   runnable1 = new ()    {    public void run()      {logger.info("Inside RenameDialog::LoadOperation::run");      validateFields();      }    };*/  LoadOperation()    {    }  } //End of LoadOperation Class

  public void startOperation(String s)
    {logger.info("Inside RenameDialog::startOperation");
    renameButton.setVisible(false);
    cancelButton.setVisible(false);
    new_idTextField.setEnabled(false);
    new_nameTextField.setEnabled(false);
    new_revidTextField.setEnabled(false);
    validate();
    for(int i = 0; i < jTable1.getRowCount(); i++)
      {
      Object value = "";
      boolean isSelected = false;
      boolean hasFocus = false;
      for(int j = 0; j < jTable1.getColumnCount(); j++)
        {
        TableCellRenderer rend = jTable1.getCellRenderer(i, j);
        TableCellEditor edit = jTable1.getCellEditor(i, j);
        edit.getTableCellEditorComponent(jTable1, value, isSelected, i, j).setEnabled(false);
        rend.getTableCellRendererComponent(jTable1, value, isSelected, hasFocus, i, j).setEnabled(false);
        }
      }
    validateFields();
    }
  } // end of class






////////////////////////////////////////////////////////////////////////////////
// Custom Table Model
////////////////////////////////////////////////////////////////////////////////
class CustomTableModel extends AbstractTableModel
  {	  private static final long serialVersionUID = 1L;
  public Vector<rowData> vectorTableData = new Vector<rowData>();
  public Vector<ColumnData> columnData = new Vector<ColumnData>();
  int[] indexes;
  public int[] getIndexes()
    {
    int n = getRowCount();
    if (indexes != null)
      {
      if (indexes.length == n)
        {
        return indexes;
        }
      }
    indexes = new int[n];
    for (int i = 0; i < n; i++)
      {
      indexes[i] = i;
      }
    return indexes;
    }

  // Get the number of columns.
  public int getColumnCount()
    {
    return columnData.size();
    }
  // Get the number of rows.
  public int getRowCount()
    {
    return vectorTableData.size();
    }
  public Class getColumnClass (int c)
    {
    return getValueAt(0, c).getClass();
    }
  // Add a new column to the table.
  public void addColumn(String colName, int width, int alignment)
    {
    columnData.addElement(new ColumnData(colName, width, alignment));
    }
  // Get the column name.
  public String getColumnName(int col)
    {
    return ((ColumnData) columnData.elementAt(col)).m_title;
    }
  // Get the column width.
  public int getColumnWidth(int col)
    {
    return ((ColumnData) columnData.elementAt(col)).m_width;
    }
  // Get the column alignment.
  public int getColumnAlignment(int col)
    {
    return ((ColumnData) columnData.elementAt(col)).m_alignment;
    }

  // Get value of a cell at column/row.
  public Object getValueAt(int row, int col)
    {
    if (row < 0 || row >= getRowCount())
      return "";
    int rowIndex = row;
    if(indexes != null)
      {
      rowIndex = indexes[row];
      }
    rowData r = (rowData) vectorTableData.elementAt(rowIndex);
    switch (col)
      {
      case 0 :
        return r.m_rev;
      case 1 :
        return r.m_newID;
      case 2 :
        return r.m_newName;
      }
    return "";
    }

  // Set the value of a cell at column/row.
  public void setValueAt(Object value, int row, int col)
    {
    if (row < 0 || row >= getRowCount())
      return;
    int rowIndex = row;
    if(indexes != null)
      {
      rowIndex = indexes[row];
      }
    rowData r = (rowData) vectorTableData.elementAt(rowIndex);
    switch (col)
      {
      case 0 :
        break; // do nothing, read only
      case 1 :    	  
        if(((String)value).length() > 32)
          {
          MessageBox messagebox1 = new MessageBox("New ID values cannot exceed 32 characters", "Error", MessageBox.ERROR);
          messagebox1.setModal(true);
          messagebox1.setVisible(true);
          r.m_newID = (String) this.getValueAt(row, col);
          fireTableDataChanged();
          break;
          }
        else
          r.m_newID = (String) value;
        fireTableDataChanged();
        break;
      case 2 :
        if(((String)value).length() > 32)
          {
          MessageBox messagebox1 = new MessageBox("New Name values cannot exceed 32 characters", "Error", MessageBox.ERROR);
          messagebox1.setModal(true);
          messagebox1.setVisible(true);
          r.m_newName = (String) this.getValueAt(row, col);
          fireTableDataChanged();
          break;
          }
        else
          r.m_newName = (String) value;
        fireTableDataChanged();
        break;
      default:
        break;
      }
    }

  // Make the cells editable or non-editable.
  public boolean isCellEditable(int row, int col)
    {
    if(col == 0)
      return false;
    else
      return true;
    }

  // Add a new row to the table.
  public void addRow(String part, String newID, String newName)
    {
    vectorTableData.addElement(new rowData(part, newID, newName));
    fireTableRowsInserted( vectorTableData.size() - 1,
                           vectorTableData.size() - 1);
    }

  // Remove a row from the table.
  public void removeRow(int row)
    {
    vectorTableData.removeElementAt(row);
    fireTableRowsDeleted(vectorTableData.size(), vectorTableData.size());
    }

  // Remove all of the rows in the table.
  public void removeAllRow()
    {
    vectorTableData.removeAllElements();
    fireTableRowsDeleted(vectorTableData.size(), vectorTableData.size());
    }

  // Private class to hold the column data
  class ColumnData
    {
    public String m_title;
    public int m_width;
    public int m_alignment;
    public ColumnData(String title, int width, int alignment)
      {
      m_title = title;
      m_width = width;
      m_alignment = alignment;
      }
    }
  }

//////////////////////////////////////////////////////////////////////////////
// Class to store the data of each row in the table
//////////////////////////////////////////////////////////////////////////////
class rowData
  {
  public String m_rev;
  public String m_newID;
  public String m_newName;
  public rowData(String part, String newIID, String newName)
    {
    m_rev = part;
    m_newID = newIID;
    m_newName = newName;
    }
  }


////////////////////////////////////////////////////////////////////////////////
// Custom cell renderer for text field.
////////////////////////////////////////////////////////////////////////////////
class myTableCellRenderer extends DefaultTableCellRenderer
  {  private static final long serialVersionUID = 1L;
  public myTableCellRenderer()
    {
    super();
    }
  public void setValue(Object value)
    {
    if (value instanceof TCComponentItemRevision)
      {
      if (value != null)
        {
        setText(value.toString());
        }
      }
    else
      {
      super.setValue(value);
      }
    }
  public Component getTableCellRendererComponent( JTable  table,
                                                  Object  value,
                                                  boolean isSelected,
                                                  boolean hasFocus,
                                                  int     row,
                                                  int     column)
    {
    Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//System.out.println("Inside cust rend, row = " + row + ", column = " + column + "rowRev = " + rowRev.toString());
    String rowString = ((String)(table.getValueAt(row, 0)));
    String rowOldID = null;
    String rowOldRevID = null;
    String rowOldName = null;
    String splitString[] = rowString.split("  <>  ");
    rowOldID = splitString[0];
    rowOldRevID = splitString[1];
    rowOldName = splitString[2];
//System.out.println("rowOldID = " + rowOldID + ", rowOldRevID = " + rowOldRevID + ", " + rowOldName);
//      rowOldID = rowRev.getTCProperty("item_id").toString();
//      rowOldRevID = rowRev.getTCProperty("item_revision_id").toString();
//      rowOldName = rowRev.getTCProperty("object_name").toString();
    int match = 0;
    if(column == 1)
      {
      match = 0;
      if ( (value.toString().equals(rowOldID)) || (value.toString().equals(rowOldRevID)))
        match = 1;
      }
    else if(column == 2)
      {
      match = 0;
      if(value.toString().equals(rowOldName))
        match = 1;
      }
    cell.setForeground(Color.BLACK);
    cell.setBackground(new Color(236, 233, 216));
    if(table.isCellEditable(row,column) == false)
      {
      cell.setBackground(new Color(236, 233, 216));
      }
    if(table.isCellEditable(row,column) == true)
      {
      cell.setBackground(Color.WHITE);
      if(match == 0)
        {
        cell.setBackground(Color.YELLOW);
        }
      }
    setValue(value);
    return cell;
    }

  }

////////////////////////////////////////////////////////////////////////////////
// Custom cell editor for text field.
////////////////////////////////////////////////////////////////////////////////

class myTableCellEditor extends AbstractCellEditor implements TableCellEditor
  {  private static final long serialVersionUID = 1L;
  // This is the component that will handle the editing of the cell value
  JComponent component = new JTextField();
  private JTextField tf;
  public myTableCellEditor(JTextField textField)
    {
    super();
    tf = textField;
    component = textField;
    }

  // This method is called when a cell value is edited by the user.
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
//System.out.println("Inside myTableCellEditor::getTableCellEditorComponent");
    //
    JTextField cell = ((JTextField)component);
//System.out.println("Inside cust edit, row = " + row + ", column = " + column + ", value = " + value.toString());
    String rowOldID = null;
    String rowOldRevID = null;
    String rowOldName = null;
    String rowString = ((String)(table.getValueAt(row, 0)));
    String splitString[] = rowString.split("  <>  ");
    rowOldID = splitString[0];
    rowOldRevID = splitString[1];
    rowOldName = splitString[2];
    int match = 0;
    if(column == 1)
      {
      match = 0;
      if((value.toString().equals(rowOldID)) || (value.toString().equals(rowOldRevID)))
        match = 1;
      }
    else if(column == 2)
      {
      match = 0;
      if(value.toString().equals(rowOldName))
        match = 1;
      }
    cell.setForeground(Color.BLACK);
    cell.setBackground(new Color(236, 233, 216));
    if(table.isCellEditable(row,column) == false)
      {
      cell.setBackground(new Color(236, 233, 216));
      }
    if(table.isCellEditable(row,column) == true)
      {
      cell.setBackground(Color.WHITE);
      if(match == 0)
        {
        cell.setBackground(Color.YELLOW);
        }
      }

    // Configure the component with the specified value
    ((JTextField)component).setText((String)value);

    // Return the configured component
    return component;
    }

  // This method is called when editing is completed.
  // It must return the new value to be stored in the cell.
  public Object getCellEditorValue()
    {
//System.out.println("Inside myTableCellEditor::getCellEditorValue");
    return ((JTextField)component).getText();
    }
  }
