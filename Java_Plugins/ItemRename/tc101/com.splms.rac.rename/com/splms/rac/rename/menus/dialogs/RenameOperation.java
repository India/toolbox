package com.splms.rac.rename.menus.dialogs;
import java.awt.Component;
import java.beans.*;
import java.io.PrintStream;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.text.JTextComponent;
import javax.swing.JComponent;
import javax.swing.table.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import org.apache.log4j.Logger;import java.util.Vector;
import java.util.Vector.*;
import com.teamcenter.rac.aif.*;
import com.teamcenter.rac.aif.kernel.AbstractAIFSession;
import com.teamcenter.rac.aif.kernel.InterfaceAIFComponent;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.commands.open.OpenCommand;
import com.teamcenter.rac.commands.paste.PasteCommand;
import com.teamcenter.rac.kernel.*;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.rac.kernel.TCPreferenceService;
public class RenameOperation extends AbstractAIFOperation
  {
  private static Logger logger = Logger.getLogger("com.splms.rac.rename");  protected String oldID;
  protected String newID;
  protected String oldName;
  protected String newName;
  protected String oldRevID;
  protected String newRevID;
  protected int idFlag;
  protected int revIDFlag;
  protected int nameFlag;
  protected TCComponentItem item;
  protected TCComponentItemRevision rev;
  protected JTable jtable;
  protected Vector<TCComponent> attachedObjects = null;
  protected TCSession session;
  protected AIFDesktop desktop;
  protected Registry registry;
  private PropertyChangeSupport propertySupport;
  private boolean successFlag;
  public RenameOperation(TCSession tcSession,
                         AIFDesktop aifDesktop,
                         String s1,
                         String s2,
                         String s3,
                         String s4,
                         String s5,
                         String s6,
                         TCComponentItem tcItem,
                         TCComponentItemRevision tcItemRev,
                         JTable jtbl,
                         Vector<TCComponent> attachedObjs    )
    {
logger.info("Inside RenameOperation");
    oldID = null;
    newID = null;
    oldName = null;
    newName = null;
    oldRevID = null;
    newRevID = null;
    idFlag = 0;
    revIDFlag = 0;
    nameFlag = 0;
    item = null;
    rev = null;
    jtable = null;
    attachedObjects = new Vector<TCComponent>();
    session = null;
    desktop = null;
    registry = null;
    successFlag = true;
    session = tcSession;
    if(aifDesktop == null)    	logger.info("aifDesktop is NULL at TOP!!!");    desktop = aifDesktop;
    if(desktop == null)    	logger.info("desktop is NULL at TOP!!!");    registry = Registry.getRegistry(this);
    oldID = s1;
    newID = s2;
    oldName = s3;
    newName = s4;
    oldRevID = s5;
    newRevID = s6;
    item = tcItem;
    rev = tcItemRev;
    jtable = jtbl;
    attachedObjects = attachedObjs;
    propertySupport = new PropertyChangeSupport(this);
    }/*
  public void addPropertyChangeListener(PropertyChangeListener propertychangelistener)
    {
    propertySupport.addPropertyChangeListener(propertychangelistener);
    }*/
  public void execute()throws Exception    {	  logger.info("Inside RenameOperation::execute");    session.setStatus(registry.getString("renamingOperation"));	  	    workTheMagic();	    for(int i = 0; i < attachedObjects.size(); i++)	      {	      // setup stuff	      String rowString = ((String)(jtable.getValueAt(i, 0)));	      String rowOldID = null;	      String rowOldRevID = null;	      String rowOldName = null;	      String splitString[] = rowString.split("  <>  ");	      rowOldID = splitString[0];	      rowOldRevID = splitString[1];	      rowOldName = splitString[2];	      // item context	      if(attachedObjects.elementAt(i) instanceof TCComponentItem)	        {	        item = (TCComponentItem)attachedObjects.elementAt(i);	        rev = null;	        oldID = rowOldID;	        newID = ((String)jtable.getValueAt(i, 1));	        oldName = rowOldName;	        newName = ((String)jtable.getValueAt(i, 2));	        oldRevID = "N/A";	        newRevID = "N/A";	        }	      // rev context	      else if(attachedObjects.elementAt(i) instanceof TCComponentItemRevision)	        {	        rev = (TCComponentItemRevision)attachedObjects.elementAt(i);	        item = rev.getItem();	        oldID = rowOldID;	        newID = rowOldID;	        oldName = rowOldName;	        newName = ((String)jtable.getValueAt(i, 2));	        oldRevID = rowOldRevID;	        newRevID = ((String)jtable.getValueAt(i, 1));	        }	      // do it	      workTheMagic();	      }	    // set ready status	    session.setReadyStatus();    }  
  public void executeOperation()throws Exception
    {
logger.info("Inside RenameOperation::executeOperation");
    session.setStatus(registry.getString("renamingOperation"));
    workTheMagic();
    // top level context
logger.info(" ");
logger.info("oldID = " + oldID + ", newID = " + newID + ", oldName = " + oldName + ", newName = " + newName + "\n  oldRevID = " + oldRevID + ", newRevID = " + newRevID);
//if(item == null)
logger.info("item is NULL");
//if(rev == null)
logger.info("rev is NULL");

    workTheMagic();

    // now loop thru related/attached objects if any
logger.info(" ");
//if(jtable != null)
logger.info("JTable rowCount = " + jtable.getRowCount() + ", columnCount = " + jtable.getColumnCount());
logger.info(" ");
logger.info("Found " + attachedObjects.size() + " Related Objects");
    for(int i = 0; i < attachedObjects.size(); i++)
      {

      // setup stuff
      String rowString = ((String)(jtable.getValueAt(i, 0)));
      String rowOldID = null;
      String rowOldRevID = null;
      String rowOldName = null;
      String splitString[] = rowString.split("  <>  ");
      rowOldID = splitString[0];
      rowOldRevID = splitString[1];
      rowOldName = splitString[2];

      // item context
      if(attachedObjects.elementAt(i) instanceof TCComponentItem)
        {
        item = (TCComponentItem)attachedObjects.elementAt(i);
        rev = null;
        oldID = rowOldID;
        newID = ((String)jtable.getValueAt(i, 1));
        oldName = rowOldName;
        newName = ((String)jtable.getValueAt(i, 2));
        oldRevID = "N/A";
        newRevID = "N/A";
        }
      // rev context
      else if(attachedObjects.elementAt(i) instanceof TCComponentItemRevision)
        {
        rev = (TCComponentItemRevision)attachedObjects.elementAt(i);
        item = rev.getItem();
        oldID = rowOldID;
        newID = rowOldID;
        oldName = rowOldName;
        newName = ((String)jtable.getValueAt(i, 2));
        oldRevID = rowOldRevID;
        newRevID = ((String)jtable.getValueAt(i, 1));
        }

logger.info("oldID = " + oldID + ", newID = " + newID + ", oldName = " + oldName + ", newName = " + newName + "\n  oldRevID = " + oldRevID + ", newRevID = " + newRevID);
//if(item == null)
//  System.out.println("item is NULL");
//if(rev == null)
//  System.out.println("rev is NULL");

      // do it
      workTheMagic();

      }
    // set ready status
    session.setReadyStatus();
    }


  public void workTheMagic()throws Exception
    {
logger.info("Inside RenameOperation::workTheMagic");

    if(newName.equals(oldName))
        nameFlag = 0;
      else
        nameFlag = 1;
      // if item context set some processing flags
      if(rev == null)
        {
        if (newID.equals(oldID))
          idFlag = 0;
        else
          idFlag = 1;
        revIDFlag = 0;
        }
      // else we have item rev context, set other flags
      else
        {
        if (newRevID.equals(oldRevID))
          revIDFlag = 0;
        else
          revIDFlag = 1;
        idFlag = 0;
        }
logger.info("idFlag = " + idFlag + ", revIDFlag = " + revIDFlag + ", nameFlag = " + nameFlag);

      if(rev == null)
        {
        // do the deep rename stuff for item
logger.info("Calling renameItem");
        renameItem();
        }
      else
        {
        // do the deep rename stuff for rev
logger.info("Calling renameRev");
        renameRev(rev);
        }

      }


  public boolean getSuccessFlag()
    {
    return successFlag;
    }
/*
  public void removePropertyChangeListener(PropertyChangeListener propertychangelistener)
    {
    propertySupport.removePropertyChangeListener(propertychangelistener);
    }*/

  public void renameItem()
    {
logger.info("  Inside renameItem");
    Registry registry = Registry.getRegistry(this);
    try
      {
      // modify item id if changed
      if(idFlag == 1)
        {
        int iret = modifyItemID(item);
        if(iret != 0)
          return;
        }
      AIFComponentContext children[] = item.getChildren();
      for(int i = 0; i < children.length; i++)
        {
        String compType = children[i].getComponent().getType();
        // gracefully skip over folders & pseudo folders (views)
        if((compType.equalsIgnoreCase("PseudoFolder")) || compType.equalsIgnoreCase("Folder"))
          continue;
        // process item revisions by renaming deeply
        if (children[i].getComponent() instanceof TCComponentItemRevision)
          {
logger.info("  children[" + i + "] is an Item Revision");
          renameRev((TCComponentItemRevision)children[i].getComponent());
          }
        // else rename simply
        else
          {
          // rename the child object
logger.info("  RENAMING item child " + compType + " named '" + children[i].getComponent().toString() + "'");
          renameObject((TCComponent)children[i].getComponent());
          }
        }
      // rename the item itself
logger.info("  RENAMING item " + item.getType() + " named '" + item.toString() + "'");
      renameObject((TCComponent)item);
      }
    catch(TCException tcException)
      {
      successFlag = false;
logger.info("Item rename exception loop");
      String s = tcException.getError();
      if (desktop != null)
        {
        MessageBox messagebox = new MessageBox(desktop, s, null,
                                    registry.getString("error.TITLE"), MessageBox.ERROR, true);
        messagebox.setVisible(true);
        }
      else
        {
        System.out.println(s);
        }
      }
    }

  public void renameRev(TCComponentItemRevision ir)
    {
logger.info("    Inside renameRev");
    try
      {
      // modify item id if changed
      if(revIDFlag == 1)
        {
        int iret = modifyRevID(ir);
        if(iret != 0)
          return;
        }
      AIFComponentContext children[] = ir.getChildren();
      for(int i = 0; i < children.length; i++)
        {
        String compType = children[i].getComponent().getType();
        // gracefully skip over folders & pseudo folders (views)
        if((compType.equalsIgnoreCase("PseudoFolder")) || compType.equalsIgnoreCase("Folder"))
          continue;
logger.info("    rev children[" + i + "].getComponent().getType() = " + compType);
//        if (children[i].getComponent() instanceof TCComponentBOMViewRevision)
logger.info("    children[" + i + "] is a BOM View Revision");
//        if (children[i].getComponent() instanceof TCComponentForm)
logger.info("    children[" + i + "] is a Form");
        // if we have a dataset, process deeply
        if (children[i].getComponent() instanceof TCComponentDataset)
          {
logger.info("    children[" + i + "] is a Dataset");
          renameDataset((TCComponentDataset)children[i].getComponent());
          }
        // else simply rename
        else
          {
          // rename the child object
logger.info("    RENAMING rev child " + compType + " named '" + children[i].getComponent().toString() + "'");
          renameObject((TCComponent)children[i].getComponent());
          }
        }
      //  rename the rev itself
logger.info("    RENAMING rev " + ir.getType() + " named '" + ir.toString() + "'");
      renameObject((TCComponent)ir);

      }
    catch(TCException tcException)
      {
      successFlag = false;
logger.info("Rev rename exception loop");
      String s = tcException.getError();
      if (desktop != null)
        {
        MessageBox messagebox = new MessageBox(desktop, s, null,
                                       registry.getString("error.TITLE"), MessageBox.ERROR, true);
        messagebox.setVisible(true);
        }
      else
        {
        System.out.println(s);
        }
      }
    }

  public void renameDataset(TCComponentDataset ds)
    {
logger.info("      Inside renameDataset");

    renameObject((TCComponent)ds);

    }

  public void renameObject(TCComponent imanComp)
    {
logger.info("Inside renameObject");
    TCPreferenceService prefService = null;
    String itemRevSep = null;
    String oldValue = null;
    String newValue = null;
    String roldID = null;
    String roldName = null;
    try
      {
logger.info("imanComp = '" + imanComp.toString() + "', type = '" +  imanComp.getType() + "', class = '" + imanComp.getClassType() + "'");
      prefService = session.getPreferenceService();
      itemRevSep = prefService.getString(com.teamcenter.rac.kernel.TCPreferenceService.TC_preference_all, "FLColumnCatIVFSeparatorPref");
logger.info("itemRevSep = " + itemRevSep);
      TCProperty prop = imanComp.getTCProperty("object_name");
      if (prop != null)
        {
        oldValue = prop.getStringValue();
logger.info("oldValue1 = '" + oldValue + "'");
        if (idFlag == 1)
          {
logger.info("newValue in if idFlag == 1");
logger.info("oldID = " + oldID);
          roldID = oldID;
          if(oldID.startsWith("["))
            {
logger.info("old ID starts with [");
            roldID = "\\" + oldID;
logger.info("Updated oldID = " + roldID);
            }
          newValue = oldValue.replaceFirst(roldID, newID);
          }
        else
          newValue = oldValue;
logger.info("oldValue = '" + oldValue + "', newValue = '" + newValue + "'");
        if ((nameFlag == 1))
          {
          // if this is an item or itemRev, just set name explicitly
          if((imanComp.getClassType().equals("ItemRevision")) || (imanComp.getClassType().equals("Item")))
            {
logger.info("newValue in if nameFlag == 1");
            newValue = newName;
            }
          else if((!(oldName.equals(oldID))) && (!(oldName.equals(newID))))
            {
            roldName = oldName;
            if(oldName.startsWith("["))
              {
              roldName = "\\" + oldName;
              }
            newValue = newValue.replaceFirst(roldName, newName);
            }
          }
logger.info("oldValue = '" + oldValue + "', newValue = '" + newValue + "'");
        if (revIDFlag == 1)
          {
logger.info("newValue in if revIDFlag == 1");
          if(newValue == null)
            newValue = oldValue;
          String oldTemp = null;
          String newTemp = null;
          oldTemp = itemRevSep + oldRevID;
          newTemp = itemRevSep + newRevID;
          newValue = newValue.replaceFirst(oldTemp, newTemp);
          oldTemp = "-" + oldRevID;
          newTemp = "-" + newRevID;
          newValue = newValue.replaceFirst(oldTemp, newTemp);
          oldTemp = "_" + oldRevID;
          newTemp = "_" + newRevID;
          newValue = newValue.replaceFirst(oldTemp, newTemp);
          }
logger.info("oldValue = '" + oldValue + "', newValue = '" + newValue + "'");
        if(!(newValue.equals(oldValue)))
          {
logger.info("NAME VALUE HAS CHANGED to '" + newValue + "'");
          prop.setStringValue(newValue);
        //  imanComp.lock();
        //  imanComp.save();
          imanComp.refresh();
          }


//else
logger.info("NAME VALUE NOT CHANGED");
        }
      }
    catch(TCException tcException)
      {
      successFlag = false;
      String s = tcException.getError();
      if (desktop != null)
        {
        MessageBox messagebox = new MessageBox(desktop, s, null,
                                       registry.getString("error.TITLE"), MessageBox.ERROR, true);
        messagebox.setVisible(true);
        }
      else
        {
        System.out.println(s);
        }
      }
    }

  public int modifyItemID(TCComponentItem imanItem)
    {
logger.info("Inside modifyItemID");
    try
      {
      TCProperty prop = imanItem.getTCProperty("item_id");
      if (prop != null)
        {
        prop.setStringValue(newID);
        imanItem.lock();
        imanItem.save();
        imanItem.refresh();
        imanItem.unlock();
        }
      return(0);
      }
    catch(TCException tcException)
      {
      successFlag = false;
logger.info("Inside modifyItemID exception loop");
      String s = tcException.getError();
      if (desktop != null)
        {
        MessageBox messagebox = new MessageBox(desktop, s, null,
                                       registry.getString("error.TITLE"), MessageBox.ERROR, true);
        messagebox.setVisible(true);
        }
      else
        {
        System.out.println(s);
        }
      return(1);
      }
    }

  public int modifyRevID(TCComponentItemRevision imanRev)
    {
logger.info("Inside modifyRevID");
    try
      {
      TCProperty prop = imanRev.getTCProperty("item_revision_id");
      if (prop != null)
        {
        prop.setStringValue(newRevID);
        imanRev.lock();
        imanRev.save();
        imanRev.refresh();
        imanRev.unlock();
        }
      return(0);
      }
    catch(TCException tcException)
      {
      successFlag = false;
logger.info("Inside modifyRevID exception loop");if(desktop == null)	logger.info("desktop is NULL!!!");
      String s = tcException.getError();
      if (desktop != null)
        {
        MessageBox messagebox = new MessageBox(desktop, s, null,
                                      registry.getString("error.TITLE"), MessageBox.ERROR, true);
        messagebox.setVisible(true);
        }
      else
        {
        System.out.println(s);
        }
      return(1);
      }
    }

  }
