package com.teamcenter.rac.ekons.rename.dialogs;

import java.util.ResourceBundle;

public class LocaleUIText {
	
	 public LocaleUIText()
	    {
	    }
	    
	    public static String getString(String s)
	    {
	        return RESOURCE_BUNDLE.getString(s);
	    }
	    
	    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
	            .getBundle("com.teamcenter.rac.ekons.rename.dialogs.dialogs_locale");

}
