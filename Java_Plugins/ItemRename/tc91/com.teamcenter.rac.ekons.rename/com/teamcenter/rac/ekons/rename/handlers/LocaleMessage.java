package com.teamcenter.rac.ekons.rename.handlers;

import java.util.ResourceBundle;

public class LocaleMessage {
	
	 public LocaleMessage()
	    {
	    }
	    
	    public static String getString(String s)
	    {
	        return RESOURCE_BUNDLE.getString(s);
	    }
	    
	    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
	            .getBundle("com.teamcenter.rac.ekons.rename.handlers.handlers_locale");

}
