/*=============================================================================

    Filename    :   Messages.java
    Module      :   com.teamcenter.terex.rename.menus.handlers
    Description :   This file is the Messages java file used for Externalize 
                    String constants used for various Terex Renaming operations 

================================================================================
                     Modification Log
================================================================================
Date            User                Company    Change Description
10-Jul-2011     Umesh Dwivedi       PRION      Initial Version

==============================================================================*/
package com.teamcenter.rac.ekons.rename.handlers;

import org.eclipse.osgi.util.NLS;

/**
 * @author udwivedi
 *
 */
public class Messages extends NLS 
{
	/**
	 * String BUNDLE_NAME
	 */
	private static final String BUNDLE_NAME = "com.teamcenter.terex.rename.menus.handlers.messages"; //$NON-NLS-1$
	
	/**
	 * String bundle
	 */
	public static final String bundle = "com.teamcenter.terex.rename.menus.handlers.messages";
	
	static 
	{
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	/**
	 * Messages
	 */
	private Messages() {
	}
	
	public static String RenameHandler_0;
	public static String RenameHandler_1;
	public static String RenameHandler_10;
	public static String RenameHandler_11;
	public static String RenameHandler_12;
	public static String RenameHandler_13;
	public static String RenameHandler_14;
	public static String RenameHandler_15;
	public static String RenameHandler_16;
	public static String RenameHandler_17;
	public static String RenameHandler_18;
	public static String RenameHandler_19;
	public static String RenameHandler_2;
	public static String RenameHandler_20;
	public static String RenameHandler_21;
	public static String RenameHandler_22;
	public static String RenameHandler_23;
	public static String RenameHandler_24;
	public static String RenameHandler_25;
	public static String RenameHandler_26;
	public static String RenameHandler_27;
	public static String RenameHandler_28;
	public static String RenameHandler_29;
	public static String RenameHandler_3;
	public static String RenameHandler_30;
	public static String RenameHandler_31;
	public static String RenameHandler_32;
	public static String RenameHandler_33;
	public static String RenameHandler_34;
	public static String RenameHandler_35;
	public static String RenameHandler_36;
	public static String RenameHandler_37;
	public static String RenameHandler_38;
	public static String RenameHandler_39;
	public static String RenameHandler_4;
	public static String RenameHandler_40;
	public static String RenameHandler_41;
	public static String RenameHandler_42;
	public static String RenameHandler_43;
	public static String RenameHandler_44;
	public static String RenameHandler_45;
	public static String RenameHandler_5;
	public static String RenameHandler_58;
	public static String RenameHandler_59;
	public static String RenameHandler_6;
	public static String RenameHandler_60;
	public static String RenameHandler_61;
	public static String RenameHandler_62;
	public static String RenameHandler_63;
	public static String RenameHandler_64;
	public static String RenameHandler_65;
	public static String RenameHandler_66;
	public static String RenameHandler_67;
	public static String RenameHandler_68;
	public static String RenameHandler_69;
	public static String RenameHandler_7;
	public static String RenameHandler_70;
	public static String RenameHandler_71;
	public static String RenameHandler_72;
	public static String RenameHandler_73;
	public static String RenameHandler_74;
	public static String RenameHandler_75;
	public static String RenameHandler_76;
	public static String RenameHandler_8;
	public static String RenameHandler_9;
}
