/*=============================================================================

    Filename    :   RenameOperationCAD.java
    Module      :   com.teamcenter.terex.rename.menus.dialogs
    Description :   This file is the Operation file used to perform various basic 
                    validations of Terex based custom Items,custom Item Revisions 
                    and their corresponding attributes used for the Renaming 
                    functionality at both the levels.

================================================================================
                     Modification Log
================================================================================
Date            User                Company    Change Description
10-Jul-2012     Umesh Dwivedi       PRION      Initial Version
30-Jul-2012     Umesh Dwivedi       PRION      Fixed the Issue: For regular users 
                                               the forms are suppressed (Default Child Properties)
19-Aug-2012     Umesh Dwivedi       PRION      Added Enhancement request features
24-Aug-2012     Umesh Dwivedi       PRION      Added specific Dataset Validation

==============================================================================*/

package com.teamcenter.terex.rename.menus.dialogs;

import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.JTable;

import org.apache.log4j.Logger;

import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentForm;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.Registry;
import com.teamcenter.terex.rename.menus.handlers.Messages;


/**
 * @author udwivedi
 *
 */
public class RenameOperationCAD extends AbstractAIFOperation
{
	/**
	 * Logger logger
	 */
	private static Logger logger = Logger.getLogger("com.teamcenter.terex.rename");

	/**
	 * String 	old_identnumber
	 */
	private String 	old_identnumber;

	/**
	 * String 	new_identnumber
	 */
	private String 	new_identnumber;

	/**
	 * String 	old_name
	 */
	private String 	old_name;

	/**
	 * String	new_name
	 */
	private String	new_name;

	/**
	 * String 	old_revID
	 */
	private String 	old_revID;

	/**
	 * String 	new_revID
	 */
	private String 	new_revID;

	/**
	 * String 	old_extension
	 */
	private String 	old_extension;

	/**
	 * String 	new_extension
	 */
	private String 	new_extension;

	/**
	 * String 	old_counter
	 */
	private String 	old_counter;

	/**
	 * String 	new_counter
	 */
	private String 	new_counter;

	/**
	 * String 	old_denoDe
	 */
	private String 	old_denoDe;

	/**
	 * String	new_denoDe
	 */
	private String	new_denoDe;

	/**
	 * String	new_denoDeAtItem
	 */
	private String	new_denoDeAtItem;

	/**
	 * String 	old_denoEn
	 */
	private String 	old_denoEn;

	/**
	 * String	new_denoEn
	 */
	private String	new_denoEn;

	/**
	 * String 	old_index
	 */
	private String 	old_index;

	/**
	 * String	new_index
	 */
	private String	new_index;

	/**
	 * String 	old_feaStage
	 */
	private String 	old_feaStage;

	/**
	 * String	new_feaStage
	 */
	private String	new_feaStage;

	/**
	 * String 	old_feaDev
	 */
	private String 	old_feaDev;

	/**
	 * String	new_feaDev
	 */
	private String	new_feaDev;	

	/**
	 * String tcClass
	 */
	private String tcClass = null;

	/**
	 * String feaItem
	 */
	private String feaItem = null;

	/**
	 * int idnFlag
	 */
	private int idnFlag;

	/**
	 * int revIDFlag
	 */
	private int revIDFlag;

	/**
	 * int nameFlag
	 */
	private int nameFlag;

	/**
	 * TCComponentItem item
	 */
	private TCComponentItem item;

	/**
	 * TCComponentItemRevision rev
	 */
	private TCComponentItemRevision rev;

	/**
	 * JTable jtable
	 */
	private JTable jtable;

	/**
	 * Vector<TCComponent> attachedObjects
	 */
	private Vector<TCComponent> attachedObjects = null;

	/**
	 * TCSession session
	 */
	private TCSession session;

	/**
	 * AIFDesktop desktop
	 */
	private AIFDesktop desktop;

	/**
	 * boolean successFlag
	 */
	private boolean successFlag;

	/**
	 * ResourceBundle resource
	 */
	private static ResourceBundle resource = ResourceBundle.getBundle( Messages.bundle  );

	/**
	 * String revisionID
	 */
	private static String revisionID = null;


	/**
	 * @param tcSession TCSession
	 * @param aifDesktop AIFDesktop
	 * @param s1 String
	 * @param s2 String
	 * @param s3 String
	 * @param s4 String
	 * @param s5 String
	 * @param s6 String
	 * @param s7 String
	 * @param s8 String
	 * @param s9 String
	 * @param s10 String
	 * @param s11 String
	 * @param s12 String
	 * @param s13 String
	 * @param s14 String
	 * @param s15 String
	 * @param s16 String
	 * @param s17 String
	 * @param s18 String
	 * @param s19 String
	 * @param s20 String
	 * @param tcItem TCComponentItem
	 * @param tcItemRev TCComponentItemRevision
	 * @param attachedObjs Vector<TCComponent>
	 */
	public RenameOperationCAD(TCSession tcSession,AIFDesktop aifDesktop,String s1,String s2,String s3,String s4,
			String s5,String s6,String s7,String s8,String s9,String s10,String s21,String s22,String s11,String s12,String s13,
			String s14,String s15,String s16,String s17,String s18,String s19,String s20,TCComponentItem tcItem,
			TCComponentItemRevision tcItemRev,Vector<TCComponent> attachedObjs)
	{
		logger.info("Inside RenameOperation");

		old_identnumber = null;
		new_identnumber = null;
		old_name 		= null;
		new_name 		= null;
		old_denoDe 		= null;
		new_denoDe 		= null;
		old_denoEn 		= null;
		new_denoEn 		= null;
		old_index 		= null;
		new_index 		= null;
		old_revID 		= null;
		new_revID 		= null;
		old_extension   = null;
		new_extension   = null;
		old_counter     = null;
		new_counter     = null;

		new_denoDeAtItem = null;

		old_feaStage   = null;
		new_feaStage   = null;
		old_feaDev     = null;
		new_feaDev     = null;

		idnFlag 		= 0;
		revIDFlag 		= 0;
		nameFlag 		= 0;
		item 			= null;
		rev 			= null;
		jtable 			= null;
		attachedObjects = new Vector<TCComponent>();
		session 		= null;
		desktop 		= null;
		successFlag 	= true;
		session 		= tcSession;


		if(aifDesktop == null)
			logger.info("aifDesktop is NULL at TOP!!!");
		desktop = aifDesktop;

		if(desktop == null)
			logger.info("desktop is NULL at TOP!!!");
		Registry.getRegistry(this);
		old_identnumber = s1;
		new_identnumber = s2;		
		old_extension   = s3;
		new_extension   = s4;
		old_counter     = s5;
		new_counter     = s6;
		old_name 		= s7;
		new_name 		= s8;
		old_revID 		= s9;
		new_revID 		= s10;
		old_denoDe      = s11;
		new_denoDe      = s12;
		old_denoEn      = s13;
		new_denoEn      = s14;
		old_index       = s15;
		new_index       = s16;
		old_feaStage    = s17;
		new_feaStage    = s18;
		old_feaDev      = s19;
		new_feaDev      = s20;
		new_denoDeAtItem = s22;
		item 			= tcItem;
		rev 			= tcItemRev;
		attachedObjects = attachedObjs;
	}


	/**
	 * @throws Exception
	 */
	public void execute()throws Exception
	{
		logger.info("Inside RenameOperation::execute");
		session.setStatus(resource.getString("renamingOperation"));

		workTheMagic();

		for(int i = 0; i < attachedObjects.size(); i++)
		{
			String rowString 		 = ((String)(jtable.getValueAt(i, 0)));
			String rowOldIdentNumber = null;
			String rowOldRevID 	     = null;
			String rowOldName 	     = null;
			String rowOldExt 		 = null;
			String rowOldExtCounter  = null;
			String rowOldDenoDE		 = null;
			String rowOldDenoEN      = null;
			String rowOldIndex 		 = null;
			String rowOldFeaStage    = null;
			String rowOldFeaDev      = null;

			String splitString[] 	= rowString.split("  <>  ");
			rowOldIdentNumber 	    = splitString[0];
			rowOldRevID 			= splitString[1];
			rowOldName 			    = splitString[2];
			rowOldExt               = splitString[3];
			rowOldExtCounter        = splitString[4];
			rowOldDenoDE            = splitString[5];
			rowOldDenoEN            = splitString[6];
			rowOldIndex             = splitString[7];
			rowOldFeaStage          = splitString[8];
			rowOldFeaDev            = splitString[9];

			// item context
			if(attachedObjects.elementAt(i) instanceof TCComponentItem)
			{
				item 			= (TCComponentItem)attachedObjects.elementAt(i);
				rev 			= null;
				old_identnumber = rowOldIdentNumber;
				new_identnumber = ((String)jtable.getValueAt(i, 1));
				old_extension   = rowOldExt;
				new_extension   = ((String)jtable.getValueAt(i, 2));
				old_counter     = rowOldExtCounter;
				new_counter     = ((String)jtable.getValueAt(i, 3));
				old_name		= rowOldName;
				new_name 		= ((String)jtable.getValueAt(i, 4));
				old_revID 		= "N/A";
				new_revID 		= "N/A";
				new_denoDeAtItem 		= ((String)jtable.getValueAt(i, 5));

			}

			// revision context
			else if(attachedObjects.elementAt(i) instanceof TCComponentItemRevision)
			{
				rev 			= (TCComponentItemRevision)attachedObjects.elementAt(i);				
				item 			= rev.getItem();
				old_identnumber = rowOldIdentNumber;
				new_identnumber = rowOldIdentNumber;				
				old_revID 		= rowOldRevID;
				new_revID 		= ((String)jtable.getValueAt(i, 1));
				old_name 		= rowOldName;
				new_name 		= ((String)jtable.getValueAt(i, 2));
				old_denoDe 		= rowOldDenoDE;
				new_denoDe 		= ((String)jtable.getValueAt(i, 3));
				old_denoEn 		= rowOldDenoEN;
				new_denoEn 		= ((String)jtable.getValueAt(i, 4));
				old_index 		= rowOldIndex;
				new_index 		= ((String)jtable.getValueAt(i, 5));

				if(rev!=null)
				{
					try 
					{
						tcClass = rev.getTCClass().getClassName();
					} 
					catch (TCException e) 
					{						
						e.printStackTrace();
					}
					if(tcClass!=null)
					{
						if(rev!=null && 
								tcClass.equals(resource.getString("T4FEARevision")) || 
								tcClass.equals(resource.getString("CAEModelRevision")) || 
								tcClass.equals(resource.getString("CAEAnalysisRevision")) || 
								tcClass.equals(resource.getString("CAEGeometryRevision")))
						{
							old_feaStage 	= rowOldFeaStage;
							new_feaStage 	= ((String)jtable.getValueAt(i, 6));
							old_feaDev 		= rowOldFeaDev;
							new_feaDev 		= ((String)jtable.getValueAt(i, 7));
						}}
				}
			}
			// do it
			workTheMagic();
		}
		// set ready status
		session.setReadyStatus(); 
	}

	/* (non-Javadoc)
	 * @see com.teamcenter.rac.aif.AbstractAIFOperation#executeOperation()
	 */
	public void executeOperation()throws Exception
	{
		logger.info("Inside RenameOperation::executeOperation");
		session.setStatus(resource.getString("renamingOperation"));

		workTheMagic();

		// top level context
		logger.info(" ");
		logger.info("oldIdentNumber = " + old_identnumber + ", newIdentNumber = " + new_identnumber + ", oldName = " 
				+ old_name + ", newName = " + new_name + "\n  oldRevID = " + old_revID + ", newRevID = " + new_revID);

		//if(item == null)
		logger.info("item is NULL");
		logger.info("rev is NULL");

		workTheMagic();	


		for(int i = 0; i < attachedObjects.size(); i++)
		{
			// setup stuff
			String rowString 		 = ((String)(jtable.getValueAt(i, 0)));
			String rowOldIdentNumber = null;
			String rowOldRevID	     = null;
			String rowOldName 	     = null;
			String rowOldExt 		 = null;
			String rowOldExtCounter  = null;
			String rowOldDenoDE		 = null;
			String rowOldDenoEN      = null;
			String rowOldIndex 		 = null;
			String rowOldFeaStage    = null;
			String rowOldFeaDev 	 = null;

			String splitString[] 	= rowString.split("  <>  ");
			rowOldIdentNumber 	    = splitString[0];
			rowOldRevID 			= splitString[1];
			rowOldName 			    = splitString[2];
			rowOldExt               = splitString[3];
			rowOldExtCounter        = splitString[4];
			rowOldDenoDE            = splitString[5];
			rowOldDenoEN            = splitString[6];
			rowOldIndex             = splitString[7];
			rowOldFeaStage          = splitString[8];
			rowOldFeaDev            = splitString[9];

			// item context
			if(attachedObjects.elementAt(i) instanceof TCComponentItem)
			{
				item 			= (TCComponentItem)attachedObjects.elementAt(i);
				rev 			= null;
				old_identnumber = rowOldIdentNumber;
				new_identnumber = ((String)jtable.getValueAt(i, 1));
				old_extension   = rowOldExt;
				new_extension   = ((String)jtable.getValueAt(i, 2));
				old_counter     = rowOldExtCounter;
				new_counter     = ((String)jtable.getValueAt(i, 3));
				old_name 		= rowOldName;
				new_name 		= ((String)jtable.getValueAt(i, 4));
				old_revID 		= "N/A";
				new_revID 		= "N/A";
				new_denoDeAtItem 		= ((String)jtable.getValueAt(i, 5));
			}

			// revision context
			else if(attachedObjects.elementAt(i) instanceof TCComponentItemRevision)
			{
				rev 			= (TCComponentItemRevision)attachedObjects.elementAt(i);
				item 			= rev.getItem();
				old_identnumber = rowOldIdentNumber;
				new_identnumber = rowOldIdentNumber;
				old_revID 		= rowOldRevID;
				new_revID 		= ((String)jtable.getValueAt(i, 1));
				old_name 		= rowOldName;
				new_name 		= ((String)jtable.getValueAt(i, 2));
				old_denoDe 		= rowOldDenoDE;
				new_denoDe 		= ((String)jtable.getValueAt(i, 3));
				old_denoEn 		= rowOldDenoEN;
				new_denoEn 		= ((String)jtable.getValueAt(i, 4));
				old_index 		= rowOldIndex;
				new_index 		= ((String)jtable.getValueAt(i, 5));


				if(rev!=null && 
						tcClass.equals(resource.getString("T4FEARevision")) || 
						tcClass.equals(resource.getString("CAEModelRevision")) || 
						tcClass.equals(resource.getString("CAEAnalysisRevision")) || 
						tcClass.equals(resource.getString("CAEGeometryRevision")))
				{
					old_feaStage 		= rowOldFeaStage;
					new_feaStage 		= ((String)jtable.getValueAt(i, 6));
					old_feaDev 		= rowOldFeaDev;
					new_feaDev 		= ((String)jtable.getValueAt(i, 7));
				}

			}
			logger.info("oldIdentNumber = " + old_identnumber + ", newID = " + new_identnumber + ", oldName = " + old_name 
					+ ", newName = " + new_name + "\n  oldRevID = " + old_revID + ", newRevID = " + new_revID);

			// do it
			workTheMagic();
			refreshRevChildrenOnClient(rev);
		}
		// set ready status
		session.setReadyStatus();
	}

	/**
	 * @throws Exception
	 */
	public void workTheMagic()throws Exception
	{
		logger.info("Inside RenameOperation::workTheMagic");

		if(new_name.equals(old_name) && rev == null)
		{
			modifyIdentNumber(item);
			modifyExt(item);
			modifyExtCounter(item);

			validateChildrenAtItemChange(item);
			nameFlag = 1;
		}				

		else if(new_extension.equals(old_extension) && rev == null)
		{
			modifyIdentNumber(item);
			modifyExtCounter(item);
			modifyItemName(item);

			validateChildrenAtItemChange(item);
			nameFlag = 1;
		}

		else if(new_counter.equals(old_counter) && rev == null)
		{
			modifyIdentNumber(item);
			modifyExt(item);
			modifyItemName(item);

			validateChildrenAtItemChange(item);
			nameFlag = 1;
		}

		else if(new_identnumber.equals(old_identnumber) && rev == null)
		{
			modifyExt(item);
			modifyExtCounter(item);
			modifyItemName(item);

			validateChildrenAtItemChange(item);
			nameFlag = 1;
		}
		else 
		{
			nameFlag = 1;
		}

		// if item context set some processing flags
		if(rev == null)
		{
			modifyIdentNumber(item);
			modifyExt(item);
			modifyExtCounter(item);
			modifyItemName(item);

			validateChildrenAtItemChange(item);
		}

		// else we have item revision context, set other flags
		else
		{
			if (new_revID.equals(old_revID))
				revIDFlag = 0;

			if (new_denoDe.equals(old_denoDe))
				revIDFlag = 0;

			if (new_denoEn.equals(old_denoEn))
				revIDFlag = 0;

			if (new_index.equals(old_index))
				revIDFlag = 0;

			if(tcClass!=null)
			{
				if(rev!=null && tcClass.equals(resource.getString("T4FEARevision")) || 
						tcClass.equals(resource.getString("CAEModelRevision")) || 
						tcClass.equals(resource.getString("CAEAnalysisRevision")) || 
						tcClass.equals(resource.getString("CAEGeometryRevision")))
				{
					if (new_feaStage.equals(old_feaStage))
						revIDFlag = 0;
					if (new_feaDev.equals(old_feaDev))
						revIDFlag = 0;
				}
			}

			else
				revIDFlag = 1;
			idnFlag = 0;
		}

		logger.info("idnFlag = " + idnFlag + ", revIDFlag = " + revIDFlag + ", nameFlag = " + nameFlag);

		if(rev == null)
		{
			// do the deep renaming for item
			logger.info("Calling renameItem");
			renameItem();

		}
		else
		{
			// do the deep renaming for revision
			logger.info("Calling renameRev");
			renameRev(rev);
		}
	}

	/**
	 * @return boolean
	 */
	public boolean getSuccessFlag()
	{
		return successFlag;
	}

	/**
	 * Method: renameItem 
	 */
	public void renameItem()
	{
		logger.info("  Inside renameItem");
		Registry.getRegistry(this);

		try
		{
			// modify item id if changed
			if(idnFlag == 1)
			{				
				int idnret = modifyIdentNumber(item);
				idnret = modifyExt(item);
				idnret = modifyExtCounter(item);
				idnret = modifyItemName(item);

				if(idnret != 0)

					return;
			}

			AIFComponentContext children[] = item.getChildren();
			for(int i = 0; i < children.length; i++)
			{
				String compType = children[i].getComponent().getType(); 


				TCProperty itemIdProperty = item.getTCProperty(resource.getString("ItemId"));
				itemIdProperty.setStringValue(new_identnumber + "-" + new_extension + new_counter);				


				String currIdStr = item.getTCProperty(resource.getString("CurrentId")).toString();
				TCProperty currIdProperty = item.getTCProperty(resource.getString("ObjectDesc"));
				currIdProperty.setStringValue(currIdStr);

				TCComponent[] components = null;
				components = item.getReferenceListProperty(resource.getString("IMANMasterForm"));

				for(int z = 0; z < components.length; z++)
				{
					TCProperty propObjNameForMF = components[z].getTCProperty(resource.getString("ObjectName"));				
					String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
					propObjNameForMF.setStringValue(identNoInRevContext);	
				}

				renameObject((TCComponent)children[i].getComponent());


				// gracefully skip over folders & pseudo folders (views)
				if((compType.equalsIgnoreCase(resource.getString("PseudoFolder"))) || compType.equalsIgnoreCase(resource.getString("Folder")))

					continue;

				// process item revisions by renaming deeply
				if (children[i].getComponent() instanceof TCComponentItemRevision)
				{
					logger.info("  children[" + i + "] is an Item Revision");
					renameRev((TCComponentItemRevision)children[i].getComponent());
				}

				if (children[i].getComponent() instanceof TCComponentForm)
				{
					logger.info("  children[" + i + "] is an Item Revision");
				}

				// else rename simply
				else
				{
					// rename the child object
					logger.info("  RENAMING item child " + compType + " named '" + children[i].getComponent().toString() + "'");
					renameObject((TCComponent)children[i].getComponent());
				}
			}

			// rename the item itself
			logger.info("  RENAMING item " + item.getType() + " named '" + item.toString() + "'");			


			feaItem = item.getTCClass().getClassName();

			if ((feaItem.equals("T4_FEA")) || feaItem.equals("CAEModel") || feaItem.equals("CAEAnalysis") || feaItem.equals("CAEGeometry"))
			{
				renameObject((TCComponent)item);

				if (new_denoDe.equals(old_denoDe))
					revIDFlag = 0;

				if (new_denoEn.equals(old_denoEn))
					revIDFlag = 0;

				if (new_index.equals(old_index))
					revIDFlag = 0;

				if(tcClass!=null)
				{
					if(rev!=null && tcClass.equals(resource.getString("T4FEARevision")) || 
							tcClass.equals(resource.getString("CAEModelRevision")) || 
							tcClass.equals(resource.getString("CAEAnalysisRevision")) || 
							tcClass.equals(resource.getString("CAEGeometryRevision")))
					{
						if (new_feaStage.equals(old_feaStage))
							revIDFlag = 0;
						if (new_feaDev.equals(old_feaDev))
							revIDFlag = 0;
					}
				}

				else
					revIDFlag = 1;


				for(int i = 0; i < children.length; i++)
				{
					String compType = children[i].getComponent().getType();

					// gracefully skip over folders & pseudo folders (views)
					if((compType.equalsIgnoreCase(resource.getString("PseudoFolder"))) || compType.equalsIgnoreCase(resource.getString("Folder")))

						continue;

					// process item revisions by renaming deeply
					if (children[i].getComponent() instanceof TCComponentItemRevision)
					{
						logger.info("  children[" + i + "] is an Item Revision");
						renameRev((TCComponentItemRevision)children[i].getComponent());
					}
					// else rename simply
					else
					{
						// rename the child object
						logger.info("  RENAMING item child " + compType + " named '" + children[i].getComponent().toString() + "'");
						renameObject((TCComponent)children[i].getComponent());
					}	
				}
			}
		}
		catch(TCException tcException)
		{
			successFlag = false;
			logger.info("Item rename exception loop");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);
				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
		}
	}

	/**
	 * Method: renameRev
	 * @param ir TCComponentItemRevision
	 */
	public void renameRev(TCComponentItemRevision ir)
	{
		logger.info("    Inside renameRev");

		try
		{
			// modify item id if changed
			if(revIDFlag == 1 || ir == null)
			{
				int iret = modifyRevDenoDE(ir);
				iret = modifyRevDenoEN(ir);
				iret = modifyRevIndex(ir);				
				iret = modifyRevFeaStage(ir);
				iret = modifyRevFeaDev(ir);					

				if(iret != 0)

					return;
			}

			AIFComponentContext children[] = ir.getChildren();
			for(int i = 0; i < children.length; i++)
			{
				String compType = children[i].getComponent().getType();

				TCComponentItem mainItem = ir.getItem();
				String objNameStr = mainItem.getTCProperty(resource.getString("ObjectName")).toString();
				TCProperty objNameProperty = ir.getTCProperty(resource.getString("ObjectName"));
				objNameProperty.setStringValue(objNameStr);

				String currentIDStr = mainItem.getTCProperty(resource.getString("CurrentId")).toString();
				TCProperty objDescProperty = ir.getTCProperty(resource.getString("ObjectDesc"));
				objDescProperty.setStringValue(currentIDStr);

				revisionID = ir.getTCProperty(resource.getString("ItemRevId")).toString();

				TCComponent[] components = null;
				components = ir.getReferenceListProperty(resource.getString("IMANMasterFormRev"));

				for(int z = 0; z < components.length; z++)
				{
					TCProperty propObjNameForRMF = components[z].getTCProperty(resource.getString("ObjectName"));				
					String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();					
					propObjNameForRMF.setStringValue(identNoInRevContext + "/" + revisionID);	
				}

				// gracefully skip over folders & pseudo folders (views)
				if((compType.equalsIgnoreCase(resource.getString("PseudoFolder"))) || compType.equalsIgnoreCase(resource.getString("Folder")))
					continue;

				logger.info("    rev children[" + i + "].getComponent().getType() = " + compType);

				//if (children[i].getComponent() instanceof TCComponentBOMViewRevision)
				logger.info("    children[" + i + "] is a BOM View Revision");

				//if (children[i].getComponent() instanceof TCComponentForm)
				logger.info("    children[" + i + "] is a Form");

				//if we have a dataset, process deeply
				if (children[i].getComponent() instanceof TCComponentDataset)
				{
					logger.info("    children[" + i + "] is a Dataset");
					renameDataset((TCComponentDataset)children[i].getComponent());
					renameDatasetOfImage((TCComponentDataset)children[i].getComponent());
				}

				//else simply rename
				else
				{
					// rename the child object
					logger.info("    RENAMING rev child " + compType + " named '" + children[i].getComponent().toString() + "'");
					renameObject((TCComponent)children[i].getComponent());
				}
			}

			//  rename the revision itself
			logger.info("    RENAMING rev " + ir.getType() + " named '" + ir.toString() + "'");
			renameObject((TCComponent)ir);
			refreshRevChildrenOnClient(ir);

		}
		catch(TCException tcException)
		{
			successFlag = false;
			logger.info("Rev rename exception loop");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, "Error in Renaming Revision",resource.getString("error.TITLE"), MessageBox.ERROR, true);
				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
		}
	}

	/**
	 * Method: renameDatasetOfImage
	 * @param ds TCComponentDataset
	 */
	public void renameDatasetOfImage(TCComponentDataset ds)
	{
		logger.info(" Inside renameDatasetOfImage");
		try {

			if (ds.getType().equals(resource.getString("IdeasDrawing")))
			{
				AIFComponentContext[] dsContext = ds.getChildren();
				for(int i = 0; i < dsContext.length; i++)
				{
					TCComponent[] components = null;
					components = ds.getReferenceListProperty(resource.getString("RelationIMANCapture"));

					for(int z = 0; z < components.length; z++)
					{				

						TCProperty propObjNameOfDataset = components[z].getTCProperty(resource.getString("ObjectName"));				
						String objNameInRevContext = item.getTCProperty(resource.getString("ObjectName")).toString();
						propObjNameOfDataset.setStringValue(objNameInRevContext);	
					}
				}
			}

		} 
		catch (TCException e) 
		{			
			e.printStackTrace();
		}
	}

	/**
	 * Method: renameDataset
	 * @param ds TCComponentDataset
	 */
	public void renameDataset(TCComponentDataset ds)
	{
		logger.info(" Inside renameDataset");
		try {
			if (ds.getType().equals(resource.getString("IdeasDrawing")))
			{
				String objNameInRevContext = item.getTCProperty(resource.getString("ObjectName")).toString();
				String newValue = null;
				newValue = objNameInRevContext;	
				TCProperty prop = ds.getTCProperty(resource.getString("ObjectName"));
				prop.setStringValue(newValue);
			}
			else if (ds.getType().equals(resource.getString("IdeasFem")))
			{
				String objNameInRevContext = item.getTCProperty(resource.getString("ObjectName")).toString();
				String newValue = null;
				newValue = objNameInRevContext;	
				TCProperty prop = ds.getTCProperty(resource.getString("ObjectName"));
				prop.setStringValue(newValue);
			}			
			else
			{
				String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
				String newValue = null;
				newValue = identNoInRevContext + "-" + revisionID;
				TCProperty prop = ds.getTCProperty(resource.getString("ObjectName"));
				prop.setStringValue(newValue);
			}
		} 
		catch (TCException e) 
		{			
			e.printStackTrace();
		}
	}



	/**
	 * Method: renameObject
	 * @param imanComp TCComponent
	 */
	public void renameObject(TCComponent imanComp)
	{
		logger.info("Inside renameObject");

		TCPreferenceService prefService = null;
		String itemRevSep 				= null;
		String oldValue 				= null;
		String newValue 				= null;
		String roldIdentNumber 			= null;
		String roldName 				= null;

		try
		{
			try 
			{
				feaItem = item.getTCClass().getClassName();
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			logger.info("imanComp = '" + imanComp.toString() + "', type = '" +  imanComp.getType() + "', class = '" + imanComp.getClassType() + "'");
			prefService = session.getPreferenceService();
			itemRevSep = prefService.getString(com.teamcenter.rac.kernel.TCPreferenceService.TC_preference_all, "FLColumnCatIVFSeparatorPref");
			logger.info("itemRevSep = " + itemRevSep);
			TCProperty prop = imanComp.getTCProperty(resource.getString("ObjectName"));
			if (prop != null)
			{
				oldValue = prop.getStringValue();
				logger.info("oldValue1 = '" + oldValue + "'");

				if (idnFlag == 1)
				{
					logger.info("newValue in if idnFlag == 1");
					logger.info("oldIdentNumber = " + old_identnumber);
					roldIdentNumber = old_identnumber;

					if(old_identnumber.startsWith("["))
					{
						logger.info("old ID starts with [");
						roldIdentNumber = "\\" + old_identnumber;
						logger.info("Updated oldIdentNumber = " + roldIdentNumber);
					}
					newValue = oldValue.replaceFirst(roldIdentNumber, new_identnumber);
				}

				else
					newValue = oldValue;
				logger.info("oldValue = '" + oldValue + "', newValue = '" + newValue + "'");

				if ((nameFlag == 1))
				{
					// if this is an item or itemRevision, just set name explicitly
					if((imanComp.getClassType().equals(resource.getString("ItemRevision"))) || 
							(imanComp.getClassType().equals(resource.getString("Item"))) || 
							(imanComp.getClassType().equals(resource.getString("ItemRevisionMaster"))) || 
							(imanComp.getClassType().equals(resource.getString("ItemMaster")))||
							(imanComp.getClassType().equals(resource.getString("IMANMasterForm"))) || 
							(imanComp.getClassType().equals(resource.getString("IMANMasterFormRev")) || 
									(imanComp.getClassType().equals(resource.getString("Form")))  || 
									(imanComp.getClassType().equals(resource.getString("Dataset"))) ) 

					)
					{
						logger.info("newValue in if nameFlag == 1");

						if ((imanComp instanceof TCComponentItemRevision))
						{
							String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
							newValue = identNoInRevContext;	
						}
						else if(imanComp.getType().endsWith(resource.getString("Master"))) 
						{
							String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
							newValue = identNoInRevContext;	
							prop.setStringValue(newValue);
						}						
					}

					else if(imanComp.getClassType().equals(resource.getString("PSBOMView"))) 
					{
						String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
						newValue = identNoInRevContext + "-" + resource.getString("View");	
						prop.setStringValue(newValue);
					}
					else if(imanComp.getClassType().equals(resource.getString("PSBOMViewRevision"))) 
					{
						String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
						newValue = identNoInRevContext + "/" + revisionID + "-" + resource.getString("View");	
						prop.setStringValue(newValue);
					}

					if (imanComp.getType().endsWith(resource.getString("RevisionMaster")))
					{					
						String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
						newValue = identNoInRevContext + "/" + revisionID;	
						prop.setStringValue(newValue);
					}
					if (imanComp.getType().endsWith(resource.getString("RevisionMasters")))
					{
						String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
						newValue = identNoInRevContext + "/" + revisionID;	
						prop.setStringValue(newValue);
					}
					else if((item != null) && ((item.equals(Messages.RenameHandler_16)) ||  
							(item.equals(Messages.RenameHandler_17))  ||  
							(item.equals(Messages.RenameHandler_19)) ||  
							(item.equals(Messages.RenameHandler_20)) ||  
							(item.equals(Messages.RenameHandler_21))  ||  
							(item.equals(Messages.RenameHandler_22))  ||   
							(item.equals(Messages.RenameHandler_23))  || 
							(item.equals(Messages.RenameHandler_75)) ))

						if((!(old_name.equals(old_identnumber))) && (!(old_name.equals(new_identnumber))))
						{
							roldName = old_name;

							if(old_name.startsWith("["))
							{
								roldName = "\\" + old_name;
							}
							newValue = newValue.replace(roldName, new_name);
						}	


						else if((feaItem != null) && ((feaItem.equals(Messages.RenameHandler_18)) || 
								feaItem.equals(Messages.RenameHandler_24) || 
								feaItem.equals(Messages.RenameHandler_25) || 
								feaItem.equals(Messages.RenameHandler_26)))
						{
							if((!(old_name.equals(old_identnumber))) && (!(old_name.equals(new_identnumber))))
							{
								roldName = old_name;
								if(old_name.startsWith("["))
								{
									roldName = "\\" + old_name;
								}
								newValue = newValue.replace(roldName, new_name);	
							}
						}
				}

				logger.info("oldValue = '" + oldValue + "', newValue = '" + newValue + "'");

				if (revIDFlag == 1)
				{
					logger.info("newValue in if revIDFlag == 1");

					if(newValue == null)

						newValue 		 = oldValue;
					String oldTemp = null;
					String newTemp = null;

					oldTemp 		 = itemRevSep + old_revID;
					newTemp 		 = itemRevSep + new_revID;
					newValue 		 = newValue.replaceFirst(oldTemp, newTemp);
					oldTemp 		 = "-" + old_revID;
					newTemp 		 = "-" + new_revID;
					newValue 		 = newValue.replaceFirst(oldTemp, newTemp);
					oldTemp 		 = "_" + old_revID;
					newTemp 		 = "_" + new_revID;

					newValue = newValue.replaceFirst(oldTemp, newTemp);
				}

				logger.info("oldValue = '" + oldValue + "', newValue = '" + newValue + "'");

				if(!(newValue.equals(oldValue)))
				{
					logger.info("NAME VALUE HAS CHANGED to '" + newValue + "'");
					prop.setStringValue(newValue);
					imanComp.refresh();
				}

				refreshRevChildrenOnClient(rev);
			}
			logger.info("NAME VALUE NOT CHANGED");
		}

		catch(TCException tcException)
		{

			successFlag = false;
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);
				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
		}
	}

	/**
	 * @param imanItem TCComponentItem
	 * @return int
	 */
	public int modifyItemID(TCComponentItem imanItem)
	{
		logger.info("Inside modifyItemID");

		try
		{
			TCProperty prop = imanItem.getTCProperty(resource.getString("ItemId"));

			if (prop != null)
			{
				prop.setStringValue(new_identnumber);

				imanItem.lock();

				imanItem.save();

				imanItem.refresh();

				imanItem.unlock();
			}
			return(0);
		}

		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyItemID exception loop");

			String s = tcException.getError();

			if (desktop != null)
			{

				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}

			else
			{
				System.out.println(s);
			}

			return(1);
		}
	}

	/**
	 * @param imanItemName TCComponentItem
	 * @return int
	 */
	public int modifyItemName(TCComponentItem imanItemName)
	{
		logger.info("Inside modifyItemName");

		try
		{
			TCProperty prop = imanItemName.getTCProperty(resource.getString("ObjectName"));

			if (prop != null)
			{
				prop.setStringValue(new_name);

				imanItemName.lock();

				imanItemName.save();

				imanItemName.refresh();

				imanItemName.unlock();
			}
			return(0);
		}

		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyItemName exception loop");

			String s = tcException.getError();

			if (desktop != null)
			{

				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}

			else
			{
				System.out.println(s);
			}

			return(1);
		}
	}


	/**
	 * @param imanIdentNumber TCComponentItem
	 * @return int
	 */
	public int modifyIdentNumber(TCComponentItem imanIdentNumber){


		logger.info("Inside modifyItemIdentNumber");

		try
		{
			TCProperty prop = imanIdentNumber.getTCProperty(resource.getString("T4IdentNumber"));

			if (prop != null)
			{
				prop.setStringValue(new_identnumber);

				imanIdentNumber.lock();

				imanIdentNumber.save();

				imanIdentNumber.refresh();

				imanIdentNumber.unlock();
			}
			return(0);
		}

		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyIdentNumber exception loop");

			String s = tcException.getError();

			if (desktop != null)
			{

				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}

			else
			{
				System.out.println(s);
			}

			return(1);
		}

	}

	/**
	 * @param imanRev TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevID(TCComponentItemRevision imanRev)
	{
		logger.info("Inside modifyRevID");

		try
		{
			TCProperty prop = imanRev.getTCProperty(resource.getString("ItemRevId"));

			if (prop != null)
			{
				prop.setStringValue(new_revID);

				imanRev.lock();

				imanRev.save();

				imanRev.refresh();

				imanRev.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyRevID exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanExt TCComponentItem
	 * @return int
	 */
	public int modifyExt(TCComponentItem imanExt){


		logger.info("Inside modifyExt");

		try
		{
			TCProperty prop = imanExt.getTCProperty(resource.getString("T4IdentExt"));

			if (prop != null)
			{
				prop.setStringValue(new_extension);

				imanExt.lock();

				imanExt.save();

				imanExt.refresh();

				imanExt.unlock();
			}
			return(0);
		}

		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside imanExt exception loop");

			String s = tcException.getError();

			if (desktop != null)
			{

				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}

			else
			{
				System.out.println(s);
			}

			return(1);
		}
	}

	/**
	 * @param imanExtCounter TCComponentItem
	 * @return int
	 */
	public int modifyExtCounter(TCComponentItem imanExtCounter){


		logger.info("Inside modifyExtCounter");

		try
		{
			TCProperty prop = imanExtCounter.getTCProperty(resource.getString("T4IdentExtCounter"));

			if (prop != null)
			{
				prop.setStringValue(new_counter);

				imanExtCounter.lock();

				imanExtCounter.save();

				imanExtCounter.refresh();

				imanExtCounter.unlock();
			}
			return(0);
		}

		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside imanExtCounter exception loop");

			String s = tcException.getError();

			if (desktop != null)
			{

				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}

			else
			{
				System.out.println(s);
			}

			return(1);
		}
	}

	/**
	 * @param imanRevDe TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevDenoDEAtItemContext(TCComponentItemRevision imanRevDe)
	{
		logger.info("Inside modifyRevDenoDEAtItemContext");

		try
		{
			TCProperty prop = imanRevDe.getTCProperty(resource.getString("T4DenotationDe"));

			if (prop != null)
			{
				prop.setStringValue(new_denoDeAtItem);

				imanRevDe.lock();

				imanRevDe.save();

				imanRevDe.refresh();

				imanRevDe.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside imanRevDe exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanRevDe TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevDenoDE(TCComponentItemRevision imanRevDe)
	{
		logger.info("Inside modifyRevDenoDE");

		try
		{
			TCProperty prop = imanRevDe.getTCProperty(resource.getString("T4DenotationDe"));

			if (prop != null)
			{
				prop.setStringValue(new_denoDe);

				imanRevDe.lock();

				imanRevDe.save();

				imanRevDe.refresh();

				imanRevDe.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside imanRevDe exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanRevEn TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevDenoEN(TCComponentItemRevision imanRevEn)
	{
		logger.info("Inside modifyRevDenoEN");

		try
		{
			TCProperty prop = imanRevEn.getTCProperty(resource.getString("T4DenotationEn"));

			if (prop != null)
			{
				prop.setStringValue(new_denoEn);

				imanRevEn.lock();

				imanRevEn.save();

				imanRevEn.refresh();

				imanRevEn.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyRevDenoEn exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanRevIndex TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevIndex(TCComponentItemRevision imanRevIndex)
	{
		logger.info("Inside modifyRevIndex");

		try
		{
			TCProperty prop = imanRevIndex.getTCProperty(resource.getString("T4Index"));

			if (prop != null)
			{
				prop.setStringValue(new_index);

				imanRevIndex.lock();

				imanRevIndex.save();

				imanRevIndex.refresh();

				imanRevIndex.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyRevIndex exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanRevStage TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevFeaStage(TCComponentItemRevision imanRevStage)
	{
		logger.info("Inside modifyRevFeaStage");

		try
		{
			TCProperty prop = imanRevStage.getTCProperty(resource.getString("T4FEAModelStage"));

			if (prop != null)
			{
				prop.setStringValue(new_feaStage);

				imanRevStage.lock();

				imanRevStage.save();

				imanRevStage.refresh();

				imanRevStage.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyRevFEAStage exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanRevDev TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevFeaDev(TCComponentItemRevision imanRevDev)
	{
		logger.info("Inside modifyRevFeaDev");

		try
		{
			TCProperty prop = imanRevDev.getTCProperty(resource.getString("T4FEAModelVersion"));

			if (prop != null)
			{
				prop.setStringValue(new_feaDev);

				imanRevDev.lock();

				imanRevDev.save();

				imanRevDev.refresh();

				imanRevDev.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyRevFEADev exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @Desc: This method will refresh all the children of the selected revision
	 * @param rev TCComponentItemRevision
	 */
	private void refreshRevChildrenOnClient(TCComponentItemRevision rev)
	{
		if(rev != null)
		{
			try
			{
				AIFComponentContext[] aifCompContext = rev.getChildren();

				for(int i = 0; i < aifCompContext.length; i++)
				{
					((TCComponent)(aifCompContext[i].getComponent())).refresh();
				}
			}
			catch(TCException e)
			{
				logger.debug(e.getMessage());
				MessageBox.post(e);
			}
		}
	}

	/**
	 * @Desc: This method is used to validate all the children at Item Context level
	 * @param item TCComponentItem
	 */
	private void validateChildrenAtItemChange(TCComponentItem item)
	{
		AIFComponentContext childrenContext[] = null;
		try {
			childrenContext = item.getChildren();
			for(int i = 0; i < childrenContext.length; i++)
			{
				if (childrenContext[i].getComponent() instanceof TCComponentItemRevision)
				{					
					try {
						String denoDeProperty = childrenContext[i].getComponent().getProperty(resource.getString("T4DenotationDe"));
						String denoEnProperty =childrenContext[i].getComponent().getProperty(resource.getString("T4DenotationEn"));
						String indexProperty =childrenContext[i].getComponent().getProperty(resource.getString("T4Index"));

						String feaMStageProperty =childrenContext[i].getComponent().getProperty(resource.getString("T4FEAModelStage"));
						String feaMVersionProperty =childrenContext[i].getComponent().getProperty(resource.getString("T4FEAModelVersion"));

						if((denoDeProperty != null) || (denoEnProperty != null) || (indexProperty != null))
						{
							System.out.println("Do not modify the rest of Terex based Custom Revision children attributes");
							modifyRevDenoDEAtItemContext((TCComponentItemRevision) childrenContext[i].getComponent());	

						}
						else 
						{
							modifyRevDenoDE((TCComponentItemRevision) childrenContext[i].getComponent());	
							modifyRevDenoEN((TCComponentItemRevision) childrenContext[i].getComponent());
							modifyRevIndex((TCComponentItemRevision) childrenContext[i].getComponent());
						}

						if((TCComponentItemRevision) childrenContext[i].getComponent()!=null)
						{
							tcClass = (String) childrenContext[i].getComponent().getClass().toString();
							if(tcClass!=null)
							{

								if((TCComponentItemRevision) childrenContext[i].getComponent()!=null && 
										tcClass.equals(resource.getString("T4FEARevision")) || 
										tcClass.equals(resource.getString("CAEModelRevision")) || 
										tcClass.equals(resource.getString("CAEAnalysisRevision")) || 
										tcClass.equals(resource.getString("CAEGeometryRevision")))
								{
									if((denoDeProperty != null) || 
											(denoEnProperty != null) || 
											(indexProperty != null)|| 
											(feaMStageProperty != null) || 
											(feaMVersionProperty != null))
									{
										System.out.println("Do not modify the rest of Terex based FEA Revision children attributes");

									}
									else 
									{
										modifyRevFeaStage((TCComponentItemRevision) childrenContext[i].getComponent());	
										modifyRevFeaDev((TCComponentItemRevision) childrenContext[i].getComponent());
									}
								}
							}
						}
					} catch (Exception e) {

						e.printStackTrace();
					}
				}				
			}
		} catch (TCException e) {

			e.printStackTrace();
		}
	}
}