/*=============================================================================

    Filename    :   RenameHandler.java
    Module      :   com.teamcenter.terex.rename.menus.handlers
    Description :   This file is the entry point i,e. Handler file used to 
                    perform various basic validations of Terex based custom 
                    Items,custom Item Revisions and their corresponding attributes 
                    used for the Renaming functionality at both the levels.

================================================================================
                     Modification Log
================================================================================
Date            User                Company    Change Description
10-Jul-2012     Umesh Dwivedi       PRION      Initial Version
30-Jul-2012     Umesh Dwivedi       PRION      Fixed the changes in order to restrict only 
                                               following release statuses:- T4_Pending,
                                               T4_Obsolete,T4_Approved,T4_3D_Frozen,
                                               TCM Released,T4_3D_Obsolete for rename
31-Jul-2012     G. Langenbacher     PRION      Added status check to acl check
24-Aug-2012     Umesh Dwivedi       PRION      Added Rename Menu Validation  
31-Aug-2012     G. Langenbacher     PRION      Deactivated plugin for T4_Standard_Part                                      

==============================================================================*/
package com.teamcenter.terex.rename.menus.handlers;


import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFUIApplication;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.aifrcp.Application;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentFolder;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentPseudoFolder;
import com.teamcenter.rac.kernel.TCComponentReleaseStatus;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.terex.rename.menus.dialogs.RenameDialog;
import com.teamcenter.terex.rename.menus.dialogs.RenameDialogCAD;



/**
 * @author udwivedi
 *
 */
public class RenameHandler extends AbstractHandler implements IHandler
{
	/**
	 * Logger logger
	 */
	private static Logger logger = Logger.getLogger(Messages.RenameHandler_1);

	/**
	 * TCSession session
	 */
	private TCSession session=null; 

	/**
	 * AIFDesktop currentDesktop
	 */
	private AIFDesktop currentDesktop = null;

	/**
	 * TCComponentItem  selectedPartItem
	 */
	private TCComponentItem  selectedPartItem = null;

	/**
	 * TCComponentItemRevision selectedPartRev
	 */
	private TCComponentItemRevision selectedPartRev = null;

	/**
	 * String caseSensitivity
	 */
	private String caseSensitivity = Messages.RenameHandler_9;

	/**
	 * boolean itemNameEditable
	 */
	boolean itemNameEditable=true;

	/**
	 * int theRevIdLength
	 */
	private int theRevIdLength;

	/**
	 * int theRevNameLength
	 */
	private int theRevNameLength;

	/**
	 * int thedenoDELength
	 */
	private int thedenoDELength;

	/**
	 * int thedenoENLength
	 */
	private int thedenoENLength;

	/**
	 * int theindexLength
	 */
	private int theindexLength;

	/**
	 * int thefeaStageLength
	 */
	private int thefeaStageLength;

	/**
	 * int thefeaDevLength
	 */
	private int thefeaDevLength;

	/**
	 * char invalidCharacters[]
	 */
	private char invalidCharacters[] = {',', '!', '$', '*', '@', '"', '?', ':', ';'};

	/**
	 * application AbstractAIFUIApplication
	 */
	private AbstractAIFUIApplication application;


	/**
	 * ResourceBundle resource
	 */
	private static ResourceBundle resource = ResourceBundle.getBundle( Messages.bundle );


	/**
	 * RenameHandler Constructor
	 */
	public RenameHandler(){}

	/** (non-Javadoc)
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event)  throws ExecutionException
	{

		logger.info(Messages.RenameHandler_2);

		application = AIFDesktop.getActiveDesktop().getCurrentApplication();
		String appId = application.getPerspectiveHeader().getCurrentAppDef().getApplicationId();
		if (appId.equals(resource.getString("navigator.perspective"))) 
		{
			HandlerUtil.getActiveShell(event);
			final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
			final ISelectionService service = window.getSelectionService();
			final ISelection iSelection = service.getSelection();

			if (iSelection instanceof TreeSelection) {
				final TreeSelection selection = (TreeSelection) iSelection;


				if ((selection != null) && (selection.size() > 1)) 
				{
					MessageBox.post(resource.getString(Messages.RenameHandler_3), resource.getString(Messages.RenameHandler_4), MessageBox.ERROR);
					return null;
				}
				else if ((selection == null) || (selection.size() < 1)) 
				{
					MessageBox.post(resource.getString(Messages.RenameHandler_58), resource.getString(Messages.RenameHandler_61), MessageBox.ERROR);
					return null;
				}
				else{
					final AIFComponentContext context = selection == null ? null : (AIFComponentContext) selection.getFirstElement();
					final TCComponent   tcComp = context == null ? null : (TCComponent) context.getComponent();

					currentDesktop = Application.aif_portal.getDesktopManager().getCurrentDesktop();
					session = tcComp.getSession();

					TCPreferenceService prefService = session.getPreferenceService();	

					// case sensitivity option
					String stringValue = prefService.getString(com.teamcenter.rac.kernel.TCPreferenceService.TC_preference_all, resource.getString(Messages.RenameHandler_5));
					if((stringValue != null) && (stringValue.length() > 0) && (stringValue.equalsIgnoreCase(Messages.RenameHandler_6)))
					{
						caseSensitivity = Messages.RenameHandler_7;  
					}
					else if((stringValue != null) && (stringValue.length() > 0) && (stringValue.equalsIgnoreCase(Messages.RenameHandler_8)))
					{
						caseSensitivity = Messages.RenameHandler_9;  
					} 

					// itemName editable option
					boolean boolValue = prefService.isFalse(com.teamcenter.rac.kernel.TCPreferenceService.TC_preference_all, resource.getString(Messages.RenameHandler_10));
					if(boolValue == true)
					{
						itemNameEditable = false;  
					}

					// invalid id characters option
					String  stringValues[] = prefService.getStringArray(com.teamcenter.rac.kernel.TCPreferenceService.TC_preference_all, resource.getString(Messages.RenameHandler_11));
					if((stringValues != null) && (stringValues.length > 0))
					{
						invalidCharacters = new char[stringValues.length];
						for(int i = 0; i < stringValues.length; i++)
						{
							invalidCharacters[i] = stringValues[i].charAt(0);
						}
					}

					selectedPartItem =null;
					selectedPartRev=null;

					if(tcComp instanceof TCComponentItem){
						selectedPartItem = ((TCComponentItem)tcComp); 
					}
					else if(tcComp instanceof TCComponentItemRevision)
					{
						selectedPartRev = ((TCComponentItemRevision)tcComp);  
					}

					else
					{
						MessageBox.post(resource.getString(Messages.RenameHandler_12), resource.getString(Messages.RenameHandler_13), MessageBox.ERROR);
						return null; 
					}

					//perform checking on object
					logger.info(Messages.RenameHandler_62);  
					int iFlag = checkStatusAclCheckout(tcComp);
					logger.info(Messages.RenameHandler_63 + iFlag); 
					if (iFlag == 1)
					{
						MessageBox messagebox1 = new MessageBox(currentDesktop, resource.getString(Messages.RenameHandler_64), resource.getString(Messages.RenameHandler_65), MessageBox.ERROR);
						messagebox1.setModal(true);
						messagebox1.setVisible(true);
						return null;
					}
					else if (iFlag == 2)
					{
						MessageBox messagebox1 = new MessageBox(currentDesktop, resource.getString(Messages.RenameHandler_66), resource.getString(Messages.RenameHandler_67), MessageBox.ERROR);
						messagebox1.setModal(true);
						messagebox1.setVisible(true);
						return null;
					}
					else if (iFlag == 3)
					{
						MessageBox messagebox1 = new MessageBox(currentDesktop, resource.getString(Messages.RenameHandler_68), resource.getString(Messages.RenameHandler_69), MessageBox.ERROR);
						messagebox1.setModal(true);
						messagebox1.setVisible(true);
						return null;
					}				

					if(tcComp instanceof TCComponentItem)
					{
						selectedPartItem = (TCComponentItem) (context == null ? null : (TCComponent) context.getComponent());

						try 
						{
							Display display = Display.getDefault();
							Shell shell = new Shell(display);

							String tcClass = selectedPartItem.getTCClass().getClassName();

							if(/*tcClass.equals(Messages.RenameHandler_14)
									||*/tcClass.equals(Messages.RenameHandler_15))
							{
								new RenameDialog(shell, SWT.BORDER, session, currentDesktop, selectedPartItem, caseSensitivity, itemNameEditable, invalidCharacters);	
							}

							else if(tcClass.equals(Messages.RenameHandler_16)
									||tcClass.equals(Messages.RenameHandler_17)
									||tcClass.equals(Messages.RenameHandler_18)
									||tcClass.equals(Messages.RenameHandler_19)
									||tcClass.equals(Messages.RenameHandler_20)
									||tcClass.equals(Messages.RenameHandler_21)
									||tcClass.equals(Messages.RenameHandler_22)
									||tcClass.equals(Messages.RenameHandler_23)
									||tcClass.equals(Messages.RenameHandler_24)
									||tcClass.equals(Messages.RenameHandler_25)
									||tcClass.equals(Messages.RenameHandler_26)
									||tcClass.equals(Messages.RenameHandler_75)
							)

							{
								new  RenameDialogCAD(shell, SWT.BORDER,  session, currentDesktop, selectedPartItem, caseSensitivity, itemNameEditable, invalidCharacters);
							}

							else
							{
								MessageBox.post(resource.getString(Messages.RenameHandler_27), resource.getString(Messages.RenameHandler_28), MessageBox.ERROR);
								return null;
							}

						} 
						catch (TCException e) {
							e.printStackTrace();
						}				

					}

					else if(tcComp instanceof TCComponentItemRevision)
					{
						selectedPartRev = (TCComponentItemRevision) (context == null ? null : (TCComponent) context.getComponent());

						try  
						{
							Display display = Display.getDefault();
							Shell shell = new Shell(display);						
							String tcClass = selectedPartRev.getTCClass().getClassName();

							if(/*tcClass.equals(Messages.RenameHandler_29)
									||*/tcClass.equals(Messages.RenameHandler_30)
									||tcClass.equals(Messages.RenameHandler_31)								
									||tcClass.equals(Messages.RenameHandler_32)
									||tcClass.equals(Messages.RenameHandler_33)
									||tcClass.equals(Messages.RenameHandler_34)
									||tcClass.equals(Messages.RenameHandler_35)
									||tcClass.equals(Messages.RenameHandler_36)
									||tcClass.equals(Messages.RenameHandler_37)
									||tcClass.equals(Messages.RenameHandler_38)
									||tcClass.equals(Messages.RenameHandler_39)
									||tcClass.equals(Messages.RenameHandler_40)
									||tcClass.equals(Messages.RenameHandler_41)
									||tcClass.equals(Messages.RenameHandler_45)
									||tcClass.equals(Messages.RenameHandler_76)

							)

							{
								new  RenameDialogCAD(shell, SWT.BORDER,  session, currentDesktop, selectedPartRev, caseSensitivity, itemNameEditable, 
										invalidCharacters,theRevIdLength,theRevNameLength, thedenoDELength, thedenoENLength, theindexLength, thefeaStageLength, thefeaDevLength);
							}								

							else
							{
								MessageBox.post(resource.getString(Messages.RenameHandler_42), resource.getString(Messages.RenameHandler_43), MessageBox.ERROR);
								return null;
							}
						}
						catch (TCException e) {
							e.printStackTrace();
						}	
					}
				}
			}
		}
		else
		{
			MessageBox.post(resource.getString("application.message"), resource.getString("information.TITLE"), MessageBox.INFORMATION);
			return null;
		}
		return null;
	}


	/**
	 * @param component TCComponent
	 * @return boolean
	 */
	private boolean checkMoreTerexReleaseStatus(TCComponent component)
	{
		String restrictedStatusStr = resource.getString("RESTRICTED_STATUS_TYPES"); 
		String[] restrictedStatuses = restrictedStatusStr.split(",");

		boolean found = false ;
		for(String restrictedStatus : restrictedStatuses )
		{
			TCComponent[] releaseStatuses = null;
			try
			{
				releaseStatuses = component.getReferenceListProperty(Messages.RenameHandler_59);
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			if( releaseStatuses != null &&
					releaseStatuses.length > 0 )
			{
				String latestStatus = null;
				try {
					latestStatus = releaseStatuses[releaseStatuses.length-1].getStringProperty(Messages.RenameHandler_60);
				} catch (TCException e) {								
					e.printStackTrace();
				}
				if(latestStatus.equals(restrictedStatus) )
				{
					found = true ;
					return found;
				}				
			}
		}
		return found;		
	} 

	/**
	 * @param component TCComponent
	 * @return int
	 */
	private int checkStatusAclCheckout(TCComponent component)

	{
		if((component == null) && (selectedPartItem == null))
		{
			component = selectedPartRev;
		}
		else if((component == null) && (selectedPartItem != null))
		{
			component = selectedPartItem;	
		}

		try

		{
			if ( (component instanceof TCComponent) &&
					(! (component instanceof TCComponentReleaseStatus)) &&
					(! (component instanceof TCComponentFolder)) &&
					(! (component instanceof TCComponentPseudoFolder)))

			{

				if(!(component.okToModify()))

				{
					logger.info("Component '" +  ((TCComponent)component).toString() + "', Type = '" + ((TCComponent)component).getType() + "' is NOT MODIFIABLE");
					return (1);
				}

				// check if the object has a release status
				if (checkMoreTerexReleaseStatus(component))
				{
					logger.info("Component '" +  ((TCComponent)component).toString() + "', Type = '" + ((TCComponent)component).getType() + "' is NOT MODIFIABLE BECAUSE OF RELEASE STATUS");
					return (2);
				}				

				AIFComponentContext compContext[] = component.getChildren();
				for (int i = 0; i < compContext.length; i++)
				{
					// Check for the Children only if the component is item or item revision
					if((component instanceof TCComponentItem ) || (component instanceof TCComponentItemRevision) )
					{
						int iret = checkStatusAclCheckout((TCComponent)compContext[i].getComponent());
						if (iret != 0)
						{
							return (iret);
						}
					}
				}
			}
		}
		catch(TCException tcException)
		{
			MessageBox messagebox = new MessageBox(currentDesktop, tcException.getError(), resource.getString(Messages.RenameHandler_73), MessageBox.ERROR);
			messagebox.setVisible(true);
		}
		return(0);
	}	
}