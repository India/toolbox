/*=============================================================================

    Filename    :   RenameOperation.java
    Module      :   com.teamcenter.terex.rename.menus.dialogs
    Description :   This file is the Operation file used to perform various basic 
                    validations of Terex based custom Items,custom Item Revisions 
                    and their corresponding attributes used for the Renaming 
                    functionality at both the levels.

================================================================================
                     Modification Log
================================================================================
Date            User                Company    Change Description
10-Jul-2012     Umesh Dwivedi       PRION      Initial Version
30-Jul-2012     Umesh Dwivedi       PRION      Fixed the Issue: For regular users 
                                               the forms are suppressed (Default Child Properties)
19-Aug-2012     Umesh Dwivedi       PRION      Added Enhancement request features
24-Aug-2012     Umesh Dwivedi       PRION      Added specific Dataset Validation
==============================================================================*/
package com.teamcenter.terex.rename.menus.dialogs;

import java.util.ResourceBundle;
import java.util.Vector;
import javax.swing.JTable;
import org.apache.log4j.Logger;
import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.aif.kernel.AIFComponentContext;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentDataset;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCPreferenceService;
import com.teamcenter.rac.kernel.TCProperty;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.terex.rename.menus.handlers.Messages;

/**
 * @author udwivedi
 *
 */
public class RenameOperation extends AbstractAIFOperation
{
	/**
	 * Logger logger
	 */
	private static Logger logger = Logger.getLogger("com.teamcenter.terex.rename");

	/**
	 * String oldID
	 */
	private String oldID;

	/**
	 * String newID
	 */
	private String newID;

	/**
	 * String oldName
	 */
	private String oldName;

	/**
	 * String newName
	 */
	private String newName;

	/**
	 * String oldRevID
	 */
	private String oldRevID;

	/**
	 * String newRevID
	 */
	private String newRevID;

	/**
	 * int idFlag
	 */
	private int idFlag;

	/**
	 * int revIDFlag
	 */
	private int revIDFlag;

	/**
	 * int nameFlag
	 */
	private int nameFlag;

	/**
	 * String new_denoDe
	 */
	private String new_denoDe;

	/**
	 * String	new_denoDeAtItem
	 */
	private String	new_denoDeAtItem;

	/**
	 * String new_denoEn
	 */
	private String new_denoEn;

	/**
	 * String new_index
	 */
	private String new_index;

	/**
	 * TCComponentItem item
	 */
	private TCComponentItem item;

	/**
	 * TCComponentItemRevision rev
	 */
	private TCComponentItemRevision rev;

	/**
	 * JTable jtable
	 */
	private JTable jtable;

	/**
	 * Vector<TCComponent> attachedObjects
	 */
	private Vector<TCComponent> attachedObjects = null;

	/**
	 * TCSession session
	 */
	private TCSession session;

	/**
	 * AIFDesktop desktop
	 */
	private AIFDesktop desktop;

	/**
	 * boolean successFlag
	 */
	private boolean successFlag;

	/**
	 * ResourceBundle resource
	 */
	private static ResourceBundle resource = ResourceBundle.getBundle( Messages.bundle );

	/**
	 * String revisionID
	 */
	private static String revisionID = null;


	/**
	 * @param tcSession TCSession
	 * @param aifDesktop AIFDesktop
	 * @param s1 String
	 * @param s2 String
	 * @param s3 String
	 * @param s4 String
	 * @param s5 String
	 * @param s6 String
	 * @param tcItem TCComponentItem
	 * @param tcItemRev TCComponentItemRevision
	 * @param attachedObjs Vector<TCComponent>
	 */
	public RenameOperation(TCSession tcSession, AIFDesktop aifDesktop, String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8,
			TCComponentItem tcItem, TCComponentItemRevision tcItemRev, Vector<TCComponent> attachedObjs)
	{
		logger.info("Inside RenameOperation");
		this.oldID = null;
		this.newID = null;
		this.oldName = null;
		this.newName = null;
		this.oldRevID = null;
		this.newRevID = null;
		this.new_denoDeAtItem = null;
		this.idFlag = 0;
		this.revIDFlag = 0;
		this.nameFlag = 0;
		this.item = null;
		this.rev = null;
		this.jtable = null;
		this.attachedObjects = new Vector<TCComponent>();
		this.session = null;
		this.desktop = null;
		this.successFlag = true;
		this.session = tcSession;
		if (aifDesktop == null)
			logger.info("aifDesktop is NULL at TOP!!!");
		this.desktop = aifDesktop;
		if (this.desktop == null)
			logger.info("desktop is NULL at TOP!!!");		
		this.oldID = s1;
		this.newID = s2;
		this.oldName = s3;
		this.newName = s4;
		this.oldRevID = s5;
		this.new_denoDeAtItem = s8;
		this.newRevID = s6;
		this.item = tcItem;
		this.rev = tcItemRev;
		this.attachedObjects = attachedObjs;
	}

	/**
	 * @throws Exception
	 */
	public void execute() throws Exception
	{
		logger.info("Inside RenameOperation::execute");
		this.session.setStatus(RenameOperation.resource.getString("renamingOperation"));

		workTheMagic();
		for (int i = 0; i < this.attachedObjects.size(); i++)
		{
			String rowString = (String)this.jtable.getValueAt(i, 0);

			String rowOldID = null;

			String rowOldRevID = null;

			String rowOldName = null;

			String[] splitString = rowString.split("  <>  ");

			rowOldID = splitString[0];

			rowOldRevID = splitString[1];

			rowOldName = splitString[2];

			if ((this.attachedObjects.elementAt(i) instanceof TCComponentItem))
			{
				this.item = ((TCComponentItem)this.attachedObjects.elementAt(i));

				this.rev = null;

				this.oldID = rowOldID;

				this.newID = ((String)this.jtable.getValueAt(i, 1));

				this.oldName = rowOldName;

				this.newName = ((String)this.jtable.getValueAt(i, 2));

				this.oldRevID = "N/A";

				this.newRevID = "N/A";
				this.new_denoDeAtItem = ((String)this.jtable.getValueAt(i, 3));
			}
			else if ((this.attachedObjects.elementAt(i) instanceof TCComponentItemRevision))
			{
				this.rev = ((TCComponentItemRevision)this.attachedObjects.elementAt(i));

				this.item = this.rev.getItem();

				this.oldID = rowOldID;

				this.newID = rowOldID;

				this.oldName = rowOldName;

				this.newName = ((String)this.jtable.getValueAt(i, 2));

				this.oldRevID = rowOldRevID;

				this.newRevID = ((String)this.jtable.getValueAt(i, 1));
			}

			workTheMagic();
		}

		this.session.setReadyStatus();
	}

	/* (non-Javadoc)
	 * @see com.teamcenter.rac.aif.AbstractAIFOperation#executeOperation()
	 */
	public void executeOperation()
	throws Exception
	{
		logger.info("Inside RenameOperation::executeOperation");

		this.session.setStatus(RenameOperation.resource.getString("renamingOperation"));

		workTheMagic();

		logger.info(" ");

		logger.info("oldID = " + this.oldID + ", newID = " + this.newID + ", oldName = " + this.oldName + ", newName = " + this.newName + "\n  oldRevID = " + this.oldRevID + ", newRevID = " + this.newRevID);

		logger.info("item is NULL");

		logger.info("rev is NULL");

		workTheMagic();

		logger.info(" ");

		logger.info("JTable rowCount = " + this.jtable.getRowCount() + ", columnCount = " + this.jtable.getColumnCount());

		logger.info(" ");

		logger.info("Found " + this.attachedObjects.size() + " Related Objects");

		for (int i = 0; i < this.attachedObjects.size(); i++)
		{
			String rowString = (String)this.jtable.getValueAt(i, 0);

			String rowOldID = null;

			String rowOldRevID = null;

			String rowOldName = null;

			String[] splitString = rowString.split("  <>  ");

			rowOldID = splitString[0];

			rowOldRevID = splitString[1];

			rowOldName = splitString[2];

			if ((this.attachedObjects.elementAt(i) instanceof TCComponentItem))
			{
				this.item = ((TCComponentItem)this.attachedObjects.elementAt(i));

				this.rev = null;

				this.oldID = rowOldID;

				this.newID = ((String)this.jtable.getValueAt(i, 1));

				this.oldName = rowOldName;

				this.newName = ((String)this.jtable.getValueAt(i, 2));

				this.oldRevID = "N/A";

				this.newRevID = "N/A";

				this.new_denoDeAtItem = ((String)this.jtable.getValueAt(i, 3));
			}
			else if ((this.attachedObjects.elementAt(i) instanceof TCComponentItemRevision))
			{
				this.rev = ((TCComponentItemRevision)this.attachedObjects.elementAt(i));

				this.item = this.rev.getItem();

				this.oldID = rowOldID;

				this.newID = rowOldID;

				this.oldName = rowOldName;

				this.newName = ((String)this.jtable.getValueAt(i, 2));

				this.oldRevID = rowOldRevID;

				this.newRevID = ((String)this.jtable.getValueAt(i, 1));
			}

			logger.info("oldID = " + this.oldID + ", newID = " + this.newID + ", oldName = " + this.oldName + ", newName = " + this.newName + "\n  oldRevID = " + this.oldRevID + ", newRevID = " + this.newRevID);

			workTheMagic();
		}

		this.session.setReadyStatus();
	}

	/**
	 * @throws Exception
	 */
	public void workTheMagic()
	throws Exception
	{
		logger.info("Inside RenameOperation::workTheMagic");

		if(newName.equals(oldName) && rev == null)
		{
			modifyItemID(item);
			modifyExt(item);
			modifyExtCounter(item);
			item.refresh();		

			validateChildrenAtItemChange(item);
			nameFlag = 1;
		}

		else if(newID.equals(oldID) && rev == null)
		{
			modifyItemName(item);
			modifyExt(item);
			modifyExtCounter(item);
			item.refresh();

			validateChildrenAtItemChange(item);
			nameFlag = 1;
		}
		else
		{
			this.nameFlag = 1;
		}

		if (this.rev == null)
		{
			modifyItemID(item);
			modifyItemName(item);
			modifyExt(item);
			modifyExtCounter(item);
			item.refresh();

			validateChildrenAtItemChange(item);
		}
		else
		{
			if (this.newRevID.equals(this.oldRevID))
			{
				this.revIDFlag = 0;
			}
			else
			{
				this.revIDFlag = 1;
			}
			this.idFlag = 0;
		}

		logger.info("idFlag = " + this.idFlag + ", revIDFlag = " + this.revIDFlag + ", nameFlag = " + this.nameFlag);

		if (this.rev == null)
		{
			logger.info("Calling renameItem");

			renameItem();
		}
		else
		{
			logger.info("Calling renameRev");

			renameRev(this.rev);
		}
	}

	/**
	 * @return boolean
	 */
	public boolean getSuccessFlag()
	{
		return this.successFlag;
	}

	/**
	 * Method: renameItem
	 */
	public void renameItem()
	{
		logger.info("  Inside renameItem");
		try
		{
			if (this.idFlag == 1)
			{
				int iret = modifyItemID(this.item);

				if (iret != 0)
				{
					return;
				}
			}

			AIFComponentContext[] children = this.item.getChildren();

			for (int i = 0; i < children.length; i++)
			{
				String compType = children[i].getComponent().getType();

				TCProperty itemIdProperty = item.getTCProperty(resource.getString("ItemId"));

				itemIdProperty.setStringValue(newID);				


				String currIdStr = item.getTCProperty(resource.getString("CurrentId")).toString();

				TCProperty objDescProperty = item.getTCProperty(resource.getString("ObjectDesc"));

				objDescProperty.setStringValue(currIdStr);

				TCComponent[] components = null;
				components = item.getReferenceListProperty(resource.getString("IMANMasterForm"));

				for(int z = 0; z < components.length; z++)
				{
					TCProperty propObjNameForMF = components[z].getTCProperty(resource.getString("ObjectName"));				
					String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
					propObjNameForMF.setStringValue(identNoInRevContext);	
				}

				if ((compType.equalsIgnoreCase(resource.getString("PseudoFolder"))) || (compType.equalsIgnoreCase(resource.getString("Folder"))))
				{
					continue;
				}

				if ((children[i].getComponent() instanceof TCComponentItemRevision))
				{
					logger.info("  children[" + i + "] is an Item Revision");

					renameRev((TCComponentItemRevision)children[i].getComponent());
				}
				else
				{
					logger.info("  RENAMING item child " + compType + " named '" + children[i].getComponent().toString() + "'");

					renameObject((TCComponent)children[i].getComponent());
				}

			}

			logger.info("  RENAMING item " + this.item.getType() + " named '" + this.item.toString() + "'");

			renameObject(this.item);
		}
		catch (TCException tcException)
		{
			this.successFlag = false;

			logger.info("Item rename exception loop");

			String s = tcException.getError();

			if (this.desktop != null)
			{
				MessageBox messagebox = new MessageBox(this.desktop, s, null, 
						resource.getString("error.TITLE"), 1, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
		}
	}

	/**
	 * Method: renameRev
	 * @param ir TCComponentItemRevision
	 */
	public void renameRev(TCComponentItemRevision ir)
	{
		logger.info("    Inside renameRev");
		try
		{
			if (this.revIDFlag == 1)
			{
				int iret = modifyRevID(ir);

				if (iret != 0)
				{
					return;
				}
			}

			AIFComponentContext[] children = ir.getChildren();

			for (int i = 0; i < children.length; i++)
			{
				String compType = children[i].getComponent().getType();

				TCComponentItem mainItem = ir.getItem();		

				String objectNameStr = mainItem.getTCProperty(resource.getString("ObjectName")).toString();

				TCProperty objectNameProperty = ir.getTCProperty(resource.getString("ObjectName"));

				objectNameProperty.setStringValue(objectNameStr);


				String currentIDStr = mainItem.getTCProperty(resource.getString("CurrentId")).toString();

				TCProperty objectDescriptionProperty = ir.getTCProperty(resource.getString("ObjectDesc"));

				objectDescriptionProperty.setStringValue(currentIDStr);


				revisionID = ir.getTCProperty(resource.getString("ItemRevId")).toString();

				TCComponent[] components = null;
				components = ir.getReferenceListProperty(resource.getString("IMANMasterFormRev"));

				for(int z = 0; z < components.length; z++)
				{
					TCProperty propObjNameForRMF = components[z].getTCProperty(resource.getString("ObjectName"));				
					String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();					
					propObjNameForRMF.setStringValue(identNoInRevContext + "/" + revisionID);	
				}


				if ((compType.equalsIgnoreCase(resource.getString("PseudoFolder"))) || (compType.equalsIgnoreCase(resource.getString("Folder"))))
				{
					continue;
				}
				logger.info("    rev children[" + i + "].getComponent().getType() = " + compType);

				logger.info("    children[" + i + "] is a BOM View Revision");

				logger.info("    children[" + i + "] is a Form");

				if ((children[i].getComponent() instanceof TCComponentDataset))
				{
					logger.info("    children[" + i + "] is a Dataset");

					renameDataset((TCComponentDataset)children[i].getComponent());
				}
				else
				{
					logger.info("    RENAMING rev child " + compType + " named '" + children[i].getComponent().toString() + "'");

					renameObject((TCComponent)children[i].getComponent());
				}
			}
			logger.info("    RENAMING rev " + ir.getType() + " named '" + ir.toString() + "'");

			renameObject(ir);
			refreshRevChildrenOnClient(ir);
		}
		catch (TCException tcException)
		{
			this.successFlag = false;

			logger.info("Rev rename exception loop");

			String s = tcException.getError();

			if (this.desktop != null)
			{
				MessageBox messagebox = new MessageBox(this.desktop, s, null, 
						RenameOperation.resource.getString("error.TITLE"), 1, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
		}
	}

	/**
	 * Method: renameDataset
	 * @param ds TCComponentDataset
	 */
	public void renameDataset(TCComponentDataset ds)
	{
		logger.info(" Inside renameDataset");		
		renameObject((TCComponent)ds);
	}

	/**
	 * Method: renameObject
	 * @param imanComp TCComponent
	 */
	public void renameObject(TCComponent imanComp)
	{
		logger.info("Inside renameObject");

		TCPreferenceService prefService = null;

		String itemRevSep = null;

		String oldValue = null;

		String newValue = null;

		String roldID = null;

		String roldName = null;
		try
		{
			logger.info("imanComp = '" + imanComp.toString() + "', type = '" + imanComp.getType() + "', class = '" + imanComp.getClassType() + "'");

			prefService = this.session.getPreferenceService();

			itemRevSep = prefService.getString(0, "FLColumnCatIVFSeparatorPref");

			logger.info("itemRevSep = " + itemRevSep);

			TCProperty prop = imanComp.getTCProperty(resource.getString("ObjectName"));

			if (prop != null)
			{
				oldValue = prop.getStringValue();

				logger.info("oldValue1 = '" + oldValue + "'");

				if (this.idFlag == 1)
				{
					logger.info("newValue in if idFlag == 1");

					logger.info("oldID = " + this.oldID);

					roldID = this.oldID;

					if (this.oldID.startsWith("["))
					{
						logger.info("old ID starts with [");

						roldID = "\\" + this.oldID;

						logger.info("Updated oldID = " + roldID);
					}

					newValue = oldValue.replaceFirst(roldID, this.newID);
				}
				else
				{
					newValue = oldValue;
				}
				logger.info("oldValue = '" + oldValue + "', newValue = '" + newValue + "'");

				if (this.nameFlag == 1)
				{
					if((imanComp.getClassType().equals(resource.getString("ItemRevision"))) || (imanComp.getClassType().equals(resource.getString("Item"))) || 
							(imanComp.getClassType().equals(resource.getString("ItemRevisionMaster"))) || (imanComp.getClassType().equals(resource.getString("ItemMaster")))||
							(imanComp.getClassType().equals(resource.getString("IMANMasterForm"))) || (imanComp.getClassType().equals(resource.getString("IMANMasterFormRev")) 
									|| (imanComp.getClassType().equals(resource.getString("Form")))  || (imanComp.getClassType().equals(resource.getString("Dataset"))) ) 

					)
					{
						logger.info("newValue in if nameFlag == 1");

						if ((imanComp instanceof TCComponentItemRevision))
						{
							String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();

							newValue = identNoInRevContext;	
						}
						else if(imanComp.getClassType().equals(resource.getString("Dataset"))) 
						{
							String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();

							newValue = identNoInRevContext + "-" + revisionID;	
							prop.setStringValue(newValue);
						}

						else if(imanComp.getType().endsWith(resource.getString("Master"))) 
						{
							String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();

							newValue = identNoInRevContext;	
							prop.setStringValue(newValue);
						}				
					}

					else if(imanComp.getClassType().equals(resource.getString("PSBOMView"))) 
					{
						String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();

						newValue = identNoInRevContext + "-" + resource.getString("View");	
						prop.setStringValue(newValue);
					}
					else if(imanComp.getClassType().equals(resource.getString("PSBOMViewRevision"))) 
					{
						String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();

						newValue = identNoInRevContext + "/" + revisionID + "-" + resource.getString("View");	
						prop.setStringValue(newValue);
					}

					if (imanComp.getType().endsWith(resource.getString("RevisionMaster")))
					{					
						String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
						newValue = identNoInRevContext + "/" + revisionID;	
						prop.setStringValue(newValue);
					}
					if (imanComp.getType().endsWith(resource.getString("RevisionMasters")))
					{
						String identNoInRevContext = item.getTCProperty(resource.getString("ItemId")).toString();
						newValue = identNoInRevContext + "/" + revisionID;	
						prop.setStringValue(newValue);
					}
					else if((item != null) && ((item.equals("T4_Article")) || (item.equals("T4_Standard_Part")) ))
					{
						if ((!this.oldName.equals(this.oldID)) && (!this.oldName.equals(this.newID)))
						{
							roldName = this.oldName;

							if (this.oldName.startsWith("["))
							{
								roldName = "\\" + this.oldName;
							}

							newValue = newValue.replaceFirst(roldName, this.newName);
						}
					}

				}

				logger.info("oldValue = '" + oldValue + "', newValue = '" + newValue + "'");

				if (this.revIDFlag == 1)
				{
					logger.info("newValue in if revIDFlag == 1");

					if (newValue == null)
					{
						newValue = oldValue;
					}
					String oldTemp = null;

					String newTemp = null;

					oldTemp = itemRevSep + this.oldRevID;

					newTemp = itemRevSep + this.newRevID;

					newValue = newValue.replaceFirst(oldTemp, newTemp);

					oldTemp = "-" + this.oldRevID;

					newTemp = "-" + this.newRevID;

					newValue = newValue.replaceFirst(oldTemp, newTemp);

					oldTemp = "_" + this.oldRevID;

					newTemp = "_" + this.newRevID;

					newValue = newValue.replaceFirst(oldTemp, newTemp);
				}

				logger.info("oldValue = '" + oldValue + "', newValue = '" + newValue + "'");

				if (!newValue.equals(oldValue))
				{
					logger.info("NAME VALUE HAS CHANGED to '" + newValue + "'");

					prop.setStringValue(newValue);

					imanComp.refresh();
				}

				logger.info("NAME VALUE NOT CHANGED");
			}

		}
		catch (TCException tcException)
		{
			this.successFlag = false;

			String s = tcException.getError();

			if (this.desktop != null)
			{
				MessageBox messagebox = new MessageBox(this.desktop, s, null, 
						RenameOperation.resource.getString("error.TITLE"), 1, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
		}
	}

	/**
	 * @param imanItem TCComponentItem
	 * @return int
	 */
	public int modifyItemID(TCComponentItem imanItem)
	{
		logger.info("Inside modifyItemID");
		try
		{
			TCProperty prop = imanItem.getTCProperty(resource.getString("T4IdentNumber"));

			if (prop != null)
			{
				prop.setStringValue(this.newID);

				imanItem.lock();

				imanItem.save();

				imanItem.refresh();

				imanItem.unlock();
			}

			return 0;
		}
		catch (TCException tcException)
		{
			this.successFlag = false;

			logger.info("Inside modifyItemID exception loop");

			String s = tcException.getError();

			if (this.desktop != null)
			{
				MessageBox messagebox = new MessageBox(this.desktop, s, null, 
						RenameOperation.resource.getString("error.TITLE"), 1, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
		}

		return 1;
	}

	/**
	 * @param imanRev TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevID(TCComponentItemRevision imanRev)
	{
		logger.info("Inside modifyRevID");
		try
		{
			TCProperty prop = imanRev.getTCProperty(resource.getString("ItemRevId"));

			if (prop != null)
			{
				prop.setStringValue(this.newRevID);

				imanRev.lock();

				imanRev.save();

				imanRev.refresh();

				imanRev.unlock();
			}

			return 0;
		}
		catch (TCException tcException)
		{
			this.successFlag = false;

			logger.info("Inside modifyRevID exception loop");
			if (this.desktop == null) {
				logger.info("desktop is NULL!!!");
			}
			String s = tcException.getError();

			if (this.desktop != null)
			{
				MessageBox messagebox = new MessageBox(this.desktop, s, null, 
						RenameOperation.resource.getString("error.TITLE"), 1, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
		}

		return 1;
	}


	/**
	 * @param rev TCComponentItemRevision
	 * @desc: This method will refresh all the children of the revision
	 */
	private void refreshRevChildrenOnClient(TCComponentItemRevision rev)
	{
		if(rev != null)
		{
			try
			{
				AIFComponentContext[] aifCompContext = rev.getChildren();

				for(int i = 0; i < aifCompContext.length; i++)
				{
					((TCComponent)(aifCompContext[i].getComponent())).refresh();
				}
			}
			catch(TCException e)
			{
				logger.debug(e.getMessage());
				MessageBox.post(e);
			}
		}
	}

	/**
	 * @param imanItemName TCComponentItem
	 * @return int
	 */
	public int modifyItemName(TCComponentItem imanItemName)
	{
		logger.info("Inside modifyItemName");

		try
		{
			TCProperty prop = imanItemName.getTCProperty(resource.getString("ObjectName"));

			if (prop != null)
			{
				prop.setStringValue(newName);

				imanItemName.lock();

				imanItemName.save();

				imanItemName.refresh();

				imanItemName.unlock();
			}
			return(0);
		}

		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyItemName exception loop");

			String s = tcException.getError();

			if (desktop != null)
			{

				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}

			else
			{
				System.out.println(s);
			}

			return(1);
		}
	}

	/**
	 * @param imanRevDe TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevDenoDE(TCComponentItemRevision imanRevDe)
	{
		logger.info("Inside modifyRevDenoDE");

		try
		{
			TCProperty prop = imanRevDe.getTCProperty(resource.getString("T4DenotationDe"));

			if (prop != null)
			{
				prop.setStringValue(new_denoDe);

				imanRevDe.lock();

				imanRevDe.save();

				imanRevDe.refresh();

				imanRevDe.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside imanRevDe exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanRevEn TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevDenoEN(TCComponentItemRevision imanRevEn)
	{
		logger.info("Inside modifyRevDenoEN");

		try
		{
			TCProperty prop = imanRevEn.getTCProperty(resource.getString("T4DenotationEn"));

			if (prop != null)
			{
				prop.setStringValue(new_denoEn);

				imanRevEn.lock();

				imanRevEn.save();

				imanRevEn.refresh();

				imanRevEn.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyRevDenoEn exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanRevIndex TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevIndex(TCComponentItemRevision imanRevIndex)
	{
		logger.info("Inside modifyRevIndex");

		try
		{
			TCProperty prop = imanRevIndex.getTCProperty(resource.getString("T4Index"));

			if (prop != null)
			{
				prop.setStringValue(new_index);

				imanRevIndex.lock();

				imanRevIndex.save();

				imanRevIndex.refresh();

				imanRevIndex.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside modifyRevIndex exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanRevDe TCComponentItemRevision
	 * @return int
	 */
	public int modifyRevDenoDEAtItemContext(TCComponentItemRevision imanRevDe)
	{
		logger.info("Inside modifyRevDenoDEAtItemContext");

		try
		{
			TCProperty prop = imanRevDe.getTCProperty(resource.getString("T4DenotationDe"));

			if (prop != null)
			{
				prop.setStringValue(new_denoDeAtItem);

				imanRevDe.lock();

				imanRevDe.save();

				imanRevDe.refresh();

				imanRevDe.unlock();
			}
			return(0);
		}
		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside imanRevDe exception loop");
			if(desktop == null)
				logger.info("desktop is NULL!!!");
			String s = tcException.getError();

			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null, resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
			return(1);
		}
	}

	/**
	 * @param imanExt TCComponentItem
	 * @return int
	 */
	public int modifyExt(TCComponentItem imanExt){


		logger.info("Inside modifyExt");

		try
		{
			TCProperty prop = imanExt.getTCProperty(resource.getString("T4IdentExt"));

			if (prop != null)
			{
				prop.setStringValue("");

				imanExt.lock();

				imanExt.save();

				imanExt.refresh();

				imanExt.unlock();
			}
			return(0);
		}

		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside imanExt exception loop");

			String s = tcException.getError();

			if (desktop != null)
			{

				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}

			else
			{
				System.out.println(s);
			}

			return(1);
		}

	}


	/**
	 * @param imanExtCounter TCComponentItem
	 * @return int
	 */
	public int modifyExtCounter(TCComponentItem imanExtCounter){


		logger.info("Inside modifyExtCounter");

		try
		{
			TCProperty prop = imanExtCounter.getTCProperty(resource.getString("T4IdentExtCounter"));

			if (prop != null)
			{
				prop.setStringValue("");

				imanExtCounter.lock();

				imanExtCounter.save();

				imanExtCounter.refresh();

				imanExtCounter.unlock();
			}
			return(0);
		}

		catch(TCException tcException)
		{
			successFlag = false;

			logger.info("Inside imanExtCounter exception loop");

			String s = tcException.getError();

			if (desktop != null)
			{

				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);

				messagebox.setVisible(true);
			}

			else
			{
				System.out.println(s);
			}

			return(1);
		}
	}

	/**
	 * @Desc: This method is used to validate all the children at Item Context level
	 * @param item TCComponentItem
	 */
	private void validateChildrenAtItemChange(TCComponentItem item)
	{
		AIFComponentContext compChildren[] = null;
		try 
		{
			compChildren = item.getChildren();

			for(int i = 0; i < compChildren.length; i++)
			{
				if (compChildren[i].getComponent() instanceof TCComponentItemRevision)
				{					
					try {
						String denoDeProperty = compChildren[i].getComponent().getProperty(resource.getString("T4DenotationDe"));
						String denoEnProperty =compChildren[i].getComponent().getProperty(resource.getString("T4DenotationEn"));
						String indexProperty =compChildren[i].getComponent().getProperty(resource.getString("T4Index"));

						if((denoDeProperty != null) || (denoEnProperty != null) || (indexProperty != null))
						{							
							modifyRevDenoDEAtItemContext((TCComponentItemRevision) compChildren[i].getComponent());	
						}
						else 
						{
							modifyRevDenoDE((TCComponentItemRevision) compChildren[i].getComponent());	
							modifyRevDenoEN((TCComponentItemRevision) compChildren[i].getComponent());
							modifyRevIndex((TCComponentItemRevision) compChildren[i].getComponent());
						}
					} catch (Exception e) {

						e.printStackTrace();
					}
				}				
			}
		} 
		catch (TCException e) 
		{
			e.printStackTrace();
		}		
	}
}