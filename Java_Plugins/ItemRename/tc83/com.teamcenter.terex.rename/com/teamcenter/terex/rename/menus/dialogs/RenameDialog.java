/*=============================================================================

    Filename    :   RenameDialog.java
    Module      :   com.teamcenter.terex.rename.menus.dialogs
    Description :   This file is used to create the dialog for Terex based 
                    various custom Items and their corresponding Item Revisions. 
                    This file contains the basic validations for their attributes
                    and the Renaming functionality at both the levels.

================================================================================
                     Modification Log
================================================================================
Date            User                Company    Change Description
10-Jul-2012     Umesh Dwivedi       PRION      Initial Version
19-Aug-2012     Umesh Dwivedi       PRION      Added Enhancement request features
24-Aug-2012     Umesh Dwivedi       PRION      Added Usability Enhancement features
31-Aug-2012     G. Langenbacher     PRION      Set align of existing values to left 
==============================================================================*/
package com.teamcenter.terex.rename.menus.dialogs;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ButtonLayout;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Separator;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.terex.rename.menus.handlers.Messages;
import com.teamcenter.terex.rename.menus.handlers.ValidateTextField;

/**
 * @author udwivedi
 *
 */
public class RenameDialog extends AbstractAIFDialog{

	/**
	 * long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger logger
	 */
	private static Logger logger = Logger.getLogger("com.teamcenter.terex.rename");

	/**
	 * String caseSensitivity
	 */
	private String caseSensitivity;

	/**
	 * boolean nameEditable
	 */
	private boolean nameEditable;

	/**
	 * char[] invalidCharacters
	 */
	private char[] invalidCharacters;

	/**
	 * int itemIdentNumberLength
	 */
	private int itemIdentNumberLength;

	/**
	 * int itemNameLength
	 */
	private int itemNameLength;

	/**
	 * JTextField old_identnumberTextField
	 */
	private JTextField old_identnumberTextField;

	/**
	 * ValidateTextField new_identnumberTextField
	 */
	private ValidateTextField new_identnumberTextField;

	/**
	 * JTextField old_ident_extensionTextField
	 */
	private JTextField old_ident_extensionTextField;

	/**
	 * JTextField new_ident_extensionTextField
	 */
	private JTextField new_ident_extensionTextField;

	/**
	 * JTextField old_ident_ext_counterTextField
	 */
	private JTextField old_ident_ext_counterTextField;

	/**
	 * JTextField new_ident_ext_counterTextField
	 */
	private JTextField new_ident_ext_counterTextField;

	/**
	 * JTextField old_nameTextField
	 */
	private JTextField old_nameTextField;

	/**
	 * ValidateTextField new_nameTextField
	 */
	private ValidateTextField new_nameTextField;

	/**
	 * JTextField old_revidTextField
	 */
	private JTextField old_revidTextField;

	/**
	 * JTextField new_revidTextField
	 */
	private JTextField new_revidTextField;

	/**
	 * JButton renameButton
	 */
	private JButton renameButton;

	/**
	 * JButton cancelButton
	 */
	private JButton cancelButton;

	/**
	 * TCSession session
	 */
	private TCSession session;

	/**
	 * TCComponentItem item
	 */
	private TCComponentItem item;

	/**
	 * TCComponentItemRevision rev
	 */
	private TCComponentItemRevision rev;

	/**
	 * AIFDesktop desktop
	 */
	private AIFDesktop desktop;

	/**
	 * String old_identnumber
	 */
	private String old_identnumber;

	/**
	 * String new_identnumber
	 */
	private String new_identnumber;

	/**
	 * String old_name
	 */
	private String old_name;

	/**
	 * String new_name
	 */
	private String new_name;

	/**
	 * String old_revid
	 */
	private String old_revid;

	/**
	 * String new_revid
	 */
	private String new_revid;

	/**
	 * JTextField old_denoEnTextField
	 */
	private JTextField old_denoDeAtItemTextField;

	/**
	 * ValidateTextField new_denoEnTextField
	 */
	private ValidateTextField new_denoDeAtItemTextField;

	/**
	 * Vector<TCComponent> attachedObjects
	 */
	private Vector<TCComponent> attachedObjects = null;

	/**
	 * String old_denoDeVar
	 */
	private String old_denoDeVar;

	/**
	 * String new_denoDe
	 */
	private String new_denoDe;

	/**
	 * LoadOperation loadOp
	 */
	private LoadOperation loadOp;

	/**
	 * RenameOperation renameOp
	 */
	private RenameOperation renameOp;	

	/**
	 * ResourceBundle resource
	 */
	private static ResourceBundle resource = ResourceBundle.getBundle( Messages.bundle  );


	/**
	 * @return String
	 */
	public String getOldIdentNumber() {
		return old_identnumberTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewIdentNumber() {
		return new_identnumberTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getOldIdentExtension() {
		return old_ident_extensionTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewIdentExtension() {
		return new_ident_extensionTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getOldIdentExtCounter() {
		return old_ident_ext_counterTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewIdentExtCounter() {
		return new_ident_ext_counterTextField.getText();
	} 

	/**
	 * @return String
	 */
	public String getOldName(){
		return old_nameTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewName(){
		return new_nameTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getOldRevID() {
		return old_revidTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewRevID() {
		return new_revidTextField.getText();
	} 

	/**
	 * @return String
	 */
	public String getOldDENODEAtItemContext() 
	{
		return old_denoDeAtItemTextField.getText();
	} 

	/**
	 * @return String
	 */
	public String getNewDENODEAtItemContext() 
	{
		return new_denoDeAtItemTextField.getText();
	} 


	/**
	 * @param shellParent Shell
	 * @param iStyle int
	 * @param tcSession TCSession
	 * @param currentDesktop AIFDesktop
	 * @param selectedPart TCComponentItem
	 * @param theCaseSensitivity String
	 * @param theItemNameEditable boolean
	 * @param theInvalidCharacters[] char
	 */
	public RenameDialog(Shell shellParent, int iStyle, TCSession tcSession, AIFDesktop currentDesktop, TCComponentItem selectedPart,
			String theCaseSensitivity, boolean theItemNameEditable, char theInvalidCharacters[])
	{
		super(currentDesktop, true);
		logger.info("Inside RenameDialog");		
		session 		= null;
		item 			= null;
		rev 			= null;
		old_identnumber	= null;
		new_identnumber	= null;
		old_name		= null;
		new_name		= null;
		attachedObjects = new Vector<TCComponent>();
		new Vector<String>();
		new Vector<String>();
		new Vector<String>();
		new Vector<String>();
		new Vector<String>();
		new Vector<String>();
		desktop 		= currentDesktop;
		session 		= tcSession;
		item 			= selectedPart;
		caseSensitivity = theCaseSensitivity;
		nameEditable 	= theItemNameEditable;
		invalidCharacters = theInvalidCharacters;
		itemIdentNumberLength = 8;
		itemNameLength 	= 40;

		if(desktop == null)
			logger.info("desktop is NULL at TOP of RenameDialog Constructor Itemcontext!!!");		

		logger.info("caseSensitivity = '" + caseSensitivity + "', nameEditable = " + nameEditable + ", itemIdentNumberLength = " + itemIdentNumberLength + ", itemNameLength = " + itemNameLength);
		for(int i = 0; i < invalidCharacters.length; i++)
			logger.info("invalidCharacters[" + i + "] = '" + invalidCharacters[i] + "'");

		try
		{
			old_identnumber = item.getTCProperty(resource.getString("T4IdentNumber")).toString();
			old_name        = item.getTCProperty(resource.getString("ObjectName")).toString();
			old_revid 		= "N/A";
			new_revid 		= "N/A";

			// get latest revision
			TCComponentItemRevision latestRev = item.getLatestItemRevision();

			item = latestRev.getItem();
			old_denoDeVar = latestRev.getTCProperty(resource.getString("T4DenotationDe")).toString();
		}
		catch(TCException tcException)
		{
			String s = tcException.getError();
			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);
				messagebox.setVisible(true);
			}
			else
			{
				logger.info(s);
			}
		}

		rev = null;
		logger.info("oldIdentNumber = " + old_identnumber + ", oldName = " + old_name);
		initializeDialog();  
	}

	/**
	 * Method: endOperation
	 */
	public void endOperation()
	{

		setVisible(false);
		dispose();
		setCursor(Cursor.getPredefinedCursor(0));

		boolean suc = false;  
		suc = renameOp.getSuccessFlag();

		session.setReadyStatus();

		if (suc == true)
		{
		}
		else
		{
			MessageBox messagebox = new MessageBox(desktop,resource.getString("renameFailure.info"),resource.getString("info.TITLE"), MessageBox.INFORMATION);
			messagebox.setModal(true);
			messagebox.setVisible(true);
		}
	}

	/**
	 * Method: initializeDialog
	 */
	private void initializeDialog()
	{

		logger.info("Inside RenameDialog::initializeDialog"); 		

		// old identnumber
		old_identnumberTextField= new JTextField(32);
		old_identnumberTextField.setEnabled(false);	
		//old_identnumberTextField.setHorizontalAlignment(JTextField.CENTER);

		// new identnumber
		new_identnumberTextField= new ValidateTextField(resource.getString("allowedDigits"),null, 32,8);
		new_identnumberTextField.setEnabled(true);
		new_identnumberTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{	
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});


		// old name
		old_nameTextField = new JTextField(32);
		old_nameTextField.setEnabled(false);
		//old_nameTextField.setHorizontalAlignment(JTextField.CENTER);


		// new name
		new_nameTextField = new ValidateTextField(resource.getString("allowedChars"),null, 32,40);
		if(nameEditable == true)
			new_nameTextField.setEnabled(true);
		else	
			new_nameTextField.setEnabled(false);
		new_nameTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e)
			{	
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		// old revision id
		old_revidTextField = new JTextField(32);
		old_revidTextField.setEnabled(false);
		//old_revidTextField.setHorizontalAlignment(JTextField.CENTER);

		// new revision id
		new_revidTextField = new JTextField(32);

		// old_denoDeAtItemTextField
		old_denoDeAtItemTextField= new JTextField(32);
		old_denoDeAtItemTextField.setEnabled(false);
		//old_denoDeAtItemTextField.setHorizontalAlignment(JTextField.CENTER);

		// new_denoDeAtItemTextField
		new_denoDeAtItemTextField = new ValidateTextField(resource.getString("allowedChars"),null, 32,40);		
		new_denoDeAtItemTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e)
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		// Rename Button definitions
		renameButton = new JButton(resource.getString("Rename"));
		renameButton.setEnabled(false);
		renameButton.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});
		renameButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent actionevent)
			{
				renameButton.setEnabled(false);
				int checkID 	 = 0;
				int doNotProceed = 0;

				// check to make sure that identnumber has not been used
				if(!(new_identnumberTextField.getText().equalsIgnoreCase(old_identnumber)))
				{
					try
					{
						TCComponentItemType itemType = (TCComponentItemType)session.getTypeComponent("Item");
						TCComponentItem  item = new TCComponentItem();

						item = itemType.find(new_identnumberTextField.getText());

						if(item != null)
						{
							renameButton.setEnabled(false);
							MessageBox messagebox = new MessageBox(desktop, resource.getString("existingItemID.error"), resource.getString("error.TITLE"), MessageBox.ERROR);
							messagebox.setModal(true);
							messagebox.setVisible(true);

							if(caseSensitivity.equalsIgnoreCase("UPPER"))
								new_identnumberTextField.setText(old_identnumber.toUpperCase());
							else if(caseSensitivity.equalsIgnoreCase("LOWER"))
								new_identnumberTextField.setText(old_identnumber.toLowerCase());
							else
								new_identnumberTextField.setText(old_identnumber);

							checkID = 1;
						}
					}
					catch(TCException tcException)
					{
						String s = tcException.getError();
						if (desktop != null)
						{
							MessageBox messagebox = new MessageBox(desktop, s, resource.getString("existingItemID.error"),resource.getString("error.TITLE"), MessageBox.ERROR, true);
							messagebox.setVisible(true);
						}
						else
						{
							System.out.println(s);
						}
					}
				}

				doNotProceed = checkID;

				if(doNotProceed == 0)
					startCreateOperation();
			}
		});


		//Cancel Button Defintions
		cancelButton = new JButton(resource.getString("Cancel"));
		cancelButton.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});
		cancelButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent actionevent)
			{
				session.setReadyStatus();
				setVisible(false);

				dispose();
			}
		});

		setDefaultCloseOperation(0);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				cancelButton.doClick();
			}
		});


		if(rev != null)
			setTitle(resource.getString("rev.dialog.title"));

		else
			setTitle(resource.getString("item.dialog.title"));

		JPanel verticalPanel 	= new JPanel(new VerticalLayout(5, 2, 2, 2, 2));
		getContentPane().add(verticalPanel);
		JPanel buttonPanel 	= new JPanel(new ButtonLayout());
		JPanel verPanel 	= new JPanel(new VerticalLayout());
		JPanel fieldPanel 	= new JPanel(new PropertyLayout());
		JPanel propertyPanel 	= new JPanel(new PropertyLayout());
		JLabel jLabel11 = new JLabel("");
		JLabel existingValLabel = new JLabel("                   " + resource.getString("ExistingValue")+ "                   ");
		existingValLabel.setHorizontalAlignment(0);
		JLabel desiredValLabel = new JLabel("                   "+ resource.getString("DesiredValue") + "                   ");
		desiredValLabel.setHorizontalAlignment(0);
		JLabel identNoLabel = new JLabel(resource.getString("Identnumber"));
		JLabel itemNameLabel = new JLabel(resource.getString("ItemName"));
		JLabel revIDLabel = new JLabel(resource.getString("RevisionID"));
		JLabel denoDELabel = new JLabel(resource.getString("DENOTATIONDE"));

		if (rev == null)
		{
			// Add the Dialog components
			buttonPanel.add(renameButton);
			buttonPanel.add(cancelButton);
			fieldPanel.add("1.1.right.top", jLabel11);
			fieldPanel.add("1.2.right.top", existingValLabel);
			fieldPanel.add("1.3.right.top", desiredValLabel);
			fieldPanel.add("2.1.right.top", identNoLabel);
			fieldPanel.add("2.2.right.top", old_identnumberTextField);
			fieldPanel.add("2.3.right.top", new_identnumberTextField);
			fieldPanel.add("3.1.right.top", itemNameLabel);
			fieldPanel.add("3.2.right.top", old_nameTextField);
			fieldPanel.add("3.3.right.top", new_nameTextField);		
			fieldPanel.add("4.1.right.top", denoDELabel );
			fieldPanel.add("4.2.right.top", old_denoDeAtItemTextField);
			fieldPanel.add("4.3.right.top", new_denoDeAtItemTextField);
		}

		if(rev != null)
		{
			itemNameLabel.setText(resource.getString("RevName"));
			fieldPanel.add("4.1.right.top", revIDLabel);
			fieldPanel.add("4.2.right.top", old_revidTextField);
			fieldPanel.add("4.3.right.top", new_revidTextField);
		}

		// Add the Dialog components and pack
		verticalPanel.add("top.bind", verPanel);
		verticalPanel.add("top.bind", new JLabel(""));
		verticalPanel.add("top.bind", new JLabel(""));
		verticalPanel.add("top.bind", fieldPanel);
		verticalPanel.add("top.bind.resizable.resizable",propertyPanel);
		verticalPanel.add("bottom.bind.center.top", buttonPanel);
		verticalPanel.add("bottom.bind", new Separator());
		pack();
		setModal(true);
		centerToScreen(1.0D, 1.0D);
		startLoadOperation();
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher
		(
				new KeyEventDispatcher() 
				{  
					public boolean dispatchKeyEvent(KeyEvent e) 
					{  
						boolean keyHandled = false;  
						if (e.getID() == KeyEvent.KEY_PRESSED) 
						{  
							if (e.getKeyCode() == KeyEvent.VK_ESCAPE) 
							{  
								session.setReadyStatus();
								setVisible(false);
								dispose();
								keyHandled = true;  
							}  
						}  
						return keyHandled;  
					}
				}
		); 
	}

	/* (non-Javadoc)
	 * @see com.teamcenter.rac.aif.AbstractAIFDialog#run()
	 */
	public void run(){

		logger.info("Inside RenameDialog::run");	  
		setVisible(true);

	}

	/**
	 * Method: startCreateOperation
	 */
	private void startCreateOperation(){
		logger.info("Inside RenameDialog::startCreateOperation");
		if(desktop == null)
			logger.info("desktop is NULL at RenameDialog::RenameOperation constructor call!!!");

		renameOp = new RenameOperation(session, desktop, getOldIdentNumber(), getNewIdentNumber(),getOldName(), getNewName(), getOldRevID(), getNewRevID(),getOldDENODEAtItemContext(),getNewDENODEAtItemContext(), item, rev, attachedObjects);		

		try
		{
			session.setStatus(resource.getString("renamingOperation"));
			setVisible(false);
			dispose();
			setCursor(Cursor.getPredefinedCursor(0));
			renameOp.execute();
			endOperation();
		}
		catch(Exception exception)
		{
			String s = exception.getMessage();
			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, resource.getString("renamingOperation"),
						resource.getString("error.TITLE"), MessageBox.ERROR, true);
				messagebox.setVisible(true);
			}
			else
			{
				logger.info(s);
			}
		}	  
	}

	/**
	 * Method: startLoadOperation
	 */
	private void startLoadOperation(){
		logger.info("Inside RenameDialog::startLoadOperation");
		loadOp = new LoadOperation();

		session.queueOperation(loadOp);
	}

	/**
	 * Method: validateFields
	 */
	private void validateFields(){
		logger.info("Inside validateFields");

		String testString = null;

		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_identnumberTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_identnumberTextField.getText().toLowerCase();
		else
			testString = new_identnumberTextField.getText();

		if (! (new_identnumberTextField.getText().equals(testString)))

			new_identnumberTextField.setText(testString);

		if(rev == null)
		{
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_nameTextField.getText().toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString = new_nameTextField.getText().toLowerCase();
			else
				testString = new_nameTextField.getText();

			if (! (new_nameTextField.getText().equals(testString)))

				new_nameTextField.setText(testString);
		}

		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_revidTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_revidTextField.getText().toLowerCase();
		else
			testString = new_revidTextField.getText();

		if(!(new_revidTextField.getText().equals(testString)))
			new_revidTextField.setText(testString);

		renameButton.setEnabled(false);
		boolean emptyValue = false;
		int change = 0;

		old_identnumberTextField.setEnabled(true);
		old_identnumberTextField.setEnabled(false);
		old_nameTextField.setEnabled(true);
		old_nameTextField.setEnabled(false);
		old_revidTextField.setEnabled(true);
		old_revidTextField.setEnabled(false);

		if(old_identnumberTextField.isEditable())

			old_identnumberTextField.setBackground(Color.WHITE);

		if(old_nameTextField.isEditable())

			old_nameTextField.setBackground(Color.WHITE);

		if(old_denoDeAtItemTextField.isEditable())

			old_denoDeAtItemTextField.setBackground(Color.WHITE);

		if(new_identnumberTextField.isEditable())

			new_identnumberTextField.setBackground(Color.WHITE);

		if(nameEditable == true)
			new_nameTextField.setEnabled(true);
		else	
			new_nameTextField.setEnabled(false);

		if(new_nameTextField.isEditable())

			new_nameTextField.setBackground(Color.WHITE);

		if(new_revidTextField.isEditable())

			new_revidTextField.setBackground(Color.WHITE);

		if(new_denoDeAtItemTextField.isEditable())

			new_denoDeAtItemTextField.setBackground(Color.WHITE);

		// add up if we need a change
		if((new_identnumberTextField.getText() == null) || new_identnumberTextField.getText().length() < 8)
			emptyValue = true;

		if((!(new_identnumberTextField.getText().equals(old_identnumber))))
		{
			new_identnumberTextField.setBackground(Color.YELLOW);
			change++;

			// JBH2 - auto upper case for Identnumber field
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_identnumberTextField.getText().toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString = new_identnumberTextField.getText().toLowerCase();
			else
				testString = new_identnumberTextField.getText();

			if (! (new_identnumberTextField.getText().equals(testString)))
			{
				new_identnumberTextField.setText(testString);
				if(new_identnumberTextField.getText().equals(old_identnumber))
				{
					change--;
					new_identnumberTextField.setBackground(Color.WHITE);
				}
			}
			// JBH2
		}

		if((new_nameTextField.getText() == null) || new_nameTextField.getText().length() == 0)
			emptyValue = true;

		if((! (new_nameTextField.getText().equals(old_name))))
		{
			new_nameTextField.setBackground(Color.YELLOW);
			change++;

			// JBH2 - auto upper case for Name field
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_nameTextField.getText().toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString = new_nameTextField.getText().toLowerCase();
			else
				testString = new_nameTextField.getText();

			if(!(new_nameTextField.getText().equals(testString)))
			{
				new_nameTextField.setText(testString);
				if(new_nameTextField.getText().equals(old_name))
				{
					change--;
					new_nameTextField.setBackground(Color.WHITE);
				}
			}
			// JBH2
		}


		if((new_revidTextField.getText() == null) || new_revidTextField.getText().length() == 0)
			emptyValue = true;

		if((!(new_revidTextField.getText().equals(old_revid))))
		{
			change++;

			// JBH2 - auto upper case for Rev ID field
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_revidTextField.getText().toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString = new_revidTextField.getText().toLowerCase();
			else
				testString = new_revidTextField.getText();

			if(!(new_revidTextField.getText().equals(testString)))
			{
				new_revidTextField.setText(testString);
				if(new_revidTextField.getText().equals(old_revid))
				{
					change--;
					new_revidTextField.setBackground(Color.WHITE);
				}
			}
			// JBH2
		}

		if(rev == null)
		{
			if((new_denoDeAtItemTextField.getText() == null) || new_denoDeAtItemTextField.getText().length() == 0)
				emptyValue = true;			

			if((!(new_denoDeAtItemTextField.getText().equals(old_denoDeVar))))

			{	
				new_denoDeAtItemTextField.setBackground(Color.YELLOW);
				change++;
			}
		}

		//Adding check to distinguish rename button be enable Or, disable
		if((change != 0) && (emptyValue == false))
		{
			renameButton.setEnabled(true);
			renameButton.setFocusable(true);
		}
		else

			renameButton.setEnabled(false);
	}

	/**
	 * Method: populateFields
	 */
	private void populateFields()
	{
		logger.info("Inside populateFields");		

		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			new_identnumber = old_identnumber.toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			new_identnumber = old_identnumber.toLowerCase();
		else
			new_identnumber = old_identnumber;

		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			new_name = old_name.toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			new_name = old_name.toLowerCase();
		else
			new_name = old_name;		

		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			new_revid = old_revid.toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			new_revid = old_revid.toLowerCase();
		else
			new_revid = old_revid;

		if(rev != null)
		{
			new_identnumberTextField.setEnabled(true);
			new_identnumberTextField.setEnabled(false);
		}

		if(rev == null)
		{
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_denoDe = old_denoDeVar.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_denoDe = old_denoDeVar.toLowerCase();
			else
				new_denoDe = old_denoDeVar;
		}
		old_identnumberTextField.setEnabled(true);
		old_identnumberTextField.setEnabled(false);
		old_nameTextField.setEnabled(true);
		old_nameTextField.setEnabled(false);
		old_revidTextField.setEnabled(true);
		old_revidTextField.setEnabled(false);

		old_denoDeAtItemTextField.setText(old_denoDeVar);
		new_denoDeAtItemTextField.setText(new_denoDe);

		// Places the info into the fields
		old_identnumberTextField.setText(old_identnumber);
		new_identnumberTextField.setText(new_identnumber);
		old_nameTextField.setText(old_name);
		new_nameTextField.setText(new_name);
		old_revidTextField.setText(old_revid);
		new_revidTextField.setText(new_revid);

		validateFields();
		setVisible(true);
	} // End of PopulateFields Class

	/**
	 * @author udwivedi
	 *
	 */
	private class LoadOperation extends AbstractAIFOperation{

		public void executeOperation(){

			logger.info("Inside RenameDialog::LoadOperation::executeOperation");
			populateFields();
		}

		LoadOperation()
		{
		}

	}//End of LoadOperation Class

	/**
	 * @param s String
	 */
	public void startOperation(String s){

		logger.info("Inside RenameDialog::startOperation");
		renameButton.setVisible(false);
		cancelButton.setVisible(false);
		new_identnumberTextField.setEnabled(false);
		new_nameTextField.setEnabled(false);
		new_revidTextField.setEnabled(false);
		validate();
		validateFields();
	}

	/**
	 * @param e keyrelease
	 */
	public void keyrelease(KeyEvent e)
	{
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) 
		{  
			session.setReadyStatus();
			setVisible(false);
			dispose();
			return ;
		}
		validateFields();
	}
}//end of class