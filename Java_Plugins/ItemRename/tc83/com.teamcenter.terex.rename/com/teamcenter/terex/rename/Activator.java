package com.teamcenter.terex.rename;

import org.osgi.framework.BundleContext;

import com.teamcenter.rac.kernel.AbstractRACPlugin;
import com.teamcenter.rac.services.IAspectService;
import com.teamcenter.rac.services.IAspectUIService;

import org.apache.log4j.Logger;


public class Activator extends AbstractRACPlugin 
{
	// The plug-in ID
	public static final String PLUGIN_ID = "com.teamcenter.terex.rename";

	// The shared instance
	private static Activator plugin;
	
	/**
	 * Reference to the Logger
	 */
	private static Logger logger = Logger.getLogger("com.teamcenter.terex.rename");
	
	/**
	 * The constructor
	 */
	public Activator() 
	{
		super();
		Activator.plugin = this;

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception 
	{
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception 
	{
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() 
	{
		return plugin;
	}
	
	@Override
	public IAspectUIService getUIService()
	{
		return null;
	}
	
	@Override
	public IAspectService getLogicService()
	{
		return null;
	}
	
	@Override
    protected void setupServices( BundleContext context )
	{
        
	}

}
