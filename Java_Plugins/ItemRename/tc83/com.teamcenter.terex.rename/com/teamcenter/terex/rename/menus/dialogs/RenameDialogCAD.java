/*==============================================================================

    Filename    :   RenameDialogCAD.java
    Module      :   com.teamcenter.terex.rename.menus.dialogs
    Description :   This file is used to create the dialog for Terex based 
                    various custom Items and their corresponding Item Revisions. 
                    This file contains the basic validations for their attributes
                    and the Renaming functionality at both the levels.

================================================================================
                     Modification Log
================================================================================
Date            User                Company    Change Description
10-Jul-2012     Umesh Dwivedi       PRION      Initial Version
19-Aug-2012     Umesh Dwivedi       PRION      Added Enhancement request features
24-Aug-2012     Umesh Dwivedi       PRION      Added Usability Enhancement features
31-Aug-2012     G. Langenbacher     PRION      Set align of existing values to left
 											   Set fea stage min char to 1, instead of 2
==============================================================================*/


package com.teamcenter.terex.rename.menus.dialogs;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.eclipse.swt.widgets.Shell;

import com.teamcenter.rac.aif.AIFDesktop;
import com.teamcenter.rac.aif.AbstractAIFDialog;
import com.teamcenter.rac.aif.AbstractAIFOperation;
import com.teamcenter.rac.kernel.TCComponent;
import com.teamcenter.rac.kernel.TCComponentItem;
import com.teamcenter.rac.kernel.TCComponentItemRevision;
import com.teamcenter.rac.kernel.TCComponentItemType;
import com.teamcenter.rac.kernel.TCException;
import com.teamcenter.rac.kernel.TCSession;
import com.teamcenter.rac.util.ButtonLayout;
import com.teamcenter.rac.util.MessageBox;
import com.teamcenter.rac.util.PropertyLayout;
import com.teamcenter.rac.util.Separator;
import com.teamcenter.rac.util.VerticalLayout;
import com.teamcenter.terex.rename.menus.handlers.Messages;
import com.teamcenter.terex.rename.menus.handlers.ValidateTextField;

/**
 * @author udwivedi
 *
 */
public class RenameDialogCAD extends AbstractAIFDialog
{

	/**
	 * long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger logger
	 */

	private static Logger logger = Logger.getLogger("com.teamcenter.terex.rename");

	/**
	 * ResourceBundle resource
	 */
	private static ResourceBundle resource = ResourceBundle.getBundle( Messages.bundle  );

	/**
	 * String caseSensitivity
	 */
	private String caseSensitivity;

	/**
	 * boolean nameEditable
	 */
	private boolean nameEditable;

	/**
	 * char[] invalidCharacters
	 */
	private char[] invalidCharacters;

	/**
	 * int itemIdentNumberLength
	 */
	private int itemIdentNumberLength;

	/**
	 * int itemIdentExtensionLength
	 */
	private int itemIdentExtensionLength;

	/**
	 * int itemNameLength
	 */
	private int itemNameLength;

	/**
	 * int itemIdentExtCounterLength
	 */
	private int itemIdentExtCounterLength;

	/**
	 * int revIdLength
	 */
	private int revIdLength;

	/**
	 * JTextField old_identnumberTextField
	 */
	private JTextField old_identnumberTextField;

	/**
	 * ValidateTextField new_identnumberTextField
	 */
	private ValidateTextField new_identnumberTextField;

	/**
	 * JTextField new_identnumberAtRevTextField
	 */
	private JTextField new_identnumberAtRevTextField;


	/**
	 * JTextField old_denoDeAtItemTextField
	 */
	private JTextField old_denoDeAtItemTextField;

	/**
	 * ValidateTextField new_denoDeAtItemTextField
	 */
	private ValidateTextField new_denoDeAtItemTextField;


	/**
	 * JTextField old_ident_extensionTextField
	 */
	private JTextField old_ident_extensionTextField;

	/**
	 * ValidateTextField new_ident_extensionTextField
	 */
	private ValidateTextField new_ident_extensionTextField;

	/**
	 * JTextField old_ident_ext_counterTextField
	 */
	private JTextField old_ident_ext_counterTextField;

	/**
	 * ValidateTextField new_ident_ext_counterTextField
	 */
	private ValidateTextField new_ident_ext_counterTextField;

	/**
	 * JTextField old_nameTextField
	 */
	private JTextField old_nameTextField;

	/**
	 * ValidateTextField new_nameTextField
	 */
	private ValidateTextField new_nameTextField;

	/**
	 * JTextField old_revidTextField
	 */
	private JTextField old_revidTextField;

	/**
	 * ValidateTextField new_revidTextField
	 */
	private ValidateTextField new_revidTextField;

	/**
	 * JTextField old_denoDeTextField
	 */
	private JTextField old_denoDeTextField;

	/**
	 * ValidateTextField new_denoDeTextField
	 */
	private ValidateTextField new_denoDeTextField;

	/**
	 * String new_denoDe
	 */
	private String new_denoDe;

	/**
	 * JTextField old_denoEnTextField
	 */
	private JTextField old_denoEnTextField;

	/**
	 * ValidateTextField new_denoEnTextField
	 */
	private ValidateTextField new_denoEnTextField;

	/**
	 * String new_denoEn
	 */
	private String new_denoEn;

	/**
	 * String newRevStr
	 */
	private static String newRevStr= null;

	/**
	 * String old_feaStage
	 */
	private String old_feaStage;

	/**
	 * String new_feaStage
	 */
	private String new_feaStage;

	/**
	 * String old_feaDev
	 */
	private String old_feaDev;

	/**
	 * String new_feaDev
	 */
	private String new_feaDev;

	/**
	 * JTextField old_feaStageTextField
	 */
	private JTextField old_feaStageTextField;

	/**
	 * ValidateTextField new_feaStageTextField
	 */
	private ValidateTextField new_feaStageTextField;

	/**
	 * JTextField old_feaDevTextField
	 */
	private JTextField old_feaDevTextField;

	/**
	 * ValidateTextField new_feaDevTextField
	 */
	private ValidateTextField new_feaDevTextField;

	/**
	 * JTextField old_indexTextField
	 */
	private JTextField old_indexTextField;

	/**
	 * ValidateTextField new_indexTextField
	 */
	private ValidateTextField new_indexTextField;

	/**
	 * String new_index
	 */
	private String new_index;

	/**
	 * JButton renameButton
	 */
	private JButton renameButton;

	/**
	 * JButton cancelButton
	 */
	private JButton cancelButton;

	/**
	 * TCSession session
	 */
	private TCSession session;

	/**
	 * TCComponentItem item
	 */
	private TCComponentItem item = new TCComponentItem();

	/**
	 * TCComponentItemRevision rev
	 */
	private TCComponentItemRevision rev;

	/**
	 * AIFDesktop desktop
	 */
	private AIFDesktop desktop;

	/**
	 * String old_identnumber
	 */
	private String old_identnumber;

	/**
	 * String new_identnumber
	 */
	private String new_identnumber;

	/**
	 * String old_ident_extension
	 */
	private String old_ident_extension;

	/**
	 * String new_ident_extension
	 */
	private String new_ident_extension;

	/**
	 * String old_ident_ext_counter
	 */
	private String old_ident_ext_counter;

	/**
	 * String new_ident_ext_counter
	 */
	private String new_ident_ext_counter;

	/**
	 * String old_name
	 */
	private String old_name;

	/**
	 * String new_name
	 */
	private String new_name;

	/**
	 * String old_revid
	 */
	private String old_revid;

	/**
	 * String new_revid
	 */
	private String new_revid;

	/**
	 * String old_denoDeVar
	 */
	private String old_denoDeVar;

	/**
	 * String old_denoEnVar
	 */
	private String old_denoEnVar;

	/**
	 * String old_indexVar
	 */
	private String old_indexVar;

	/**
	 * Vector<TCComponent> attachedObjects
	 */
	private Vector<TCComponent> attachedObjects = null;

	/**
	 * String tcClass
	 */
	private String tcClass = null;

	/**
	 * LoadOperation loadOp
	 */
	private LoadOperation loadOp;

	/**
	 * RenameOperationCAD renameOp
	 */
	private RenameOperationCAD renameOp;

	/**
	 * @return String
	 */
	public String getOldIdentNumber() 
	{
		return old_identnumberTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewIdentNumber() 
	{
		return new_identnumberTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getOldIdentExtension() 
	{
		return old_ident_extensionTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewIdentExtension() 
	{
		return new_ident_extensionTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getOldIdentExtCounter() 
	{
		return old_ident_ext_counterTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewIdentExtCounter() 
	{
		return new_ident_ext_counterTextField.getText();
	} 

	/**
	 * @return String
	 */
	public String getOldName()
	{
		return old_nameTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewName()
	{
		return new_nameTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getOldRevID() 
	{
		return old_revidTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewRevID() 
	{
		return new_revidTextField.getText();
	} 

	/**
	 * @return String
	 */
	public String getOldDENODEAtItemContext() 
	{
		return old_denoDeAtItemTextField.getText();
	} 

	/**
	 * @return String
	 */
	public String getOldDENODE() 
	{
		return old_denoDeTextField.getText();
	}


	/**
	 * @return String
	 */
	public String getNewDENODEAtItemContext() 
	{
		return new_denoDeAtItemTextField.getText();
	} 

	/**
	 * @return String
	 */
	public String getNewDENODE() 
	{
		return new_denoDeTextField.getText();
	} 

	/**
	 * @return String
	 */
	public String getOldDENOEN() 
	{
		return old_denoEnTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewDENOEN() 
	{
		return new_denoEnTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getOldIndex() 
	{
		return old_indexTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewIndex() 
	{
		return new_indexTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getOldfeaStage() 
	{
		return old_feaStageTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewfeaStage() 
	{
		return new_feaStageTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getOldfeaDev() 
	{
		return old_feaDevTextField.getText();
	}

	/**
	 * @return String
	 */
	public String getNewfeaDev() 
	{
		return new_feaDevTextField.getText();
	}


	/**
	 * @param shellParent Shell
	 * @param iStyle int
	 * @param tcSession TCSession
	 * @param currentDesktop AIFDesktop
	 * @param selectedPart TCComponentItem
	 * @param theCaseSensitivity String
	 * @param theItemNameEditable boolean
	 * @param theInvalidCharacters char
	 */
	public RenameDialogCAD(Shell shellParent, int iStyle, TCSession tcSession, AIFDesktop currentDesktop, TCComponentItem selectedPart,
			String theCaseSensitivity, boolean theItemNameEditable, char theInvalidCharacters[]){

		super(currentDesktop, true);
		logger.info("Inside RenameDialogCAD");

		session 												= null;   
		item 													= null;
		rev 													= null;
		old_identnumber											= null;
		new_identnumber											= null;
		old_name												= null;		
		new_name												= null;
		old_ident_extension										= null;
		new_ident_extension										= null;
		old_ident_ext_counter									= null;
		new_ident_ext_counter									= null;
		attachedObjects 										= new Vector<TCComponent>();			
		desktop 												= currentDesktop;

		if(desktop == null)
			logger.info("desktop is NULL at TOP of RenameDialogCAD Constructor Item Context!!!");

		session 					= tcSession;
		item 						= selectedPart;
		caseSensitivity 			= theCaseSensitivity;
		nameEditable 				= theItemNameEditable;
		invalidCharacters 			= theInvalidCharacters;
		itemIdentNumberLength 		= 8;
		revIdLength 				= 2;
		itemNameLength 				= 40;
		itemIdentExtensionLength	= 4;
		itemIdentExtCounterLength	= 4;

		logger.info("caseSensitivity = '" + caseSensitivity + "', nameEditable = " + nameEditable + ", itemIdentNumberLength = " + itemIdentNumberLength + ", itemNameLength = " + 
				itemNameLength+",itemExtensionLength="+itemIdentExtensionLength+",itemcounterLength"+itemIdentExtCounterLength);

		for(int i = 0; i < invalidCharacters.length; i++)
			logger.info("invalidCharacters[" + i + "] = '" + invalidCharacters[i] + "'");

		try
		{
			old_identnumber 			= item.getTCProperty(resource.getString("T4IdentNumber")).toString();
			old_ident_extension   	    = item.getTCProperty(resource.getString("T4IdentExt")).toString();
			old_ident_ext_counter	  	= item.getTCProperty(resource.getString("T4IdentExtCounter")).toString();
			old_name        			= item.getTCProperty(resource.getString("ObjectName")).toString();
			old_revid = "N/A";
			new_revid = "N/A";

			// get latest revision
			TCComponentItemRevision latestRev = item.getLatestItemRevision();

			item = latestRev.getItem();
			old_denoDeVar = latestRev.getTCProperty(resource.getString("T4DenotationDe")).toString();
		}

		catch(TCException tcException)
		{
			String s = tcException.getError();
			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);
				messagebox.setVisible(true);
			}
			else
			{
				logger.info(s);
			}
		}

		rev = null;
		logger.info("oldIdentNumber = " + old_identnumber + ", oldName = " + old_name);
		initializeDialog(tcClass); 

	}

	/**
	 * @param shellParent Shell
	 * @param iStyle int
	 * @param tcSession TCSession
	 * @param currentDesktop AIFDesktop
	 * @param selectedPartRev TCComponentItemRevision
	 * @param theCaseSensitivity String
	 * @param theRevNameEditable boolean
	 * @param theInvalidCharacters char
	 * @param theRevIdLength int
	 * @param theRevNameLength int
	 * @param thedenoDELength int
	 * @param thedenoENLength int
	 * @param theindexLength int
	 * @param thefeaStageLength int
	 * @param thefeaDevLength int
	 */
	public RenameDialogCAD(Shell shellParent, int iStyle, TCSession tcSession, AIFDesktop currentDesktop, TCComponentItemRevision selectedPartRev, String theCaseSensitivity, 
			boolean theRevNameEditable, char theInvalidCharacters[], int theRevIdLength, int theRevNameLength, int thedenoDELength, int thedenoENLength, int theindexLength, int thefeaStageLength, int thefeaDevLength)
	{

		super(currentDesktop, true);
		logger.info("Inside RenameDialogCAD ItemRevision Context");

		session = null;
		item = null;
		rev = null;
		old_identnumber = null;
		new_identnumber = null;
		old_name = null;
		new_name = null;
		old_revid = null;
		new_revid = null;
		old_denoDeVar=null;
		old_denoEnVar=null;
		old_indexVar=null;
		attachedObjects = new Vector<TCComponent>();		
		desktop = currentDesktop;
		if(desktop == null)
			logger.info("desktop is NULL at RenameDialogCAD Constructor ItemRevision Context!!!");
		session = tcSession;
		rev = selectedPartRev;
		caseSensitivity = theCaseSensitivity;
		nameEditable = theRevNameEditable;
		invalidCharacters = theInvalidCharacters;	
		revIdLength = theRevIdLength;
		itemIdentNumberLength = 8;
		itemNameLength= 40;
		logger.info("caseSensitivity = '" + caseSensitivity + "', nameEditable = " + nameEditable + ", itemIdLength = " + itemIdentNumberLength+ ", revIdLength = " + revIdLength + ", nameLength = " + itemNameLength);
		for(int i = 0; i < invalidCharacters.length; i++)
			logger.info("invalidCharacters[" + i + "] = '" + invalidCharacters[i] + "'");

		try

		{			
			item = rev.getItem();

			tcClass = selectedPartRev.getTCClass().getClassName();
			if(selectedPartRev!=null && (tcClass.equals("T4_ArticleRevision") || tcClass.equals("T4_Standard_PartRevision") ))
			{
				item = selectedPartRev.getItem();
				old_identnumber = item.getTCProperty(resource.getString("T4IdentNumber")).toString();

				newRevStr = item.getTCProperty(resource.getString("ItemId")).toString();
			}
			else
			{
				old_identnumber = rev.getTCProperty(resource.getString("T4IdentNumber")).toString();
				old_identnumber = old_identnumber + "-";
				old_identnumber = old_identnumber+ rev.getTCProperty(resource.getString("T4IdentExt")).toString();
				old_identnumber = old_identnumber + rev.getTCProperty(resource.getString("T4IdentExtCounter")).toString();
				newRevStr = item.getTCProperty(resource.getString("ItemId")).toString();
			}

			old_name = item.getTCProperty(resource.getString("ObjectName")).toString();
			old_revid = rev.getTCProperty(resource.getString("ItemRevId")).toString();
			old_denoDeVar = rev.getTCProperty(resource.getString("T4DenotationDe")).toString();
			old_denoEnVar = rev.getTCProperty(resource.getString("T4DenotationEn")).toString();
			old_indexVar = rev.getTCProperty(resource.getString("T4Index")).toString();


			tcClass = selectedPartRev.getTCClass().getClassName();

			if(selectedPartRev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || 
					tcClass.equals(resource.getString("CAEModelRevision")) || 
					tcClass.equals(resource.getString("CAEAnalysisRevision")) || 
					tcClass.equals(resource.getString("CAEGeometryRevision"))))
			{
				old_feaStage = selectedPartRev.getTCProperty(resource.getString("T4FEAModelStage")).toString();			
			}

			tcClass = selectedPartRev.getTCClass().getClassName();

			if(selectedPartRev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || 
					tcClass.equals(resource.getString("CAEModelRevision")) || 
					tcClass.equals(resource.getString("CAEAnalysisRevision")) || 
					tcClass.equals(resource.getString("CAEGeometryRevision"))))
			{				
				old_feaDev = selectedPartRev.getTCProperty(resource.getString("T4FEAModelVersion")).toString();
			}
		}
		catch(TCException tcException)
		{
			String s = tcException.getError();
			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, null,resource.getString("error.TITLE"), MessageBox.ERROR, true);
				messagebox.setVisible(true);
			}
			else
			{
				System.out.println(s);
			}
		}
		logger.info("oldIdentNumber = " + old_identnumber + ", oldName = " + old_name);
		initializeDialog(tcClass);
	}

	/**
	 * endOperation
	 */
	public void endOperation()
	{
		setVisible(false);
		dispose();
		setCursor(Cursor.getPredefinedCursor(0));

		boolean suc = false;  
		suc = renameOp.getSuccessFlag();

		session.setReadyStatus();		

		if (suc == true)
		{	
			System.out.println("Renaming Successful");			
		}
		else
		{
			MessageBox messagebox = new MessageBox(desktop,resource.getString("renameFailure.info"),resource.getString("info.TITLE"), MessageBox.INFORMATION);
			messagebox.setModal(true);
			messagebox.setVisible(true);
			System.out.println("Renaming Failure");
		}
	}

	/**
	 * @param tcClass String
	 */
	private void initializeDialog(String tcClass)
	{

		logger.info("Inside RenameDialogCAD::initializeDialog");

		// old identnumber
		old_identnumberTextField= new JTextField(32);
		old_identnumberTextField.setEnabled(false);
		//old_identnumberTextField.setHorizontalAlignment(JTextField.CENTER);


		// new identnumber at item level
		new_identnumberTextField= new ValidateTextField(resource.getString("allowedDigits"),null, 32,8);		
		new_identnumberTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		// new identnumber at revision level
		new_identnumberAtRevTextField= new JTextField(32);
		new_identnumberAtRevTextField.setEnabled(false);		

		// old name
		old_nameTextField = new JTextField(32);
		old_nameTextField.setEnabled(false);
		//old_nameTextField.setHorizontalAlignment(JTextField.CENTER);

		// new name
		new_nameTextField = new ValidateTextField(resource.getString("allowedChars"),null, 32,40);
		if(nameEditable == true)
			new_nameTextField.setEnabled(true);
		else	
			new_nameTextField.setEnabled(false);		
		new_nameTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});


		//old extension
		old_ident_extensionTextField=new JTextField(32);
		old_ident_extensionTextField.setEnabled(false);
		//old_ident_extensionTextField.setHorizontalAlignment(JTextField.CENTER);

		//new extension    
		new_ident_extensionTextField= new ValidateTextField(resource.getString("allowedCapitalChars"),null, 32,4);		
		new_ident_extensionTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		//old_ident_ext_counter
		old_ident_ext_counterTextField = new JTextField(32);
		old_ident_ext_counterTextField.setEnabled(false);
		//old_ident_ext_counterTextField.setHorizontalAlignment(JTextField.CENTER);

		//new_ident_ext_counter
		new_ident_ext_counterTextField = new ValidateTextField(resource.getString("allowedDigits"),null, 32,4);		
		new_ident_ext_counterTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		// old rev id
		old_revidTextField = new JTextField(32);
		old_revidTextField.setEnabled(false);
		//old_revidTextField.setHorizontalAlignment(JTextField.CENTER);

		// new rev id
		new_revidTextField = new ValidateTextField(resource.getString("allowedDigits"),null, 32,2);
		new_revidTextField.setEnabled(false);
		new_revidTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		// old identnumber
		old_denoDeAtItemTextField= new JTextField(32);
		old_denoDeAtItemTextField.setEnabled(false);
		//old_denoDeAtItemTextField.setHorizontalAlignment(JTextField.CENTER);

		// new deno DE
		new_denoDeAtItemTextField = new ValidateTextField(resource.getString("allowedChars"),null, 32,40);		
		new_denoDeAtItemTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e)
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});


		// old deno DE
		old_denoDeTextField = new JTextField(32);
		old_denoDeTextField.setEnabled(false);
		//old_denoDeTextField.setHorizontalAlignment(JTextField.CENTER);

		// new deno DE
		new_denoDeTextField = new ValidateTextField(resource.getString("allowedChars"),null, 32,40);		
		new_denoDeTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e)
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		// old deno EN
		old_denoEnTextField = new JTextField(32);
		old_denoEnTextField.setEnabled(false);
		//old_denoEnTextField.setHorizontalAlignment(JTextField.CENTER);

		// new deno EN
		new_denoEnTextField = new ValidateTextField(resource.getString("allowedChars"),null, 32,40);		
		new_denoEnTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		// old index
		old_indexTextField = new JTextField(32);
		old_indexTextField.setEnabled(false);
		//old_indexTextField.setHorizontalAlignment(JTextField.CENTER);

		// new index
		new_indexTextField = new ValidateTextField(resource.getString("allowedIndexes"),null, 32,1);		
		new_indexTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		// old FEA Stage
		old_feaStageTextField = new JTextField(32);
		old_feaStageTextField.setEnabled(false);
		if(tcClass!=null)
		{
			try 
			{
				tcClass = rev.getTCClass().getClassName();
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}

			if(rev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision"))))
			{				
				//old_feaStageTextField.setHorizontalAlignment(JTextField.CENTER);
			}
		}


		// new FEA Stage
		new_feaStageTextField = new ValidateTextField(resource.getString("allowedDigits"),null, 32,2);		
		new_feaStageTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		// old FEA DEV
		old_feaDevTextField = new JTextField(32);
		old_feaDevTextField.setEnabled(false);
		if(tcClass!=null)
		{
			try 
			{
				tcClass = rev.getTCClass().getClassName();
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}

			if(rev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision"))))
			{

				//old_feaDevTextField.setHorizontalAlignment(JTextField.CENTER);
			}
		}

		// new FEA DEV
		new_feaDevTextField = new ValidateTextField(resource.getString("allowedSmallChars"),null, 32,1);		
		new_feaDevTextField.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});


		// Rename Button definitions
		renameButton = new JButton(resource.getString("Rename"));
		renameButton.setEnabled(false);
		renameButton.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});

		renameButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent actionevent)
			{
				renameButton.setEnabled(false);

				int checkID 	 = 0;
				int doNotProceed = 0;

				// check to make sure that Item ID is unique
				if(
						!(new_identnumberTextField.getText().equalsIgnoreCase(old_identnumber)) 
						|| !(new_ident_extensionTextField.getText().equals(old_ident_extension)) 
						|| !(new_ident_ext_counterTextField.getText().equals(old_ident_ext_counter)
								|| !(new_denoDeAtItemTextField.getText().equals(old_denoDeVar)))
				)
				{
					try
					{
						TCComponentItemType itemType = (TCComponentItemType)session.getTypeComponent("Item");
						TCComponentItem  item = new TCComponentItem();

						item = itemType.find((new_identnumberTextField.getText() + "-" + new_ident_extensionTextField.getText() + new_ident_ext_counterTextField.getText()));

						if(item != null)
						{
							renameButton.setEnabled(false);
							MessageBox messagebox = new MessageBox(desktop, resource.getString("existingItem.error"), resource.getString("error.TITLE"), MessageBox.ERROR);
							messagebox.setModal(true);
							messagebox.setVisible(true);

							if(caseSensitivity.equalsIgnoreCase("UPPER"))
								new_identnumberTextField.setText(old_identnumber.toUpperCase());
							else if(caseSensitivity.equalsIgnoreCase("LOWER"))
								new_identnumberTextField.setText(old_identnumber.toLowerCase());
							else
								new_identnumberTextField.setText(old_identnumber);

							checkID = 1;
						}
					}
					catch(TCException tcException)
					{
						String s = tcException.getError();
						if (desktop != null)
						{
							MessageBox messagebox = new MessageBox(desktop, s, resource.getString("existingItem.error"),resource.getString("error.TITLE"), MessageBox.ERROR, true);
							messagebox.setVisible(true);
						}
						else
						{
							System.out.println(s);
						}
					}
				}
				doNotProceed = checkID;

				if(doNotProceed == 0)
					startCreateOperation();
			}
		});


		//Cancel Button Defintions
		cancelButton = new JButton(resource.getString("Cancel"));
		cancelButton.addKeyListener(new KeyListener() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			}

			@Override
			public void keyReleased(KeyEvent e) 
			{
				keyrelease(e);
			}

			@Override
			public void keyPressed(KeyEvent e) 
			{
			}
		});
		cancelButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent actionevent)
			{
				session.setReadyStatus();
				setVisible(false);

				dispose();
			}
		});

		setDefaultCloseOperation(0);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				cancelButton.doClick();
			}
		});


		if(rev != null)
			setTitle(resource.getString("rev.dialog.title"));

		else
			setTitle(resource.getString("item.dialog.title"));

		JPanel jpanel = new JPanel(new VerticalLayout(5, 2, 2, 2, 2));

		getContentPane().add(jpanel);

		JPanel buttonPanel = new JPanel(new ButtonLayout());
		JPanel verticalPanel = new JPanel(new VerticalLayout());
		JPanel fieldPanel = new JPanel(new PropertyLayout());
		JPanel propertyPanel = new JPanel(new PropertyLayout());
		JLabel emptyLabel1 = new JLabel("             ");
		JLabel existValLabel = new JLabel("                   " + resource.getString("ExistingValue")+ "                   ");
		existValLabel.setHorizontalAlignment(0);
		JLabel desiredValLabel = new JLabel("                   "+ resource.getString("DesiredValue") + "                   ");
		desiredValLabel.setHorizontalAlignment(0);
		JLabel identNoLabel = new JLabel(resource.getString("Identnumber"));
		JLabel extLabel = new JLabel(resource.getString("Extension"));
		JLabel counterLabel = new JLabel(resource.getString("Counter")); 
		JLabel itemNameLabel = new JLabel(resource.getString("ItemName"));
		JLabel revIdLabel = new JLabel(resource.getString("RevisionID"));
		JLabel denoDELabel = new JLabel(resource.getString("DENOTATIONDE")); 
		JLabel denoENLabel = new JLabel(resource.getString("DENOTATIONEN"));
		JLabel indexLabel = new JLabel(resource.getString("INDEX"));
		JLabel feaStageLabel = new JLabel(resource.getString("FEASTAGE"));
		JLabel feaVersionLabel = new JLabel(resource.getString("FEAVERSION"));


		if (rev == null)
		{
			// Add the Dialog components
			buttonPanel.add(renameButton);
			buttonPanel.add(cancelButton);
			fieldPanel.add("1.1.right.top", emptyLabel1);
			fieldPanel.add("1.2.right.top", existValLabel);
			fieldPanel.add("1.3.right.top", desiredValLabel);
			fieldPanel.add("2.1.right.top", identNoLabel);
			fieldPanel.add("2.2.right.top", old_identnumberTextField);
			fieldPanel.add("2.3.right.top", new_identnumberTextField);
			fieldPanel.add("3.1.right.top",extLabel);
			fieldPanel.add("3.2.right.top",old_ident_extensionTextField);
			fieldPanel.add("3.3.right.top",new_ident_extensionTextField);
			fieldPanel.add("4.1.right.top",counterLabel);
			fieldPanel.add("4.2.right.top",old_ident_ext_counterTextField);
			fieldPanel.add("4.3.right.top",new_ident_ext_counterTextField);
			fieldPanel.add("5.1.right.top", itemNameLabel);
			fieldPanel.add("5.2.right.top", old_nameTextField);
			fieldPanel.add("5.3.right.top", new_nameTextField);
			fieldPanel.add("6.1.right.top", denoDELabel);
			fieldPanel.add("6.2.right.top", old_denoDeAtItemTextField);
			fieldPanel.add("6.3.right.top", new_denoDeAtItemTextField);
		}

		if(rev != null)
		{
			// Add the Dialog components
			buttonPanel.add(renameButton);
			buttonPanel.add(cancelButton);
			fieldPanel.add("1.1.right.top", emptyLabel1);
			fieldPanel.add("1.2.right.top", existValLabel);
			fieldPanel.add("1.3.right.top", desiredValLabel);
			JLabel itemIDAtRevLabel = new JLabel(resource.getString("ItemIDAtRevLevel"));
			itemIDAtRevLabel.setToolTipText(resource.getString("ItemIDToolTip"));
			fieldPanel.add("2.1.right.top", itemIDAtRevLabel);
			fieldPanel.add("2.2.right.top", old_identnumberTextField);
			fieldPanel.add("2.3.right.top", new_identnumberAtRevTextField);
			fieldPanel.add("3.1.right.top", itemNameLabel);
			fieldPanel.add("3.2.right.top", old_nameTextField);
			fieldPanel.add("3.3.right.top", new_nameTextField);
			itemNameLabel.setText(resource.getString("RevName"));
			fieldPanel.add("4.1.right.top", revIdLabel);
			fieldPanel.add("4.2.right.top", old_revidTextField);
			fieldPanel.add("4.3.right.top", new_revidTextField);
			fieldPanel.add("5.1.right.top", denoDELabel);
			fieldPanel.add("5.2.right.top", old_denoDeTextField);
			fieldPanel.add("5.3.right.top", new_denoDeTextField);
			fieldPanel.add("6.1.right.top", denoENLabel);
			fieldPanel.add("6.2.right.top", old_denoEnTextField);
			fieldPanel.add("6.3.right.top", new_denoEnTextField);
			fieldPanel.add("7.1.right.top", indexLabel);
			fieldPanel.add("7.2.right.top", old_indexTextField);
			fieldPanel.add("7.3.right.top", new_indexTextField);

			if(rev!=null && tcClass.equals(resource.getString("T4FEARevision")) || 
					tcClass.equals(resource.getString("CAEModelRevision")) || 
					tcClass.equals(resource.getString("CAEAnalysisRevision")) || 
					tcClass.equals(resource.getString("CAEGeometryRevision")))
			{

				fieldPanel.add("8.1.right.top", feaStageLabel);
				fieldPanel.add("8.2.right.top", old_feaStageTextField);
				fieldPanel.add("8.3.right.top", new_feaStageTextField);

				fieldPanel.add("9.1.right.top", feaVersionLabel);
				fieldPanel.add("9.2.right.top", old_feaDevTextField);
				fieldPanel.add("9.3.right.top", new_feaDevTextField);
			}
		}

		// Add the Dialog components and pack
		jpanel.add("top.bind", verticalPanel);
		jpanel.add("top.bind", new JLabel(""));
		jpanel.add("top.bind", new JLabel(""));
		jpanel.add("top.bind", fieldPanel);
		jpanel.add("top.bind.resizable.resizable",propertyPanel);

		jpanel.add("bottom.bind.center.top", buttonPanel);
		jpanel.add("bottom.bind", new Separator());
		pack();
		setModal(true);
		centerToScreen(1.0D, 1.0D);
		startLoadOperation();
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher
		(
				new KeyEventDispatcher() 
				{  
					public boolean dispatchKeyEvent(KeyEvent e) 
					{  
						boolean keyHandled = false;  
						if (e.getID() == KeyEvent.KEY_PRESSED) 
						{  
							if (e.getKeyCode() == KeyEvent.VK_ESCAPE) 
							{  
								session.setReadyStatus();
								setVisible(false);
								dispose();
								keyHandled = true;  
							}  
						}  
						return keyHandled;  
					}
				}
		); 
	}

	/* (non-Javadoc)
	 * @see com.teamcenter.rac.aif.AbstractAIFDialog#run()
	 */
	public void run(){

		logger.info("Inside RenameDialogCAD::run");	  
		setVisible(true);

	}

	/**
	 * startCreateOperation
	 */
	private void startCreateOperation(){
		logger.info("Inside RenameDialogCAD::startCreateOperation");

		if(desktop == null)
			logger.info("desktop is NULL at RenameDialogCAD::RenameOperationCAD constructor call!!!");



		renameOp = new RenameOperationCAD(session, desktop, getOldIdentNumber(), getNewIdentNumber(),getOldIdentExtension(),getNewIdentExtension(),
				getOldIdentExtCounter(),getNewIdentExtCounter(),getOldName(), getNewName(), getOldRevID(), getNewRevID(), 
				getOldDENODEAtItemContext(), getNewDENODEAtItemContext(),getOldDENODE(),getNewDENODE(), getOldDENOEN(), getNewDENOEN(), getOldIndex(), getNewIndex(), getOldfeaStage(), getNewfeaStage(),
				getOldfeaDev(), getNewfeaDev(), item, rev, attachedObjects);


		try
		{
			session.setStatus(resource.getString("renamingOperation"));
			setVisible(false);
			dispose();
			setCursor(Cursor.getPredefinedCursor(0));
			renameOp.execute();	
			endOperation();
		}
		catch(Exception exception)
		{
			String s = exception.getMessage();
			if (desktop != null)
			{
				MessageBox messagebox = new MessageBox(desktop, s, "Error in StartCreateOperation",resource.getString("error.TITLE"), MessageBox.ERROR, true);
				messagebox.setVisible(true);
			}
			else
			{
				logger.info(s);
			}
		}	  
	}

	/**
	 * startLoadOperation
	 */
	private void startLoadOperation()
	{
		logger.info("Inside RenameDialogCAD::startLoadOperation");
		loadOp = new LoadOperation();
		session.queueOperation(loadOp);
	}

	/**
	 * validateFields
	 */
	private void validateFields()
	{
		logger.info("Inside validateFields");
		String testString = null;

		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_identnumberTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_identnumberTextField.getText().toLowerCase();
		else
			testString = new_identnumberTextField.getText();
		if (! (new_identnumberTextField.getText().equals(testString)))
			new_identnumberTextField.setText(testString);


		if(rev == null)
		{
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_nameTextField.getText().toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString = new_nameTextField.getText().toLowerCase();
			else
				testString = new_nameTextField.getText();

			if (! (new_nameTextField.getText().equals(testString)))
				new_nameTextField.setText(testString);

			new_nameTextField.setBackground(Color.WHITE);

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_ident_extensionTextField.getText().toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString =  new_ident_extensionTextField.getText().toLowerCase();
			else
				testString =  new_ident_extensionTextField.getText();

			if (! ( new_ident_extensionTextField.getText().equals(testString)))
				new_ident_extensionTextField.setText(testString);

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_ident_ext_counterTextField.getText().toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString =  new_ident_ext_counterTextField.getText().toLowerCase();
			else
				testString =  new_ident_ext_counterTextField.getText();

			if (! ( new_ident_ext_counterTextField.getText().equals(testString)))
				new_ident_ext_counterTextField.setText(testString);

		}

		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_revidTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_revidTextField.getText().toLowerCase();
		else
			testString = new_revidTextField.getText();

		if(!(new_revidTextField.getText().equals(testString)))
			new_revidTextField.setText(testString);


		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_denoDeTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_denoDeTextField.getText().toLowerCase();
		else
			testString = new_denoDeTextField.getText();

		if(!(new_denoDeTextField.getText().equals(testString)))
			new_denoDeTextField.setText(testString);

		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_denoEnTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_denoEnTextField.getText().toLowerCase();
		else
			testString = new_denoEnTextField.getText();

		if(!(new_denoEnTextField.getText().equals(testString)))
			new_denoEnTextField.setText(testString);

		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_indexTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_indexTextField.getText().toLowerCase();
		else
			testString = new_indexTextField.getText();

		if(!(new_indexTextField.getText().equals(testString)))
			new_indexTextField.setText(testString);

		if(tcClass!=null)
		{
			try {
				tcClass = rev.getTCClass().getClassName();
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(rev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || 
					tcClass.equals(resource.getString("CAEModelRevision")) || 
					tcClass.equals(resource.getString("CAEAnalysisRevision")) || 
					tcClass.equals(resource.getString("CAEGeometryRevision"))))
			{

				if(caseSensitivity.equalsIgnoreCase("UPPER"))
					testString = new_feaStageTextField.getText().toUpperCase();
				else if(caseSensitivity.equalsIgnoreCase("LOWER"))
					testString = new_feaStageTextField.getText().toLowerCase();
				else
					testString = new_feaStageTextField.getText();

				if(!(new_feaStageTextField.getText().equals(testString)))
					new_feaStageTextField.setText(testString);

			}
		}

		if(tcClass!=null)
		{
			try 
			{
				tcClass = rev.getTCClass().getClassName();
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}

			if(rev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision"))))
			{
				if(caseSensitivity.equalsIgnoreCase("UPPER"))
					testString = new_feaDevTextField.getText().toLowerCase();
				else if(caseSensitivity.equalsIgnoreCase("LOWER"))
					testString = new_feaDevTextField.getText().toUpperCase();
				else
					testString = new_feaDevTextField.getText();

				if(!(new_feaDevTextField.getText().equals(testString)))
					new_feaDevTextField.setText(testString);
			}
		}


		renameButton.setEnabled(false);
		boolean emptyValue = false;
		int change = 0;

		old_identnumberTextField.setEnabled(false);		
		old_nameTextField.setEnabled(false);		
		old_revidTextField.setEnabled(false);		
		old_denoDeTextField.setEnabled(false);		
		old_denoEnTextField.setEnabled(false);		
		old_indexTextField.setEnabled(false);

		if(tcClass!=null)
		{
			try 
			{
				tcClass = rev.getTCClass().getClassName();
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}

			if(rev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision"))))
			{
				old_feaStageTextField.setEnabled(false);				
				old_feaDevTextField.setEnabled(false);
			}
		}

		if(old_identnumberTextField.isEditable())

			old_identnumberTextField.setBackground(Color.WHITE);

		if(old_nameTextField.isEditable())

			old_nameTextField.setBackground(Color.WHITE);

		if(old_denoDeAtItemTextField.isEditable())

			old_denoDeAtItemTextField.setBackground(Color.WHITE);

		if(old_ident_extensionTextField.isEditable())

			old_ident_extensionTextField.setBackground(Color.WHITE);

		if(old_ident_ext_counterTextField.isEditable())

			old_ident_ext_counterTextField.setBackground(Color.WHITE);

		if(old_nameTextField.isEditable())

			old_nameTextField.setBackground(Color.WHITE);

		if(old_denoDeTextField.isEditable())

			old_denoDeTextField.setBackground(Color.WHITE);

		if(old_denoEnTextField.isEditable())

			old_denoEnTextField.setBackground(Color.WHITE);

		if(old_indexTextField.isEditable())

			old_indexTextField.setBackground(Color.WHITE);

		if(old_feaDevTextField.isEditable())

			old_feaDevTextField.setBackground(Color.WHITE);

		if(old_feaStageTextField.isEditable())

			old_feaStageTextField.setBackground(Color.WHITE);

		if(old_revidTextField.isEditable())

			old_revidTextField.setBackground(Color.WHITE);

		if(new_identnumberTextField.isEditable())

			new_identnumberTextField.setBackground(Color.WHITE);

		if(rev!=null)
		{
			new_nameTextField.setEnabled(false);
			if(new_nameTextField.isEditable())

				new_nameTextField.setBackground(Color.WHITE);
		}

		else
		{
			if(nameEditable == true)
				new_nameTextField.setEnabled(true);
			else	
				new_nameTextField.setEnabled(false);

			if(new_nameTextField.isEditable())

				new_nameTextField.setBackground(Color.WHITE);
		}

		if(new_revidTextField.isEditable())

			new_revidTextField.setBackground(Color.WHITE);

		if(new_identnumberAtRevTextField.isEditable())

			new_identnumberAtRevTextField.setBackground(Color.WHITE);

		if(new_ident_extensionTextField.isEditable())

			new_ident_extensionTextField.setBackground(Color.WHITE);

		if(new_ident_ext_counterTextField.isEditable())

			new_ident_ext_counterTextField.setBackground(Color.WHITE);

		if(new_denoDeTextField.isEditable())

			new_denoDeTextField.setBackground(Color.WHITE);

		if(new_denoDeAtItemTextField.isEditable())

			new_denoDeAtItemTextField.setBackground(Color.WHITE);

		if(new_denoEnTextField.isEditable())

			new_denoEnTextField.setBackground(Color.WHITE);

		if(new_indexTextField.isEditable())

			new_indexTextField.setBackground(Color.WHITE);

		if(new_feaStageTextField.isEditable())

			new_feaStageTextField.setBackground(Color.WHITE);

		if(new_feaDevTextField.isEditable())

			new_feaDevTextField.setBackground(Color.WHITE);

		if(rev == null)
		{
			if((new_identnumberTextField.getText() == null) || new_identnumberTextField.getText().length() < 8)
				emptyValue = true;

			if((!(new_identnumberTextField.getText().equals(old_identnumber))))

			{
				new_identnumberTextField.setBackground(Color.YELLOW);
				change++;

				// JBH2 - auto upper case for Identnumber field
				if(caseSensitivity.equalsIgnoreCase("UPPER"))
					testString = new_identnumberTextField.getText().toUpperCase();
				else if(caseSensitivity.equalsIgnoreCase("LOWER"))
					testString = new_identnumberTextField.getText().toLowerCase();
				else
					testString = new_identnumberTextField.getText();

				if (! (new_identnumberTextField.getText().equals(testString)))
				{
					new_identnumberTextField.setText(testString);
					if(new_identnumberTextField.getText().equals(old_identnumber))
					{
						change--;
						new_identnumberTextField.setBackground(Color.WHITE);
					}
				}
				// JBH2
			}
		}

		if(tcClass!=null)
		{
			try 
			{
				tcClass = rev.getTCClass().getClassName();
			} 
			catch (TCException e) 
			{				
				e.printStackTrace();
			}
			if(rev != null && (tcClass.equals("T4_ArticleRevision") || tcClass.equals("T4_Standard_PartRevision") ))

			{
				if((new_identnumberAtRevTextField.getText() == null) || new_identnumberAtRevTextField.getText().length() < 8)
					emptyValue = true;

				if((!(new_identnumberAtRevTextField.getText().equals(old_identnumber))))

				{			
					change++;					
				}

			}
		}


		if(rev != null)
		{
			if((new_identnumberAtRevTextField.getText() == null) || new_identnumberAtRevTextField.getText().length() < 8)
				emptyValue = true;

			if((!(new_identnumberAtRevTextField.getText().equals(old_identnumber))))

			{			
				change++;

			}
		}		

		if(rev == null)
		{
			if((new_nameTextField.getText() == null) || new_nameTextField.getText().length() == 0)
				emptyValue = true;

			if((! (new_nameTextField.getText().equalsIgnoreCase(old_name))))
			{	
				new_nameTextField.setBackground(Color.YELLOW);
				change++;
			}

		}
		// JBH2 - auto upper case for Name field
		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_nameTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_nameTextField.getText().toLowerCase();
		else
			testString = new_nameTextField.getText();

		if(!(new_nameTextField.getText().equals(testString)))
		{
			new_nameTextField.setText(testString);
			if(new_nameTextField.getText().equals(old_name))
			{
				change--;
				new_nameTextField.setBackground(Color.WHITE);
			}
		}

		if(rev != null)
		{
			if((new_nameTextField.getText() == null) || new_nameTextField.getText().length() == 0)
				emptyValue = true;

			if((! (new_nameTextField.getText().equals(old_name))))
			{				
				change++;
			}
		}

		// JBH2

		if(rev == null)
		{
			if((new_ident_extensionTextField.getText() == null) || new_ident_extensionTextField.getText().length() < 2)
				emptyValue = true;

			if((! (new_ident_extensionTextField.getText().equals(old_ident_extension))))
			{	
				new_ident_extensionTextField.setBackground(Color.YELLOW);
				change++;
			}

			// JBH2 - auto upper case for Name field
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_ident_extensionTextField.getText().toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString = new_ident_extensionTextField.getText().toLowerCase();
			else
				testString = new_ident_extensionTextField.getText();

			if(!(new_ident_extensionTextField.getText().equals(testString)))
			{
				new_ident_extensionTextField.setText(testString);
				if(new_ident_extensionTextField.getText().equals(old_ident_extension))
				{
					change--;
					new_ident_extensionTextField.setBackground(Color.WHITE);
				}
			}
		}

		if(rev == null)
		{
			if((new_ident_ext_counterTextField.getText() == null) || new_ident_ext_counterTextField.getText().length() == 0)
				emptyValue = true;

			if((! (new_ident_ext_counterTextField.getText().equals(old_ident_ext_counter))))
			{	
				new_ident_ext_counterTextField.setBackground(Color.YELLOW);
				change++;
			}
		}

		// JBH2 - auto upper case for Name field
		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_ident_ext_counterTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_ident_ext_counterTextField.getText().toLowerCase();
		else
			testString = new_ident_ext_counterTextField.getText();

		if(!(new_ident_ext_counterTextField.getText().equals(testString)))
		{
			new_ident_ext_counterTextField.setText(testString);
			if(new_ident_ext_counterTextField.getText().equals(old_ident_ext_counter))
			{
				change--;
				new_ident_ext_counterTextField.setBackground(Color.WHITE);
			}
		}

		if(rev != null )
		{
			if((new_revidTextField.getText() == null) || new_revidTextField.getText().length() == 0)
				emptyValue = true;

			if((!(new_revidTextField.getText().equals(old_revid))))

			{
				change++;

				// JBH2 - auto upper case for Rev ID field
				if(caseSensitivity.equalsIgnoreCase("UPPER"))
					testString = new_revidTextField.getText().toUpperCase();
				else if(caseSensitivity.equalsIgnoreCase("LOWER"))
					testString = new_revidTextField.getText().toLowerCase();
				else
					testString = new_revidTextField.getText();

				if(!(new_revidTextField.getText().equals(testString)))

				{
					new_revidTextField.setText(testString);
					if(new_revidTextField.getText().equals(old_revid))
					{
						change--;
						new_revidTextField.setBackground(Color.WHITE);
					}
				}
			}
		}		

		if(rev == null)
		{
			if((new_denoDeAtItemTextField.getText() == null) || new_denoDeAtItemTextField.getText().length() == 0)
				emptyValue = true;			

			if((!(new_denoDeAtItemTextField.getText().equals(old_denoDeVar))))

			{	
				new_denoDeAtItemTextField.setBackground(Color.YELLOW);
				change++;
			}
		}


		if(rev != null)

		{
			if((new_denoDeTextField.getText() == null) || new_denoDeTextField.getText().length() == 0)
				emptyValue = true;

			if((!(new_denoDeTextField.getText().equals(old_denoDeVar))))

			{
				new_denoDeTextField.setBackground(Color.YELLOW);
				change++;
			}

		}

		// JBH2 - auto upper case for Rev ID field
		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_denoDeTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_denoDeTextField.getText().toLowerCase();
		else
			testString = new_denoDeTextField.getText();

		if(!(new_denoDeTextField.getText().equals(testString)))

		{
			new_denoDeTextField.setText(testString);
			if(new_denoDeTextField.getText().equals(old_denoDeVar))
			{
				change--;
				new_denoDeTextField.setBackground(Color.WHITE);
			}
		}		

		if(rev != null )

		{
			if((new_denoEnTextField.getText() == null) || new_denoEnTextField.getText().length() == 0)
				emptyValue = true;

			if((!(new_denoEnTextField.getText().equals(old_denoEnVar))))

			{
				new_denoEnTextField.setBackground(Color.YELLOW);
				change++;
			}

		}

		// JBH2 - auto upper case for Rev ID field
		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_denoEnTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_denoEnTextField.getText().toLowerCase();
		else
			testString = new_denoEnTextField.getText();

		if(!(new_denoEnTextField.getText().equals(testString)))

		{
			new_denoEnTextField.setText(testString);
			if(new_denoEnTextField.getText().equals(old_denoEnVar))
			{
				change--;
				new_denoEnTextField.setBackground(Color.WHITE);
			}
		}		

		if(rev != null)

		{
			if((new_indexTextField.getText() == null) || new_indexTextField.getText().length() == 0)
				emptyValue = true;

			if((!(new_indexTextField.getText().equals(old_indexVar))))

			{
				new_indexTextField.setBackground(Color.YELLOW);
				change++;
			}

		}

		// JBH2 - auto upper case for Rev ID field
		if(caseSensitivity.equalsIgnoreCase("UPPER"))
			testString = new_indexTextField.getText().toUpperCase();
		else if(caseSensitivity.equalsIgnoreCase("LOWER"))
			testString = new_indexTextField.getText().toLowerCase();
		else
			testString = new_indexTextField.getText();

		if(!(new_indexTextField.getText().equals(testString)))

		{
			new_indexTextField.setText(testString);
			if(new_indexTextField.getText().equals(old_indexVar))
			{
				change--;
				new_indexTextField.setBackground(Color.WHITE);
			}
		}

		if(tcClass!=null)
		{
			try {
				tcClass = rev.getTCClass().getClassName();
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(rev != null && (tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || 
					tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision"))))

			{
				if((new_feaStageTextField.getText() == null) || new_feaStageTextField.getText().length() < 1)
					emptyValue = true;

				if((!(new_feaStageTextField.getText().equals(old_feaStage))))

				{
					new_feaStageTextField.setBackground(Color.YELLOW);
					change++;
				}
			}

			// JBH2 - auto upper case for Rev ID field
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_feaStageTextField.getText().toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString = new_feaStageTextField.getText().toLowerCase();
			else
				testString = new_feaStageTextField.getText();

			if(!(new_feaStageTextField.getText().equals(testString)))

			{
				new_feaStageTextField.setText(testString);
				if(new_feaStageTextField.getText().equals(old_feaStage))
				{
					change--;
					new_feaStageTextField.setBackground(Color.WHITE);
				}
			}
		}

		if(tcClass!=null)
		{
			try {
				tcClass = rev.getTCClass().getClassName();
			} catch (TCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(rev != null && (tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || 
					tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision"))))

			{
				if((new_feaDevTextField.getText() == null) || new_feaDevTextField.getText().length() == 0)
					emptyValue = true;

				if((!(new_feaDevTextField.getText().equals(old_feaDev))))

				{
					new_feaDevTextField.setBackground(Color.YELLOW);
					change++;
				}
			}

			// JBH2 - auto upper case for Rev ID field
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				testString = new_feaDevTextField.getText().toLowerCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				testString = new_feaDevTextField.getText().toUpperCase();
			else
				testString = new_feaDevTextField.getText();

			if(!(new_feaDevTextField.getText().equals(testString)))

			{
				new_feaDevTextField.setText(testString);
				if(new_feaDevTextField.getText().equals(old_feaDev))
				{
					change--;
					new_feaDevTextField.setBackground(Color.WHITE);
				}
			}
		}

		//Adding check to distinguish rename button be enable Or, disable
		if((change != 0) && (emptyValue == false))
		{
			renameButton.setEnabled(true);
			renameButton.setFocusable(true);
		}
		else
			renameButton.setEnabled(false);
	}

	/**
	 * populateFields
	 */
	private void populateFields(){
		logger.info("Inside populateFields");		

		if(rev == null)
		{
			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_identnumber = old_identnumber.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_identnumber = old_identnumber.toLowerCase();
			else
				new_identnumber = old_identnumber;			

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_ident_extension = old_ident_extension.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_ident_extension = old_ident_extension.toLowerCase();
			else
				new_ident_extension = old_ident_extension;

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_ident_ext_counter = old_ident_ext_counter.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_ident_ext_counter = old_ident_ext_counter.toLowerCase();
			else
				new_ident_ext_counter = old_ident_ext_counter;

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_name = old_name.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_name = old_name.toLowerCase();
			else
				new_name = old_name;

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_denoDe = old_denoDeVar.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_denoDe = old_denoDeVar.toLowerCase();
			else
				new_denoDe = old_denoDeVar;
		}		

		if(rev != null)
		{			
			new_identnumberAtRevTextField.setEnabled(false);					
			new_nameTextField.setEnabled(false);
			new_revidTextField.setEnabled(false);

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_name = old_name.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_name = old_name.toLowerCase();
			else
				new_name = old_name;

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_revid = old_revid.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_revid = old_revid.toLowerCase();
			else
				new_revid = old_revid;

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_denoDe = old_denoDeVar.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_denoDe = old_denoDeVar.toLowerCase();
			else
				new_denoDe = old_denoDeVar;

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_denoEn = old_denoEnVar.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_denoEn = old_denoEnVar.toLowerCase();
			else
				new_denoEn = old_denoEnVar;

			if(caseSensitivity.equalsIgnoreCase("UPPER"))
				new_index = old_indexVar.toUpperCase();
			else if(caseSensitivity.equalsIgnoreCase("LOWER"))
				new_index = old_indexVar.toLowerCase();
			else
				new_index = old_indexVar;

			if(tcClass!=null)
			{
				try 
				{
					tcClass = rev.getTCClass().getClassName();
				} catch (TCException e) 
				{					
					e.printStackTrace();
				}

				if(rev!=null && tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || 
						tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision")))
				{
					if(caseSensitivity.equalsIgnoreCase("UPPER"))
						new_feaStage = old_feaStage.toUpperCase();
					else if(caseSensitivity.equalsIgnoreCase("LOWER"))
						new_feaStage = old_feaStage.toLowerCase();
					else
						new_feaStage = old_feaStage;
				}
			}

			if(tcClass!=null)
			{
				try 
				{
					tcClass = rev.getTCClass().getClassName();
				} 
				catch (TCException e)
				{					
					e.printStackTrace();
				}

				if(rev!=null && tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || 
						tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision")))
				{
					if(caseSensitivity.equalsIgnoreCase("UPPER"))
						new_feaDev = old_feaDev.toUpperCase();
					else if(caseSensitivity.equalsIgnoreCase("LOWER"))
						new_feaDev = old_feaDev.toLowerCase();
					else
						new_feaDev = old_feaDev;

				}
			}

			try 
			{
				tcClass = rev.getTCClass().getClassName();
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}

			if(rev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || 
					tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision"))))
			{

				old_feaStageTextField.setEnabled(false);				
				old_feaDevTextField.setEnabled(false);
			}
		}

		old_identnumberTextField.setEnabled(false);		
		old_nameTextField.setEnabled(false);		
		old_revidTextField.setEnabled(false);		
		old_denoDeTextField.setEnabled(false);		
		old_denoEnTextField.setEnabled(false);		
		old_indexTextField.setEnabled(false);

		if(tcClass!=null)
		{
			try 
			{
				tcClass = rev.getTCClass().getClassName();
			} 
			catch (TCException e) 
			{
				e.printStackTrace();
			}

			if(rev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision"))))
			{
				old_feaStageTextField.setEnabled(false);
				old_feaDevTextField.setEnabled(false);
			}
		}

		// Places the info into the fields
		old_identnumberTextField.setText(old_identnumber);

		if(tcClass!=null)
		{
			try {
				tcClass = rev.getTCClass().getClassName();
			} catch (TCException e) {

				e.printStackTrace();
			}

			if(rev!=null && (tcClass.equals("T4_ArticleRevision") || tcClass.equals("T4_Standard_PartRevision") ))
			{
				new_identnumberAtRevTextField.setText(newRevStr);
			}

			else if (rev != null)
			{
				new_identnumberAtRevTextField.setText(newRevStr);
			}				
		}
		if (rev == null)
		{
			new_identnumberTextField.setText(new_identnumber);
			old_ident_extensionTextField.setText(old_ident_extension);
			old_ident_ext_counterTextField.setText(old_ident_ext_counter);
			new_ident_extensionTextField.setText(new_ident_extension);
			new_ident_ext_counterTextField.setText(new_ident_ext_counter);
			old_nameTextField.setText(old_name);
			new_nameTextField.setText(new_name);
			old_denoDeAtItemTextField.setText(old_denoDeVar);
			new_denoDeAtItemTextField.setText(new_denoDe);

		}
		else if (rev != null)
		{
			old_nameTextField.setText(old_name);
			new_nameTextField.setText(new_name);
			old_revidTextField.setText(old_revid);
			new_revidTextField.setText(new_revid);		
			old_denoDeTextField.setText(old_denoDeVar);
			new_denoDeTextField.setText(new_denoDe);
			old_denoEnTextField.setText(old_denoEnVar);
			new_denoEnTextField.setText(new_denoEn);
			old_indexTextField.setText(old_indexVar);
			new_indexTextField.setText(new_index);
			if(tcClass!=null)
			{
				try 
				{
					tcClass = rev.getTCClass().getClassName();
				} 
				catch (TCException e) 
				{
					e.printStackTrace();
				}

				if(rev!=null && (tcClass.equals(resource.getString("T4FEARevision")) || tcClass.equals(resource.getString("CAEModelRevision")) || tcClass.equals(resource.getString("CAEAnalysisRevision")) || tcClass.equals(resource.getString("CAEGeometryRevision"))))
				{
					old_feaStageTextField.setText(old_feaStage);
					new_feaStageTextField.setText(new_feaStage);
					old_feaDevTextField.setText(old_feaDev);
					new_feaDevTextField.setText(new_feaDev);
				}
			}
		}

		validateFields();
		setVisible(true);
	} // End of PopulateFields Class

	/**
	 * @author udwivedi
	 *
	 */
	private class LoadOperation extends AbstractAIFOperation{

		public void executeOperation(){

			logger.info("Inside RenameDialogCAD::LoadOperation::executeOperation");
			populateFields();
		}

		LoadOperation()
		{
		}

	}//End of LoadOperation Class

	/**
	 * @param e keyrelease
	 */
	public void keyrelease(KeyEvent e)
	{
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) 
		{  
			session.setReadyStatus();
			setVisible(false);
			dispose();
			return ;
		}
		validateFields();
	}
}//end of class