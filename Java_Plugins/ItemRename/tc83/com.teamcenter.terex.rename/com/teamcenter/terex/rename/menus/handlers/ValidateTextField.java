/*=============================================================================

    Filename    :   ValidateTextField.java
    Module      :   com.teamcenter.terex.rename.menus.handlers
    Description :   This file is used to Validate the authenticated user inputs against each 
                    Textfield meant for various Terex Renaming operations.

================================================================================
                     Modification Log
================================================================================
Date            User                Company    Change Description
10-Jul-2011     Umesh Dwivedi       PRION      Initial Version

==============================================================================*/
package com.teamcenter.terex.rename.menus.handlers;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 * @author udwivedi
 *
 */
public class ValidateTextField extends JTextField 
{
	/**
	 * long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * String allowedChars
	 */
	String allowedChars = null;

	/**
	 * String notAllowedChars
	 */
	String notAllowedChars = null;

	/**
	 * int noOfChars
	 */
	int noOfChars = 1;

	/**
	 * @param allowedChars String
	 * @param notAllowedChars String
	 * @param size int
	 * @param noOfChars int
	 */
	public ValidateTextField(String allowedChars,String notAllowedChars, int size, int noOfChars) {
		super("" , size);
		this.allowedChars = allowedChars ;
		this.notAllowedChars = notAllowedChars;
		this.noOfChars  = noOfChars;
	}

	/** (non-Javadoc)
	 * @see javax.swing.JTextField#createDefaultModel()
	 */
	protected Document createDefaultModel() 
	{ 
		return new TextDocument();
	}

	/** (non-Javadoc)
	 * @see java.awt.Component#isValid()
	 */
	public boolean isValid() 
	{
		return true;
	}

	/**
	 * @return int
	 */
	public int getValue() {
		try {
			return Integer.parseInt(getText());
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * @author udwivedi
	 *
	 */
	class TextDocument extends PlainDocument 
	{
		/**
		 *  long serialVersionUID
		 */
		private static final long serialVersionUID = 1L;

		/** (non-Javadoc)
		 * @see javax.swing.text.PlainDocument#insertString(int, java.lang.String, javax.swing.text.AttributeSet)
		 */
		public void insertString(int offs, String substr, AttributeSet a)
		throws BadLocationException {

			if (substr == null)
			{
				return;
			}

			String oldString = getText(0, getLength());
			boolean isValidStr = true ;

			for(int i=0 ; isValidStr == true && i < substr.length() ; i++)
			{
				char str = substr.charAt(i);

				if( !((allowedChars != null && allowedChars.indexOf(str) > -1 &&  oldString.length() < noOfChars) ))
				{if(allowedChars != null)
					isValidStr = false;
				}

				if( !(notAllowedChars != null && notAllowedChars.indexOf(str) == -1 &&  oldString.length() < noOfChars))
				{if(notAllowedChars != null)
					isValidStr = false;
				}				

			}

			if(isValidStr)
			{				
				super.insertString(offs, substr, a);
			}
		}
	}	
}