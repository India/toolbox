/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_ownership_main.cxx

    Description: This File contains functions that are entry point to Change Ownership executable

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28-Aug-13      Tarun Kumar Singh   Initial Release
========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_library.hxx>
#include <sma3_library.hxx>
#include <sma3_const.hxx>
#include <sma3_errors.hxx>
#include <sma3_change_ownership.hxx>


int SMA3_init_ITK_login(int argc, char **argv);

void SMA3_display_usage();

/**
 * This function is the main function where program starts execution. It is responsible for the high-level organization of the
 * program's functionality, and try to access command arguments which basically are login credentials when this executable is run.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int ITK_user_main( int iArgCount, char** ppszArgVal)
{
    int iRetCode   = ITK_ok;

    /* display help if user asked for it */
	if ((ITK_ask_cli_argument("-h")) || (iArgCount == 0) )
    {
        SMA3_display_usage();
        exit(0);
    }

    char* pszLogFile = ITK_ask_cli_argument( "-log_file=" );
    /* validating the log file */ 
    if((pszLogFile == NULL) || (tc_strlen(pszLogFile) == 0))
    {
        fprintf(stderr,"\n\nError: Log file not specified.\n");
        SMA3_display_usage();
        exit(0);
    }

    char* pszRepeatFileInfo  = ITK_ask_cli_argument( "-repeat_file=");

    /* validating repeat file */ 
    if((pszRepeatFileInfo == NULL) || (tc_strlen(pszRepeatFileInfo) == 0))
    {
        fprintf(stderr,"\n\nError: Repeat File name not specified.\n");
        SMA3_display_usage();
        exit(0);
    }

    char* pszConfigFileInfo  = ITK_ask_cli_argument( "-input_file=");

    /* validating input file */ 
    if((pszConfigFileInfo == NULL) || (tc_strlen(pszConfigFileInfo) == 0))
    {
        fprintf(stderr,"\n\nError: File name not specified.\n");
        SMA3_display_usage();
        exit(0);
    }

    char* pszSeparator  = ITK_ask_cli_argument( "-sep=");

    /* validating Separator */ 
    if((pszSeparator == NULL) || (tc_strlen(pszSeparator) == 0))
    {
        pszSeparator = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(SMA3_STRING_DEFAULT_SEPARATOR) +1)));
        tc_strcpy(pszSeparator, SMA3_STRING_DEFAULT_SEPARATOR);
    }

	char* pszClassName  = ITK_ask_cli_argument( "-class=");

    /* validating Class Name */ 
    if((pszClassName == NULL) || (tc_strlen(pszClassName) == 0))
    {
        fprintf(stderr,"\n\nError: Class name not specified. Change Ownership will be applied to instances of this class.\n");
        SMA3_display_usage();
        exit(0);
    }

    SMA3_ITKCALL( iRetCode, SMA3_init_ITK_login(iArgCount, ppszArgVal));

    if(iRetCode != ITK_ok)
    {
        char* pszErrorText = NULL;
        EMH_ask_error_text(iRetCode, &pszErrorText);
        printf("Login Unsuccessful. %s \n", pszErrorText);
        SMA3_MEM_TCFREE( pszErrorText );
    }
    else
    {
        printf("Successfully Logged In Teamcenter \n");
    }

    if(iRetCode == ITK_ok)
    {                           
        SMA3_ITKCALL( iRetCode, SMA3_perform_change_ownership(pszConfigFileInfo, pszLogFile, pszRepeatFileInfo, pszSeparator, pszClassName));
    }

	SMA3_ITKCALL( iRetCode, ITK_exit_module(FALSE));

    return iRetCode;
}

/**
 * This function retrieves login credentials and try to login to Teamcenter with these credential. If not supplied executable
 * performs auto login.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_init_ITK_login(int argc, char **argv)
{
    int   iRetCode        = ITK_ok;

    /* Gets the user ID */
    char* pszUser  = ITK_ask_cli_argument( "-u=");

    /* Gets the user Password */
    char* pszPassword  = ITK_ask_cli_argument( "-p=");

    /* Gets the user group */
    char* pszGroup  = ITK_ask_cli_argument( "-g=");

    /*  To run against motif libs.*/
    SMA3_ITKCALL(iRetCode, ITK_initialize_text_services(ITK_BATCH_TEXT_MODE));

    /*  Put -h CLI argument here to avoid starting the whole of iman just for a help message! */
    if( ITK_ask_cli_argument( "-h" ) != 0 )
    {
        SMA3_display_usage();
        return iRetCode;
    }

	if(pszGroup == NULL || pszUser == NULL)
	{
		SMA3_ITKCALL( iRetCode, ITK_auto_login());		
	}
	else
	{
		SMA3_ITKCALL( iRetCode, ITK_init_module(pszUser, pszPassword, pszGroup));		
	}

    return iRetCode;
}

/**
 * This function displays the usage of this Executable
 */
void SMA3_display_usage()
{
    printf("     \n\t\t\n Security Utility Usage \n");
    printf("     \t\t\n-------------------------------\n");
    printf("           -u                   = <user name> to specify user name\n");
    printf("           -p                   = <password> to specify password\n");
    printf("           -g                   = <group name> to specify group name\n");
    printf("           -log_file            = log_file: Specifies the log file.\n\t\t\t\t  Information about the object will be written\n\t\t\t\t  to the log file.\n");
    printf("           -repeat_file         = repeat_file: Specifies the repeat file.\n\t\t\t\t  All non-processed entries from the input file\n\t\t\t\t  have to be written to the repeat file.\n");
    printf("           -input_file          = input_file: Specifies the input file to be\n\t\t\t\t  processed. It has the format\n\t\t\t\t  ItemID~AttributeKey~AttributeValue.\n\t\t\t\t  Commented lines start with \"#\". \n\t\t\t\t  These lines will be ignored while processing.\n\t\t\t\t  Following AttributeKey's are supported \n\t\t\t\t\tProject\n\t\t\t\t\tLicense\n\t\t\t\t\tIPClassification\n\t\t\t\t\tOwningProject\n"); 
    printf("           -sep                 = Specifies the separator to be\n\t\t\t\t  used in the input file. If not specified the\n\t\t\t\t  default will be \"~\".\n");
    printf("           -class               = Specifies Teamcenter class name.\n\t\t\t\t Change Ownership will be applied to instances\n\t\t\t\t of this class.\n");
    printf("           -h                     [Help]\n");
}

#ifdef __cplusplus
}
#endif




