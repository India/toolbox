/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_const.hxx

    Description:  Header File for constants used during scope of the executable.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28-Aug-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef SMA3_CONST_HXX
#define SMA3_CONST_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>

#define MAX_CHARS               300

/* Query Names    */
#define SMA3_QURY_POM_APP_OBJ                                    "SMA3_pom_app_obj_query"
#define SMA3_QURY_POM_APP_OBJ_ONLY_USER                          "SMA3_pom_app_obj_query_only_user"

/* Date Time Format */
#define SMA3_DATE_TIME_FORMAT_FILENAME                            "%y%m%d%H%M%S"

/* Attributes  */
#define SMA3_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define SMA3_ATTR_OWNING_GROUP_ATTRIBUTE                          "owning_group"
#define SMA3_ATTR_OWNING_USER_ATTRIBUTE                           "owning_user"
#define SMA3_ATTR_ACTIVE_SEQ                                      "active_seq"


/* Class Name */
#define SMA3_CLASS_WORKSPACEOBJECT                                "WorkspaceObject"
#define SMA3_CLASS_POM_APP_OBJECT                                 "POM_application_object"


/* Separators */
#define SMA3_STRING_UNDERSCORE                                    "_"
#define SMA3_STRING_HIFEN                                         "-"
#define SMA3_STRING_SEMICOLON                                     ";"
#define SMA3_STRING_FORWARD_SLASH                                 "/"
#define SMA3_STRING_SINGLE_SPACE								  " "
#define SMA3_CHARACTER_COLON                                      ':'


#define SMA3_SEPARATOR_LENGTH                                     1
#define SMA3_NUMERIC_ERROR_LENGTH                                 12
#define SMA3_STRING_DEFAULT_SEPARATOR	      					  ";"

#endif


