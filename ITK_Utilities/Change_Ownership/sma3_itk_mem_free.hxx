/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_itk_mem_free.hxx

    Description:  This file contains macros for freeing portion of memory allocated with TCe 'MEM_' functions and by TCe itself.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28-Aug-13     Tarun Kumar Singh   Initial Release
   
========================================================================================================================================================================*/
#ifndef SMA3_ITK_MEM_FREE_HXX
#define SMA3_ITK_MEM_FREE_HXX

#include <sma3_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef SMA3_MEM_TCFREE
#undef SMA3_MEM_TCFREE
#endif

// Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc

#define SMA3_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }

#define SMA3_SM_FREE( x ) \
         { \
            if ( x != NULL ) { \
	        SM_free( x ); \
	        x = NULL; \
        } \
}

#endif










