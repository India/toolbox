/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_change_ownership.hxx

    Description:  Header File containing declaration

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28-Aug-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef SMA3_CHANGE_OWNERSHIP_HXX
#define SMA3_CHANGE_OWNERSHIP_HXX

#include <sma3_library.hxx>
#include <sma3_errors.hxx>

#define SMA3_REPORT_CHANGE_OWNERSHIP_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup, pszPAppObjInfo){\
													char* pszErrorText = NULL;\
													EMH_ask_error_text(iRetCode, &pszErrorText);\
													fprintf(fLogFile, "Error: Cannot change ownership of '%s' belonging to user '%s' of group %s to user '%s' of group %s.\n\tERROR TEXT:'%s'\n",  pszPAppObjInfo, pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup, pszErrorText);\
													fflush(fLogFile);\
													fprintf(fRepeatFile, "%s\n", szLine);\
													fflush(fRepeatFile);\
													SMA3_MEM_TCFREE(pszErrorText);\
}

#define SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist){\
                                if((fRepeatFile = fopen(pszRepeatFile, "w")) == NULL ){\
                                    printf("\n** ERROR: Cannot create %s for writing unprocessed contents **\n\n",pszRepeatFile);}\
                                else{\
                                bRepeatFileExist = true;}\
}

#define SMA3_REPORT_OBJ_NOT_FOUND_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszUser, pszGroup, pszClassName){\
                                            fprintf(fLogFile, "Error: Cannot find any '%s' owned by user '%s' of group %s.\n", pszClassName, pszUser, pszGroup);\
                                            fflush(fLogFile);\
                                            fprintf(fRepeatFile, "%s\n", szLine);\
                                            fflush(fRepeatFile);\
}

#define SMA3_REPORT_UNABLE_TO_PARSE_LINE_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszConfigFileInfo){\
                                                    fprintf(fLogFile, "Error: Cannot Parse the line \n \t'%s' of file %s.\n", szLine, pszConfigFileInfo);\
                                                    fflush(fLogFile);\
                                                    fprintf(fRepeatFile, "%s\n", szLine);\
                                                    fflush(fRepeatFile);\
}

#define SMA3_REPORT_SUCCESS(iRetCode, fLogFile, fRepeatFile, szLine, pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup, pszPAppObjInfo){\
								fprintf(fLogFile, "Success: Ownership of '%s' belonging to user '%s' of group '%s' has been transferred to user '%s' of group '%s'.\n", pszPAppObjInfo, pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup);\
								fflush(fLogFile);\
}

#define SMA3_REPORT_SA_OR_QRY_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup){\
													char* pszErrorText = NULL;\
													EMH_ask_error_text(iRetCode, &pszErrorText);\
													fprintf(fLogFile, "Error: User '%s'. Group '%s'. New User '%s'. New Group %s.\n\tERROR TEXT:'%s'\n", pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup, pszErrorText);\
													fflush(fLogFile);\
													fprintf(fRepeatFile, "%s\n", szLine);\
													fflush(fRepeatFile);\
													SMA3_MEM_TCFREE(pszErrorText);\
}


int SMA3_perform_change_ownership
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    char*   pszClassName
); 

int SMA3_extract_info
(
    char*   pszLine,
    char*   pszSeparator,
    char**  ppszExistingUser,
    char**  ppszExistingGroup,
    char**  ppszNewUser,
    char**  ppszNewGroup
);

#endif






