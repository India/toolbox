/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_object_utilities.cxx

    Description: This file contains function performing various operations on Teamcenter Business Objects

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28-Aug-13     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_errors.hxx>
#include <sma3_library.hxx>

/**
 * This function gets the current date time.
 *
 * @param[in]  pszFormat   Expected date time format.
 * @param[out] szDateTime  current Date time in required format.
 *
 * @note 'szDateTime' must be a character array of at least [IMF_filename_size_c + 1] 
 *       characters 
 *
 * @retval ITK_ok on successful execution
 * @retval INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME if format resulted in
 *         resultant String length greater then IMF_filename_size_c + 1
 */
int SMA3_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
)
{
    int         iRetCode       = ITK_ok;
    int         iStrLength     = 0;
    struct tm   *clLocalTime   = NULL;
    time_t      tCurrentTime;

    /*Get system's local time*/
    tCurrentTime = time(NULL);
    clLocalTime  = localtime(&tCurrentTime);
        
    /*Get system time  in required format*/
    iStrLength =(int)( strftime( szDateTime, (IMF_filename_size_c + 1), pszFormat, clLocalTime));
   
      
    return iRetCode;
}

/**
 * Parses a string containing values seperated by a given separator
 * into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in] pszList          Input list to be parsed
 * @param[in] cSep             Separator character
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by SMA3_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 * @retval INCORRECT_INPUT If list or seprator is Null.
 * @retval MEMORY_ALLOCATION_FAILED If cannot allocate memory.
 */
int SMA3_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
)
{
    int        iRetCode    = ITK_ok;
    int        iCounter    = 0;
    char*      pszToken    = "";
    char*      pszTempList = NULL;

    /* Convert separator (char) to char* - strtok(char* input, Char* sep) */
    char pszSeparator[SMA3_SEPARATOR_LENGTH+1];
    pszSeparator[0] = cSep;
    pszSeparator[1] = '\0';

    /* Validate input */
    if((pszList == NULL) || (cSep == '\0'))
    {
        TC_write_syslog( "ERROR: Missing input data:List passed or seprator is null\n");
    }
    else
    {
        /* Allocating memory to pszTempList */
        pszTempList  = (char*) MEM_alloc(sizeof(char)*((int)tc_strlen(pszList)+1));
        if (pszTempList == NULL)
        {
            TC_write_syslog( "ERROR: Cannot allocate memory\n");
        }
        else
        {
            /* Copying the input string to Null terminated string */
            tc_strcpy(pszTempList, pszList);

            /* Get the first token */
            pszToken = tc_strtok( pszTempList,pszSeparator );

            *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
            /* Tokenize the input string */
            while( pszToken != NULL )
            {
                /* Allocate memory for each string pointer */
                (*pppszValuelist)[iCounter] = (char*) MEM_alloc(sizeof(char)*(((int)tc_strlen(pszToken))+1));
                
                /* Copy the token to the output string pointer */
                tc_strcpy((*pppszValuelist)[iCounter],pszToken);

                /* Count the number of elements in the string */
                iCounter++;
                
                /* Allocating memory to pointer of string (output) as per the number of tokens found */
                *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));
                
                /* Get next token: */
                pszToken = tc_strtok( NULL, pszSeparator );
               
            }

            if(iRetCode == ITK_ok)
            {
                *iCount = iCounter;
            }
        }  
    }

    /* Free the temporary memory used for tokenizing */
    SMA3_MEM_TCFREE(pszTempList);
    return iRetCode;
}

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int SMA3_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
	tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    SMA3_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    SMA3_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    SMA3_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}


/**
 * Function to Strips leading and trailing space. 
 *
 * @param[in]  pszString  String to trim white spaces 
 * @param[out] Pointer to Trimmed string 
 */
char* SMA3_trim( char* pszString )
{
  if (pszString == NULL)
    return(NULL);

  SMA3_trim_end(pszString);

    /*===================================================================*/
    /* Skip over spaces, and return the address of the first non-space   */
    /* character. If there are no leading spaces, this just returns str. */
    /*===================================================================*/

  return &pszString[strspn( pszString, " \t" )];

} 

/**
 * Function to Strips all trailing spaces from the given null-terminated string
 *
 * @param[in/out] pszString  String to trim white spaces
 *
 */

void SMA3_trim_end(char *pszString)
{
  int iLen = (int)(tc_strlen( pszString ) - 1);

  while (iLen >= 0  &&  ((pszString[iLen] == ' ') || 
          (pszString[iLen] == '\t') || (pszString[iLen] == '\n')))
      --iLen;

  pszString[iLen + 1] = '\0';

}

/**
 * This function query the database and gets all 'POM_application_object' of user 'tUser' belonging to group 'tGroup'
 * Function returns list of 'POM_application_object' belonging to user 'tUser' of group 'tGroup'
 *
 *  Select "puid" from POM_application_object
 *       (where 'owning_user' = tUser )
 *           AND
 *      (where 'owning_group' = tGroup )
 *
 * @param[in]  tUser          Tag to User whose data needs to be searched
 * @param[in]  tGroup         Tag of group of the above mentioned user
 * @param[out] piPOMAppObjCnt 'POM_application_object' count 
 * @param[out] pptPOMAppObj   Tags to the 'POM_application_object'
 *
 * @retval ITK_ok if everything went okay, Else ITK return code.
 */
int SMA3_query_pom_app_obj
(
    tag_t    tUser,
    tag_t    tGroup,
    int*     piPOMAppObjCnt,
    tag_t**  pptPOMAppObj
)
{
    int      iRetCode                   = ITK_ok;
    int      iNoOfRowsAfterQuery        = 0;
    int      iNoOfColumnsAfterQuery     = 0;
    void *** QueryReport                = NULL;
   
    /* Initializing the Out Parameter to this function. */
    (*pptPOMAppObj)  = NULL;
    (*piPOMAppObjCnt)= 0;

    /* This attribute is important as it provides tags to the POM_application_object. Fetching
       'puid' form the 'POM_application_object' table will not give tags to POM_application_object.
    */
    const char* pszUIDAttrList[]    = {"puid"};

    /* Creating a enquiry with 'SMA3_pom_app_obj_query' which is a unique identifier within the
       user's current session. This identifier can be any name.
    */
    static tag_t item_rev_query_initialized = NULLTAG;
    if ( NULLTAG == item_rev_query_initialized )
    {
        SMA3_ITKCALL(iRetCode, POM_enquiry_create(SMA3_QURY_POM_APP_OBJ));

        SMA3_ITKCALL(iRetCode, POM_enquiry_add_select_attrs(SMA3_QURY_POM_APP_OBJ,
                                                            SMA3_CLASS_POM_APP_OBJECT, 1, pszUIDAttrList));

        SMA3_ITKCALL(iRetCode, POM_enquiry_set_tag_value(SMA3_QURY_POM_APP_OBJ, "owning_user_tag",
                                                         1, &tUser, POM_enquiry_bind_value));

        SMA3_ITKCALL(iRetCode, POM_enquiry_set_attr_expr(SMA3_QURY_POM_APP_OBJ, "auniqueExprId_1",
                                                         SMA3_CLASS_POM_APP_OBJECT, SMA3_ATTR_OWNING_USER_ATTRIBUTE,
                                                         POM_enquiry_equal, "owning_user_tag"));
       
        SMA3_ITKCALL(iRetCode, POM_enquiry_set_tag_value(SMA3_QURY_POM_APP_OBJ, "owning_group_tag",
                                                         1, &tGroup, POM_enquiry_bind_value));

        SMA3_ITKCALL(iRetCode, POM_enquiry_set_attr_expr(SMA3_QURY_POM_APP_OBJ, "auniqueExprId_2",
                                                         SMA3_CLASS_POM_APP_OBJECT, SMA3_ATTR_OWNING_GROUP_ATTRIBUTE,
                                                         POM_enquiry_equal, "owning_group_tag"));

        SMA3_ITKCALL(iRetCode, POM_enquiry_set_expr(SMA3_QURY_POM_APP_OBJ, "auniqueExprId_3",
                                                    "auniqueExprId_1", POM_enquiry_and, "auniqueExprId_2"));

        SMA3_ITKCALL(iRetCode, POM_enquiry_set_where_expr(SMA3_QURY_POM_APP_OBJ, "auniqueExprId_3"));

        if(iRetCode == ITK_ok)
        {
            POM_cache_for_session (&item_rev_query_initialized);
            item_rev_query_initialized = 1;
         }
    }
    else
    {
        SMA3_ITKCALL(iRetCode, POM_enquiry_set_tag_value(SMA3_QURY_POM_APP_OBJ, "owning_user_tag",
                                                         1, &tUser, POM_enquiry_bind_value));

        SMA3_ITKCALL(iRetCode, POM_enquiry_set_tag_value(SMA3_QURY_POM_APP_OBJ, "owning_group_tag",
                                                         1, &tGroup, POM_enquiry_bind_value));
    }

    SMA3_ITKCALL(iRetCode, POM_enquiry_execute (SMA3_QURY_POM_APP_OBJ, &iNoOfRowsAfterQuery,
                                                &iNoOfColumnsAfterQuery, &QueryReport));
    if(iRetCode == ITK_ok)
    {
        if (iNoOfRowsAfterQuery > 0)
        {
            (*piPOMAppObjCnt) = iNoOfRowsAfterQuery;

            (*pptPOMAppObj) = (tag_t*) MEM_alloc(sizeof(tag_t) * iNoOfRowsAfterQuery);

            for(int idx = 0; idx < iNoOfRowsAfterQuery; idx++)
            {

                (*pptPOMAppObj)[idx] = *(tag_t*)(QueryReport[idx][0]); 
            }
        }
    }
    
    SMA3_MEM_TCFREE(QueryReport);
    return iRetCode;
}

/**
 * This function query the database and gets all 'POM_application_object' of user 'tUser'
 * Function returns list of 'POM_application_object' belonging to user 'tUser'
 *
 *  Select "puid" from POM_application_object where 'owning_user' = tUser
 *
 * @param[in]  tUser          Tag to User whose data needs to be searched
 * @param[out] piPOMAppObjCnt 'POM_application_object' count 
 * @param[out] pptPOMAppObj   Tags to the 'POM_application_object'
 *
 * @retval ITK_ok if everything went okay, Else ITK return code.
 */
int SMA3_query_pom_app_obj_one_arg
(
    tag_t    tUser,
    int*     piPOMAppObjCnt,
    tag_t**  pptPOMAppObj
)
{
    int      iRetCode                   = ITK_ok;
    int      iNoOfRowsAfterQuery        = 0;
    int      iNoOfColumnsAfterQuery     = 0;
    void *** QueryReport                = NULL;
   
    /* Initializing the Out Parameter to this function. */
    (*pptPOMAppObj)  = NULL;
    (*piPOMAppObjCnt)= 0;

    /* This attribute is important as it provides tags to the occurrences. Fetching
       'puid' form the Occurrence table will not give tags to Occurrences.
    */
    const char* pszUIDAttrList[]    = {"puid"};

    /* Creating a enquiry with 'SMA3_pom_app_obj_query' which is a unique identifier within the
       user's current session. This identifier can be any name.
    */
    static tag_t item_rev_query_initialized = NULLTAG;
    if ( NULLTAG == item_rev_query_initialized )
    {
        SMA3_ITKCALL(iRetCode, POM_enquiry_create(SMA3_QURY_POM_APP_OBJ_ONLY_USER));

        SMA3_ITKCALL(iRetCode, POM_enquiry_add_select_attrs(SMA3_QURY_POM_APP_OBJ_ONLY_USER,
                                                            SMA3_CLASS_POM_APP_OBJECT, 1, pszUIDAttrList));

        SMA3_ITKCALL(iRetCode, POM_enquiry_set_tag_value(SMA3_QURY_POM_APP_OBJ_ONLY_USER, "owning_user_tag",
                                                         1, &tUser, POM_enquiry_bind_value));

        SMA3_ITKCALL(iRetCode, POM_enquiry_set_attr_expr(SMA3_QURY_POM_APP_OBJ_ONLY_USER, "auniqueExprId_1",
                                                         SMA3_CLASS_POM_APP_OBJECT, SMA3_ATTR_OWNING_USER_ATTRIBUTE,
                                                         POM_enquiry_equal, "owning_user_tag"));

        SMA3_ITKCALL(iRetCode, POM_enquiry_set_where_expr(SMA3_QURY_POM_APP_OBJ_ONLY_USER, "auniqueExprId_1"));

        if(iRetCode == ITK_ok)
        {
            POM_cache_for_session (&item_rev_query_initialized);
            item_rev_query_initialized = 1;
         }
    }
    else
    {
        SMA3_ITKCALL(iRetCode, POM_enquiry_set_tag_value(SMA3_QURY_POM_APP_OBJ_ONLY_USER, "owning_user_tag",
                                                         1, &tUser, POM_enquiry_bind_value));
    }

    SMA3_ITKCALL(iRetCode, POM_enquiry_execute (SMA3_QURY_POM_APP_OBJ_ONLY_USER, &iNoOfRowsAfterQuery,
                                                &iNoOfColumnsAfterQuery, &QueryReport));
    if(iRetCode == ITK_ok)
    {
        if (iNoOfRowsAfterQuery > 0)
        {
            (*piPOMAppObjCnt) = iNoOfRowsAfterQuery;

            (*pptPOMAppObj) = (tag_t*) MEM_alloc(sizeof(tag_t) * iNoOfRowsAfterQuery);

            for(int idx = 0; idx < iNoOfRowsAfterQuery; idx++)
            {

                (*pptPOMAppObj)[idx] = *(tag_t*)(QueryReport[idx][0]); 
            }
        }
    }
    
    SMA3_MEM_TCFREE(QueryReport);
    return iRetCode;
}

#ifdef __cplusplus
}
#endif


