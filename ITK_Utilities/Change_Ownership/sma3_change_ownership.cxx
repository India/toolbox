/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_change_ownership.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28-Aug-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_errors.hxx>
#include <sma3_library.hxx>
#include <sma3_change_ownership.hxx>

/**
 * The utility reads that input file that has the following format: 
 *      CurrentUserName;CurrentGroup;NewUser;NewGroup 
 *
 * As a default separator ";" is used (if not specified).
 *
 * Utility can change the ownership of all the 'POM_application_object' and its sub classes owned by user 'CurrentUserName' of group 'CurrentGroup' 
 * to user 'NewUser' of group 'NewGroup'
 *
 * @note: Utility can be executed using following options:
 *       -class
 *            Define the Teamcenter Class name. Instances of this class will be considered for transferring the ownership
 *
 * @param[in] pszConfigFileInfo  Name of the Configuration File to be processed
 * @param[in] pszLogFile         Name of Log file
 * @param[in] pszRepeatFile      Name of the Repeat File
 * @param[in] pszSeparator       Separator used in Configuration File 
 * @param[in] pszClassName       Teamcenter Name of the class whose instance ownership will be Transferred 
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_perform_change_ownership
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    char*   pszClassName
)
{
    int     iRetCode              = ITK_ok;
    char    szLine[MAX_CHARS]     = {'\0'};
    logical bRepeatFileExist      = false;
    FILE*   fCfgFile;
    FILE*   fLogFile;
    FILE*   fRepeatFile;
   
    if ((fCfgFile = fopen(pszConfigFileInfo, "r")) == NULL )
    {
        printf("\n** ERROR: Cannot open %s for reading - exiting **\n\n", pszConfigFileInfo);
        return 0;
    }

    if ((fLogFile = fopen(pszLogFile, "w")) == NULL )
    {
        printf("\n** ERROR: Cannot create %s for writing log  **\n\n",pszLogFile);
    }
    else
    {
        /*-------------------------------------*/
        /* Do not update date fields on change */
        /*-------------------------------------*/
        SMA3_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_attr_update, false, 0,0,0, NULL));

        /*----------------------*/
        /* Bypass access checks */
        /*----------------------*/
        SMA3_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_access_check, true, 0,0,0, NULL));


        SMA3_ITKCALL(iRetCode, ITK_set_bypass(true));

		if(iRetCode != ITK_ok )
        {
            return iRetCode;
        }

        while (fgets(szLine, MAX_CHARS, fCfgFile) != NULL)
        {
            if ((szLine[0] != '#') && (szLine[0] != '\0') && (szLine[0] != '\n'))
            {
                int     iLen               = 0;
                char*   pszExistingUser    = NULL;
                char*   pszExistingGroup   = NULL;
                char*   pszNewUser         = NULL;
                char*   pszNewGroup        = NULL;

                /*================================================*/
                /* Trim the line to remove leading and trailing   */
                /* spaces and discard the \n on the end.          */
                /*================================================*/
                tc_strcpy(szLine, SMA3_trim(szLine));

                SMA3_ITKCALL(iRetCode , SMA3_extract_info(szLine, pszSeparator, &pszExistingUser, &pszExistingGroup, &pszNewUser, &pszNewGroup));

				if(pszExistingUser != NULL && pszNewGroup != NULL)
				{
					int    iPOMAppObjCnt   = 0;
					tag_t  tExistingUser   = NULLTAG;
					tag_t  tExistingGroup  = NULLTAG;
					tag_t  tNewUser        = NULLTAG;
					tag_t  tNewGroup       = NULLTAG;
                    tag_t* ptPOMAppObj     = NULL;

					SMA3_ITKCALL(iRetCode, SA_find_user(pszExistingUser, &tExistingUser));

					if(pszNewUser == NULL || tc_strlen(pszNewUser) == 0)
					{
						tNewUser = tExistingUser;
					}
					else
					{
						SMA3_ITKCALL(iRetCode, SA_find_user(pszNewUser, &tNewUser));
					}

					if(pszExistingGroup != NULL)
					{
						if(tc_strlen(pszExistingGroup) != 0)
						{
							SMA3_ITKCALL(iRetCode, SA_find_group(pszExistingGroup, &tExistingGroup));
						}
					}

					SMA3_ITKCALL(iRetCode, SA_find_group(pszNewGroup, &tNewGroup));

					if(tExistingGroup != NULLTAG)
					{
						SMA3_ITKCALL(iRetCode, SMA3_query_pom_app_obj(tExistingUser, tExistingGroup, &iPOMAppObjCnt, &ptPOMAppObj));
					}
					else
					{
						SMA3_ITKCALL(iRetCode, SMA3_query_pom_app_obj_one_arg(tExistingUser, &iPOMAppObjCnt, &ptPOMAppObj));
					}

					if(iPOMAppObjCnt == 0 && iRetCode == ITK_ok)
					{
						/* Open repeat file */
                        if(bRepeatFileExist == false)
                        {
                            SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                        }
       
                        SMA3_REPORT_OBJ_NOT_FOUND_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszExistingUser, pszExistingGroup, pszClassName);
					}
					
					if(iRetCode != ITK_ok)
					{
						/* Open repeat file */
                        if(bRepeatFileExist == false)
                        {
                            SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                        }
						/* Report Error */
                        SMA3_REPORT_SA_OR_QRY_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup);
					}

					for(int iDx = 0; iDx < iPOMAppObjCnt && iRetCode == ITK_ok; iDx++)
					{
						char*   pszPAppObjInfo       = NULL;
						logical bIsWSO               = false;
						logical bErrorStored         = false;
						logical bBelongsToInputClass = false;

						/*** Section Start: This section is meant for retrieving information which will be used in Logging ***/
						SMA3_obj_is_type_of(ptPOMAppObj[iDx], SMA3_CLASS_WORKSPACEOBJECT, &bIsWSO);

						if(bIsWSO == true)
						{
							AOM_ask_value_string(ptPOMAppObj[iDx], SMA3_ATTR_OBJECT_STRING_ATTRIBUTE, &pszPAppObjInfo);
						}
						else
						{
							POM_tag_to_uid(ptPOMAppObj[iDx], &pszPAppObjInfo);
						}
						/***   Section End   ***/

						SMA3_ITKCALL(iRetCode, SMA3_obj_is_type_of(ptPOMAppObj[iDx], pszClassName, &bBelongsToInputClass));

						if(bBelongsToInputClass == true)
						{
							tag_t tOwner  = NULLTAG;
							tag_t tGroup  = NULLTAG;

							SMA3_ITKCALL(iRetCode, AOM_ask_owner(ptPOMAppObj[iDx], &tOwner));

							SMA3_ITKCALL(iRetCode, AOM_ask_group(ptPOMAppObj[iDx], &tGroup));

							if(tOwner != tNewUser || tGroup != tNewGroup)
							{
								SMA3_ITKCALL(iRetCode, AOM_set_ownership(ptPOMAppObj[iDx], tNewUser, tNewGroup));

								if(iRetCode == ITK_ok)
								{
									/* Report Success */
									SMA3_REPORT_SUCCESS(iRetCode, fLogFile, fRepeatFile, szLine, pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup, pszPAppObjInfo);
								}
								else
								{
									bErrorStored = true;
									/* Open repeat file  */
									if(bRepeatFileExist == false)
									{
										SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
									}
									/* Report Error */
									SMA3_REPORT_CHANGE_OWNERSHIP_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup, pszPAppObjInfo);
								}
							}
						}

						if(iRetCode != ITK_ok && bErrorStored == false)
						{
							/* Open repeat file */
							if(bRepeatFileExist == false)
							{
								SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
							}
							/* Report Error */
							SMA3_REPORT_CHANGE_OWNERSHIP_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszExistingUser, pszExistingGroup, pszNewUser, pszNewGroup, pszPAppObjInfo);
						}

						iRetCode = ITK_ok;
						SMA3_MEM_TCFREE( pszPAppObjInfo );
					}
					
					SMA3_MEM_TCFREE( ptPOMAppObj );
				}
				else
				{
					 /* Generating repeat file */
                    if(bRepeatFileExist == false)
                    {
                        SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                    }

                    /* Report Error */
                    SMA3_REPORT_UNABLE_TO_PARSE_LINE_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszConfigFileInfo);
				}

                iRetCode = ITK_ok;            
                
                SMA3_MEM_TCFREE( pszExistingUser );
                SMA3_MEM_TCFREE( pszExistingGroup );
                SMA3_MEM_TCFREE( pszNewUser );
                SMA3_MEM_TCFREE( pszNewGroup );
            }
        } 

        SMA3_ITKCALL(iRetCode, ITK_set_bypass(false));
    }

    if(bRepeatFileExist == true)
    {
        fclose( fRepeatFile );
    }

    fclose( fCfgFile );
    fclose( fLogFile );
    return iRetCode;
}

/**
 * This function parse the line from the load file and initialize the variable with appropriate information.
 *
 * @param[in]  pszLine            Line from the Configuration File to be processed
 * @param[in]  pszSeparator       Character separating the information in the load file
 * @param[out] ppszExistingUser   Existing user whose ownership of data will be transferred 
 * @param[out] ppszExistingGroup  Group of above mentioned user
 * @param[out] ppszNewUser        New user to whom ownership of data will be transferred
 * @param[out] ppszNewGroup       New group of above mentioned new user
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_extract_info
(
    char*   pszLine,
    char*   pszSeparator,
    char**  ppszExistingUser,
    char**  ppszExistingGroup,
    char**  ppszNewUser,
    char**  ppszNewGroup
)
{
    int    iRetCode   = ITK_ok;
    int    iInfoCnt   = 0;
    char** ppszInfo   = NULL;

    SMA3_ITKCALL(iRetCode , SMA3_arg_parse_multipleVal(pszLine, pszSeparator[0], &iInfoCnt, &ppszInfo));

    if(iInfoCnt == 4)
    {
        (*ppszExistingUser)  = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[0]) +1)));
		(*ppszExistingGroup) = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[1]) +1)));
        (*ppszNewUser)       = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[2]) +1)));
        (*ppszNewGroup)      = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[3]) +1)));

		tc_strcpy(*ppszExistingUser, ppszInfo[0]);
        tc_strcpy(*ppszExistingGroup, ppszInfo[1]);
        tc_strcpy(*ppszNewUser, ppszInfo[2]);
        tc_strcpy(*ppszNewGroup, ppszInfo[3]);
    }

    SMA3_MEM_TCFREE( ppszInfo );
    return iRetCode;
}

#ifdef __cplusplus
}
#endif


