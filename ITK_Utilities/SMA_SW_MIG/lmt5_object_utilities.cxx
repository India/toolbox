/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_object_utilities.cxx

    Description: This file contains function performing various operations on Teamcenter Business Objects

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <lmt5_errors.hxx>
#include <lmt5_library.hxx>

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int LMT5_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
	tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    LMT5_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    LMT5_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    LMT5_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}

/**
 * This function gets the name of type for the input business
 * objet.
 *
 * e.g. Consider Item type 'LMT5_ZSS'. If we pass tag to an instance of 'LMT5_ZSS' this function will return string
 *      'LMT5_ZSS' and not 'Item'. Consider Dataset of type 'catia'. If we pass tag to an instance of 'catia' this function will
 *      return 'catia' and not 'Dataset'.
 *
 * @param[in]  tObject       Business Object tag
 * @param[out] ppszObjType   Object type
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMT5_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
)
{
    int     iRetCode            = ITK_ok;
    tag_t   tObjectType         = NULLTAG;
    char    szObjectType[TCTYPE_name_size_c+1] = "";

    /* Initializing the Out Parameter to this function. */
    (*ppszObjType) = NULL;
    
    LMT5_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject,&tObjectType));
    
    LMT5_ITKCALL(iRetCode, TCTYPE_ask_name(tObjectType, szObjectType));
    
    if(iRetCode == ITK_ok)
    {
        LMT5_CUST_STRCPY((*ppszObjType), szObjectType);
    }
    
    return iRetCode;
}

/**
 * Save and unlock the given object
 *
 * @param[in] tObject tag of object to be saved and unloaded
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMT5_obj_save_and_unlock
(
    tag_t       tObject
)
{
    int iRetCode  = ITK_ok;

    /*  Save the object.  */
    LMT5_ITKCALL(iRetCode, AOM_save( tObject));

    /* Unlock the object. */
    LMT5_ITKCALL(iRetCode, AOM_unlock( tObject));

    return iRetCode;

}

/**
 * This function get's the tag to status of input status type(pszStatusName) out 
 * of the list of status attached to input Workspace Object.
 *
 * @param[in]  tObject        The object to get the release status from
 * @param[in]  pszStatusName  Name of status whose tag is to be recovered
 * @param[out] ptStatusTag    Tag to status of input type
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMT5_obj_ask_status_tag
(
    tag_t      tObject,
    char*      pszStatusName,
    tag_t*     ptStatusTag
)
{
    int     iRetCode              = ITK_ok;
    int     iNoOfStatuses         = 0;
    tag_t   item_rev              = NULLTAG;
    tag_t*  ptStatusTagList       = NULL;
    logical bFound                = false;
    char    szRelease_status_type[WSO_name_size_c+1] = {""};

    /* Initializing the Out Parameter to this function. */
    (*ptStatusTag) = NULLTAG;


     LMT5_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses,
                                                          &ptStatusTagList));
    
    for (int iDx = 0; iDx < iNoOfStatuses && iRetCode == ITK_ok && bFound == false; iDx++)
    {
        
        LMT5_ITKCALL(iRetCode, CR_ask_release_status_type(ptStatusTagList[iDx],
                                                           szRelease_status_type));
        
        if(tc_strcmp(pszStatusName, szRelease_status_type) == 0)
        {
              *ptStatusTag = ptStatusTagList[iDx];
              bFound = true;
        }
            
    }
      
      
    LMT5_MEM_TCFREE(ptStatusTagList);
    return iRetCode;
}

/**
 * Get the release status string and tag of an object.
 *
 * @note  Do not add any custom error code to this function as it will
 *        impact functionality for SMA3 321-3. We can use custom error
 *        in the calling function.
 *
 * Get the attribute length of "release_status_list" attribute of the object
 * length == 0? no status
 * else (length > 0)
 * get the tag of all the status from the "release_status_list"
 * get the name of the release status with the found tags.
 *
 * @param[in]  tObject             The object to get the release status from
 * @param[out] pppszStatusNameList Listof  status names
 * @param[out] piStatusCount       Status Count
 * @param[out] pptStatusTagList    Status list of tags.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMT5_obj_ask_status_list
(
    tag_t    tObject,
    char***  pppszStatusNameList,
    int*     piStatusCount,
    tag_t**  pptStatusTagList
)
{
    int   iRetCode       = ITK_ok;
    int   iNoOfStatuses  = 0;

    /* Initializing the Out Parameter to this function. */
    (*piStatusCount)    = 0;
    (*pptStatusTagList) = NULL;
    (*pppszStatusNameList) = NULL;

    LMT5_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses, pptStatusTagList));
    
    for (int iDx = 0; iDx < iNoOfStatuses && iRetCode == ITK_ok; iDx++)
	{
        char szRelease_status_type[WSO_name_size_c+1] = {""};
        
        LMT5_ITKCALL(iRetCode, CR_ask_release_status_type((*pptStatusTagList)[iDx], szRelease_status_type));

        if(iRetCode == ITK_ok)
		{
            LMT5_CUST_UPDATE_STRING_ARRAY((*piStatusCount), (*pppszStatusNameList), szRelease_status_type);               
        }            
    }
   
    return iRetCode;
}

/** 
 * This function does not create status, it only set's the status of an object to the status tag supplied to it.
 *
 * @param[in]  tObjectTag     Tag to Object On which supplied status is to be set.
 * @param[in]  tReleaseStatus Tag to the existing status.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMT5_obj_set_existing_status 
(
    tag_t tObjectTag,
    tag_t tReleaseStatus
)
{
    int    iRetCode        = ITK_ok;
    char   szClassName[TCTYPE_class_name_size_c+1] = {""};
    tag_t  tRelStatusList  = NULLTAG;
    tag_t  tReleDate       = NULLTAG;
    tag_t  tTypeTag        = NULLTAG;
    tag_t  tClassTag       = NULLTAG;
    tag_t  tDateTag        = NULLTAG;
    date_t dReleaseDate    = NULLDATE;

    /*  Lock the object.  */
    LMT5_ITKCALL(iRetCode, AOM_refresh(tObjectTag, true));

    LMT5_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObjectTag, &tTypeTag));
    
    LMT5_ITKCALL(iRetCode, TCTYPE_ask_class_name(tTypeTag, szClassName));
        
    /* Get attribute tags for Release Status and Date Released on Item revision class. */
    LMT5_ITKCALL(iRetCode, POM_attr_id_of_attr(LMT5_ATTR_RELEASE_STATUS_LIST, szClassName, &tRelStatusList)); 

    LMT5_ITKCALL(iRetCode, POM_attr_id_of_attr(LMT5_ATTR_DATE_RELEASED, szClassName, &tReleDate));  
    
    /* Get current date */
    LMT5_ITKCALL(iRetCode, LMT5_dt_get_date(&dReleaseDate));
    
    LMT5_ITKCALL(iRetCode, POM_set_attr_date(1,&tObjectTag, tReleDate, dReleaseDate));
        
    /* Set Release status tag to item revision Object  */
    LMT5_ITKCALL(iRetCode, POM_set_attr_tags(1, &tObjectTag, tRelStatusList, 0, 1, &tReleaseStatus));
    
    LMT5_ITKCALL(iRetCode, POM_save_instances(1, &tObjectTag, false));

    /*  Unlock the object.  */
    LMT5_ITKCALL(iRetCode, AOM_refresh(tObjectTag, false));

    return iRetCode; 
}

/** 
 * This function does not create status, it only set's the status of input source object to input destination object.
 *
 * @param[in] tSrcObj  Tag to source object whose status will be set to destination object.
 * @param[in] tDestObj Tag to the existing status.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMT5_obj_set_src_obj_status_to_dest
(
    tag_t tSrcObj,
    tag_t tDestObj
)
{
    int    iRetCode      = ITK_ok;
    int    iStatCnt      = 0;
    char** ppszStatList  = NULL;
    tag_t* ptStatus      = NULL;

    LMT5_ITKCALL(iRetCode, LMT5_obj_ask_status_list(tSrcObj, &ppszStatList, &iStatCnt, &ptStatus));

    if(iStatCnt > 0 && tDestObj != NULLTAG)
    {
        for(int iDx = 0; iDx < iStatCnt && iRetCode == ITK_ok; iDx++)
        {
            LMT5_ITKCALL(iRetCode, LMT5_obj_set_existing_status(tDestObj, ptStatus[iDx]));
        }
    }

    LMT5_MEM_TCFREE(ppszStatList); 
    LMT5_MEM_TCFREE(ptStatus); 
    return iRetCode; 
}


/**
 * Function dereferences the statuses attached to the Input Object.
 *
 * @param[in] tObject  Object tag whose status are to be removed
 *
 * @retval ITK_ok if successful else ITK return code
 */
int LMT5_obj_dereference_status
( 
    tag_t  tObject
)
{
    int    iRetCode         = ITK_ok;
    int    iStatusCount     = 0;
    tag_t* ptStatuses       = NULL;
    char** ppszStatusList   = NULL;
   
    /* Lists release statuses attached to a workspace object */
    LMT5_ITKCALL(iRetCode, LMT5_obj_ask_status_list(tObject, &ppszStatusList, &iStatusCount, &ptStatuses));

    for(int iDx = 0; iDx < iStatusCount && iRetCode == ITK_ok; iDx++)
    {
        LMT5_ITKCALL(iRetCode, LMT5_obj_remove_value_from_vla_attr(tObject, LMT5_ATTR_RELEASE_STATUS_LIST, ptStatuses[iDx]));       
    }
    
    LMT5_MEM_TCFREE(ptStatuses); 
    LMT5_MEM_TCFREE(ppszStatusList); 
    return iRetCode;
}

/**
 * Function modify the given attribute on the Item Revision. This will remove attribute values of status objects 
 * (release_status_list attribute) on the Item Revision.
 *
 * @param[in] tObject        tag of the Item Revision
 * @param[in] pszAttrName   name of the attribute to update
 * @param[in] tValue         tag of the value to be update
 *
 * @retval ITK_ok if successful else ITK return code
 */
int LMT5_obj_remove_value_from_vla_attr
( 
    tag_t  tObject,
    char*  pszAttrName,
    tag_t  tValue
)
{
    int     iRetCode           = ITK_ok;
    int     iValueIndex        = -1;
    tag_t   tInstClassId       = NULLTAG;
    tag_t   tAttrId            = NULLTAG;
    char*   pszClassName       = NULL;

    /* Finds out the class to which the instance belongs */
    LMT5_ITKCALL(iRetCode, POM_class_of_instance(tObject, &tInstClassId));

    /* Returns the name of a class which is specified by its ID */
    LMT5_ITKCALL(iRetCode, POM_name_of_class(tInstClassId, &pszClassName));

    /* Returns the attr_id for the specified attribute in the specified class. */
    LMT5_ITKCALL(iRetCode, POM_attr_id_of_attr(pszAttrName, pszClassName, &tAttrId));

    /* Finds the index of the first occurrence of the given value in an attribute. */
    LMT5_ITKCALL(iRetCode, POM_ask_index_of_tag(tObject, tAttrId, tValue, &iValueIndex));

    /* Locks an object against modification by another process. */
    LMT5_ITKCALL(iRetCode, AOM_refresh(tObject, true)); 

    /* Removes attribute value of the specified object. */
    LMT5_ITKCALL(iRetCode, POM_remove_from_attr(1, &tObject, tAttrId, iValueIndex, 1));

    /* Saves an application object to the database. */
    LMT5_ITKCALL(iRetCode, AOM_save(tObject));

    /* Unlocks an object for modification by another process. */
    LMT5_ITKCALL(iRetCode, AOM_refresh(tObject, false)); 
    
    if(iRetCode != ITK_ok)
    {
        int iFail = ITK_ok; 
        iFail = AOM_refresh(tObject, false);
    }

    LMT5_MEM_TCFREE(pszClassName);
    return iRetCode;
}

/**
 * This function checks if item revision is released before creation date of input dataset. 
 *
 * @param[in]  tMSSRev             Tag to MSS Revision
 * @param[in]  tCurrentMSSBOMLine  BOM Line tag of MSS revision
 * @param[in]  tRelatedTSSTeil     Tag of TSS Revision to which above MSS Revision attached. This argument can be 'NULLTAG'
 * @param[out] pbIsReleased        'true' if MSS revision has status 'LM5_freigegeben' else 'false'
 *
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMT5_validate_ir_rel_and_ds_creation
(
    tag_t    tItemRev,
    tag_t    tDataset,
    logical* pbIsRelBefore
)
{
    int     iRetCode    = ITK_ok;
    int     iStatusCnt  = 0;
    tag_t*  ptStatList  = NULL;

    /* Initializing out Parameter of function to default value. */
    (*pbIsRelBefore) = false;

    LMT5_ITKCALL(iRetCode, WSOM_ask_release_status_list(tItemRev, &iStatusCnt, &ptStatList));

    if(iStatusCnt > 0)
    {
        int    iAnswer        = 0;
        date_t dtIRRelDate    = NULLDATE;
        date_t dtDSCreateDate = NULLDATE;

        LMT5_ITKCALL(iRetCode, AOM_ask_value_date(tItemRev, LMT5_ATTR_DATE_RELEASED, &dtIRRelDate));

        LMT5_ITKCALL(iRetCode, AOM_ask_value_date(tDataset, LMT5_ATTR_CREATION_DATE, &dtDSCreateDate));
        /* Compares two dates for equality. Function result is returned in 'iAnswer' as
           0 if dates are equal 
           1 if date1 is later than date2 (date1 > date2)
           -1 if date1 is earlier than date2 (date1 < date2)
        */  
        LMT5_ITKCALL(iRetCode, POM_compare_dates(dtIRRelDate, dtDSCreateDate, &iAnswer));

        if(iAnswer == -1)
        {
            (*pbIsRelBefore) = true;
        }
    }

    LMT5_MEM_TCFREE(ptStatList);  
    return iRetCode;
}