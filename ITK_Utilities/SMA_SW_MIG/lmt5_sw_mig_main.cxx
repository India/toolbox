/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_sw_mig_main.cxx

    Description: This File contains functions that are entry point to Solidworks 2D and 3D migration executable

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14      Tarun Kumar Singh   Initial Release
========================================================================================================================================================================*/

#include <lmt5_library.hxx>
#include <lmt5_library.hxx>
#include <lmt5_const.hxx>
#include <lmt5_errors.hxx>
#include <lmt5_process_sw_migration.hxx>


int LMT5_init_ITK_login(int argc, char **argv);

void LMT5_display_usage();

/**
 * This function is the main function where program starts execution. It is responsible for the high-level organization of the
 * program's functionality, and try to access command arguments which basically are login credentials when this executable is run.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int ITK_user_main( int iArgCount, char** ppszArgVal)
{
    int     iRetCode     = ITK_ok;
    logical bSkipBOM     = false;
    logical bDryRun      = false;
    logical bMigDrw      = false;
	logical bIgnCreaDate = false;

    /* display help if user asked for it */
	if ((ITK_ask_cli_argument("-h")) || (iArgCount == 0) )
    {
        LMT5_display_usage();
        exit(0);
    }

    char* pszLogFile = ITK_ask_cli_argument("-log_file=" );

    /* validating the log file */ 
    if((pszLogFile == NULL) || (tc_strlen(pszLogFile) == 0))
    {
        fprintf(stderr,"\n\nError: Log file not specified.\n");
        LMT5_display_usage();
        exit(0);
    }
    /* Mapping file containing inputs for Item ID that needs to be migrated as part of 2D and 3D migration */
    char* pszMappingFileInfo  = ITK_ask_cli_argument("-mapping_file=");

    /* validating input file */ 
    if((pszMappingFileInfo == NULL) || (tc_strlen(pszMappingFileInfo) == 0))
    {
        fprintf(stderr,"\n\nError: Mapping File name not specified.\n");
        LMT5_display_usage();
        exit(0);
    }
    /* Mapping file required only for 2D migration. File contains Item ID that needs for SWIM dependency update during 2D migration */
    char* pszDependencyMigFileInfo  = ITK_ask_cli_argument("-dependency_mig_file=");
    
    /* Optional argument. If not provided through command prompt then default value will be considered. */
    char* pszSeparator  = ITK_ask_cli_argument("-sep=");

    /* validating Separator */ 
    if((pszSeparator == NULL) || (tc_strlen(pszSeparator) == 0))
    {
        pszSeparator = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(LMT5_STRING_DEFAULT_SEPARATOR) +1)));
        tc_strcpy(pszSeparator, LMT5_STRING_DEFAULT_SEPARATOR);
    }

    /* Optional argument: Needed during 3D migration only.If not provided through command prompt then default value will be considered. 
                          Default value will be Latest Revision.
    */
	char* pszRevID  = ITK_ask_cli_argument("-rev_id=");

    /* Optional argument: Needed during 3D migration only. If not provided through command prompt then default value will be considered.
                          Default value will be 'Latest Working'.
    */
	char* pszRevRule  = ITK_ask_cli_argument("-rev_rule=");

    for(int iDx = 0; iDx < iArgCount; iDx++)
    {
       if(tc_strcmp("-skip_bom", ppszArgVal[iDx]) == 0)
       {
            bSkipBOM = true;
            printf("Running utility to skip BOM building based on mapping file.\n");
       }

       if(tc_strcmp("-dryrun", ppszArgVal[iDx]) == 0)
       {
            bDryRun = true;
            printf("Running utility in dry run mode based on mapping file.\n");
       }

       if(tc_strcmp("-drawing_migration", ppszArgVal[iDx]) == 0)
       {
            bMigDrw = true;
            printf("Running utility for Drawing migration based on mapping file.\n");
       }

       if(tc_strcmp("-ignore_creation_date_check", ppszArgVal[iDx]) == 0)
       {
            bIgnCreaDate = true;
            printf("Running utility to ignore create date check for 2D dataset migration.\n");
       }  
    }
//getch();
    /* Needed during 3D migration only. This is the Item ID of the top most assembly Item whose components will be migrated. */
	char* pszItemID  = ITK_ask_cli_argument("-item_id=");

    if(bMigDrw == false)
    {
        /* validating Item ID */ 
        if((pszItemID == NULL) || (tc_strlen(pszItemID) == 0))
        {
            fprintf(stderr,"\n\nError: Top assembly Item ID not specified.\n");
            LMT5_display_usage();
            exit(0);
        }
    }
    else
    {
        if((pszDependencyMigFileInfo == NULL) || (tc_strlen(pszDependencyMigFileInfo) == 0))
        {
            fprintf(stderr,"\n\n   Information: Dependency Migration Mapping File not specified for solidworks drawing migration.\n");
        }
    }

    LMT5_ITKCALL( iRetCode, LMT5_init_ITK_login(iArgCount, ppszArgVal));

    if(iRetCode != ITK_ok)
    {
        char* pszErrorText = NULL;
        EMH_ask_error_text(iRetCode, &pszErrorText);
        printf("Login Unsuccessful. %s \n", pszErrorText);
        LMT5_MEM_TCFREE( pszErrorText );
    }
    else
    {
        printf("Successfully Logged In Teamcenter \n");
    }
    
    if(iRetCode == ITK_ok)
    {                           
        LMT5_ITKCALL( iRetCode, LMT5_process_sw_mig(pszMappingFileInfo, pszDependencyMigFileInfo, pszLogFile, pszSeparator,
                                                     pszItemID, pszRevID, pszRevRule, bSkipBOM, bDryRun, bMigDrw, bIgnCreaDate));
    }

	LMT5_ITKCALL( iRetCode, ITK_exit_module(FALSE));

    return iRetCode;
}

/**
 * This function retrieves login credentials and try to login to Teamcenter with these credential. If not supplied executable
 * performs auto login.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_init_ITK_login(int argc, char **argv)
{
    int   iRetCode        = ITK_ok;

    /* Gets the user ID */
    char* pszUser  = ITK_ask_cli_argument( "-u=");

    /* Gets the user Password */
    char* pszPassword  = ITK_ask_cli_argument( "-p=");

    /* Gets the user group */
    char* pszGroup  = ITK_ask_cli_argument( "-g=");

    /*  To run against motif libs.*/
    LMT5_ITKCALL(iRetCode, ITK_initialize_text_services(ITK_BATCH_TEXT_MODE));

    /*  Put -h CLI argument here to avoid starting the whole of iman just for a help message! */
    if( ITK_ask_cli_argument( "-h" ) != 0 )
    {
        LMT5_display_usage();
        return iRetCode;
    }

	if(pszGroup == NULL || pszUser == NULL)
	{
		LMT5_ITKCALL( iRetCode, ITK_auto_login());		
	}
	else
	{
		LMT5_ITKCALL( iRetCode, ITK_init_module(pszUser, pszPassword, pszGroup));		
	}

    return iRetCode;
}

/**
 * This function displays the usage of this Executable
 */
void LMT5_display_usage()
{
    printf("     \n\t\t\n Solidworks Migration Utility Usage \n");
    printf("     \t\t\n-------------------------------\n");
    printf("    -u                          = <user name> to specify Teamcenter user name\n");
    printf("    -p                          = <password> to specify password\n");
    printf("    -g                          = <group name> to specify group name\n");
    printf("    -log_file                   = log_file: Specifies the log file.\n\t\t\t\t  Error information about the object will be\n\t\t\t\t  written to the log file.\n");
    printf("    -mapping_file               = mapping_file: Specifies the mapping file to be\n\t\t\t\t  processed. It has the format\n\t\t\t\t  <Source Item-ID>~<Destination Item-ID>.\n\t\t\t\t  Commented lines start with \"#\". \n\t\t\t\t  These lines will be ignored while processing.\n"); 
    printf("    -sep                        = Specifies the separator to be\n\t\t\t\t  used in the input file. If not specified the\n\t\t\t\t  default will be \"~\".\n");
    printf("    -item_id                    = Item ID of top assembly item\n\t\t\t\t  whose sub assembly needs to be migrated.\n\t\t\t\t  This argument is required only for 3D\n\t\t\t\t  Solidworks dataset migration.\n");
    printf("    -rev_id                     = Revision ID of top assembly item whose sub\n\t\t\t\t  assembly needs to be migrated.\n\t\t\t\t  By default Latest Revision will be considered.\n\t\t\t\t  This argument is required only for 3D\n\t\t\t\t  Solidworks dataset migration.\n");  
    printf("    -rev_rule                   = Revision Rule to configure imprecise\n\t\t\t\t  assembly of appropriate assembly revision.\n\t\t\t\t  By default 'Latest Working' will be considered\n\t\t\t\t  This argument is required only for 3D\n\t\t\t\t  Solidworkscdataset migration.\n");  
    printf("    -skip_bom                   = Optional: BOM creation will be skipped\n\t\t\t\t  when provide during execution.\n");
    printf("    -dryrun                     = Optional: Only validation will be performed\n\t\t\t\t  during execution of the utility.\n");
    printf("    -drawing_migration          = This option used for 2D Solidworks dataset\n\t\t\t\t  migration.\n");
    printf("    -dependency_mig_file        = mapping_file: Specifies the mapping file to be\n\t\t\t\t  read for SW drawing datasets\n\t\t\t\t  SWIM dependency migration. It has the format\n\t\t\t\t  <Source Item-ID>~<Destination Item-ID>.\n\t\t\t\t  Commented lines start with \"#\". \n\t\t\t\t  These lines will be ignored while processing.\n"); 
	printf("    -ignore_creation_date_check = Optional: If this option is specified then\n\t\t\t\t  check for creation date will be\n\t\t\t\t  ignored.\n");
	printf("    -h                     [Help]\n");
}




