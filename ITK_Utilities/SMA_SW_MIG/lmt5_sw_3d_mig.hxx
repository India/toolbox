/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_sw_3d_mig.hxx

    Description:  Header File containing declaration

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15-Oct-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LMT5_SW_3D_MIG_HXX
#define LMT5_SW_3D_MIG_HXX

#include <lmt5_library.hxx>
#include <lmt5_errors.hxx>

int LMT5_find_asm_info
(
    FILE*   fLogFile,
    char*   pszItemID,
    char*   pszRevID,
    tag_t*  ptAssyRev
);

int LMT5_perform_3d_migration
(
    FILE*   fLogFile,
    tag_t   tAssyRev,
    char*   pszRevRule,
    int     iItemCnt,
    tag_t*  ptOldItem,
    tag_t*  ptNewItem,
    logical bSkipBOM,
    logical bDryRun,
    logical bIgnCreaDate
);

int LMT5_check_3d_mig_condition
(
    FILE*    fLogFile,
    tag_t    tCurrentIR, 
    tag_t    tCurrentBL,
    tag_t    tTargetIR,
    logical* pbFoundError,
    logical  bIgnCreaDate
);

int LMT5_validate_3d_asm_ds_dependency
(
    FILE*    fLogFile,
    tag_t    tCurrentIR,
    tag_t    tCurrentBL,
    tag_t    tSWAsmDS,
    logical* pbFoundError
);

int LMT5_construct_imprecise_bom
(
    tag_t  tAssyRev,
    char*  pszRevRule, 
    int    iItemCnt,
    tag_t* ptOldItemID,
    tag_t* ptNewItemID,
    int    iBLCount,
    tag_t* ptItems,
    tag_t* ptChildIRs, 
    tag_t* ptChildBLs
);

int LMT5_check_if_imprecise_occ_exist
(
    tag_t    tNewParentITBVR, 
    tag_t    tNewItem,
    tag_t    tSrcParentBL,
    tag_t    tSrcChildIR,
    logical* pbFoundExtraOcc
);

int LMT5_set_ps_attributes
(
    tag_t  tSourceChildBL,
    tag_t  tNewPSOcc,
    tag_t  tParentITBVR  
);

int LMT5_migrate_sw_3d_dataset
(
    FILE*  fLogFile,
    tag_t  tTopLine,
    tag_t  tAssyRev,
    int    iSrcAssyIRCnt, 
    tag_t* ptSrcAssyIR, 
    tag_t* ptSrcItem,
    int    iItemCnt,
    tag_t* ptOldItem,
    tag_t* ptNewItem
);

int LMT5_perform_dataset_migration
(
    FILE*  fLogFile,
    tag_t  tSrcItem,
    tag_t  tSrcAssyIR,
    int    iDSTypeCnt,
    char** ppszDSType,
    char*  pszDrtModelType,
    int    iItemCnt,
    tag_t* ptOldItem,
    tag_t* ptNewItem
);

int LMT5_validate_and_move_dataset
(
    FILE*  fLogFile,
    tag_t  tSrcIR,
    tag_t  tDestIR,
    char*  pszRelType, 
    int    iDSTypeCnt,
    char** ppszDSType,
    int    iItemCnt,
    tag_t* ptOldItem,
    tag_t* ptNewItem
);

int LMT5_migrate_sw_3d_dependency
(
    FILE*  fLogFile,
    tag_t  tItemRev,
    tag_t  tSWDataset,
    char*  pszRelType,
    int    iDependencyRevCnt,
    tag_t* ptSWDepIR, 
    tag_t* ptSWRelObj,
    int    iItemCnt, 
    tag_t* ptOldItem, 
    tag_t* ptNewItem
);


#endif
