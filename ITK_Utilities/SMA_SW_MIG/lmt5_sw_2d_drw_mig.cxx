/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_sw_2d_drw_mig.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15-Oct-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <lmt5_errors.hxx>
#include <lmt5_library.hxx>
#include <lmt5_process_sw_migration.hxx>
#include <lmt5_sw_2d_drw_mig.hxx>


/**
 * This function performs the Solidworks 2D dataset migration.
 *
 * @param[in] fLogFile
 * @param[in] iItemCnt
 * @param[in] ptOldItem
 * @param[in] ptNewItem
 * @param[in] bDryRun 
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_perform_2d_migration
(
    FILE*   fLogFile,
    int     iItemCnt,
    tag_t*  ptOldItem,
    tag_t*  ptNewItem,
    int     iDepMigItemCnt,
    tag_t*  ptDepMigOldItem,
    tag_t*  ptDepMigNewItem,
    logical bDryRun,
    logical bIgnCreaDate
)
{
    int    iRetCode       = ITK_ok;
    int    iDSTypeCnt     = 0;
    char** ppszDSTypeList = NULL;
    
    LMT5_CUST_UPDATE_STRING_ARRAY(iDSTypeCnt, ppszDSTypeList, LMT5_CLASS_SOLIDWORKS_DRAWING);

    for( int iDx = 0; iDx < iItemCnt; iDx++)
    {
        int     iSWDepIRRelCnt     = 0;
        int     iNewDepMigInfoCnt  = 0;
        tag_t   tSrcIR             = NULLTAG;
        tag_t   tTargIR            = NULLTAG;
        tag_t   tSWDrwDS           = NULLTAG;
        tag_t   tIRAndDrwDSRelObj  = NULLTAG;
        tag_t*  ptSWDepIRRelObj    = NULL;
        tag_t*  ptNewDepMigInfoObj = NULL;
        logical bFoundError        = false;

        LMT5_ITKCALL(iRetCode, LMT5_check_pre_condition_for_2d_migration(fLogFile, ptOldItem[iDx], ptNewItem[iDx], iDSTypeCnt,  ppszDSTypeList,
                                                                          iDepMigItemCnt, ptDepMigOldItem, ptDepMigNewItem, &tSrcIR, &tTargIR,
                                                                          &tSWDrwDS, &tIRAndDrwDSRelObj, &iSWDepIRRelCnt, &ptSWDepIRRelObj, 
                                                                          &iNewDepMigInfoCnt, &ptNewDepMigInfoObj, &bFoundError, bIgnCreaDate));

        if(bDryRun == false && bFoundError == false && tSWDrwDS != NULLTAG && iRetCode == ITK_ok)
        {
            LMT5_ITKCALL(iRetCode, LMT5_migrate_2d_sw_ds_and_dependency(fLogFile, tSrcIR, tSWDrwDS, tIRAndDrwDSRelObj, iSWDepIRRelCnt,
                                                                         ptSWDepIRRelObj, tTargIR, iNewDepMigInfoCnt, ptNewDepMigInfoObj));
        }

        if(iRetCode  != ITK_ok)
        {
            /* Get OOTB Error Stored on the Stack and Write it in Log File */
            LMT5_REPORT_OOTB_ERROR(iRetCode, fLogFile);

            /* Suppress error if error occurs so as to process rest of the objects. */
            iRetCode  = ITK_ok;
        }

        LMT5_MEM_TCFREE(ptSWDepIRRelObj);
        LMT5_MEM_TCFREE(ptNewDepMigInfoObj);
    }

    LMT5_MEM_TCFREE(ppszDSTypeList);
    return iRetCode;
}


/**
 * This function assumes that 2D migration is required for latest revision of input source Item. Function gets the 
 * latest revision and validated if only one SWDrw dataset is attached to the source revision if validation fails
 * then error is reported in the log file.
 *
 * @param[in]  fLogFile
 * @param[in]  tItem
 * @param[in]  iDSTypeCnt
 * @param[in]  ppszDSTypeList
 * @param[out] ptSrcIR
 * @param[out] ptDrwDS
 * @param[out] ptIRAndDrwDSRelObj
 * @param[out] ppszIRObjStr  
 * @param[out] pbFoundError
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_check_pre_condition_for_2d_migration
(
    FILE*    fLogFile, 
    tag_t    tOldItem, 
    tag_t    tNewItem, 
    int      iDSTypeCnt,  
    char**   ppszDSTypeList,
    int      iDepMigItemCnt,
    tag_t*   ptDepMigOldItem,
    tag_t*   ptDepMigNewItem,
    tag_t*   ptSrcIR, 
    tag_t*   ptTargIR, 
    tag_t*   ptSWDrwDS, 
    tag_t*   ptIRAndDrwDSRelObj,
    int*     piSWDepIRRelCnt,
    tag_t**  pptSWDepIRRelObj, 
    int*     piNewDepMigInfoCnt, 
    tag_t**  pptNewDepMigInfoObj,
    logical* pbFoundError,
    logical  bIgnCreaDate
)
{
    int     iRetCode          = ITK_ok;
    tag_t   tSWAsmOrPrtDS     = NULLTAG;
    tag_t*  ptSWDepIRRelObj   = NULL;
    char*   pszSrcIRObjStr    = NULL;
    char*   pszTrgObjStr      = NULL;
    logical bSrcIRError       = false;
    logical bTargIRError      = false;
    logical bIsRelBefore      = false;
    logical bSWIMDepError     = false;

    /* Get latest revision of source Item and check if source revision has none or more than one SW Drawing dataset.  */
    LMT5_ITKCALL(iRetCode, LMT5_get_and_validate_src_sw_ds(fLogFile, tOldItem, iDSTypeCnt, ppszDSTypeList, ptSrcIR, 
                                                             ptSWDrwDS, ptIRAndDrwDSRelObj, &pszSrcIRObjStr, &bSrcIRError)); 

    /*  Get latest revision of target Item and check that target revision should not have SW Drawing dataset and should not have
        more than one Assembly or Part dataset and also Assembly or Part dataset should not be present on the same revision.
    */
    LMT5_ITKCALL(iRetCode, LMT5_validate_target_ir_and_sw_ds(fLogFile, tNewItem, ptTargIR, &tSWAsmOrPrtDS, &pszTrgObjStr,
                                                              &bTargIRError));
    
    if(bSrcIRError == false && bTargIRError == false && (*ptSWDrwDS) != NULLTAG && iRetCode == ITK_ok)
    {
        if(bIgnCreaDate == false)
        {
            /*  If target latest revision has any status then validate that item revision should not be
                released before creation date of Drawing dataset in case of 2D migration.
            */
            LMT5_ITKCALL(iRetCode, LMT5_validate_ir_rel_and_ds_creation((*ptTargIR), (*ptSWDrwDS), &bIsRelBefore));
        }

        if(bIsRelBefore == true)
        {           
            LMT5_REPORT_IR_RELEASE_DS_CREATION_DATE_MISMATCH_ERROR(fLogFile, (*ptSWDrwDS), pszTrgObjStr, pszSrcIRObjStr);
        }

        /*  Construct list of new Item revision that will replace SWIM dependency revision of 2D datasets. Also validate
            if target revision has SW part or assembly dataset in case if one of the dependency revision will be replaced by
            target revision.
        */
        LMT5_ITKCALL(iRetCode, LMT5_validate_dependency_and_get_3d_rev_info(fLogFile, pszSrcIRObjStr, (*ptSWDrwDS), (*ptTargIR), tSWAsmOrPrtDS,
                                                                            iDepMigItemCnt, ptDepMigOldItem, ptDepMigNewItem, piSWDepIRRelCnt,
                                                                            pptSWDepIRRelObj, piNewDepMigInfoCnt, pptNewDepMigInfoObj,
                                                                            &bSWIMDepError));
    }

    if(bSrcIRError == true || bTargIRError == true|| bIsRelBefore == true || bSWIMDepError == true)
    {
        (*pbFoundError) = true;
    }

    LMT5_MEM_TCFREE(ptSWDepIRRelObj);
    LMT5_MEM_TCFREE(pszSrcIRObjStr);
    LMT5_MEM_TCFREE(pszTrgObjStr);
    return iRetCode;
}

/**
 * This function assumes that 2D migration is required for latest revision of input source Item. Function gets the 
 * latest revision and validated if only one SWDrw dataset is attached to the source revision if validation fails
 * then error is reported in the log file.
 *
 * @param[in]  fLogFile
 * @param[in]  tItem
 * @param[in]  iDSTypeCnt
 * @param[in]  ppszDSTypeList
 * @param[out] ptSrcIR
 * @param[out] ptDrwDS
 * @param[out] ptIRAndDrwDSRelObj
 * @param[out] ppszIRObjStr  
 * @param[out] pbFoundError
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_get_and_validate_src_sw_ds
(
    FILE*    fLogFile,
    tag_t    tItem,
    int      iDSTypeCnt,
    char**   ppszDSTypeList,
    tag_t*   ptSrcIR,
    tag_t*   ptDrwDS,
    tag_t*   ptIRAndDrwDSRelObj,
    char**   ppszIRObjStr,
    logical* pbFoundError
)
{
    int     iRetCode      = ITK_ok;
    int     iDSCnt        = 0;
    tag_t*  ptSWDrwDS     = NULL;
    tag_t*  ptSWDrwRelObj = NULL;

    /* Initializing the Out Parameter to this function. */
    (*ptDrwDS)      = NULLTAG;
    (*ppszIRObjStr) = NULL;
    (*pbFoundError) = false;

    LMT5_ITKCALL(iRetCode, ITEM_ask_latest_rev(tItem, ptSrcIR));

    LMT5_ITKCALL(iRetCode, AOM_ask_value_string((*ptSrcIR), LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, ppszIRObjStr));

    LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type((*ptSrcIR), LMT5_IMAN_SPECIFICATION_RELATION, iDSTypeCnt, 
                                                            ppszDSTypeList, NULL, false, &ptSWDrwDS, &ptSWDrwRelObj, &iDSCnt));
    if(iDSCnt > 1)
    {
        /* Report Error in log file: More than one Drawing DS are attached to IR */
        (*pbFoundError) = true;
        fprintf(fLogFile, "SW Dataset Error: More than one Solidworks Drawing datasets are attached to IR '%s'.\n", (*ppszIRObjStr));
        fflush(fLogFile);
    }
    else
    {
        if(iDSCnt == 1)
        {
            (*ptDrwDS) = ptSWDrwDS[iDSCnt-1];
            (*ptIRAndDrwDSRelObj) = ptSWDrwRelObj[iDSCnt-1];
        }
        else
        {
            fprintf(fLogFile, "SW Dataset Info: No Solidworks Drawing datasets is attached to IR '%s'.\n", (*ppszIRObjStr));
            fflush(fLogFile);
        }
    }
    
    LMT5_MEM_TCFREE(ptSWDrwDS);
    LMT5_MEM_TCFREE(ptSWDrwRelObj);
    return iRetCode;
}

/**
 * This function checks if Target revision to which 2D dataset will be migrated does not have any drawing dataset attached to it.
 * Function also reports error if there are more than one SWAsm or SWPart dataset sets are attached to target revision and error
 * is also report if both SWAsm and SWPrt datasets are attached to Target revision.
 *
 * @param[in]  fLogFile
 * @param[in]  tItem
 * @param[in]  ptTargIR
 * @param[out] ptSWAsmOrPrtDS
 * @param[out] ppszIRObjStr
 * @param[out] pbFoundError 
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_validate_target_ir_and_sw_ds
(
    FILE*    fLogFile,
    tag_t    tItem,
    tag_t*   ptTargIR,
    tag_t*   ptSWAsmOrPrtDS,
    char**   ppszIRObjStr,
    logical* pbFoundError
)
{
    int    iRetCode       = ITK_ok;
    int    iDSCnt         = 0;
    int    iTypeCnt       = 0;
    int    iDSTypeCnt     = 0;
    int    iSWDSCnt       = 0;
    tag_t* ptSWDrwDS      = NULL;
    tag_t* ptSWDrwRelObj  = NULL;
    tag_t* ptSWDDS        = NULL;
    tag_t* ptSWDRelObj    = NULL;
    char** ppszDSTypeList = NULL;
    char** ppszTypeList   = NULL;

    /* Initializing the Out Parameter to this function. */
    (*ptSWAsmOrPrtDS) = NULLTAG;
    (*ppszIRObjStr)   = NULL;
    (*pbFoundError)   = false;

    LMT5_CUST_UPDATE_STRING_ARRAY(iDSTypeCnt, ppszDSTypeList, LMT5_CLASS_SOLIDWORKS_DRAWING);
    LMT5_CUST_UPDATE_STRING_ARRAY(iTypeCnt, ppszTypeList, LMT5_CLASS_SOLIDWORKS_ASSEMBLY);
    LMT5_CUST_UPDATE_STRING_ARRAY(iTypeCnt, ppszTypeList, LMT5_CLASS_SOLIDWORKS_PART);

    LMT5_ITKCALL(iRetCode, ITEM_ask_latest_rev(tItem, ptTargIR));

    LMT5_ITKCALL(iRetCode, AOM_ask_value_string((*ptTargIR), LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, ppszIRObjStr));
    /* Get SW Drawing DS attached to the Target Revision */
    LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type((*ptTargIR), LMT5_IMAN_SPECIFICATION_RELATION, iDSTypeCnt, 
                                                            ppszDSTypeList, NULL, false, &ptSWDrwDS, &ptSWDrwRelObj, &iDSCnt));
    /* Get SW Assembly or Part DS attached to the Target Revision */
    LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type((*ptTargIR), LMT5_IMAN_SPECIFICATION_RELATION, iTypeCnt, 
                                                             ppszTypeList, NULL, false, &ptSWDDS, &ptSWDRelObj, &iSWDSCnt));
    if(iDSCnt != 0)
    {
        /* Report Error in log file: New Target revision have More than one Drawing DS attached */
        (*pbFoundError) = true;
        fprintf(fLogFile, "SW Dataset Error:  New Target IR '%s' already have Solidworks Drawing dataset attached to it.\n", (*ppszIRObjStr));
        fflush(fLogFile);
    }

    if(iSWDSCnt > 1)
    {
        /* Report Error in log file: New Target revision have More than one Assembly DS attached */
        (*pbFoundError) = true;
        fprintf(fLogFile, "SW Dataset Error:  New Target IR '%s' has more than one Solidworks Assembly or Part datasets are attached to it.\n", (*ppszIRObjStr));
        fflush(fLogFile);
    }
    else
    {
        if(iSWDSCnt == 1)
        {
            (*ptSWAsmOrPrtDS) = ptSWDDS[iSWDSCnt-1];
        }
    }

    LMT5_MEM_TCFREE(ptSWDrwDS);
    LMT5_MEM_TCFREE(ptSWDrwRelObj);
    LMT5_MEM_TCFREE(ptSWDDS);
    LMT5_MEM_TCFREE(ptSWDRelObj);
    LMT5_MEM_TCFREE(ppszDSTypeList);
    LMT5_MEM_TCFREE(ppszTypeList);
    return iRetCode;
}

/**
 * This function migrates the 
 *
 * @param[in]   fLogFile
 * @param[in]   tItemRev
 * @param[in]   tSWDataset
 * @param[in]   pszRelType
 * @param[in]   iDependencyRevCnt
 * @param[in]   ptSWDepIR
 * @param[in]   ptSWRelObj
 * @param[in]   iItemCnt
 * @param[in]   ptOldItem
 * @param[in]   ptNewItem 
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_validate_dependency_and_get_3d_rev_info
(
    FILE*    fLogFile,
    char*    pszSrcIRObjStr,
    tag_t    tSWDrwDS,
    tag_t    tTargIR,
    tag_t    tTargSWAsmOrPrtDS,
    int      iDepMigItemCnt,
    tag_t*   ptDepMigOldItem,
    tag_t*   ptDepMigNewItem, 
    int*     piSWDepIRRelCnt,
    tag_t**  pptSWDepIRRelObj,
    int*     piNewDepMigInfoCnt,
    tag_t**  pptNewDepMigInfoObj,
    logical* pbFoundError
)
{
    int    iRetCode     = ITK_ok;
    tag_t* ptSWDrwDepIR = NULL;

    /* Initializing the Out Parameter to this function. */
    (*piSWDepIRRelCnt)     = 0;
    (*piNewDepMigInfoCnt)  = 0;
    (*pptSWDepIRRelObj)    = NULL;
    (*pptNewDepMigInfoObj) = NULL;

    LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type(tSWDrwDS, LMT5_SWIM_DEPENDENCY_RELATION, 0, NULL, NULL,
                                                              false, &ptSWDrwDepIR, pptSWDepIRRelObj, piSWDepIRRelCnt));
    if((*piSWDepIRRelCnt) > 0 && iDepMigItemCnt <= 0)
    {
        /* Report Error in log file: SWIM Dependency revision exist by the Dependency mapping file is empty. */
        (*pbFoundError) = true;
        fprintf(fLogFile, "SW Dependency Error: SW Drawing dataset attached to IR '%s' has SWIM Dependency but the input Dependency Mapping file provided to the utility is empty.\n", pszSrcIRObjStr);
        fflush(fLogFile);
    }
    else
    {
        for(int iDx = 0; iDx < (*piSWDepIRRelCnt) && iRetCode == ITK_ok; iDx++)
        {
            tag_t   tDepItem     = NULLTAG;
            logical bFoundNewIR  = false;
            logical bReportDepER = false;
            
            LMT5_ITKCALL(iRetCode, AOM_ask_value_tag(ptSWDrwDepIR[iDx], LMT5_ATTR_ITEM_TAG, &tDepItem));

            for(int iFx = 0; iFx < iDepMigItemCnt && iRetCode == ITK_ok; iFx++)
            {
                if(tDepItem == ptDepMigOldItem[iFx])
                {
                    tag_t tDepMigNewIR = NULLTAG;

                    bFoundNewIR = true;

                    LMT5_ITKCALL(iRetCode, ITEM_ask_latest_rev(ptDepMigNewItem[iFx], &tDepMigNewIR));

                    if(iRetCode == ITK_ok)
                    {
                        if(tDepMigNewIR == tTargIR)
                        {
                            if(tTargSWAsmOrPrtDS != NULLTAG)
                            {
                                LMT5_UPDATE_TAG_ARRAY((*piNewDepMigInfoCnt), (*pptNewDepMigInfoObj), tTargSWAsmOrPrtDS);
                            }
                            else
                            {
                                /* Report Error in log file: SWIM Dependency revision does not exist by the Dependency mapping file. */
                                bReportDepER = true;
                                LMT5_REPORT_TARG_REV_MISSING_ASM_OR_PRT_DS_SWIM_DEP_ERROR(fLogFile, ptSWDrwDepIR[iDx], tTargIR, pszSrcIRObjStr);
                            }
                        }
                        else
                        {
                            LMT5_UPDATE_TAG_ARRAY((*piNewDepMigInfoCnt), (*pptNewDepMigInfoObj), tDepMigNewIR);
                        }

                        break;
                    }
                }
            }

            if(bFoundNewIR == false && iRetCode == ITK_ok)
            {
                /* Report Error in log file: SWIM Dependency revision does not exist by the Dependency mapping file. */
                (*pbFoundError) = true;
                LMT5_REPORT_2D_SWIM_DEP_IR_NOT_FOUND_IN_MAPPING_FILE(fLogFile, ptSWDrwDepIR[iDx], pszSrcIRObjStr);
                break;
            }

            if(bReportDepER == true)
            {
                (*pbFoundError) = true;
                break;
            }   
        }
    }
  
    LMT5_MEM_TCFREE(ptSWDrwDepIR);
    return iRetCode;
}



/**
 * This function migrates the 
 *
 * @param[in]   fLogFile
 * @param[in]   tItemRev
 * @param[in]   tSWDataset
 * @param[in]   pszRelType
 * @param[in]   iDependencyRevCnt
 * @param[in]   ptSWDepIR
 * @param[in]   ptSWRelObj
 * @param[in]   iItemCnt
 * @param[in]   ptOldItem
 * @param[in]   ptNewItem 
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_migrate_2d_sw_ds_and_dependency
(
    FILE*  fLogFile,
    tag_t  tSrcIR,
    tag_t  tSWDrwDS,
    tag_t  tIRAndDrwDSRelObj,
    int    iObjCnt,
    tag_t* ptSWDepIRRelObj,
    tag_t  tTargIR,
    int    iNewDepMigInfoCnt, 
    tag_t* ptNewDepMigInfoObj
)
{
    int   iRetCode      = ITK_ok;
    int   iMarkPoint    = 0;
    tag_t tRelation     = NULLTAG;
    tag_t tRelDrwTo3DDS = NULLTAG;

    /* Define a markpoint for database transaction */
    LMT5_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));
                
    for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
    {
        LMT5_ITKCALL(iRetCode, LMT5_grm_delete_relation(tSWDrwDS, ptSWDepIRRelObj[iDx]));
    }

    /* Delete existing relation */
    LMT5_ITKCALL(iRetCode, LMT5_grm_delete_relation(tSrcIR, tIRAndDrwDSRelObj));

    /* Create new relation */
    LMT5_ITKCALL(iRetCode, LMT5_grm_create_relation(tTargIR, tSWDrwDS, LMT5_IMAN_SPECIFICATION_RELATION, &tRelation));

    /* Remove existing status if any from dataset */
    LMT5_ITKCALL(iRetCode, LMT5_obj_dereference_status(tSWDrwDS));

    /* Associating status of New Item revision to the dataset */
    LMT5_ITKCALL(iRetCode, LMT5_obj_set_src_obj_status_to_dest(tTargIR, tSWDrwDS));

    if(iNewDepMigInfoCnt > 0)
    {
        for(int iZx = 0; iZx < iNewDepMigInfoCnt && iRetCode == ITK_ok; iZx++)
        {
            /* Create new relation */
            LMT5_ITKCALL(iRetCode, LMT5_grm_create_relation(tSWDrwDS, ptNewDepMigInfoObj[iZx], LMT5_SWIM_DEPENDENCY_RELATION, &tRelDrwTo3DDS));
        }
    }

        /* Commit the information into the database. */
    LMT5_ITKCALL(iRetCode, POM_forget_markpoint(iMarkPoint));

    if(iRetCode != ITK_ok)
    {
        logical bStateChanged = false;

        /* Do the rollback of the database. */
        POM_roll_to_markpoint(iMarkPoint, &bStateChanged);

        /* Suppress error if error occurs so as to process rest of the objects. */
        iRetCode  = ITK_ok;
    }

    return iRetCode;
}
