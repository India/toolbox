/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_sw_3d_mig.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15-Oct-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <lmt5_errors.hxx>
#include <lmt5_library.hxx>
#include <lmt5_process_sw_migration.hxx>
#include <lmt5_sw_3d_mig.hxx>


/**
 * This function tries to get 3D top assembly Item revision with Item ID 'pszItemID' and revision ID 'pszRevID' in the system. If 'pszRevID' 
 * is NULL function will get the latest revision for the Item.
 *
 * @param[in]  fLogFile   Pointer to log file for reporting information.
 * @param[in]  pszItemID  Assembly Item ID
 * @param[in]  pszRevID   Revision ID
 * @param[out] ptAssyRev  Tag of desired top assembly revision.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_find_asm_info
(
    FILE*   fLogFile,
    char*   pszItemID,
    char*   pszRevID,
    tag_t*  ptAssyRev
)
{
    int    iRetCode    = ITK_ok;
    int    iItemCnt    = 0;
    tag_t* ptAssyItem  = NULL;

    LMT5_ITKCALL(iRetCode, ITEM_find(pszItemID, &iItemCnt, &ptAssyItem));

    if(iItemCnt == 1)
    {
        if((pszRevID == NULL) || (tc_strlen(pszRevID) == 0))
        {
            LMT5_ITKCALL(iRetCode, ITEM_ask_latest_rev(ptAssyItem[iItemCnt-1], ptAssyRev));
        }
        else
        {
            LMT5_ITKCALL(iRetCode, ITEM_find_revision(ptAssyItem[iItemCnt-1], pszRevID, ptAssyRev));
        }
    }
    else
    {
        if(iItemCnt == 0)
        {
            /* No Item with this ID exist in the system */
            iRetCode = CUSTOM_ERROR_SEE_LOG_FILE;
            fprintf(fLogFile, "Error: No Item with '%s' ID exist in the system.\n", pszItemID);
            fflush(fLogFile);
            printf("No Item with '%s' ID exist in the system.\n", pszItemID);
        }
        else
        {
            /* More than one Item with this ID exist in the system */
            iRetCode = CUSTOM_ERROR_SEE_LOG_FILE;
            fprintf(fLogFile, "Error: More than one Item with '%s' ID exist in the system.\n", pszItemID);
            fflush(fLogFile);
            printf("Error: More than one Item with '%s' ID exist in the system.\n", pszItemID);
        }
    }

    LMT5_MEM_TCFREE(ptAssyItem);
    return iRetCode;
}

/**
 * This function comprises of different function calls which will perform 3D migration. 3D migration included execution of following
 * steps:
 * 1. The source assembly revision 'tAssyRev' is traversed and for each component new Item ID is retrieved from the mapping file.
 * 2. All components of assembly will be validate using following criteria:
 *       a. 
 * 3. If all the components passes the validation criteria then new BOM will be constructed using the corresponding new Item ID of the
 *    old assembly components which are mentioned in the mapping file.
 * 4. IF BOM creation is successful then the 3D datasets will be migrated from Old assembly components to new assembly components.
 * 5. Migrated dataset will inherit the status of new IR revision to which this dataset is getting attached.
 * 6. Solidworks 3D dataset contains Item revisions that are associated through relation SWIM dependency and SWIM suppressed dependency.
 *    These are old revision and will be replaced with corresponding new revisions that are will be retrieved from mapping file. All old
 *    dependency will be removed and new dependency will be associated with the dataset.
 *
 * @param[in] fLogFile   Pointer to log file for reporting information.
 * @param[in] tAssyRev   Tag of Top assembly Item revision
 * @param[in] pszRevRule Revision rule for expanding the assembly
 * @param[in] iItemCnt   Count of Item ID in mapping file
 * @param[in] ptOldItem  List of Old Item ID's from the mapping file whose latest revision needs to be migrated.
 * @param[in] ptNewItem  List of New Item ID's from the mapping file whose latest revision will receive 3Dmigrated dataset.
 * @param[in] bSkipBOM   If 'true' then new BOM creation will be skipped.
 * @param[in] bDryRun    If 'true' then only validation for 3D migration will be performed. No migration will be executed.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_perform_3d_migration
(
    FILE*   fLogFile,
    tag_t   tAssyRev,
    char*   pszRevRule,
    int     iItemCnt,
    tag_t*  ptOldItem,
    tag_t*  ptNewItem,
    logical bSkipBOM,
    logical bDryRun,
    logical bIgnCreaDate
)
{
    int   iRetCode   = ITK_ok;
    tag_t tBomWindow = NULLTAG;
    tag_t tTopLine   = NULLTAG;

    /* Set the item revision as the top line and creates a new BOM Window */
    LMT5_ITKCALL(iRetCode, LMT5_bomline_get_top_line(tAssyRev, pszRevRule, LMT5_BOM_VIEW_TYPE_VIEW, &tBomWindow, &tTopLine));
    
    if(tBomWindow != NULLTAG)
    {
        int     iBLCount    = 0;
        tag_t*  ptItems     = NULL;
        tag_t*  ptChildIRs  = NULL;
        tag_t*  ptChildBLs  = NULL;
        logical bFoundError = false;

        /* Gets all the children bonmline tags for the given parent BOMline*/
        LMT5_ITKCALL(iRetCode, LMT5_bom_list_structure(tTopLine, 0, -1, false, &iBLCount, &ptItems, &ptChildIRs, &ptChildBLs));
        
        for(int iDx = 0; iDx < iBLCount && iRetCode == ITK_ok; iDx++)
        {
            tag_t   tTargetIR = NULLTAG;
            logical bFound    = false;

            for(int iNx = 0; iNx < iItemCnt && iRetCode == ITK_ok; iNx++)
            {
                if(ptOldItem[iNx] == ptItems[iDx])
                {
                    LMT5_ITKCALL(iRetCode, ITEM_ask_latest_rev(ptNewItem[iNx], &tTargetIR));
                    bFound = true;
                    break;
                }
            }
            
            if(bFound == false)
            {
                char* pszObjStr = NULL;

                if(bFoundError == false)
                {
                    bFoundError = true;
                }

                AOM_ask_value_string(ptChildIRs[iDx], LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr);
                /* Log Error: Item Revision present in the BOM are not present in the Mapping File */
                fprintf(fLogFile, "Error: Item Revision '%s' present in the BOM are not present in the Mapping File.\n", pszObjStr);
                fflush(fLogFile);

                LMT5_MEM_TCFREE( pszObjStr );
            }
            else
            {
                LMT5_ITKCALL(iRetCode, LMT5_check_3d_mig_condition(fLogFile, ptChildIRs[iDx], ptChildBLs[iDx],
                                                                      tTargetIR, &bFoundError, bIgnCreaDate));
            }
        }

        if(bFoundError == false && bDryRun == false)
        {
            if(bSkipBOM == false)
            {
                int iMarkPoint = 0;

                /* Define a markpoint for database transaction */
                LMT5_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));

                /* Construct BOM */
                LMT5_ITKCALL(iRetCode, LMT5_construct_imprecise_bom(tAssyRev, pszRevRule, iItemCnt, ptOldItem,
                                                                     ptNewItem, iBLCount, ptItems, ptChildIRs, ptChildBLs));

                /* Commit the information into the database. */
                LMT5_ITKCALL(iRetCode, POM_forget_markpoint(iMarkPoint));

                if (iRetCode != ITK_ok)
                {
                    logical bStateChanged = false;

                    /* Do the rollback of the database. */
                    POM_roll_to_markpoint(iMarkPoint, &bStateChanged);
                }
            }

            /* Migrate Dataset */
            LMT5_ITKCALL(iRetCode, LMT5_migrate_sw_3d_dataset(fLogFile, tTopLine, tAssyRev, iBLCount, ptChildIRs, ptItems, 
                                                         iItemCnt, ptOldItem, ptNewItem));
        }

        LMT5_MEM_TCFREE( ptItems );
        LMT5_MEM_TCFREE( ptChildIRs );
        LMT5_MEM_TCFREE( ptChildBLs );
    }

    LMT5_ITKCALL(iRetCode, BOM_close_window(tBomWindow));

    return iRetCode;
}

/**
 * This function checks if source Item revision present in the BOM has only one SWPrt or SWAsm dataset attached to it. If target
 * revision to which 3D dataset needs to be migrated has any status then this function validate that target item revision should not be
 * released before creation date of Assembly or Part dataset of source Item revision which needs to be migrated.
 *
 * @param[in]  fLogFile     Pointer to log file for reporting information.
 * @param[in]  tCurrentIR   BOM Line Item revision tag which needs to be validated
 * @param[in]  tCurrentBL   Tag of BOM Line which needs to be validated.
 * @param[in]  tTargetIR    Tag of Target revision to which 3D data will be migrated
 * @param[out] pbFoundError Set to 'true' if validation criteria is not achieved.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_check_3d_mig_condition
(
    FILE*    fLogFile,
    tag_t    tCurrentIR, 
    tag_t    tCurrentBL,
    tag_t    tTargetIR,
    logical* pbFoundError,
    logical  bIgnCreaDate
)
{
    int    iRetCode           = ITK_ok;
    int    iDSCnt             = 0;
    int    iObjCnt            = 0;
    tag_t* ptSWAsmOrPrtDS     = NULL;
    tag_t* ptSWAsmOrPrtRelObj = NULL;
    char** ppszObjTypeList    = NULL;

    LMT5_CUST_UPDATE_STRING_ARRAY(iObjCnt, ppszObjTypeList, LMT5_CLASS_SOLIDWORKS_ASSEMBLY);
    LMT5_CUST_UPDATE_STRING_ARRAY(iObjCnt, ppszObjTypeList, LMT5_CLASS_SOLIDWORKS_PART);

    /* Validating SW dependency */
    LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type(tCurrentIR, LMT5_IMAN_SPECIFICATION_RELATION, iObjCnt, ppszObjTypeList,
                                                             NULL, false, &ptSWAsmOrPrtDS, &ptSWAsmOrPrtRelObj, &iDSCnt));
    if(iRetCode == ITK_ok && iDSCnt > 0)
    {
        if(iDSCnt > 1)
        { 
            char* pszObjStr = NULL;

            /* Report Error in log file: More than one Assembly or Part DS are attached to IR */
            if((*pbFoundError) == false)
            {
                (*pbFoundError) = true;
            }

            AOM_ask_value_string(tCurrentIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr);
            fprintf(fLogFile, "SW Dependency Error: More than one Solidworks 3D datasets are attached to IR '%s'.\n", pszObjStr);
            fflush(fLogFile);

            LMT5_MEM_TCFREE( pszObjStr );
        }
        else
        {
            logical bIsSWAsmDS = false;

            LMT5_ITKCALL(iRetCode, LMT5_obj_is_type_of(ptSWAsmOrPrtDS[iDSCnt-1], LMT5_CLASS_SOLIDWORKS_ASSEMBLY, &bIsSWAsmDS));

            if(bIsSWAsmDS == true)
            {
                LMT5_ITKCALL(iRetCode, LMT5_validate_3d_asm_ds_dependency(fLogFile, tCurrentIR, tCurrentBL, ptSWAsmOrPrtDS[iDSCnt-1], pbFoundError));
            }

            if(ptSWAsmOrPrtDS[iDSCnt-1] != NULLTAG && tTargetIR != NULLTAG && bIgnCreaDate == false)
            {
                /*  If target latest revision has any status then validate that item revision should not be
                    released before creation date of Assembly or Part dataset in case of 3D migration.
                */
                logical bIsRelBefore = false;

                LMT5_ITKCALL(iRetCode, LMT5_validate_ir_rel_and_ds_creation(tTargetIR, ptSWAsmOrPrtDS[iDSCnt-1], &bIsRelBefore));

                if(bIsRelBefore == true)
                {
                    char* pszSrcObjStr = NULL;
                    char* pszTrgObjStr = NULL;
                    char* pszDSObjStr  = NULL;

                    if((*pbFoundError) == false)
                    {
                        (*pbFoundError) = true;
                    }

                    AOM_ask_value_string(tTargetIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszTrgObjStr);
                    AOM_ask_value_string(tCurrentIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszSrcObjStr);
                    AOM_ask_value_string(ptSWAsmOrPrtDS[iDSCnt-1], LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszDSObjStr);
                    fprintf(fLogFile, "SW Error: Released date of IR '%s' is earlier than creation date of dataset '%s' which is attached to IR '%s'.\n",pszTrgObjStr, pszDSObjStr, pszSrcObjStr);
                    fflush(fLogFile);

                    LMT5_MEM_TCFREE( pszSrcObjStr );
                    LMT5_MEM_TCFREE( pszTrgObjStr );
                    LMT5_MEM_TCFREE( pszDSObjStr );
                }
            }
        }
    }
    
    LMT5_MEM_TCFREE( ppszObjTypeList );
    LMT5_MEM_TCFREE( ptSWAsmOrPrtDS );
    LMT5_MEM_TCFREE( ptSWAsmOrPrtRelObj );
    return iRetCode;
}

/**
 * This function checks the input BOM line for the following criteria:
 *  1. Number of unique child BOM line of the input BOM line 'tCurrentBL' should be equal to the number of Item revisions which are 
 *     associated to Solidworks Assembly dataset through relation 'SWIM_dependency' or 'SWIM_suppressed_dependency'.
 *
 * @note: Unique BOM Line indicated that any  Item revision reoccurring more than once as child BOM line will only be considered
 *        once for validation. Solidworks Assembly dataset is associated to BOM Line revision 'tCurrentIR'.
 *
 * @param[in]  fLogFile     Pointer to log file for reporting information.
 * @param[in]  tCurrentIR   BOM Line Item revision tag which needs to be validated
 * @param[in]  tCurrentBL   Tag of BOM Line which needs to be validated.
 * @param[in]  tSWAsmDS     Tag of assembly dataset which is attached to Iem revision which is represented by BOM Line 'tCurrentBL'
 * @param[out] pbFoundError Set to 'true' of validation criteria is not achieved.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_validate_3d_asm_ds_dependency
(
    FILE*    fLogFile,
    tag_t    tCurrentIR,
    tag_t    tCurrentBL,
    tag_t    tSWAsmDS,
    logical* pbFoundError
)
{ 
    int    iRetCode          = ITK_ok;
    int    iIRCnt            = 0;
    int    iSWBlCnt          = 0;
    int    iSuppresIRCnt     = 0;
    tag_t* ptSWDepItem       = NULL;
    tag_t* ptSWDepIR         = NULL;
    tag_t* ptSWRelObj        = NULL;
    tag_t* ptSWChildIR       = NULL;
    tag_t* ptSWChildBL       = NULL;
    tag_t* ptSWSuppresDepIR  = NULL;
    tag_t* ptSWSuppresRelObj = NULL;

    LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type(tSWAsmDS, LMT5_SWIM_DEPENDENCY_RELATION,
                                                             0, NULL, NULL, false, &ptSWDepIR, &ptSWRelObj, &iIRCnt));

    LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type(tSWAsmDS, LMT5_SWIM_SUPPRESSED_DEPENDENCY_RELATION,
                                                             0, NULL, NULL, false, &ptSWSuppresDepIR, &ptSWSuppresRelObj,
                                                             &iSuppresIRCnt));
    /* Extracting First level childs of input BOM Line */
    LMT5_ITKCALL(iRetCode, LMT5_bom_list_structure(tCurrentBL, 0, 1, true, &iSWBlCnt, &ptSWDepItem, &ptSWChildIR, &ptSWChildBL));

    if(iRetCode == ITK_ok)
    {
        if(iSWBlCnt == (iIRCnt + iSuppresIRCnt))
        {  
            /* Validating if Item Revisions list of SW dependency matches Item Revisions list of child BOM Line revisions
               that are extracted by 'LMT5_bom_list_structure' function.
            */
            for(int iYx = 0; iYx < iSWBlCnt && iRetCode == ITK_ok; iYx++)
            {
                logical bFoundIR = false;

                for(int iZx = 0; iZx < iIRCnt; iZx++)
                {
                    if(ptSWDepIR[iZx] == ptSWChildIR[iYx])
                    {
                        bFoundIR = true;
                        break;
                    }
                }

                /* Validating 'bFoundItem' variable in for loop restricts execution of this loop if Item revision is already
                   found in the above SWIM dependency validation loop.
                */
                for(int iHx = 0; iHx < iSuppresIRCnt && bFoundIR == false; iHx++)
                {
                    /*  If Item Revision list of child BOM Line revisions that are extracted by 'LMT5_bom_list_structure' function are not
                        present as part of SWIM dependency then validate if these Item revisions are present as SWIM suppressed dependency
                    */
                    if(ptSWSuppresDepIR[iHx] == ptSWChildIR[iYx])
                    {
                        bFoundIR = true;
                        break;
                    }
                }

                if(bFoundIR == false)
                {
                    char* pszDSObjStr      = NULL;
                    char* pszObjStr        = NULL;
                    char* pszSWDepIRObjStr = NULL;

                    /* Log Error: This Childs of assembly are not present in SW dependency IR */
                    if((*pbFoundError) == false)
                    {
                        (*pbFoundError) = true;
                    }

                    AOM_ask_value_string(tSWAsmDS, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszDSObjStr);
                    AOM_ask_value_string(tCurrentIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr);
                    AOM_ask_value_string(ptSWDepIR[iYx], LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszSWDepIRObjStr);
                    fprintf(fLogFile, "SW Dependency Error: Item Revision '%s' present as Solidworks dependency or suppressed dependency in datasets '%s' is not present as child BOM Line of Item revision '%s'.\n", pszSWDepIRObjStr, pszDSObjStr, pszObjStr);
                    fflush(fLogFile);

                    LMT5_MEM_TCFREE( pszDSObjStr );
                    LMT5_MEM_TCFREE( pszObjStr );
                    LMT5_MEM_TCFREE( pszSWDepIRObjStr );
                }
            }
        }
        else
        {
            /* Log Error: No of Child Item revision does not match the number of no. of SW dependency IR */
            if((*pbFoundError) == false)
            {
                (*pbFoundError) = true;
            }

            char* pszObjStr  = NULL;

            AOM_ask_value_string(tCurrentIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr);
            fprintf(fLogFile, "SW Dependency Error: Number of Item revisions attached to Solidworks assembly datasets is not the same as number of child BOM Line of Item revision '%s'.\n", pszObjStr);
            fflush(fLogFile);

            LMT5_MEM_TCFREE( pszObjStr );
        }
    }

    LMT5_MEM_TCFREE( ptSWDepItem );
    LMT5_MEM_TCFREE( ptSWDepIR );
    LMT5_MEM_TCFREE( ptSWRelObj );
    LMT5_MEM_TCFREE( ptSWChildIR );
    LMT5_MEM_TCFREE( ptSWChildBL );
    LMT5_MEM_TCFREE( ptSWSuppresDepIR );
    LMT5_MEM_TCFREE( ptSWSuppresRelObj );
    return iRetCode;
}

/**
 * This function clones the BOM assembly based on the existing assembly. All the child BOM lines of source assembly will
 * be replaced with new Item revision in the cloned assembly. Information about new item will be retrieved from the mapping file.
 * Some of the BOM Line attribute information is copied from the source BOM to newly constructed BOM.
 *
 * @note: Function has the intelligence to create multiple occurrence of same Item revision if required.
 *
 * @param[in] tAssyRev   Tag of Top assembly Item revision that needs to be migrated
 * @param[in] pszRevRule Revision rule to expand 'tAssyRev' BOM assembly 
 * @param[in] iItemCnt   Count of Item ID in mapping file
 * @param[in] ptOldItem  List of Old Item ID's from the mapping file whose latest revision needs to be migrated.
 * @param[in] ptNewItem  List of New Item ID's from the mapping file whose latest revision will receive 3Dmigrated dataset.
 * @param[in] iBLCount   Number of BOM Line in the 'tAssyRev' assembly
 * @param[in] ptItems    List of Item Tags of the corresponding child BOM Lines
 * @param[in] ptChildIRs List of Item revision Tags of the corresponding child BOM Lines
 * @param[in] ptChildBLs List of child BOM Lines
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_construct_imprecise_bom
(
    tag_t  tAssyRev,
    char*  pszRevRule, 
    int    iItemCnt,
    tag_t* ptOldItem,
    tag_t* ptNewItem,
    int    iBLCount, 
    tag_t* ptItems,
    tag_t* ptChildIRs, 
    tag_t* ptChildBLs
)
{
    int  iRetCode = ITK_ok;

    for(int iDx = 0; iDx < iBLCount && iRetCode == ITK_ok; iDx++)
    {
        tag_t tNewItem       = NULLTAG;
        tag_t tNewParentItem = NULLTAG;
        tag_t tOldChildBL    = NULLTAG;
        tag_t tParentBL      = NULLTAG;
        tag_t tParentItem    = NULLTAG;
        tag_t tParentIR      = NULLTAG;

        /* Get Parent BOM Line and Item revision of input child BOM Line. */
        LMT5_ITKCALL(iRetCode, LMT5_bomline_get_parent_bomline_item_and_revision(ptChildBLs[iDx], &tParentBL, 
                                                                                  &tParentItem, &tParentIR));
        
        /* Search for Destination Item tag for current child source BOM Line and parent source BOM Line */
        for(int iFx = 0; iFx < iItemCnt && iRetCode == ITK_ok; iFx++)
        {
            if(tNewItem != NULLTAG && tNewParentItem != NULLTAG)
            {
                break;
            }
            else
            {
                if(tNewItem == NULLTAG)
                {
                    if(ptOldItem[iFx] == ptItems[iDx])
                    {
                        tNewItem = ptNewItem[iFx];
                        tOldChildBL = ptChildBLs[iDx];
                    }
                }

                if(tNewParentItem == NULLTAG)
                {
                    if(ptOldItem[iFx] == tParentItem)
                    {
                        tNewParentItem = ptNewItem[iFx];
                    }
                }
            }
        }

        if(tNewItem != NULLTAG && tNewParentItem != NULLTAG)
        {
            int     iOldBLCount     = 0;
            tag_t   tNewIR          = NULLTAG;
            tag_t   tNewITBV        = NULLTAG;
            tag_t   tNewIRBVR       = NULLTAG;
            tag_t   tNewParentIR    = NULLTAG;
            tag_t   tNewParentITBV  = NULLTAG;
            tag_t   tNewParentITBVR = NULLTAG;
            tag_t*  ptBLines        = NULL;
            tag_t*  ptNewOcc        = NULL;
            logical bFoundExtraOcc  = false;

            LMT5_ITKCALL(iRetCode, ITEM_ask_latest_rev(tNewItem, &tNewIR));

            LMT5_ITKCALL(iRetCode, ITEM_ask_latest_rev(tNewParentItem, &tNewParentIR));

            LMT5_ITKCALL(iRetCode, BOM_line_ask_child_lines(tOldChildBL, &iOldBLCount, &ptBLines));

            /* This check avoid creation of BOM View and BOM view revision for those Item revision which does not 
               have any assembly.
            */
            if(iOldBLCount > 0)
            {
                LMT5_ITKCALL(iRetCode, LMT5_bom_get_bomview_revision(tNewItem, tNewIR, LMT5_BOM_VIEW_TYPE_VIEW,
                                                                        true, false, false, &tNewITBV, &tNewIRBVR));
            }

            LMT5_ITKCALL(iRetCode, LMT5_bom_get_bomview_revision(tNewParentItem, tNewParentIR, LMT5_BOM_VIEW_TYPE_VIEW,
                                                                    true, false, false, &tNewParentITBV, &tNewParentITBVR));
            
            /* This check is required to avoid recreation of ocurence if it has been created during early part
               of BOM traversal
            */
            LMT5_ITKCALL(iRetCode, LMT5_check_if_imprecise_occ_exist(tNewParentITBVR, tNewItem, tParentBL, ptChildIRs[iDx],
                                                                        &bFoundExtraOcc));
            
            if(bFoundExtraOcc == false && iRetCode == ITK_ok)
            {
                LMT5_ITKCALL(iRetCode, LMT5_ps_add_new_component(tNewParentITBVR, tNewItem, tNewITBV, 1, &ptNewOcc));

                 /* Reporting error when BOM creation fails. */
                if(iRetCode != ITK_ok)
                {
                    char* pszParITBVR = NULL;
                    char* pszItemID   = NULL;

                    AOM_ask_value_string(tNewParentITBVR, LMT3_ATTR_OBJECT_NAME_ATTRIBUTE, &pszParITBVR);
                    AOM_ask_value_string(tNewItem, LMT3_ATTR_ITEM_ID, &pszItemID);
                    
                    TC_write_syslog("ERROR: Error occurred while creating occurrence which links parent BOMView Revision  %s  and child Item  %s  \n.",  pszParITBVR, pszItemID );
                    
                    LMT5_MEM_TCFREE( pszParITBVR );
                    LMT5_MEM_TCFREE( pszItemID );
                }
                
                if(ptNewOcc[0] != NULLTAG)
                {
                    LMT5_ITKCALL(iRetCode, LMT5_set_ps_attributes(ptChildBLs[iDx], ptNewOcc[0], tNewParentITBVR));
                }
            }

            LMT5_MEM_TCFREE( ptNewOcc );
            LMT5_MEM_TCFREE( ptBLines );
        }
    }

    return iRetCode;
}

/**
 * This function is sub part of new BOM construction and provides intelligence to the BOM construction function to create multiple occurrence
 * of same Item revision. Function check if the count of particular Item revision as child BOM Lines in the source BOM is same or more than
 * number of occurrences of child BOM line for corresponding new item revision in the newly constructed BOM.
 *
 * @param[in]  tNewParentITBVR  Tag of newly created Parent BOM view revision
 * @param[in]  tNewItem         Tag of new Item
 * @param[in]  tSrcParentBL     Tag of Source Parent BOM Line
 * @param[in]  tSrcChildIR      Tag of child item revision whose number of occurrences needs to be validated.
 * @param[out] pbFoundExtraOcc  'true' if extra occurrences are found
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_check_if_imprecise_occ_exist
(
    tag_t    tNewParentITBVR, 
    tag_t    tNewItem,
    tag_t    tSrcParentBL,
    tag_t    tSrcChildIR,
    logical* pbFoundExtraOcc
)
{
    int    iRetCode       = ITK_ok;
    int    iOccCount      = 0;
    int    iDestSameIRCnt = 0;
    tag_t* ptOccurrences  = NULL;

    LMT5_ITKCALL(iRetCode, PS_list_occurrences_of_bvr(tNewParentITBVR, &iOccCount, &ptOccurrences));

    for(int iDx = 0; iDx < iOccCount && iRetCode == ITK_ok; iDx++)
    {
        tag_t tItem    = NULLTAG;
        tag_t tChildBV = NULLTAG;

        LMT5_ITKCALL(iRetCode, PS_ask_occurrence_child(tNewParentITBVR, ptOccurrences[iDx], &tItem, &tChildBV));

        if(tItem == tNewItem)
        {
            iDestSameIRCnt++;
        }
    }
    
    if(iRetCode == ITK_ok)
    {
        /* Logic is that it is possible that multiple occurrence of same item exist for the same parent BOM Line
           i.e. it is the case of adding same BOM Line as different occurrence. SO we have to check that number of 
           child Item revision occurrences should be less than the number of corresponding destination Item revision
           in the destination BOM.
           Very important check because this will also avoid creation of same sub assembly at multiple levels.
           i.e. if parent A has child B and C and this parent is present at two different places in BOM then reconstruction
           of parent A assembly will not be constructed again. 
        */
        if(iDestSameIRCnt == 0)
        {
            (*pbFoundExtraOcc) = false;
        }
        else
        {
            int    iSrcIRCnt    = 0;
            int    iSubAsmBLCnt = 0;
            tag_t* ptChildItems = NULL;
            tag_t* ptChildIRs   = NULL;
            tag_t* ptChildBLs   = NULL;

            /* Gets all the children bonmline tags for the given parent BOMline*/
            LMT5_ITKCALL(iRetCode, LMT5_bom_list_structure(tSrcParentBL, 0, 1, false, &iSubAsmBLCnt, &ptChildItems, 
                                                                    &ptChildIRs, &ptChildBLs));

            for(int iUx = 0; iUx < iSubAsmBLCnt && iRetCode == ITK_ok; iUx++)
            {
                if(ptChildIRs[iUx] == tSrcChildIR)
                {
                    iSrcIRCnt++;
                }
            }

            if(iDestSameIRCnt < iSrcIRCnt)
            {
                (*pbFoundExtraOcc) = false;

            }
            else
            {
                (*pbFoundExtraOcc) = true;
            }

            LMT5_MEM_TCFREE(ptChildItems);
            LMT5_MEM_TCFREE(ptChildIRs);
            LMT5_MEM_TCFREE(ptChildBLs);
        }
    }

    LMT5_MEM_TCFREE(ptOccurrences);
    return iRetCode;
}

/**
 * This function copies the BOM Line attribute from the source BOM Line to the newly constructed BOM Line.
 *
 * @param[in] tSourceChildBL  Tag of Source BOM Line from which BOM Line attribute information needs to be copied
 * @param[in] tNewPSOcc       Tag of target PS occurrence to which BOM Line attribute information needs to be set
 * @param[in] tParentITBVR    Tag of newly created parent BOM view revision.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_set_ps_attributes
(
    tag_t  tSourceChildBL,
    tag_t  tNewPSOcc,
    tag_t  tParentITBVR  
)
{
    int   iRetCode        = ITK_ok;
    char* pszSourceSeqNo  = NULL;   

    LMT5_ITKCALL(iRetCode, LMT5_bomline_get_attribute_val(tSourceChildBL, LMT5_ATTR_BL_SEQUENCE, &pszSourceSeqNo));
    
    LMT5_ITKCALL(iRetCode, AOM_refresh(tParentITBVR, true));

    LMT5_ITKCALL(iRetCode, PS_set_seq_no(tParentITBVR, tNewPSOcc, pszSourceSeqNo));

    LMT5_ITKCALL(iRetCode, AOM_save(tParentITBVR));

    LMT5_ITKCALL(iRetCode, AOM_refresh(tParentITBVR, false));

    LMT5_MEM_TCFREE( pszSourceSeqNo );
    return iRetCode;
}

/**
 * This function comprises of sub functions which will move 3D dataset from all the components of source assembly to respective
 * new Item revisions whose Item information is present in the migration mapping file. Function further update the SWIM dependencies.
 *
 * @param[in] fLogFile      Pointer to log file for reporting information.
 * @param[in] tTopLine      Tag of Top BOM Line of the source assembly
 * @param[in] tAssyRev      Tag of source assembly revision to be migrated
 * @param[in] iSrcAssyIRCnt Count of BOM Lines in the source assembly
 * @param[in] ptSrcAssyIR   List of Item revisions constructing the source BOM
 * @param[in] ptSrcItem     List of Item corresponding to BOM Line of the source assembly
 * @param[in] iItemCnt      Count of Item ID in mapping file
 * @param[in] ptOldItem     List of Old Item ID's from the mapping file whose latest revision needs to be migrated.
 * @param[in] ptNewItem     List of New Item ID's from the mapping file whose latest revision will receive 3Dmigrated dataset.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_migrate_sw_3d_dataset
(
    FILE*  fLogFile,
    tag_t  tTopLine,
    tag_t  tAssyRev,
    int    iSrcAssyIRCnt, 
    tag_t* ptSrcAssyIR, 
    tag_t* ptSrcItem,
    int    iItemCnt,
    tag_t* ptOldItem,
    tag_t* ptNewItem
)
{
    int    iRetCode        = ITK_ok;
    int    iDSTypeCnt      = 0;
    char*  pszDrtModelType = NULL;
    char** ppszDSType      = NULL;

    LMT5_CUST_UPDATE_STRING_ARRAY(iDSTypeCnt, ppszDSType, LMT5_CLASS_SOLIDWORKS_ASSEMBLY);
    LMT5_CUST_UPDATE_STRING_ARRAY(iDSTypeCnt, ppszDSType, LMT5_CLASS_SOLIDWORKS_PART);

    LMT5_CUST_STRCPY(pszDrtModelType, LMT5_CLASS_DIRECTMODEL);

    for(int iDx = 0; iDx < iSrcAssyIRCnt; iDx++)
    {
        if(iDx == 0)
        {
            tag_t tAssyItem = NULLTAG;

            LMT5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tAssyRev, &tAssyItem));

            /* Validate and move SW Top assembly Item revision */
            LMT5_ITKCALL(iRetCode, LMT5_perform_dataset_migration(fLogFile, tAssyItem, tAssyRev, iDSTypeCnt,
                                                                   ppszDSType, pszDrtModelType, iItemCnt, ptOldItem, ptNewItem));
        }

        /* Validate and move SW Asm and SWPrt from Source IR */
        LMT5_ITKCALL(iRetCode, LMT5_perform_dataset_migration(fLogFile, ptSrcItem[iDx], ptSrcAssyIR[iDx], iDSTypeCnt,
                                                               ppszDSType, pszDrtModelType, iItemCnt, ptOldItem, ptNewItem));
        if(iRetCode != ITK_ok)
        {
            /* Get OOTB Error Stored on the Stack and Write it in Log File */
            LMT5_REPORT_OOTB_ERROR(iRetCode, fLogFile);
            /* Suppress error if error occurs so as to process rest of the objects. */
            iRetCode  = ITK_ok;
        }
    }

    LMT5_MEM_TCFREE( pszDrtModelType );
    LMT5_MEM_TCFREE( ppszDSType );
    return iRetCode;
}


/**
 * This function    
 *
 * @param[in] fLogFile
 * @param[in] tSrcItem
 * @param[in] tSrcAssyIR
 * @param[in] iDSTypeCnt
 * @param[in] ppszDSType
 * @param[in] pszDrtModelType
 * @param[in] iItemCnt
 * @param[in] ptOldItem
 * @param[in] ptNewItem  
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_perform_dataset_migration
(
    FILE*  fLogFile,
    tag_t  tSrcItem,
    tag_t  tSrcAssyIR,
    int    iDSTypeCnt,
    char** ppszDSType,
    char*  pszDrtModelType,
    int    iItemCnt,
    tag_t* ptOldItem,
    tag_t* ptNewItem
)
{
    int   iRetCode  = ITK_ok;
    tag_t tDestIRev = NULLTAG;

    for(int iNx = 0; iNx < iItemCnt; iNx++)
    {
        if(ptOldItem[iNx] == tSrcItem)
        {
            LMT5_ITKCALL(iRetCode, ITEM_ask_latest_rev(ptNewItem[iNx], &tDestIRev));
            break;
        }
    }

    /* Validate and move SW Asm and SWPrt from Source IR */
    LMT5_ITKCALL(iRetCode, LMT5_validate_and_move_dataset(fLogFile, tSrcAssyIR, tDestIRev, LMT5_IMAN_SPECIFICATION_RELATION,
                                                           iDSTypeCnt, ppszDSType, iItemCnt, ptOldItem, ptNewItem));

    /* Validate and move Direct Model from Source IR */
    LMT5_ITKCALL(iRetCode, LMT5_validate_and_move_dataset(fLogFile, tSrcAssyIR, tDestIRev, LMT5_IMAN_RENDERING_RELATION,
                                                             1, &(pszDrtModelType), iItemCnt, ptOldItem, ptNewItem));
    return iRetCode;
}

/**
 * This function    
 *
 * @param[in]  fLogFile
 * @param[in]  tSrcIR
 * @param[in]  tDestIR
 * @param[in]  pszRelType
 * @param[in]  iDSTypeCnt
 * @param[in]  ppszDSType
 * @param[in]  iItemCnt
 * @param[in]  ptOldItem
 * @param[in]  ptNewItem  
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_validate_and_move_dataset
(
    FILE*  fLogFile,
    tag_t  tSrcIR,
    tag_t  tDestIR,
    char*  pszRelType, 
    int    iDSTypeCnt,
    char** ppszDSType,
    int    iItemCnt,
    tag_t* ptOldItem,
    tag_t* ptNewItem
)
{
    int    iRetCode     = ITK_ok;
    int    iSrcDSCnt    = 0;
    tag_t* ptSrcDataset = NULL;
    tag_t* ptSrcRelObj  = NULL;

    LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type(tSrcIR, pszRelType, iDSTypeCnt, ppszDSType, NULL, false,
                                                             &ptSrcDataset, &ptSrcRelObj, &iSrcDSCnt));
    if(iSrcDSCnt == 1)
    {
        int    iDestDSCnt     = 0;
        tag_t* ptDestDataset  = NULL;
        tag_t* ptDestRelObj   = NULL;

        LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type(tDestIR, pszRelType, iDSTypeCnt, ppszDSType, NULL, false,
                                                                 &ptDestDataset, &ptDestRelObj, &iDestDSCnt));

        if(iDestDSCnt == 0)
        {
            int iMarkPoint  = 0;

            /* Define a markpoint for database transaction */
            LMT5_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));

            for(int iDx = 0; iDx < iSrcDSCnt && iRetCode == ITK_ok; iDx++)
            {
                tag_t  tRelation  = NULLTAG;
                char*  pszDSType  = NULL;

                /* Delete existing relation */
                LMT5_ITKCALL(iRetCode, LMT5_grm_delete_relation(tSrcIR, ptSrcRelObj[iDx]));

                /* Create new relation */
                LMT5_ITKCALL(iRetCode, LMT5_grm_create_relation(tDestIR, ptSrcDataset[iDx], pszRelType, &tRelation));

                /* Remove existing status if any from dataset */
                LMT5_ITKCALL(iRetCode, LMT5_obj_dereference_status(ptSrcDataset[iDx]));
             
                /* Associating status of New Item revision to the dataset */
                LMT5_ITKCALL(iRetCode, LMT5_obj_set_src_obj_status_to_dest(tDestIR, ptSrcDataset[iDx]));

                LMT5_ITKCALL(iRetCode, LMT5_obj_ask_type(ptSrcDataset[iDx], &pszDSType));

                if(tc_strcmp(pszDSType, LMT5_CLASS_SOLIDWORKS_ASSEMBLY) == 0)
                {
                    int    iIRCnt     = 0;
                    tag_t* ptSWDepIR  = NULL;
                    tag_t* ptSWRelObj = NULL;

                    LMT5_ITKCALL(iRetCode, LMT5_grm_get_related_obj_of_type(ptSrcDataset[iDx], LMT5_SWIM_DEPENDENCY_RELATION,
                                                                            0, NULL, NULL, false, &ptSWDepIR, &ptSWRelObj, &iIRCnt));

                    LMT5_ITKCALL(iRetCode, LMT5_migrate_sw_3d_dependency(fLogFile, tDestIR, ptSrcDataset[iDx], LMT5_SWIM_DEPENDENCY_RELATION,
                                                                       iIRCnt, ptSWDepIR, ptSWRelObj, iItemCnt, ptOldItem, ptNewItem));

                    LMT5_MEM_TCFREE(ptSWDepIR);
                    LMT5_MEM_TCFREE(ptSWRelObj);
                }

                LMT5_MEM_TCFREE(pszDSType);
            }
            
            /* Commit the information into the database. */
            LMT5_ITKCALL(iRetCode, POM_forget_markpoint(iMarkPoint));

            if (iRetCode != ITK_ok)
            {
                logical bStateChanged = false;

                /* Do the rollback of the database. */
                POM_roll_to_markpoint(iMarkPoint, &bStateChanged);

                /* Suppress error if error occurs so as to process rest of the objects. */
                iRetCode  = ITK_ok;
            }
        }
        else
        {
            /* Report Error: Dataset of specific type is already attached to destination Item revision */
            LMT5_REPORT_SW_MULTIPLE_DS_EXISTS_ERROR(fLogFile, tDestIR, iDSTypeCnt, ppszDSType);
        }

        LMT5_MEM_TCFREE( ptDestRelObj );
        LMT5_MEM_TCFREE( ptDestDataset );
    }
    else
    {
        if(iSrcDSCnt > 1)
        {
             /* Report Error: More than one Dataset of specific type is already attached to source Item revision */
            LMT5_REPORT_SW_MULTIPLE_DS_EXISTS_ON_SRC_IR_ERROR(fLogFile, tSrcIR, iDSTypeCnt, ppszDSType);
        }
    }

    LMT5_MEM_TCFREE(ptSrcDataset);
    LMT5_MEM_TCFREE(ptSrcRelObj);
    return iRetCode;
}

/**
 * This function migrates the 
 *
 * @param[in]   fLogFile
 * @param[in]   tItemRev
 * @param[in]   tSWDataset
 * @param[in]   pszRelType
 * @param[in]   iDependencyRevCnt
 * @param[in]   ptSWDepIR
 * @param[in]   ptSWRelObj
 * @param[in]   iItemCnt
 * @param[in]   ptOldItem
 * @param[in]   ptNewItem 
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_migrate_sw_3d_dependency
(
    FILE*  fLogFile,
    tag_t  tItemRev,
    tag_t  tSWDataset,
    char*  pszRelType,
    int    iDependencyRevCnt,
    tag_t* ptSWDepIR, 
    tag_t* ptSWRelObj,
    int    iItemCnt, 
    tag_t* ptOldItem, 
    tag_t* ptNewItem
)
{
    int iRetCode   = ITK_ok;

    for(int iDx = 0; iDx < iDependencyRevCnt && iRetCode == ITK_ok; iDx++)
    {
        tag_t tSrcItem  = NULLTAG;
        tag_t tDestItem = NULLTAG;

        LMT5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(ptSWDepIR[iDx], &tSrcItem));

        for(int iNx = 0; iNx < iItemCnt; iNx++)
        {
            if(ptOldItem[iNx] == tSrcItem)
            {
                tDestItem = ptNewItem[iNx];
                break;
            }
        }

        if(tDestItem == NULLTAG)
        {
            char* pszObjStr  = NULL;
            char* pszSWDSStr = NULL;
            char* pszIRStr   = NULL;

            /* Report Error: Dataset of specific type is already attached to Item revision */
            AOM_ask_value_string(ptSWDepIR[iDx], LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr);
            AOM_ask_value_string(tSWDataset, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszSWDSStr);
            AOM_ask_value_string(tItemRev, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszIRStr);
            fprintf(fLogFile, "Error: Solidworks dependency revision '%s' information is not present in the mapping file. SW dataset '%s' is attached to Item revision '%s' \n", pszObjStr, pszSWDSStr, pszIRStr);
            fflush(fLogFile);

            LMT5_MEM_TCFREE( pszObjStr );
            LMT5_MEM_TCFREE( pszSWDSStr );
            LMT5_MEM_TCFREE( pszIRStr );
        }
        else
        { 
            tag_t tDestItemRev = NULL;
            tag_t tRelation    = NULL;

            LMT5_ITKCALL(iRetCode, ITEM_ask_latest_rev(tDestItem, &tDestItemRev));

            /* Delete existing relation */
            LMT5_ITKCALL(iRetCode, LMT5_grm_delete_relation(tSWDataset, ptSWRelObj[iDx]));

            /* Create new relation */
            LMT5_ITKCALL(iRetCode, LMT5_grm_create_relation(tSWDataset, tDestItemRev, pszRelType, &tRelation));
        }
    }

    return iRetCode;
}
