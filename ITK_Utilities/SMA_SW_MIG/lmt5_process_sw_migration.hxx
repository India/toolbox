/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_process_sw_migration.hxx

    Description:  Header File containing declaration

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LMT5_PROCESS_SW_MIGRATION_HXX
#define LMT5_PROCESS_SW_MIGRATION_HXX

#include <lmt5_library.hxx>
#include <lmt5_errors.hxx>



#define LMT5_REPORT_OOTB_ERROR(iRetCode, fLogFile){\
                                    char* pszErrorText = NULL;\
                                    EMH_ask_error_text(iRetCode, &pszErrorText);\
                                    fprintf(fLogFile, "Error: %s. \n", pszErrorText);\
                                    fflush(fLogFile);\
                                    LMT5_MEM_TCFREE(pszErrorText);\
}

#define LMT5_REPORT_SW_MULTIPLE_DS_EXISTS_ERROR(fLogFile, tDestIR, iDSTypeCnt, ppszDSType){\
                                                    char* pszObjStr = NULL;\
                                                    char* psrTemp   = NULL;\
                                                    for(int iFx = 0; iFx < iDSTypeCnt; iFx++){\
                                                        LMT5_CUST_STRCAT(psrTemp, ppszDSType[iFx]);\
                                                        if((iFx+1) < iDSTypeCnt){\
                                                            LMT5_CUST_STRCAT(psrTemp, "or");\
                                                        }\
                                                    }\
                                                    AOM_ask_value_string(tDestIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr);\
                                                    fprintf(fLogFile, "Error: Dataset of type '%s' is/are already attached to destination Item revision '%s'.\n", psrTemp, pszObjStr);\
                                                    fflush(fLogFile);\
                                                    LMT5_MEM_TCFREE( pszObjStr );\
                                                    LMT5_MEM_TCFREE( psrTemp );\
}

#define LMT5_REPORT_SW_MULTIPLE_DS_EXISTS_ON_SRC_IR_ERROR(fLogFile, tSrcIR, iDSTypeCnt, ppszDSType){\
                                                                char* pszObjStr = NULL;\
                                                                char* psrTemp   = NULL;\
                                                                for(int iFx = 0; iFx < iDSTypeCnt; iFx++){\
                                                                    LMT5_CUST_STRCAT(psrTemp, ppszDSType[iFx]);\
                                                                    if((iFx+1) < iDSTypeCnt){\
                                                                        LMT5_CUST_STRCAT(psrTemp, " or ");\
                                                                    }\
                                                                }\
                                                                AOM_ask_value_string(tSrcIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr);\
                                                                fprintf(fLogFile, "Error: More than one Dataset of type '%s' is/are already attached to source Item revision '%s'.\n", psrTemp, pszObjStr);\
                                                                fflush(fLogFile);\
                                                                LMT5_MEM_TCFREE( pszObjStr );\
                                                                LMT5_MEM_TCFREE( psrTemp );\
}

int LMT5_process_sw_mig
(
    char*   pszMappingFileInfo,
    char*   pszDependencyMigFileInfo,
    char*   pszLogFile,
    char*   pszSeparator,
    char*   pszItemID,
    char*   pszRevID, 
    char*   pszRevRule,
    logical bSkipBOM,
    logical bDryRun, 
    logical bMigDrw,
    logical bIgnCreaDate
); 

int LMT5_validate_mapping_file_and_extract_info
(
    FILE*    fLogFile,
    char*    pszFileInfo,
    char*    pszSeparator,
    int*     piItemCnt,
    tag_t**  pptOldItem,
    tag_t**  pptNewItem,
    char***  pppszOldItemID, 
    char***  pppszNewItemID,
    logical* pbFoundError
);

int LMT5_validate_and_extract_info
(
    FILE*    fLogFile,
    char*    pszLine,
    char*    pszSeparator,
    int*     piItemCnt,
    tag_t**  pptOldItemID,
    tag_t**  pptNewItemID,
    char***  pppszOldItemID, 
    char***  pppszNewItemID,
    logical* pbFoundError
);


#endif
