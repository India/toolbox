/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_grm_utilities.cxx

    Description:  This File contains custom functions for GRM to perform generic operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <lmt5_library.hxx>
#include <lmt5_const.hxx>
#include <lmt5_errors.hxx>

/**
 * This function creates specification relationship between primary and secondary object.
 * 
 * @param[in] tPrimaryObj      Tag of the primary object.
 * @param[in] tSecondaryObj    Tag of the secondary object.
 * @param[in] pszRelationType  Name of relation type.
 * @param[out] ptRelation      Tag to newly created relation type.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMT5_grm_create_relation
(
    tag_t   tPrimaryObj,
    tag_t   tSecondaryObj,
    char*   pszRelationType,
    tag_t*  ptRelation
)
{
    int    iRetCode        = ITK_ok;
    tag_t  tRelationType   = NULLTAG;
    tag_t  tNewRelation    = NULLTAG;

    /* Initializing the Out Parameter to this function. */
    *ptRelation = NULLTAG;

    /* Get relation type */
    LMT5_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationType, &tRelationType));

    if(tRelationType == NULLTAG)
        iRetCode = GRM_invalid_relation_type;
    if(tPrimaryObj == NULLTAG)
        iRetCode = GRM_invalid_primary_object;
    if(tSecondaryObj == NULLTAG)
        iRetCode = GRM_invalid_secondary_object;

     /*     Find relation to check if relation already exists   */
    LMT5_ITKCALL(iRetCode, GRM_find_relation(tPrimaryObj, tSecondaryObj, tRelationType, &tNewRelation));

    if(tNewRelation == NULLTAG)
    {
        LMT5_ITKCALL(iRetCode, GRM_create_relation(tPrimaryObj, tSecondaryObj, tRelationType, NULLTAG, &tNewRelation));

        if(tNewRelation != NULLTAG)
        {
            LMT5_ITKCALL(iRetCode, GRM_save_relation(tNewRelation));
        }
    }

    if(iRetCode == ITK_ok)
    {
        (*ptRelation) = tNewRelation;
    }
    
    return iRetCode;
}

/**
 * This function creates specification relationship between primary and secondary object.
 * 
 * @param[in] tPrimaryObj      Tag of the primary object.
 * @param[in] tSecondaryObj    Tag of the secondary object.
 * @param[in] pszRelationType  Name of relation type.
 * @param[out] ptRelation      Tag to newly created relation type.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMT5_grm_delete_relation
(
    tag_t   tPrimaryObj,
    tag_t   tRelation
)
{
    int  iRetCode  = ITK_ok;

    LMT5_ITKCALL(iRetCode, AOM_refresh(tPrimaryObj, true));

    /* Delete relation */
    LMT5_ITKCALL(iRetCode, GRM_delete_relation(tRelation));

    LMT5_ITKCALL(iRetCode, AOM_save(tPrimaryObj));

    LMT5_ITKCALL(iRetCode, AOM_refresh(tPrimaryObj, false));

    return iRetCode;
}

/**
 * This function get's the object's related to input business object 'tObject'. Related objects are retrieved based on various 
 * input criteria such as:
 *
 *   <b> Relation Type between business objects </b>
 *   <b> Type of related object </b>
 *   <b> Related objects can be excluded based on status </b>
 * 
 * @note: This function is generic function and is capable of retrieving Primary and Secondary relation objects based on value
 *        of input arguments 'bGetPrimary' If argument 'pszRelationTypeName' is NULL then all related objects irrespective
 *        of relation type will be retrieved. If 'pszObjType' is NOT NULL then related objects of type 'pszObjType' will be 
 *        retrieved.
 *
 * @param[in]  tObject              Tag of Object
 * @param[in]  pszRelationTypeName  Relation Type. NULLTAG means all relation types.
 * @param[in]  pszObjType           Specific type of object which is related to input object 'tObject'.
 *                                  NULL means any type of related object.
 * @param[in]  pszExcludeStatus     Related objects with this status will be excluded from valid related objects list
 * @param[in]  bGetPrimary          'true' of Primary related objects needs to be retrieved from input objects else  
 *                                  secondary objects will be retrieved. 
 * @param[out] pptValidRelatedObj   List of valid related objects
 * @param[out] piRelatedObjCnt      Count of valid related objects.
 *
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMT5_grm_get_related_obj
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    char*    pszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary,
    tag_t**  pptValidRelatedObj,
    int*     piRelatedObjCnt
)
{
    int     iRetCode              = ITK_ok;
    int     iCount                = 0;
    int     iTempCnt              = 0;
    int     iRelatedObjCnt        = 0;
    int     iLoadedObjCnt         = 0;    
    tag_t   tRelationType         = NULLTAG;
    tag_t*  ptTempObj             = NULL;
    tag_t*  ptLoadedObj           = NULL;
    GRM_relation_t *ptRelatedObj  = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piRelatedObjCnt)    = 0;
    (*pptValidRelatedObj) = NULL;

    if(pszRelationTypeName != NULL)
    {
        /* Get the relation type tag e.g. "IMAN_specification"  */
        LMT5_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    }
    
    if(bGetPrimary == true)
    {
        LMT5_ITKCALL(iRetCode, GRM_list_primary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }
    else
    {
        LMT5_ITKCALL(iRetCode, GRM_list_secondary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }

    /* Related objects are deliberately collected in this loop so that they can be loaded using POM API in one shot */                 
    for(int iNx = 0; iNx < iRelatedObjCnt && iRetCode == ITK_ok; iNx++)
    {
        int     iActiveSeq  = 0;
        tag_t   tObject     = NULLTAG;
        logical bIsValid    = false;
        logical bIsWSObj    = false;
        
        if(bGetPrimary == true)
        {
            tObject = ptRelatedObj[iNx].primary;
        }
        else
        {
            tObject = ptRelatedObj[iNx].secondary;
        }

        LMT5_ITKCALL(iRetCode, LMT5_obj_is_type_of(tObject, LMT5_CLASS_WORKSPACEOBJECT, &bIsWSObj));

        if(iRetCode == ITK_ok)
        {
            if(bIsWSObj == true)
            {
                LMT5_ITKCALL(iRetCode, AOM_ask_value_int(tObject, LMT5_ATTR_ACTIVE_SEQ, &iActiveSeq));
            }
            else
            {
                iActiveSeq = 1;
            }
        }
    
        /* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0 && iRetCode == ITK_ok)
        {
            logical bBelongsToClass = false;

            if(pszObjType != NULL && tc_strlen(pszObjType) > 0)
            {
                LMT5_ITKCALL(iRetCode, LMT5_obj_is_type_of(tObject, pszObjType, &bBelongsToClass));
            }
            else
            {
                bBelongsToClass = true;
            }

            if( bBelongsToClass ==  true && iRetCode == ITK_ok)
            {
                tag_t  tStatus  = NULLTAG;

                if(pszExcludeStatus != NULL && tc_strlen(pszExcludeStatus) > 0)
                {
                    LMT5_ITKCALL(iRetCode, LMT5_obj_ask_status_tag(tObject, pszExcludeStatus, &tStatus));
                }

                if(tStatus == NULLTAG && iRetCode == ITK_ok)
                {
                    LMT5_UPDATE_TAG_ARRAY(iCount, (*pptValidRelatedObj), tObject);
                }
            }
        }
    }
  
    (*piRelatedObjCnt) = iCount;

    LMT5_MEM_TCFREE(ptLoadedObj);
    LMT5_MEM_TCFREE(ptTempObj);
    LMT5_MEM_TCFREE(ptRelatedObj);
    return iRetCode;
}

/**
 * This function get's the object's related to input business object 'tObject'. Related objects are retrieved based on various 
 * input criteria such as:
 *
 *   <b> Relation Type between business objects </b>
 *   <b> Type of related object </b>
 *   <b> Related objects can be excluded based on status </b>
 *
 * @param[in]  tObject              Tag of Object
 * @param[in]  pszRelationTypeName  Relation Type. NULLTAG means all relation types.
 * @param[in]  iTypeCnt             Related object type name list count.
 * @param[in]  ppszObjType          Specific type of object which is related to input object 'tObject'.
 *                                  NULL means any type of related object.
 * @param[in]  pszExcludeStatus     Related objects with this status will be excluded from valid related objects list
 * @param[in]  bGetPrimary          'true' of Primary related objects needs to be retrieved from input objects else  
 *                                  secondary objects will be retrieved. 
 * @param[out] pptValidRelatedObj   List of valid related objects
 * @param[out] piRelatedObjCnt      Count of valid related objects.
 *
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LMT5_grm_get_related_obj_of_type
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    int      iTypeCnt,
    char**   ppszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary,
    tag_t**  pptValidRelatedObj,
    tag_t**  pptRelationObj,
    int*     piRelatedObjCnt
)
{
    int     iRetCode              = ITK_ok;
    int     iCount                = 0;
    int     iRelatedObjCnt        = 0;   
    tag_t   tRelationType         = NULLTAG;
    GRM_relation_t* ptRelatedObj  = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piRelatedObjCnt)    = 0;
    (*pptValidRelatedObj) = NULL;
    (*pptRelationObj)     = NULL;

    if(pszRelationTypeName != NULL)
    {
        /* Get the relation type tag e.g. "IMAN_specification"  */
        LMT5_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    }
    
    if(bGetPrimary == true)
    {
        LMT5_ITKCALL(iRetCode, GRM_list_primary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }
    else
    {
        LMT5_ITKCALL(iRetCode, GRM_list_secondary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }
                
    for(int iNx = 0; iNx < iRelatedObjCnt && iRetCode == ITK_ok; iNx++)
    {
        int     iActiveSeq   = 0;
        tag_t   tObject      = NULLTAG;
        tag_t   tRelationObj = NULLTAG;
        logical bIsValid     = false;
        logical bIsWSObj     = false;

        if(bGetPrimary == true)
        {
            tObject = ptRelatedObj[iNx].primary;
        }
        else
        {
            tObject = ptRelatedObj[iNx].secondary;
        }

        tRelationObj = ptRelatedObj[iNx].the_relation; 

        if(iTypeCnt == 0)
        {
            /* As Object type validation is optional check hence if no type is specified then any valid related objects are returned */
            bIsValid = true;
        }
        else
        {
            for(int iFx = 0; iFx < iTypeCnt && iRetCode == ITK_ok; iFx++)
            {
                LMT5_ITKCALL(iRetCode, LMT5_obj_is_type_of(tObject, ppszObjType[iFx], &bIsValid));

                if(bIsValid == true)
                {
                    break;
                }
            }
        }

        if(bIsValid == true && iRetCode == ITK_ok)
        {
            LMT5_ITKCALL(iRetCode, LMT5_obj_is_type_of(tObject, LMT5_CLASS_WORKSPACEOBJECT, &bIsWSObj));

            /* If related object is a Workspace object then Active sequence will be considered */
            if(iRetCode == ITK_ok)
            {
                if(bIsWSObj == true)
                {
                    LMT5_ITKCALL(iRetCode, AOM_ask_value_int(tObject, LMT5_ATTR_ACTIVE_SEQ, &iActiveSeq));
                }
                else
                {
                    iActiveSeq = 1;
                }
            }
        
            if(iActiveSeq != 0 && iRetCode == ITK_ok)
            {
                tag_t  tStatus  = NULLTAG;

                if(pszExcludeStatus != NULL && tc_strlen(pszExcludeStatus) > 0)
                {
                    LMT5_ITKCALL(iRetCode, LMT5_obj_ask_status_tag(tObject, pszExcludeStatus, &tStatus));
                }

                if(tStatus == NULLTAG && iRetCode == ITK_ok)
                {
                    LMT5_UPDATE_TAG_ARRAY(iCount, (*pptValidRelatedObj), tObject);

                    LMT5_ADD_TAG_TO_ARRAY(iCount, (*pptRelationObj), tRelationObj);
                }
            }
        }
    }
  
    (*piRelatedObjCnt) = iCount;

    LMT5_MEM_TCFREE(ptRelatedObj);
    return iRetCode;
}
