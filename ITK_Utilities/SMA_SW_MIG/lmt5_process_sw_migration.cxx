/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_process_sw_migration.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <lmt5_errors.hxx>
#include <lmt5_library.hxx>
#include <lmt5_sw_3d_mig.hxx>
#include <lmt5_sw_2d_drw_mig.hxx>
#include <lmt5_process_sw_migration.hxx>

/**
 * The utility 
 *
 * @note: Utility can be executed using following options:
 *       -class
 *            Define the Teamcenter Class name. Instances of this class will be considered for transferring the ownership
 *
 * @param[in] pszMappingFileInfo 
 * @param[in] pszDependencyMigFileInfo 
 * @param[in] pszLogFile          
 * @param[in] pszSeparator       
 * @param[in] pszItemID      
 * @param[in] pszRevID     
 * @param[in] pszRevRule
 * @param[in] bSkipBOM
 * @param[in] bDryRun
 * @param[in] bMigDrw
 * @param[in] bIgnCreaDate
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_process_sw_mig
(
    char*   pszMappingFileInfo,
    char*   pszDependencyMigFileInfo,
    char*   pszLogFile,
    char*   pszSeparator,
    char*   pszItemID,
    char*   pszRevID, 
    char*   pszRevRule,
    logical bSkipBOM,
    logical bDryRun, 
    logical bMigDrw,
    logical bIgnCreaDate
)
{
    int    iRetCode  = ITK_ok;
    FILE*  fLogFile;
   
    if((fLogFile = fopen(pszLogFile, "w")) == NULL)
    {
        printf("\n** ERROR: Cannot create %s for writing log  **\n\n",pszLogFile);
    }
    else
    {
        tag_t tAssyRev = NULLTAG;

        if(bMigDrw == false)
        {               
            if((pszRevRule == NULL) || (tc_strlen(pszRevRule) == 0))
            {
                LMT5_CUST_STRCPY(pszRevRule, LMT5_REVISION_RULE_LATEST_WORKING);
            }

            LMT5_ITKCALL(iRetCode, LMT5_find_asm_info(fLogFile, pszItemID, pszRevID, &tAssyRev));
        }

        if(iRetCode == ITK_ok && (tAssyRev != NULLTAG || bMigDrw == true))
        {
            int     iItemCnt            = 0;
            int     iDepMigItemCnt      = 0;
            tag_t*  ptOldItem           = NULL;
            tag_t*  ptNewItem           = NULL;
            tag_t*  ptDepMigOldItem     = NULL;
            tag_t*  ptDepMigNewItem     = NULL;
            char**  ppszOldItemID       = NULL;
            char**  ppszNewItemID       = NULL;
            char**  ppszDepMigOldItemID = NULL;
            char**  ppszDepMigNewItemID = NULL;
            logical bFoundError         = false;

            /*-------------------------------------*/
            /* Do not update date fields on change */
            /*-------------------------------------*/
            LMT5_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_attr_update, false, 0,0,0, NULL));

            /*----------------------*/
            /* Bypass access checks */
            /*----------------------*/
            LMT5_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_access_check, true, 0,0,0, NULL));

            LMT5_ITKCALL(iRetCode, ITK_set_bypass(true));


            LMT5_ITKCALL(iRetCode, LMT5_validate_mapping_file_and_extract_info(fLogFile, pszMappingFileInfo, pszSeparator, &iItemCnt, &ptOldItem,
                                                                               &ptNewItem, &ppszOldItemID, &ppszNewItemID, &bFoundError));
            
            if(pszDependencyMigFileInfo != NULL && (tc_strlen(pszDependencyMigFileInfo) > 0))
            {
                LMT5_ITKCALL(iRetCode, LMT5_validate_mapping_file_and_extract_info(fLogFile, pszDependencyMigFileInfo, pszSeparator, &iDepMigItemCnt, &ptDepMigOldItem,
                                                                                    &ptDepMigNewItem, &ppszDepMigOldItemID, &ppszDepMigNewItemID, &bFoundError));
            }

            /* Validate if all child Item revision of the assembly are present in the mapping file. */
            if(bFoundError == false && iRetCode == ITK_ok)
            {
                if(bMigDrw == true)
                {
                    LMT5_ITKCALL(iRetCode, LMT5_perform_2d_migration(fLogFile, iItemCnt, ptOldItem, ptNewItem, iDepMigItemCnt, ptDepMigOldItem,
                                                                       ptDepMigNewItem, bDryRun, bIgnCreaDate));
                }
                else
                {
                    LMT5_ITKCALL(iRetCode, LMT5_perform_3d_migration(fLogFile, tAssyRev, pszRevRule, iItemCnt, ptOldItem, ptNewItem,
                                                                        bSkipBOM, bDryRun, bIgnCreaDate));
                }
            }

            LMT5_ITKCALL(iRetCode, ITK_set_bypass(false));

            LMT5_MEM_TCFREE( ptOldItem );
            LMT5_MEM_TCFREE( ptNewItem );
            LMT5_MEM_TCFREE( ptDepMigOldItem );
            LMT5_MEM_TCFREE( ptDepMigNewItem );
            LMT5_MEM_TCFREE( ppszOldItemID );
            LMT5_MEM_TCFREE( ppszNewItemID );
            LMT5_MEM_TCFREE( ppszDepMigOldItemID );
            LMT5_MEM_TCFREE( ppszDepMigNewItemID );
        }
    }

    fclose( fLogFile );
    return iRetCode;
}


/**
 * This function 
 *
 * @param[in]  fLogFile  
 * @param[in]  pszLine
 * @param[in]  pszSeparator
 * @param[in]  piItemCnt
 * @param[in]  pptOldItem
 * @param[in]  pptNewItem
 * @param[in]  pppszOldItemID
 * @param[in]  pppszNewItemID
 * @param[out] pbFoundError
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_validate_mapping_file_and_extract_info
(
    FILE*    fLogFile,
    char*    pszFileInfo,
    char*    pszSeparator,
    int*     piItemCnt,
    tag_t**  pptOldItem,
    tag_t**  pptNewItem,
    char***  pppszOldItemID, 
    char***  pppszNewItemID,
    logical* pbFoundError
)
{
    int   iRetCode          = ITK_ok;
    char  szLine[MAX_CHARS] = {'\0'};
    FILE* fMappingFile;

    if ((fMappingFile = fopen(pszFileInfo, "r")) == NULL )
    {
        /* No Item with this ID exist in the system */
        (*pbFoundError) = true;
        iRetCode = CUSTOM_ERROR_SEE_LOG_FILE;
        fprintf(fLogFile, "\n** ERROR: Cannot open '%s' File for reading - exiting **\n\n", pszFileInfo);
        fflush(fLogFile);
        printf("\n** ERROR: Cannot open '%s' File for reading - exiting **\n\n", pszFileInfo);
    }
    else
    {
        while (fgets(szLine, MAX_CHARS, fMappingFile) != NULL)
        {
            if ((szLine[0] != '#') && (szLine[0] != '\0') && (szLine[0] != '\n'))
            {
                /*================================================*/
                /* Trim the line to remove leading and trailing   */
                /* spaces and discard the \n on the end.          */
                /*================================================*/
                tc_strcpy(szLine, LMT5_trim(szLine));

                LMT5_ITKCALL(iRetCode, LMT5_validate_and_extract_info(fLogFile, szLine, pszSeparator, piItemCnt, pptOldItem,
                                                                      pptNewItem, pppszOldItemID, pppszNewItemID, pbFoundError));
            }
        }
    }

    /* Closing the Mapping File */
    fclose(fMappingFile);
    return iRetCode;
}

/**
 * This function parse the line from the load file and initialize the variable with appropriate information.
 *
 * @param[in]  fLogFile  
 * @param[in]  pszLine
 * @param[in]  pszSeparator
 * @param[in]  piItemCnt
 * @param[in]  pptOldItem
 * @param[in]  pptNewItem
 * @param[in]  pppszOldItemID
 * @param[in]  pppszNewItemID
 * @param[out] pbFoundError
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int LMT5_validate_and_extract_info
(
    FILE*    fLogFile,
    char*    pszLine,
    char*    pszSeparator,
    int*     piItemCnt,
    tag_t**  pptOldItem,
    tag_t**  pptNewItem,
    char***  pppszOldItemID, 
    char***  pppszNewItemID,
    logical* pbFoundError
)
{
    int    iRetCode = ITK_ok;
    int    iInfoCnt = 0;
    char** ppszInfo = NULL;

    LMT5_ITKCALL(iRetCode , LMT5_arg_parse_multipleVal(pszLine, pszSeparator[0], &iInfoCnt, &ppszInfo));

    if(iInfoCnt == 2)
    {
        int     iOldItemCnt = 0;
        int     iNewItemCnt = 0;
        tag_t*  ptOldItem   = NULL;
        tag_t*  ptNewItem   = NULL;
        logical bError      = false;

        if((ppszInfo[0] == NULL) || (tc_strlen(ppszInfo[0]) == 0) || 
            (ppszInfo[1] == NULL) || (tc_strlen(ppszInfo[1]) == 0))
        {
            /* Old or New Item is missing in the Mapping File */
            fprintf(fLogFile, "Error: Mapping File construct is not proper. It may contain blank.\n");
            fflush(fLogFile);
             bError = true;
        }
        else
        {
            LMT5_ITKCALL(iRetCode, ITEM_find(ppszInfo[0], &iOldItemCnt, &ptOldItem));

            if(iRetCode == ITK_ok)
            {
                if(iOldItemCnt == 0 || iOldItemCnt > 1)
                {
                    /* Report Error : More than One Item Exist With this ID  */
                    bError = true;

                    if(iOldItemCnt == 0)
                    {
                        /* No Item with this ID exist in the system */
                        fprintf(fLogFile, "Error: No source item with ID '%s' exist in the system.\n", ppszInfo[0]);
                        fflush(fLogFile);
                    }
                    else
                    {
                        /* More than one Item with this ID exist in the system */
                        fprintf(fLogFile, "Error: More than one source item with ID '%s' exist in the system.\n", ppszInfo[0]);
                        fflush(fLogFile);
                    }
                }
                else
                {
                    LMT5_ITKCALL(iRetCode, ITEM_find(ppszInfo[1], &iNewItemCnt, &ptNewItem));

                    if(iRetCode == ITK_ok)
                    {
                        if(iNewItemCnt == 0 || iNewItemCnt > 1)
                        {
                            /* Report Error : More than One Item Exist With this ID  */
                            bError = true;
                            
                            if(iNewItemCnt == 0)
                            {
                                /* No Item with this ID exist in the system */
                                fprintf(fLogFile, "Error: No destination item with ID '%s' exist in the system. \n", ppszInfo[1]);
                                fflush(fLogFile);
                            }
                            else
                            {
                                /* More than one Item with this ID exist in the system */
                                fprintf(fLogFile, "Error: More than one destination item with ID '%s' exist in the system.\n", ppszInfo[1]);
                                fflush(fLogFile);
                            }
                        }
                    }
                    else
                    {
                        /* Get OOTB Error Stored on the Stack and Write it in Log File */
                        LMT5_REPORT_OOTB_ERROR(iRetCode, fLogFile);
                    }
                }
            }
            else
            {
                /* Get OOTB Error Stored on the Stack and Write it in Log File */
                LMT5_REPORT_OOTB_ERROR(iRetCode, fLogFile);
            }
        }

        if(bError == false && iRetCode == ITK_ok)
        {
            for(int iDx = 0; iDx < (*piItemCnt); iDx++)
            {
                if(ptOldItem[iOldItemCnt-1] == (*pptOldItem)[iDx] || 
                    ptNewItem[iNewItemCnt-1] == (*pptNewItem)[iDx])
                {
                    bError = true;

                    /* Report Error: Duplicate Old Item ID is present in the Mapping File */
                    if(ptOldItem[iOldItemCnt-1] == (*pptOldItem)[iDx])
                    {
                        fprintf(fLogFile, "Mapping File Error: Mapping File construct is not proper. Item ID '%s' appears more than one in the mapping file.\n", ptOldItem[iOldItemCnt-1]);
                    }
                    else
                    {
                        fprintf(fLogFile, "Mapping File Error: Mapping File construct is not proper. Item ID '%s' appears more than one in the mapping file.\n", ptNewItem[iNewItemCnt-1]);
                    }

                    fflush(fLogFile);
                }
            }
        }

        if(bError == false && iRetCode == ITK_ok)
        {
            LMT5_CUST_UPDATE_STRING_ARRAY((*piItemCnt), (*pppszOldItemID), ppszInfo[0]);
            LMT5_CUST_ADD_STRING_TO_ARRAY((*piItemCnt), (*pppszNewItemID), ppszInfo[1]);
            LMT5_ADD_TAG_TO_ARRAY((*piItemCnt), (*pptOldItem), ptOldItem[iOldItemCnt-1]);
            LMT5_ADD_TAG_TO_ARRAY((*piItemCnt), (*pptNewItem), ptNewItem[iNewItemCnt-1]);
        }
        else
        {
            if((*pbFoundError) == false)
            {
                (*pbFoundError) = true;
            }
        }
        
        LMT5_MEM_TCFREE(ptOldItem);
        LMT5_MEM_TCFREE(ptNewItem);
    }
    else
    {
        /* Invalid line present in the Mapping File */
        fprintf(fLogFile, "Mapping File Error: Invalid line present in the Mapping File.'%s'.\n", pszLine);
    }

    LMT5_MEM_TCFREE( ppszInfo );
    return iRetCode;
}

