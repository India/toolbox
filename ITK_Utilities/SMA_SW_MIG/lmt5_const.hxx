/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_const.hxx

    Description:  Header File for constants used during scope of the executable.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LMT5_CONST_HXX
#define LMT5_CONST_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>

#define MAX_CHARS                                                 300

/* Date Time Format */
#define LMT5_DATE_TIME_FORMAT_FILENAME                            "%y%m%d%H%M%S"

/* Relation    */
#define LMT5_IMAN_SPECIFICATION_RELATION                          "IMAN_specification"
#define LMT5_IMAN_RENDERING_RELATION                              "IMAN_Rendering"
#define LMT5_SWIM_DEPENDENCY_RELATION                             "SWIM_dependency"
#define LMT5_SWIM_SUPPRESSED_DEPENDENCY_RELATION                  "SWIM_suppressed_dependency"

/* Attributes  */
#define LMT5_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define LMT5_ATTR_OWNING_GROUP_ATTRIBUTE                          "owning_group"
#define LMT5_ATTR_OWNING_USER_ATTRIBUTE                           "owning_user"
#define LMT5_ATTR_ACTIVE_SEQ                                      "active_seq"
#define LMT3_ATTR_OBJECT_NAME_ATTRIBUTE                           "object_name"
#define LMT5_ATTR_RELEASE_STATUS_LIST                             "release_status_list"
#define LMT5_ATTR_DATE_RELEASED                                   "date_released"
#define LMT5_ATTR_CREATION_DATE                                   "creation_date"
#define LMT5_ATTR_BL_QUANTITY                                     "bl_quantity"
#define LMT5_ATTR_BL_SEQUENCE                                     "bl_sequence_no"
#define LMT5_ATTR_ITEM_TAG                                        "items_tag"
#define LMT3_ATTR_ITEM_ID                                         "item_id"

/* Class Name */
#define LMT5_CLASS_WORKSPACEOBJECT                                "WorkspaceObject"
#define LMT5_CLASS_SOLIDWORKS_ASSEMBLY                            "SWAsm"
#define LMT5_CLASS_SOLIDWORKS_PART                                "SWPrt"
#define LMT5_CLASS_SOLIDWORKS_DRAWING                             "SWDrw"
#define LMT5_CLASS_DIRECTMODEL                                    "DirectModel"

/* BOM view types  */
#define LMT5_BOM_VIEW_TYPE_VIEW                                   "view"

/*  Revision Rule  */
#define LMT5_REVISION_RULE_PRECISE_ONLY                           "Precise Only"
#define LMT5_REVISION_RULE_LATEST_WORKING                         "Latest Working"

/* Separators */
#define LMT5_STRING_UNDERSCORE                                    "_"
#define LMT5_STRING_HIFEN                                         "-"
#define LMT5_STRING_SEMICOLON                                     ";"
#define LMT5_STRING_FORWARD_SLASH                                 "/"
#define LMT5_STRING_SINGLE_SPACE								  " "
#define LMT5_CHARACTER_COLON                                      ':'

#define LMT5_BOM_ATTR_STRING_LEN							      256
#define LMT5_SEPARATOR_LENGTH                                     1
#define LMT5_NUMERIC_ERROR_LENGTH                                 12
#define LMT5_STRING_DEFAULT_SEPARATOR	      					  ";"

#endif


