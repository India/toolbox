/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_sw_2d_drw_mig.hxx

    Description:  Header File containing declaration

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15-Oct-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LMT5_SW_2D_DRW_MIG_HXX
#define LMT5_SW_2D_DRW_MIG_HXX

#include <lmt5_library.hxx>
#include <lmt5_errors.hxx>

#define LMT5_REPORT_IR_RELEASE_DS_CREATION_DATE_MISMATCH_ERROR(fLogFile, tSWDrwDS, pszTrgObjStr, pszObjStr){\
                                                                char* pszDSObjStr = NULL;\
                                                                AOM_ask_value_string(tSWDrwDS, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszDSObjStr);\
                                                                fprintf(fLogFile, "SW Error: Released date of IR '%s' is earlier than creation date of dataset '%s' which is attached to IR '%s'.\n",pszTrgObjStr, pszDSObjStr, pszObjStr);\
                                                                fflush(fLogFile);\
                                                                LMT5_MEM_TCFREE(pszDSObjStr);\
}

#define LMT5_REPORT_TARG_REV_MISSING_ASM_OR_PRT_DS_SWIM_DEP_ERROR(fLogFile, tSWDrwDepIR, tTargIR, pszSrcIRObjStr){\
                                                                    char* pszDepIRObjStr = NULL;\
                                                                    char* pszTargIRObjStr = NULL;\
                                                                    AOM_ask_value_string(tSWDrwDepIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszDepIRObjStr);\
                                                                    AOM_ask_value_string(tTargIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszTargIRObjStr);\
                                                                    fprintf(fLogFile, "SW Dependency Error: Dependency revision '%s' attached to SW Drawing dataset of IR '%s' cannot be updated because target IR '%s' does not have any Solidworks part or assembly dataset.\n", pszDepIRObjStr, pszSrcIRObjStr, pszTargIRObjStr);\
                                                                    fflush(fLogFile);\
                                                                    LMT5_MEM_TCFREE(pszDepIRObjStr);\
                                                                    LMT5_MEM_TCFREE(pszTargIRObjStr);\
}

#define LMT5_REPORT_2D_SWIM_DEP_IR_NOT_FOUND_IN_MAPPING_FILE(fLogFile, tSWDrwDepIR, pszSrcIRObjStr){\
                                                                char* pszDepIRObjStr = NULL;\
                                                                AOM_ask_value_string(tSWDrwDepIR, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszDepIRObjStr);\
                                                                fprintf(fLogFile, "SW Dependency Error: Dependency revision '%s' attached to SW Drawing dataset of IR '%s' is not present in the input Dependency Mapping file provided to the utility.\n", pszDepIRObjStr, pszSrcIRObjStr);\
                                                                fflush(fLogFile);\
                                                                LMT5_MEM_TCFREE(pszDepIRObjStr);\
}

int LMT5_perform_2d_migration
(
    FILE*   fLogFile,
    int     iItemCnt,
    tag_t*  ptOldItem,
    tag_t*  ptNewItem,
    int     iDepMigItemCnt,
    tag_t*  ptDepMigOldItem,
    tag_t*  ptDepMigNewItem,
    logical bDryRun,
    logical bIgnCreaDate
);


int LMT5_check_pre_condition_for_2d_migration
(
    FILE*    fLogFile, 
    tag_t    tOldItem, 
    tag_t    tNewItem, 
    int      iDSTypeCnt,  
    char**   ppszDSTypeList,
    int      iDepMigItemCnt,
    tag_t*   ptDepMigOldItem,
    tag_t*   ptDepMigNewItem,
    tag_t*   ptSrcIR, 
    tag_t*   ptTargIR, 
    tag_t*   ptSWDrwDS, 
    tag_t*   ptIRAndDrwDSRelObj,
    int*     piSWDepIRRelCnt,
    tag_t**  pptSWDepIRRelObj, 
    int*     piNewDepMigInfoCnt, 
    tag_t**  pptNewDepMigInfoObj,
    logical* pbFoundError,
    logical  bIgnCreaDate
);

int LMT5_get_and_validate_src_sw_ds
(
    FILE*   fLogFile,
    tag_t    tItem,
    int      iDSTypeCnt,
    char**   ppszDSTypeList,
    tag_t*   ptSrcIR,
    tag_t*   ptDrwDS,  
    tag_t*   ptIRAndDrwDSRelObj,
    char**   ppszIRObjStr,
    logical* pbFoundError
);

int LMT5_validate_target_ir_and_sw_ds
(
    FILE*   fLogFile,
    tag_t    tItem,
    tag_t*   ptTargIR,
    tag_t*   ptSWAsmOrPrtDS,
    char**   ppszIRObjStr,
    logical* pbFoundError
);

int LMT5_validate_dependency_and_get_3d_rev_info
(
    FILE*    fLogFile,
    char*    pszSrcIRObjStr,
    tag_t    tSWDrwDS,
    tag_t    tTargIR,
    tag_t    tTargSWAsmOrPrtDS,
    int      iDepMigItemCnt,
    tag_t*   ptDepMigOldItem,
    tag_t*   ptDepMigNewItem, 
    int*     piSWDepIRRelCnt,
    tag_t**  pptSWDepIRRelObj,
    int*     piNewDepMigInfoCnt,
    tag_t**  pptNewDepMigInfoObj,
    logical* pbFoundError
);

int LMT5_migrate_2d_sw_ds_and_dependency
(
    FILE*  fLogFile,
    tag_t  tSrcIR,
    tag_t  tSWDrwDS,
    tag_t  tIRAndDrwDSRelObj,
    int    iObjCnt,
    tag_t* ptSWDepIRRelObj,
    tag_t  tTargIR,
    int    iNewDepMigInfoCnt, 
    tag_t* ptNewDepMigInfoObj
);

#endif

