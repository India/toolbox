/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_bom_utilities.cxx

    Description: This file contains function that provides various information about BOM and BOM Line. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28-Sep-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <lmt5_errors.hxx>
#include <lmt5_library.hxx>
#include <lmt5_const.hxx>

/**
 * Applies a given config rule to the given BOM window
 *
 * @param[in] tBomWindow   Tag of BOMview to apply the rule to
 * @param[in] pszRule      Name of the config rule to be applied
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMT5_bom_apply_config_rule
(
    tag_t       tBomWindow,
    const char* pszRule
)
{

    /* Find rule tag with CFM_find */
    /* Set the config rule with BOM_set_window_config_rule */

    int   iRetCode      = ITK_ok;
    tag_t tRevRule      = NULLTAG;

    LMT5_ITKCALL(iRetCode, CFM_find(pszRule, &tRevRule));
    
    LMT5_ITKCALL(iRetCode, BOM_set_window_config_rule(tBomWindow, tRevRule));
    
    return iRetCode;
}

/**
 * This function retrieves the tag of the BOM view type attached to input 
 * Item revision depending upon the input BOM view type.
 *   
 * @param[in]  tItemRev            Tag of Item Revision
 * @param[in]  pszInputViewType    Type of BOMView
 * @param[out] ptBomViewType       Tag of the required BOMView
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMT5_bom_get_tag_to_view_type
(
    tag_t   tItemRev,
    char*   pszInputViewType,
    tag_t*  ptBomViewType
)
{
    int     iRetCode             = ITK_ok;
    int     iBomCount            = 0;
    tag_t   tItem                = NULLTAG;
    tag_t*  ptBomViews           = NULL;
    logical bFoundBomView        = false;

    LMT5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));
    
    LMT5_ITKCALL(iRetCode, ITEM_list_bom_views(tItem, &iBomCount, &ptBomViews));
   
    /* Cheching the BOM views attached to the ItemRev for "View" type */
    for(int iCount = 0; iCount< iBomCount && bFoundBomView == false; iCount++)
    {
        tag_t   tViewType       = NULLTAG;
        char*   pszViewName     = NULL;
        
        LMT5_ITKCALL(iRetCode, PS_ask_bom_view_type(ptBomViews[iCount], &tViewType));
        
        LMT5_ITKCALL(iRetCode, PS_ask_view_type_name(tViewType, &pszViewName));

        if (tc_strcmp(pszViewName, pszInputViewType) == 0 )
        {
            *ptBomViewType = ptBomViews[iCount];
            bFoundBomView  = true;
        }
    }

    LMT5_MEM_TCFREE(ptBomViews);
    return iRetCode;
}

/**
 * This function creates a new BOM window and applies input Revision Rule to the
 * window. All Items in the BOM will automatically be reconfigured using the new
 * Rule. Function also sets the top line of the BOM window to contain the
 * input Item Revision.
 *
 * @note The function uses error protect mark to protects the current state of the 
 *       error store. BOM commands clears the error stack which result in loss of
 *       previous errors. In order to avoid loss of error function uses protect
 *       mark commands.
 *
 * @param[in]  tItemRev         Tag of input Item revision.
 * @param[in]  pszRevisionRule  Revision rule used to configure the
 * @param[in]  pszViewType      Type of "BOM View"
 * @param[out] ptBomwindow      Tag of the newly created window.
 * @param[out] ptTopLine        Tag of the top line of the BOM window to contain
 *                              the input Item Revision
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMT5_bomline_get_top_line
(
    tag_t   tItemRev,
    char*   pszRevisionRule,
    char*   pszViewType,
    tag_t*  ptBomwindow,
    tag_t*  ptTopLine
)
{
    int     iRetCode          = ITK_ok;
    tag_t   tBomwindow        = NULLTAG;
    tag_t   tTopLine          = NULLTAG;
    tag_t   tBomViewType      = NULLTAG;
    int     iMark             = 0;

    /* Set error protect mark to protects the current state of the error store.
       BOM commands clears the error stack which result in loss of previous errors.
       In order to avoid loss of error use protect mark commands.
    */
    EMH_set_protect_mark(&iMark);

    LMT5_ITKCALL(iRetCode, LMT5_bom_get_tag_to_view_type(tItemRev, pszViewType, &tBomViewType));
    
    if(tBomViewType != NULLTAG)
    {
        LMT5_ITKCALL(iRetCode, BOM_create_window(ptBomwindow));
       

        LMT5_ITKCALL(iRetCode, LMT5_bom_apply_config_rule(*ptBomwindow, pszRevisionRule));
        
        LMT5_ITKCALL(iRetCode, BOM_set_window_top_line(*ptBomwindow, NULLTAG, tItemRev, tBomViewType, ptTopLine));
       
        /* Do a refresh of the BOM window. The content of the structure might have been changed by a different process. */
        LMT5_ITKCALL(iRetCode, BOM_refresh_window( *ptBomwindow ));
    }
    else
    {
        char*  pszObjStr  = NULL;

        LMT5_ITKCALL(iRetCode, AOM_ask_value_string(tItemRev, LMT5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr));

        TC_write_syslog("View of type %s is not attached to the %s.\n", pszViewType, pszObjStr);

        LMT5_MEM_TCFREE(pszObjStr);
    }

    EMH_clear_protect_mark(iMark);

    return iRetCode;
}

/**
 * This function gets the specific type of item rev tags and BOMlines tags from a bom structure that
 * falls in the valid list of Item revision type. Function will return item revisions from the bom in array of tags.
 *
 * @note The returned lists are not ordered but are returned bottom-up per branch.
 *
 * @note If ptItemRevs is NULL the Item revisions list is not created
 *       If ptBomLines is NULL the BOMlines list is not created
 *
 * @note Properly initialize the variable "piCount" before calling this function.
 *
 * If level <= 0 initialize lists and count
 * If level > 0 add given BOM line and its ItemRev to the lists if not NULL) use BOM_line_look_up_attribute with 
 *              bomAttr_lineItemRevTag to get the id of the attribute
 *              use BOM_line_ask_attribute_tag to get the ItemRev tag from a BOMline
 * If lev < maxlevel or maxlevel == -1:
 *              use BOM_line_ask_child_lines to get the BOM lines from one level
 *              call this function recursively for each BOM line that was returned
 *
 * @warning  This function does not close the BOM Window which is already created nor does it creates new BOM Windows while traversing 
 *           the assembly. Any usage of this function should be followed by BOM window close command if no further processing is required.
 * 
 * @warning  Learning: Sub structure occurring recursively at different level in BOM will not have same BOM Line
 *                     but instead they have unique tags.
 *
 * @param[in]  tBomLine        The top line of a BOMview
 * @param[in]  iLevel          The start level. 0 = top level
 * @param[in]  iMaxLevel       The number of levels to process. -1 = all levels.
 * @param[in]  bUnique         TRUE returns a unique list of Item revisions and BOM lines, otherwise FALSE.
 * @param[in]  iItemTypeCnt    Count of Item Revision types
 * @param[in]  ppszItemRevType List of Item revision types
 * @param[out] piCount         The number of Item Revisions returned
 * @param[out] pptItemRevs     An array of item Revision tags (may be NULL)
 * @param[out] pptBomLines     An array of BOMline tags (may be NULL)
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LMT5_bom_list_structure
(
    tag_t     tBomLine,
    int       iLevel,
    int       iMaxLevel,
    logical   bUnique,
    int*      piCount,
    tag_t**   pptItem,
    tag_t**   pptItemRevs,
    tag_t**   pptBomLines
)
{

    int      iRetCode           = ITK_ok;
    int      iMark              = 0;
    int      iAttrRevId         = 0;
    int      iBomLineCount      = 0;
    int      iCount             = 0;
    tag_t    tItem              = NULLTAG;
    tag_t    tItemRev           = NULLTAG;
    tag_t*   ptChildrenBomLines = NULL;    
    logical  bFound             = false;

    /* Set error protect mark to protects the current state of the error store.
       BOM commands clears the error stack which result in loss of previous errors.
       In order to avoid loss of error use protect mark commands.
    */
    EMH_set_protect_mark(&iMark );

    iCount = (*piCount);

    if (iLevel > 0)
    {
       int  iActiveSeq   = 0;
       
       LMT5_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId));
       
       LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBomLine, iAttrRevId, &tItemRev));

       LMT5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));
       
       LMT5_ITKCALL(iRetCode, AOM_ask_value_int(tItemRev, LMT5_ATTR_ACTIVE_SEQ, &iActiveSeq));
        /* This check indicates that Item Revisions with Active sequence will be considered */

       if(tItemRev != NULLTAG && iActiveSeq != 0 && iRetCode == ITK_ok)
       {
            logical bAddToList = false;

            for(int iIterator = 0; iIterator < iCount && bUnique == true; iIterator++)
            {
               if((*pptItemRevs)[iIterator] == tItemRev)
               {
                   bFound = true;
                   break;
               }
            }

            if(bUnique == true)
            {
               if(bFound == false)
               {
                    bAddToList = true;
               }
               else
               {
                    bAddToList = false;
               }                   
            }
            else
            {
              bAddToList = true;
            }

            if(bAddToList == true)
            {
               if(iCount == 0)
               {
                   iCount++;
                   LMT5_ADD_TAG_TO_ARRAY(iCount, (*pptItem), tItem);
                   LMT5_ADD_TAG_TO_ARRAY(iCount, (*pptItemRevs), tItemRev);
                   LMT5_ADD_TAG_TO_ARRAY(iCount, (*pptBomLines), tBomLine);
               }
               else
               {
                   iCount++;
                   LMT5_ADD_TAG_TO_ARRAY(iCount, (*pptItem), tItem);
                   LMT5_ADD_TAG_TO_ARRAY(iCount, (*pptItemRevs), tItemRev);
                   LMT5_ADD_TAG_TO_ARRAY(iCount, (*pptBomLines), tBomLine);
               }
            }
        }
    }
    else
    {
        (*pptItemRevs) = NULL;
        (*piCount) = 0;
    }


    if ((iLevel < iMaxLevel) || iMaxLevel==-1)
    {
        LMT5_ITKCALL(iRetCode, BOM_line_ask_child_lines(tBomLine, &iBomLineCount, &ptChildrenBomLines));

        for(int iLineCounter = 0; iLineCounter < iBomLineCount && iRetCode == ITK_ok; iLineCounter++)
        {
            LMT5_ITKCALL(iRetCode, LMT5_bom_list_structure(ptChildrenBomLines[iLineCounter],(iLevel+1), iMaxLevel, bUnique, 
                                                                           &iCount, pptItem, pptItemRevs, pptBomLines));
        }     
    }

    (*piCount) = iCount;
    EMH_clear_protect_mark(iMark);

    LMT5_MEM_TCFREE(ptChildrenBomLines);
    return iRetCode;
}

/**
 * This function retrives the Parent Item and Item Revision from the current BOMLine.
 *
 * @param[in]  tBomLine         Current BOMLine
 * @param[out] ptParentBOMLine  Tag of parent BOM Line
 * @param[out] ptParentItem     Tag of parent item
 * @param[out] ptParentItemRev  Tag of parent item reivsion
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMT5_bomline_get_parent_bomline_item_and_revision
(
    tag_t    tBomLine,
    tag_t*   ptParentBOMLine,
    tag_t*   ptParentItem,
    tag_t*   ptParentItemRev
)
{
    int   iRetCode           = ITK_ok;
    int   iAttrRevId         = 0;

    /* This function returns the Id of the property named "Parent" which is
     * representing the immediate parent BOMLine. Macro "bomAttr_lineParentTag"
     * represents string for the Parent BOMLine.
     */
    LMT5_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineParentTag, &iAttrRevId));

    /* We will get the tag of the Immediate Parent BOMLine */
    LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBomLine, iAttrRevId, ptParentBOMLine));
     
    iAttrRevId = 0;
    LMT5_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId));
     
    LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_tag((*ptParentBOMLine), iAttrRevId, ptParentItemRev));
     
    LMT5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(*ptParentItemRev, ptParentItem));
     
    return iRetCode;
}

/**
 * This function retrieves Item and Item Revision tags  from the current BOM Line.
 *
 * @param[in]  tBomLine   Current BOM Line
 * @param[out] ptItem     Tag of Item of current BOM Line
 * @param[out] ptItemRev  Tag of Item Revision at current BOM Line
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMT5_bomline_get_item_and_revision_from_bomline
(
    tag_t   tBomLine,
    tag_t*  ptItem,
    tag_t*  ptItemRev
)
{
    int   iRetCode           = ITK_ok;
    int   iAttrRevId         = 0;
   
    LMT5_ITKCALL(iRetCode, BOM_line_look_up_attribute(bomAttr_lineItemRevTag, &iAttrRevId));

    LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBomLine, iAttrRevId, ptItemRev));

    LMT5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(*ptItemRev, ptItem));
     
    return iRetCode;
}


/**
 * This function creates a number of occurrences linking the specified parent BOMView Revision
 * and the child Item / BOMView pairing.
 *
 * @param[in]  tParentBOMViewRev  Parent BOM View Revision tag
 * @param[in]  tNewItemRev        Tag to Item Revision to be add to BOM
 * @param[in]  tChildBOMView      Tag of the child BOMView
 * @param[in]  iNoOfOcc           The number of occurrences to be created
 * @param[out] pptOccurrences     Array of the tags of the occurrences created
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMT5_ps_add_new_component
( 
    tag_t   tParentBOMViewRev,
    tag_t   tNewItemOrRev,
    tag_t   tChildBOMView,
    int     iNoOfOcc,
    tag_t** pptOccurrences
)
{
    int  iRetCode      = ITK_ok;
   
    LMT5_ITKCALL(iRetCode, AOM_refresh(tParentBOMViewRev, true));

    LMT5_ITKCALL(iRetCode, PS_create_occurrences(tParentBOMViewRev, tNewItemOrRev, tChildBOMView,
                                                 iNoOfOcc, pptOccurrences));

    LMT5_ITKCALL(iRetCode, AOM_save(tParentBOMViewRev));

    LMT5_ITKCALL(iRetCode, AOM_refresh(tParentBOMViewRev, false));

    return iRetCode;
}

/**
 * Given the item, item revision and bom view type returns the tag for the bom
 * view connected to item and the bom view revison tag connected to item 
 * revision.
 *
 * @note  If bCreateNew flag is true, it creates the bom view revision for the
 *        item revision.
 * @note  If bEmptyBVR flag is true, if bom view revision is already connected to
 *        item revision, then it will clear the occurences of bom view revision.
 *
 * @param[in] tItem              Tag of item
 * @param[in] tItemRev           Tag of item revision
 * @param[in] pszViewType        BOM view type which needs to searched/created
 * @param[in] bCreateNew         Creates bom view revision if there is no bom view
 *                               connected
 * @param[in] bEmptyBVR          If true will delete all occurences of BVR if there is
 *                               already a BVR connected to item revision.
 * @param[in] bCreatePreciseBvr  'true' if BVR to be created should be Precise
 * @param[out] ptBomView         Tag of BOM view connected to item
 * @param[out] ptBomViewRevision Tag of BVR connected to item revision.
 *
 * @retval  BOM_VIEW_TYPE_NOT_DEFINED if bom view type not found
 * @retval ITK_ok if successfull in getting/creating bom view revision or itk
 * error code.
 */
int LMT5_bom_get_bomview_revision
(
   tag_t   tItem,
   tag_t   tItemRev,
   char*   pszViewType,
   logical bCreateNew,
   logical bEmptyBVR,
   logical bCreatePreciseBvr,
   tag_t*  ptBomView,
   tag_t*  ptBomViewRevision
)
{
    int     iRetCode     = ITK_ok;
    int     iBVRCount    = 0;
    tag_t   tBVType      = NULLTAG;
    tag_t*  ptBomViewRev = NULL;

    /* Initializing the Out Parameter to this function. */
    (*ptBomView)         = NULLTAG;
    (*ptBomViewRevision) = NULLTAG;

    /* Get bom view type tag */
    LMT5_ITKCALL(iRetCode, PS_find_view_type(pszViewType, &tBVType));
    
    if(tBVType != NULLTAG)
    {
        /* Get all the connected bom view to item revision */
        LMT5_ITKCALL(iRetCode, ITEM_rev_list_bom_view_revs(tItemRev, &iBVRCount, &ptBomViewRev));
       
        for(int iCount = 0; iCount < iBVRCount && iRetCode == ITK_ok; iCount++)
        {
            tag_t tBomView  = NULLTAG;
            tag_t tViewType = NULLTAG;

            LMT5_ITKCALL(iRetCode, PS_ask_bom_view_of_bvr(ptBomViewRev[iCount], &tBomView));
           
            LMT5_ITKCALL(iRetCode, PS_ask_bom_view_type(tBomView, &tViewType));

            if (tViewType == tBVType)
            {
                (*ptBomViewRevision) = ptBomViewRev[iCount];
                break;
            }
        }

        /* Get item bom view revision */
        LMT5_ITKCALL(iRetCode, LMT5_bom_get_bom_view(tItem, pszViewType, bCreateNew, ptBomView));
        
        /* Check whether the BOM view was found,if not found create one */
        if((*ptBomViewRevision) == NULLTAG && bCreateNew == true)
        {
            tag_t tItemBV = NULLTAG;

            /* 'tItemBV' cannot be null */
            LMT5_ITKCALL(iRetCode, PS_create_bvr((*ptBomView), NULL, NULL, bCreatePreciseBvr, 
                                                  tItemRev, ptBomViewRevision));

            LMT5_ITKCALL(iRetCode, LMT5_obj_save_and_unlock(*ptBomViewRevision)); 

            /*  Save the object.  */
            LMT5_ITKCALL(iRetCode, AOM_save( tItemRev));
        }
        else if(bEmptyBVR == true)
        {
            LMT5_ITKCALL(iRetCode, LMT5_bom_clear_bvr(*ptBomViewRevision));            
        }
    }
    else
    {
        iRetCode = BOM_VIEW_TYPE_NOT_DEFINED;

        /*store error into the error stack*/
        EMH_store_error(EMH_severity_error,iRetCode);
    }

    LMT5_MEM_TCFREE(ptBomViewRev);
    return iRetCode;
}

/**
 * Clear the occurences of Bom view revision.
 *
 * @param[in] tBomViewRevision BVR whoes occurence needs to be cleared.
 *
 * @retval ITK_ok if successfull otherwise itk error code
 */
int LMT5_bom_clear_bvr
(
    tag_t  tBomViewRevision
)
{
    int    iRetCode   = ITK_ok;
    int    iNoOfOccs  = 0;
    tag_t* ptNotes    = NULL;
    tag_t* ptOccs     = NULL;

    /* Get all occurences */
    LMT5_ITKCALL(iRetCode, PS_list_occurrences_of_bvr(tBomViewRevision, &iNoOfOccs, &ptOccs));

    /* Lock the BVR for modification */
    LMT5_ITKCALL(iRetCode, AOM_refresh(tBomViewRevision, true));
    
    /* List all notes, delete them and then delete the occurence */
    for(int iCount = 0; iCount < iNoOfOccs && iRetCode == ITK_ok; iCount++)
    {
        int iNoOfNotes = 0;
        int iNoteCount = 0;

        LMT5_ITKCALL(iRetCode, PS_list_occurrence_notes(tBomViewRevision, ptOccs[iCount],
                                                        &iNoOfNotes, &ptNotes));
        
        for(iNoteCount = 0; iNoteCount<iNoOfNotes; iNoteCount++)
        {
            LMT5_ITKCALL(iRetCode, PS_delete_occurrence_note(tBomViewRevision, ptOccs[iCount],
                                                             ptNotes[iNoteCount]));
        }

        LMT5_MEM_TCFREE(ptNotes);

        /* Delete occurence */
        LMT5_ITKCALL(iRetCode, PS_delete_occurrence(tBomViewRevision, ptOccs[iCount]));
    }

    LMT5_ITKCALL(iRetCode, AOM_save(tBomViewRevision));

    LMT5_ITKCALL(iRetCode, AOM_refresh(tBomViewRevision, false));
    
    LMT5_MEM_TCFREE(ptOccs);
    LMT5_MEM_TCFREE(ptNotes);
    return iRetCode;

}


/**
 * Given the item and the bom view type, gets the given bom view type attahced
 * to item or if there is no bom view connected and lCreateNew flag is true,
 * creates a BOM view and attaches to the given item
 *
 * param[in] tItem        Item whoes bom view needs to be retrieved
 * param[in] pszViewType  Type of bom view to be retrived/created
 * param[in] bCreateNew   If true and bom view not found, creates bom view for
 *                        given item
 * param[out] ptBomView   Found/ created bom view.
 *
 * @retval BOM_VIEW_TYPE_NOT_DEFINED if bom view type not found
 * @retval ITK_ok if successfull in getting/creating bom view or itk error code.
 */
int LMT5_bom_get_bom_view
(
   tag_t    tItem,
   char*    pszViewType,
   logical  bCreateNew,
   tag_t*   ptBomView
)
{
    int     iRetCode    = ITK_ok;
    int     iBomCount   = 0;
    tag_t   tBvType     = NULLTAG;
    tag_t   tFoundBV    = NULLTAG;
    tag_t*  ptBomViews  = NULL;

    /* Initializing the Out Parameter to this function. */
    (*ptBomView) = NULLTAG;

    LMT5_ITKCALL(iRetCode, PS_find_view_type(pszViewType, &tBvType));
    
    if(tBvType != NULLTAG)
    {
        LMT5_ITKCALL(iRetCode, ITEM_list_bom_views(tItem, &iBomCount, &ptBomViews));
        
        for(int iCount = 0; iCount < iBomCount && iRetCode == ITK_ok; iCount++)
        {
            LMT5_ITKCALL(iRetCode, PS_ask_bom_view_type(ptBomViews[iCount], &tFoundBV));
            
            if(tFoundBV == tBvType)
            {
                (*ptBomView) = ptBomViews[iCount];
                break;
            }
        }

        /* Check whether the BOM view was found, if not found create one if bCreateNew is set */
        if((*ptBomView) == NULLTAG && bCreateNew == true)
        {
            /* Create bom view */
            LMT5_ITKCALL(iRetCode, PS_create_bom_view(tBvType, NULL, NULL, tItem, ptBomView));
            
            LMT5_ITKCALL(iRetCode, LMT5_obj_save_and_unlock(*ptBomView));

            /*  Save the object.  */
            LMT5_ITKCALL(iRetCode, AOM_save(tItem));
        }
    }
    else
    {
        iRetCode = BOM_VIEW_TYPE_NOT_DEFINED;

        /*store error into the error stack*/
        EMH_store_error(EMH_severity_error,iRetCode);
    }

    LMT5_MEM_TCFREE(ptBomViews);
    return iRetCode;
}

/**
 * This function gets the occurrence from the input Parent item revision and its
 * required child item revision. It expands the BOM of the parent item revision
 * based on the input revision rule to check if the required item revision
 * is one of its child. If yes, returns its occurrence tag.
 * Bomview and BomviewRevision are also returned, so that if required
 * it can be used by the calling function.
 *
 * @note This function does a single level expand of BOM.
 *
 * @param[in]  tItemRev          Tag of Item Revision whose BOM is to be traversed.
 * @param[in]  tRequiredItemRev  Tag of Item revision which is to be searched among
 * 								 children of parent item revision.
 * @param[in]  tConfigRule       Tag of Revision rule that is applied.
 * @param[out] ptOccurrence		 Tag of occurrence.
 * @param[out] ptBomView		 Tag of BOMView of parent item revision.
 * @param[out] ptBomViewRevision Tag of BOMView revision of parent item revision.
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMT5_bomocc_get_occurance
(
	tag_t  tItemRev,
	tag_t  tRequiredItemRev,
	tag_t  tConfigRule,
	tag_t* ptOccurrence,
	tag_t* ptBomView,
	tag_t* ptBomViewRevision
)
{
	int   	iRetCode            = ITK_ok;
	int   	iNoOfOccs			= 0;
	tag_t   tItem				= NULLTAG;
    tag_t*  ptOccurrences		= NULL;
	logical lIsPrecise			= false;

    LMT5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));

	LMT5_ITKCALL(iRetCode, LMT5_bom_get_bomview_revision(tItem, tItemRev, LMT5_BOM_VIEW_TYPE_VIEW, false,
		 									              false, false, ptBomView, ptBomViewRevision));

	LMT5_ITKCALL(iRetCode, PS_ask_is_bvr_precise(*ptBomViewRevision, &lIsPrecise));

 	LMT5_ITKCALL(iRetCode, PS_list_occurrences_of_bvr(*ptBomViewRevision, &iNoOfOccs, &ptOccurrences));

 	for(int iCount = 0; iCount < iNoOfOccs && iRetCode == ITK_ok; iCount++)
	{
        tag_t tOccItemOrIR   = NULLTAG;
        tag_t tChildBomView  = NULLTAG;
        tag_t tConfiguredRev = NULLTAG;

		/* Function returns the tag of the child Item or Item Revision referenced by the occurrence
           If the parent BOMViewRevision is "precise," child_item will be the tag of an Item Revision.
           If the parent BOMViewRevision is "imprecise", child_item will be the tag of an Item.
        */
		LMT5_ITKCALL(iRetCode, PS_ask_occurrence_child(*ptBomViewRevision, ptOccurrences[iCount],
											             &tOccItemOrIR, &tChildBomView));
		if(lIsPrecise == true)
        {
            tConfiguredRev = tOccItemOrIR;
        }
        else
        {
            char* pszHowConfigured = NULL;

            /* In "imprecise" case we need to find the ItemRev. */
            LMT5_ITKCALL(iRetCode, CFM_item_ask_configured(tConfigRule, tOccItemOrIR, &tConfiguredRev,
                                                           &pszHowConfigured));
	        LMT5_MEM_TCFREE(pszHowConfigured);
        }

        if(tConfiguredRev == tRequiredItemRev)
        {
            (*ptOccurrence) = ptOccurrences[iCount];
            break;
        }
	}

	LMT5_MEM_TCFREE(ptOccurrences);
    return iRetCode;
}

/**
 * This function retrieves the attribute value of the input attribute name
 * from input BOM Line.
 *
 * @param[in] tBomLine	   		BOM line tag.
 * @param[in] pszAttrName  		Name of  BOM line attribute whose value is to be retrieved.
 * @param[out] ppszAttributeVal	Value of input attribute as string.
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LMT5_bomline_get_attribute_val
(
	tag_t  tBomLine,
	char*  pszAttrName,
	char** ppszAttributeVal
)
{
    int  iRetCode 		= ITK_ok;
    int	 iMode 			= 0;
    int  iAttributeId	= 0;
    char *pszName 		= NULL;

    LMT5_ITKCALL(iRetCode, BOM_line_look_up_attribute((char*)pszAttrName, &iAttributeId));
   
    LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_mode(iAttributeId, &iMode));
    
    *ppszAttributeVal = (char*) MEM_alloc(sizeof (char) * LMT5_BOM_ATTR_STRING_LEN);

    switch(iMode)
    {
        case BOM_attribute_mode_string :
        {
            LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_string(tBomLine, iAttributeId, &pszName));

            sprintf(*ppszAttributeVal, "%s",  pszName == NULL ? "" : pszName);
            break;
        }
        case BOM_attribute_mode_logical :
        {
            logical bYesNo;

            LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_logical(tBomLine, iAttributeId, &bYesNo));

            bYesNo ? sprintf(*ppszAttributeVal , "%s" , "y") : printf (*ppszAttributeVal , "%s", "n");
            break;
        }
        case BOM_attribute_mode_int :
        {
            int iValue = 0;

            LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_int(tBomLine, iAttributeId, &iValue));

            sprintf(*ppszAttributeVal, "%d",  iValue);
            break;
        }
        case BOM_attribute_mode_tag :
        {
            tag_t 	tValue  	  = NULLTAG ;
            char 	*pszStringVal = NULL;

            LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_tag(tBomLine, iAttributeId, &tValue));

            if(tValue != NULLTAG)
                iRetCode = PROP_tag_to_string (tValue, &pszStringVal );

            sprintf (*ppszAttributeVal, "%s",  pszStringVal == NULL ? "" : pszStringVal);

            LMT5_MEM_TCFREE(pszStringVal);
            break;
        }
        case BOM_attribute_mode_double :
        {
            double dValue = 0.0;

            LMT5_ITKCALL(iRetCode, BOM_line_ask_attribute_double(tBomLine, iAttributeId, &dValue));

            sprintf (*ppszAttributeVal, "%f",  dValue);
            break;
        }
        default :
        {
            sprintf (*ppszAttributeVal, "%s",  "");
        }
    }

	LMT5_MEM_TCFREE(pszName);
    return iRetCode;
}