/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_string_macros.hxx

    Description:  This file contains macros for string's basic operation such as copy, concatination etc.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LMT5_STRING_MACROS_HXX
#define LMT5_STRING_MACROS_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <lmt5_library.hxx>

#ifdef LMT5_CUST_STRCPY
#undef LMT5_CUST_STRCPY
#endif

#define LMT5_CUST_STRCPY(pszDestiStr, pszSourceStr){\
            pszDestiStr = (char*)MEM_alloc((int)(sizeof(char)* ( tc_strlen(pszSourceStr) + 1 )));\
            tc_strcpy(pszDestiStr, pszSourceStr);\
}

#define LMT5_CUST_STRCAT(pszDestiStr, pszSourceStr){\
    int iSourceStrLen = (int) tc_strlen(pszSourceStr);\
    int iDestiStrLen = 0;\
    if(pszDestiStr != NULL){\
        iDestiStrLen = (int) tc_strlen (pszDestiStr);\
        pszDestiStr = (char*)MEM_realloc(pszDestiStr, (int)((iSourceStrLen + iDestiStrLen + 1)* sizeof(char)));\
    }\
    else{\
        pszDestiStr = (char*)MEM_alloc((int)(sizeof(char) * (iSourceStrLen + 1)));\
    }\
    if(iDestiStrLen == 0){\
       tc_strcpy(pszDestiStr, pszSourceStr);\
    }\
    else{\
       tc_strcat(pszDestiStr, pszSourceStr);\
       tc_strcat(pszDestiStr, '\0');\
       }\
}

/**
 * This function adds input new string to the String Array. This also increases the value of the variable holding the 
 * original size of array
 *  
 */
#define LMT5_CUST_UPDATE_STRING_ARRAY(iCurrentArrayLen, ppszStrArray, pszNewValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ppszStrArray = (char**) MEM_alloc(iCurrentArrayLen * sizeof(char*));\
            }\
            else{\
                ppszStrArray = (char**) MEM_realloc(ppszStrArray, iCurrentArrayLen * sizeof(char*));\
            }\
            ppszStrArray[iCurrentArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszNewValue) + 1)* sizeof(char)));\
            tc_strcpy(ppszStrArray[iCurrentArrayLen - 1], pszNewValue);\
}


/* This macro add the new string to new index which is supplied to this macro as input argument. 
   This macro does not increments the variable 'iNewArrayLen' instead expects the calling function to should
   provide the incremented size of the string array.
*/
#define LMT5_CUST_ADD_STRING_TO_ARRAY(iNewArrayLen, ppszStrArray, pszNewValue){\
            if(iNewArrayLen == 1){\
                ppszStrArray = (char**) MEM_alloc(iNewArrayLen * sizeof(char*));\
            }\
            else{\
                ppszStrArray = (char**) MEM_realloc(ppszStrArray, iNewArrayLen * sizeof(char*));\
            }\
            ppszStrArray[iNewArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszNewValue) + 1)* sizeof(char)));\
            tc_strcpy(ppszStrArray[iNewArrayLen - 1], pszNewValue);\
}

#endif //LMT5_STRING_MACROS_HXX







