/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_itk_mem_free.hxx

    Description:  This file contains macros for freeing portion of memory allocated with TCe 'MEM_' functions and by TCe itself.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14     Tarun Kumar Singh   Initial Release
   
========================================================================================================================================================================*/
#ifndef LMT5_ITK_MEM_FREE_HXX
#define LMT5_ITK_MEM_FREE_HXX

#include <lmt5_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef LMT5_MEM_TCFREE
#undef LMT5_MEM_TCFREE
#endif

// Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc

#define LMT5_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }


#ifdef LMT5_UPDATE_TAG_ARRAY
#undef LMT5_UPDATE_TAG_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation if done for the very first time
*/
#define LMT5_UPDATE_TAG_ARRAY(iCurrentArrayLen, ptArray, tValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ptArray = (tag_t*) MEM_alloc(iCurrentArrayLen * sizeof(tag_t));\
            }\
            else{\
                ptArray = (tag_t*) MEM_realloc(ptArray, iCurrentArrayLen * sizeof(tag_t));\
            }\
            ptArray[iCurrentArrayLen-1] = tValue;\
}


#ifdef LMT5_ADD_TAG_TO_ARRAY
#undef LMT5_ADD_TAG_TO_ARRAY
#endif
/* This macro add the new tag to new index which is supplied to this macro as input argument. 
   This macro does not increments the variable 'iNewArrayLen' instead expects the calling function to should
   provide the incremented size of the tag array.
*/
#define LMT5_ADD_TAG_TO_ARRAY(iNewArrayLen, ptArray, tValue){\
            if(iNewArrayLen == 1){\
                ptArray = (tag_t*) MEM_alloc(iNewArrayLen * sizeof(tag_t));\
            }\
            else{\
                ptArray = (tag_t*) MEM_realloc(ptArray, iNewArrayLen * sizeof(tag_t));\
            }\
            ptArray[iNewArrayLen-1] = tValue;\
}

//#ifdef LMT5_FREE_SW_STRUCT_ARRAY
//#undef LMT5_FREE_SW_STRUCT_ARRAY
//#endif
///* This macro is used to free the memory of structure. */
//#define LMT5_FREE_SW_STRUCT_ARRAY(psSWDepIRLst, iCount) {\
//          for(int iDx = 0; iDx < iCount; iDx++) {\
//            if((psSWDepIRLst[iDx].iRelatedIRCnt) != 0){\
//               LMT5_MEM_TCFREE(psSWDepIRLst[iDx].ptRelatedIR);\
//            }\
//          }\
//          LMT5_MEM_TCFREE(psSWDepIRLst);\
//        }

#endif










