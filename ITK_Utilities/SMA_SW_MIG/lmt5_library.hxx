/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_library.hxx

    Description:  Header File containing declarations of function spread across all

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LMT5_LIBRARY_HXX
#define LMT5_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom/pom/pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me/me.h>
#include <form/form.h>
#include <tccore/project.h>
#include <res/res_itk.h>
#include <sa/am.h>
#include <fclasses/tc_date.h>
#include <tc/tc_util.h>
#include <tcinit/tcinit.h>
#include <conio.h>
#include <tccore/license.h>
#include <lmt5_const.hxx>
#include <lmt5_itk_macro.hxx>
#include <lmt5_itk_mem_free.hxx>
#include <lmt5_string_macros.hxx>

/*        Utility Functions For Error Logging        */
int LMT5_err_write_to_logfile
(
    char*       pszErrorFunction,
    int         iErrorNumber,
    char*       pszFileName,
    int         iLineNumber
);

int LMT5_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
);


/*        Utility Functions For Objects        */
int LMT5_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
);

int LMT5_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
);

int LMT5_obj_save_and_unlock
(
    tag_t       tObject
);

int LMT5_obj_ask_status_tag
(
    tag_t      tObject,
    char*      pszStatusName,
    tag_t*     ptStatusTag
);

int LMT5_obj_ask_status_list
(
    tag_t       tObject,
    char***     pppszStatusNameList,
    int*        piStatusCount,
    tag_t**     pptStatusTagList
);

int LMT5_obj_set_existing_status 
(
    tag_t tObjectTag,
    tag_t tReleaseStatus
);

int LMT5_obj_set_src_obj_status_to_dest
(
    tag_t tSrcObj,
    tag_t tDestObj
);

int LMT5_obj_dereference_status
( 
    tag_t  tObject
);

int LMT5_obj_remove_value_from_vla_attr
( 
    tag_t  tObject,
    char*  pszAttrName,
    tag_t  tValue
);

int LMT5_validate_ir_rel_and_ds_creation
(
    tag_t    tItemRev,
    tag_t    tDataset,
    logical* pbIsRelBefore
);

/*        Utility Functions For String Operations        */
int LMT5_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
);

int LMT5_dt_get_date
(
    date_t* pdReleaseDate
);

int LMT5_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
);

char* LMT5_trim( char* pszString );

void LMT5_trim_end(char *pszString);


/*        Utility Functions For BOM        */
int LMT5_bom_apply_config_rule
(
    tag_t       tBomWindow,
    const char* pszRule
);

int LMT5_bom_get_tag_to_view_type
(
    tag_t   tItemRev,
    char*   pszInputViewType,
    tag_t*  ptBomViewType
);

int LMT5_bomline_get_top_line
(
    tag_t   tItemRev,
    char*   pszRevisionRule,
    char*   pszViewType,
    tag_t*  ptBomwindow,
    tag_t*  ptTopLine
);

int LMT5_bom_list_structure
(
    tag_t     tBomLine,
    int       iLevel,
    int       iMaxLevel,
    logical   bUnique,
    int*      piCount,
    tag_t**   pptItem,
    tag_t**   pptItemRevs,
    tag_t**   pptBomLines
);

int LMT5_bomline_get_parent_bomline_item_and_revision
(
    tag_t    tBomLine,
    tag_t*   ptParentBOMLine,
    tag_t*   ptParentItem,
    tag_t*   ptParentItemRev
);

int LMT5_bomline_get_item_and_revision_from_bomline
(
    tag_t   tBomLine,
    tag_t*  ptItem,
    tag_t*  ptItemRev
);

int LMT5_ps_add_new_component
( 
    tag_t   tParentBOMViewRev,
    tag_t   tNewItemOrRev,
    tag_t   tChildBOMView,
    int     iNoOfOcc,
    tag_t** pptOccurrences
);

int LMT5_bom_get_bomview_revision
(
   tag_t   tItem,
   tag_t   tItemRev,
   char*   pszViewType,
   logical bCreateNew,
   logical bEmptyBVR,
   logical bCreatePreciseBvr,
   tag_t*  ptBomView,
   tag_t*  ptBomViewRevision
);

int LMT5_bom_clear_bvr
(
    tag_t  tBomViewRevision
);

int LMT5_bom_get_bom_view
(
   tag_t    tItem,
   char*    pszViewType,
   logical  bCreateNew,
   tag_t*   ptBomView
);

int LMT5_bomocc_get_occurance
(
	tag_t  tItemRev,
	tag_t  tRequiredItemRev,
	tag_t  tConfigRule,
	tag_t* ptOccurrence,
	tag_t* ptBomView,
	tag_t* ptBomViewRevision
);

int LMT5_bomline_get_attribute_val
(
	tag_t  tBomLine,
	char*  pszAttrName,
	char** ppszAttributeVal
);

/*        Utility Functions For GRM        */
int LMT5_grm_create_relation
(
    tag_t   tPrimaryObj,
    tag_t   tSecondaryObj,
    char*   pszRelationType,
    tag_t*  ptRelation
);

int LMT5_grm_delete_relation
(
    tag_t   tPrimaryObj,
    tag_t   tRelation
);

int LMT5_grm_get_related_obj
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    char*    pszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary,
    tag_t**  pptValidRelatedObj,
    int*     piRelatedObjCnt
);

int LMT5_grm_get_related_obj_of_type
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    int      iTypeCnt,
    char**   ppszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary,
    tag_t**  pptValidRelatedObj,
    tag_t**  pptRelationObj,
    int*     piRelatedObjCnt
);

#endif





