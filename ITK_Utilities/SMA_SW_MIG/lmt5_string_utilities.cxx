/*======================================================================================================================================================================
                Copyright 2014  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lmt5_string_utilities.cxx

    Description:  This File contains custom functions for Stringto perform generic operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Sep-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#include <lmt5_library.hxx>
#include <lmt5_const.hxx>
#include <lmt5_errors.hxx>

/**
 * This function gets the current date time.
 *
 * @param[in]  pszFormat   Expected date time format.
 * @param[out] szDateTime  current Date time in required format.
 *
 * @note 'szDateTime' must be a character array of at least [IMF_filename_size_c + 1] 
 *       characters 
 *
 * @retval ITK_ok on successful execution
 * @retval INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME if format resulted in
 *         resultant String length greater then IMF_filename_size_c + 1
 */
int LMT5_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
)
{
    int         iRetCode       = ITK_ok;
    int         iStrLength     = 0;
    struct tm   *clLocalTime   = NULL;
    time_t      tCurrentTime;

    /*Get system's local time*/
    tCurrentTime = time(NULL);
    clLocalTime  = localtime(&tCurrentTime);
        
    /*Get system time  in required format*/
    iStrLength =(int)( strftime( szDateTime, (IMF_filename_size_c + 1), pszFormat, clLocalTime));
   
      
    return iRetCode;
}

/**
 * Function gets current date in TC required Format. Get the current system date
 * and set it to TC Date structure.
 *
 * @param[out] dReleaseDate  Constructed Current date.
 *    
 * @return ITK_ok if function succedded.
 */
int LMT5_dt_get_date
(
    date_t* pdReleaseDate
)
{
    time_t  run_time;
    struct  tm* sConvTime =  NULL;

    /* Get time thru system calls. */
    run_time = time(NULL);
    sConvTime = localtime(&run_time);

    /* Convert the time into TC format */
    pdReleaseDate->year   = 1900+ sConvTime->tm_year;
    pdReleaseDate->month  = sConvTime->tm_mon;
    pdReleaseDate->day    = sConvTime->tm_mday;
    pdReleaseDate->hour   = sConvTime->tm_hour;
    pdReleaseDate->minute = sConvTime->tm_min;
    pdReleaseDate->second = sConvTime->tm_sec;

    return 0;
}

/**
 * Parses a string containing values seperated by a given separator
 * into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in] pszList          Input list to be parsed
 * @param[in] cSep             Separator character
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by LMT5_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 * @retval INCORRECT_INPUT If list or seprator is Null.
 * @retval MEMORY_ALLOCATION_FAILED If cannot allocate memory.
 */
int LMT5_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
)
{
    int        iRetCode    = ITK_ok;
    int        iCounter    = 0;
    char*      pszToken    = "";
    char*      pszTempList = NULL;

    /* Convert separator (char) to char* - strtok(char* input, Char* sep) */
    char pszSeparator[LMT5_SEPARATOR_LENGTH+1];
    pszSeparator[0] = cSep;
    pszSeparator[1] = '\0';

    /* Validate input */
    if((pszList == NULL) || (cSep == '\0'))
    {
        TC_write_syslog( "ERROR: Missing input data:List passed or seprator is null\n");
    }
    else
    {
        /* Allocating memory to pszTempList */
        pszTempList  = (char*) MEM_alloc(sizeof(char)*((int)tc_strlen(pszList)+1));
        if (pszTempList == NULL)
        {
            TC_write_syslog( "ERROR: Cannot allocate memory\n");
        }
        else
        {
            /* Copying the input string to Null terminated string */
            tc_strcpy(pszTempList, pszList);

            /* Get the first token */
            pszToken = tc_strtok( pszTempList,pszSeparator );

            *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
            /* Tokenize the input string */
            while( pszToken != NULL )
            {
                /* Allocate memory for each string pointer */
                (*pppszValuelist)[iCounter] = (char*) MEM_alloc(sizeof(char)*(((int)tc_strlen(pszToken))+1));
                
                /* Copy the token to the output string pointer */
                tc_strcpy((*pppszValuelist)[iCounter],pszToken);

                /* Count the number of elements in the string */
                iCounter++;
                
                /* Allocating memory to pointer of string (output) as per the number of tokens found */
                *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));
                
                /* Get next token: */
                pszToken = tc_strtok( NULL, pszSeparator );
               
            }

            if(iRetCode == ITK_ok)
            {
                *iCount = iCounter;
            }
        }  
    }

    /* Free the temporary memory used for tokenizing */
    LMT5_MEM_TCFREE(pszTempList);
    return iRetCode;
}

/**
 * Function to Strips leading and trailing space. 
 *
 * @param[in]  pszString  String to trim white spaces 
 * @param[out] Pointer to Trimmed string 
 */
char* LMT5_trim( char* pszString )
{
  if (pszString == NULL)
    return(NULL);

  LMT5_trim_end(pszString);

    /*===================================================================*/
    /* Skip over spaces, and return the address of the first non-space   */
    /* character. If there are no leading spaces, this just returns str. */
    /*===================================================================*/

  return &pszString[strspn( pszString, " \t" )];

} 

/**
 * Function to Strips all trailing spaces from the given null-terminated string
 *
 * @param[in/out] pszString  String to trim white spaces
 *
 */

void LMT5_trim_end(char *pszString)
{
  int iLen = (int)(tc_strlen( pszString ) - 1);

  while (iLen >= 0  &&  ((pszString[iLen] == ' ') || 
          (pszString[iLen] == '\t') || (pszString[iLen] == '\n')))
      --iLen;

  pszString[iLen + 1] = '\0';

}