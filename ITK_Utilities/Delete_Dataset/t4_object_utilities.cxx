/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_object_utilities.cxx

    Description: This file contains function performing various operations on Teamcenter Business Objects

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Mar-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <t4_errors.hxx>
#include <t4_library.hxx>

/**
 * Set the Tag attribute value.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] tPropVal        Value of the property to be set
 *
 * @note The object must be an instance of Application Object class or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int T4_obj_set_tag_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    tag_t       tPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    T4_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute string value.
     */    
    T4_ITKCALL(iRetCode, AOM_set_value_tag(tObject, pszPropertyName, tPropVal));

    /*  Save the object.  */
    T4_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     T4_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}

/**
 * Set the string attribute value.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] pszPropVal      Value of the property to be set
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int T4_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    T4_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute string value.
     */    
    T4_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszPropertyName, pszPropVal));

    /*  Save the object.  */
    T4_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     T4_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}

/**
 * This function sets the string attribute value of the dataset and saves any changes made to the dataset mydataset directly to itself. 
 * This function uses AE_save_myself saves changes directly into the mydataset dataset version without creating any new dataset version.
 *
 * @note : We recommend you use AE_save_myself whenever you make changes to the attributes or owner/group of a dataset. 
 *
 * @param[in] tDataset        Tag of Dataset
 * @param[in] pszPropertyName Name of the property
 * @param[in] pszPropVal      Value of the property to be set
 *
 * @return ITK_ok is successful else ITK return code.
 */
int T4_obj_set_ds_str_attr_without_version_change
(
    tag_t       tDataset,
    char*       pszPropertyName,
    char*       pszPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    T4_ITKCALL(iRetCode, AOM_refresh( tDataset, true));
   
    /*
     * Set the attribute string value.
     */    
    T4_ITKCALL(iRetCode, AOM_set_value_string(tDataset, pszPropertyName, pszPropVal));

    /*  Save the object.  */
    T4_ITKCALL(iRetCode, AE_save_myself( tDataset ));

    /*  Unlock the object.  */
     T4_ITKCALL(iRetCode, AOM_refresh( tDataset, false));

    return iRetCode;
}

/**
 * Parses a string containing values seperated by a given separator
 * into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in] pszList          Input list to be parsed
 * @param[in] cSep             Separator character
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by T4_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 * @retval INCORRECT_INPUT If list or seprator is Null.
 * @retval MEMORY_ALLOCATION_FAILED If cannot allocate memory.
 */
int T4_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
)
{
    int        iRetCode    = ITK_ok;
    int        iCounter    = 0;
    char*      pszToken    = "";
    char*      pszTempList = NULL;

    /* Convert separator (char) to char* - strtok(char* input, Char* sep) */
    char pszSeparator[T4_SEPARATOR_LENGTH+1];
    pszSeparator[0] = cSep;
    pszSeparator[1] = '\0';

    /* Validate input */
    if((pszList == NULL) || (cSep == '\0'))
    {
        TC_write_syslog( "ERROR: Missing input data:List passed or seprator is null\n");
    }
    else
    {
        /* Allocating memory to pszTempList */
        pszTempList  = (char*) MEM_alloc(sizeof(char)*((int)tc_strlen(pszList)+1));
        if (pszTempList == NULL)
        {
            TC_write_syslog( "ERROR: Cannot allocate memory\n");
        }
        else
        {
            /* Copying the input string to Null terminated string */
            tc_strcpy(pszTempList, pszList);

            /* Get the first token */
            pszToken = tc_strtok( pszTempList,pszSeparator );

            *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
            /* Tokenize the input string */
            while( pszToken != NULL )
            {
                /* Allocate memory for each string pointer */
                (*pppszValuelist)[iCounter] = (char*) MEM_alloc(sizeof(char)*(((int)tc_strlen(pszToken))+1));
                
                /* Copy the token to the output string pointer */
                tc_strcpy((*pppszValuelist)[iCounter],pszToken);

                /* Count the number of elements in the string */
                iCounter++;
                
                /* Allocating memory to pointer of string (output) as per the number of tokens found */
                *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));
                
                /* Get next token: */
                pszToken = tc_strtok( NULL, pszSeparator );
               
            }

            if(iRetCode == ITK_ok)
            {
                *iCount = iCounter;
            }
        }  
    }

    /* Free the temporary memory used for tokenizing */
    T4_MEM_TCFREE(pszTempList);
    return iRetCode;
}

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int T4_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
	tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    T4_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    T4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    T4_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}

/**
 * Set the release status of an object. Function allows adding multiple status to
 * input Business Object. Gets the attribute id for "release_status_list" and "date_released"
 * for input object and then set the attribute values to create status.
 * 
 *
 * @note Return Error if unable to set release status.
 *
 * @note Code gets the VLA length to the Business Object to which status is to be 
 *       attached. If Business Object already have some status attached then  
 *       'Position in the VLA to start setting values' argument to function  
 *       'POM_set_attr_tags' will be increased by one. Hence this allows to add
 *       multiple status to same Business Object.
 *
 * @param[in]  pszStatusName    Name of status to be created
 * @param[in]  tObjectTag       The object to get the release status from
 * @param[out] ptReleaseStatus  Returned status
 *
 * @return ITK_ok is successful else ITK return code.
 */
int T4_obj_create_and_set_status 
(
    char*  pszStatusName, 
    tag_t  tObjectTag,
    tag_t* ptReleaseStatus
)
{
    int      iRetCode                                = ITK_ok;
    int      iVLALength                              = 0;
    tag_t    tReleaseStatus                          = NULLTAG;
    tag_t    tReleaseStatusList                      = NULLTAG;
    tag_t    tReleaseDateTag                         = NULLTAG;
    tag_t    tTypeTag                                = NULLTAG;
    tag_t    tClassTag                               = NULLTAG;
    tag_t    tDateTag                                = NULLTAG;
    char     szClassName[TCTYPE_class_name_size_c+1] = {""};
    date_t   dReleaseDate                            = NULLDATE;

    /* Initializing the Out Parameter to this function. */
    (*ptReleaseStatus) = NULLTAG;
    
    /*  Lock the object.  */
    T4_ITKCALL(iRetCode, AOM_lock( tObjectTag));

    /* Create an instance of Release Tag */
    T4_ITKCALL(iRetCode, CR_create_release_status(pszStatusName, &tReleaseStatus));
    
    if (iRetCode != ITK_ok)
    {
        TC_write_syslog("\nUnable to create the instance of Release status:%s \n", pszStatusName);
        return iRetCode;
    }
  
    T4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObjectTag, &tTypeTag));
        
    T4_ITKCALL(iRetCode, TCTYPE_ask_class_name(tTypeTag, szClassName));
        
    /* Get attribute tags for Release Status and Date Released on 
       Item revision class
    */
     T4_ITKCALL(iRetCode, POM_attr_id_of_attr(T4_ATTR_RELEASE_STATUS_LIST, szClassName, &tReleaseStatusList)); 
        
     T4_ITKCALL(iRetCode, POM_attr_id_of_attr(T4_ATTR_DATE_RELEASED, szClassName, &tReleaseDateTag));  
      
    /* Get attribute tags for Date Released on Releasestatus class */
    T4_ITKCALL(iRetCode, POM_attr_id_of_attr(T4_ATTR_DATE_RELEASED, T4_CLASS_RELEASE_STATUS, &tDateTag));

    /* Get current date */
    T4_ITKCALL(iRetCode, T4_get_date(&dReleaseDate));
     
    /* Set current date to Release status and item revision Objects */
    T4_ITKCALL(iRetCode, POM_set_attr_date(1,&tReleaseStatus, tDateTag, dReleaseDate));

    T4_ITKCALL(iRetCode, POM_set_attr_date(1,&tObjectTag,tReleaseDateTag, dReleaseDate));
       
    /* This portion of code gets the VLA length to the Business Object to which 
       status is to be attached. If Business Object already have some status attached
       then 'Position in the VLA to start setting values' argument to function
       'POM_set_attr_tags' will be increased by one.
    */
    T4_ITKCALL(iRetCode, POM_length_of_attr(tObjectTag, tReleaseStatusList, &iVLALength));

    if (iRetCode == ITK_ok)
    {
        iVLALength = iVLALength +1;
    }

    /* Set Release status tag to item revision Object  */
    T4_ITKCALL(iRetCode, POM_set_attr_tags(1, &tObjectTag, tReleaseStatusList, (iVLALength-1), 1, &tReleaseStatus));
    
    if (iRetCode != ITK_ok)
    {
        /*  Unlock the object.  */
        iRetCode = AOM_unlock(tObjectTag);
        TC_write_syslog("\nUnable to add the Release status %s on Business Object.\n", pszStatusName);
        iRetCode = UNABLE_TO_ADD_STATUS_TO_BUSINESS_OBJ;
        EMH_store_error_s1(EMH_severity_error,iRetCode,pszStatusName);  
    }
    else
    {
        /* Save instances*/
        T4_ITKCALL(iRetCode, POM_save_instances(1,&tReleaseStatus,FALSE));
        
        T4_ITKCALL(iRetCode, POM_save_instances(1,&tObjectTag,FALSE));   
               
        /* Returning Newly Created Release Status */
        *ptReleaseStatus = tReleaseStatus;
    }

    return iRetCode;
}

/**
 * This function cut the release status of an object. Function allows cutting of multiple status to input Business Object. 
 * Gets the attribute id for "release_status_list" for input object and then set the attribute values to NULL.
 * 
 * @note Return Error if unable to cut release status.
 *
 * @note Code gets the VLA length to the Business Object to which status is to be cut.
 *
 * @param[in]  tObject          Tag of object whose status needs to be cut.
 * @param[in]  piStatusCount    Number of status to be cut 
 * @param[out] pptStatusTagList Tag of status to be cut.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int T4_obj_cut_status 
(
    tag_t    tObject,
    int*     piStatusCount,
    tag_t**  pptStatusTagList
)
{
    int    iRetCode           = ITK_ok;
    int    iStatusCnt         = 0;
    tag_t  tAttrRelStatList   = NULLTAG;
    
    /* Initializing the Out Parameter to this function. */
    (*pptStatusTagList) = NULL;
    
    T4_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, piStatusCount, pptStatusTagList));

    iStatusCnt = (*piStatusCount);

	if(iStatusCnt > 0)
	{
		tag_t  tObjType           = NULLTAG;
		char   szObjClassName[TCTYPE_class_name_size_c+1] = {""};

		T4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tObjType));

		T4_ITKCALL(iRetCode, TCTYPE_ask_class_name(tObjType, szObjClassName));

		T4_ITKCALL(iRetCode, POM_attr_id_of_attr(T4_ATTR_RELEASE_STATUS_LIST, szObjClassName, &tAttrRelStatList));

		/*  Lock the object.  */
		T4_ITKCALL(iRetCode, AOM_refresh(tObject, true));

		for (int iDx = iStatusCnt-1; iDx >= 0 && iRetCode == ITK_ok; iDx--)
		{
			T4_ITKCALL(iRetCode, POM_remove_from_attr(1, &tObject, tAttrRelStatList, iDx, 1));

			/* Save instances*/
			T4_ITKCALL(iRetCode, POM_save_instances(1, &tObject, false));
		}

		T4_ITKCALL(iRetCode, AOM_refresh(tObject, false));
	}

    return iRetCode;
}

/**
 * This function attach the input status to the TC Business Object. 
 *
 * @param[in] tObject         Tag of Object
 * @param[in] iStatusCount    Count of status to be attached
 * @param[in] ptStatusTagList Tag of status to be attached
 *
 * @return ITK_ok is successful else ITK return code.
 */
int T4_obj_paste_status 
(
    tag_t   tObject,
    int     iStatusCount,
    tag_t*  ptStatusTagList
)
{
    int    iRetCode                                = ITK_ok;
    tag_t  tObjType                                = NULLTAG;                              
    tag_t  tRelStatList                            = NULLTAG;
    tag_t  tDate                                   = NULLTAG;
    tag_t  tReleaseDate                            = NULLTAG;
    char   szClassName[TCTYPE_class_name_size_c+1] = {""};
    date_t dReleaseDate                            = NULLDATE;

    T4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tObjType));
        
    T4_ITKCALL(iRetCode, TCTYPE_ask_class_name(tObjType, szClassName));

    /*  Lock the object.  */
    T4_ITKCALL(iRetCode, AOM_refresh(tObject, true));

    /* Get attribute tags for Release Status and Date Released on Item revision class  */
    T4_ITKCALL(iRetCode, POM_attr_id_of_attr(T4_ATTR_RELEASE_STATUS_LIST, szClassName, &tRelStatList)); 
        
    T4_ITKCALL(iRetCode, POM_attr_id_of_attr(T4_ATTR_DATE_RELEASED, szClassName, &tReleaseDate));

    /* Get current date */
    T4_ITKCALL(iRetCode, T4_get_date(&dReleaseDate));
     
    /* Set current date to Item revision Objects */
    T4_ITKCALL(iRetCode, POM_set_attr_date(1, &tObject, tReleaseDate, dReleaseDate));

    /* Set Release status tag to item revision Object  */
    T4_ITKCALL(iRetCode, POM_set_attr_tags(1, &tObject, tRelStatList, 0, iStatusCount, ptStatusTagList));

    /* Save instances*/
    T4_ITKCALL(iRetCode, POM_save_instances(1, &tObject, false));
   
    T4_ITKCALL(iRetCode, AOM_refresh(tObject, false));

    return iRetCode;
}

/**
 * This function gets the name of type for the input business
 * objet.
 *
 * e.g. Consider Item type 'Z9_ZSS'. If we pass tag to an instance of 'Z9_ZSS' this function will return string
 *      'Z9_ZSS' and not 'Item'. Consider Dataset of type 'catia'. If we pass tag to an instance of 'catia' this function will
 *      return 'catia' and not 'Dataset'.
 *
 * @param[in]  tObject       Business Object tag
 * @param[out] ppszObjType   Object type
 *
 * @return ITK_ok is successful else ITK return code.
 */
int T4_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
)
{
    int     iRetCode            = ITK_ok;
    tag_t   tObjectType         = NULLTAG;
    char    szObjectType[TCTYPE_name_size_c+1] = {""};

    /* Initializing the Out Parameter to this function. */
    (*ppszObjType) = NULL;
    
    T4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject,&tObjectType));
    
    T4_ITKCALL(iRetCode, TCTYPE_ask_name(tObjectType, szObjectType));
    
    if(iRetCode == ITK_ok)
    {
        (*ppszObjType) = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(szObjectType) +1)));
       
        tc_strcpy(*ppszObjType, szObjectType);
    }
    
    return iRetCode;
}


#ifdef __cplusplus
}
#endif


