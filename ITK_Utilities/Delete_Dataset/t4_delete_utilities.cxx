/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_delete_utilities.cxx

    Description:  This File contains custom functions for deleting TC objects. These functions are part of T4 library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Mar-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <t4_library.hxx>
#include <t4_const.hxx>
#include <t4_errors.hxx>

/**
 * Function deletes the dataset which is supplied to this function as input argument. Before deleting the dataset all the references
 * should be cut such as reference to Envelop, Folder, Status and other POM referenced. All Imanrelation are deleted which are associated
 * with dataset.
 *
 * @param[in] tDataset  Tag of dataset to be deleted
 *
 * @retval ITK_ok if successful else ITK return code
 */
int T4_delete_dataset
( 
    tag_t tDataset
)
{
    int     iRetCode              = ITK_ok;
    int     iStatusCount          = 0;
    int     iNamedRefCount        = 0;
    tag_t   tReferencedObject     = NULLTAG;
    tag_t*  ptStatuses            = NULL;
    tag_t*  ptReferencers         = NULL;
    char*   pszReferenceName      = NULL;       
    AE_reference_type_t  eReferenceType; 

    T4_ITKCALL(iRetCode, WSOM_ask_release_status_list(tDataset, &iStatusCount, &ptStatuses));

    for(int idx = 0; idx < iStatusCount && iRetCode == ITK_ok; idx++)
    {
        /* Modify the status attribute */
        T4_ITKCALL(iRetCode, T4_delete_modifyattribute(tDataset, T4_ATTR_RELEASE_STATUS_LIST,
                                                               ptStatuses[idx]));
    }

    /* Delete some junk like "relationships", etc. */
    T4_ITKCALL(iRetCode, T4_delete_ds_pomreferences(tDataset));

    /* Delete workflows and crap like that */
    T4_ITKCALL(iRetCode, T4_delete_wsom_references(tDataset));

    /* Lock the object against modification by another process. */
    T4_ITKCALL(iRetCode, AOM_lock_for_delete(tDataset));

    T4_ITKCALL(iRetCode, AE_ask_dataset_ref_count(tDataset, &iNamedRefCount));

    if(iNamedRefCount > 0)
    {
        /* Finds named references in datasets */
        T4_ITKCALL(iRetCode, AE_find_dataset_named_ref2(tDataset, 0, &pszReferenceName, &eReferenceType, 
                                                         &tReferencedObject ));
        /* Removing all named reference */
        T4_ITKCALL(iRetCode, AE_remove_dataset_named_ref2(tDataset, pszReferenceName));
    }

    /* Deletes all revisions of a dataset that are not referenced by some other object in the database */
    T4_ITKCALL(iRetCode, AE_delete_all_dataset_revs(tDataset));

    T4_MEM_TCFREE(pszReferenceName);
    T4_MEM_TCFREE(ptStatuses);
    T4_MEM_TCFREE(ptReferencers);
    return iRetCode;
}

/**
 * Function modify the given attribute on the Item Revision. This will remove attribute values of status objects
 * (release_status_list attribute) and the BVR's(structure_revisions attribute) on the Item Revision.
 *
 * @param[in] tObject      Tag of the Item Revision
 * @param[in] pszAttrName  Name of the attribute to update
 * @param[in] tValue       Tag of the value to be update
 *
 * @retval ITK_ok if successful else ITK return code
 */
int T4_delete_modifyattribute
( 
    tag_t  tObject,
    char*  pszAttrName,
    tag_t  tValue
)
{
    int     iRetCode           = ITK_ok;
    int     iValueIndex        = -1;
    tag_t   tInstClassId       = NULLTAG;
    tag_t   tAttrId            = NULLTAG;
    char*   pszClassName       = NULL;

    /* Finds out the class to which the instance belongs */
    T4_ITKCALL(iRetCode, POM_class_of_instance(tObject, &tInstClassId));

    /* Returns the name of a class which is specified by its ID */
    T4_ITKCALL(iRetCode, POM_name_of_class(tInstClassId, &pszClassName));

    /* Returns the attr_id for the specified attribute in the specified class. */
    T4_ITKCALL(iRetCode, POM_attr_id_of_attr(pszAttrName, pszClassName, &tAttrId));

    /* Finds the index of the first occurrence of the given value in an attribute. */
    T4_ITKCALL(iRetCode, POM_ask_index_of_tag(tObject, tAttrId, tValue, &iValueIndex));

    /* Locks an object against modification by another process. */
    T4_ITKCALL(iRetCode, AOM_refresh(tObject, true)); 

    /* Removes attribute value of the specified object. */
    T4_ITKCALL(iRetCode, POM_remove_from_attr(1, &tObject, tAttrId, iValueIndex, 1));

    /* Saves an application object to the database. */
    T4_ITKCALL(iRetCode, AOM_save(tObject));

    /* Unlocks an object for modification by another process. */
    T4_ITKCALL(iRetCode, AOM_refresh(tObject, false)); 
    
    T4_MEM_TCFREE(pszClassName);
    return iRetCode;
}

/**
 * Function deletes the POM reference relations of the Primary Object such as ImanRelation, and detach it from
 * the reference object. If input primary object is associated with any EPM task or Dispatcher request then the 
 * function cuts/remove the input object from such task or requests.
 *
 * @param[in] tPrimaryObject  Tag of Primary Object
 *
 * @retval ITK_ok if successful else ITK return code
 */
int T4_delete_ds_pomreferences
( 
    tag_t tPrimaryObject
)
{
    int    iRetCode                = ITK_ok;
    int    iNoOfInstances          = 0;
    int    iNoOfRefClasses         = 0;
    int    iLoadedObjCnt           = 0;
    int*   piInstwhereFound        = NULL;
    int*   piLevels                = NULL;
    int*   piClassLevels           = NULL;
    int*   piClassWhereFound       = NULL;
    tag_t* ptReferencers           = NULL;        
    tag_t* ptRefClasses            = NULL;  
    tag_t* ptLoadedObj             = NULL;
    
    /* Ask all the POM refeerences of the specified WSO */
    T4_ITKCALL(iRetCode, POM_referencers_of_instance(tPrimaryObject, 1, POM_in_db_only,
                                                       &iNoOfInstances, &ptReferencers, &piLevels, 
                                                       &piInstwhereFound, &iNoOfRefClasses, &ptRefClasses,
                                                       &piClassLevels, &piClassWhereFound));

    /* Instances are loaded because due to high latency some of related instances are loaded and while extracting 
        attribute value of these related object error is prompted to GUI.
    */
    if(iNoOfInstances > 0)
    {
        T4_ITKCALL(iRetCode, POM_load_instances_possible(iNoOfInstances, ptReferencers, &iLoadedObjCnt, &ptLoadedObj));
    }

    /* Removes the Item references */
    for(int iNx = 0; iNx < iNoOfInstances && iRetCode == ITK_ok; iNx++)
    {
        char* pszClassName = NULL;
        tag_t tClassId     = NULLTAG;

        T4_ITKCALL(iRetCode, POM_class_of_instance(ptReferencers[iNx], &tClassId));

        T4_ITKCALL(iRetCode, POM_name_of_class(tClassId, &pszClassName));

        /* match either ImanRelation or PSOccurrance reference */
        if(tc_strcmp(pszClassName, T4_CLASS_IMANRELATION )== 0 )
        {      
            tag_t tTempReference = NULLTAG;

            tTempReference = ptReferencers[iNx];
            /* It cuts from parent first and then delete */
            T4_ITKCALL(iRetCode, POM_delete_instances( 1, (const tag_t *)&tTempReference));          
        }

        if(tc_strcmp(pszClassName, T4_CLASS_EPM_TASK) == 0)
        {
            /* Remove Object from EPMTask reference attribute */
            T4_ITKCALL(iRetCode, T4_delete_modifyattribute(ptReferencers[iNx], T4_ATTR_EPM_TASK_ATTACHMENTS,
                                                                tPrimaryObject));
        }

        if(tc_strcmp(pszClassName, T4_CLASS_DISPATCHERREQUEST) == 0)
        {
            /* Remove Object from Dispatcher Request reference attribute */
            T4_ITKCALL(iRetCode, T4_delete_modifyattribute(ptReferencers[iNx], T4_ATTR_PRIMARY_OBJ, tPrimaryObject));                                              
        }

        T4_MEM_TCFREE(pszClassName );
    }
    
    T4_MEM_TCFREE(piInstwhereFound);
    T4_MEM_TCFREE(piLevels);  
    T4_MEM_TCFREE(piClassLevels);    
    T4_MEM_TCFREE(piClassWhereFound);
    T4_MEM_TCFREE(ptReferencers);
    T4_MEM_TCFREE(ptRefClasses); 
    T4_MEM_TCFREE(ptLoadedObj);
    return iRetCode;                                                
}

/**
 * This function removes the references such as Folder etc. by using 'WSOM_where_referenced' ITK function
 *
 * @param[in] tObject    Tag of the Object whose references are to be removed
 *
 * @retval ITK_ok if successful else ITK return code
 */
int T4_delete_wsom_references
( 
    tag_t tObject
)
{
    int    iRetCode         = ITK_ok;
    int    iReferencers     = 0;
    int*   piLevels         = NULL;
    tag_t* ptReferencers    = NULL;
    char** ppszRelations    = NULL;

    /* Finds all WorkspaceObjects that reference the specified WorkspaceObject. */
    T4_ITKCALL(iRetCode, WSOM_where_referenced(tObject, 1, &iReferencers, &piLevels,
                                                 &ptReferencers, &ppszRelations));

    T4_ITKCALL(iRetCode, AOM_refresh(tObject, true));

    /*  Removes the Item reference from a EPMTAsk */
    for(int iCount = 0; iCount < iReferencers && iRetCode == ITK_ok; iCount++)
    {
        char* pszTypeName    = NULL;

        T4_ITKCALL(iRetCode,  T4_obj_ask_type(ptReferencers[iCount], &pszTypeName));

        if(tc_strcmp(pszTypeName, T4_CLASS_FOLDER) == 0 || tc_strcmp(pszTypeName, T4_TYPE_NEWSTUFF_FOLDER) == 0 ||
            tc_strcmp(pszTypeName, T4_CLASS_ENVELOPE) == 0)
        {
            /* Removes a reference from a folder (specified by folder_tag) */
            T4_ITKCALL(iRetCode, FL_remove(ptReferencers[iCount], tObject));

            /* Saves reference object to the database. */
            T4_ITKCALL(iRetCode, AOM_save(ptReferencers[iCount]));

            /* Saves Item revision object to the database.*/
            T4_ITKCALL(iRetCode, AOM_save(tObject));
        }

        T4_MEM_TCFREE(pszTypeName);
    }  

    T4_ITKCALL(iRetCode, AOM_refresh(tObject, false)); 

    T4_MEM_TCFREE(piLevels);
    T4_MEM_TCFREE(ptReferencers);
    T4_MEM_TCFREE(ppszRelations);
    return iRetCode;
}


#ifdef __cplusplus
}
#endif


