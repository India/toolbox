/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_delete_dataset.hxx

    Description:  Header File containing declaration

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Mar-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_DELETE_DATASET_HXX
#define T4_DELETE_DATASET_HXX

#include <t4_library.hxx>
#include <t4_errors.hxx>


#define T4_ERROR_UNABLE_TO_PROCESS_DS(iRetCode, fLogFile, fRepeatFile, gszLine, pszDSObjStr){\
                                            char* pszErrorText = NOVALUE;\
                                            EMH_ask_error_text(iRetCode, &pszErrorText);\
                                            fprintf(fLogFile, "Error: Unable to process Dataset '%s' with UID %s.\n   %s.\n", pszDSObjStr, gszLine, pszErrorText);\
                                            fflush(fLogFile);\
                                            fprintf(fRepeatFile, "%s\n", gszLine);\
                                            fflush(fRepeatFile);\
                                            T4_MEM_TCFREE(pszErrorText);\
}



#define T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist){\
                                if((fRepeatFile = fopen(pszRepeatFile, "w")) == NULL ){\
                                    printf("\n** ERROR: Cannot create %s for writing unprocessed contents **\n\n",pszRepeatFile);}\
                                else{\
                                bRepeatFileExist = true;}\
}

#define T4_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, gszLine){\
                            char* pszErrorText = NOVALUE;\
                            EMH_ask_error_text(iRetCode, &pszErrorText);\
                            fprintf(fLogFile, "Error: %s.\n", pszErrorText);\
                            fflush(fLogFile);\
                            fprintf(fRepeatFile, "%s\n", gszLine);\
                            fflush(fRepeatFile);\
                            T4_MEM_TCFREE(pszErrorText);\
}

int T4_process_dataset_delete_req
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    char*   pszDSTypeInfo,
    logical bByPass,
    logical bNOModify
); 

#endif






