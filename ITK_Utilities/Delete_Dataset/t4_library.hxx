/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_library.hxx

    Description:  Header File containing declarations of function spread across all

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Mar-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_LIBRARY_HXX
#define T4_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <epm/epm.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/item_msg.h>
#include <tccore/releasestatus.h>
#include <tccore/aom.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <tccore/project.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/dataset_msg.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <sa/user.h>
#include <sa/role.h>
#include <sa/am.h>
#include <bom/bom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/pom/pom.h>
#include <pom/enq/enq.h>
#include <me/me.h>
#include <form/form.h>
#include <res/res_itk.h>
#include <epm/signoff.h>
#include <fclasses/tc_date.h>
#include <dispatcher/dispatcher_itk.h>
#include <tccore/uom.h>
#include <tc/tc_util.h>
#include <tcinit/tcinit.h>
#include <conio.h>
#include <tccore/license.h>
#include <t4_const.hxx>
#include <t4_itk_macro.hxx>
#include <t4_itk_mem_free.hxx>


/*        Utility Functions For Error Logging        */
int T4_err_write_to_logfile
(
    char*       pszErrorFunction,
    int         iErrorNumber,
    char*       pszFileName,
    int         iLineNumber
);

int T4_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
);


/*        Utility Functions For Objects        */
int T4_obj_set_tag_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    tag_t       tPropVal
);

int T4_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
);

int T4_obj_set_ds_str_attr_without_version_change
(
    tag_t       tDataset,
    char*       pszPropertyName,
    char*       pszPropVal
);

int T4_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
);

int T4_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
);

int T4_obj_create_and_set_status 
(
    char*  pszStatusName, 
    tag_t  tObjectTag,
    tag_t* ptReleaseStatus
);

int T4_obj_cut_status 
(
    tag_t    tObject,
    int*     piStatusCount,
    tag_t**  pptStatusTagList
);

int T4_obj_paste_status 
(
    tag_t   tObject,
    int     iStatusCount,
    tag_t*  ptStatusTagList
);

int T4_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
);

/*        Utility Functions For GRM        */
int T4_grm_get_all_secondary_obj_of_type
(
    tag_t   tPrimaryObj,
    char*   pszSecType,
    tag_t** pptSecObjects,
    int*    piObjectCount  
);

int T4_grm_get_secondary_obj_of_type
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    char*   pszSecondaryObjType,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
);


/*        Utility Functions For Date & Time        */
int T4_get_date
(
    date_t *dReleaseDate
);

int T4_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
);

/*        String Utility Functions        */
int T4_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
);

char* T4_trim( char* pszString );

void T4_trim_end(char *pszString);

int T4_str_check_digital_alpha_type
(
    char*    pszString,
    char**   ppszDigitalPart,
    char**   ppszAlphaPart,
    logical* pbIsDigital,
    logical* pbIsAlpha,
    logical* pbIsDigitalAlpha,
    logical* pbIsAlphaDigital,
    logical* pbWrongStr
);

void T4_get_str_after_delimiter_from_end
(
    const char*   pszInputStr,
    const char    szDelim, 
    char**        ppszOutputStr
);

/*        Utility Functions For Deleting TC Object        */
int T4_delete_dataset
( 
    tag_t tDataset
);

int T4_delete_modifyattribute
( 
    tag_t  tObject,
    char*  pszAttrName,
    tag_t  tValue
);

int T4_delete_ds_pomreferences
( 
    tag_t tPrimaryObject
);

int T4_delete_wsom_references
( 
    tag_t tObject
);

#endif





