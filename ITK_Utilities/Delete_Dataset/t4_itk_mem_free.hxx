/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_itk_mem_free.hxx

    Description:  This file contains macros for freeing portion of memory allocated with TCe 'MEM_' functions and by TCe itself.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Mar-14    Tarun Kumar Singh    Initial Release
   
========================================================================================================================================================================*/
#ifndef T4_ITK_MEM_FREE_HXX
#define T4_ITK_MEM_FREE_HXX

#include <t4_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef T4_MEM_TCFREE
#undef T4_MEM_TCFREE
#endif

// Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc

#define T4_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }

/*  Macro to free memory for user defined data type. */
#ifdef T4_FREE_IR_STATUS_STRUCT
#undef T4_FREE_IR_STATUS_STRUCT
#endif

/* This macro is used to free the memory of structure. */
#define T4_FREE_IR_STATUS_STRUCT(sIRStatusList,iCount) {\
          for(int iDx = 0; iDx < iCount; iDx++) {\
               T4_MEM_TCFREE(sIRStatusList[iDx].ptIRStatus);\
          }\
          T4_MEM_TCFREE(sIRStatusList);\
        }


#endif










