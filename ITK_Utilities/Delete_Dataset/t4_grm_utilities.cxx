/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_grm_utilities.cxx

    Description: This file contains function performing various operations on Teamcenter Business Objects

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Mar-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <t4_errors.hxx>
#include <t4_library.hxx>

	
/**
 * This function returns a list and count of all secondary objects with a specified
 * relation for specified secondary object class to the specified primary_object.
 *
 * e.g. Suppose 'Text' is a type of 'Dataset'. Then object type for 'Text' is 'Text'
 *
 * @note This function returns the value deplending on Secondary Object Type
 * 
 * @param[in]  tPrimaryObj   The object.
 * @param[in]  pszSecType    Type of the primary object.
 * @param[out] pptSecObjects List of Primary object of specified type.
 * @param[out] piObjectCount Primary Object count.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int T4_grm_get_all_secondary_obj_of_type
(
    tag_t   tPrimaryObj,
    char*   pszSecType,
    tag_t** pptSecObjects,
    int*    piObjectCount  
)
{
    int     iRetCode              = ITK_ok;
    int     iCount                = 0;
    int     iIterator             = 0;
    int     iSecCount             = 0;
    GRM_relation_t *ptSecObjects  = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount) = 0;
    (*pptSecObjects) = NULL;
    
    T4_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, NULLTAG, 
                                                       &iSecCount, &ptSecObjects));
                                       
    for(iIterator = 0; iIterator < iSecCount && iRetCode == ITK_ok; iIterator++)
    {      
        logical  bIsTypeOf = false;

        T4_ITKCALL(iRetCode, T4_obj_is_type_of(ptSecObjects[iIterator].secondary, pszSecType, &bIsTypeOf));
            
        if (bIsTypeOf == true)
        {
            iCount++;
            (*pptSecObjects) = (tag_t*)MEM_realloc ( (*pptSecObjects) , (sizeof(tag_t) * iCount) );
            (*pptSecObjects)[iCount -1] = ptSecObjects[iIterator].secondary;
        }
    }

	(*piObjectCount ) = iCount;

    T4_MEM_TCFREE(ptSecObjects);
    return iRetCode;
}

/**
 * This function returns a list and count of all secondary objects with a specified
 * relation for specified secondary object Type to the specified primary_object.
 *
 * e.g. Suppose 'Part' is a type of 'Item'. Then object type for 'Part' item is 'Part'
 *
 * @note This function returns the value deplending on Secondary Object Type
 * 
 * @param[in] tPrimaryObj              The object.
 * @param[in] pszRelationTypeName      Name of relation type. Can be NULL.
 * @param[in] pszSecondaryObjType      Type of the primary object.
 * @param[out] pptSecondaryObjects     List of Primary object of specified type.
 * @param[out] piObjectCount           Primary Object count.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int T4_grm_get_secondary_obj_of_type
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    char*   pszSecondaryObjType,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
)
{
    int     iRetCode                        = ITK_ok;
    int     iCount                          = 0;
    int     iIterator                       = 0;
    int     iSecondaryCount                 = 0;
    tag_t   tRelationType                   = NULLTAG;
    GRM_relation_t *ptSecondaryObjects      = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount) = 0;
    (*pptSecondaryObjects) = NULL;

    /* get the relation type tag e.g. "IMAN_specification"  */
    T4_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    
    T4_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, tRelationType, 
                                                       &iSecondaryCount, &ptSecondaryObjects));
                                       
    for(iIterator = 0; iIterator < iSecondaryCount && iRetCode == ITK_ok; iIterator++)
    {
        logical  bIsTypeOf = false;

        T4_ITKCALL(iRetCode, T4_obj_is_type_of(ptSecondaryObjects[iIterator].secondary, pszSecondaryObjType, &bIsTypeOf));
            
        if (bIsTypeOf == true)
        {
            iCount++;
            (*pptSecondaryObjects) = (tag_t*)MEM_realloc ( (*pptSecondaryObjects) , (sizeof(tag_t) * iCount) );
            (*pptSecondaryObjects)[iCount -1] = ptSecondaryObjects[iIterator].secondary;
        }
    }

	(*piObjectCount ) = iCount;

    T4_MEM_TCFREE(ptSecondaryObjects);
    return iRetCode;
}

#ifdef __cplusplus
}
#endif


