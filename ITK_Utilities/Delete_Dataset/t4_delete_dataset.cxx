/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_delete_dataset.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Mar-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <t4_errors.hxx>
#include <t4_library.hxx>
#include <t4_delete_dataset.hxx>

/**
 * This utility reads that input file that has the following format: OldItemID~NewItemID~ItemType~ObjectName. As a default separator "~" is used 
 * (if not specified). 'NewItemID' should not exist in the system. Last attribute ObjectName is optional
 *
 * @note: 'NewItemID' for the specified type should not exist in the system.
 *
 * @param[in] pszConfigFileInfo  Name of the Configuration File to be processed
 * @param[in] pszLogFile         Name of Log file
 * @param[in] pszRepeatFile      Name of the Repeat File
 * @param[in] pszSeparator       Separator used in Configuration File 
 * @param[in] bByPass            'true' if By Pass is set else 'false'
 * @param[in] bNOModify          If 'true' then last modification date and user of the objects will not to be changed
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_process_dataset_delete_req
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    char*   pszDSTypeInfo,
    logical bByPass,
    logical bNOModify
)
{
    int     iRetCode               = ITK_ok;
    char    gszLine[MAX_CHARS]     = {'\0'};
    logical bRepeatFileExist       = false;
    FILE*   fCfgFile;
    FILE*   fLogFile;
    FILE*   fRepeatFile;
   
    if ((fCfgFile = fopen(pszConfigFileInfo, "r")) == NULL )
    {
        printf("\n** ERROR: Cannot open %s for reading - exiting **\n\n", pszConfigFileInfo);
        return 0;
    }

    if ((fLogFile = fopen(pszLogFile, "w")) == NULL )
    {
        printf("\n** ERROR: Cannot create %s for writing log  **\n\n",pszLogFile);
    }
    else
    {
        if(bByPass == true || bNOModify == true)
        {
            if(bNOModify == true)
            {
                /*-------------------------------------*/
                /* Do not update date fields on change */
                /*-------------------------------------*/
                T4_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_attr_update, false, 0,0,0, NULL));
        
                /*----------------------*/
                /* Bypass access checks */
                /*----------------------*/
                T4_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_access_check, true, 0,0,0, NULL));
            }

            T4_ITKCALL(iRetCode, ITK_set_bypass(true));
        }

        while (fgets(gszLine, MAX_CHARS, fCfgFile) != NULL)
        {
            if ((gszLine[0] != '#') && (gszLine[0] != '\0') && (gszLine[0] != '\n'))
            {
                tag_t tDataset = NULLTAG;

                /*================================================*/
                /* Trim the line to remove leading and trailing   */
                /* spaces and discard the \n on the end.          */
                /*================================================*/
                tc_strcpy(gszLine, T4_trim(gszLine));

                T4_ITKCALL(iRetCode, POM_string_to_tag(gszLine, &tDataset));

                if(iRetCode != ITK_ok)
                {
                    /* Report Error */
                    if(bRepeatFileExist == false)
                    {
                        T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                    }

                    T4_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, gszLine); 
                }
                else
                {
                    int    iDSTypeCnt    = 0;
                    char** ppszDSTypes   = NULL;

                    if((pszDSTypeInfo != NULL) || (tc_strlen(pszDSTypeInfo) != 0))
                    {
                        T4_ITKCALL(iRetCode, T4_arg_parse_multipleVal(pszDSTypeInfo, pszSeparator[0], &iDSTypeCnt, &ppszDSTypes));
                    }

                    if(iRetCode == ITK_ok)
                    {
                        logical bProcessDS  = false;

                        if(iDSTypeCnt > 0)
                        {
                            char* pszType = NULL;

                            T4_ITKCALL(iRetCode, T4_obj_ask_type(tDataset, &pszType));

                            for(int iHx = 0; iHx < iDSTypeCnt && iRetCode == ITK_ok; iHx++)
                            {
                                if(tc_strcmp(pszType, ppszDSTypes[iHx]) == 0)
                                {
                                    bProcessDS = true;
                                }
                            }

                            T4_MEM_TCFREE(pszType);
                        }
                        else
                        {
                            T4_ITKCALL(iRetCode, T4_obj_is_type_of(tDataset, T4_CLASS_DATASET, &bProcessDS));
                        }

                        if(bProcessDS == true)
                        {
                            int  iMarkPoint = 0;

                            /*
                             * Define a markpoint for database transaction.
                             */
                            T4_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));

                            T4_ITKCALL(iRetCode, T4_delete_dataset(tDataset));

                            if(iRetCode == ITK_ok)
                            {
                                fprintf(fLogFile, "Success: Dataset with UID '%s' has been successfully deleted.\n", gszLine);
                                fflush(fLogFile);
                            }
                            else
                            {
                                char*   pszAttrVal    = NULL;
                                logical bStateChanged = false;

                                /* Report Error */
                                if(bRepeatFileExist == false)
                                {
                                    T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                }
                                
                                AOM_ask_value_string(tDataset, T4_ATTR_OBJECT_STRING_ATTRIBUTE, &pszAttrVal);

                                T4_ERROR_UNABLE_TO_PROCESS_DS(iRetCode, fLogFile, fRepeatFile, gszLine, pszAttrVal);

	                            /* Roll Back API is deliberately placed in the  end so that even if error occurred during roll back the 
                                   actual error occurred during deletion can still be reported properly otherwise error stack will contain
                                   latest error which will not help to debug the error. 
                                */
                                /* Do the rollback of the database. */

                                POM_roll_to_markpoint(iMarkPoint, &bStateChanged);

                                T4_MEM_TCFREE(pszAttrVal);
                            }
                        }

                        if(bProcessDS == false || iRetCode != ITK_ok)
                        {
                            if(bProcessDS == false)
                            {
                                /* Report Error */
                                fprintf(fLogFile, "Error: Input UID '%s' does not match with a valid dataset type \n", gszLine); 
                                fflush(fLogFile);
                            }

                            /* Generating repeat file */
                            if(bRepeatFileExist == false)
                            {
                                T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                            }

                            if(iRetCode != ITK_ok)
                            {
                                T4_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, gszLine); 
                            }
                            else
                            {
                                fprintf(fRepeatFile, "%s\n", gszLine); 
                                fflush(fRepeatFile);
                            }
                        }
                    }
                    else
                    {
                        /* Report Error */
                        fprintf(fLogFile, "Error: Cannot Parse input Dataset Types \n \t'%s'. Separator used is %s.\n", pszDSTypeInfo, pszSeparator); 
                        fflush(fLogFile); 

                        /* Generating repeat file */
                        if(bRepeatFileExist == false)
                        {
                            T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                        }

                        fprintf(fRepeatFile, "%s\n", gszLine); 
                        fflush(fRepeatFile);
                    }

                    T4_MEM_TCFREE(ppszDSTypes);
                }

                iRetCode = ITK_ok;
            }
        } 

        if(bByPass == true)
        {
            ITK_set_bypass(false);
        }
    }

    if(bRepeatFileExist == true)
    {
        fclose( fRepeatFile );
    }

    fclose( fCfgFile );
    fclose( fLogFile );
    return iRetCode;
}

#ifdef __cplusplus
}
#endif


