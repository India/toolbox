/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_datetime_utilities.cxx

    Description: This file contains function performing various operations on Teamcenter Business Objects

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Mar-14     Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <t4_library.hxx>
#include <t4_errors.hxx>

/**
 * Function gets current date in TC required Format. Get the current system date
 * and set it to TC Date structure.
 *
 * @param[out] dReleaseDate  Constructed Current date.
 *    
 * @return ITK_ok if function succedded.
 */
int T4_get_date(date_t *dReleaseDate)
{
    time_t  run_time;
    struct  tm* sConvTime =  NULL;

    /* Get time thru system calls. */
    run_time = time(NULL);
    sConvTime = localtime(&run_time);

    /* Convert the time into TC format */
    dReleaseDate->year   = 1900+ sConvTime->tm_year;
    dReleaseDate->month  = sConvTime->tm_mon;
    dReleaseDate->day    = sConvTime->tm_mday;
    dReleaseDate->hour   = sConvTime->tm_hour;
    dReleaseDate->minute = sConvTime->tm_min;
    dReleaseDate->second = sConvTime->tm_sec;

    return 0;
}


/**
 * This function gets the current date time.
 *
 * @param[in]  pszFormat   Expected date time format.
 * @param[out] szDateTime  current Date time in required format.
 *
 * @note 'szDateTime' must be a character array of at least [IMF_filename_size_c + 1] 
 *       characters 
 *
 * @retval ITK_ok on successful execution
 * @retval INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME if format resulted in
 *         resultant String length greater then IMF_filename_size_c + 1
 */
int T4_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
)
{
    int         iRetCode       = ITK_ok;
    int         iStrLength     = 0;
    struct tm   *clLocalTime   = NULL;
    time_t      tCurrentTime;

    /*Get system's local time*/
    tCurrentTime = time(NULL);
    clLocalTime  = localtime(&tCurrentTime);
        
    /*Get system time  in required format*/
    iStrLength =(int)( strftime( szDateTime, (IMF_filename_size_c + 1), pszFormat, clLocalTime));

    return iRetCode;
}


#ifdef __cplusplus
}
#endif


