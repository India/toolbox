/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_object_utilities.cxx

    Description: This file contains function performing various operations on Teamcenter Business Objects

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Mar-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <t4_errors.hxx>
#include <t4_library.hxx>

/**
 * This function returns the substring when provided with start and end position on the 
 * the input string. Substring is the character sequence that starts at character position 
 * 'iStrStartPosition' and ends at character position 'iStrEndPosition'
 *
 * For example If 'pszInputString' = 'Application'
 *                'iStrStartPosition' = 4
 *                'iStrEndPosition'   = 9
 *             then Output Will be: 'licati'
 *
 * Function will copy the characters starting 4th character to the 9th character of
 * input string.
 * 
 * @param[in]  pszInputString    Actual input string
 * @param[in]  iStrStartPosition Position of a character in the current string to be
 *                               used as starting character for the substring.
 * @param[in]  iStrEndPosition   Position of a character in the current string to be
 *                               used as last character for the substring.
 *
 * @param[out] ppszCopyedStr     Copied string.
 *
 * @retval INVALID_INPUT_TO_FUNCTION if input not correct
 * @retval ITK_ok on successful execution
 */
int T4_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
)
{
    int  iRetCode    = ITK_ok;
    int  iStrLen     = 0;

    /* Initializing the Out Parameter to this function. */
    (*ppszCopyedStr) = NULL;

    iStrLen = (int) tc_strlen(pszInputString);
    
    if ((pszInputString != NULL) && (iStrStartPosition >= 0) && (iStrEndPosition <=  iStrLen) && 
        (iStrStartPosition <= iStrEndPosition))
    {
        if(tc_strcmp(pszInputString, "") != 0)
        {
            int idx        = 0;
            int iNewStrLen = 0;

            iNewStrLen = iStrEndPosition - iStrStartPosition + 2;

            (*ppszCopyedStr) = (char*) MEM_alloc(sizeof(char*) * iNewStrLen);

            while(iStrStartPosition <= iStrEndPosition)
            {
                (*ppszCopyedStr)[idx] = pszInputString[iStrStartPosition-1];
                idx++;
                iStrStartPosition++;
            }

            (*ppszCopyedStr)[idx] = '\0';
        }
    }

    return iRetCode;
}

/**
 * Function to Strips leading and trailing space. 
 *
 * @param[in]  pszString  String to trim white spaces 
 * @param[out] Pointer to Trimmed string 
 */
char* T4_trim( char* pszString )
{
  if (pszString == NULL)
    return(NULL);

  T4_trim_end(pszString);

    /*===================================================================*/
    /* Skip over spaces, and return the address of the first non-space   */
    /* character. If there are no leading spaces, this just returns str. */
    /*===================================================================*/

  return &pszString[strspn( pszString, " \t" )];

} 

/**
 * Function to Strips all trailing spaces from the given null-terminated string
 *
 * @param[in/out] pszString  String to trim white spaces
 *
 */

void T4_trim_end(char *pszString)
{
  int iLen = (int)(tc_strlen( pszString ) - 1);

  while (iLen >= 0  &&  ((pszString[iLen] == ' ') || 
          (pszString[iLen] == '\t') || (pszString[iLen] == '\n')))
      --iLen;

  pszString[iLen + 1] = '\0';

}

/**
 * This function provide the logical confirmation that if the input string is one
 * of the following type:
 *    1. Digital(e.g. "1234")
 *    2. Alphabetical (e.g. "ABCDG")
 *    3. Digital Alpha (e.g. "1234ABCDG")
 *    4. Alpha Digital (e.g. "ABCDG1234")
 *    5. Wrong String (e.g. "ABCD*G-1234")
 * @note: If the input string is 'Digital Alpha' or 'Alpha Digital' function will return two
 *        stings with Alpha and digital part. Consider example 3 and 4 the output will be
 *        Alpha Part "ABCDG" and Digital Part "1234".
 *
 * @param[in]  pszString        Input String
 * @param[out] ppszDigitalPart  Digital Part of the input string
 * @param[out] ppszAlphaPart    Alphabetical Part of the input string
 * @param[out] pbIsDigital      'TRUE' If Input String is Digital Else 'FALSE'
 * @param[out] pbIsAlpha        'TRUE' If Input String is Alphabetical Else 'FALSE'
 * @param[out] pbIsDigitalAlpha 'TRUE' If Input String is Digital Alpha Else 'FALSE'
 * @param[out] pbIsAlphaDigital 'TRUE' If Input String is Alpha Digital Else 'FALSE'
 * @param[out] pbWrongStr       'TRUE' If Input String is incorrect Else 'FALSE'
 *
 * @retval ITK_ok on successful execution
 */
int T4_str_check_digital_alpha_type
(
    char*    pszString,
    char**   ppszDigitalPart,
    char**   ppszAlphaPart,
    logical* pbIsDigital,
    logical* pbIsAlpha,
    logical* pbIsDigitalAlpha,
    logical* pbIsAlphaDigital,
    logical* pbWrongStr
)
{
    int     iRetCode      = ITK_ok;
    int     iStrLen       = 0;
    int     iFlipIndex    = 0;
    logical bIsFirstFlip  = false;

    /* Initialise the output array allocating memory*/
    (*pbIsDigital) = false;
    (*pbIsAlpha)   = false;
    (*pbWrongStr)  = false;
    (*pbIsDigitalAlpha) = false;
    (*pbIsAlphaDigital) = false;
    (*ppszDigitalPart) = NULL;
    (*ppszAlphaPart)   = NULL;

    if (pszString != NULL)
    {   
        if(tc_strcmp(pszString, "") != 0)
        {
            iStrLen = (int) tc_strlen(pszString);

            for(int idx = 0; idx < iStrLen && (*pbWrongStr) == false; idx++)
            {
                if(isalnum(pszString[idx]))
                {
                    if(isdigit(pszString[idx]))
                    {
                        if((*pbIsAlpha) == false && bIsFirstFlip == false)
                        {
                            (*pbIsDigital) = true;
                        }
                        else
                        {
                            if(bIsFirstFlip == false)
                            {
                                iFlipIndex = idx;
                                bIsFirstFlip = true;                            
                                (*pbIsAlphaDigital) = true;
                            }
                            else
                            {
                                if((*pbIsDigitalAlpha) == false)
                                {
                                    /* Condition just to Pass */
                                }
                                else
                                {
                                    (*pbWrongStr)       = true;
                                    /* Resetting Logical out parameters. */
                                    (*pbIsDigital)      = false;
                                    (*pbIsAlpha)        = false;
                                    (*pbIsDigitalAlpha) = false;
                                    (*pbIsAlphaDigital) = false;
                                }
                            }
                        }
                    }
                    
                    if(isalpha(pszString[idx]))
                    {
                        if((*pbIsDigital) == false  && bIsFirstFlip == false)
                        {
                            (*pbIsAlpha) = true;
                        }
                        else
                        {
                            if(bIsFirstFlip == false)
                            {
                                iFlipIndex = idx;
                                bIsFirstFlip = true;                            
                                (*pbIsDigitalAlpha) = true;
                            }
                            else
                            {
                                if((*pbIsAlphaDigital) == false)
                                {
                                    /* Condition just to Pass */
                                }
                                else
                                {
                                    (*pbWrongStr)       = true;
                                    /* Resetting Logical out parameters. */
                                    (*pbIsDigital)      = false;
                                    (*pbIsAlpha)        = false;
                                    (*pbIsDigitalAlpha) = false;
                                    (*pbIsAlphaDigital) = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    (*pbWrongStr)       = true;
                    /* Resetting Logical out parameters. */
                    (*pbIsDigital)      = false;
                    (*pbIsAlpha)        = false;
                    (*pbIsDigitalAlpha) = false;
                    (*pbIsAlphaDigital) = false;
                }
            }

            if((*pbWrongStr) == false && ((*pbIsDigitalAlpha) == true || (*pbIsAlphaDigital)== true ))
            {
                (*pbIsDigital) = false;
                (*pbIsAlpha) = false;

                if((*pbIsDigitalAlpha) == true)
                {
                    (*ppszDigitalPart) = (char*) MEM_alloc(sizeof(char) * (iFlipIndex +1));
                    T4_ITKCALL(iRetCode, T4_str_copy_substring(pszString, 1, iFlipIndex, ppszDigitalPart));

                    (*ppszAlphaPart)   = (char*) MEM_alloc(sizeof(char) * (iStrLen - iFlipIndex +1));
                    T4_ITKCALL(iRetCode, T4_str_copy_substring(pszString, (iFlipIndex +1), iStrLen, ppszAlphaPart));

                }
                else
                {
                    (*ppszAlphaPart)   = (char*) MEM_alloc(sizeof(char) * (iFlipIndex +1));
                    T4_ITKCALL(iRetCode, T4_str_copy_substring(pszString, 1, iFlipIndex, ppszAlphaPart));

                    (*ppszDigitalPart) = (char*) MEM_alloc(sizeof(char) * (iStrLen - iFlipIndex +1));    
                    T4_ITKCALL(iRetCode, T4_str_copy_substring(pszString, (iFlipIndex +1), iStrLen, ppszDigitalPart));
                }
            }
        }
    }
    else
    {
        *pbWrongStr = true;
    }

    return iRetCode;
}

/**
 * Function split the input string based on very last occurrence of input Delimiter character. This function will
 * return the string with the Delimiter. For e.g. 
 * If input string is "ENT_01-Test_Data-1" and Delimiter character is '-' then the output string will be "-1"
 *
 * @param[in]  pszInputStr    String to be checked for Delimiter occurrence
 * @param[in]  szDelim        Delimiter
 * @param[out] ppszOutputStr  String array of tokens
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
void T4_get_str_after_delimiter_from_end
(
    const char*   pszInputStr,
    const char    szDelim, 
    char**        ppszOutputStr
)
{
    char* pszTemp = NULL;
    
     
 /*   pszTemp = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(pszInputStr)+1)));
    tc_strcpy(pszTemp, pszInputStr);*/

    pszTemp = STRNG_find_last_char(pszInputStr, szDelim);
   
	if(pszTemp != NULL)
	{
		(*ppszOutputStr) = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(pszTemp) +1)));

		tc_strcpy((*ppszOutputStr), pszTemp);
	}

    /* Do not free this pointer as it is an alias for the memory containing information of 'pszInputStr' variable */
    pszTemp = NULL;

}

#ifdef __cplusplus
}
#endif


