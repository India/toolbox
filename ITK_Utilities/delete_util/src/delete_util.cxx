#include <iman.h>
#include <iman_string.h>
#include <pom.h>
#include <emh.h>
#include <bom.h>
#include <qry.h>
#include <aom.h>
#include <aom_prop.h>
#include <pie.h>
#include <epm.h>
#include <item.h>
#include<tcinit/tcinit.h>
#include <cfm.h>
#include <tc/tc_startup.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/revisionanchor.h>
#include <ss/ss_const.h>
#include <sa/tcfile.h>
#include <property/prop.h>
#include <property/prop_msg.h>
#include <tccore/tctype.h>
#include <property/prop_errors.h>
#include <property/propdesc.h>
#include <sa/audit.h>
#include <sa/auditmgr.h>
#include <ps.h>
#include <tccore/grm.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <stdexcept>
#include <sstream>  
#include <stdexcept>  
#include <list>  
#include <sys/types.h>  
#include <sys/stat.h>   
#include <errno.h>
#include <time.h>
#include <enq.h>
#include <res_itk.h>
#include <am.h>
#include <windows.h>
#include <process.h>
#include <folder.h>
//--------------------------------
#include <stdio.h>
#include<conio.h>
#include <stdlib.h>
#include <tc/tc_startup.h>
#include <aom_prop.h>
#include <tctype.h>
#include <ss_errors.h>
#include <tctype.h>
#include <ss_errors.h>
//--------------------------------
//--------------------------------

#define BUFSIZE 256


#ifdef __cplusplus
extern "C"
{
#endif

#define BUFSIZE 256

static logical nodryrun = false;

using namespace std;

extern void AM__set_application_bypass (logical bAllowBypass );

int PNG_delete_Datasets(tag_t dstTag );
int PNG_delete_attached_datasets( tag_t rev_tag );
int PNG_delete_specification_datasets( tag_t rev_tag );
int PNG_cut_or_delete_ds_attached_with_rel_type( tag_t rev_tag, int iRelTypeCnt, char** ppszRelTypeList);
int PNG_obj_is_type_of(tag_t tObject, char*  pszTypeName, logical* pbBelongsToInputClass);
int PNG_grm_get_secondary_obj( tag_t  tPrimaryObj, int iRelTypeCnt, char**  ppszRelTypeNames, tag_t** pptSecondaryObjects, int* piObjectCount);

void execute_saved_query(  );
void  PNG_get_string_tokens(const char* pszInputStr, char* pszDelim, int* iTokenCnt, char*** pppszAllTokens);

static int PNG_read_item_ids_from_file( FILE* inputFile, FILE* logFile, int* item_cnt, char*** item_ids	); // Modified by Sadok
int is_exists_in_vector( vector<string> vec , char* str );
string& trim( string &str );
int PNG_updateAttribute( tag_t objTag, char *attrname, tag_t value );
int PNG_where_ask_referencers ( tag_t   objectTag, int* referencerCount, tag_t** referencerList );
int PNG_delete_item_master( tag_t item );
int PNG_remove_from_all_folders(tag_t object);
int PNG_remove_from_all_tasks(tag_t object);
int PNG_delete_rev_master( tag_t rev_tag );
int PNG_delete_item_revision( tag_t revtag );
int PNG_delete_obj_relations( tag_t obj );
int PNG_delete_free_ds_forms( char* item_id, char* rev_id, tag_t item_tag );
//======================================================================================
void PNG_fprintf ( FILE* fOutput, const char* formatString, ... );
void PNG_util_va_fprintf ( FILE* fOutput, const char* formatString,	va_list vaArgs );
void PNG_util_printf ( const char* formatString, ... );
char* PNG_util_string_copy ( const char* inputString );
char* PNG_util_string_append ( char* str, const char* appendage );
char* PNG_util_string_append_realloc ( char** str, const char* appendage );
char* PNG_trim(char *pc_string);
char* PNG_trim_leading(char *pc_string);
char* PNG_trim_trailing(char *pc_string);
static int PNG_is_owning_site( tag_t objTag, logical* answer );
static int PNG_print_usage();
//=======================================================================================
int PNG_ideas_object_process_cleanup( 	char* itemsFileName, char* logFileName, char* errorFileName , int iRelTypeCnt, char** ppszRelTypeList);
int PNG_tc_login();
//=======================================================================================
#define SM_FREE( x ) { if( x != NULL ) { MEM_free( x ); } }

#define STR_END    '\x0'

#define ITK_CALL( x ) {           \
    int stat;                     \
    char *err_string;             \
	FILE* errorlog;               \
    if( ( stat = ( x ) ) != ITK_ok )   \
    {                             \
    EMH_get_error_string ( NULLTAG , stat , &err_string );                 \
    errorlog = fopen( "c:/Temp/consumption_Report_error_log.txt" , "w" );  \
    fprintf ( errorlog , "ERROR: %d ERROR MSG: %s.\n" , stat , err_string ); \
	fprintf ( errorlog , "FUNCTION: %s\nFILE: %s LINE: %d\n" , #x , __FILE__, __LINE__ ); \
	fclose( errorlog ); \
    if( err_string ) MEM_free( err_string );                                \
    }                                                                    \
}

#define PNG_cust_strcpy(pszDestiStr, pszSourceStr){\
            pszDestiStr = (char*)MEM_alloc((int)(sizeof(char)* ( tc_strlen(pszSourceStr) + 1 )));\
            tc_strcpy(pszDestiStr, pszSourceStr);\
}

#define PNG_update_string_array(array_len,  array_strings,  value){\
        (*array_len)++;\
        *array_strings = (char**) MEM_realloc(*array_strings, (int)((*array_len) * sizeof(char*)));\
        (*array_strings)[*array_len - 1] = (char*) MEM_alloc((int)((strlen(value) + 1)* sizeof(char)));\
        tc_strcpy((*array_strings)[*array_len - 1], value);\
}

#define ENTER(params) PNG_fprintf(stderr, "--> %s()\n", params)
#define LEAVE(params) PNG_fprintf(stderr, "<-- %s()\n", params)
//=======================================================================================

extern int ITK_user_main(int argc,char *argv[])
{
    int retcode = ITK_ok;

    const char*  theFunction = "ITK_user_main";
    ENTER(theFunction);

	getc(stdin);

	if (ITK_ask_cli_argument("-h") != NULL) 
	{
		PNG_print_usage();
	}
	else
	{
		retcode = PNG_tc_login();

		if(retcode == ITK_ok) 
		{
			char*  items_file_name   = ITK_ask_cli_argument("-input_file=");
			char*  log_file_name     = ITK_ask_cli_argument("-log_file=");
            char*  pszRelTypesString = ITK_ask_cli_argument("-relation_types=");
			char*  error_file_name   = NULL;
            int    iRelTypeCnt       = 0;
            char** ppszRelTypeList   = NULL;

			fprintf(stderr, "Successfully logged in to Teamcenter...\n" );
			fprintf(stderr, "Memory consumption : %d\n" , TC_ask_memory_in_use() );	
			
			if (ITK_ask_cli_argument("-nodryrun") != NULL) 
			{
				nodryrun = true;
			} 
			else 
			{
				nodryrun = false;
			}
			PNG_util_printf("%s: Simulation is [%s]\n", theFunction, (nodryrun == true ? "OFF" : "ON"));


            if(pszRelTypesString != NULL)
            {
                logical bReportError = false;

                PNG_get_string_tokens(pszRelTypesString, ",", &iRelTypeCnt, &ppszRelTypeList);

                if(iRelTypeCnt != 0)
                {
                    if(tc_strcasecmp(pszRelTypesString, "all") != 0)
                    {
                        for(int iDx = 0; iDx < iRelTypeCnt; iDx++)
                        {
                            tag_t tRelType = NULLTAG;

                            ITK_CALL( GRM_find_relation_type (ppszRelTypeList[iDx], &tRelType ));

                            if(tRelType == NULLTAG)
                            {
                                bReportError = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    bReportError = true;
                }

                if(bReportError == true)
                {
                    fprintf(stderr, "Invalid relation types are provided to the utility. Please check and restart the tool with the right parameters.\n");
                    retcode = !ITK_ok;
                }
            }

			if (retcode == ITK_ok && items_file_name != NULL && log_file_name != NULL) 
			{
				PNG_util_printf("%s: Items file [%s]\n", theFunction, items_file_name);
				PNG_util_printf("%s: Log file [%s]\n", theFunction, log_file_name);
				error_file_name = PNG_util_string_append(log_file_name, ".error");
				
				if (error_file_name != NULL)
				{
					PNG_util_printf("%s: Error file [%s]\n", theFunction, error_file_name);
				}
				else
				{
					fprintf(stderr, "Could not create an error file name.\n");
					retcode = !ITK_ok;
				}
                /* This function contains all the procedure the delete the each reference & attachments to an Item */
				ITK_CALL(PNG_ideas_object_process_cleanup(items_file_name, log_file_name, error_file_name, iRelTypeCnt, ppszRelTypeList));
			} 
			else 
			{
				fprintf(stderr, "Invalid parameters were provided for the programm. Please check and restart the tool with the right parameters.\n");
                retcode = !ITK_ok;
			}

            SM_FREE(ppszRelTypeList);
			SM_FREE(error_file_name);
		}

		if (retcode != ITK_ok) 
		{
			PNG_print_usage();
		}

		ITK_exit_module(true);
	}
	
	fprintf(stderr, "Exit status: %s\n", (retcode == ITK_ok ? "successful" : "failed"));

    LEAVE(theFunction);
   	return retcode;
}

/***************************************************************************************************/
int PNG_tc_login()
{
	int	retcode = ITK_ok;
	
	char* user = ITK_ask_cli_argument("-u=");
	char* password = ITK_ask_cli_argument("-p=");
	char* group = ITK_ask_cli_argument("-g=");
	
	logical bypass = true;
	
	if (user == NULL || password == NULL || group == NULL) 
	{
		retcode = !ITK_ok;
	}
	
	if (retcode == ITK_ok) 
	{
		char* error_message	= NULL;
		
		retcode = ITK_init_module(user, password, group);
		if (retcode != ITK_ok) 
		{
			EMH_ask_error_text(retcode, &error_message);
			fprintf(stderr, "TC Login failed. retcode=[%d]. error message=[%s]\n",retcode, error_message);
			if (error_message != NULL) 
			{
				SM_FREE(error_message);
			}
		}

		/* set bypass */
		if(bypass == true) 
		{
		  	retcode = ITK_set_bypass(true);
			if(retcode == 10012) 
			{
				fprintf(stderr, "You are not allowed to bypass the ACL\n");
    			retcode = ITK_ok;
		  	} 
			else 
			{
				ITK_CALL(retcode);
			}
		}
	}
	
    if (retcode != ITK_ok)
	{
		fprintf(stderr, "Login into Teamcenter failed!\n"  );
	}
   	else
	{
		fprintf(stderr, "Logged into Teamcenter...\n\n" );
	}
	
	return retcode;
}
/***************************************************************************************************/
int PNG_ideas_object_process_cleanup(
	char*  itemsFileName,
	char*  logFileName,
	char*  errorFileName,
    int    iRelTypeCnt,
    char** ppszRelTypeList
)
{
  	int retcode = ITK_ok;

	FILE* itemsFile = NULL;
	FILE* logFile = NULL;
	FILE* errorFile = NULL;
	int errorCount	= 0;

	char **itm_ids = NULL;
	int itm_num = 0;
	int cnt = 0;
	time_t start_time;
	time_t end_time;

  	tag_t item_tag = NULLTAG;
	int rev_count = 0;
	tag_t* revs;
	int inx = 0 ;
	char* rev_id;
	int n_references = 0;
	int* levels;
	tag_t* references;
	char** relations;

	tag_t qry = NULLTAG;
	char** entries;
	char** values;
	int result_count = 0;
	tag_t* results;
	tag_t item_from_rev = NULLTAG;
    char* item_id ;
    int n_target_ir = 0;
    tag_t* target_irs;
    PROP_value_type_t valtype;
    char* n_valtype;
    int n_target_obj = 0;
    tag_t* target_obj;
    int num_bv = 0;
    tag_t* bv_list;
	
  	const char* theFunction = "PNG_ideas_object_process_cleanup";
  	ENTER(theFunction);

	/* open log file */
	if (retcode ==  ITK_ok && (logFile = fopen(logFileName, "w")) == NULL) 
    {
		retcode = !ITK_ok;
		printf("failed to open Log File %s\n", logFileName);
	}
	/* open error file */
	if (retcode ==  ITK_ok && (errorFile = fopen(errorFileName, "w")) == NULL) 
    {
		retcode = !ITK_ok;
		printf("failed to open Error File %s.error\n", errorFileName);
	}
	/* open input items file */
	if (retcode ==  ITK_ok && (itemsFile = fopen(itemsFileName, "r")) == NULL) 
    {
		retcode = !ITK_ok;
		printf("failed to open Items File %s\n", itemsFileName);
	}

	/* Log begin of process */
	time ( &start_time );
	fprintf(logFile, "Utility started at : %s\n", ctime ( &start_time ) );
	PNG_util_printf( "%s: Utility started at : %s\n", theFunction, ctime ( &start_time ) );
	
	if ( retcode == ITK_ok ) 
    {
		fprintf(logFile,"Starting program to process Items\n");

		ITK_CALL(PNG_read_item_ids_from_file(itemsFile, logFile, &itm_num, &itm_ids));
		fprintf(logFile,"Read %d objects from items File\n\n", itm_num);

		AM__set_application_bypass(true);
        /************************************************************************************************************
        *   the following loop runs the delete procedure for each item read from the input file                     *
        *************************************************************************************************************/
        for( cnt = 0; cnt < itm_num; cnt++ )
		{
			fprintf(logFile, "%d/%d: Start Processing of item [%s]\n", cnt+1, itm_num, itm_ids[cnt]);
			PNG_util_printf("%s: %d/%d: Start Processing of item [%s]\n",theFunction, cnt+1, itm_num, itm_ids[cnt]);

			//Get the item tag
			ITK_CALL( ITEM_find_item( itm_ids[ cnt ] , &item_tag ) );
			printf( "\n============================\nItem ID: %s\n============================\n", itm_ids[ cnt ] );

			if( item_tag != NULLTAG )
			{
				//finding all revisions occurring under this item
				ITK_CALL( ITEM_list_all_revs( item_tag , &rev_count , &revs ) );
                /****************************************************************
                *   Following loop runs for each IR & deletes all refs for      *
                *   its attached objects, then the attached object & finally    *
                *   deletes the revision itself                                 *
                *****************************************************************/
				for( inx = rev_count-1 ; inx >=0  ; inx -- )
				{
					ITK_CALL( AOM_ask_value_string( revs[ inx ] , "item_revision_id" , &rev_id ) );
					printf( "---------------------------\nRevision: %s\n---------------------------\n", rev_id );
                    // Delete Datasets
                    ITK_CALL( PNG_cut_or_delete_ds_attached_with_rel_type(revs[inx], iRelTypeCnt, ppszRelTypeList));

					// Delete Forms
					ITK_CALL( PNG_delete_attached_datasets( revs[inx] ));
					ITK_CALL( PNG_delete_specification_datasets( revs[inx] ));
					ITK_CALL( PNG_delete_rev_master( revs[inx] ) );
                    ITK_CALL( PNG_remove_from_all_folders( revs[inx] ) );
					ITK_CALL( PNG_remove_from_all_tasks( revs[inx] ) );
                    ITK_CALL( PNG_delete_item_revision( revs[inx] ) );
                    if( rev_id != NULL )
                        SM_FREE( rev_id );
                    
				}
                /* Delete the direct references other than IRs for Item & finally the item */

				// Delete Item Master Forms
				ITK_CALL( PNG_delete_item_master( item_tag ) );
				// Remove references of Item from Folders & then delete the item
				ITK_CALL( PNG_remove_from_all_folders( item_tag ) );
				if( item_tag != NULLTAG )
					ITK_CALL( ITEM_delete_item( item_tag ) );
                // Free Memory allocated
                if( num_bv > 0 )
                    SM_FREE( bv_list );
                if( rev_count > 0 )
                    SM_FREE( revs );
			}
			else
			{
				fprintf(logFile, "Item [%s] not found in database\n" , itm_ids[cnt] );
				PNG_util_printf("%s: Item [%s] not found in database\n" , theFunction, itm_ids[cnt] );
                printf( "Item [%s] not found in database\n" , itm_ids[cnt] );
			}
			fprintf(logFile,"%d/%d: End Processing of item [%s]\n\n", cnt+1, itm_num, itm_ids[cnt]);
			PNG_util_printf("%s: %d/%d: End Processing of item [%s]\n\n", theFunction, cnt+1, itm_num, itm_ids[cnt]);
		}
        SM_FREE( itm_ids );
		
		AM__set_application_bypass(false);

		fprintf(stderr, "Memory consumption : %d\n" , TC_ask_memory_in_use() );
		
		fprintf(logFile,"Stopping program to process Items, errorCount was %d\n",errorCount);
	}
	
	/* Log end of process */
	time ( &end_time );
	fprintf(logFile, "\nUtility ended at : %s\n", ctime ( &end_time ) );
	PNG_util_printf( "%s: Utility ended at : %s\n", theFunction, ctime ( &end_time ) );

	if (logFile != NULL) 
    {
		fclose(logFile);
	}
	if (errorFile != NULL) 
    {
		fclose(errorFile);
	}
	if (itemsFile != NULL) 
    {
		fclose(itemsFile);
	}

	LEAVE(theFunction);
    return retcode;
}


/***************************************************************************************************/




/*****************************************************************/

/***************************************************************************************************************/

static int PNG_read_item_ids_from_file(
		FILE* inputFile,				/* I */
		FILE* logFile,					/* I */
		int* item_cnt,					/* O */
		char*** item_ids				/* OF */
)
{
	int retcode = ITK_ok;
    char line[BUFSIZE];

	int count = 0;
	char** items = NULL;

	const char* theFunction = "PNG_read_item_ids_from_file";
	ENTER(theFunction);

	while(fgets(line, BUFSIZE, inputFile) != NULL) 
	{
		char* resultString 	= PNG_util_string_copy(line);

		if (resultString == NULL || strlen(resultString) == 0) 
		{
			continue;
		}
		if (resultString[strlen(resultString) - 1] == '\n') 
		{
			resultString[strlen(resultString) - 1] = '\0';
		}
		//PNG_trim(resultString);
		
		items = (char **) MEM_realloc(items, (count+1) * sizeof(char **));
		
		items[count] = PNG_util_string_copy(resultString);
		PNG_util_printf("%s: index %d: item_id [%s]\n",theFunction, count, items[count]);
		count++;
		
		SM_FREE(resultString);
		retcode = ITK_ok;
	}

	fclose(inputFile);

	if (count > 0 && items != NULL) 
	{
		*item_cnt = count;
		*item_ids = items;
	}

  	LEAVE(theFunction);
  	return retcode;
}
/*******************************************************************************************************/
int is_exists_in_vector( vector<string> vec , char* str )
{
    int flag = 0;
	string s = str;
    for( int inx = 0 ; inx < vec.size() ; inx ++ )
	{
	    if( vec[ inx ].compare( s ) == 0 )
        {
		    flag = 1;;
        }		
	}
	return flag;
}


string& trim( string &str )
{
    int i,j,start,end;
    //ltrim
    for (i=0; ( str[ i ]!=0 && str[ i ]<=32 ); )
    {
        i++;
	}
    start=i;
    //rtrim
    for( i=0 , j=0 ; str[ i ]!=0 ; i++ )
    {
        j = ( ( str[ i ] <=32 )? j+1 : 0 );
	}
    end = i-j;
    str = str.substr( start , end-start );
    return str;
}


/********************************************************************************************************************
 * PNG_updateAttribute will update the given attribute on the Item Revision. This will remove attribute values of
 * status objects (release_status_list attribute) and the BVR's(structure_revisions attribute) on the Item Revision.
 *
 * Author - Sridhar Kandibanda
 ********************************************************************************************************************/

int PNG_updateAttribute( tag_t objTag,            // <I>
                         char *attrname,          // <I>  
                         tag_t value              // <I>
                       )
{
    int stat = ITK_ok;
    char *className = NULL;
    tag_t instClassId = NULLTAG;
    tag_t tAttrId = NULLTAG;
    int valuesindex = -1;
    logical bypass = true;
	int attrlength = 0;

    ITK_CALL( POM_class_of_instance(objTag,&instClassId) );
    ITK_CALL( POM_name_of_class(instClassId,&className) );
	ITK_CALL( AOM_lock(objTag) );	
    ITK_CALL( POM_attr_id_of_attr(attrname,className,&tAttrId) );
	ITK_CALL( POM_length_of_attr(objTag,tAttrId,&attrlength));
    ITK_CALL( POM_ask_index_of_tag(objTag,tAttrId,value,&valuesindex));
    ITK_CALL( ITK_set_bypass(bypass));

    if(bypass)
    {
        if( valuesindex != -1 )
            ITK_CALL( POM_remove_from_attr(1,&objTag,tAttrId,valuesindex,1));
        
        ITK_CALL( AOM_save(objTag));
    }
	ITK_CALL( AOM_unlock(objTag));	
	
    return 0;
}


/**************************************************************************/


int PNG_delete_attached_datasets_old( tag_t rev_tag )
{
    int n_attach = 0;
    tag_t *attachs;
    tag_t tc_attach_relation_type =NULLTAG;
    char ds_type[WSO_name_size_c+1];
    //char** values, entries;
    tag_t qry = NULLTAG;

	int result_count = 0;
    tag_t* results;
    char* ds_name;

    // Find attachments (DS) with TC_Attaches relation & delete
    //ITK_CALL( GRM_find_relation_type ( "TC_Attaches", &tc_attach_relation_type ) );
    ITK_CALL( GRM_list_secondary_objects_only ( rev_tag, tc_attach_relation_type, &n_attach, &attachs ) );
    printf( "Attachments with TC_Attaches : %d\n", n_attach );
    
    for( int iatch = 0; iatch < n_attach; iatch++ )
    {
        tag_t ds_rev_anchor = NULLTAG;

        if( attachs[iatch] != NULLTAG )
        {   
            ITK_CALL( WSOM_ask_object_type( attachs[iatch], ds_type ) );
            ITK_CALL( PNG_remove_from_all_folders( attachs[iatch] ) );
            printf( "Dataset/Form type : %s\n", ds_type );

			int n_ref = 0;           
			int* levels;
                tag_t* refs;
                tag_t rel_type = NULLTAG, rel_tag = NULLTAG;
                
                ITK_CALL( PNG_delete_obj_relations( attachs[iatch] ) );
                ITK_CALL( PNG_where_ask_referencers( attachs[iatch], &n_ref, &refs ));
                for( int bb = 0; bb < n_ref; bb++ )
                {
                    if( refs[bb] != NULLTAG )
                        ITK_CALL( AOM_delete_from_parent( attachs[iatch], rev_tag ) );
                }
                if( n_ref > 0 )
                    SM_FREE( refs );

                if( attachs[iatch]!= NULLTAG )
                    ITK_CALL( AOM_delete( attachs[iatch] ) );

        }
    }
    
    if( n_attach > 0 )
        SM_FREE( attachs );
    return 0;
}



int PNG_cut_or_delete_ds_attached_with_rel_type( tag_t rev_tag, int iRelTypeCnt, char** ppszRelTypeList)
{
    int    iAttachCnt  = 0;
    tag_t* ptAttachs   = NULL;

    if(iRelTypeCnt == 0)
    {
        ITK_CALL( GRM_list_secondary_objects_only (rev_tag, NULLTAG, &iAttachCnt, &ptAttachs));
    }
    else
    {
        if(tc_strcasecmp(ppszRelTypeList[0], "all") == 0)
        {
            ITK_CALL( GRM_list_secondary_objects_only (rev_tag, NULLTAG, &iAttachCnt, &ptAttachs));
        }
        else
        {
            ITK_CALL( PNG_grm_get_secondary_obj(rev_tag, iRelTypeCnt, ppszRelTypeList, &ptAttachs, &iAttachCnt));
        }
    }

    // Find attachments (DS) with input relation & delete
    printf( "Dataset Attachment Count : %d\n", iAttachCnt );
    
	AM__set_application_bypass( true ) ;
    
	for( int iDx = 0; iDx < iAttachCnt; iDx++ )
    {  
        logical bIsTypeOf = false;

        ITK_CALL( PNG_obj_is_type_of(ptAttachs[iDx], "Dataset", &bIsTypeOf));

        if(bIsTypeOf == true)
        {
            int             iRelCnt = 0;
		    GRM_relation_t* relObj = NULL;
            char            ds_type[WSO_name_size_c+1] = "";

		    ITK_CALL( PNG_remove_from_all_tasks( ptAttachs[iDx] ));
     
            ITK_CALL( PNG_remove_from_all_folders( ptAttachs[iDx] ));

            ITK_CALL( WSOM_ask_object_type( ptAttachs[iDx], ds_type ));
            printf( "Dataset type : %s\n", ds_type );

		    ITK_CALL(GRM_list_all_related_objects( ptAttachs[iDx], &iRelCnt, &relObj ));

		    for( int iJx = 0; iJx < iRelCnt; iJx++ )
		    {
		       if( relObj[iJx].the_relation != NULLTAG )
		       {      
			       tag_t rel = NULLTAG;
			       ITK_CALL( GRM_find_relation( relObj[iJx].primary, relObj[iJx].secondary, relObj[iJx].relation_type, &rel ) );

			       if (rel != NULLTAG)
				    {
					    ITK_CALL(AOM_unload( rel ));
					    ITK_CALL( GRM_delete_relation(rel));
			       }
		       }
		    }
		    if( iRelCnt > 0 )
			    SM_FREE( relObj );

		    if( ptAttachs[iDx]!= NULLTAG && iRelTypeCnt != 0)
            {
			    ITK_CALL(AOM_refresh(ptAttachs[iDx], POM_no_lock));
			    ITK_CALL(AOM_unload(ptAttachs[iDx]));
			    ITK_CALL( AOM_delete( ptAttachs[iDx] ) );
		    }
        }
    }

    if( iAttachCnt > 0 )
        SM_FREE( ptAttachs );
    return 0;

}

int PNG_delete_attached_datasets( tag_t rev_tag )
{
    int n_attach = 0;
    tag_t *attachs;
    tag_t tc_attach_relation_type =NULLTAG;
    char ds_type[WSO_name_size_c+1];
    //char** values, entries;
    tag_t qry = NULLTAG;

	int result_count = 0;
    tag_t* results;
    char* ds_name;

    // Find attachments (DS) with TC_Attaches relation & delete
    ITK_CALL( GRM_find_relation_type ( "TC_Attaches", &tc_attach_relation_type ) );
    ITK_CALL( GRM_list_secondary_objects_only ( rev_tag, tc_attach_relation_type, &n_attach, &attachs ) );
    printf( "Attachments with TC_Attaches : %d\n", n_attach );
    
	AM__set_application_bypass( true ) ;
    
	for( int iatch = 0; iatch < n_attach; iatch++ )
    {
        tag_t ds_rev_anchor = NULLTAG;

        if( attachs[iatch] != NULLTAG )
        {
            logical bIsTypeOf = false;

            ITK_CALL( PNG_obj_is_type_of(attachs[iatch], "Dataset", &bIsTypeOf));

            if(bIsTypeOf == false)
            {
                ITK_CALL( WSOM_ask_object_type( attachs[iatch], ds_type ) );
                ITK_CALL( PNG_remove_from_all_folders( attachs[iatch] ) );
                printf( "Dataset/Form type : %s\n", ds_type );
			    int relCnt;
			    GRM_relation_t* relObj = NULL;

			    ITK_CALL(GRM_list_all_related_objects( attachs[iatch], &relCnt, &relObj ));
			    for( int rr = 0; rr < relCnt; rr++ )
			    {
			       if( relObj[rr].the_relation != NULLTAG )
			       {      
				       tag_t rel = NULLTAG;
				       ITK_CALL( GRM_find_relation( relObj[rr].primary, relObj[rr].secondary, relObj[rr].relation_type, &rel ) );

				       if (rel != NULLTAG)
					    ITK_CALL( GRM_delete_relation(rel));
			       }
			    }
			    if( relCnt > 0 )
				    SM_FREE( relObj );
    	 
                //ITK_CALL( AOM_delete_from_parent( attachs[iatch], rev_tag ) );

                if( attachs[iatch]!= NULLTAG )
                        ITK_CALL( AOM_delete( attachs[iatch] ) );
            }
        }
    }

    if( n_attach > 0 )
        SM_FREE( attachs );
    return 0;
}




int PNG_delete_specification_datasets( tag_t rev_tag )
{
    int n_attach = 0;
    tag_t *attachs;
    tag_t tc_attach_relation_type =NULLTAG;
    char ds_type[WSO_name_size_c+1];
    //char** values, entries;
    tag_t qry = NULLTAG;

	int result_count = 0;
    tag_t* results;
    char* ds_name;

    // Find attachments (DS) with TC_Attaches relation & delete
    ITK_CALL( GRM_find_relation_type ( "IMAN_specification", &tc_attach_relation_type ) );
    ITK_CALL( GRM_list_secondary_objects_only ( rev_tag, tc_attach_relation_type, &n_attach, &attachs ) );
    printf( "Attachments with IMAN_specification : %d\n", n_attach );
    
	AM__set_application_bypass( true ) ;
    
	for( int iatch = 0; iatch < n_attach; iatch++ )
    {
        tag_t ds_rev_anchor = NULLTAG;

        if( attachs[iatch] != NULLTAG )
        {
            logical bIsTypeOf = false;

            ITK_CALL( PNG_obj_is_type_of(attachs[iatch], "Dataset", &bIsTypeOf));

            if(bIsTypeOf == false)
            {
			    ITK_CALL( PNG_remove_from_all_tasks( attachs[iatch] ) );
                ITK_CALL( WSOM_ask_object_type( attachs[iatch], ds_type ) );
                ITK_CALL( PNG_remove_from_all_folders( attachs[iatch] ) );
                printf( "Dataset/Form type : %s\n", ds_type );
			    int relCnt;
			    GRM_relation_t* relObj = NULL;

			    ITK_CALL(GRM_list_all_related_objects( attachs[iatch], &relCnt, &relObj ));
			    for( int rr = 0; rr < relCnt; rr++ )
			    {
			       if( relObj[rr].the_relation != NULLTAG )
			       {      
				       tag_t rel = NULLTAG;
				       ITK_CALL( GRM_find_relation( relObj[rr].primary, relObj[rr].secondary, relObj[rr].relation_type, &rel ) );

				       if (rel != NULLTAG)
					    {
						    //ITK_CALL(AOM_refresh(job, POM_modify_lock));
						    ITK_CALL(AOM_unload( rel ));
						    ITK_CALL( GRM_delete_relation(rel));
				       }
			       }
			    }
			    if( relCnt > 0 )
				    SM_FREE( relObj );
    	 
                //ITK_CALL( AOM_delete_from_parent( attachs[iatch], rev_tag ) );

			    if( attachs[iatch]!= NULLTAG ){
				    ITK_CALL(AOM_refresh(attachs[iatch], POM_no_lock));
				    ITK_CALL(AOM_unload(attachs[iatch]));
				    ITK_CALL( AOM_delete( attachs[iatch] ) );
			    }
            }
        }
    }

    if( n_attach > 0 )
        SM_FREE( attachs );
    return 0;
}


/***********************************************************************************************************
*
***********************************************************************************************************/

int PNG_delete_Datasets(tag_t dstTag )
{
      int relCnt = 0;
      GRM_relation_t* relObj = NULL;
      int retcode = ITK_ok;
      int iIdx2;
      logical flag =true;
      //--------------------------
      int dd = 0;
      char* ds_name;
      int nFound = 0;
      tag_t* dataset;
      int num_ds = 0;
      tag_t revision_anchor = NULLTAG;
      char* name = NULL;
      char* dname;

      AM__set_application_bypass ( true ) ;

      if( dstTag != NULLTAG )
      {
          ITK_CALL( AOM_ask_value_string( dstTag, "object_name", &ds_name ) );
          ITK_CALL( AE_find_all_datasets ( ds_name, &nFound, &dataset ) );
      }
     
      if( nFound > 0 )
      {
          for( dd = 0; dd < nFound; dd++ )
          {
              if( dataset[dd] != NULLTAG )
              {
                  //002: Delete all relations
                  ITK_CALL(GRM_list_all_related_objects( dataset[dd], &relCnt, &relObj ));
                  for( int rr = 0; rr < relCnt; rr++ )
                  {
                     if( relObj[rr].the_relation != NULLTAG )
                      {
                          ITK_CALL( GRM_delete_relation(relObj[rr].the_relation ));
                      }
                  }
                  if( relCnt > 0 )
                      SM_FREE( relObj );
                  //003: Delete Dataset Anchor
                  ITK_CALL( AE_ask_dataset_anchor( dataset[dd], &revision_anchor ) );
                  if( revision_anchor != NULLTAG )
                    ITK_CALL( AOM_delete( revision_anchor ) );
              }
          }
          // 004: Delete the Dataset Version
          for( dd = 0; dd < nFound; dd++ )
          {
              ITK_CALL( AOM_delete( dataset[dd] ) );
          }
          SM_FREE( dataset );
      }
      if( dstTag != NULLTAG )
          SM_FREE( ds_name );

      return 0 ;
}




/********************************************************************************/
int PNG_delete_item_revision( tag_t revtag )
{
      int relCnt = 0;
      GRM_relation_t* relObj = NULL;
      int retcode = ITK_ok;
      int iIdx2;
      logical flag =true;

      ITK_CALL(GRM_list_all_related_objects(revtag,&relCnt,&relObj));

      for(iIdx2=0; iIdx2<relCnt; iIdx2++)
      {
          if( relObj[iIdx2].the_relation != NULLTAG )
              ITK_CALL(GRM_delete_relation(relObj[iIdx2].the_relation));
      }

      AM__set_application_bypass( true ) ;

      if( revtag != NULLTAG )
        ITK_CALL( ITEM_delete_rev( revtag ) );

      if( revtag != NULLTAG )
          ITK_CALL( AOM_delete( revtag ) );
      
      return retcode ;
}

/********************************************************************************

/********************************************************************************
********************************************************************************/

int PNG_where_ask_referencers (
    tag_t   objectTag,                               /* (I) */
    int*    referencerCount,                         /* (o) */
    tag_t** referencerList                           /* (OF) SM_FREE */
    )
{
    int   retcode        = ITK_ok;
    int   classCount     = 0;
    int   *instanceLvl   = NULL;
    int   *instanceFound = NULL;
    int   *classLvl      = NULL;
    int   *classFound    = NULL;
    tag_t *classList     = NULL;

    *referencerCount = 0;
    *referencerList  = NULL;

    ITK_CALL(POM_referencers_of_instance(objectTag, 1, POM_in_ds_and_db, referencerCount, 
                                    referencerList, &instanceLvl, &instanceFound, 
                                    &classCount, &classList, &classLvl, &classFound));

    SM_FREE(instanceLvl);
    SM_FREE(instanceFound);
    SM_FREE(classList);
    SM_FREE(classLvl);
    SM_FREE(classFound);


    return retcode;
}
/********************************************************************************
********************************************************************************/
int PNG_delete_item_master( tag_t item )
{
    tag_t master_rel = NULLTAG, rel=NULLTAG;
    int nmaster = 0;
    tag_t* masters;
    int nref = 0;
    tag_t* refs;

    ITK_CALL( GRM_find_relation_type ( "IMAN_master_form", &master_rel ) );
    ITK_CALL( GRM_list_secondary_objects_only ( item, master_rel, &nmaster, &masters ) );

    for( int mm = 0; mm < nmaster; mm++ )
    {
       ITK_CALL( GRM_find_relation( item, masters[mm], master_rel, &rel ) );
       ITK_CALL( GRM_delete_relation( rel ) );
       ITK_CALL( PNG_where_ask_referencers( masters[mm], &nref, &refs ) );
       ITK_CALL( POM_delete_instances( nref, refs ) );
       if( masters[mm] != NULLTAG )
           ITK_CALL( AOM_delete( masters[mm] ) );
    }

    return 0;
}
/********************************************************************************************************************/

int PNG_remove_from_all_folders(tag_t object)
{
    int
        cnt = 0,
        ii,
        n_levels,
        n_instances,
        *instance_levels,
        *instance_where_found,
        n_classes,
        *class_levels,
        *class_where_found;
    tag_t
        folder_type,
        newstuff_type,
        *ref_instances,
        *ref_classes,
        type = NULLTAG;
	string taskName;

    ITK_CALL(TCTYPE_find_type("Folder", NULL, &folder_type));
    if (folder_type == NULLTAG)
    {
        //printf("Folder type not found!\n");
        return FALSE;
    }
    ITK_CALL(TCTYPE_find_type("Newstuff Folder", NULL, &newstuff_type));
    if (newstuff_type == NULLTAG)
    {
        //ECHO("Newstuff Folder type not found!\n");
        return FALSE;
    }
    ITK_CALL(POM_referencers_of_instance(object, 1, POM_in_ds_and_db,
        &n_instances, &ref_instances, &instance_levels,
        &instance_where_found, &n_classes, &ref_classes, &class_levels,
        &class_where_found));

    if (n_instances > 0)
    {
        for (ii = 0; ii < n_instances; ii++)
        {
            ITK_CALL(TCTYPE_ask_object_type(ref_instances[ii], &type));
            if ((type == folder_type) || (type == newstuff_type ))
            {
                ITK_CALL(AOM_refresh(ref_instances[ii], TRUE));
                ITK_CALL(FL_remove(ref_instances[ii], object));
                ITK_CALL(AOM_save(ref_instances[ii]));
                ITK_CALL(AOM_refresh(ref_instances[ii], FALSE));
                cnt++;
            }
        }
        SM_FREE(ref_instances);
        SM_FREE(instance_levels);
        SM_FREE(instance_where_found);
    }
    if (n_classes > 0)
    {
        SM_FREE(ref_classes);
        SM_FREE(class_levels);
        SM_FREE(class_where_found);
    }

    return cnt;
}



int PNG_remove_from_all_tasks(tag_t object)
{
    int
        cnt = 0,
        ii,
        n_levels,
        n_instances,
        *instance_levels,
        *instance_where_found,
        n_classes,
        *class_levels,
        *class_where_found;
    tag_t
		epmtask_type,
		job_type,
		parent_task,
        *ref_instances,
        *ref_classes,
        type = NULLTAG;
	string taskName;

    ITK_CALL(TCTYPE_find_type("EPMTask", NULL, &epmtask_type));
    if (epmtask_type == NULLTAG)
    {
        //printf("EPMTask type not found!\n");
        return FALSE;
    }
    ITK_CALL(TCTYPE_find_type("Job", NULL, &job_type));
    if (job_type == NULLTAG)
    {
        //printf("Job type not found!\n");
        return FALSE;
    }

    ITK_CALL(POM_referencers_of_instance(object, 1, POM_in_ds_and_db,
        &n_instances, &ref_instances, &instance_levels,
        &instance_where_found, &n_classes, &ref_classes, &class_levels,
        &class_where_found));

    if (n_instances > 0)
    {
        for (ii = 0; ii < n_instances; ii++)
        {
            ITK_CALL(TCTYPE_ask_object_type(ref_instances[ii], &type));
			if (type == epmtask_type)
			{
				// DELETE JOB				
                tag_t job = NULLTAG;

				ITK_CALL(EPM_ask_job( ref_instances[ii], &job ));
				ITK_CALL(AOM_refresh(job, POM_no_lock));
				ITK_CALL(PNG_remove_from_all_folders(job));
				ITK_CALL(AOM_unload(job));
				ITK_CALL(AOM_delete( job ));

				// DELETE TASK
				ITK_CALL(AOM_refresh(ref_instances[ii], POM_no_lock));
				ITK_CALL(AOM_unload(ref_instances[ii]));
				ITK_CALL(AOM_delete( ref_instances[ii] ));
			}
        }
        SM_FREE(ref_instances);
        SM_FREE(instance_levels);
        SM_FREE(instance_where_found);
    }
    if (n_classes > 0)
    {
        SM_FREE(ref_classes);
        SM_FREE(class_levels);
        SM_FREE(class_where_found);
    }

    return cnt;
}



/******************************************************************************************************************/
int PNG_delete_rev_master( tag_t rev_tag )
{
    tag_t relation_type = NULLTAG;
    int nsec_obj = 0;
    tag_t* sec_objs;
    char* name;
    //-------------------
    int n_ref = 0;
    int* levels;
    tag_t* refs;
    char** relations;
    tag_t rel_type = NULLTAG, rel_tag = NULLTAG;
    
    ITK_CALL( GRM_find_relation_type ( "IMAN_master_form", &relation_type ) );
    ITK_CALL( GRM_list_secondary_objects_only ( rev_tag, relation_type, &nsec_obj, &sec_objs ) );
    
    for( int ii = 0; ii < nsec_obj; ii++ )
    {        
        ITK_CALL( PNG_where_ask_referencers( sec_objs[ii], &n_ref, &refs ));
        for( int bb = 0; bb < n_ref; bb++ )
        {
            if( refs[bb] != NULLTAG )
                ITK_CALL( AOM_delete( refs[bb] ) );
        }
        if( n_ref > 0 )
            SM_FREE( refs );
        
        /*ITK_CALL( WSOM_where_referenced( attachs[iatch], 1, &n_ref, &levels, &refs, &relations ) );
        for( int rr = 0; rr < n_ref; rr++ )
        {
            ITK_CALL( GRM_find_relation_type( relations[rr], &rel_type ) );
            ITK_CALL( GRM_find_relation( attachs[iatch], refs[rr], rel_type, &rel_tag ));
            if ( rel_tag != NULLTAG )
                ITK_CALL( GRM_delete_relation( rel_tag ));
            if( refs[rr] != NULLTAG )
                ITK_CALL( AOM_delete( refs[rr] ));
        }*/
        if( sec_objs[ii]!= NULLTAG )
            ITK_CALL( AOM_delete( sec_objs[ii] ) );

    }
    if( nsec_obj > 0 )
        SM_FREE( sec_objs );

    return 0;
}

/*********************************************************************************************************************/
int PNG_delete_obj_relations( tag_t obj )
{
      int n_ref = 0;
      int* levels;
      tag_t* refs;
      char** relations;
      tag_t rel_type= NULLTAG;
      tag_t rel_tag = NULLTAG;

      ITK_CALL( WSOM_where_referenced( obj, 1, &n_ref, &levels, &refs, &relations ) );
      for( int rr = 0; rr < n_ref; rr++ )
      {
        ITK_CALL( GRM_find_relation_type( relations[rr], &rel_type ) );
        ITK_CALL( GRM_find_relation( obj, refs[rr], rel_type, &rel_tag ));
        if ( rel_tag != NULLTAG )
            ITK_CALL( GRM_delete_relation( rel_tag ));
      }

      return 0;
}



/*********************************************************************/
int PNG_delete_free_ds_forms( char* item_id, char* rev_id, tag_t item_tag )
{
	char* ds_name1;
	char* ds_name2;

	ds_name1 = (char*) MEM_alloc( 256 * sizeof( char ) );
	tag_t ds_tag1 = NULLTAG;
	strcpy( ds_name1, item_id );
	strcat( ds_name1, "/" );
	strcat( ds_name1, rev_id );
	printf( "Finding Dataset: %s\n", ds_name1 );
	// Ideas Part/Assembly Dataset
	ITK_CALL( AE_find_dataset( ds_name1, &ds_tag1 ) );
	ITK_CALL( PNG_delete_Datasets( ds_tag1 ) );
    // Ideas Drawing dataset
	ITK_CALL( AOM_ask_value_string( item_tag, "object_name", &ds_name2 ) );
	ITK_CALL( AE_find_dataset( ds_name1, &ds_tag1 ) );
	ITK_CALL( PNG_delete_Datasets( ds_tag1 ) );
    // Custom Forms
	//ITK_CALL( PNG_delete_forms( ds_name2 ) );
    // Free memory
    SM_FREE(ds_name1);
    SM_FREE(ds_name2);
    return 0;
}

/*****************************************************************
*   Functions from sd_ideas_cleanup_util.c
******************************************************************/
void PNG_fprintf (
    FILE*       fOutput,        /* I */
    const char* formatString,
    ...
    )
{
    va_list ap;
	const void* theArg = NULL;
	logical toSysLog = true; // syslog shoulf ne activated in the ENV. check later from ENV if syslog activated
	static logical prefix = true;
    static logical newline  = true;

    va_start(ap, formatString);

	if (prefix) 
	{
		if (toSysLog) 
		{
			TC_write_syslog("PNG> ");
		}
		else 
		{
			fprintf(fOutput, "PNG> ");
		}
	}
	
    theArg = va_arg(ap, const void*);
	
	if (toSysLog) 
	{
        TC_write_syslog(formatString, theArg);
	}
	else 
	{
        fprintf(fOutput, formatString, theArg);
	}

    /* Compute newline=true when a new line has been print out */
    if (formatString[strlen(formatString)-1] != '\n')
    {
        /* check for something like PNG_fprintf(stderr, "%c", '\n') */
        if (strcmp(formatString, "%c") == 0) {
            newline = ((unsigned int)theArg == 0x0a) ? (true) : (false);
        }
        else {
            newline = false;
        }
    }
    else /* New line is the last char of the format string */
    {
        newline = true;
    }
    prefix = newline;

	va_end(ap);
}

/**
 * @brief
 *  
 */
void PNG_util_va_fprintf (
    FILE*       fOutput,        /* I */
    const char* formatString,	/* I */
    va_list     vaArgs			/* I */
)
{
    const char* pFormat;
	
    for (pFormat = formatString; (*pFormat != '\0'); pFormat++)
    {
        if (*pFormat == '%')
        {
            /* just standard holder */
			int         i  = 0;
			int  percentModifier = -1;
			char fmt[128];
			const char* pt = NULL;
			const void* next = va_arg(vaArgs, const void*);
			
			memset(fmt, 0, sizeof(fmt));
			fmt[0] = *pFormat;
			for (i = 1, pt = pFormat+1; ((*pt != '\0') && (*pt != '%') && (*pt != '$') && (i < (int)sizeof(fmt))); pt++)
			{
				if (percentModifier == -1) {
					if ((isupper(*pt)) || (islower(*pt))) percentModifier = i;
				}
				fmt[i++] = *pt;
			}

			if ((next == NULL) && (percentModifier != -1) && (fmt[percentModifier] == 's')) {
				PNG_fprintf(fOutput, fmt, "NullPointer");
			}
			else {
				PNG_fprintf(fOutput, fmt, next);
			}

			pFormat+=(strlen(fmt) - 1);
        }
        else /* Flush the current format character */
        {
            PNG_fprintf(fOutput, "%c", *pFormat);
        }
    }	
	
	return;
}

/**
 * @brief
 *  
 */
void PNG_util_printf (
    const char* formatString,	/* I */
    ...                         /* I */
)
{
    va_list ap;

    va_start(ap, formatString);
	PNG_util_va_fprintf(stdout, formatString, ap);
	va_end(ap);
}

/**
 * @brief
 * copy given string and creates the mem for it.
 *
 */
char* PNG_util_string_copy (
	const char* inputString		/* I */
)
{
    char* result = NULL;

    if (inputString != (const char*)NULL) {
        if ((result = (char*)MEM_alloc((strlen(inputString)+1) * sizeof(char))) != (char*)NULL) {
            strcpy(result, inputString);
        }
    }

    return result;
}

/**
 * @brief
 * creates a new string and mem from to strings as appendage. it uses MEM_alloc. 
 *
 */
char* PNG_util_string_append (
    char* str,					/* I */
    const char* appendage   	/* I */
)
{
    char* result = NULL;

    if ((str != NULL) && (appendage != NULL))
    {
        size_t len = ((strlen(str)+1) + (strlen(appendage)+1)) * sizeof(char);

        if ((result = (char*)MEM_alloc(len)) != NULL) {
            memset(result, 0, len);
            strcpy(result, str);
            strcat(result, appendage);
        }
    }

    return result;
}

/**
 * @brief
 * appends a string to given one and delivers the new pointer. it uses MEM_realloc. 
 *
 */
char* PNG_util_string_append_realloc (
    char** str,		 			/* I/O */
    const char* appendage	 	/* I */
) 
{
    if ((str != NULL) && (appendage != NULL))
    {
        size_t len = ((strlen(*str)+1) + (strlen(appendage)+1)) * sizeof(char);

        if ((*str = (char*)MEM_realloc(*str, len)) != NULL) {
            strcat(*str, appendage);
        }
    }
    return *str;
}

char* PNG_trim(char *pc_string)
{
   pc_string = PNG_trim_leading(pc_string);
   pc_string = PNG_trim_trailing(pc_string);

   return (pc_string);
}

char* PNG_trim_leading(char *pc_string)
{
   size_t n_strlen_cnt = 0;
   size_t n_strlen = 0;
   unsigned long n_count = 0;

   if (pc_string)
   {
      n_strlen_cnt = strlen(pc_string);
      n_strlen = n_strlen_cnt;

      while ((n_strlen > 0) && isspace((unsigned char) pc_string[n_count]))
      {
         n_strlen--;
         n_count++;
      }
      memmove(pc_string,
              pc_string + n_strlen_cnt - n_strlen,
              n_strlen);
      memmove(pc_string + n_strlen,
              " ",
              n_strlen_cnt - n_strlen);
      pc_string[n_strlen_cnt - n_count] = STR_END;
   }

   return (pc_string);
} 

char* PNG_trim_trailing(char *pc_string)
{
   size_t n_strlen = 0;

   if (pc_string)
   {
      n_strlen = strlen(pc_string);
      while ((n_strlen > 0) && isspace((unsigned char) pc_string[n_strlen - 1]))
      {
         n_strlen--;
      }
      pc_string[n_strlen] = STR_END;
   } 

   return (pc_string);
} 

int PNG_print_usage() 
{
	int retcode = ITK_ok;
	
  	//fprintf(stderr, "Usage:\n");
    //fprintf(stderr, "delete_util.exe -u=<login> -p=<password> -g=<group> -input_file=<fullpath_items_file> -log_file=<fullpath_log_file> [-dryrun] [-relation_types=]\n");
  	//fprintf(stderr, "\nSet the nodryrun if you want to make changes in the DB\n\n");
    printf("     \n\t\t\n Item Delete Utility Usage \n");
    printf("     \t\t\n-------------------------------\n");
    printf("           -u                   = <user name> to specify user name\n");
    printf("           -p                   = <password> to specify password\n");
    printf("           -g                   = <group name> to specify group name\n");
    printf("           -input_file          = Specifies the input file to be\n\t\t\t\t  processed. File contains one Item ID per line\n");
    printf("           -log_file            = Specifies the log file.\n\t\t\t\t  Information about the Item will be written\n\t\t\t\t  to the log file.\n");
    printf("           -relation_types      = Specifies the relation types separated by\n\t\t\t\t  coma(,).Datasets attached with these\n\t\t\t\t  relations will be deleted while deleting Item\n\t\t\t\t  If not specified Dataset attached to Item  \n\t\t\t\t  revision will be cut and not deleted.\n");
    printf("           -h                     [Help]\n");
  
	return retcode;
}

/**
 * Function tokenize the given string with provided delimited
 *
 * @param[in]  pszInputStr      String to be tokenized
 * @param[in]  pszDelim         Delimiter
 * @param[out] iTokenCnt        Total number of tokens
 * @param[out] pppszAllTokens   String array of tokens
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
void  PNG_get_string_tokens
(
    const char* pszInputStr,
    char*       pszDelim, 
    int*        iTokenCnt,
    char***     pppszAllTokens
)
{
    char* pszTempStr = NULL;
    char* pszToken = NULL;

    PNG_cust_strcpy(pszTempStr, pszInputStr);
   
    if(NULL != (pszToken = tc_strtok(pszTempStr, pszDelim)))
    {
        PNG_update_string_array(iTokenCnt, pppszAllTokens, pszToken);
    }
    while(NULL != (pszToken = tc_strtok(NULL, pszDelim)))
    {
        PNG_update_string_array(iTokenCnt,pppszAllTokens, pszToken);
    }

    SM_FREE(pszTempStr);
}

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int PNG_obj_is_type_of
(
    tag_t    tObject,
    char*    pszTypeName,
    logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
    tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    ITK_CALL(TCTYPE_find_type(pszTypeName, NULL, &tType));

    ITK_CALL(TCTYPE_ask_object_type(tObject, &tInputObjType));

    ITK_CALL(TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}

/**
 * This function returns a list and count of all secondary objects attached with a specified
 * relation to the specified primary_object.
 *
 * @param[in] tPrimaryObj              The object.
 * @param[in] pszRelationTypeName      Name of relation type. Can be NULL.
 * @param[out] pptSecondaryObjects     List of Primary object
 * @param[out] piObjectCount           Primary Object count.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int PNG_grm_get_secondary_obj
(
    tag_t   tPrimaryObj,
    int     iRelTypeCnt,
    char**  ppszRelTypeNames,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
)
{
    int  iRetCode   = ITK_ok;
    int  iCount     = 0;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount) = 0;
    (*pptSecondaryObjects) = NULL;

    for(int iDx = 0; iDx < iRelTypeCnt && iRetCode == ITK_ok; iDx++)
    {
        int     iSecondaryCount      = 0;
        tag_t   tRelationType        = NULLTAG;
        tag_t*  ptSecondaryObjects   = NULL;

        /* get the relation type tag e.g. "IMAN_specification"  */
        ITK_CALL(GRM_find_relation_type(ppszRelTypeNames[iDx], &tRelationType)); 
        
        ITK_CALL(GRM_list_secondary_objects_only(tPrimaryObj, tRelationType, &iSecondaryCount, &ptSecondaryObjects));
                                           
        for(int iJx = 0; iJx < iSecondaryCount && iRetCode == ITK_ok; iJx++)
        {
            int  iActiveSeq   = 0;
          
            ITK_CALL(AOM_ask_value_int(ptSecondaryObjects[iJx], "active_seq", &iActiveSeq));
        
            /* This check indicates that Item Revisions with Active sequence will be considered */
            if(iActiveSeq != 0)
            {
                iCount++;
                (*pptSecondaryObjects) = (tag_t*)MEM_realloc ( (*pptSecondaryObjects) , (sizeof(tag_t) * iCount) );
                (*pptSecondaryObjects)[iCount -1] = ptSecondaryObjects[iJx];
            }
        }

        SM_FREE(ptSecondaryObjects);
    }

    (*piObjectCount ) = iCount;
    return iRetCode;
}

#ifdef __cplusplus
}
#endif

