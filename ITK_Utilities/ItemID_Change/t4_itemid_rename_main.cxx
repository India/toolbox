/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_itemid_rename_main.cxx

    Description: This File contains functions that are entry point to ECO report generation executable

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-May-13   Tarun Kumar Singh   Initial Release
========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <t4_library.hxx>
#include <t4_library.hxx>
#include <t4_const.hxx>
#include <t4_errors.hxx>
#include <t4_update_item_id.hxx>


int T4_init_ITK_login(int argc, char **argv);

void T4_display_usage();

/**
 * This function is the main function where program starts execution. It is responsible for the high-level organization of the
 * program's functionality, and try to access command arguments which basically are login credentials when this executable is run.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int ITK_user_main( int iArgCount, char** ppszArgVal)
{
    int     iRetCode   = ITK_ok;
    logical bByPass    = false;
    logical bNOModify  = false;

    /* display help if user asked for it */
	if ((ITK_ask_cli_argument("-h")) || (iArgCount == 0) )
    {
        T4_display_usage();
        exit(0);
    }

    char* pszLogFile = ITK_ask_cli_argument( "-log_file=" );
    /* validating the log file */ 
    if((pszLogFile == NULL) || (tc_strlen(pszLogFile) == 0))
    {
        fprintf(stderr,"\n\nError: Log file not specified.\n");
        T4_display_usage();
        exit(0);
    }

    char* pszRepeatFileInfo  = ITK_ask_cli_argument( "-repeat_file=");

    /* validating repeat file */ 
    if((pszRepeatFileInfo == NULL) || (tc_strlen(pszRepeatFileInfo) == 0))
    {
        fprintf(stderr,"\n\nError: Repeat File name not specified.\n");
        T4_display_usage();
        exit(0);
    }

    char* pszConfigFileInfo  = ITK_ask_cli_argument( "-input_file=");

    /* validating input file */ 
    if((pszConfigFileInfo == NULL) || (tc_strlen(pszConfigFileInfo) == 0))
    {
        fprintf(stderr,"\n\nError: File name not specified.\n");
        T4_display_usage();
        exit(0);
    }

    char* pszSeparator  = ITK_ask_cli_argument( "-sep=");

    /* validating Separator */ 
    if((pszSeparator == NULL) || (tc_strlen(pszSeparator) == 0))
    {
        pszSeparator = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(T4_STRING_DEFAULT_SEPARATOR) +1)));
        tc_strcpy(pszSeparator, T4_STRING_DEFAULT_SEPARATOR);
    }
	
    for(int iDx = 0; iDx < iArgCount; iDx++)
    {
       if(tc_strcmp("-no_mod", ppszArgVal[iDx]) == 0)
       {
            bNOModify = true;
            break;
       }
    }

	//getch();

    T4_ITKCALL( iRetCode, T4_init_ITK_login(iArgCount, ppszArgVal));

    printf("Successfully Logged In Teamcenter \n");

    if(iRetCode == ITK_ok)
    {
        T4_ITKCALL( iRetCode, T4_process_itemid_update(pszConfigFileInfo, pszLogFile, pszRepeatFileInfo, pszSeparator, true, bNOModify));
    }

	T4_ITKCALL( iRetCode, ITK_exit_module(FALSE));

    return iRetCode;
}

/**
 * This function retrieves login credentials and try to login to Teamcenter with these credential. If not supplied executable
 * performs auto login.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_init_ITK_login(int argc, char **argv)
{
    int   iRetCode        = ITK_ok;

    /* Gets the user ID */
    char* pszUser  = ITK_ask_cli_argument( "-u=");

    /* Gets the user Password */
    char* pszPassword  = ITK_ask_cli_argument( "-p=");

    /* Gets the user group */
    char* pszGroup  = ITK_ask_cli_argument( "-g=");

    /*  To run against motif libs.*/
    T4_ITKCALL(iRetCode, ITK_initialize_text_services(ITK_BATCH_TEXT_MODE));

    /*  Put -h CLI argument here to avoid starting the whole of iman just for a help message! */
    if( ITK_ask_cli_argument( "-h" ) != 0 )
    {
        T4_display_usage();
        return iRetCode;
    }

	if(pszGroup == NULL || pszUser == NULL)
	{
		T4_ITKCALL( iRetCode, ITK_auto_login());		
	}
	else
	{
		T4_ITKCALL( iRetCode, ITK_init_module(pszUser, pszPassword, pszGroup));		
	}

    return iRetCode;
}

/**
 * This function displays the usage of this Executable
 */
void T4_display_usage()
{
    printf("     \n\t\t\n Security Utility Usage \n");
    printf("     \t\t\n-------------------------------\n");
    printf("           -u                   = <user name> to specify user name\n");
    printf("           -p                   = <password> to specify password\n");
    printf("           -g                   = <group name> to specify group name\n");
    printf("           -input_file          = input_file: nSpecifies the input file to be\n\t\t\t\t  processed. It has the format\n\t\t\t\t  ItemID~AttributeKey~AttributeValue.\n\t\t\t\t  Commented lines start with \"#\". \n\t\t\t\t  These lines will be ignored while processing.\n\t\t\t\t  Following AttributeKey's are supported \n\t\t\t\t\tProject\n\t\t\t\t\tLicense\n\t\t\t\t\tIPClassification\n\t\t\t\t\tOwningProject\n");
    printf("           -log_file            = log_file: Specifies the log file.\n\t\t\t\t  Information about the object will be written\n\t\t\t\t  to the log file.\n");
    printf("           -repeat_file         = repeat_file: Specifies the repeat file.\n\t\t\t\t  All non-processed entries from the input file\n\t\t\t\t  have to be written to the repeat file.\n");
    printf("           -sep                 = Specifies the separator to be\n\t\t\t\t  used in the input file. If not specified the\n\t\t\t\t  default will be \"~\".\n");
    printf("           -no_mod                Run this utility in the mode where the last modification date and user will not be updated.\n");
    printf("           -h                     [Help]\n");
}

#ifdef __cplusplus
}
#endif




