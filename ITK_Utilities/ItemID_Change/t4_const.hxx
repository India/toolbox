/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_const.hxx

    Description:  Header File for constants used during scope of the executable.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-May-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_CONST_HXX
#define T4_CONST_HXX

#define MAX_CHARS                                               300

/* Log File Names    */

/* Date Time Format */
#define T4_DATE_TIME_FORMAT_FILENAME                            "%y%m%d%H%M%S"

/* Attributes  */
#define T4_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define T4_ATTR_OWNING_PROJECT                                  "owning_project"
#define T4_ATTR_LICENSE_LIST                                    "license_list"
#define T4_ATTR_IP_CLASSIFICATION                               "ip_classification"
#define T4_ATTR_ACTIVE_SEQ                                      "active_seq"
#define T4_ATTR_DATE_RELEASED                                   "date_released"
#define T4_ATTR_RELEASE_STATUS_LIST                             "release_status_list"
#define T4_ATTR_ITEM_ID                                         "item_id"
#define T4_ATTR_OBJECT_TYPE                                     "object_type"
#define T4_ATTR_OBJECT_NAME                                     "object_name"
#define T4_ATTR_IDENT_NUMBER                                    "t4_ident_number"
#define T4_ATTR_IDENT_EXT                                       "t4_ident_ext"
#define T4_ATTR_IDENT_EXT_COUNTER                               "t4_ident_ext_counter"
#define T4_ATTR_DENOTATION_DE                                   "t4_denotation_de"

/* Relation    */
#define T4_IMAN_MASTER_FORM                                     "IMAN_master_form"
#define T4_TC_DEFAULT_ARRANGEMENT                               "TC_DefaultArrangement"
#define T4_TC_ARRANGEMENT                                       "TC_Arrangement"
#define T4_IMAN_CAPTURE_REL                                     "IMAN_capture"

/* Class Name */
#define T4_CLASS_T4ITEM                                         "T4_Item"
#define T4_CLASS_DATASET                                        "Dataset"
#define T4_CLASS_WORKSPACEOBJECT                                "WorkspaceObject"
#define T4_CLASS_ASSEMBLY_ARRANGEMENT                           "AssemblyArrangement"
#define T4_CLASS_IDEAS_DRAWING                                  "IdeasDrawing"
#define T4_CLASS_IMAGE                                          "Image"

/* Propagation Information */
#define T4_CONST_PROJECT                                        "Project"
#define T4_CONST_LICENSE                                        "License"
#define T4_CONST_IP_CLASSIFICATION                              "IPClassification"
#define T4_CONST_OWNING_PROJECT                                 "OwningProject"
#define T4_CLASS_RELEASE_STATUS                                 "ReleaseStatus"

/* Separators */
#define T4_STRING_SINGLE_SPACE								    " "
#define T4_STRING_DEFAULT_SEPARATOR	      					    "~"
#define T4_CHARACTER_DASH    								    '-'
#define T4_CHARACTER_COLON                                      ':'
#define T4_CHARACTER_FORWARD_SLASH                              '/'



#define NOVALUE                                                 "NoValue"

#define T4_SEPARATOR_LENGTH                                     1
#define T4_NUMERIC_ERROR_LENGTH                                 12


#endif


