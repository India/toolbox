/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_update_item_id.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-May-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <t4_errors.hxx>
#include <t4_library.hxx>
#include <t4_update_item_id.hxx>

/**
 * This utility reads that input file that has the following format: OldItemID~NewItemID~ItemType~ObjectName. As a default separator "~" is used 
 * (if not specified). 'NewItemID' should not exist in the system. Last attribute ObjectName is optional
 *
 * @note: 'NewItemID' for the specified type should not exist in the system.
 *
 * @param[in] pszConfigFileInfo  Name of the Configuration File to be processed
 * @param[in] pszLogFile         Name of Log file
 * @param[in] pszRepeatFile      Name of the Repeat File
 * @param[in] pszSeparator       Separator used in Configuration File 
 * @param[in] bByPass            'true' if By Pass is set else 'false'
 * @param[in] bNOModify          If 'true' then last modification date and user of the objects will not to be changed
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_process_itemid_update
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    logical bByPass,
    logical bNOModify
)
{
    int     iRetCode               = ITK_ok;
    char    gszLine[MAX_CHARS]     = {'\0'};
    logical bRepeatFileExist       = false;
    FILE*   fCfgFile;
    FILE*   fLogFile;
    FILE*   fRepeatFile;
   
    if ((fCfgFile = fopen(pszConfigFileInfo, "r")) == NULL )
    {
        printf("\n** ERROR: Cannot open %s for reading - exiting **\n\n", pszConfigFileInfo);
        return 0;
    }

    if ((fLogFile = fopen(pszLogFile, "w")) == NULL )
    {
        printf("\n** ERROR: Cannot create %s for writing log  **\n\n",pszLogFile);
    }
    else
    {
        if(bByPass == true || bNOModify == true)
        {
            if(bNOModify == true)
            {
                /*-------------------------------------*/
                /* Do not update date fields on change */
                /*-------------------------------------*/
                T4_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_attr_update, false, 0,0,0, NULL));
        
                /*----------------------*/
                /* Bypass access checks */
                /*----------------------*/
                T4_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_access_check, true, 0,0,0, NULL));
            }

            T4_ITKCALL(iRetCode, ITK_set_bypass(true));
        }

        while (fgets(gszLine, MAX_CHARS, fCfgFile) != NULL)
        {
            if ((gszLine[0] != '#') && (gszLine[0] != '\0') && (gszLine[0] != '\n'))
            {
                int    iInfoCnt    = 0;
                char** ppszInfo    = NULL;

                /*================================================*/
                /* Trim the line to remove leading and trailing   */
                /* spaces and discard the \n on the end.          */
                /*================================================*/
                tc_strcpy(gszLine, T4_trim(gszLine));

                T4_ITKCALL(iRetCode, T4_arg_parse_multipleVal(gszLine, pszSeparator[0], &iInfoCnt, &ppszInfo));

                if(iInfoCnt > 0)
                {
                    if(iInfoCnt >= 3)
                    {
                        int     iLen            = 0;
                        int     iNoOfItem       = 0;
                        int     iNewItemCnt     = 0; 
                        tag_t*  ptItem          = NULL;
                        logical bUpdateNameOnly = false;

                        if(tc_strcmp(ppszInfo[0], ppszInfo[1]) == 0)
                        {
                            bUpdateNameOnly = true;
                        }

				        /* Find if Item whose ID needs to be changed exist in the database */
                        T4_ITKCALL(iRetCode, T4_find_item(ppszInfo[0], ppszInfo[2], &iNoOfItem, &ptItem));

				        if(iNoOfItem == 1 &&  bUpdateNameOnly == false)
				        {
					        tag_t* ptNewItem = NULL;

					        /* Find if Item exist in the database with new ID which needs to be assigned to Old Item ID */
					        T4_ITKCALL(iRetCode, T4_find_item(ppszInfo[1], ppszInfo[2], &iNewItemCnt, &ptNewItem));

					        T4_MEM_TCFREE(ptNewItem);
				        }

                        if(iRetCode != ITK_ok)
                        {
                            /* Report Error */
                            if(bRepeatFileExist == false)
                            {
                                T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                            }

                            T4_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, gszLine); 
                        }
                        else
                        {
                            if(iNoOfItem == 1 && (iNewItemCnt == 0 || bUpdateNameOnly == true))
                            {
                                int     iMarkPoint         = 0;
                                int     iIRCount           = 0;
                                int     iStructArraySize   = 0;
                                tag_t*  ptItemRev          = NULL;
                                sIRStatusInfo* sStatusList = NULL;

                                T4_ITKCALL(iRetCode, ITEM_list_all_revs(ptItem[0], &iIRCount, &ptItemRev));

                                /*
                                 * Define a markpoint for database transaction.
                                 */
                                T4_ITKCALL(iRetCode, POM_place_markpoint(&iMarkPoint));

                                for(int iDx = 0; iDx < iIRCount && iRetCode == ITK_ok; iDx++ )
                                {
                                    int    iStatCnt = 0;
                                    tag_t* ptStatus = NULL;

                                    T4_ITKCALL(iRetCode, T4_obj_cut_status(ptItemRev[iDx], &iStatCnt, &ptStatus));

                                    if(iRetCode != ITK_ok)
                                    {
                                        /* Report Error */
                                        char* pszObjString = NOVALUE;

                                        if(bRepeatFileExist == false)
                                        {
                                            T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                        }

                                        AOM_ask_value_string(ptItemRev[iDx], T4_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjString);

                                        T4_REPORT_STATUS_CUT_ERROR(iRetCode, fLogFile, fRepeatFile, gszLine, pszObjString);   

                                        T4_MEM_TCFREE(pszObjString);
                                    }
                                    
                                    if(iStatCnt > 0)
                                    {
                                        iStructArraySize++;
                                        if(iStructArraySize == 1)
                                        {
                                            sStatusList = (sIRStatusInfo*) MEM_alloc( sizeof(struct sIRStatusInfo) * iStructArraySize);
                                        }
                                        else
                                        {
                                            sStatusList = (sIRStatusInfo*) MEM_realloc( sStatusList, sizeof(struct sIRStatusInfo) * iStructArraySize);
                                        }
                                        
                                        sStatusList[iStructArraySize-1].iStatusCnt = iStatCnt;
                                        sStatusList[iStructArraySize-1].ptIRStatus = (tag_t*) MEM_alloc(sizeof(tag_t)*iStatCnt);
                                        sStatusList[iStructArraySize-1].tItemRev   = ptItemRev[iDx];

                                        for(int iJx = 0; iJx < iStatCnt; iJx++)
                                        {
                                            sStatusList[iStructArraySize-1].ptIRStatus[iJx] = ptStatus[iJx];
                                        }
                                    }

                                    T4_MEM_TCFREE(ptStatus);
                                }

                                if(bUpdateNameOnly == false)
                                {
					                /* Set 'item_id' attribute of Item */
                                    T4_ITKCALL(iRetCode, T4_obj_set_str_attribute_val(ptItem[0], T4_ATTR_ITEM_ID, ppszInfo[1]));
                                }

                                if(iInfoCnt > 3)
                                {
                                    /* Set 'object_name' attribute of Item */
                                    T4_ITKCALL(iRetCode, T4_obj_set_str_attribute_val(ptItem[0], T4_ATTR_OBJECT_NAME, ppszInfo[3]));
                                }

                                /* Attributes containing Item ID will be update only when the New Item ID of different from existing Item ID. */
                                if(bUpdateNameOnly == false)
                                {
					                /* Set custom attributes of T4_Item */
					                T4_ITKCALL(iRetCode, T4_set_custom_attribute(ptItem[0], ppszInfo[1]));
                                  
					                /* Set 'object_name' attribute of all secondary Object that are attached to Item */
					                T4_ITKCALL(iRetCode, T4_update_item_secondary_obj_name(ptItem[0], iInfoCnt, ppszInfo));

					                /* Set 'object_name' attribute of BOM View and BOM View Revision that are attached to Item & Revision */
					                T4_ITKCALL(iRetCode, T4_update_bv_and_bvr_name(iInfoCnt, ppszInfo, ptItem[0], iIRCount, ptItemRev));
                                }
						
					            /* Set 'object_name' attribute of all secondary Object that are attached to Item Revision*/
                                T4_ITKCALL(iRetCode, T4_update_ir_secondary_obj_name(iInfoCnt, ppszInfo, iIRCount, ptItemRev));

                                for(int iWx = 0; iWx < iStructArraySize && iRetCode == ITK_ok; iWx++)
                                {
                                    T4_ITKCALL(iRetCode, T4_obj_paste_status(sStatusList[iWx].tItemRev, sStatusList[iWx].iStatusCnt, sStatusList[iWx].ptIRStatus));
                                }
                                
                                if (iRetCode != ITK_ok)
                                {
                                    logical bStateChanged = false;

                                    /* Do the rollback of the database. */
                                    POM_roll_to_markpoint(iMarkPoint, &bStateChanged);

                                    /* Report Error */
                                    if(bRepeatFileExist == false)
                                    {
                                        T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                    }

                                    T4_ERROR_UNABLE_TO_PROCESS_ITEM(iRetCode, fLogFile, fRepeatFile, gszLine, ppszInfo[0]); 
                                }
                                else
                                {
                                    /* Report Error */
                                    if(bUpdateNameOnly == true)
                                    {
                                        fprintf(fLogFile, "Success: Name of the Item '%s' has been successfully changed to '%s'.\n", ppszInfo[0], ppszInfo[3]);
                                    }
                                    else
                                    {
                                        fprintf(fLogFile, "Success: ID of Item '%s' has been successfully changed to ID '%s'.\n", ppszInfo[0], ppszInfo[1]);
                                    }
                                    fflush(fLogFile);
                                }

					            T4_MEM_TCFREE(ptItemRev);
                                T4_FREE_IR_STATUS_STRUCT(sStatusList, iStructArraySize);                   
                            }
                            else
                            {
                                if(iNoOfItem == 0)
                                {
                                    /* Report Error */
                                    fprintf(fLogFile, "Error: Item ID '%s' does not exist in the system. Cannot process the request. \n", ppszInfo[0]); 
                                    fflush(fLogFile); 
                                }
                                else
                                {
                                    if(iNoOfItem > 1)
                                    {
                                        /* Report Error */
                                        fprintf(fLogFile, "Error: More than one Item exist in the system  with ID '%s'.\n", ppszInfo[0]); 
                                        fflush(fLogFile); 
                                    }
                                    else
                                    {
                                        /* Report Error */
                                        if(iNewItemCnt > 0 && bUpdateNameOnly == false)
                                        {
                                            fprintf(fLogFile, "Error: Item ID '%s' already exist in the system. ID of Item '%s' cannot be changed.\n", ppszInfo[1], ppszInfo[0]);
                                        }
                                        else
                                        {
                                            fprintf(fLogFile, "Error: Line '%s' in file %s does not comply with the valid input format of input file.\n", gszLine, pszConfigFileInfo);
                                        }

                                        fflush(fLogFile);
                                    }
                                }

                                /* Generating repeat file */
                                if(bRepeatFileExist == false)
                                {
                                    T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                }

                                fprintf(fRepeatFile, "%s\n", gszLine); 
                                fflush(fRepeatFile);
                            }
                        }

                        T4_MEM_TCFREE(ptItem);
                   }
                   else
                   {
                        /* Report Error */
                        fprintf(fLogFile, "Error: Line '%s' in file %s does not comply with the valid input format of input file.\n", gszLine, pszConfigFileInfo); 
                        fflush(fLogFile); 

                        /* Generating repeat file */
                        if(bRepeatFileExist == false)
                        {
                            T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                        }

                        fprintf(fRepeatFile, "%s\n", gszLine); 
                        fflush(fRepeatFile);

                   }
                }
                else
                {
                    /* Report Error */
                    fprintf(fLogFile, "Error: Cannot Parse the line \n \t'%s' of file %s.\n", gszLine, pszConfigFileInfo); 
                    fflush(fLogFile); 

                    /* Generating repeat file */
                    if(bRepeatFileExist == false)
                    {
                        T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                    }

                    fprintf(fRepeatFile, "%s\n", gszLine); 
                    fflush(fRepeatFile);
                }

                iRetCode = ITK_ok;    

                T4_MEM_TCFREE(ppszInfo);
            }
        } 

        if(bByPass == true)
        {
            ITK_set_bypass(false);
        }
    }

    if(bRepeatFileExist == true)
    {
        fclose( fRepeatFile );
    }

    fclose( fCfgFile );
    fclose( fLogFile );
    return iRetCode;
}

/**
 * This function search the TC database for Item with ID 'pszItemID' and of type 'pszType'.
 *
 * @param[in]  pszItemID  ItemID 
 * @param[in]  pszType    Item Type 
 * @param[out] piItemCnt  Count of Item that are present in the database with Item ID as 'pszItemID' and of type 'pszType' 
 * @param[out] ptItem     List if Item Tags
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_find_item
( 
    char*   pszItemID, 
    char*   pszType, 
    int*    piItemCnt, 
    tag_t** pptItem
)
{
    int    iRetCode       = ITK_ok;
	int    iItemCnt       = 0;
	int    iValidItemCnt  = 0;
	tag_t* ptItem         = NULL;
    char** ppszAttrName   = NULL;
    char** ppszAttrVal    = NULL;
   
	/* Initializing the Out Parameter to this function. */
    (*piItemCnt) = 0;
    (*pptItem)   = NULL;

    ppszAttrName = (char**) MEM_alloc(1 * sizeof(const char*));
    ppszAttrName[0] = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(T4_ATTR_ITEM_ID) +1)));
    tc_strcpy(ppszAttrName[0], T4_ATTR_ITEM_ID);


	ppszAttrVal = (char**) MEM_alloc(1 * sizeof(const char*));
    ppszAttrVal[0] = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(pszItemID) +1)));
    tc_strcpy(ppszAttrVal[0], pszItemID);

    T4_ITKCALL(iRetCode, ITEM_find_items_by_key_attributes(1, (const char**)ppszAttrName, (const char**)ppszAttrVal, &iItemCnt, &ptItem));

	for(int iDx = 0; iDx < iItemCnt && iRetCode == ITK_ok; iDx++)
	{
		logical bFound = false;

		T4_ITKCALL(iRetCode, T4_obj_is_type_of(ptItem[iDx], pszType, &bFound));

		if(bFound == true)
		{
			iValidItemCnt++;

			if(iValidItemCnt == 1)
			{
				(*pptItem)  = (tag_t*) MEM_alloc((int)(sizeof(tag_t)*iValidItemCnt));
				(*pptItem)[iValidItemCnt-1]= ptItem[iDx];
			}
			else
			{
				(*pptItem)  = (tag_t*) MEM_realloc((*pptItem), (int)(sizeof(tag_t)*iValidItemCnt));
				(*pptItem)[iValidItemCnt-1]= ptItem[iDx];
			}

		}
	}
    
	(*piItemCnt) = iValidItemCnt;

	T4_MEM_TCFREE(ptItem);
    T4_MEM_TCFREE(ppszAttrName);
    T4_MEM_TCFREE(ppszAttrVal);
    return iRetCode;
}

/**
 * This function updates the 'object_name' of Item revision and all secondary object that are attached to input list of Item Revisions.
 * Function also updates the 't4_denotation_de' attribute of Item revision to the value of new 'object_name'
 *
 * @param[in] iInfoCnt  Count of values that are collected after parsing a Line from Input file to this utility
 * @param[in] ppszInfo  List of parsed values. 
 * @param[in] iIRCount  Count of Item Revision
 * @param[in] ptItemRev List of Item Revision
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_update_ir_secondary_obj_name
(
    int    iInfoCnt,
    char** ppszInfo,
    int    iIRCount,
    tag_t* ptItemRev
)
{
    int     iRetCode        = ITK_ok;
    char*   pszNewName      = NULL;
    logical bUpdateObjName  = false;

    if(iInfoCnt > 3)
    {
        bUpdateObjName = true;
        pszNewName = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[3]) +1)));
        tc_strcpy(pszNewName, ppszInfo[3]);
    }

    for(int iZx = 0; iZx < iIRCount && iRetCode == ITK_ok; iZx++)
    {
        int    iSecCount             = 0;
        GRM_relation_t* ptSecObjects = NULL;

        if(bUpdateObjName == true)
        {
            /* Set 'object_name' attribute of Item Revision */
            T4_ITKCALL(iRetCode, T4_obj_set_str_attribute_val(ptItemRev[iZx], T4_ATTR_OBJECT_NAME, ppszInfo[3]));

            /* Set 't4_denotation_de' attribute of Item Revision */
            T4_ITKCALL(iRetCode, T4_obj_set_str_attribute_val(ptItemRev[iZx], T4_ATTR_DENOTATION_DE, ppszInfo[3]));
        }

        T4_ITKCALL(iRetCode, GRM_list_secondary_objects(ptItemRev[iZx], NULLTAG, &iSecCount, &ptSecObjects));

        for(int iMx = 0; iMx < iSecCount && iRetCode == ITK_ok; iMx++)
        {
            logical  bIsTypeOf = false;

            T4_ITKCALL(iRetCode, T4_obj_is_type_of(ptSecObjects[iMx].secondary, T4_CLASS_WORKSPACEOBJECT, &bIsTypeOf));
                
            if (bIsTypeOf == true)
            {
			    T4_ITKCALL(iRetCode, T4_calculate_and_set_obj_name(ptSecObjects[iMx].secondary, ppszInfo[0], ppszInfo[1], pszNewName));
            }
        }

        T4_MEM_TCFREE(ptSecObjects);
    }

    T4_MEM_TCFREE(pszNewName);
    return iRetCode;
}

/**
 * This function updates the 'object_name' of all secondary object that are attached to input Item.
 *
 * @param[in] tItem     Tag of Item
 * @param[in] iInfoCnt  Count of values that are collected after parsing a Line from Input file to this utility
 * @param[in] ppszInfo  List of parsed values. 
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_update_item_secondary_obj_name
(
	tag_t  tItem,
    int    iInfoCnt,
    char** ppszInfo
)
{
    int  iRetCode      = ITK_ok;
	int  iSecCount     = 0;
    GRM_relation_t* ptSecObjects = NULL;

    T4_ITKCALL(iRetCode, GRM_list_secondary_objects(tItem, NULLTAG, &iSecCount, &ptSecObjects));

    for(int iMx = 0; iMx < iSecCount && iRetCode == ITK_ok; iMx++)
    {
        logical  bIsTypeOf = false;

        T4_ITKCALL(iRetCode, T4_obj_is_type_of(ptSecObjects[iMx].secondary, T4_CLASS_WORKSPACEOBJECT, &bIsTypeOf));
            
        if (bIsTypeOf == true)
        {
		    T4_ITKCALL(iRetCode, T4_calculate_and_set_obj_name(ptSecObjects[iMx].secondary, ppszInfo[0], ppszInfo[1], NULL));
        }
    }

    T4_MEM_TCFREE(ptSecObjects);
    return iRetCode;
}

/**
 * This function updates the 'object_name' of BOM View and BOM-View revision attached to input Item and Item revision
 *
 * @param[in] iInfoCnt  Count of values that are collected after parsing a Line from Input file to this utility
 * @param[in] ppszInfo  List of parsed values. 
 * @param[in] tItem     Tag of Item
 * @param[in] iIRCount  Count of Item Revision
 * @param[in] ptItemRev List of Item Revision
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_update_bv_and_bvr_name
(
    int    iInfoCnt,
    char** ppszInfo,
	tag_t  tItem,
	int    iIRCount, 
	tag_t* ptItemRev
)
{
    int    iRetCode     = ITK_ok;
    int    iBomCount    = 0;
    tag_t* ptBomViews   = NULL;

	T4_ITKCALL(iRetCode, ITEM_list_bom_views(tItem, &iBomCount, &ptBomViews));

	for(int iDx = 0; iDx < iBomCount && iRetCode == ITK_ok; iDx++)
	{
		T4_ITKCALL(iRetCode, T4_calculate_and_set_obj_name(ptBomViews[iDx], ppszInfo[0], ppszInfo[1], NULL));
	}
		
	for(int iJx = 0; iJx < iIRCount && iRetCode == ITK_ok; iJx++)
	{
		int    iBVRCount    = 0;
		tag_t* ptBVR        = NULL;

		/* Get all the connected bom view to item revision */
		T4_ITKCALL(iRetCode, ITEM_rev_list_bom_view_revs(ptItemRev[iJx], &iBVRCount, &ptBVR));

		for(int iNx = 0; iNx < iBVRCount && iRetCode == ITK_ok; iNx++)
		{
			T4_ITKCALL(iRetCode, T4_calculate_and_set_obj_name(ptBVR[iNx], ppszInfo[0], ppszInfo[1], NULL));
		}

		T4_MEM_TCFREE(ptBVR);
	}

	T4_MEM_TCFREE(ptBomViews);
    return iRetCode;
}

/**
 * This function change the 'object_name' prefix of input TC Business object to new Item ID prefix.
 *
 * @param[in] tObject       Tag of TC Business object
 * @param[in] pszOldID      Old Item ID 
 * @param[in] pszNewID      New Item ID
 * @param[in] pszNewObjName New Object Name
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_calculate_and_set_obj_name
(
	tag_t  tObject,
    char*  pszOldID,
	char*  pszNewID,
    char*  pszNewObjName
)
{
	int    iRetCode     = ITK_ok;
	int    iOldStrLen   = 0;
	int    iNewStrLen   = 0;
	char*  pszObjName   = NULL;

    iOldStrLen = (int)tc_strlen(pszOldID);
    iNewStrLen = (int)tc_strlen(pszNewID);

	T4_ITKCALL(iRetCode, AOM_ask_value_string(tObject, T4_ATTR_OBJECT_NAME, &pszObjName));
   
    if(tc_strcmp(pszOldID, pszNewID) != 0)
    {
	    if(tc_strncasecmp(pszObjName, pszOldID, iOldStrLen) == 0)
	    {
		    int     iTempLen    = 0;
		    char*   pszEndStr   = NULL;
		    char*   pszNewName  = NULL;
            logical bIsTypeOfDS = false;

		    T4_ITKCALL(iRetCode, T4_str_copy_substring(pszObjName, iOldStrLen+1, (int)(tc_strlen(pszObjName)), &pszEndStr));

		    if(pszEndStr == NULL)
		    {
			    iTempLen = iNewStrLen+ 1;
		    }
		    else
		    {
			    iTempLen = (int)(iNewStrLen+ tc_strlen(pszEndStr)+ 1);
		    }			

		    pszNewName = (char*) MEM_alloc(sizeof(char)*(int)(iTempLen));

		    tc_strcpy(pszNewName, pszNewID);

		    if(pszEndStr != NULL)
		    {
			    tc_strcat(pszNewName, pszEndStr);
			    tc_strcat(pszNewName, '\0'); 
		    }

            T4_ITKCALL(iRetCode, T4_obj_is_type_of(tObject, T4_CLASS_DATASET, &bIsTypeOfDS));

            if(bIsTypeOfDS == true)
            {
                logical bIsIdeasDS  = false;

                T4_ITKCALL(iRetCode, T4_obj_is_type_of(tObject, T4_CLASS_IDEAS_DRAWING, &bIsIdeasDS));

                if(bIsIdeasDS == true)
                {
                    /* This check is deliberately done individually. */
                    if(tc_strcmp(pszNewObjName, pszObjName) != 0)
                    {
                        T4_ITKCALL(iRetCode, T4_obj_set_ds_str_attr_without_version_change(tObject, T4_ATTR_OBJECT_NAME, pszNewObjName));
                    }
                }
                else
                {
                    /* This check is deliberately done individually. */
                    if(tc_strcmp(pszNewName, pszObjName) != 0)
                    {
                        T4_ITKCALL(iRetCode, T4_obj_set_ds_str_attr_without_version_change(tObject, T4_ATTR_OBJECT_NAME, pszNewName));
                    }
                }
            }
            else
            {   /* This check is deliberately done individually. */
                if(tc_strcmp(pszNewName, pszObjName) != 0)
                {
                    T4_ITKCALL(iRetCode, T4_obj_set_str_attribute_val(tObject, T4_ATTR_OBJECT_NAME, pszNewName));
                }
            }

		    T4_MEM_TCFREE(pszEndStr);
		    T4_MEM_TCFREE(pszNewName);
	    }
    }
    else
    {
        if(pszNewObjName != NULL)
        {
            logical bIsIdeasDS  = false;

            T4_ITKCALL(iRetCode, T4_obj_is_type_of(tObject, T4_CLASS_IDEAS_DRAWING, &bIsIdeasDS));

            if(bIsIdeasDS == true)
            {
                T4_ITKCALL(iRetCode, T4_set_ideas_ds_info(tObject, pszObjName, pszNewObjName));               
            }
        }
    }

	T4_MEM_TCFREE(pszObjName);
	return iRetCode;
}

/**
 * This function sets the custom attribute for all Item of type 'T4_Item'.
 *
 * @param[in] tItem     Tag Item
 * @param[in] pszItemID Item ID 
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_set_custom_attribute
(
	tag_t  tItem,
	char*  pszItemID
)
{
	int     iRetCode   = ITK_ok;

	if(pszItemID != NULL)
	{
		logical bIsOfInputType = false;

		T4_ITKCALL(iRetCode, T4_obj_is_type_of(tItem, T4_CLASS_T4ITEM, &bIsOfInputType));

		if(bIsOfInputType == true)
		{
			int    iParsCnt  = 0;
			char** ppszInfo  = NULL;

			T4_ITKCALL(iRetCode, T4_arg_parse_multipleVal(pszItemID, T4_CHARACTER_DASH, &iParsCnt, &ppszInfo));

			if(iParsCnt > 1)
			{
				char*   pszDigitalPart    = NULL;
				char*   pszAlphaPart      = NULL;
				logical bIsDigital        = false;
				logical bIsAlpha          = false;
				logical bIsDigitalAlpha   = false;
				logical bIsAlphaDigital   = false;
				logical bWrongStr         = false;

				T4_ITKCALL(iRetCode, T4_obj_set_str_attribute_val(tItem, T4_ATTR_IDENT_NUMBER, ppszInfo[0]));

				T4_ITKCALL(iRetCode, T4_str_check_digital_alpha_type(ppszInfo[1], &pszDigitalPart, &pszAlphaPart, &bIsDigital,
																		&bIsAlpha, &bIsDigitalAlpha, &bIsAlphaDigital, &bWrongStr));

				if(bIsAlphaDigital == true)
				{
					T4_ITKCALL(iRetCode, T4_obj_set_str_attribute_val(tItem, T4_ATTR_IDENT_EXT, pszAlphaPart));

					T4_ITKCALL(iRetCode, T4_obj_set_str_attribute_val(tItem, T4_ATTR_IDENT_EXT_COUNTER, pszDigitalPart));
				}

				T4_MEM_TCFREE(pszDigitalPart)
				T4_MEM_TCFREE(pszAlphaPart);
			}

			T4_MEM_TCFREE(ppszInfo);
		}
	}

	return iRetCode;
}

/**
 * This function sets the IdeasDrawing and related Image dataset names. IdeasDrawing can have Image dataset attached with relation 'IMAN_capture'
 *
 * @param[in] tDataset         Tag Dataset
 * @param[in] pszCurrentDSName Current name of dataset
 * @param[in] pszDSNewName     Dataset New name
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_set_ideas_ds_info
(
    tag_t  tDataset,
    char*  pszCurrentDSName,
	char*  pszDSNewName
)
{
    int    iRetCode     = ITK_ok;
    int    iImgDSCnt    = 0;
    char*  pszPostfix   = NULL;
    char*  pszNameToSet = NULL;
    tag_t* ptImageDS    = NULL;

    T4_get_str_after_delimiter_from_end(pszCurrentDSName, T4_CHARACTER_FORWARD_SLASH, &pszPostfix);

    pszNameToSet = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(pszPostfix) + tc_strlen(pszDSNewName) +1)));

    tc_strcpy(pszNameToSet, pszDSNewName);
    tc_strcat(pszNameToSet, pszPostfix);
    tc_strcat(pszNameToSet, '\0');

    if(tc_strcmp(pszNameToSet, pszCurrentDSName) != 0)
    {
        /* Set 'object_name' attribute of Dataset */
        T4_ITKCALL(iRetCode, T4_obj_set_ds_str_attr_without_version_change(tDataset, T4_ATTR_OBJECT_NAME, pszNameToSet));
    }

    T4_ITKCALL(iRetCode, T4_grm_get_secondary_obj_of_type(tDataset, T4_IMAN_CAPTURE_REL, T4_CLASS_IMAGE, &ptImageDS, &iImgDSCnt));

    for(int iDx = 0; iDx < iImgDSCnt && iRetCode == ITK_ok; iDx++)
    {
        char* pszCurrentName = NULL;

        T4_ITKCALL(iRetCode, AOM_ask_value_string(ptImageDS[iDx], T4_ATTR_OBJECT_NAME, &pszCurrentName));

        if(tc_strcmp(pszCurrentName, pszNameToSet) != 0)
        {
            /* Set 'object_name' attribute of Dataset */
            T4_ITKCALL(iRetCode, T4_obj_set_ds_str_attr_without_version_change(ptImageDS[iDx], T4_ATTR_OBJECT_NAME, pszNameToSet));    
        }

        T4_MEM_TCFREE(pszCurrentName);
    }

    T4_MEM_TCFREE(pszPostfix);
    T4_MEM_TCFREE(pszNameToSet);
    T4_MEM_TCFREE(ptImageDS);
	return iRetCode;
}


#ifdef __cplusplus
}
#endif


