/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_update_item_id.hxx

    Description:  Header File containing declaration

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
19-May-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_UPDATE_ITEM_ID_HXX
#define T4_UPDATE_ITEM_ID_HXX

#include <t4_library.hxx>
#include <t4_errors.hxx>

struct sIRStatusInfo
{
    int     iStatusCnt;
    tag_t   tItemRev;
    tag_t*  ptIRStatus;
};

#define T4_REPORT_STATUS_CUT_ERROR(iRetCode, fLogFile, fRepeatFile, gszLine, pszObjString){\
                                            char* pszErrorText = NOVALUE;\
                                            EMH_ask_error_text(iRetCode, &pszErrorText);\
                                            fprintf(fLogFile, "Error: Cannot cut status from '%s'.\n  %s.\n", pszObjString, pszErrorText);\
                                            fflush(fLogFile);\
                                            fprintf(fRepeatFile, "%s\n", gszLine);\
                                            fflush(fRepeatFile);\
                                            T4_MEM_TCFREE(pszErrorText);\
}

#define T4_ERROR_UNABLE_TO_PROCESS_ITEM(iRetCode, fLogFile, fRepeatFile, gszLine, pszItemID){\
                                            char* pszErrorText = NOVALUE;\
                                            EMH_ask_error_text(iRetCode, &pszErrorText);\
                                            fprintf(fLogFile, "Error: Unable to process Item with ID %s'.\n   %s.\n", pszItemID, pszErrorText);\
                                            fflush(fLogFile);\
                                            fprintf(fRepeatFile, "%s\n", gszLine);\
                                            fflush(fRepeatFile);\
                                            T4_MEM_TCFREE(pszErrorText);\
}



#define T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist){\
                                if((fRepeatFile = fopen(pszRepeatFile, "w")) == NULL ){\
                                    printf("\n** ERROR: Cannot create %s for writing unprocessed contents **\n\n",pszRepeatFile);}\
                                else{\
                                bRepeatFileExist = true;}\
}

#define T4_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, gszLine){\
                            char* pszErrorText = NOVALUE;\
                            EMH_ask_error_text(iRetCode, &pszErrorText);\
                            fprintf(fLogFile, "Error: %s.\n", pszErrorText);\
                            fflush(fLogFile);\
                            fprintf(fRepeatFile, "%s\n", gszLine);\
                            fflush(fRepeatFile);\
                            T4_MEM_TCFREE(pszErrorText);\
}

int T4_process_itemid_update
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    logical bByPass,
    logical bNOModify
); 

int T4_find_item
( 
    char*   pszItemID, 
    char*   pszType, 
    int*    piItemCnt, 
    tag_t** pptItem
);

int T4_update_ir_secondary_obj_name
(
    int     iInfoCnt,
    char**  ppszInfo,
    int     iIRCount,
    tag_t*  ptItemRev
);

int T4_update_item_secondary_obj_name
(
	tag_t  tItem,
    int    iInfoCnt,
    char** ppszInfo
);

int T4_update_bv_and_bvr_name
(
    int    iInfoCnt,
    char** ppszInfo,
	tag_t  tItem,
	int    iIRCount, 
	tag_t* ptItemRev
);

int T4_calculate_and_set_obj_name
(
	tag_t  tObject,
    char*  pszOldID,
	char*  pszNewID,
    char*  pszNewObjName
);

int T4_set_custom_attribute
(
	tag_t  tItem,
	char*  pszItemID
);

int T4_set_ideas_ds_info
(
    tag_t  tDataset,
    char*  pszCurrentObjName,
	char*  pszDSNewName
);

#endif






