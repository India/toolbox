/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_object_utilities.cxx

    Description: This file contains function performing various operations on Teamcenter Business Objects

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
21-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <t4_errors.hxx>
#include <t4_library.hxx>

/**
 * This function gets the current date time.
 *
 * @param[in]  pszFormat   Expected date time format.
 * @param[out] szDateTime  current Date time in required format.
 *
 * @note 'szDateTime' must be a character array of at least [IMF_filename_size_c + 1] 
 *       characters 
 *
 * @retval ITK_ok on successful execution
 * @retval INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME if format resulted in
 *         resultant String length greater then IMF_filename_size_c + 1
 */
int T4_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
)
{
    int         iRetCode       = ITK_ok;
    int         iStrLength     = 0;
    struct tm   *clLocalTime   = NULL;
    time_t      tCurrentTime;

    /*Get system's local time*/
    tCurrentTime = time(NULL);
    clLocalTime  = localtime(&tCurrentTime);
        
    /*Get system time  in required format*/
    iStrLength =(int)( strftime( szDateTime, (IMF_filename_size_c + 1), pszFormat, clLocalTime));
   
      
    return iRetCode;
}

/**
 * Set the Tag attribute value.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] tPropVal        Value of the property to be set
 *
 * @note The object must be an instance of Application Object class or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int T4_obj_set_tag_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    tag_t       tPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    T4_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute string value.
     */    
    T4_ITKCALL(iRetCode, AOM_set_value_tag(tObject, pszPropertyName, tPropVal));

    /*  Save the object.  */
    T4_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     T4_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}

/**
 * Set the string attribute value.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] pszPropVal      Value of the property to be set
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int T4_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    T4_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute string value.
     */    
    T4_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszPropertyName, pszPropVal));

    /*  Save the object.  */
    T4_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     T4_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}

/**
 * Parses a string containing values seperated by a given separator
 * into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in] pszList          Input list to be parsed
 * @param[in] cSep             Separator character
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by T4_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 * @retval INCORRECT_INPUT If list or seprator is Null.
 * @retval MEMORY_ALLOCATION_FAILED If cannot allocate memory.
 */
int T4_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
)
{
    int        iRetCode    = ITK_ok;
    int        iCounter    = 0;
    char*      pszToken    = "";
    char*      pszTempList = NULL;

    /* Convert separator (char) to char* - strtok(char* input, Char* sep) */
    char pszSeparator[T4_SEPARATOR_LENGTH+1];
    pszSeparator[0] = cSep;
    pszSeparator[1] = '\0';

    /* Validate input */
    if((pszList == NULL) || (cSep == '\0'))
    {
        TC_write_syslog( "ERROR: Missing input data:List passed or seprator is null\n");
    }
    else
    {
        /* Allocating memory to pszTempList */
        pszTempList  = (char*) MEM_alloc(sizeof(char)*((int)tc_strlen(pszList)+1));
        if (pszTempList == NULL)
        {
            TC_write_syslog( "ERROR: Cannot allocate memory\n");
        }
        else
        {
            /* Copying the input string to Null terminated string */
            tc_strcpy(pszTempList, pszList);

            /* Get the first token */
            pszToken = tc_strtok( pszTempList,pszSeparator );

            *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
            /* Tokenize the input string */
            while( pszToken != NULL )
            {
                /* Allocate memory for each string pointer */
                (*pppszValuelist)[iCounter] = (char*) MEM_alloc(sizeof(char)*(((int)tc_strlen(pszToken))+1));
                
                /* Copy the token to the output string pointer */
                tc_strcpy((*pppszValuelist)[iCounter],pszToken);

                /* Count the number of elements in the string */
                iCounter++;
                
                /* Allocating memory to pointer of string (output) as per the number of tokens found */
                *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));
                
                /* Get next token: */
                pszToken = tc_strtok( NULL, pszSeparator );
               
            }

            if(iRetCode == ITK_ok)
            {
                *iCount = iCounter;
            }
        }  
    }

    /* Free the temporary memory used for tokenizing */
    T4_MEM_TCFREE(pszTempList);
    return iRetCode;
}

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int T4_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
	tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    T4_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    T4_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    T4_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}

/**
 * This function set or removes the owning project for the components associated with Item for e.g. 
 * BOM View Revision, Item Revision, Dataset etc.
 * This function also set or remove the 'owning_project' property for all the 'AssemblyArrangement' object that 
 * are attached to any BOM View Revision.
 * 
 * @param[in] tItem                Tag of Item whose associated objects needs to be assigned 'owning_project'
 * @param[in] tProject             Tag of project to be assigned
 * @param[in] pszSecObjClass       Name of Secondary Object Class which can be associated with Item revisions
 * @param[in] bApplySecInfoToItem  This argument decides if Owning Project needs to be set for Item revision only 
 *                                 or needs to be updated for Item, Item Revision and all its secondary object.
 * @param[in] pszAddOrRmvSecInfo   This argument decides if the Owning project needs to be set or removed. Valid 
 *                                 values are 'add' or 'remove'
 *
 * @return  ITK_ok on successful execution, else returns ITK error code.
 */
int T4_set_or_remove_owning_proj
(
    tag_t   tObjects,
    tag_t   tProject,
    char*   pszSecObjClass,
    logical bApplySecInfoToItem,
    char*   pszAddOrRmvSecInfo
)
{
    int    iRetCode       = ITK_ok;
    int    iRevCount      = 0;
    int    iSecObjCount   = 0;
    tag_t  tRelationType  = NULLTAG;
    tag_t* ptItemRev      = NULL;
    GRM_relation_t* ptSecObjects = NULL;

    if(bApplySecInfoToItem == true)
    {
        T4_ITKCALL(iRetCode, ITEM_list_all_revs(tObjects, &iRevCount, &ptItemRev));

        T4_ITKCALL(iRetCode, GRM_find_relation_type( T4_IMAN_MASTER_FORM, &tRelationType));

        T4_ITKCALL(iRetCode, GRM_list_secondary_objects(tObjects, tRelationType, &iSecObjCount, &ptSecObjects));

        if( iSecObjCount > 0 )
        {
            tag_t tTemp = NULLTAG;

            T4_ITKCALL(iRetCode, AOM_ask_value_tag(ptSecObjects[0].secondary, T4_ATTR_OWNING_PROJECT, &tTemp));

            if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0 && tTemp == tProject)
            {
                /* Set Owning Project attribute to NULLTAG. */
                T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptSecObjects[0].secondary, T4_ATTR_OWNING_PROJECT, NULLTAG));
            }
            else
            {
                if(tTemp != tProject)
                {
                    T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptSecObjects[0].secondary, T4_ATTR_OWNING_PROJECT, tProject));
                }
            }
        }
    }
    else
    {
        iRevCount = 1;
        ptItemRev = (tag_t*) MEM_alloc(sizeof(tag_t)*iRevCount);
        ptItemRev[0] = tObjects;
    }

    for(int iDx = 0; iDx < iRevCount && iRetCode == ITK_ok; iDx++)
    {
        int  iActiveSeq   = 0;
      
        T4_ITKCALL(iRetCode, AOM_ask_value_int(ptItemRev[iDx], T4_ATTR_ACTIVE_SEQ, &iActiveSeq));
	
		/* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0)
        {
            int    iBVRCount          = 0;
            int    iDSCnt             = 0;
            int    iObjCnt            = 0;
            int    iRelNameCnt        = 0;
            tag_t  tRelType           = NULLTAG;
            tag_t  tIROPrj            = NULLTAG;
            tag_t* ptDataset          = NULL;
            tag_t* ptBomViewRev       = NULL;                
            char** ppszRelTypeNames   = NULL;
            GRM_relation_t* ptObjects = NULL;

            /* We have to use relation IMAN_master_form for getting item revision master form. */
            T4_ITKCALL(iRetCode, GRM_find_relation_type( T4_IMAN_MASTER_FORM, &tRelType));

            T4_ITKCALL(iRetCode, GRM_list_secondary_objects(ptItemRev[iDx], tRelType, &iObjCnt, &ptObjects));

            if( iObjCnt > 0 )
            {
                tag_t tTemp = NULLTAG;

                T4_ITKCALL(iRetCode, AOM_ask_value_tag(ptObjects[0].secondary, T4_ATTR_OWNING_PROJECT, &tTemp));

                if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0 && tTemp == tProject)
                {
                    /* Set Owning Project attribute to NULLTAG. */
                    T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptObjects[0].secondary, T4_ATTR_OWNING_PROJECT, NULLTAG));
                }
                else
                {
                    if(tTemp != tProject)
                    {
                        T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptObjects[0].secondary, T4_ATTR_OWNING_PROJECT, tProject));
                    }
                }
            }

            T4_ITKCALL(iRetCode, AOM_ask_value_tag(ptItemRev[iDx], T4_ATTR_OWNING_PROJECT, &tIROPrj));

            if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0 && tIROPrj == tProject)
            {
                /* Set Owning Project attribute to NULLTAG. */
                T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptItemRev[iDx], T4_ATTR_OWNING_PROJECT, NULLTAG));
            }
            else
            {
                if(tIROPrj != tProject)
                {
                    T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptItemRev[iDx], T4_ATTR_OWNING_PROJECT, tProject));
                }
            }

            T4_ITKCALL(iRetCode, ITEM_rev_list_bom_view_revs(ptItemRev[iDx], &iBVRCount, &ptBomViewRev));

            for(int iYx = 0; iYx < iBVRCount && iRetCode == ITK_ok; iYx++)
            {
                int    iSecObjCnt = 0;
                tag_t* ptSecObj   = NULL;

                tag_t tBVROPrj = NULLTAG;

                T4_ITKCALL(iRetCode, AOM_ask_value_tag(ptBomViewRev[iYx], T4_ATTR_OWNING_PROJECT, &tBVROPrj));

                if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0 && tBVROPrj == tProject)
                {
                    /* Set Owning Project attribute to NULLTAG. */
                    T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptBomViewRev[iYx], T4_ATTR_OWNING_PROJECT, NULLTAG));
                }
                else
                {
                    if(tBVROPrj != tProject)
                    {
                        T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptBomViewRev[iYx], T4_ATTR_OWNING_PROJECT, tProject));
                    }
                }

                if(iRetCode == ITK_ok && iYx == 0)
                {
                    iRelNameCnt = 2;
                    ppszRelTypeNames = (char**) MEM_alloc(sizeof(char*)*iRelNameCnt);
                    ppszRelTypeNames[0] = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(T4_TC_DEFAULT_ARRANGEMENT)+1)));
                    ppszRelTypeNames[1] = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(T4_TC_ARRANGEMENT)+1)));
                    tc_strcpy(ppszRelTypeNames[0], T4_TC_DEFAULT_ARRANGEMENT);
                    tc_strcpy(ppszRelTypeNames[1], T4_TC_ARRANGEMENT);
                }

                /* Getting all secondary  'AssemblyArrangement' objects that are associated with BOM View Revision with
                       relation 'TC_DefaultArrangement' or 'TC_Arrangement'*/
                T4_ITKCALL(iRetCode, T4_grm_get_secondary_obj_of_type(ptBomViewRev[iYx], iRelNameCnt, ppszRelTypeNames,
                                                                        T4_CLASS_ASSEMBLY_ARRANGEMENT, &ptSecObj, &iSecObjCnt));

                for(int iMx = 0; iMx < iSecObjCnt && iRetCode == ITK_ok; iMx++)
                {
                    tag_t tSecObjProj = NULLTAG;

                    T4_ITKCALL(iRetCode, AOM_ask_value_tag(ptSecObj[iMx], T4_ATTR_OWNING_PROJECT, &tSecObjProj));

                    if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0 && tSecObjProj == tProject)
                    {
                        /* Set Owning Project attribute to NULLTAG. */
                        T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptSecObj[iMx], T4_ATTR_OWNING_PROJECT, NULLTAG));
                    }
                    else
                    {
                        if(tSecObjProj != tProject)
                        {
                            T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptSecObj[iMx], T4_ATTR_OWNING_PROJECT, tProject));
                        }
                    }
                }

                T4_MEM_TCFREE(ptSecObj);
            }

            T4_ITKCALL(iRetCode, T4_grm_get_all_secondary_obj_of_type(ptItemRev[iDx], pszSecObjClass, &ptDataset, &iDSCnt));

            for(int iZx = 0; iZx < iDSCnt && iRetCode == ITK_ok; iZx++)
            {
                tag_t tTemp = NULLTAG;

                T4_ITKCALL(iRetCode, AOM_ask_value_tag(ptDataset[iZx], T4_ATTR_OWNING_PROJECT, &tTemp));

                if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0 && tTemp == tProject)
                {
                    /* Set Owning Project attribute to NULLTAG. */
                    T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptDataset[iZx], T4_ATTR_OWNING_PROJECT, NULLTAG));
                }
                else
                {
                    if(tTemp != tProject)
                    {
                        T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptDataset[iZx], T4_ATTR_OWNING_PROJECT, tProject));
                    }
                }
            }

            T4_MEM_TCFREE(ptBomViewRev);
            T4_MEM_TCFREE(ptDataset);
            T4_MEM_TCFREE(ptObjects);
            T4_MEM_TCFREE(ppszRelTypeNames);
        }

    }

    T4_MEM_TCFREE(ptSecObjects);
    T4_MEM_TCFREE(ptItemRev);
    return iRetCode;
}

/**
 * Function to Strips leading and trailing space. 
 *
 * @param[in]  pszString  String to trim white spaces 
 * @param[out] Pointer to Trimmed string 
 */
char* T4_trim( char* pszString )
{
  if (pszString == NULL)
    return(NULL);

  T4_trim_end(pszString);

    /*===================================================================*/
    /* Skip over spaces, and return the address of the first non-space   */
    /* character. If there are no leading spaces, this just returns str. */
    /*===================================================================*/

  return &pszString[strspn( pszString, " \t" )];

} 

/**
 * Function to Strips all trailing spaces from the given null-terminated string
 *
 * @param[in/out] pszString  String to trim white spaces
 *
 */

void T4_trim_end(char *pszString)
{
  int iLen = (int)(tc_strlen( pszString ) - 1);

  while (iLen >= 0  &&  ((pszString[iLen] == ' ') || 
          (pszString[iLen] == '\t') || (pszString[iLen] == '\n')))
      --iLen;

  pszString[iLen + 1] = '\0';

}

#ifdef __cplusplus
}
#endif


