/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_apply_security_info.hxx

    Description:  Header File containing declaration

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
21-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_APPLY_SECURITY_INFO_HXX
#define T4_APPLY_SECURITY_INFO_HXX

#include <t4_library.hxx>
#include <t4_errors.hxx>

#define T4_REPORT_SECURITY_ASSIGN_REMOVE_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszItemID, pszRevID, pszAttrName, pszAttrVal, pszAddOrRmvSecInfo){\
                                                    char* pszErrorText = NULL;\
                                                    EMH_ask_error_text(iRetCode, &pszErrorText);\
                                                    if(pszRevID == NULL){\
                                                        if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0){\
                                                                fprintf(fLogFile, "Error: Cannot remove '%s' security information from Item with ID '%s'.(Security Attribute:'%s').\n  %s.\n", pszAttrVal, pszItemID, pszAttrName, pszErrorText);\
                                                        }\
                                                        else{\
                                                                fprintf(fLogFile, "Error: Item with ID '%s' cannot be assigned '%s' security information.(Security Attribute:'%s').\n  %s.\n", pszItemID, pszAttrVal, pszAttrName, pszErrorText);\
                                                        }\
                                                    }\
                                                    else{\
                                                        if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0){\
                                                                fprintf(fLogFile, "Error: Cannot remove '%s' security information from Item Revision '%s/%s'.(Security Attribute:'%s').\n  %s.\n", pszAttrVal, pszItemID, pszRevID, pszAttrName, pszErrorText);\
                                                        }\
                                                        else{\
                                                                fprintf(fLogFile, "Error: Item Revision '%s/%s' cannot be assigned '%s' security information.(Security Attribute:'%s').\n  %s.\n", pszItemID, pszRevID, pszAttrVal, pszAttrName, pszErrorText);\
                                                        }\
                                                    }\
                                                    fflush(fLogFile);\
                                                    fprintf(fRepeatFile, "%s\n", szLine);\
                                                    fflush(fRepeatFile);\
                                                    T4_MEM_TCFREE(pszErrorText);\
}

#define T4_REPORT_SET_ATTR_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, iNoOfItem, pszItemID, pszRevID, pszAttrName){\
                                        char* pszErrorText = NULL;\
                                        EMH_ask_error_text(iRetCode, &pszErrorText);\
                                        if(pszRevID == NULL){\
                                            fprintf(fLogFile, "Error: Cannot set property '%s' of Item. %d Item exist in the system with ID %s'.\n   %s.\n", pszAttrName, iNoOfItem,  pszItemID, pszErrorText);\
                                        }\
                                        else{\
                                            fprintf(fLogFile, "Error: Cannot set property '%s' of Item Revision. %d Item revision '%s/%s' exist in the system.\n  %s.\n", pszAttrName, iNoOfItem,  pszItemID, pszRevID, pszErrorText);\
                                        }\
                                        fflush(fLogFile);\
                                        fprintf(fRepeatFile, "%s\n", szLine);\
                                        fflush(fRepeatFile);\
                                        T4_MEM_TCFREE(pszErrorText);\
}

#define T4_REPORT_INCORRECT_SECURITY_ATTR(iRetCode, fLogFile, fRepeatFile, szLine, pszItemID, pszRevID, pszAttrName){\
                                                if(pszRevID == NULL){\
                                                    fprintf(fLogFile, "Error: '%s' is not a valid security attribute for Item %s.\n", pszAttrName, pszItemID);\
                                                }\
                                                else{\
                                                    fprintf(fLogFile, "Error: '%s' is not a valid security attribute for Item Revision '%s/%s'.\n", pszAttrName, pszItemID, pszRevID);\
                                                }\
                                                fflush(fLogFile);\
                                                fprintf(fRepeatFile, "%s\n", szLine);\
                                                fflush(fRepeatFile);\
}

#define T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist){\
                                if((fRepeatFile = fopen(pszRepeatFile, "w")) == NULL ){\
                                    printf("\n** ERROR: Cannot create %s for writing unprocessed contents **\n\n",pszRepeatFile);}\
                                else{\
                                bRepeatFileExist = true;}\
}

#define T4_REPORT_OBJ_NOT_FOUND_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszItemID, pszRevID){\
                                            if(pszRevID == NULL){\
                                                fprintf(fLogFile, "Error: Cannot find Item with ID '%s'.\n", pszItemID);\
                                            }\
                                            else{\
                                                fprintf(fLogFile, "Error: Cannot find Item Revision '%s/%s'.\n", pszItemID, pszRevID);\
                                            }\
                                            fflush(fLogFile);\
                                            fprintf(fRepeatFile, "%s\n", szLine);\
                                            fflush(fRepeatFile);\
}

#define T4_REPORT_UNABLE_TO_PARSE_LINE_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszConfigFileInfo){\
                                                    fprintf(fLogFile, "Error: Cannot Parse the line \n \t'%s' of file %s.\n", szLine, pszConfigFileInfo);\
                                                    fflush(fLogFile);\
                                                    fprintf(fRepeatFile, "%s\n", szLine);\
                                                    fflush(fRepeatFile);\
}

#define T4_REPORT_SUCCESS(iRetCode, fLogFile, fRepeatFile, szLine, pszItemID, pszRevID, pszAttrName, pszAttrVal, pszAddOrRmvSecInfo){\
                                if(pszRevID == NULL){\
                                        if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0){\
                                                fprintf(fLogFile, "Success: Security Information '%s' removed from Item with ID '%s'.(Security Attribute:'%s')\n", pszAttrVal, pszItemID,  pszAttrName);\
                                        }\
                                        else{\
                                                fprintf(fLogFile, "Success: Security Information '%s' assigned to Item with ID '%s'.(Security Attribute:'%s')\n", pszAttrVal, pszItemID,  pszAttrName);\
                                        }\
                                }\
                                else{\
                                        if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0){\
                                                fprintf(fLogFile, "Success: Security Information '%s' removed from Item Revision '%s/%s'.(Security Attribute:'%s')\n", pszAttrValue, pszItemID, pszRevID, pszAttrName);\
                                        }\
                                        else{\
                                                fprintf(fLogFile, "Success: Security Information '%s' assigned to Item Revision '%s/%s'.(Security Attribute:'%s')\n", pszAttrValue, pszItemID, pszRevID, pszAttrName);\
                                        }\
                                }\
                                fflush(fLogFile);\
}


int T4_process_security_assignment
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    char*   pszAddOrRmvSecInfo,
    logical bByPass,
    logical bNOModify
); 

int T4_extract_info
(
    char*   pszLine,
    char*   pszSeparator,
    char**  ppszItemID,
    char**  ppszRev,
    char**  ppszAttrKey,
    char**  ppszAttrValue
);

#endif






