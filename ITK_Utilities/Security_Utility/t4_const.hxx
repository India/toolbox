/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_const.hxx

    Description:  Header File for constants used during scope of the executable.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
21-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_CONST_HXX
#define T4_CONST_HXX

#define MAX_CHARS                                               300

/* Utility Argument Names/Values */
#define T4_ARG_VAL_ADD_MODE                                     "add"
#define T4_ARG_VAL_REMOVE_MODE                                  "remove"

/* Date Time Format */
#define T4_DATE_TIME_FORMAT_FILENAME                            "%y%m%d%H%M%S"

/* Attributes  */
#define T4_ATTR_ITEM_ID                                         "item_id"
#define T4_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define T4_ATTR_OWNING_PROJECT                                  "owning_project"
#define T4_ATTR_LICENSE_LIST                                    "license_list"
#define T4_ATTR_IP_CLASSIFICATION                               "ip_classification"
#define T4_ATTR_ACTIVE_SEQ                                      "active_seq"

/* Relation    */
#define T4_IMAN_MASTER_FORM                                     "IMAN_master_form"
#define T4_TC_DEFAULT_ARRANGEMENT                               "TC_DefaultArrangement"
#define T4_TC_ARRANGEMENT                                       "TC_Arrangement"

/* Class Name */
#define T4_CLASS_DATASET                                        "Dataset"
#define T4_CLASS_ASSEMBLY_ARRANGEMENT                           "AssemblyArrangement"

/* Propagation Information */
#define T4_CONST_PROJECT                                        "Project"
#define T4_CONST_LICENSE                                        "License"
#define T4_CONST_IP_CLASSIFICATION                              "IPClassification"
#define T4_CONST_OWNING_PROJECT                                 "OwningProject"

/* Separators */
#define T4_STRING_SINGLE_SPACE								    " "
#define T4_STRING_DEFAULT_SEPARATOR	      					    "~"
#define T4_CHARACTER_COLON                                      ':'



#define DEBUG                                                   "true"

#define T4_SEPARATOR_LENGTH                                     1
#define T4_NUMERIC_ERROR_LENGTH                                 12


#endif


