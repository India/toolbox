/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_apply_security_info.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <t4_errors.hxx>
#include <t4_library.hxx>
#include <t4_apply_security_info.hxx>

/**
 * The utility reads that input file that has the following format: 
 *      ItemID~AttributeKey~AttributeValue 
 *          or
 *      ItemID;Rev;AttributeKey~AttributeValue
 *
 * As a default separator "~" is used (if not specified). As AttributeKey "Project", "License", "IP_Classification" or "Owning_Project" is valid.
 * Only one attribute can be processed per input line. In case of a project the project ID has to be supplied as a value.
 *
 * @note: If 'AttributeKey' 'Owning_Project' is supplied to project is also applied and property 'owning_project' is also set
 *
 * Utility can also apply or remove security information to Item or Item revision only.
 * We need to provide the revision ID as part of input file. The file header will appear as mentioned below.
 *          ItemID;Rev;AttrKey;AttrValue
 * If "Rev" is provided then security information is assign to or removed from a particular revision. If "Rev" is not provided then security 
 * information is assigned to or removed from an item. Project is not assigned to or removed from related secondary objects of Item or Item
 * revision because this can be configured through Propagation Rules
 *
 * @note:Utility can be executed using following additional options:
 *       -mode
 *            As a mode "add" or "remove" can be used. Add will add specified information to the objects and remove will remove this
 *            particular information from the object. If this option is not given, "add" will  be the default.
 *       -no_mod
 *            If this argument is provided the last modification date and last modification user will not change.
 *       -bypass
 *            Run this utility in bypass mode.
 *
 * @param[in] pszConfigFileInfo  Name of the Configuration File to be processed
 * @param[in] pszLogFile         Name of Log file
 * @param[in] pszRepeatFile      Name of the Repeat File
 * @param[in] pszSeparator       Separator used in Configuration File 
 * @param[in] pszAddOrRmvSecInfo If 'add' then security info will be applied to the objects specified in the input file else
 *                               If 'remove' then security info will be removed.
 * @param[in] bByPass            'true' if By Pass is set else 'false'
 * @param[in] bNOModify          If 'true' then last modification date and user of the objects will not to be changed
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_process_security_assignment
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    char*   pszAddOrRmvSecInfo,
    logical bByPass,
    logical bNOModify
)
{
    int     iRetCode              = ITK_ok;
    char    szLine[MAX_CHARS]     = {'\0'};
    logical bRepeatFileExist      = false;
    FILE*   fCfgFile;
    FILE*   fLogFile;
    FILE*   fRepeatFile;
   
    if ((fCfgFile = fopen(pszConfigFileInfo, "r")) == NULL )
    {
        printf("\n** ERROR: Cannot open %s for reading - exiting **\n\n", pszConfigFileInfo);
        return 0;
    }

    if ((fLogFile = fopen(pszLogFile, "w")) == NULL )
    {
        printf("\n** ERROR: Cannot create %s for writing log  **\n\n",pszLogFile);
    }
    else
    {
        if(bByPass == true || bNOModify == true)
        {
            if(bNOModify == true)
            {
                /*-------------------------------------*/
                /* Do not update date fields on change */
                /*-------------------------------------*/
                T4_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_attr_update, false, 0,0,0, NULL));
        
                /*----------------------*/
                /* Bypass access checks */
                /*----------------------*/
                T4_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_access_check, true, 0,0,0, NULL));
            }

            T4_ITKCALL(iRetCode, ITK_set_bypass(true));
        }

        while (fgets(szLine, MAX_CHARS, fCfgFile) != NULL)
        {
            if ((szLine[0] != '#') && (szLine[0] != '\0') && (szLine[0] != '\n'))
            {
                int     iLen                 = 0;
                char*   pszItemID            = NULL;
                char*   pszRev               = NULL;
                char*   pszAttrKey           = NULL;
                char*   pszAttrValue         = NULL;
                logical bApplySecInfoToItem  = false;

                /*================================================*/
                /* Trim the line to remove leading and trailing   */
                /* spaces and discard the \n on the end.          */
                /*================================================*/
                tc_strcpy(szLine, T4_trim(szLine));

                T4_ITKCALL(iRetCode , T4_extract_info(szLine, pszSeparator, &pszItemID, &pszRev, &pszAttrKey, &pszAttrValue));

                if(pszRev == NULL)
                {
                    bApplySecInfoToItem = true;
                }
                else
                {
                     bApplySecInfoToItem = false;
                }

                if(pszItemID != NULL)
                {
                    int     iNoOfObj           = 0;
                    tag_t   tProject           = NULLTAG;
                    tag_t*  ptObjects          = NULL;
                    logical bProcessProj       = false;
                    logical bProcessOwningProj = false;
                    logical bProcessLic        = false;
                    logical bProcessIPClass    = false;

                    if(pszRev == NULL)
                    {
                        T4_ITKCALL(iRetCode, ITEM_find(pszItemID, &iNoOfObj, &ptObjects ));
                    }
                    else
                    {
                        char* pszAttrName = NULL;

                        pszAttrName = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(T4_ATTR_ITEM_ID) +1)));
                        tc_strcpy(pszAttrName, T4_ATTR_ITEM_ID);

                        T4_ITKCALL(iRetCode, ITEM_find_item_revs_by_key_attributes(1, (const char**) &pszAttrName, (const char**) &pszItemID, pszRev,
                                                                                         &iNoOfObj, &ptObjects));
                        T4_MEM_TCFREE(pszAttrName);
                    }

                    if(iNoOfObj == 0)
                    {
                        /* Open repeat file */
                        if(bRepeatFileExist == false)
                        {
                            T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                        }
       
                        T4_REPORT_OBJ_NOT_FOUND_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszItemID, pszRev)
                    }
                    else
                    {
                        if(tc_strcmp(T4_CONST_OWNING_PROJECT, pszAttrKey) == 0 || tc_strcmp(T4_CONST_PROJECT, pszAttrKey) == 0)
                        {
                            bProcessProj = true;
                          
                            if(tc_strcmp(T4_CONST_OWNING_PROJECT, pszAttrKey) == 0 )
                            {
                                bProcessOwningProj = true;
                            }
                        }
                        else
                        {
                            if(tc_strcmp(T4_CONST_LICENSE, pszAttrKey) == 0)
                            {
                                bProcessLic = true;
                            }
                            else
                            {
                                if(tc_strcmp(T4_CONST_IP_CLASSIFICATION, pszAttrKey) == 0)
                                {
                                    bProcessIPClass = true;
                                }
                            }
                        }

                        if(bProcessProj == true)
                        {
                            T4_ITKCALL(iRetCode, PROJ_find ( pszAttrValue, &tProject )); 	

                            if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0)
                            {
                                T4_ITKCALL(iRetCode, PROJ_remove_objects(1, &tProject, iNoOfObj, ptObjects));
                            }
                            else
                            {
                                T4_ITKCALL(iRetCode, PROJ_assign_objects(1, &tProject, iNoOfObj, ptObjects));
                            }

                            if(iRetCode != ITK_ok)
                            {
                                /* Open repeat file */
                                if(bRepeatFileExist == false)
                                {
                                    T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                }
                                /* Report Error */
                                T4_REPORT_SECURITY_ASSIGN_REMOVE_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszItemID, pszRev, pszAttrKey, pszAttrValue, pszAddOrRmvSecInfo);                                  
                            }
                        }

                        if(bProcessIPClass == true || bProcessLic == true || bProcessProj == true)
                        {
                            for(int iZx = 0; iZx < iNoOfObj &&  iRetCode == ITK_ok; iZx++)
                            {   
                                if(bProcessOwningProj == true)
                                {
                                    /* Do not set Owning Project attribute for Item and BOM View if load file contains revision ID of Item */
                                    if(bApplySecInfoToItem == true)
                                    {
                                        int    iBVCount  = 0;
                                        tag_t  tTemp     = NULLTAG;
                                        tag_t* ptBomView = NULL;

                                        if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0)
                                        {
                                            /* Set Owning Project attribute for Item to NULLTAG. */
                                            T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptObjects[iZx], T4_ATTR_OWNING_PROJECT, NULLTAG));
                                        }
                                        else
                                        {
                                            T4_ITKCALL(iRetCode, AOM_ask_value_tag(ptObjects[iZx], T4_ATTR_OWNING_PROJECT, &tTemp));
                                            if(tTemp != tProject)
                                            {
                                                /* Set Owning Project attribute for Item. */
                                                T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptObjects[iZx], T4_ATTR_OWNING_PROJECT, tProject));
                                            }
                                        }

                                        /* Set Owning Project attribute for BOM View . */
                                        T4_ITKCALL(iRetCode, ITEM_list_bom_views(ptObjects[iZx], &iBVCount, &ptBomView));

                                        for(int iHx = 0; iHx < iBVCount && iRetCode == ITK_ok; iHx++)
                                        {
                                            if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0)
                                            {
                                                /* Set Owning Project attribute for Item to NULLTAG. */
                                                T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptBomView[iHx], T4_ATTR_OWNING_PROJECT, NULLTAG));
                                            }
                                            else
                                            {
                                                tag_t tTemp2 = NULLTAG;

                                                T4_ITKCALL(iRetCode, AOM_ask_value_tag(ptBomView[iHx], T4_ATTR_OWNING_PROJECT, &tTemp2));
                                               
                                                if(tTemp2 != tProject)
                                                {
                                                    T4_ITKCALL(iRetCode, T4_obj_set_tag_attribute_val(ptBomView[iHx], T4_ATTR_OWNING_PROJECT, tProject));
                                                }
                                            }
                                        }

                                        T4_MEM_TCFREE(ptBomView);
                                    }

                                    /* Set or remove Owning Project attribute for the following business objects:
                                       Item Revision, Dataset, BOM View Revision, Item Master Form and Item Revision Master form.
                                    */
                                    T4_ITKCALL(iRetCode, T4_set_or_remove_owning_proj(ptObjects[iZx], tProject, T4_CLASS_DATASET, bApplySecInfoToItem, pszAddOrRmvSecInfo));

                                    if(iRetCode != ITK_ok)
                                    {
                                        /* Open repeat file  */
                                        if(bRepeatFileExist == false)
                                        {
                                            T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                        }
                                        /* Report Error */
                                        T4_REPORT_SET_ATTR_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, iNoOfObj, pszItemID, pszRev, pszAttrKey);                                  
                                    }
                                }
                                else if(bProcessLic == true)
                                {
                                    tag_t  tLicense  = NULLTAG;
                                     	
                                    T4_ITKCALL(iRetCode, ADA_find_license(pszAttrValue, &tLicense) );

                                    if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0)
                                    {
                                        T4_ITKCALL(iRetCode, ADA_remove_license_object(tLicense, ptObjects[iZx]));
                                    }
                                    else
                                    {
                                        T4_ITKCALL(iRetCode, ADA_add_license_object(tLicense, ptObjects[iZx]));
                                    }

                                    if(iRetCode != ITK_ok)
                                    {
                                        /* Open repeat file  */
                                        if(bRepeatFileExist == false)
                                        {
                                            T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                        }
                                        /* Report Error */
                                        T4_REPORT_SECURITY_ASSIGN_REMOVE_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszItemID, pszRev, pszAttrKey, pszAttrValue, pszAddOrRmvSecInfo);
                                    }
                                }
                                else if(bProcessIPClass ==true)
                                {
                                    if(tc_strcasecmp(pszAddOrRmvSecInfo, T4_ARG_VAL_REMOVE_MODE) == 0)
                                    {
                                        T4_MEM_TCFREE( pszAttrValue );
                                    }

                                    T4_ITKCALL(iRetCode, T4_obj_set_str_attribute_val(ptObjects[iZx], T4_ATTR_IP_CLASSIFICATION, pszAttrValue));
                                
                                    if(iRetCode != ITK_ok)
                                    {
                                        /* Open repeat file  */
                                        if(bRepeatFileExist == false)
                                        {
                                            T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                        }
                                        /* Report Error */
                                        T4_REPORT_SET_ATTR_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, iNoOfObj, pszItemID, pszRev, pszAttrKey);
                                    }
                                }
                            }

                            if(iRetCode == ITK_ok)
                            {
                                /* Report Success */
                                T4_REPORT_SUCCESS(iRetCode, fLogFile, fRepeatFile, szLine, pszItemID, pszRev, pszAttrKey, pszAttrValue, pszAddOrRmvSecInfo)
                            }
                        }
                        else
                        {
                            /* Open repeat file  */
                            if(bRepeatFileExist == false)
                            {
                                T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                            }
                            /* Report Error */
                            T4_REPORT_INCORRECT_SECURITY_ATTR(iRetCode, fLogFile, fRepeatFile, szLine, pszItemID, pszRev, pszAttrKey);
                        }
                    }

                    T4_MEM_TCFREE(ptObjects);
                }
                else
                {
                    /* Generating repeat file */
                    if(bRepeatFileExist == false)
                    {
                        T4_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                    }
                    /* Report Error */
                    T4_REPORT_UNABLE_TO_PARSE_LINE_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszConfigFileInfo);
                }

                iRetCode = ITK_ok;            
                
                T4_MEM_TCFREE( pszItemID );
                T4_MEM_TCFREE( pszRev );
                T4_MEM_TCFREE( pszAttrKey );
                T4_MEM_TCFREE( pszAttrValue );
            }
        } 

        if(bByPass == true)
        {
            ITK_set_bypass(false);
        }
    }

    if(bRepeatFileExist == true)
    {
        fclose( fRepeatFile );
    }

    fclose( fCfgFile );
    fclose( fLogFile );
    return iRetCode;
}


/**
 * This function parse the line from the load file and initialize the variable with appropriate information.
 *
 * @param[in]  pszLine       Name of the Configuration File to be processed
 * @param[in]  pszSeparator  Character separating the information in the load file
 * @param[out] ppszItemID    Item ID
 * @param[out] ppszRev       Revision ID
 * @param[out] ppszAttrKey   Attribute Key Name 
 * @param[out] ppszAttrValue Attribute value from the load file
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int T4_extract_info
(
    char*   pszLine,
    char*   pszSeparator,
    char**  ppszItemID,
    char**  ppszRev,
    char**  ppszAttrKey,
    char**  ppszAttrValue
)
{
    int    iRetCode   = ITK_ok;
    int    iInfoCnt   = 0;
    char** ppszInfo   = NULL;


    T4_ITKCALL(iRetCode , T4_arg_parse_multipleVal(pszLine, pszSeparator[0], &iInfoCnt, &ppszInfo));

    if(iInfoCnt == 3 || iInfoCnt == 4)
    {
        (*ppszItemID) = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[0]) +1)));
        tc_strcpy(*ppszItemID, ppszInfo[0]);

        if(iInfoCnt == 4)
        {
            (*ppszRev)       = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[1]) +1)));
            (*ppszAttrKey)   = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[2]) +1)));
            (*ppszAttrValue) = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[3]) +1)));

            tc_strcpy(*ppszRev, ppszInfo[1]);
            tc_strcpy(*ppszAttrKey, ppszInfo[2]);
            tc_strcpy(*ppszAttrValue, ppszInfo[3]);
        }
        else
        {
            (*ppszAttrKey)   = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[1]) +1)));
            (*ppszAttrValue) = (char*) MEM_alloc((int)(sizeof(char)*(tc_strlen(ppszInfo[2]) +1)));

            tc_strcpy(*ppszAttrKey, ppszInfo[1]);
            tc_strcpy(*ppszAttrValue, ppszInfo[2]);
        }
    }

    T4_MEM_TCFREE( ppszInfo );
    return iRetCode;
}

#ifdef __cplusplus
}
#endif


