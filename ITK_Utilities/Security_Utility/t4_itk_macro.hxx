/*======================================================================================================================================================================
                Copyright 2016  LMtec GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_itk_macro.hxx

    Description:  This file contains macros for handling ITK errors, macros for general errors, different Trace macros, and a function TextEMHErrors 
                  which encapsulates EMH_store_error, EMH_store_error_s1...

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
21-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_ITK_MACRO_HXX
#define T4_ITK_MACRO_HXX

#include <t4_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef T4_ITKCALL
#undef T4_ITKCALL
#endif

// This macro works for functions which return integer. The function
// return value is an input argument and return value in the same time.
// f is either ITK function, or a function which uses only ITK calls
// and therefore has to return ITK integer return code

#define T4_ITKCALL(iErrorCode,f) {\
           if(iErrorCode == ITK_ok) {\
               (iErrorCode) = (f);\
               T4_err_start_debug(#f, iErrorCode,__FILE__, __LINE__);\
               if(iErrorCode != ITK_ok) {\
                T4_err_write_to_logfile(#f, iErrorCode,__FILE__, __LINE__);\
                }\
             }\
          }

#endif







