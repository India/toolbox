/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_workflow_attr_mig.cxx

    Description: This file contains function 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_const.hxx>
#include <sma3_errors.hxx>
#include <sma3_library.hxx>
#include <sma3_workflow_attr_mig.hxx>

    
int SMA3_workflow_attr_migration
(
    char* pszFileInfo
)
{
    int    iRetCode                            = ITK_ok;
    int    iAttrCnt                            = 0;
    int    iObjCnt                             = 0;
    int    iDocTypCnt                          = 0;
    int    iStWFMigCnt                         = 0;
    char   gszLine[MAX_CHARS]                  = {'\0'};
    char   szDateTime[IMF_filename_size_c + 1] = {'\0'}; 
    char** ppszAttrDocTypeVal                  = NULL;
    char*  pszLogFileName                      = NULL;
    tag_t  tQuery                              = NULLTAG;
    tag_t* ptObject                            = NULL;
    sWFAttrMIGInfo* sWFMigList                 = NULL;
    FILE*  fCfgFile;
    FILE*  fLogFile;

    if ((fCfgFile = fopen(pszFileInfo, "r")) == NULL )
    {
        printf("\n** ERROR: Cannot open %s for reading - exiting **\n\n",pszFileInfo);
        return 0;
    }

    SMA3_ITKCALL(iRetCode, SMA3_dt_get_current_date_time(SMA3_DATE_TIME_FORMAT_FILENAME, szDateTime));

    int iLen = 0;

    iLen= tc_strlen(SMA3_WORKFLOW_ATTR_MIG_LOG) + tc_strlen(szDateTime) + tc_strlen(SMA3_LOG_EXTENSION) + 1;
    pszLogFileName = (char*) MEM_alloc(sizeof(char)* iLen);
    tc_strcpy(pszLogFileName, SMA3_WORKFLOW_ATTR_MIG_LOG);
    tc_strcat(pszLogFileName, szDateTime);
    tc_strcat(pszLogFileName, SMA3_LOG_EXTENSION);
    tc_strcat(pszLogFileName, '\0');

    if ((fLogFile = fopen(pszLogFileName, "w")) == NULL )
    {
        printf("\n** ERROR: Cannot create %s for writing log  **\n\n",pszLogFileName);
    }
    else
    {
        while (fgets(gszLine, MAX_CHARS, fCfgFile) != NULL)
        {
            int     iCount      = 0;
            char**  ppszVal     = NULL;
            char*   pszFileLine = NULL;
            int     iLen        = 0;

            iLen = tc_strlen(gszLine);

            SMA3_ITKCALL(iRetCode, SMA3_string_copy_substring(gszLine, 1, iLen-1, &pszFileLine));
            
            SMA3_ITKCALL(iRetCode, SMA3_arg_parse_multipleVal(pszFileLine, ',', &iCount, &ppszVal));

            if(iCount > 0)
            {
                if(iCount == 2)
                {
                    iStWFMigCnt++;

                    if(iStWFMigCnt == 0)
                    {
                        sWFMigList = (sWFAttrMIGInfo*) MEM_alloc( sizeof(struct sWFAttrMIGInfo) * iStWFMigCnt);
                    }
                    else
                    {
                        sWFMigList = (sWFAttrMIGInfo*) MEM_realloc(sWFMigList, sizeof(struct sWFAttrMIGInfo) * iStWFMigCnt);
                    }

                    sWFMigList[iStWFMigCnt-1].iAttrWFCnt = iCount;
                    sWFMigList[iStWFMigCnt-1].iDocTypeCnt = 0; 
                    sWFMigList[iStWFMigCnt-1].pptDocWFAttVal = (char**) MEM_alloc(sizeof(char*)* iCount);

                    for(int iKx = 0; iKx < iCount; iKx++)
                    {
                        sWFMigList[iStWFMigCnt-1].pptDocWFAttVal[iKx] = (char*) MEM_alloc(sizeof(char)* (tc_strlen(ppszVal[iKx])+1));
                        tc_strcpy(sWFMigList[iStWFMigCnt-1].pptDocWFAttVal[iKx], ppszVal[iKx]);
                    }
                }
                else
                {
                    int iTemp = 0;

                    iTemp = sWFMigList[iStWFMigCnt-1].iDocTypeCnt;

                    iTemp++;

                    if((sWFMigList[iStWFMigCnt-1].iDocTypeCnt) == 0)
                    {
                        sWFMigList[iStWFMigCnt-1].ppszDocTypeVal = (char**) MEM_alloc(sizeof(char*)* iTemp);
                    }
                    else
                    {
                        sWFMigList[iStWFMigCnt-1].ppszDocTypeVal = (char**) MEM_realloc((sWFMigList[iStWFMigCnt-1].ppszDocTypeVal),sizeof(char*)* iTemp);
                    }

                    sWFMigList[iStWFMigCnt-1].iDocTypeCnt = iTemp;

                    sWFMigList[iStWFMigCnt-1].ppszDocTypeVal[iTemp -1] = (char*) MEM_alloc(sizeof(char)* (tc_strlen(ppszVal[0])+1));
                    tc_strcpy(sWFMigList[iStWFMigCnt-1].ppszDocTypeVal[iTemp -1], ppszVal[0]);
                }
            }

            SMA3_MEM_TCFREE(pszFileLine);
            SMA3_MEM_TCFREE(ppszVal);
        }

        SMA3_ITKCALL(iRetCode, QRY_find( SMA3_QURY_DOCUMENTS, &tQuery ));

        SMA3_ITKCALL(iRetCode, QRY_execute( tQuery, 0, NULL, NULL, &iObjCnt, &ptObject ));

        ITK_set_bypass(true); 

        for( int iDx = 0; iDx < iObjCnt; iDx++)
        {
            char*   pszObjStr          = NULL;
            char*   pszDocTypeVal      = NULL;
            char*   pszWorkflowTypeVal = NULL;
            int     iNewValAttrIndex   = 0;
            logical bFound             = false;

            iRetCode = ITK_ok;

            AOM_ask_value_string(ptObject[iDx], SMA3_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr);

            SMA3_ITKCALL(iRetCode, AOM_ask_value_string( ptObject[iDx], SMA3_ATTR_WORKFLOW, &pszWorkflowTypeVal ) );

            SMA3_ITKCALL(iRetCode, AOM_ask_value_string( ptObject[iDx], SMA3_ATTR_DOCUMENT_TYPE, &pszDocTypeVal ) );

            if(iRetCode != ITK_ok)
            {
                fprintf(fLogFile, "Error :%s :Old WF Attr %s : SMA3_doc_type  %s\n", pszObjStr, pszWorkflowTypeVal, pszDocTypeVal); 
                fflush(fLogFile); 
            }
            else
            {

                for(int iYx = 0; iYx < iStWFMigCnt && bFound == false; iYx++)
                {
                    if(tc_strcmp(sWFMigList[iYx].pptDocWFAttVal[0], pszWorkflowTypeVal) == 0)
                    {
                        for(int iZx = 0; iZx < (sWFMigList[iYx].iDocTypeCnt) && bFound == false; iZx++)
                        {
                            if(tc_strcmp(sWFMigList[iYx].ppszDocTypeVal[iZx], pszDocTypeVal) == 0)
                            {
                                SMA3_ITKCALL(iRetCode, AOM_refresh(ptObject[iDx], true));

                                SMA3_ITKCALL(iRetCode, AOM_set_value_string(ptObject[iDx], SMA3_ATTR_WORKFLOW, sWFMigList[iYx].pptDocWFAttVal[1]));

                                SMA3_ITKCALL(iRetCode, AOM_save(ptObject[iDx]));

                                SMA3_ITKCALL(iRetCode, AOM_refresh(ptObject[iDx], false));

                                if(iRetCode == ITK_ok)
                                {
                                    bFound = true;
                                    fprintf(fLogFile, "Success :%s : Old WF Attr %s : New Val %s : SMA3_doc_type  %s \n", pszObjStr,pszWorkflowTypeVal, sWFMigList[iYx].pptDocWFAttVal[1], pszDocTypeVal); 
                                    fflush(fLogFile); 
                                }
                                else
                                {
                                    fprintf(fLogFile, "Error :%s :Old WF Attr %s : SMA3_doc_type  %s\n", pszObjStr, pszWorkflowTypeVal, pszDocTypeVal); 
                                    fflush(fLogFile); 
                                }   
                            }
                        }
                    }

                }
            }

            if(bFound == false && iRetCode == ITK_ok)
            {
              fprintf(fLogFile, "Unprocessed  :%s :Old WF Attr %s : SMA3_doc_type  %s\n", pszObjStr, pszWorkflowTypeVal, pszDocTypeVal); 
              fflush(fLogFile);
            }

            SMA3_MEM_TCFREE(pszObjStr);
            SMA3_MEM_TCFREE(pszDocTypeVal);
            SMA3_MEM_TCFREE(pszWorkflowTypeVal);
        }

        ITK_set_bypass(false); 
    }

    fclose( fCfgFile );
    fclose( fLogFile );

    SMA3_STRUCT_WFATTRMIG_FREE(sWFMigList, iStWFMigCnt);
    SMA3_MEM_TCFREE(pszLogFileName);
    SMA3_MEM_TCFREE(ptObject);
    return iRetCode;
}



#ifdef __cplusplus
}
#endif