/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_material_doc_mig_rel.cxx

    Description: This file contains function .

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_const.hxx>
#include <sma3_errors.hxx>
#include <sma3_library.hxx>
#include <sma3_material_doc_mig_rel.hxx>

/**
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_material_document_migrate_rel
(
    char* pszFileInfo
)
{
    int     iRetCode                            = ITK_ok;
    int     iObjCnt                             = 0;
    int     iStRelDocCnt                        = 0;
    tag_t   tQuery                              = NULLTAG;
    tag_t*  ptObject                            = NULL;
    char*   pszLogFileName                      = NULL;
    char    gszLine[MAX_CHARS]                  = {'\0'};
    char    szDateTime[IMF_filename_size_c + 1] = {'\0'};
    sMatDocRelMIGInfo* sRelDocInfo              = NULL;
    FILE*   fCfgFile;
    FILE*   fLogFile;
    

    if ((fCfgFile = fopen(pszFileInfo, "r")) == NULL )
    {
        printf("\n** ERROR: Cannot open %s for reading - exiting **\n\n",pszFileInfo);
        return 0;
    }

    SMA3_ITKCALL(iRetCode, SMA3_dt_get_current_date_time(SMA3_DATE_TIME_FORMAT_FILENAME, szDateTime));

    int iLen = 0;

    iLen= tc_strlen(SMA3_MATERIAL_DOCUMENT_MIG_REL_LOG) + tc_strlen(szDateTime) + tc_strlen(SMA3_LOG_EXTENSION) + 1;
    pszLogFileName = (char*) MEM_alloc(sizeof(char)* iLen);
    tc_strcpy(pszLogFileName, SMA3_MATERIAL_DOCUMENT_MIG_REL_LOG);
    tc_strcat(pszLogFileName, szDateTime);
    tc_strcat(pszLogFileName, SMA3_LOG_EXTENSION);
    tc_strcat(pszLogFileName, '\0');

    if ((fLogFile = fopen(pszLogFileName, "w")) == NULL )
    {
        printf("\n** ERROR: Cannot create %s for writing log  **\n\n",pszLogFileName);
    }
    else
    {
        while (fgets(gszLine, MAX_CHARS, fCfgFile) != NULL)
        {
            logical bISRelation = false;
            char*   pszFileLine = NULL;
            int     iLen        = 0;

            iLen = tc_strlen(gszLine);

            SMA3_ITKCALL(iRetCode, SMA3_string_copy_substring(gszLine, 1, iLen-1, &pszFileLine));

            iLen = tc_strlen(SMA3_REL_PRE_FIX);

            if(tc_strncasecmp(gszLine, SMA3_REL_PRE_FIX, iLen) == 0)
            {
                iStRelDocCnt++;

                if(iStRelDocCnt == 0)
                {
                    sRelDocInfo = (sMatDocRelMIGInfo*) MEM_alloc( sizeof(struct sMatDocRelMIGInfo) * iStRelDocCnt);
                }
                else
                {
                    sRelDocInfo = (sMatDocRelMIGInfo*) MEM_realloc(sRelDocInfo, sizeof(struct sMatDocRelMIGInfo) * iStRelDocCnt);
                }

                sRelDocInfo[iStRelDocCnt-1].iAttrDocTypeCnt = 0; 
                sRelDocInfo[iStRelDocCnt-1].pszRelName = (char*) MEM_alloc(sizeof(char)*(tc_strlen(pszFileLine)+1));
                tc_strcpy(sRelDocInfo[iStRelDocCnt-1].pszRelName, pszFileLine);
            }
            else
            {
                int iTemp = 0;

                iTemp = sRelDocInfo[iStRelDocCnt-1].iAttrDocTypeCnt;

                iTemp++;

                if((sRelDocInfo[iStRelDocCnt-1].iAttrDocTypeCnt) == 0)
                {
                    sRelDocInfo[iStRelDocCnt-1].pptDocTypeVal = (char**) MEM_alloc(sizeof(char*)* iTemp);
                }
                else
                {
                    sRelDocInfo[iStRelDocCnt-1].pptDocTypeVal = (char**) MEM_realloc((sRelDocInfo[iStRelDocCnt-1].pptDocTypeVal), sizeof(char*)* iTemp);
                }

                sRelDocInfo[iStRelDocCnt-1].iAttrDocTypeCnt = iTemp;

                sRelDocInfo[iStRelDocCnt-1].pptDocTypeVal[iTemp -1] = (char*) MEM_alloc(sizeof(char)* (tc_strlen(pszFileLine)+1));
                tc_strcpy(sRelDocInfo[iStRelDocCnt-1].pptDocTypeVal[iTemp -1], pszFileLine);

            }

            SMA3_MEM_TCFREE(pszFileLine);
        }

        ITK_set_bypass(true); 

        SMA3_ITKCALL(iRetCode, QRY_find( SMA3_QURY_MATERIAL_DOC_MIG, &tQuery ));

        SMA3_ITKCALL(iRetCode, QRY_execute( tQuery, 0, NULL, NULL, &iObjCnt, &ptObject ));

        for( int iDx = 0; iDx < iObjCnt; iDx++)
        {
            int    iSecObjCnt       = 0;
            char*  pszPartObjStr    = NULL;
            char*  pszDocObjStr     = NULL;
            tag_t  tNewRelation     = NULLTAG;
            tag_t* ptSecObject      = NULL;
            tag_t* ptRelation       = NULL;

            iRetCode = ITK_ok;

            SMA3_ITKCALL(iRetCode, SMA3_grm_get_secondary_obj_of_class(ptObject[iDx], SMA3_IMAN_REFERENCE_RELATION, SMA3_CLASS_SMA_DOCUMENT_REVISION, &ptSecObject,
                                                                       &ptRelation, &iSecObjCnt ));

            for(int iJx = 0; iJx< iSecObjCnt; iJx++)
            {
                char*   pszAttrVal    = NULL;
                char*   pszNewRelName = NULL;
                tag_t   tItem         = NULLTAG;
                tag_t   tNewRelation  = NULLTAG;
                logical bFoundRel     = false;

                SMA3_ITKCALL(iRetCode, ITEM_ask_item_of_rev (ptSecObject[iJx], &tItem ));

                SMA3_ITKCALL(iRetCode, AOM_ask_value_string(tItem, SMA3_ATTR_DOCUMENT_TYPE,  &pszAttrVal ));

                for(int iMx = 0; iMx < iStRelDocCnt && bFoundRel == false && iRetCode == ITK_ok; iMx++)
                {
                    int iCount = 0;

                    iCount = sRelDocInfo[iMx].iAttrDocTypeCnt;

                    for(int iFx = 0; iFx < iCount && bFoundRel == false; iFx++)
                    {
                        if(tc_strcmp(sRelDocInfo[iMx].pptDocTypeVal[iFx], pszAttrVal) == 0)
                        {
                            bFoundRel = true;
                            pszNewRelName = (char*) MEM_alloc(sizeof(char)* (tc_strlen(sRelDocInfo[iMx].pszRelName)+1));
                            tc_strcpy(pszNewRelName, sRelDocInfo[iMx].pszRelName);
                        }
                    }         
                } 

                AOM_ask_value_string(ptObject[iDx], SMA3_ATTR_OBJECT_STRING_ATTRIBUTE, &pszPartObjStr);

                AOM_ask_value_string(ptSecObject[iJx], SMA3_ATTR_OBJECT_STRING_ATTRIBUTE, &pszDocObjStr);

                if(bFoundRel == true)
                {
                    SMA3_ITKCALL(iRetCode, GRM_find_relation_type( pszNewRelName, &tNewRelation));
                    
                    SMA3_ITKCALL(iRetCode, AOM_refresh(ptObject[iDx], true));

                    SMA3_ITKCALL(iRetCode, GRM_create_relation(ptObject[iDx], tItem, tNewRelation, NULLTAG, &tNewRelation));

                    SMA3_ITKCALL(iRetCode, GRM_save_relation( tNewRelation));

                    SMA3_ITKCALL(iRetCode, AOM_save(ptObject[iDx]));

                    SMA3_ITKCALL(iRetCode, GRM_delete_relation(ptRelation[iJx]));
                    

                    SMA3_ITKCALL(iRetCode, AOM_refresh(ptObject[iDx], false));
                }

                if(iRetCode == ITK_ok)
                {
                    if( bFoundRel == true)
                    {
                        char* pszItemObjStr = NULL;

                        AOM_ask_value_string(tItem, SMA3_ATTR_OBJECT_STRING_ATTRIBUTE, &pszItemObjStr);
                        fprintf(fLogFile, "Success : Material :%s : Document  %s : Relation: %s\n", pszPartObjStr, pszItemObjStr, pszNewRelName); 
                        fflush(fLogFile); 
                        SMA3_MEM_TCFREE(pszItemObjStr);
                    }
                    else
                    {
                        fprintf(fLogFile, "%s attribute value %s of Material :%s is not proper. \n",SMA3_ATTR_DOCUMENT_TYPE, pszAttrVal, pszPartObjStr); 
                        fflush(fLogFile); 

                    }
                }
                else
                {
                    fprintf(fLogFile, "Error : Material :%s : Document  %s \n", pszPartObjStr, pszDocObjStr); 
                    fflush(fLogFile); 
                }

                SMA3_MEM_TCFREE(pszAttrVal);
                SMA3_MEM_TCFREE(pszNewRelName);
            }
        
            SMA3_MEM_TCFREE(pszPartObjStr);
            SMA3_MEM_TCFREE(pszDocObjStr);
            SMA3_MEM_TCFREE(ptSecObject);
            SMA3_MEM_TCFREE(ptRelation);
        }

        ITK_set_bypass(false);
    }

    //for(int iRx = 0; iRx < iStRelDocCnt; iRx++)
    //{
    //    fprintf(fLogFile, "Material Val From Text :%s \n", sRelDocInfo[iRx].pszRelName); 
    //    for(int iHx = 0; iHx < sRelDocInfo[iRx].iAttrDocTypeCnt; iHx++)
    //    {
    //        fprintf(fLogFile, "\t\tDocument:%s \n", sRelDocInfo[iRx].pptDocTypeVal[iHx]);
    //    }
    //    fflush(fLogFile);
    //}

    SMA3_STRUCT_MATDOCRELMI_FREE(sRelDocInfo, iStRelDocCnt);
    SMA3_MEM_TCFREE(pszLogFileName);
    SMA3_MEM_TCFREE(ptObject);
    SMA3_MEM_TCFREE(sRelDocInfo);
    return iRetCode;
}



#ifdef __cplusplus
}
#endif


