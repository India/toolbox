/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: SMA3_itk_mem_free.hxx

    Description:  This file contains macros for freeing portion of memory allocated with TCe 'MEM_' functions and by TCe itself.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release
   
========================================================================================================================================================================*/
#ifndef SMA3_ITK_MEM_FREE_HXX
#define SMA3_ITK_MEM_FREE_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <sma3_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef SMA3_MEM_TCFREE
#undef SMA3_MEM_TCFREE
#endif

// Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc

#define SMA3_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }

/*  Macro to free memory for user defined data type. */
#ifdef SMA3_STRUCT_MATDOCRELMI_FREE
#undef SMA3_STRUCT_MATDOCRELMI_FREE
#endif

/* This macro is used to free the memory of structure used in . */
#define SMA3_STRUCT_MATDOCRELMI_FREE(sRelDocInfo,iCount) {\
          for(int idx =0; idx<iCount; idx++) {\
               SMA3_MEM_TCFREE(sRelDocInfo[idx].pszRelName);\
               SMA3_MEM_TCFREE(sRelDocInfo[idx].pptDocTypeVal);\
          }\
          SMA3_MEM_TCFREE(sRelDocInfo);\
        }


/*  Macro to free memory for user defined data type. */
#ifdef SMA3_STRUCT_WFATTRMIG_FREE
#undef SMA3_STRUCT_WFATTRMIG_FREE
#endif

/* This macro is used to free the memory of structure used in . */
#define SMA3_STRUCT_WFATTRMIG_FREE(sWFMigList,iCount) {\
          for(int idx =0; idx<iCount; idx++) {\
               SMA3_MEM_TCFREE(sWFMigList[idx].pptDocWFAttVal);\
               SMA3_MEM_TCFREE(sWFMigList[idx].ppszDocTypeVal);\
          }\
          SMA3_MEM_TCFREE(sWFMigList);\
        }
#endif










