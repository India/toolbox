/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: SMA3_grm_utilities.cxx

    Description: This File contains custom functions for GRM to perform generic operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release
========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_library.hxx>
#include <sma3_const.hxx>
#include <sma3_errors.hxx>


/**
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int SMA3_grm_get_secondary_obj_of_class
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    char*   pszParentClassName,
    tag_t** pptSecondaryObjects,
    tag_t** pptRelationTag,
    int*    piObjectCount  
)
{

    int     iRetCode                        = ITK_ok;
    int     iCount                          = 0;
    int     iIterator                       = 0;
    int     iSecondaryCount                 = 0;
    tag_t   tRelationType                   = NULLTAG;
    char*   pszObjectType                   = NULL;
    GRM_relation_t *ptSecondaryObjects      = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount) = 0;
    (*pptSecondaryObjects) = NULL;

    /* get the relation type tag e.g. "IMAN_specification"  */
    SMA3_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    
    SMA3_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, tRelationType, 
                                                       &iSecondaryCount, &ptSecondaryObjects));
                                       
    for(iIterator = 0; iIterator < iSecondaryCount && iRetCode == ITK_ok; iIterator++)
    {
        int  iActiveSeq   = 0;
      
        SMA3_ITKCALL(iRetCode, AOM_ask_value_int(ptSecondaryObjects[iIterator].secondary, SMA3_ATTR_ACTIVE_SEQ, &iActiveSeq));
	
		/* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0)
        {
            tag_t   tParentClassName     = NULLTAG;
			tag_t   tSecObjType          = NULLTAG;
			logical bIsTypeOf            = false;

			SMA3_ITKCALL(iRetCode, TCTYPE_find_type(pszParentClassName, NULL, &tParentClassName));

			SMA3_ITKCALL(iRetCode, TCTYPE_ask_object_type(ptSecondaryObjects[iIterator].secondary, &tSecObjType));

			SMA3_ITKCALL(iRetCode,  TCTYPE_is_type_of(tSecObjType, tParentClassName, &bIsTypeOf));

			if(bIsTypeOf == true)
			{
                if(iCount == 0)
                {
                    iCount++;
                    (*pptSecondaryObjects) = (tag_t*)MEM_alloc ((sizeof(tag_t) * iCount) );
                    (*pptRelationTag) = (tag_t*)MEM_alloc ((sizeof(tag_t) * iCount) );
                    
                    (*pptSecondaryObjects)[iCount -1] = ptSecondaryObjects[iIterator].secondary;
                    (*pptRelationTag)[iCount -1] = ptSecondaryObjects[iIterator].the_relation;
                }
                else
                {
                    iCount++;
                    (*pptSecondaryObjects) = (tag_t*)MEM_realloc ( (*pptSecondaryObjects) , (sizeof(tag_t) * iCount) );
                    (*pptRelationTag) = (tag_t*)MEM_realloc ( (*pptRelationTag) , (sizeof(tag_t) * iCount) );

                    (*pptSecondaryObjects)[iCount -1] = ptSecondaryObjects[iIterator].secondary;
                    (*pptRelationTag)[iCount -1] = ptSecondaryObjects[iIterator].the_relation;
                }
            }
        }
    }

    if(iCount == 0)
    {
        TC_write_syslog("No Primary Object associated with the relation to the object.\n");
    }
    else
    {
        (*piObjectCount ) = iCount;
    }

    SMA3_MEM_TCFREE(pszObjectType);
    SMA3_MEM_TCFREE(ptSecondaryObjects);
    return iRetCode;
}


#ifdef __cplusplus
}
#endif


