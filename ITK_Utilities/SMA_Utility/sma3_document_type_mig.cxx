/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_document_type_mig.cxx

    Description: This file contains function 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_const.hxx>
#include <sma3_errors.hxx>
#include <sma3_library.hxx>
#include <sma3_document_type_mig.hxx>

int SMA3_document_prop_migration
(
    char* pszFileInfo
)
{
    int    iRetCode                            = ITK_ok;
    int    iAttrCnt                            = 0;
    int    iObjCnt                             = 0;
    char   gszLine[MAX_CHARS]                  = {'\0'};
    char   szDateTime[IMF_filename_size_c + 1] = {'\0'}; 
    char** ppsOldAttrVal                       = NULL;
    char** ppsNewAttrVal                       = NULL; 
    char*  pszLogFileName                      = NULL;
    tag_t  tQuery                              = NULLTAG;
    tag_t* ptObject                            = NULL;
    FILE*  fCfgFile;
    FILE*  fLogFile;

    if ((fCfgFile = fopen(pszFileInfo, "r")) == NULL )
    {
        printf("\n** ERROR: Cannot open %s for reading - exiting **\n\n",pszFileInfo);
        return 0;
    }

    SMA3_ITKCALL(iRetCode, SMA3_dt_get_current_date_time(SMA3_DATE_TIME_FORMAT_FILENAME, szDateTime));

    int iLen = 0;

    iLen= tc_strlen(SMA3_DOCUMENT_TYPE_MIG_LOG) + tc_strlen(szDateTime) + tc_strlen(SMA3_LOG_EXTENSION) + 1;
    pszLogFileName = (char*) MEM_alloc(sizeof(char)* iLen);
    tc_strcpy(pszLogFileName, SMA3_DOCUMENT_TYPE_MIG_LOG);
    tc_strcat(pszLogFileName, szDateTime);
    tc_strcat(pszLogFileName, SMA3_LOG_EXTENSION);
    tc_strcat(pszLogFileName, '\0');

    if ((fLogFile = fopen(pszLogFileName, "w")) == NULL )
    {
        printf("\n** ERROR: Cannot create %s for writing log  **\n\n",pszLogFileName);
    }
    else
    {
        while (fgets(gszLine, MAX_CHARS, fCfgFile) != NULL)
        {
            int     iCount      = 0;
            int     iLen        = 0;
            char*   pszFileLine = NULL;
            char**  ppszVal     = NULL;

            iLen = tc_strlen(gszLine);

            SMA3_ITKCALL(iRetCode, SMA3_string_copy_substring(gszLine, 1, iLen-1, &pszFileLine));
            
            SMA3_ITKCALL(iRetCode, SMA3_arg_parse_multipleVal(pszFileLine, ',', &iCount, &ppszVal));

            if(iCount > 0)
            {
                iAttrCnt++;

                if(iAttrCnt == 0)
                {
                    ppsOldAttrVal = (char**) MEM_alloc(iAttrCnt * sizeof(char*));

                    ppsNewAttrVal = (char**) MEM_alloc(iAttrCnt * sizeof(char*));
                }
                else
                {
                    ppsOldAttrVal = (char**) MEM_realloc(ppsOldAttrVal, iAttrCnt * sizeof(char*));

                    ppsNewAttrVal = (char**) MEM_realloc(ppsNewAttrVal, iAttrCnt * sizeof(char*));
                }

                ppsOldAttrVal[iAttrCnt-1] = (char*) MEM_alloc(sizeof(char)* (tc_strlen(ppszVal[0])+1));
                tc_strcpy(ppsOldAttrVal[iAttrCnt-1], ppszVal[0]);

                ppsNewAttrVal[iAttrCnt-1] = (char*) MEM_alloc(sizeof(char)* (tc_strlen(ppszVal[1])+1));
                tc_strcpy(ppsNewAttrVal[iAttrCnt-1], ppszVal[1]);
            }

            SMA3_MEM_TCFREE(pszFileLine);
            SMA3_MEM_TCFREE(ppszVal);
        }

        SMA3_ITKCALL(iRetCode, QRY_find( SMA3_QURY_DOCUMENTS, &tQuery ));

        SMA3_ITKCALL(iRetCode, QRY_execute( tQuery, 0, NULL, NULL, &iObjCnt, &ptObject ));

        ITK_set_bypass(true); 

        for( int iDx = 0; iDx < iObjCnt; iDx++)
        {
            char*   pszObjStr            = NULL;
            char*   pszPresentDocTypeVal = NULL;
            int     iNewValAttrIndex     = 0;
            logical bFound               = false;

            iRetCode = ITK_ok;

            SMA3_ITKCALL(iRetCode, AOM_ask_value_string( ptObject[iDx], SMA3_ATTR_DOCUMENT_TYPE, &pszPresentDocTypeVal ) );

            for(int iJx = 0; iJx < iAttrCnt && bFound == false; iJx++)
            {
                if(tc_strcmp(ppsOldAttrVal[iJx], pszPresentDocTypeVal) == 0)
                {
                    iNewValAttrIndex = iJx;
                    bFound = true;
                    break;
                }
            }

            AOM_ask_value_string(ptObject[iDx], SMA3_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjStr);

            if(bFound == true)
            {
                SMA3_ITKCALL(iRetCode, AOM_refresh(ptObject[iDx], true));

                SMA3_ITKCALL(iRetCode, AOM_set_value_string(ptObject[iDx], SMA3_ATTR_DOCUMENT_TYPE, ppsNewAttrVal[iNewValAttrIndex]));

                SMA3_ITKCALL(iRetCode, AOM_save(ptObject[iDx]));

                SMA3_ITKCALL(iRetCode, AOM_refresh(ptObject[iDx], false));

                if(iRetCode == ITK_ok)
                {
                    fprintf(fLogFile, "Success :%s : Old Val %s : New Val %s\n", pszObjStr, pszPresentDocTypeVal, ppsNewAttrVal[iNewValAttrIndex]); 
                    fflush(fLogFile); 
                }
                else
                {
                    fprintf(fLogFile, "Error :%s \n", pszObjStr); 
                    fflush(fLogFile); 
                }
            }
            else
            {
                    fprintf(fLogFile, "Unprocessed  :%s : Value of Prop --> %s \n", pszObjStr, pszPresentDocTypeVal); 
                    fflush(fLogFile);
            }

            SMA3_MEM_TCFREE(pszObjStr);
            SMA3_MEM_TCFREE(pszPresentDocTypeVal);
        }

        ITK_set_bypass(false);
    }

    fclose( fCfgFile );
    fclose( fLogFile );

    SMA3_MEM_TCFREE(pszLogFileName);
    SMA3_MEM_TCFREE(ppsOldAttrVal);
    SMA3_MEM_TCFREE(ppsNewAttrVal);
    SMA3_MEM_TCFREE(ptObject);
    return iRetCode;
}



#ifdef __cplusplus
}
#endif