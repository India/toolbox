/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: SMA3_itk_macro.hxx

    Description:  This file contains macros for handling ITK errors, macros for general errors, different Trace macros, and a function TextEMHErrors 
                  which encapsulates EMH_store_error, EMH_store_error_s1...

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef SMA3_ITK_MACRO_HXX
#define SMA3_ITK_MACRO_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <sma3_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef SMA3_ITKCALL
#undef SMA3_ITKCALL
#endif

// This macro works for functions which return integer. The function
// return value is an input argument and return value in the same time.
// f is either ITK function, or a function which uses only ITK calls
// and therefore has to return ITK integer return code

#define SMA3_ITKCALL(iErrorCode,f) {\
           if(iErrorCode == ITK_ok) {\
               (iErrorCode) = (f);\
               SMA3_err_start_debug(#f, iErrorCode,__FILE__, __LINE__);\
               if(iErrorCode != ITK_ok) {\
                SMA3_err_write_to_logfile(#f, iErrorCode,__FILE__, __LINE__);\
                }\
             }\
          }

#endif







