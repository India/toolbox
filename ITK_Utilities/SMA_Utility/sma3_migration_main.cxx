/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: SMA3_grm_utilities.cxx

    Description: This File contains functions that are entry point to ECO report generation executable

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release
========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <conio.h>
#include <sma3_library.hxx>
#include <sma3_const.hxx>
#include <sma3_errors.hxx>
#include <sma3_material_doc_mig_rel.hxx>
#include <sma3_document_type_mig.hxx>
#include <sma3_workflow_attr_mig.hxx>

int SMA3_init_ITK_login(void);

void SMA3_display_usage();

/**
 * This function is the main function where program starts execution. It is responsible for the high-level organization of the
 * program's functionality, and try to access command arguments which basically are login credentials when this executable is run.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int ITK_user_main( int argc, char **argv)
{
    int     iRetCode     = ITK_ok;

    /* display help if user asked for it */
	if ((ITK_ask_cli_argument("-h")) || (argc == 0) )
    {
        SMA3_display_usage();
        exit(0);
    }

    char* pszMode = ITK_ask_cli_argument( "-mode=" );
    /* validating the mode */ 
    if((pszMode == NULL) || (tc_strlen(pszMode) == 0))
    {
        fprintf(stderr,"\n\nError: mode of execution not specified\n");
        SMA3_display_usage();
        exit(0);
    }

    char* pszFileInfo  = ITK_ask_cli_argument( "-file=");

    /* validating input file */ 
    if((pszFileInfo == NULL) || (tc_strlen(pszFileInfo) == 0))
    {
        fprintf(stderr,"\n\nError: File name not specified\n");
        SMA3_display_usage();
        exit(0);
    }
    
    //getch();

	SMA3_ITKCALL( iRetCode, SMA3_init_ITK_login());

     printf("Successfully Logged In Teamcenter \n");

    if(tc_strcmp(pszMode, "1") == 0)
    {
        printf("\tStarted Processing \"Document Type Migration.txt\"\n");
        SMA3_ITKCALL( iRetCode, SMA3_document_prop_migration(pszFileInfo));
    }

    if(tc_strcmp(pszMode, "2") == 0 )
    {
       printf("\tStarted Processing \"Workflow Attribute Migration.txt\"\n");
       SMA3_ITKCALL( iRetCode, SMA3_workflow_attr_migration(pszFileInfo));
    }

    if(tc_strcmp(pszMode, "3") == 0 )
    {
        printf("\tStarted Processing \"Material Document Migration.txt\"\n");
        SMA3_ITKCALL( iRetCode, SMA3_material_document_migrate_rel(pszFileInfo));
    }


	SMA3_ITKCALL( iRetCode, ITK_exit_module(FALSE));

    
	return iRetCode;
}

/**
 * This function retrieves login credentials and try to login to Teamcenter with these credential. If not supplied executable
 * performs auto login.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_init_ITK_login(void)
{
    int   iRetCode        = ITK_ok;

    /* Gets the user ID */
    char* pszUserId = ITK_ask_cli_argument( "-u=" );

    /* Gets the user Password */
    char* pszUserPasswd = ITK_ask_cli_argument( "-p=" );

    /* Gets the user group */
    char* pszUserGroup = ITK_ask_cli_argument( "-g=" );

    /*  To run against motif libs.*/
    SMA3_ITKCALL(iRetCode, ITK_initialize_text_services(ITK_BATCH_TEXT_MODE));

    /*  Put -h CLI argument here to avoid starting the whole of iman just for a help message! */
    if( ITK_ask_cli_argument( "-h" ) != 0 )
    {
        SMA3_display_usage();
        return iRetCode;
    }

	if(pszUserGroup == NULL || pszUserId == NULL)
	{
		SMA3_ITKCALL( iRetCode, ITK_auto_login());		
	}
	else
	{
		SMA3_ITKCALL( iRetCode, ITK_init_module(pszUserId, pszUserPasswd, pszUserGroup));		
	}

    return iRetCode;
}

/**
 * This function displays the usage of this Executable
 */
void SMA3_display_usage()
{
    printf("     \n\t\t\n SMA Utility Usage \n");
    printf("     \t\t\n-------------------------------\n");
    printf("           -u                   = <user name> to specify user name\n");
    printf("           -p                   = <password> to specify password\n");
    printf("           -g                   = <group name> to specify group name\n");
    printf("           -mode                = [Valid values are 1 or 2 or 3.] \n \t\t\t\tDocument Type Migration[1] \n \t\t\t\tWorkflow Attribute Migration[2] \n\t\t\t\tMaterial Document Migration[3] \n");
    printf("           -file                = In_file_name: For \n\t\t\t [-mode] 1 [-file] Document Type Migration.txt \n \t\t\t\ [-mode] 2 [-file] Workflow Attribute Migration.txt \n\t\t\t [-mode] 3 [-file] Material Document Migration.txt \n ");
    printf("           -h                     [Help]\n");
}




#ifdef __cplusplus
}
#endif




