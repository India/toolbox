/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: SMA3_error_logging_utilities.cxx

    Description:  This File contains custom functions for logging error to facilitate error reporting and problem analysis.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif


#include <sma3_const.hxx>
#include <sma3_errors.hxx>
#include <sma3_library.hxx>

static int     iStartDebug   = 0;
static logical bExecutedOnce = false;

/**
 * The function does error reporting to syslog. Translate error code to error string
 * and add information to log file.
 *
 * @param[in]  pszErrorFunction    Function causing error.
 * @param[in] iErrorNumber         Error code.
 * @param[in] pszFileName          Name of file containing function
 * @param[in] iLineNumber          Line number on which error has occurred.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_err_write_to_logfile
(
    char*       pszErrorFunction,
    int         iErrorNumber,
    char*       pszFileName,
    int         iLineNumber
)
{
    int    iRetCode  = ITK_ok;

    if (iErrorNumber != ITK_ok)  
    {    
        int          iEMH_severity   = 0;
        char*        pszErrorMsg     = NULL;   
        logical      bErrorFound     = false;

        
        iRetCode = EMH_ask_error_text(iErrorNumber, &pszErrorMsg);

        if(iRetCode != ITK_ok || pszErrorMsg == NULL)
        {
            char szErrorNumber[SMA3_NUMERIC_ERROR_LENGTH] = "";
            sprintf(szErrorNumber, "%d", iErrorNumber);
            iRetCode = ERROR_CODE_NOT_STORED_ON_STACK;
            EMH_store_error_s1(EMH_severity_warning, iRetCode, szErrorNumber);
              
        }
        else
        {
            /* EMH_severity_error   */
            TC_write_syslog("ERROR: %d  ERROR MSG: %s  ERROR Severity: %d.\n", iErrorNumber, pszErrorMsg, iEMH_severity);  

            TC_write_syslog("ERROR ON FUNCTION: %s\n  FILE: %s LINE: %d\n",  pszErrorFunction, pszFileName, iLineNumber );

        }
        
    }   

     return iRetCode;
}

/**
 * This function prints the function name and information related to execution when ever Environment variable
 * 'DEBUG' is set to 'true'.
 *
 * @param[in] pszCallingFunction   Name of function invoked.
 * @param[in] iErrorNumber         Error code.
 * @param[in] pszFileName          Name of file along with full path of file containing function
 * @param[in] iLineNumber          Line number on which error has occurred.
 *
 * @retval ITK_ok 
 */
int SMA3_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
)
{
    int iRetCode = ITK_ok;

    if(iStartDebug == 0 && bExecutedOnce == false)
    {
        const char*  pszEnvInfo  = NULL;

        pszEnvInfo = TC_getenv("DEBUG");

        bExecutedOnce = true;

        if (pszEnvInfo!=NULL)
        {
			if (tc_strlen(pszEnvInfo) > 0)
			{
				printf("DEBUG Started: Env Info = %s\n", pszEnvInfo);
				iStartDebug   = 1;
			}
        }    
    }

    if(iStartDebug == 1)
    {
        TC_write_syslog(" %-20s:%d | %5d = %s \n", pszFileName, iLineNumber, iErrorNumber, pszCallingFunction);
    }

  return iRetCode;
} 

#ifdef __cplusplus
}
#endif






