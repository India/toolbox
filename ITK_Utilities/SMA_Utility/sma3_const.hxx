/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_const.hxx

    Description:  Header File for constants used during scope of the executable.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef SMA3_CONST_HXX
#define SMA3_CONST_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>

#define MAX_CHARS               300

/* Query Names    */
#define SMA3_QURY_DOCUMENTS                                      "MIG_Document_Qry"
#define SMA3_QURY_MATERIAL_DOC_MIG                               "MIG_PurchaseParts_QRY"

/* Log File Names    */
#define SMA3_DOCUMENT_TYPE_MIG_LOG                               "Document_Type_MIG"
#define SMA3_WORKFLOW_ATTR_MIG_LOG                               "Workflow_Attr_MIG"
#define SMA3_MATERIAL_DOCUMENT_MIG_REL_LOG                       "Material_Document_MIG_rel"
#define SMA3_LOG_EXTENSION                                       ".log"

/* Pre Fix String    */
#define SMA3_REL_PRE_FIX                                        "SMA3_"


/* Date Time Format */
#define SMA3_DATE_TIME_FORMAT_FILENAME                            "%y%m%d%H%M%S"

/* Relation    */
#define SMA3_IMAN_REFERENCE_RELATION                              "IMAN_reference"

/* Attributes  */
#define SMA3_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define SMA3_ATTR_DOCUMENT_TYPE                                   "sma3_doc_type"
#define SMA3_ATTR_WORKFLOW                                        "sma3_workflow"
#define SMA3_ATTR_ACTIVE_SEQ                                      "active_seq"

/* Class Name */
#define SMA3_CLASS_SMA_DOCUMENT_REVISION                          "SMA_DOCUMENTRevision"

/* Separators */
#define SMA3_STRING_UNDERSCORE                                    "_"
#define SMA3_STRING_HIFEN                                         "-"
#define SMA3_STRING_SEMICOLON                                     ";"
#define SMA3_STRING_FORWARD_SLASH                                 "/"
#define SMA3_STRING_SINGLE_SPACE								  " "
#define SMA3_CHARACTER_COLON                                      ':'


#define DEBUG                                                     "true"

#define SMA3_SEPARATOR_LENGTH                                     1
#define SMA3_NUMERIC_ERROR_LENGTH                                 12
#define SMA3_DEFAULT_WORD_WRAP_CHAR_PER_LINE                      50


#endif


