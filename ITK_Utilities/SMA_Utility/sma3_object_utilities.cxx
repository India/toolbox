/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: SMA3_object_utilities.cxx

    Description: This file contains function performing various operations on Teamcenter Business Objects

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Feb-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_const.hxx>
#include <sma3_errors.hxx>
#include <sma3_library.hxx>

/**
 * This function gets the current date time.
 *
 * @param[in]  pszFormat   Expected date time format.
 * @param[out] szDateTime  current Date time in required format.
 *
 * @note 'szDateTime' must be a character array of at least [IMF_filename_size_c + 1] 
 *       characters 
 *
 * @retval ITK_ok on successful execution
 * @retval INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME if format resulted in
 *         resultant String length greater then IMF_filename_size_c + 1
 */
int SMA3_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
)
{
    int         iRetCode       = ITK_ok;
    int         iStrLength     = 0;
    struct tm   *clLocalTime   = NULL;
    time_t      tCurrentTime;

    /*Get system's local time*/
    tCurrentTime = time(NULL);
    clLocalTime  = localtime(&tCurrentTime);
        
    /*Get system time  in required format*/
    iStrLength = strftime( szDateTime, (IMF_filename_size_c + 1), pszFormat, clLocalTime);
   
      
    return iRetCode;
}
/**
 * This function returns the substring when provided with start and end position on the 
 * the input string. Substring is the character sequence that starts at character position 
 * 'iStrStartPosition' and ends at character position 'iStrEndPosition'
 *
 * For example if 'pszInputString' = 'Application'
 *                'iStrStartPosition' = 4
 *                'iStrEndPosition'   = 9
 *           then Output Will be: 'licati'
 *
 * Function will copy the characters starting 4th character to the 9th character of
 * input string.
 * 
 * @param[in]  pszInputString    Actual input string
 * @param[in]  iStrStartPosition Position of a character in the current string to be
 *                               used as starting character for the substring.
 * @param[in]  iStrEndPosition   Position of a character in the current string to be
 *                               used as last character for the substring.
 *
 * @param[out] ppszCopyedStr     Copied string.
 *
 * @retval ITK_ok on successful execution
 */
int SMA3_string_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
)
{
    int  iRetCode    = ITK_ok;
    int  iStrLen     = 0;

    /* Initializing the Out Parameter to this function. */
    (*ppszCopyedStr) = NULL;

    iStrLen = (int) tc_strlen(pszInputString);
    
    if ((pszInputString != NULL) && (iStrStartPosition >= 0) && (iStrEndPosition <=  iStrLen) && 
        (iStrStartPosition <= iStrEndPosition))
    {
        if(tc_strcmp(pszInputString, "") != 0)
        {
            int idx        = 0;
            int iNewStrLen = 0;

            iNewStrLen = iStrEndPosition - iStrStartPosition + 2;

            (*ppszCopyedStr) = (char*) MEM_alloc(sizeof(char*) * iNewStrLen);

            while(iStrStartPosition <= iStrEndPosition)
            {
                (*ppszCopyedStr)[idx] = pszInputString[iStrStartPosition-1];
                idx++;
                iStrStartPosition++;
            }

            (*ppszCopyedStr)[idx] = '\0';
        }
    }

    return iRetCode;
}

/**
 * Parses a string containing values seperated by a given separator
 * into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in] pszList          Input list to be parsed
 * @param[in] cSep             Separator character
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by SMA3_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 * @retval INCORRECT_INPUT If list or seprator is Null.
 * @retval MEMORY_ALLOCATION_FAILED If cannot allocate memory.
 */
int SMA3_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
)
{
    int        iRetCode    = ITK_ok;
    int        iCounter    = 0;
    char*      pszToken    = "";
    char*      pszTempList = NULL;

    /* Convert separator (char) to char* - strtok(char* input, Char* sep) */
    char pszSeparator[SMA3_SEPARATOR_LENGTH+1];
    pszSeparator[0] = cSep;
    pszSeparator[1] = '\0';

    /* Validate input */
    if((pszList == NULL) || (cSep == '\0'))
    {
        TC_write_syslog( "ERROR: Missing input data:List passed or seprator is null\n");
    }
    else
    {

        /* Allocating memory to pszTempList */
        pszTempList  = (char*) MEM_alloc(sizeof(char)*((int)tc_strlen(pszList)+1));
        if (pszTempList == NULL)
        {
            TC_write_syslog( "ERROR: Cannot allocate memory\n");
        }
        else
        {
            /* Copying the input string to Null terminated string */
            tc_strcpy(pszTempList, pszList);

            /* Get the first token */
            pszToken = tc_strtok( pszTempList,pszSeparator );

            *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
            /* Tokenize the input string */
            while( pszToken != NULL )
            {
                /* Allocate memory for each string pointer */
                (*pppszValuelist)[iCounter] = (char*) MEM_alloc(sizeof(char)*(((int)tc_strlen(pszToken))+1));
                
                /* Copy the token to the output string pointer */
                tc_strcpy((*pppszValuelist)[iCounter],pszToken);

                /* Count the number of elements in the string */
                iCounter++;
                
                /* Allocating memory to pointer of string (output) as per the number of tokens found */
                *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));
                
                /* Get next token: */
                pszToken = tc_strtok( NULL, pszSeparator );
               
            }

            if(iRetCode == ITK_ok)
            {
                *iCount = iCounter;
            }

        }
            
    }

    /* Free the temporary memory used for tokenizing */
    SMA3_MEM_TCFREE(pszTempList);
    return iRetCode;
}

#ifdef __cplusplus
}
#endif


