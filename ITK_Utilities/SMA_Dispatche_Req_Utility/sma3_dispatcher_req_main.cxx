/*======================================================================================================================================================================
                Copyright 2014  Accenture GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_dispatcher_req_main.cxx

    Description: This File contains functions that are entry point to ECO report generation executable

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-March-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_library.hxx>
#include <sma3_const.hxx>
#include <sma3_errors.hxx>
#include <sma3_create_dispatcher_req.hxx>

int SMA3_get_arg_info( int iArgc, char** ppszArgv, char** ppszItemID, char** ppszRevID, char** ppszRelType, char** ppszDSName,
                       char** ppszDSType, char** ppszPriority, char** ppszProviderName, char** ppszServiceName, char** ppszTransArgInfo,
                       logical* pbByPass, logical* pbNOModify );

int SMA3_init_ITK_login(int argc, char **argv);

void SMA3_display_usage();

/**
 * This function is the main function where program starts execution. It is responsible for the high-level organization of the
 * program's functionality, and try to access command arguments which basically are login credentials when this executable is run.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int ITK_user_main( int iArgCount, char** ppszArgVal)
{
    int     iRetCode        = ITK_ok;
    char*   pszItemID       = NULL;
    char*   pszRevID        = NULL;
    char*   pszRelType      = NULL;
    char*   pszDSName       = NULL;
    char*   pszDSType       = NULL;
    char*   pszPriority     = NULL;
    char*   pszProviderName = NULL;
    char*   pszServiceName  = NULL;
    char*   pszTransArgInfo = NULL;
    logical bByPass         = false;
    logical bNOModify       = false;

    /* display help if user asked for it */
	if ((ITK_ask_cli_argument("-h")) || (iArgCount == 0) )
    {
        SMA3_display_usage();
        exit(0);
    }

    char* pszLogFile = ITK_ask_cli_argument( "-log_file=" );
    /* validating the log file */ 
    if((pszLogFile == NULL) || (tc_strlen(pszLogFile) == 0))
    {
        fprintf(stderr,"\n\nError: Log file not specified.\n");
        SMA3_display_usage();
        exit(0);
    }

    char* pszRepeatFileInfo  = ITK_ask_cli_argument( "-repeat_file=");

    /* validating repeat file */ 
    if((pszRepeatFileInfo == NULL) || (tc_strlen(pszRepeatFileInfo) == 0))
    {
        fprintf(stderr,"\n\nError: Repeat File name not specified.\n");
        SMA3_display_usage();
        exit(0);
    }
    //getch();
    SMA3_ITKCALL( iRetCode, SMA3_get_arg_info( iArgCount, ppszArgVal, &pszItemID, &pszRevID, &pszRelType, &pszDSName,
                                               &pszDSType, &pszPriority, &pszProviderName, &pszServiceName, &pszTransArgInfo,
                                               &bByPass, &bNOModify));

    char* pszConfigFileInfo  = ITK_ask_cli_argument( "-input_file=");

    /* validating input file */ 
    if((pszConfigFileInfo == NULL) || (tc_strlen(pszConfigFileInfo) == 0))
    {
        if(pszItemID == NULL || (tc_strlen(pszItemID) == 0) || pszRevID == NULL || (tc_strlen(pszRevID) == 0) || 
            pszDSType == NULL || (tc_strlen(pszDSType) == 0) || pszPriority == NULL || (tc_strlen(pszPriority) == 0) ||
            pszProviderName == NULL || (tc_strlen(pszProviderName) == 0) || pszServiceName == NULL || (tc_strlen(pszServiceName) == 0))
        {
            fprintf(stderr,"\n\nError: File name not specified.\n");
            SMA3_display_usage();
            exit(0);
        }
    }

    char* pszSeparator  = ITK_ask_cli_argument( "-sep=");

    /* validating Separator */ 
    if((pszSeparator == NULL) || (tc_strlen(pszSeparator) == 0))
    {
        pszSeparator = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(SMA3_STRING_DEFAULT_SEPARATOR) +1)));
        tc_strcpy(pszSeparator, SMA3_STRING_DEFAULT_SEPARATOR);
    }

    SMA3_ITKCALL( iRetCode, SMA3_init_ITK_login(iArgCount, ppszArgVal));

    if(iRetCode != ITK_ok)
    {
        char* pszErrorText = NULL;
        EMH_ask_error_text(iRetCode, &pszErrorText);
        printf("Login Unsuccessful. %s \n", pszErrorText);
        SMA3_MEM_TCFREE( pszErrorText );
    }
    else
    {
        printf("Successfully Logged In Teamcenter \n");
    }

    if(iRetCode == ITK_ok)
    {
        SMA3_ITKCALL( iRetCode, SMA3_create_translation_req(pszConfigFileInfo, pszLogFile, pszRepeatFileInfo, pszSeparator, pszItemID, pszRevID,
                                                                 pszRelType, pszDSName, pszDSType, pszPriority, pszProviderName, pszServiceName,
                                                                 pszTransArgInfo, bByPass, bNOModify));
    }

	SMA3_ITKCALL( iRetCode, ITK_exit_module(FALSE));
    return iRetCode;
}

/**
 * This function retrieves argument information necessary to construct and create translation request.
 * Arguments that can be supplied from the commandline are 
 *  -i=Item ID
 *  -r=Revision ID
 *  -rn=Relation Type
 *  -dn=Dataset Name
 *  -dt=DatasetTypeName
 *  -pr=1 | 2 | 3]
 *  -pn=Translator Provider Name
 *  -tn=Service Name
 *  -ty=Type String[<Argument1>=<Value1>,<Argument2>=<Value2>,.......,<ArgumentN>=<ValueN>]
 *
 * @param[in] iArgCount                 
 * @param[in] ppszArgVal          
 * @param[in] ppszItemID          Item ID
 * @param[in] ppszRevID           Item Revision ID
 * @param[in] ppszRelType         Relation type with which dataset is attached to Item revision
 * @param[in] ppszDSName          Name of dataset attached to Item revision
 * @param[in] ppszDSType          Dataset Type
 * @param[in] ppszPriority
 * @param[in] ppszProviderName
 * @param[in] ppszServiceName
 * @param[in] ppszTransArgInfo
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_get_arg_info
(
    int      iArgCount, 
    char**   ppszArgVal,
    char**   ppszItemID,
    char**   ppszRevID,
    char**   ppszRelType,
    char**   ppszDSName,
    char**   ppszDSType,
    char**   ppszPriority,
    char**   ppszProviderName,
    char**   ppszServiceName,
    char**   ppszTransArgInfo,
    logical* pbByPass,
    logical* pbNOModify
)
{
    int   iRetCode        = ITK_ok;

    (*ppszItemID)       = ITK_ask_cli_argument("-i=");
    (*ppszRevID)        = ITK_ask_cli_argument("-r=");
    (*ppszRelType)      = ITK_ask_cli_argument("-r=n");
    (*ppszDSName)       = ITK_ask_cli_argument("-dn=");
    (*ppszDSType)       = ITK_ask_cli_argument("-dt=");
    (*ppszPriority)     = ITK_ask_cli_argument("-pr=");
    (*ppszProviderName) = ITK_ask_cli_argument("-pn=");
    (*ppszServiceName)  = ITK_ask_cli_argument("-tn=");
    (*ppszTransArgInfo) = ITK_ask_cli_argument("-ty=");

    for(int iDx = 0; iDx < iArgCount; iDx++)
    {
       if(tc_strcmp("-bypass", ppszArgVal[iDx]) == 0)
       {
            (*pbByPass) = true;
            printf("Running utility in bypass mode.\n");
       }

       if(tc_strcmp("-no_mod", ppszArgVal[iDx]) == 0)
       {
            (*pbNOModify) = true;
       }
    }

    return iRetCode;
}

/**
 * This function retrieves login credentials and try to login to Teamcenter with these credential. If not supplied executable
 * performs auto login.
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_init_ITK_login(int argc, char **argv)
{
    int   iRetCode        = ITK_ok;

    /* Gets the user ID */
    char* pszUser  = ITK_ask_cli_argument("-u=");

    /* Gets the user Password */
    char* pszPassword  = ITK_ask_cli_argument("-p=");

    /* Gets the user group */
    char* pszGroup  = ITK_ask_cli_argument("-g=");

    /*  To run against motif libs.*/
    SMA3_ITKCALL(iRetCode, ITK_initialize_text_services(ITK_BATCH_TEXT_MODE));

    /*  Put -h CLI argument here to avoid starting the whole of iman just for a help message! */
    if( ITK_ask_cli_argument( "-h" ) != 0 )
    {
        SMA3_display_usage();
        return iRetCode;
    }

	if(pszGroup == NULL || pszUser == NULL)
	{
		SMA3_ITKCALL( iRetCode, ITK_auto_login());		
	}
	else
	{
		SMA3_ITKCALL( iRetCode, ITK_init_module(pszUser, pszPassword, pszGroup));		
	}

    return iRetCode;
}

/**
 * This function displays the usage of this Executable
 */
void SMA3_display_usage()
{
    printf("     \n\t\t\n Create Dispatcher Request Utility Usage \n");
    printf("     \t\t\n-------------------------------\n");
    printf("           -u                   = <user name> to specify user name\n");
    printf("           -p                   = <password> to specify password\n");
    printf("           -g                   = <group name> to specify group name\n");
    printf("           -log_file            = log_file: Specifies the log file.\n\t\t\t\t  Information about the object will be written\n\t\t\t\t  to the log file.\n");
    printf("           -repeat_file         = repeat_file: Specifies the repeat file.\n\t\t\t\t  All non-processed entries from the input file\n\t\t\t\t  have to be written to the repeat file.\n");
    printf("           -input_file          = input_file: Specifies the input file to be\n\t\t\t\t  processed. It has the format\n\t\t\t\t  ItemID~RevID~DatasetType~DatasetName\n\t\t\t\t  ~RelationName \n\t\t\t\t\t or \n\t\t\t\t  ItemID~RevID~DatasetType~DatasetName\n\t\t\t\t  ~RelationName~Priority~TranslatorProviderName\n\t\t\t\t  ~TranslationServiceName~TypeString1\n\t\t\t\t  ~TypeString2~.......~TypeStringN \n\t\t\t\t  Commented lines start with \"#\". \n\t\t\t\t  These lines will be ignored while processing.\n"); 
    printf("           -sep                 = Specifies the separator to be\n\t\t\t\t  used in the input file. If not specified the\n\t\t\t\t  default will be \"~\".\n");
    printf("           -i                   = Specifies the Item ID.\n\t\t\t\t  This argument is used when dispatcher request\n\t\t\t\t  is created by supplying arguments to the\n\t\t\t\t  command line i.e. without using input file.\n");
    printf("           -r                   = Specifies the Item Revision ID.\n\t\t\t\t  This argument is used when dispatcher request\n\t\t\t\t  is created by supplying arguments to the\n\t\t\t\t  command line i.e. without using input file.\n");
    printf("           -rn                  = Specifies the Relation type which whicht\n\t\t\t\t  datase is attached to Item revision.\n\t\t\t\t  This argument is used when dispatcher request\n\t\t\t\t  is created by supplying arguments to the\n\t\t\t\t  command line i.e. without using input file.\n");
    printf("           -dn                  = Specifies the Dataset name which is attached\n\t\t\t\t  to Item revision.\n\t\t\t\t  This argument is used when dispatcher request\n\t\t\t\t  is created by supplying arguments to the\n\t\t\t\t  command line i.e. without using input file.\n");
    printf("           -dt                  = Specifies the Dataset type which is attached\n\t\t\t\t  to Item revision.\n\t\t\t\t  This argument is used when dispatcher request\n\t\t\t\t  is created by supplying arguments to the\n\t\t\t\t  command line i.e. without using input file.\n");
    printf("           -pr                  = Specifies the Priority for\n\t\t\t\t  translation request. Values can be 1|2|3.\n\t\t\t\t  Default value will be \"2\".\n");
    printf("           -pn                  = Specifies the Translator Provider Name.\n");
    printf("           -tn                  = Specifies the Translation Service Name.\n");
    printf("           -ty                  = Specifies the Translator argument string.\n\t\t\t\t  It has the format\n\t\t\t\t  <Argument1>=<Value1>~<Argument2>=<Value2>~\n\t\t\t\t  .......~<ArgumentN>=<ValueN>.\n\t\t\t\t  If separator is not specified the default\n\t\t\t\t  will be \"~\".\n");
    printf("           -bypass                Run this utility in bypass mode.\n");
    printf("           -no_mod                Run this utility in the mode where the\n\t\t\t\t  last modification date and user will\n\t\t\t\t  not be updated (from the where cutted object).\n");
    printf("           -h                     [Help]\n");
}

#ifdef __cplusplus
}
#endif




