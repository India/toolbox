/*======================================================================================================================================================================
                Copyright 2014  Accenture GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_string_macros.hxx

    Description:  This file contains macros for string's basic operation such as copy, concatination etc.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Mar-14     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef SMA3_STRING_MACROS_HXX
#define SMA3_STRING_MACROS_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <sma3_library.hxx>

#ifdef SMA3_CUST_STRCPY
#undef SMA3_CUST_STRCPY
#endif

#define SMA3_cust_strcpy(pszDestiStr, pszSourceStr){\
            pszDestiStr = (char*)MEM_alloc((int)(sizeof(char)* ( tc_strlen(pszSourceStr) + 1 )));\
            tc_strcpy(pszDestiStr, pszSourceStr);\
}

#ifdef SMA3_CUST_STRCAT
#undef SMA3_CUST_STRCAT
#endif

#define SMA3_cust_strcat(pszDestiStr, pszSourceStr){\
    int iSourceStrLen = (int) tc_strlen(pszSourceStr);\
    int iDestiStrLen = 0;\
    if(pszDestiStr != NULL){\
        iDestiStrLen = (int) tc_strlen (pszDestiStr);\
        pszDestiStr = (char*)MEM_realloc(pszDestiStr, (int)((iSourceStrLen + iDestiStrLen + 1)* sizeof(char)));\
    }\
    else{\
        pszDestiStr = (char*)MEM_alloc((int)(sizeof(char) * (iSourceStrLen + 1)));\
    }\
    if(iDestiStrLen == 0){\
       tc_strcpy(pszDestiStr, pszSourceStr);\
    }\
    else{\
       tc_strcat(pszDestiStr, pszSourceStr);\
       tc_strcat(pszDestiStr, '\0');\
       }\
}

#ifdef SMA3_CUST_UPDATE_STRING_ARRAY
#undef SMA3_CUST_UPDATE_STRING_ARRAY
#endif

#define SMA3_cust_update_string_array(iCurrentArrayLen, ppszStrArray, pszNewValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ppszStrArray = (char**) MEM_alloc(iCurrentArrayLen * sizeof(char*));\
            }\
            else{\
                ppszStrArray = (char**) MEM_realloc(ppszStrArray, iCurrentArrayLen * sizeof(char*));\
            }\
            ppszStrArray[iCurrentArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszNewValue) + 1)* sizeof(char)));\
            tc_strcpy(ppszStrArray[iCurrentArrayLen - 1], pszNewValue);\
}

#endif







