/*======================================================================================================================================================================
                Copyright 2014  Accenture GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_create_dispatcher_req.hxx

    Description:  Header File containing declaration

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-March-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef SMA3_CREATE_DISPATCHER_REQ_HXX
#define SMA3_CREATE_DISPATCHER_REQ_HXX

#include <dispatcher/dispatcher_itk.h>
#include <sma3_library.hxx>
#include <sma3_errors.hxx>

#define SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist){\
                                if((fRepeatFile = fopen(pszRepeatFile, "w")) == NULL ){\
                                    printf("\n** ERROR: Cannot create %s for writing unprocessed contents **\n\n",pszRepeatFile);}\
                                else{\
                                bRepeatFileExist = true;}\
}

#define SMA3_REPORT_OBJ_NOT_FOUND_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, tItemRev, iDSCnt, pszItemID, pszRevID, pszDSType, pszRelType){\
                                            if(tItemRev == NULLTAG){\
                                                fprintf(fLogFile, "Error: Cannot find Item Revision '%s|%s'.\n", pszItemID, pszRevID);\
                                            }\
                                            else if(iDSCnt == 0){\
                                                fprintf(fLogFile, "Error: Cannot find Dataset of type %s attached to Item Revision '%s|%s' with relation %s.\n", pszDSType, pszItemID, pszRevID, pszRelType);;\
                                            }\
                                            fflush(fLogFile);\
                                            fprintf(fRepeatFile, "%s\n", szLine);\
                                            fflush(fRepeatFile);\
}

#define SMA3_REPORT_UNABLE_TO_PARSE_LINE_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszConfigFileInfo){\
                                                    fprintf(fLogFile, "Error: Cannot parse the line \n \t'%s' of file %s. Information is either missing or incorrect. Please refer the utility help for proper usage. \n", szLine, pszConfigFileInfo);\
                                                    fflush(fLogFile);\
                                                    fprintf(fRepeatFile, "%s\n", szLine);\
                                                    fflush(fRepeatFile);\
}

#define SMA3_REPORT_SUCCESS(iRetCode, fLogFile, tDataset, pszItemID, pszRevID){\
                               char* pszDSInfo = NULL;\
                               AOM_ask_value_string(tDataset, SMA3_ATTR_OBJECT_STRING_ATTRIBUTE, &pszDSInfo);\
                               fprintf(fLogFile, "Success: Successfully created translation request for '%s' dataset attached to Item Revision '%s/%s'.\n", pszDSInfo, pszItemID, pszRevID);\
                               SMA3_MEM_TCFREE( pszDSInfo );\
                               fflush(fLogFile);\
}

#define SMA3_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, gszLine){\
                            char* pszErrorText = NOVALUE;\
                            EMH_ask_error_text(iRetCode, &pszErrorText);\
                            fprintf(fLogFile, "Error: %s.\n", pszErrorText);\
                            fflush(fLogFile);\
                            fprintf(fRepeatFile, "%s\n", gszLine);\
                            fflush(fRepeatFile);\
                            SMA3_MEM_TCFREE(pszErrorText);\
}

int SMA3_create_translation_req
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    char*   pszCLItemID,
    char*   pszCLRevID,
    char*   pszCLRelType,
    char*   pszCLDSName,
    char*   pszCLDSType,
    char*   pszCLPriority,
    char*   pszCLProviderName,
    char*   pszCLServiceName,
    char*   pszCLTransArgInfo,
    logical bByPass,
    logical bNOModify
); 

int SMA3_get_ds_info
(
    char*   pszItemID,
    char*   pszRevID,
    char*   pszDSType,
    char*   pszDSName,
    char*   pszRelType, 
    tag_t*  ptItemRev,
    int*    piDatasetCnt,
    tag_t** pptDataset
);

#endif






