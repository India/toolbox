/*======================================================================================================================================================================
                Copyright 2014  Accenture GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_itk_mem_free.hxx

    Description:  This file contains macros for freeing portion of memory allocated with TCe 'MEM_' functions and by TCe itself.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-March-14   Tarun Kumar Singh   Initial Release
   
========================================================================================================================================================================*/
#ifndef SMA3_ITK_MEM_FREE_HXX
#define SMA3_ITK_MEM_FREE_HXX

#include <sma3_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef SMA3_MEM_TCFREE
#undef SMA3_MEM_TCFREE
#endif

// Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc

#define SMA3_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }

#ifdef SMA3_ALLOCATE_TAG_MEM
#undef SMA3_ALLOCATE_TAG_MEM
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation if done for the very first time
*/
#define SMA3_ALLOCATE_TAG_MEM(iRetCode, ptMem, iSize){\
            if(iSize > 0){\
                   if (ptMem == NULL || iSize == 1){\
                        ptMem = (tag_t*) MEM_alloc((int)(sizeof(tag_t)*iSize));}\
                   else{\
                        ptMem = (tag_t*) MEM_realloc(ptMem, (int)((sizeof(tag_t)*iSize)));}\
            }\
            else{\
                iRetCode = INVALID_INPUT_TO_FUNCTION;\
                EMH_store_error(EMH_severity_error, iRetCode);}\
}

#ifdef SMA3_UPDATE_TAG_ARRAY
#undef SMA3_UPDATE_TAG_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation if done for the very first time
*/
#define SMA3_UPDATE_TAG_ARRAY(iCurrentArrayLen, ptArray, tValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ptArray = (tag_t*) MEM_alloc(iCurrentArrayLen * sizeof(tag_t));\
            }\
            else{\
                ptArray = (tag_t*) MEM_realloc(ptArray, iCurrentArrayLen * sizeof(tag_t));\
            }\
            ptArray[iCurrentArrayLen-1]=  tValue;\
}

#endif










