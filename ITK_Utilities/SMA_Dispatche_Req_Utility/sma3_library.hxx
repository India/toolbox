/*======================================================================================================================================================================
                Copyright 2014  Accenture GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_library.hxx

    Description:  Header File containing declarations of function spread across all

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-March-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef SMA3_LIBRARY_HXX
#define SMA3_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <epm/signoff.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me.h>
#include <form.h>
#include <project.h>
#include <folder.h>
#include <pom.h>
#include <tctype.h>
#include <fclasses/tc_date.h>
#include <tc/tc_util.h>
#include <tcinit/tcinit.h>
#include <tccore/item.h>
#include <conio.h>
#include <tccore/license.h>
#include <sma3_const.hxx>
#include <sma3_string_macros.hxx>
#include <sma3_itk_macro.hxx>
#include <sma3_itk_mem_free.hxx>


/*        Utility Functions For Error Logging        */
int SMA3_err_write_to_logfile
(
    char*       pszErrorFunction,
    int         iErrorNumber,
    char*       pszFileName,
    int         iLineNumber
);

int SMA3_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
);


/*        Utility Functions For Objects        */
int SMA3_obj_set_tag_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    tag_t       tPropVal
);

int SMA3_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
);

int SMA3_str_parse_string
(
    const char*  pszList,
    char*        pszSeparator,
    int*         iCount,
    char***      pppszValuelist
);

int SMA3_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
);

char* SMA3_trim( char* pszString );

void SMA3_trim_end(char *pszString);

int SMA3_string_start_with
(
    char*    pszInputString,
    char*    pszStartString,
    logical* pbIsMatch,
    logical* pbInvalidArg
);

/*        Utility Functions For GRM        */
int SMA3_grm_get_all_secondary_obj_of_type
(
    tag_t   tPrimaryObj,
    char*   pszSecType,
    tag_t** pptSecObjects,
    int*    piObjectCount  
);

int SMA3_grm_get_secondary_obj_of_type
(
    tag_t   tPrimaryObj,
    int     iRelCount,
    char**  ppszRelationTypeNames,
    char*   pszSecondaryObjType,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount    
);

#endif





