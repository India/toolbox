/*======================================================================================================================================================================
                Copyright 2014  Accenture GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_object_utilities.cxx

    Description: This file contains function performing various operations on Teamcenter Business Objects

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-March-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_errors.hxx>
#include <sma3_library.hxx>

/**
 * Set the Tag attribute value.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] tPropVal        Value of the property to be set
 *
 * @note The object must be an instance of Application Object class or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int SMA3_obj_set_tag_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    tag_t       tPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    SMA3_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute string value.
     */    
    SMA3_ITKCALL(iRetCode, AOM_set_value_tag(tObject, pszPropertyName, tPropVal));

    /*  Save the object.  */
    SMA3_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     SMA3_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}

/**
 * Set the string attribute value.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] pszPropVal      Value of the property to be set
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int SMA3_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    SMA3_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute string value.
     */    
    SMA3_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszPropertyName, pszPropVal));

    /*  Save the object.  */
    SMA3_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     SMA3_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}

/**
 * Parses a string containing values seperated by a given separator into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in]  pszList         Input list to be parsed
 * @param[in]  pszSeparator    Separator 
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by SMA3_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 */
int SMA3_str_parse_string
(
    const char*  pszList,
    char*        pszSeparator,
    int*         iCount,
    char***      pppszValuelist
)
{
    int    iRetCode     = ITK_ok;
    int    iCounter     = 0;
    char*  pszToken     = NULL;
    char*  pszTempList  = NULL;

     /* Validate input */
    if(pszList == NULL || pszSeparator == NULL)
    {
        TC_write_syslog( "ERROR: Missing input data: List passed or seprator is null\n");
    }
    else
    {
        /* Copying the input string to Null terminated string */
        SMA3_cust_strcpy(pszTempList, pszList);

        /* Get the first token */
        pszToken = tc_strtok( pszTempList, pszSeparator );

        *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
        /* Tokenize the input string */
        while( pszToken != NULL )
        {
            /* Copy the token to the output string pointer */
            SMA3_cust_strcpy((*pppszValuelist)[iCounter],pszToken);

            /* Count the number of elements in the string */
            iCounter++;
                
            /* Allocating memory to pointer of string (output) as per the number of tokens found */
            *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));

            /* Get next token: */
            pszToken = tc_strtok( NULL, pszSeparator );
        }

        (*iCount) = iCounter;
    }

    /* Free the temporary memory used for tokenizing */
    SMA3_MEM_TCFREE(pszTempList);
    return iRetCode;
}

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int SMA3_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
	tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    SMA3_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    SMA3_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    SMA3_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}

/**
 * Function to Strips leading and trailing space. 
 *
 * @param[in]  pszString  String to trim white spaces 
 * @param[out] Pointer to Trimmed string 
 */
char* SMA3_trim( char* pszString )
{
  if (pszString == NULL)
    return(NULL);

  SMA3_trim_end(pszString);

    /*===================================================================*/
    /* Skip over spaces, and return the address of the first non-space   */
    /* character. If there are no leading spaces, this just returns str. */
    /*===================================================================*/

  return &pszString[strspn( pszString, " \t" )];

} 

/**
 * Function to Strips all trailing spaces from the given null-terminated string
 *
 * @param[in/out] pszString  String to trim white spaces
 *
 */

void SMA3_trim_end(char *pszString)
{
  int iLen = (int)(tc_strlen( pszString ) - 1);

  while (iLen >= 0  &&  ((pszString[iLen] == ' ') || 
          (pszString[iLen] == '\t') || (pszString[iLen] == '\n')))
      --iLen;

  pszString[iLen + 1] = '\0';

}

/**
 * This function checks if input string starts with start string. i.e. 'pszInputString' starts with 'pszStartString' 
 *
 * @param[in]  pszInputString  Actual input string
 * @param[in]  pszStartString  String to match
 * @param[out] pbIsMatch       'true' if input string starts with given start string else 'False'
 * @param[out] pbInvalidArg    'true' if input string arguments are NULL
 *
 * @return ITK_ok is successful else ITK return code.
 */
int SMA3_string_start_with
(
    char*    pszInputString,
    char*    pszStartString,
    logical* pbIsMatch,
    logical* pbInvalidArg
)
{
    int  iRetCode         = ITK_ok;
    int  iStringLen       = 0;
    
    if ((pszInputString != NULL) && (pszStartString != NULL) )
    {
        if((tc_strcmp(pszInputString, "") != 0) && (tc_strcmp(pszStartString, "") != 0))
        {
            int  iCount         = 0;
            int  iInputStrLen   = 0;
            int  iStartStrLen   = 0;

            /* Initializing the Out Parameter to this function. */
            *pbIsMatch = false;
                    
            iInputStrLen = (int)tc_strlen(pszInputString);
            iStartStrLen = (int)tc_strlen(pszStartString);

            if(iStartStrLen > iInputStrLen)
            {
                *pbIsMatch = false;
            }
            else
            {
                logical bFound  = true;

                for(int iDx = 0; iDx < iStartStrLen && bFound == true; iDx++)
                {
                    if(pszInputString[iDx] == pszStartString[iDx])
                    {
                        bFound = true;

                    }
                    else
                    {
                        bFound = false;
                    }                           
                }

                (*pbIsMatch) = bFound;
            }
        }
    }       
    else
    {
        (*pbInvalidArg) = true;        
    }
      
    return iRetCode;
}

#ifdef __cplusplus
}
#endif


