/*======================================================================================================================================================================
                Copyright 2014  Accenture GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_create_dispatcher_req.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-March-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <sma3_errors.hxx>
#include <sma3_library.hxx>
#include <sma3_create_dispatcher_req.hxx>

/**
 * The utility reads that input file that has the following format: 
 *
 *  ItemID;RevID;DatasetType;DatasetName;RelationName
 *            or
 *  ItemID;RevID;DatasetType;DatasetName;RelationName;TranslationServiceName;Priority;TranslatorProviderName;TypeString1;TypeString2;.......;TypeStringN
 *
 * @note
 *      If Translator argument information is not provided in the input file then the Translator arguments supplied from command line gets the
 *       precedence else translator argument supplied in each input line of the input file will get the priority
 *
 * As a default separator "~" is used (if not specified). 
 *
 * @note:Utility can be executed using following additional options:
 *       -no_mod
 *            If this argument is provided the last modification date and last modification user will not change.
 *       -bypass
 *            Run this utility in bypass mode.
 *
 * @param[in] pszConfigFileInfo  Name of the Configuration File to be processed
 * @param[in] pszLogFile         Name of Log file
 * @param[in] pszRepeatFile      Name of the Repeat File
 * @param[in] pszSeparator       Separator used in Configuration File 
 * @param[in] pszCLItemID        Item ID
 * @param[in] pszCLRevID         Item Revision ID
 * @param[in] pszCLRelType       Relation type with which dataset is attached to Item revision
 * @param[in] pszCLDSName        Name of dataset attached to Item revision
 * @param[in] pszCLDSType        Dataset Type
 * @param[in] pszCLPriority 
 * @param[in] pszCLProviderName 
 * @param[in] pszCLServiceName 
 * @param[in] pszCLTransArgInfo 
 * @param[in] bByPass            'true' if By Pass is set else 'false'
 * @param[in] bNOModify          If 'true' then last modification date and user of the objects will not to be changed
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_create_translation_req
(
    char*   pszConfigFileInfo,
    char*   pszLogFile,
    char*   pszRepeatFile,
    char*   pszSeparator,
    char*   pszCLItemID,
    char*   pszCLRevID,
    char*   pszCLRelType,
    char*   pszCLDSName,
    char*   pszCLDSType,
    char*   pszCLPriority,
    char*   pszCLProviderName,
    char*   pszCLServiceName,
    char*   pszCLTransArgInfo,
    logical bByPass,
    logical bNOModify
)
{
    int     iRetCode           = ITK_ok;
    int     iTransArgCnt       = 0;
    int     iPriority          = 0;
    tag_t   tItemRev           = NULLTAG;
    tag_t   tTransReq          = NULLTAG;
    char    szLine[MAX_CHARS]  = {'\0'};
    char**  ppszTransArgList   = NULL;
    logical bRepeatFileExist   = false;
    FILE*   fCfgFile;
    FILE*   fLogFile;
    FILE*   fRepeatFile;

    if(pszCLPriority == NULL || tc_strlen(pszCLPriority) == 0)
    {
         iPriority = 2;
    }
    else
    {
          iPriority = atoi ( pszCLPriority );
    }    
    /* Parse Translator Argument Information supplied through command prompt */
    if(pszCLTransArgInfo != NULL || tc_strlen(pszCLTransArgInfo) != 0)
    {
        SMA3_ITKCALL(iRetCode, SMA3_str_parse_string(pszCLTransArgInfo, pszSeparator, &iTransArgCnt, &ppszTransArgList));
    }
    /* Open Log file to write success or error messages for reference */
    if ((fLogFile = fopen(pszLogFile, "w")) == NULL )
    {
        printf("\n** ERROR: Cannot create %s for writing log  **\n\n",pszLogFile);

        return iRetCode;
    }
    else
    {
        if(bByPass == true || bNOModify == true)
        {
            if(bNOModify == true)
            {
                /*-------------------------------------*/
                /* Do not update date fields on change */
                /*-------------------------------------*/
                SMA3_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_attr_update, false, 0,0,0, NULL));
        
                /*----------------------*/
                /* Bypass access checks */
                /*----------------------*/
                SMA3_ITKCALL(iRetCode, POM_set_env_info(POM_bypass_access_check, true, 0,0,0, NULL));
            }

            SMA3_ITKCALL(iRetCode, ITK_set_bypass(true));
        }
    }

    if(pszConfigFileInfo == NULL || tc_strlen(pszConfigFileInfo) == 0)
    {
        int    iDSCnt    = 0;
        tag_t* ptDataset = NULL;

        SMA3_ITKCALL(iRetCode, SMA3_get_ds_info(pszCLItemID, pszCLRevID, pszCLDSType, pszCLDSName, pszCLRelType, &tItemRev, &iDSCnt, &ptDataset));

        if(iDSCnt > 0)
        {
            /* Create translation requests on each of the attached datasets */
            for(int iJx = 0; iJx < iDSCnt; iJx++)
            {
                SMA3_ITKCALL(iRetCode, DISPATCHER_create_request( pszCLProviderName, pszCLServiceName, iPriority, 0, 0, 0, 1, &tItemRev,
                                                                  &(ptDataset[iJx]), iTransArgCnt, (const char**)ppszTransArgList, 
                                                                   "", 0, 0, 0, &tTransReq ));
                if(iRetCode != ITK_ok)
                {
                     /* Report Error */
                    if(bRepeatFileExist == false)
                    {
                        SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                    }

                    SMA3_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, szLine);
                }
                else
                {
                    SMA3_REPORT_SUCCESS(iRetCode, fLogFile, ptDataset[iJx], pszCLItemID, pszCLRevID);
                }
            }
        }
        else
        {
            /* Report Error */
            if(bRepeatFileExist == false)
            {
                SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
            }

            if(iRetCode != ITK_ok)
            {
                SMA3_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, szLine); 
            }

            if(iRetCode == ITK_ok)
            {
                SMA3_REPORT_OBJ_NOT_FOUND_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, tItemRev, iDSCnt, pszCLItemID, pszCLRevID, pszCLDSType, pszCLRelType)
            }
        }

        SMA3_MEM_TCFREE( ptDataset );
    }
    else
    {
        if ((fCfgFile = fopen(pszConfigFileInfo, "r")) == NULL )
        {
            printf("\n** ERROR: Cannot open %s for reading - exiting **\n\n", pszConfigFileInfo);
            return iRetCode;
        }
        else
        {
            while (fgets(szLine, MAX_CHARS, fCfgFile) != NULL)
            {
                if ((szLine[0] != '#') && (szLine[0] != '\0') && (szLine[0] != '\n'))
                {
                    int     iInfoCnt          = 0;
                    int     iNoOfDS           = 0;
                    tag_t*  ptDataset         = NULL;
                    char*   pszProviderName   = NULL;
                    char*   pszServiceName    = NULL;
                    char**  ppszInfo          = NULL;
                    
                    /*Initializing reusable variable */
                    tItemRev = NULLTAG;

                    /*================================================*/
                    /* Trim the line to remove leading and trailing   */
                    /* spaces and discard the \n on the end.          */
                    /*================================================*/
                    tc_strcpy(szLine, SMA3_trim(szLine));

                    SMA3_ITKCALL(iRetCode, SMA3_str_parse_string(szLine, pszSeparator, &iInfoCnt, &ppszInfo));

                    if(iInfoCnt >= 5)
                    {
                        SMA3_ITKCALL(iRetCode, SMA3_get_ds_info(ppszInfo[0], ppszInfo[1], ppszInfo[2], ppszInfo[3], ppszInfo[4], &tItemRev, &iNoOfDS, &ptDataset));

                        if(iRetCode == ITK_ok)
                        {
                            if((ppszInfo[5] != NULL || tc_strlen(ppszInfo[5]) != 0) && iInfoCnt >= 8)
                            {
                                iPriority = atoi ( ppszInfo[5] );
                            }

                            if((ppszInfo[6] != NULL || tc_strlen(ppszInfo[6]) != 0) && iInfoCnt >= 8)
                            {
                                SMA3_cust_strcpy(pszProviderName, ppszInfo[6]);
                            }
                            else
                            {
                                SMA3_cust_strcpy(pszProviderName, pszCLProviderName);
                            }

                            if((ppszInfo[7] != NULL || tc_strlen(ppszInfo[7]) != 0) && iInfoCnt >= 8)
                            {
                                SMA3_cust_strcpy(pszServiceName, ppszInfo[6]);
                            }
                            else
                            {
                                SMA3_cust_strcpy(pszServiceName, pszCLServiceName);
                            }

                            for(int iRx = 8; iRx < iInfoCnt; iRx++)
                            {
                                if(iRx == 8)
                                {
                                    /* Overriding the Translator arguments supplied from command prompt with the arguments supplied in input file. */
                                    SMA3_MEM_TCFREE( ppszTransArgList );
                                }

                                SMA3_cust_update_string_array(iTransArgCnt, ppszTransArgList, ppszInfo[iRx]);
                            }
                        }

                        if(iNoOfDS > 0)
                        {
                            if(pszProviderName == NULL || tc_strlen(pszProviderName) == 0 ||
                                pszServiceName == NULL || tc_strlen(pszProviderName) == 0)
                            {
                                /* Report Error */
                                if(bRepeatFileExist == false)
                                {
                                    SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                }
                               
                                fprintf(fLogFile, "Error: Either Translator Provider or Service Provider name is empty.\n");                                
                                fflush(fLogFile);
                            }
                            else
                            {
                                /* Create translation requests on each of the attached datasets */
                                for(int iJx = 0; iJx < iNoOfDS; iJx++)
                                {
                                    SMA3_ITKCALL(iRetCode, DISPATCHER_create_request( pszProviderName, pszServiceName, iPriority, 0, 0, 0, 1, &tItemRev,
                                                                                      &(ptDataset[iJx]), iTransArgCnt, (const char**)ppszTransArgList, 
                                                                                       SMA3_ETS_ARG_VALUE_REQUEST_TYPE_CL, 0, 0, 0, &tTransReq ));
                                    if(iRetCode != ITK_ok)
                                    {
                                         /* Report Error */
                                        if(bRepeatFileExist == false)
                                        {
                                            SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                                        }

                                        SMA3_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, szLine);
                                    }
                                    else
                                    {
                                        SMA3_REPORT_SUCCESS(iRetCode, fLogFile, ptDataset[iJx], ppszInfo[0], ppszInfo[1]);
                                    }
                                }
                            }
                        }
                        else
                        {
                             /* Report Error */
                            if(bRepeatFileExist == false)
                            {
                                SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                            }

                            if(iRetCode != ITK_ok)
                            {
                                SMA3_REPORT_ERROR(iRetCode, fLogFile, fRepeatFile, szLine); 
                            }

                            if(iRetCode == ITK_ok)
                            {
                                SMA3_REPORT_OBJ_NOT_FOUND_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, tItemRev, iNoOfDS, ppszInfo[0], ppszInfo[1], ppszInfo[2], ppszInfo[4])
                            }
                        }
                    }
                    else
                    {
                        /* Report Error */
                        if(bRepeatFileExist == false)
                        {
                            SMA3_OPEN_REPEAT_FILE(pszRepeatFile, fRepeatFile, bRepeatFileExist);
                        }

                        SMA3_REPORT_UNABLE_TO_PARSE_LINE_ERROR(iRetCode, fLogFile, fRepeatFile, szLine, pszConfigFileInfo);
                    }

                    iRetCode = ITK_ok;            
                    
                    SMA3_MEM_TCFREE( ptDataset );
                    SMA3_MEM_TCFREE( pszProviderName );
                    SMA3_MEM_TCFREE( pszServiceName );
                    SMA3_MEM_TCFREE( ppszInfo );
                }
            } 

            if(bByPass == true)
            {
                ITK_set_bypass(false);
            }
        }

        if(bRepeatFileExist == true)
        {
            fclose( fRepeatFile );
        }

        fclose( fCfgFile );
        fclose( fLogFile );
    }

    SMA3_MEM_TCFREE( ppszTransArgList );
    return iRetCode;
}

/**
 * The function get the Item revision tag and valid datasets types which are associated with Item revision with relation type which is
 * an input argument. If dataset name is provided as an input argument then function will retrieve datasets with this name. 
 *
 * @param[in]  pszItemID     Item ID
 * @param[in]  pszRevID      Item Revision ID
 * @param[in]  pszDSType     Dataset Type
 * @param[in]  pszDSName     Name of dataset which is attached to Item revision
 * @param[in]  pszRelType    Relation type with which dataset is attached to Item revision
 * @param[out] ptItemRev     Tag of Item revision found in the database.
 * @param[out] piDatasetCnt  Count of valid datasets
 * @param[out] pptDataset    Valid dataset tag list
 *
 * @retval ITK_ok if error text retrieved successfully.
 */
int SMA3_get_ds_info
(
    char*   pszItemID,
    char*   pszRevID,
    char*   pszDSType,
    char*   pszDSName,
    char*   pszRelType, 
    tag_t*  ptItemRev,
    int*    piDatasetCnt,
    tag_t** pptDataset
)
{
    int    iRetCode     = ITK_ok;
    int    iSecObjCnt   = 0;
    int    iRelCount    = 0;
    tag_t  tItem        = NULLTAG;
    tag_t  tItemRev     = NULLTAG;
    tag_t* ptSecObjCnt  = NULL; 

    /* Initializing the Out Parameter to this function. */
    (*piDatasetCnt) = 0;
    (*ptItemRev)    = NULLTAG;
    (*pptDataset)   = NULL;

    SMA3_ITKCALL(iRetCode, ITEM_find_item(pszItemID, &tItem));

    SMA3_ITKCALL(iRetCode, ITEM_find_revision(tItem, pszRevID, ptItemRev));

    if(pszRelType != NULL && tc_strlen(pszRelType) != 0)
    {
        iRelCount = 1;
    }

    SMA3_ITKCALL(iRetCode, SMA3_grm_get_secondary_obj_of_type((*ptItemRev), iRelCount, &pszRelType, pszDSType, &ptSecObjCnt, &iSecObjCnt));

    for(int iDx = 0; iDx < iSecObjCnt && iRetCode == ITK_ok; iDx++)
    {
        logical bIsValid = false;

        if(pszDSName == NULL || tc_strlen(pszDSName) == 0)
        {
            bIsValid = true;
        }
        else
        {
            char* pszName  = NULL;

            SMA3_ITKCALL(iRetCode, AOM_ask_value_string(ptSecObjCnt[iDx], SMA3_ATTR_OBJECT_NAME_ATTRIBUTE, &pszName));

            if(tc_strcmp(pszName, pszDSName) == 0)
            {
                bIsValid = true;
            }

            SMA3_MEM_TCFREE( pszName );
        }

        if(bIsValid == true)
        {
            SMA3_UPDATE_TAG_ARRAY((*piDatasetCnt), (*pptDataset), ptSecObjCnt[iDx]);
        }
    }

    SMA3_MEM_TCFREE( ptSecObjCnt );
    return iRetCode;
}


#ifdef __cplusplus
}
#endif


