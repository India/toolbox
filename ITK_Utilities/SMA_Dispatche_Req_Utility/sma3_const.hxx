/*======================================================================================================================================================================
                Copyright 2014  Accenture GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: sma3_const.hxx

    Description:  Header File for constants used during scope of the executable.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-March-14   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef SMA3_CONST_HXX
#define SMA3_CONST_HXX

#define MAX_CHARS                                               300

/* Utility Argument Names/Values */
#define SMA3_ARG_VAL_ADD_MODE                                     "add"
#define SMA3_ARG_VAL_REMOVE_MODE                                  "remove"

/* Date Time Format */
#define SMA3_DATE_TIME_FORMAT_FILENAME                            "%y%m%d%H%M%S"

/* Attributes  */
#define SMA3_ATTR_ITEM_ID                                         "item_id"
#define SMA3_ATTR_OBJECT_NAME_ATTRIBUTE                           "object_name"
#define SMA3_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define SMA3_ATTR_OWNING_PROJECT                                  "owning_project"
#define SMA3_ATTR_LICENSE_LIST                                    "license_list"
#define SMA3_ATTR_IP_CLASSIFICATION                               "ip_classification"
#define SMA3_ATTR_ACTIVE_SEQ                                      "active_seq"

/* Relation    */
#define SMA3_IMAN_MASTER_FORM                                     "IMAN_master_form"
#define SMA3_TC_DEFAULT_ARRANGEMENT                               "TC_DefaultArrangement"
#define SMA3_TC_ARRANGEMENT                                       "TC_Arrangement"

/* Dispatcher Request Arguments */
#define SMA3_ETS_ARG_VALUE_REQUEST_TYPE_CL                        "COMMAND_LINE"

/* Separators */
#define SMA3_STRING_SINGLE_SPACE								  " "
#define SMA3_STRING_DEFAULT_SEPARATOR	      					  "~"
#define SMA3_CHARACTER_COLON                                      ':'


#define NOVALUE                                                   "NoValue"
#define DEBUG                                                     "true"

#define SMA3_SEPARATOR_LENGTH                                     1
#define SMA3_NUMERIC_ERROR_LENGTH                                 12


#endif


