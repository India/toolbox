REM Make sure you installed gli_kill_installer.exe once before running this script!
set TEAMCENTER_VERSION=V10000.1.0.42_20150727.00
kill java.exe
del /F/Q/S %temp%
del /F/Q/S C:\temp
if exist "%temp%"\%TEAMCENTER_VERSION% rmdir/S/Q  "%temp%"\%TEAMCENTER_VERSION%
if exist "%USERPROFILE%\FSCCache" rmdir/S/Q  %USERPROFILE%\FSCCache
if exist "%USERPROFILE%\FCCCache" rmdir/S/Q  %USERPROFILE%\FCCCache
if exist "%USERPROFILE%\Teamcenter" rmdir/S/Q  %USERPROFILE%\Teamcenter
