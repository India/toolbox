@echo off
setlocal
REM Make sure you updated the following paths before running this script!

REM Blanks are allowed to specify the service name, don't use quotes
set DISH_SERVICE=DISH_LMtec
set FSF_SERVICE=FSF_LMtec
set IDOL_SERVICE=IDOL_LMtec
set AUTONOMY_ROOT=C:\plm\tc101\autonomy\autonomy_LMtec
REM Don't modify anything below here

REM if option is given, then the option only is executed, if not given all options are executed
if x%1==xstop goto STOP_SERVICE
if x%1==xcleanup goto CLEANUP
if x%1==xstart goto START_SERVICE

:STOP_SERVICE
REM stop services
net stop "%DISH_SERVICE%"
net stop "%IDOL_SERVICE%"
net stop "%FSF_SERVICE%"
if x%1==xstop goto FINISHED

:CLEANUP
REM cleanup dish files
if exist "%AUTONOMY_ROOT%\dish\DishDS.db" del /F/Q/S "%AUTONOMY_ROOT%\dish\DishDS.db"
if exist "%AUTONOMY_ROOT%\dish\portinfo.dat" del /F/Q/S "%AUTONOMY_ROOT%\dish\portinfo.dat"
if exist "%AUTONOMY_ROOT%\dish\license.log" del /F/Q/S "%AUTONOMY_ROOT%\dish\license.log"
if exist "%AUTONOMY_ROOT%\dish\service.log" del /F/Q/S "%AUTONOMY_ROOT%\dish\service.log"
if exist "%AUTONOMY_ROOT%\dish\*DiSHcfg.log" del /F/Q/S "%AUTONOMY_ROOT%\dish\*DiSHcfg.log"
if exist "%AUTONOMY_ROOT%\dish\*.str" del /F/Q/S "%AUTONOMY_ROOT%\dish\*.str"
if exist "%AUTONOMY_ROOT%\dish\*.lck" del /F/Q/S "%AUTONOMY_ROOT%\dish\*.lck"
REM cleanup dish folders
if exist "%AUTONOMY_ROOT%\dish\logs" rmdir/S/Q  "%AUTONOMY_ROOT%\dish\logs"
if exist "%AUTONOMY_ROOT%\dish\graphs" rmdir/S/Q  "%AUTONOMY_ROOT%\dish\graphs"
if exist "%AUTONOMY_ROOT%\dish\errors" rmdir/S/Q  "%AUTONOMY_ROOT%\dish\errors"
if exist "%AUTONOMY_ROOT%\dish\documentTracking" rmdir/S/Q  "%AUTONOMY_ROOT%\dish\documentTracking"
if exist "%AUTONOMY_ROOT%\dish\audit" rmdir/S/Q  "%AUTONOMY_ROOT%\dish\audit"
if exist "%AUTONOMY_ROOT%\dish\license" rmdir/S/Q  "%AUTONOMY_ROOT%\dish\license"
if exist "%AUTONOMY_ROOT%\dish\uid" rmdir/S/Q  "%AUTONOMY_ROOT%\dish\uid"

REM cleanup idol files
if exist "%AUTONOMY_ROOT%\idol\portinfo.dat" del /F/Q/S "%AUTONOMY_ROOT%\idol\portinfo.dat"
if exist "%AUTONOMY_ROOT%\idol\license.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\license.log"
if exist "%AUTONOMY_ROOT%\idol\service.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\service.log"
if exist "%AUTONOMY_ROOT%\idol\*.str" del /F/Q/S "%AUTONOMY_ROOT%\idol\*.str"
if exist "%AUTONOMY_ROOT%\idol\*.lck" del /F/Q/S "%AUTONOMY_ROOT%\idol\*.lck"
REM cleanup idol folders
if exist "%AUTONOMY_ROOT%\idol\logs" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\logs"
if exist "%AUTONOMY_ROOT%\idol\license" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\license"
if exist "%AUTONOMY_ROOT%\idol\uid" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\uid"

REM cleanup idol\content files
if exist "%AUTONOMY_ROOT%\idol\content\portinfo.dat" del /F/Q/S "%AUTONOMY_ROOT%\idol\content\portinfo.dat"
if exist "%AUTONOMY_ROOT%\idol\content\license.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\content\license.log"
if exist "%AUTONOMY_ROOT%\idol\content\service.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\content\service.log"
if exist "%AUTONOMY_ROOT%\idol\content\*.str" del /F/Q/S "%AUTONOMY_ROOT%\idol\content\*.str"
if exist "%AUTONOMY_ROOT%\idol\content\*.lck" del /F/Q/S "%AUTONOMY_ROOT%\idol\content\*.lck"
REM cleanup idol\content folders
if exist "%AUTONOMY_ROOT%\idol\content\license" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\content\license"
if exist "%AUTONOMY_ROOT%\idol\content\uid" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\content\uid"
REM cleanup idol\community files
if exist "%AUTONOMY_ROOT%\idol\community\portinfo.dat" del /F/Q/S "%AUTONOMY_ROOT%\idol\community\portinfo.dat"
if exist "%AUTONOMY_ROOT%\idol\community\license.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\community\license.log"
if exist "%AUTONOMY_ROOT%\idol\community\service.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\community\service.log"
if exist "%AUTONOMY_ROOT%\idol\community\*.str" del /F/Q/S "%AUTONOMY_ROOT%\idol\community\*.str"
if exist "%AUTONOMY_ROOT%\idol\community\*.lck" del /F/Q/S "%AUTONOMY_ROOT%\idol\community\*.lck"
REM cleanup idol\community folders
if exist "%AUTONOMY_ROOT%\idol\community\license" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\community\license"
if exist "%AUTONOMY_ROOT%\idol\community\uid" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\community\uid"
REM cleanup idol\category files
if exist "%AUTONOMY_ROOT%\idol\category\portinfo.dat" del /F/Q/S "%AUTONOMY_ROOT%\idol\category\portinfo.dat"
if exist "%AUTONOMY_ROOT%\idol\category\license.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\category\license.log"
if exist "%AUTONOMY_ROOT%\idol\category\service.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\category\service.log"
if exist "%AUTONOMY_ROOT%\idol\category\*.str" del /F/Q/S "%AUTONOMY_ROOT%\idol\category\*.str"
if exist "%AUTONOMY_ROOT%\idol\category\*.lck" del /F/Q/S "%AUTONOMY_ROOT%\idol\category\*.lck"
REM cleanup idol\category folders
if exist "%AUTONOMY_ROOT%\idol\category\license" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\category\license"
if exist "%AUTONOMY_ROOT%\idol\category\uid" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\category\uid"
REM cleanup idol\view files
if exist "%AUTONOMY_ROOT%\idol\view\portinfo.dat" del /F/Q/S "%AUTONOMY_ROOT%\idol\view\portinfo.dat"
if exist "%AUTONOMY_ROOT%\idol\view\license.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\view\license.log"
if exist "%AUTONOMY_ROOT%\idol\view\service.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\view\service.log"
if exist "%AUTONOMY_ROOT%\idol\view\*.str" del /F/Q/S "%AUTONOMY_ROOT%\idol\view\*.str"
if exist "%AUTONOMY_ROOT%\idol\view\*.lck" del /F/Q/S "%AUTONOMY_ROOT%\idol\view\*.lck"
REM cleanup idol\view folders
if exist "%AUTONOMY_ROOT%\idol\view\license" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\view\license"
if exist "%AUTONOMY_ROOT%\idol\view\uid" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\view\uid"
REM cleanup idol\agentstore files
if exist "%AUTONOMY_ROOT%\idol\agentstore\portinfo.dat" del /F/Q/S "%AUTONOMY_ROOT%\idol\agentstore\portinfo.dat"
if exist "%AUTONOMY_ROOT%\idol\agentstore\license.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\agentstore\license.log"
if exist "%AUTONOMY_ROOT%\idol\agentstore\service.log" del /F/Q/S "%AUTONOMY_ROOT%\idol\agentstore\service.log"
if exist "%AUTONOMY_ROOT%\idol\agentstore\*.str" del /F/Q/S "%AUTONOMY_ROOT%\idol\agentstore\*.str"
if exist "%AUTONOMY_ROOT%\idol\agentstore\*.lck" del /F/Q/S "%AUTONOMY_ROOT%\idol\agentstore\*.lck"
REM cleanup idol\agentstore folders
if exist "%AUTONOMY_ROOT%\idol\agentstore\license" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\agentstore\license"
if exist "%AUTONOMY_ROOT%\idol\agentstore\uid" rmdir/S/Q  "%AUTONOMY_ROOT%\idol\agentstore\uid"
if x%1==xcleanup goto FINISHED

:START_SERVICE
REM start services
net start "%DISH_SERVICE%"
net start "%IDOL_SERVICE%"
net start "%FSF_SERVICE%"

:FINISHED
endlocal