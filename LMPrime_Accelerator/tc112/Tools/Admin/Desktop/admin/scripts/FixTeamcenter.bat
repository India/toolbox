REM Make sure you installed gli_kill_installer.exe once before running this script!
set TEAMCENTER_VERSION=V10000.1.0.50_20151013.00
set FSC_SERVICE_NAME=Teamcenter FSC Service FSC_DEUSRV01_Administrator
set SERVER_MANAGER_SERVICE_NAME=TeamcenterServerManager_plmprd

:STOP
::net stop "Teamcenter Dispatcher Client %TEAMCENTER_VERSION%"
::net stop "Teamcenter Dispatcher Module %TEAMCENTER_VERSION%"
::net stop "Teamcenter Dispatcher Scheduler %TEAMCENTER_VERSION%"
::net stop "taskmonitor"
::net stop "actionmgrd"
::net stop "subscripmgrd"
net stop "%SERVER_MANAGER_SERVICE_NAME%"
net stop "%FSC_SERVICE_NAME%"
kill java.exe
kill tao_imr_locator.exe
kill tao_imr_activator.exe
if exist CleanupAutonomy.bat call CleanupAutonomy.bat stop

:CLEANUP
del /F/Q/S "%temp%"
del /F/Q/S C:\temp
if exist "%TC_TMP_DIR%" del /F/Q/S "%TC_TMP_DIR%"
if exist "%temp%"\%TEAMCENTER_VERSION% rmdir/S/Q  "%temp%"\%TEAMCENTER_VERSION%
if exist "%USERPROFILE%\FSCCache" rmdir/S/Q  %USERPROFILE%\FSCCache
if exist "%USERPROFILE%\FCCCache" rmdir/S/Q  %USERPROFILE%\FCCCache
if exist "%USERPROFILE%\Teamcenter" rmdir/S/Q  %USERPROFILE%\Teamcenter
if exist CleanupAutonomy.bat call CleanupAutonomy.bat cleanup

net start "%FSC_SERVICE_NAME%"

if x%1==x goto RESTART
echo.
echo Services have been stopped and temporary files are removed. Click any key to re-start the services
pause

:RESTART
net start "%SERVER_MANAGER_SERVICE_NAME%"
::net start "taskmonitor"
::net start "subscripmgrd"
::net start "actionmgrd"
::net start "Teamcenter Dispatcher Scheduler %TEAMCENTER_VERSION%"
::net start "Teamcenter Dispatcher Module %TEAMCENTER_VERSION%"
::net start "Teamcenter Dispatcher Client %TEAMCENTER_VERSION%"
if exist CleanupAutonomy.bat call CleanupAutonomy.bat start
