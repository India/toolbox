@echo off
setlocal
set CURRENT_DIR=%~dp0
REM e.g. 1.0.0
set CURRENT_RELEASE=%1
set TEAMCENTER_VERSION=V11000.2.0.21_20160719.00

REM this script is used to start release_<CURRENT_RELEASE>.cmd and write a log file at the same time using tee utility.
if not exist tee.exe (
    echo WARNING: tee.exe was not found in this directory! Starting process wihtout writing a log file...
    echo.
    %CURRENT_DIR%release_%CURRENT_RELEASE%.cmd %*
    goto :EOF
)
if not exist %CURRENT_DIR%logs mkdir %CURRENT_DIR%logs
if not exist %CURRENT_DIR%logs\%COMPUTERNAME% mkdir %CURRENT_DIR%logs\%COMPUTERNAME%
set LOG_FILE=%CURRENT_DIR%logs\%COMPUTERNAME%\install_release_%CURRENT_RELEASE%_%computername%_%random%.log
%CURRENT_DIR%release_%CURRENT_RELEASE%.cmd %* | %CURRENT_DIR%tee -a %LOG_FILE%
endlocal