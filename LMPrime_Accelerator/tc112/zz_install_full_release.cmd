@echo off
setlocal
set MODE=%1
set IS_SILENT=%2
if x%MODE%==x set MODE=FULL
if x%IS_SILENT%==xSILENT set SUPPRESS_PAUSE=TRUE

set thisdir=%~dp0

echo #################################################
echo Installing release 2.0.0...
echo #################################################
echo.
call %thisdir%install_release.cmd 2.0.0 %MODE% IS_OBSOLETE
echo.
echo #################################################
echo Installing release 2.0.0...
echo #################################################
echo.
call %thisdir%install_release.cmd 2.0.0 %MODE%
echo.
endlocal