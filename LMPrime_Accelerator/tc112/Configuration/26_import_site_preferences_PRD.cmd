@echo off

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import site specific site preferences for PRD
echo.
echo preferences_manager -u=infodba -p=%DBAPASS_TMP% -g=dba -mode=import -file=Preferences\site_preferences_PRD.xml -scope=SITE -action=OVERRIDE
preferences_manager -u=infodba -p=%DBAPASS% -g=dba -mode=import -file=Preferences\site_preferences_PRD.xml -scope=SITE -action=OVERRIDE

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
