@echo off

set FILENAME=%1

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
if not exist "Preferences\%FILENAME%.txt" (
    echo ERROR: File "Preferences\%FILENAME%.txt" does not exist.
    goto :EOF
)

REM Delete obsolete preference definitions
echo.
echo preferences_manager -u=infodba -p=%DBAPASS_TMP% -g=dba -mode=delete -file="Preferences\%FILENAME%.txt"
preferences_manager -u=infodba -p=%DBAPASS% -g=dba -mode=delete -file="Preferences\%FILENAME%.txt"
echo.
goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
