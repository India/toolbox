@echo off

set TRANSFERMODE=%1

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import tranfer mode for document migration
if exist "Transfermodes\%TRANSFERMODE%.xml" (
    echo -----------------------------------------------------------------------
    echo tcxml_import -u=infodba -p=%DBAPASS_TMP% -g=dba -file=Transfermodes\%TRANSFERMODE%.xml -scope_rules_mode=overwrite
    tcxml_import -u=infodba -p=%DBAPASS% -g=dba -file=Transfermodes\%TRANSFERMODE%.xml -scope_rules_mode=overwrite
    echo -----------------------------------------------------------------------
)

REM move log files to log directory
if not exist ..\logs\%COMPUTERNAME% mkdir ..\logs\%COMPUTERNAME%

if exist "Transfermodes\%TRANSFERMODE%.xml_validation.out" move "Transfermodes\%TRANSFERMODE%.xml_validation.out" ..\logs\%COMPUTERNAME%\%TRANSFERMODE%.xml_validation.out
if exist "Transfermodes\%TRANSFERMODE%.xml__importer.log" move "Transfermodes\%TRANSFERMODE%.xml__importer.log" ..\logs\%COMPUTERNAME%\%TRANSFERMODE%.xml__importer.log
if exist "Transfermodes\%TRANSFERMODE%.xml_failed_objects.xml" move "Transfermodes\%TRANSFERMODE%.xml_failed_objects.xml" ..\logs\%COMPUTERNAME%\%TRANSFERMODE%.xml_failed_objects.xml

if not exist "Transfermodes\%TRANSFERMODE%.xml" echo ERROR: File "Transfermodes\%TRANSFERMODE%.xml" does not exist.
echo.

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
