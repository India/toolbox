@echo off

set FILENAME=%1
set SCOPE=%2
set TARGET=%3

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
if not exist "Preferences\%FILENAME%.txt" (
    echo ERROR: File "Preferences\%FILENAME%.txt" does not exist.
    goto :EOF
)

if x%SCOPE%==xSITE goto SITE_SCOPE_PROCESSING
if x%SCOPE%==xGROUP goto GROUP_SCOPE_PROCESSING
if x%SCOPE%==xROLE goto ROLE_SCOPE_PROCESSING
if x%SCOPE%==x goto SITE_SCOPE_PROCESSING

echo ERROR: Undefined processing scope %SCOPE%!
goto :EOF

:SITE_SCOPE_PROCESSING
REM Remove obsolete site preferences
echo.
echo preferences_manager -u=infodba -p=%DBAPASS_TMP% -g=dba -mode=remove -file="Preferences\%FILENAME%.txt" -scope=SITE
preferences_manager -u=infodba -p=%DBAPASS% -g=dba -mode=remove -file="Preferences\%FILENAME%.txt" -scope=SITE
echo.
goto :EOF

:GROUP_SCOPE_PROCESSING
REM Remove obsolete group preferences
if x%TARGET%==x (
    echo ERROR: Undefined processing target %TARGET%!
    goto :EOF
)

echo.
echo preferences_manager -u=infodba -p=%DBAPASS_TMP% -g=dba -mode=remove -file="Preferences\%FILENAME%.txt" -scope=GROUP -target=%TARGET%
preferences_manager -u=infodba -p=%DBAPASS% -g=dba -mode=remove -file="Preferences\%FILENAME%.txt" -scope=GROUP -target=%TARGET%
echo.
goto :EOF

:ROLE_SCOPE_PROCESSING
REM Remove obsolete group preferences
if x%TARGET%==x (
    echo ERROR: Undefined processing target %TARGET%!
    goto :EOF
)

echo.
echo preferences_manager -u=infodba -p=%DBAPASS_TMP% -g=dba -mode=remove -file="Preferences\%FILENAME%.txt" -scope=ROLE -target=%TARGET%
preferences_manager -u=infodba -p=%DBAPASS% -g=dba -mode=remove -file="Preferences\%FILENAME%.txt" -scope=ROLE -target=%TARGET%
echo.
goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
