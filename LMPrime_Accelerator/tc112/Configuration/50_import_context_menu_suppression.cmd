@echo off

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import Teamcenter context menu suppression file
if exist "ContextMenu\context_menu_suppression_config_Rehau.xml" (
    echo -----------------------------------------------------------------------
    echo import_file -u=infodba -p=%DBAPASS_TMP% -g=dba -f="ContextMenu\context_menu_suppression_config_Rehau.xml" -d=context_menu_suppression_config_Rehau -type=Fnd0ContextMenuSuppRuleXML -ref=Fnd0ContextMenuSuppRuleXML -de=r -vb
    echo.
    import_file -u=infodba -p=%DBAPASS% -g=dba -f="ContextMenu\context_menu_suppression_config_Rehau.xml" -d=context_menu_suppression_config_Rehau -type=Fnd0ContextMenuSuppRuleXML -ref=Fnd0ContextMenuSuppRuleXML -de=r -vb
    echo -----------------------------------------------------------------------
)

if not exist "ContextMenu\context_menu_suppression_config_Rehau.xml" echo ERROR: File "ContextMenu\context_menu_suppression_config_Rehau.xml" does not exist.
echo.

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
