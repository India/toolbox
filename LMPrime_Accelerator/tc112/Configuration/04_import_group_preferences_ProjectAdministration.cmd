@echo off

set GROUP=%1

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import Teamcenter group preferences for OOTB group "Project Administration" (this script is required because of the space in group name)
echo.
echo preferences_manager -u=infodba -p=%DBAPASS_TMP% -g=dba -mode=import -file=Preferences\group_preferences_ProjectAdministration.xml -scope=GROUP -target="Project Administration" -action=OVERRIDE
preferences_manager -u=infodba -p=%DBAPASS% -g=dba -mode=import -file=Preferences\group_preferences_ProjectAdministration.xml -scope=GROUP -target="Project Administration" -action=OVERRIDE

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
