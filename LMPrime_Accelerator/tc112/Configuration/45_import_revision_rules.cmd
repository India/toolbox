@echo off

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import revision rules
if not exist ..\logs\%COMPUTERNAME% mkdir ..\logs\%COMPUTERNAME%
echo.
echo plmxml_import -u=infodba -p=%DBAPASS_TMP% -g=dba -xml_file=RevisionRules\TC_RevisionRules.xml -import_mode=overwrite -log=..\logs\%COMPUTERNAME%\TC_RevisionRules.log
plmxml_import -u=infodba -p=%DBAPASS% -g=dba -xml_file=RevisionRules\TC_RevisionRules.xml -import_mode=overwrite -log=..\logs\%COMPUTERNAME%\TC_RevisionRules.log

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
