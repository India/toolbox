@echo off
color 2a
echo.
echo Icon files have to be copied and the toolbar XMLs have to be overwritten.
echo.
echo Specify the IIS Webapp Root with ID (e.g. C:\plm\tc101\net_webtier_MyID)
set /P IIS_WEBAPP_ROOT=Path: 
echo.
color

:START_PROCESS
echo.
echo Starting copy toolbar XML files to %IIS_WEBAPP_ROOT%...
xcopy /s /y ThinClient\IIS_WEB_APP_ROOT %IIS_WEBAPP_ROOT%
echo.
echo Starting copy icons to %IIS_WEBAPP_ROOT%...
copy /y ThinClient\icons %IIS_WEBAPP_ROOT%\tc\typeicons 
copy /y ThinClient\StatusIcons %IIS_WEBAPP_ROOT%\tc\teamcenter\dhtml\icons
echo.
echo Copy process finished!
echo.
color