@echo off

set REPORT_NAME=%1

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import Report Design

echo install_default_report_designs -u=infodba -p=%DBAPASS_TMP% -g=dba -file=Reports\%REPORT_NAME%.xml
install_default_report_designs -u=infodba -p=%DBAPASS% -g=dba -file=Reports\%REPORT_NAME%.xml

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
