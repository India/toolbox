@echo off

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Import Rule Tree
echo.
echo am_install_tree -u=infodba -p=%DBAPASS_TMP% -g=dba -mode=replace_all -format=xml -path=Ruletree\tc_ruletree.xml
am_install_tree -u=infodba -p=%DBAPASS% -g=dba -mode=replace_all -format=xml -path=Ruletree\tc_ruletree.xml

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
