@echo off

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Update Teamcenter Organisation
echo.
echo dsa_util -u=infodba -p=%DBAPASS_TMP% -g=dba -f=import -filename=Organisation\tc_organisation.xml
dsa_util -u=infodba -p=%DBAPASS% -g=dba -f=import -filename=Organisation\tc_organisation.xml

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
