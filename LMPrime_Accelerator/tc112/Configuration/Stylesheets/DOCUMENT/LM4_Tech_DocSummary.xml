<?xml version="1.0" encoding="UTF-8"?>
<!--
=======================================================================
Copyright 2012.
Siemens Product Lifecycle Management Software Inc.
All Rights Reserved.
=======================================================================
     Filename: LM4_Tech_DocSummary.xml

    Style sheet rendering for Tech Doc summary.
=======================================================================

HISTORY
=======================================================================    
1.0     24.Feb. 2011    Siemens             Initial version
-->

<rendering xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="XMLRenderingStylesheet_Schema.xsd">
    <header>
        <image source="thumbnail"/>
        <classificationTrace/>
        <property name="owning_user"/>
        <property name="last_mod_date"/>
        <property name="release_status_list"/>
        <property name="object_type"/>
        <property name="projects_list"/>            
    </header>
    <page titleKey="tc_xrt_Overview" title="Overview">
        <column>
            <section titleKey="tc_xrt_AvailableRevisions" title="Available Revisions">
                <objectSet source="revision_list.ItemRevision" defaultdisplay="listDisplay" sortdirection="descending" sortby="item_revision_id">
                    <tableDisplay>
                        <property name="object_string"/>
                        <property name="item_revision_id"/>                        
                        <property name="release_status_list"/>
                        <property name="last_mod_date"/>
                        <property name="last_mod_user" renderingHint="objectlink" modifiable="false" />
                        <property name="checked_out_user" renderingHint="objectlink" modifiable="false" />
                    </tableDisplay>
                    <thumbnailDisplay/>
                    <treeDisplay>
                        <property name="object_string"/>
                        <property name="item_revision_id"/>                        
                        <property name="release_status_list"/>
                        <property name="last_mod_date"/>
                        <property name="last_mod_user" renderingHint="objectlink" modifiable="false" />
                        <property name="checked_out_user" renderingHint="objectlink" modifiable="false" />
                    </treeDisplay>
                    <listDisplay/>
                </objectSet>
            </section>
            <section titleKey="tc_xrt_ItemProperties" title="Item Properties">
                <property name="object_string" />
                <separator/>
                <property name="object_type"/>
                <property name="object_desc"/>
                <property name="items_tag"/>
                <separator/>
			    <property name="lm4_doc_type"/>
				<property name="lm4_doc_sub_type"/>
                <property name="ip_classification"/>
                <separator/>
                <property name="owning_user" renderingHint="objectlink" modifiable="false"/>
                <property name="owning_group" renderingHint="objectlink" modifiable="false"/>
                <separator/>
                <property name="last_mod_user" renderingHint="objectlink" modifiable="false"/>
                <property name="checked_out"/>
                <property name="checked_out_user" renderingHint="objectlink" modifiable="false"/>
                <separator/>
                <property name="creation_date" />
                <property name="last_mod_date" />
                <separator/>
                <property name="based_on" />
                <command commandId="com.teamcenter.rac.properties" titleKey="tc_xrt_moreProperties"/>
            </section>            
        </column>
        <column>
            <section titleKey="tc_xrt_Preview" title = "Preview">
                <image source="preview"/>
            </section>
            <section titleKey="tc_xrt_actions" commandLayout="vertical">
                <command actionKey="viewmarkupAction" commandId="com.teamcenter.rac.viewMarkUp" />
                <command actionKey="copyAction" commandId="com.teamcenter.rac.copy" />
                <command actionKey="saveAsAction" commandId="org.eclipse.ui.file.saveAs"/>
            </section>
        </column>
    </page>
    <page titleKey="tc_xrt_LM4_BasicData" title="Basic Data">
        <section titleKey="tc_xrt_LM4_MandatoryAttributes" title = "Mandatory Attributes">
            <property name="lm4_doc_type" />
            <property name="lm4_doc_sub_type" />
            <property name="ip_classification"/>
        </section>        
	</page>
    <page title="Related Links" titleKey="tc_xrt_RelatedLinks" visibleWhen="{pref:LIS_RelatedLinkTabVisible}==true">
		<objectSet source="IMAN_specification.Lis0Link,IMAN_reference.Lis0Link" defaultdisplay="tableDisplay" sortby="object_string" sortdirection="ascending">
			<tableDisplay>
				<property name="object_string"/>
				<property name="lis0site"/>
				<property name="lis0serviceProvider"/>
				<property name="relation"/>
				<property name="last_mod_date"/>
				<property name="last_mod_user"/>
				<property name="checked_out_user"/>
			</tableDisplay>
			<command actionKey="newBusinessObjectContextualAction" commandId="com.teamcenter.rac.lisfmwrk.newRelatedLink" renderingHint="commandbutton"/>
			<command actionKey="copyAction" commandId="com.teamcenter.rac.copy" renderingHint="commandbutton">
				<parameter name="localSelection" value="true"/>
			</command>
			<command actionKey="pasteAction" commandId="com.teamcenter.rac.viewer.pastewithContext" renderingHint="commandbutton"/>
			<command actionKey="cutAction" commandId="org.eclipse.ui.edit.cut" renderingHint="commandbutton">
				<parameter name="localSelection" value="true"/>
			</command>
		</objectSet>
	</page>    
    <page titleKey="tc_xrt_AuditLogs" title="Audit Logs" visibleWhen="{pref:TC_audit_manager_version}==3">
        <command titleKey="tc_xrt_ExportToExcel" actionKey="exportExcelAction" commandId = "com.teamcenter.rac.exportAuditSummaryToExcel" renderingHint="commandbutton"/>
        <command titleKey="tc_xrt_ExportToCSV" actionKey="exportCSVAction" commandId = "com.teamcenter.rac.exportAuditSummaryToCSV" renderingHint="commandbutton"/>
        <section titleKey="tc_xrt_WorkflowLogs" title="Workflow Logs" initialstate="collapsed">
            <customPanel java="com.teamcenter.rac.auditmanager.WorkflowLegacyAuditLinkPanel"/>
            <objectSet source="fnd0WorkflowAuditLogs.Fnd0WorkflowAudit" defaultdisplay="tableDisplay" sortby="fnd0LoggedDate" sortdirection="ascending">
                <tableDisplay>
                    <property name="fnd0LoggedDate"/>
                    <property name="process_templateDisp"/>
                    <property name="fnd0ObjectDisplayName"/>
                    <property name="fnd0SignoffUserID"/>
                    <property name="fnd0SignoffGroupName"/>
                    <property name="fnd0SignoffRoleName"/>
                    <property name="fnd0SignoffDecision"/>
                    <property name="fnd0Comments"/>
                    <property name="job_name"/>
                    <property name="fnd0EventTypeName"/>
                    <property name="responsible_party"/>
                </tableDisplay>
            </objectSet>
            <customPanel java="com.teamcenter.rac.auditmanager.WorkflowSecondaryAuditPanel" js="displayWorkflowCustomPanel" />
        </section>
        <section titleKey="tc_xrt_GeneralLogs" title="General Logs" initialstate="collapsed">
            <customPanel java="com.teamcenter.rac.auditmanager.CheckoutHistoyLinkPanel"/>
            <objectSet source="fnd0GeneralAuditLogs.Fnd0GeneralAudit" defaultdisplay="tableDisplay" sortby="fnd0LoggedDate" sortdirection="ascending">
                <tableDisplay>
                    <property name="fnd0LoggedDate"/>
                    <property name="object_type"/>
                    <property name="fnd0EventTypeName"/>
                    <property name="object_name"/>
                    <property name="fnd0PrimaryObjectID"/>
                    <property name="fnd0PrimaryObjectRevID"/>
                    <property name="fnd0UserId"/>
                    <property name="fnd0GroupName"/>
                    <property name="fnd0RoleName"/>
                    <property name="sequence_id"/>
                    <property name="fnd0ChangeID"/>
                    <property name="fnd0Reason"/>
                    <property name="fnd0SecondaryObjectType"/>
                    <property name="fnd0SecondaryObjDispName"/>
                </tableDisplay>
            </objectSet>
        </section>
        <section titleKey="tc_xrt_LicenseExportLogs" title="License Export Logs" initialstate="collapsed">
            <objectSet source="fnd0LicenseExportAuditLogs.Fnd0LicenseExportAudit" defaultdisplay="tableDisplay" sortby="fnd0LoggedDate" sortdirection="ascending">
                <tableDisplay>
                    <property name="fnd0LoggedDate"/>
                    <property name="object_type"/>
                    <property name="fnd0EventTypeName"/>
                    <property name="object_name"/>
                    <property name="fnd0UserId"/>
                    <property name="fnd0GroupName"/>
                    <property name="fnd0RoleName"/>
                    <property name="id"/>
                    <property name="fnd0SecondaryObjectID"/>
                    <property name="fnd0SecondaryObjectRevID"/>
                    <property name="fnd0SecondaryObjectType"/>
                    <property name="fnd0SecondaryObjectName"/>
                    <property name="fnd0SecondaryObjDispName"/>
                    <property name="fnd0SecondaryObjectSeqID"/>
                    <property name="ead_paragraph"/>
                </tableDisplay>
            </objectSet>
            <customPanel java="com.teamcenter.rac.auditmanager.LicenseExportSecondaryAuditPanel" js="displayLicenseExportCustomPanel" />
        </section>
        <section titleKey="tc_xrt_StructureLogs" title="Structure Logs" initialstate="collapsed">
            <objectSet source="fnd0StructureAuditLogs.Fnd0StructureAudit" defaultdisplay="tableDisplay" sortby="fnd0LoggedDate" sortdirection="ascending">
                <tableDisplay>
                    <property name="fnd0LoggedDate"/>
                    <property name="object_type"/>
                    <property name="fnd0EventTypeName"/>
                    <property name="object_name"/>
                    <property name="fnd0UserId"/>
                    <property name="fnd0GroupName"/>
                    <property name="fnd0RoleName"/>
                    <property name="item_id"/>
                    <property name="item_revision_id"/>
                    <property name="sequence_id"/>
                    <property name="fnd0SecondaryObjectID"/>
                    <property name="fnd0SecondaryObjectRevID"/>
                    <property name="fnd0SecondaryObjectType"/>
                    <property name="fnd0SecondaryObjectName"/>
                    <property name="fnd0SecondaryObjDispName"/>
                    <property name="fnd0SecondaryObjectSeqID"/>
                    <property name="comp_position"/>                    
                </tableDisplay>
            </objectSet>
            <customPanel java="com.teamcenter.rac.auditmanager.StructureSecondaryAuditPanel" js="displayStructureCustomPanel" />
        </section>
    </page>
</rendering>
