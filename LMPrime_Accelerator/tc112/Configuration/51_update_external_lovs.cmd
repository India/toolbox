@echo off

set MAX_STEPS=90
if not x%1==x set LOV_NAME=%1
if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS

if not x%LOV_NAME%==x goto SINGLE_UPDATE

REM cycle through all sub-directories of directory ExternalLOVs
for /d %%G in ("ExternalLOVs\*") do (
REM %%~nG extracts the basename from G=ExternalLOVs\basename
echo bmide_manage_batch_lovs.bat -u=infodba -p=%DBAPASS_TMP% -g=dba -option=update -file=ExternalLOVs\%%~nG\%%~nG.xml
call bmide_manage_batch_lovs.bat -u=infodba -p=%DBAPASS% -g=dba -option=update -file=ExternalLOVs\%%~nG\%%~nG.xml
pause
)
goto :EOF

:SINGLE_UPDATE
echo.
echo bmide_manage_batch_lovs.bat -u=infodba -p=%DBAPASS_TMP% -g=dba -option=update -file=ExternalLOVs\%LOV_NAME%\%LOV_NAME%.xml
call bmide_manage_batch_lovs.bat -u=infodba -p=%DBAPASS% -g=dba -option=update -file=ExternalLOVs\%LOV_NAME%\%LOV_NAME%.xml

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
