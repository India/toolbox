/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_string_macros.hxx

    Description:  This file contains macros for string's basic operation such as copy, concatination etc.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_STRING_MACROS_HXX
#define T4_STRING_MACROS_HXX

#include <t4_library.hxx>

#define T4_STRCPY(pszDestStr, pszSourceStr){\
    int iSourceStrLen = (int) (tc_strlen(pszSourceStr)+1);\
    pszDestStr = (char*)MEM_alloc((int)(iSourceStrLen * sizeof(char)));\
    tc_strcpy(pszDestStr, pszSourceStr);\
}

#define T4_STRCAT(pszDestStr,pszSourceStr){\
    int iSourceStrLen = (int) tc_strlen(pszSourceStr);\
    int iDestiStrLen = 0;\
    if(pszDestStr != NULL)\
       iDestiStrLen = (int) tc_strlen (pszDestStr);\
    pszDestStr = (char*)MEM_realloc(pszDestStr, (int)((iSourceStrLen + iDestiStrLen + 1)* sizeof(char)));\
    if(iDestiStrLen == 0)\
       tc_strcpy(pszDestStr, pszSourceStr);\
    else\
       tc_strcat(pszDestStr, pszSourceStr);\
       tc_strcat(pszDestStr, '\0');\
}

#define T4_INITIALIZE_STRING_ARRAY(piArrayLen,  pppszArrayOfStrings,  pszValue)\
{\
    (*piArrayLen)++;\
    *pppszArrayOfStrings = (char**) MEM_alloc((int)((*piArrayLen) * sizeof(char*)));\
    (*pppszArrayOfStrings)[*piArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszValue) + 1)* sizeof(char)));\
    tc_strcpy((*pppszArrayOfStrings)[*piArrayLen - 1], pszValue);\
}

#define T4_UPDATE_STRING_ARRAY(piArrayLen,  pppszArrayOfStrings,  pszValue)\
{\
    (*piArrayLen)++;\
    *pppszArrayOfStrings = (char**) MEM_realloc(*pppszArrayOfStrings, (int)((*piArrayLen) * sizeof(char*)));\
    (*pppszArrayOfStrings)[*piArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszValue) + 1)* sizeof(char)));\
    tc_strcpy((*pppszArrayOfStrings)[*piArrayLen - 1], pszValue);\
}

#endif //T4_LIB_STRING_MACROS_HXX







