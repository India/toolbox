/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_library.hxx

    Description:  Header File containing declarations of function spread across all T4_<UTILITY NAME>_utilities.cxx 
                  For e.g. t4_dataset_utilities.cxx/t4_form_utilities.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_LIBRARY_HXX
#define T4_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <epm/signoff.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me.h>
#include <form.h>
#include <project.h>
#include <folder.h>
#include <pom.h>
#include <tctype.h>
#include <fclasses/tc_date.h>
#include <t4_const.hxx>
#include <t4_itk_errors.hxx>
#include <t4_itk_mem_free.hxx>
#include <t4_string_macros.hxx>

/*        Utility Functions For Object       */
int T4_obj_ask_class
(
    tag_t    tObject,
    char**   ppszClassName
);

int T4_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
);

int T4_obj_is_type_of
(
    tag_t    tObject,
	char*    pszTypeName,
	logical* pbBelongsToInputClass 
);

int T4_obj_ask_status_tag
(
    tag_t      tObject,
    char*      pszStatusName,
    tag_t*     ptStatusTag
);

/*        Utility Functions For Error Logging        */
int T4_err_write_to_logfile
(
    char*       pszErrorFunction,
    int         iErrorNumber,
    char*       pszFileName,
    int         iLineNumber
);

int T4_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
);


/*        Utility Functions For Date & Time        */
int T4_get_date
(
    date_t *dReleaseDate
);

int T4_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
);



/*        Argument Utility Functions        */
int T4_arg_parse_multipleVal
(
    const char*  pszList,
    char         cSep,
    int*         iCount,
    char***      pppszValuelist
);

int T4_arg_get_attached_obj_of_input_type_list
(
    tag_t   task,
    int     iAttachmentType,
    char**  ppszObjTypeList,
    int     iTypeCnt,
    int*    piCount,
    tag_t** pptObject
);

int T4_arg_get_attached_objects_of_type
(
    tag_t   task,
    int     iAttachmentType,
    char*   pszAttachedObjType,
    int*    piCount,
    tag_t** pptObject
);

int T4_arg_get_arguments
(
    TC_argument_list_t*   pArgList,
    tag_t                 tTask,
    const char*           pszArgument,
    char                  cSep,
    logical               bOptional,
    int*                  piValCount,
    char***               pppszArgValue,
    char**                ppszOriginalArgVal
);

int T4_arg_get_wf_argument_value
(
    TC_argument_list_t*   pArgList,
    tag_t                 tTask,
    const char*           pszArgument,
    logical               bOptional,
    char**                ppszArgValue
);


/*        String Utility Functions        */
int T4_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
);

int T4_contains_string_value
(
    char*  pszStrVal,
    int    iTotalStrVals,
    char** pppszStrVals
);

void  T4_get_string_tokens
(
    const char* pszInputStr,
    char*       pszDelim, 
    int*        iTokenCnt,
    char***     pppszAllTokens
);


/*        Utility Functions For GRM        */
int T4_grm_get_primary_obj_of_type
(
    tag_t   tSecondaryObj,
    char*   pszRelationTypeName,
    char*   pszObjType,
	char*   pszExcludeStatus,
    tag_t** pptPrimaryObjects,
    int*    piObjectCount  
);



/*        Utility Functions For Preferences        */
int T4_pref_get_string_values
(
    char*   pszPrefName, 
    int*    iPrefValCnt, 
    char*** pppszPrefVals
);
    
int T4_pref_get_integer_value
(
    char*   pszPrefName, 
    int*    piPrefValue
);



#endif




