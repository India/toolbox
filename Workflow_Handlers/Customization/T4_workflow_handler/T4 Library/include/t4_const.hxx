/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_const.hxx

    Description:  Header File for constants used during scope of the DLL.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_CONST_HXX
#define T4_CONST_HXX

/* Date Time Format */
#define T4_DATE_TIME_FORMAT                                     "%d.%m.%Y"

/* Relation    */


/* Attributes  */
#define T4_ATTR_OBJECT_NAME_ATTRIBUTE                           "object_name"
#define T4_ATTR_DESCRIPTION_ATTRIBUTE                           "object_desc"
#define T4_ATTR_ITEM_ID                                         "item_id"
#define T4_ATTR_ITEM_REVISION_ID                                "item_revision_id"
#define T4_ATTR_OBJECT_TYPE_ATTRIBUTE                           "object_type"
#define T4_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define T4_ATTR_ACTIVE_SEQ                                      "active_seq"
#define T4_ATTR_ITEM_TAG                                        "items_tag"

/* Class Name */
#define T4_CLASS_ITEMREVISION                                   "ItemRevision"
#define T4_CLASS_ITEM                                           "Item"
#define T4_CLASS_DATASET                                        "Dataset"
#define T4_CLASS_EPM_TASK                                       "EPMTask"
#define T4_CLASS_IMANRELATION                                   "ImanRelation"
#define T4_CLASS_PDF_DATASET                                    "PDF"
#define T4_CLASS_FOLDER                                         "Folder"

/* Workflow Argument Name  */
#define T4_WF_ARG_PRIORITY                                      "priority"
#define T4_WF_ARG_PROVIDER_NAME                                 "provider_name"
#define T4_WF_ARG_SERVICE_NAME                                  "service_name"
#define T4_WF_ARG_OBJECT_TYPES                                  "object_types"
#define T4_WF_ARG_RELATION                                      "relation"
#define T4_WF_ARG_REQUEST_ARGS                                  "request_args"

/* Dispatcher Request Arguments */
#define T4_ETS_ARG_VALUE_TRIGGER_WF                             "WORKFLOW"

/* Keywords */
#define T4_KW_TARGET                                            "$TARGET"
#define T4_KW_REFERENCE                                         "$REFERENCE"


/* Separators */
#define T4_STRING_EQUAL                                         "="
#define T4_CHAR_COMMA                                           ','
#define T4_CHAR_DOT                                             '.'
#define T4_CHAR_EQUAL                                           '='


#define T4_SEPARATOR_LENGTH                                     1
#define T4_NUMERIC_ERROR_LENGTH                                 12

#endif

