/*======================================================================================================================================================================
                Copyright 2013  Prion GmbH
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_grm_utilities.hxx

    Description: This File contains custom functions for GRM to perform generic operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif
#include <t4_library.hxx>
#include <t4_const.hxx>
#include <t4_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * This function returns a list and count of all primary objects of input type attached with a specified
 * relation to the input secondary object. Function excludes all primary objects which has status same
 * as 'pszExcludeStatus'.
 * 
 * @param[in]  tSecondaryObj        Tag to secondary object.
 * @param[in]  pszRelationTypeName  Name of relation type. Can be NULL.
 * @param[in]  pszObjType           Type of Primary object that needs to be retrieved
 * @param[in]  pszExcludeStatus     Primary objects with this status needs to be excluded
 * @param[out] pptPrimaryObjects    List of Primary object.
 * @param[out] piObjectCount        Primary Object count.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int T4_grm_get_primary_obj_of_type
(
    tag_t   tSecondaryObj,
    char*   pszRelationTypeName,
    char*   pszObjType,
	char*   pszExcludeStatus,
    tag_t** pptPrimaryObjects,
    int*    piObjectCount  
)
{

    int     iRetCode                = ITK_ok;
    int     iCount                  = 0;
    int     iPrimaryCount           = 0;
    tag_t   tRelationType           = NULLTAG;
    char*   pszObjectType           = NULL;
    GRM_relation_t *ptPrimaryObj    = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount ) = 0;
    (*pptPrimaryObjects) = NULL;

    /* get the "IMAN_specification" relation type tag */
    T4_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    
    T4_ITKCALL(iRetCode, GRM_list_primary_objects(tSecondaryObj, tRelationType,
                                                     &iPrimaryCount, &ptPrimaryObj));
       
                                       
    for(int iIterator = 0; iIterator < iPrimaryCount && iRetCode == ITK_ok; iIterator++)
    {
        int  iActiveSeq   = 0;
      
        T4_ITKCALL(iRetCode, AOM_ask_value_int(ptPrimaryObj[iIterator].primary, T4_ATTR_ACTIVE_SEQ, &iActiveSeq));
	
		/* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0)
        {
            logical bIsTypeOf = false;

            T4_ITKCALL(iRetCode, T4_obj_is_type_of(ptPrimaryObj[iIterator].primary, pszObjType, &bIsTypeOf));

            if(bIsTypeOf == true)
            {
			    if(pszExcludeStatus == NULL)
                {
				    if(iCount == 0)
				    {
					    iCount++;
					    (*pptPrimaryObjects) = (tag_t*)MEM_alloc ((sizeof(tag_t) * iCount) );
				    }
				    else
				    {
					     iCount++;
					    (*pptPrimaryObjects) = (tag_t*)MEM_realloc ( (*pptPrimaryObjects) , (sizeof(tag_t) * iCount) );
				    }

				    (*pptPrimaryObjects)[iCount-1]    = ptPrimaryObj[iIterator].primary;
			    }
			    else
			    {
				    tag_t  tStatus  = NULLTAG;

                    T4_ITKCALL(iRetCode, T4_obj_ask_status_tag(ptPrimaryObj[iIterator].primary, pszExcludeStatus, &tStatus));

                    if(tStatus == NULLTAG)
                    {
                        if(iCount == 0)
					    {
						    iCount++;
						    (*pptPrimaryObjects) = (tag_t*)MEM_alloc ((sizeof(tag_t) * iCount) );
					    }
					    else
					    {
						     iCount++;
						    (*pptPrimaryObjects) = (tag_t*)MEM_realloc ( (*pptPrimaryObjects) , (sizeof(tag_t) * iCount) );
					    }

					    (*pptPrimaryObjects)[iCount-1]    = ptPrimaryObj[iIterator].primary;
                    }
			    }
            }
        }
    }
 
    if(iCount == 0)
    {
        TC_write_syslog("No Primary Object associated with the %s relation to the object.\n", pszRelationTypeName);
    }
    else
    {
           (*piObjectCount ) = iCount;
    }

    T4_MEM_TCFREE(pszObjectType);
    T4_MEM_TCFREE(ptPrimaryObj);
    return iRetCode;
}