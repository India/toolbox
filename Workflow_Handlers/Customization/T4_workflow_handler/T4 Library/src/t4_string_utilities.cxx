/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_string_utilities.cxx

    Description: This File contains functions for manipulating string.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <t4_library.hxx>
#include <t4_const.hxx>
#include <t4_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * This function returns the substring when provided with start and end position on the 
 * the input string. Substring is the character sequence that starts at character position 
 * 'iStrStartPosition' and ends at character position 'iStrEndPosition'
 *
 * For example If 'pszInputString' = 'Application'
 *                'iStrStartPosition' = 4
 *                'iStrEndPosition'   = 9
 *             then Output Will be: 'licati'
 *
 * Function will copy the characters starting 4th character to the 9th character of
 * input string.
 * 
 * @param[in]  pszInputString    Actual input string
 * @param[in]  iStrStartPosition Position of a character in the current string to be
 *                               used as starting character for the substring.
 * @param[in]  iStrEndPosition   Position of a character in the current string to be
 *                               used as last character for the substring.
 *
 * @param[out] ppszCopyedStr     Copied string.
 *
 * @retval INVALID_INPUT_TO_FUNCTION if input not correct
 * @retval ITK_ok on successful execution
 */
int T4_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
)
{
    int  iRetCode    = ITK_ok;
    int  iStrLen     = 0;

    /* Initializing the Out Parameter to this function. */
    (*ppszCopyedStr) = NULL;

    iStrLen = (int) tc_strlen(pszInputString);
    
    if ((pszInputString != NULL) && (iStrStartPosition >= 0) && (iStrEndPosition <=  iStrLen) && 
        (iStrStartPosition <= iStrEndPosition))
    {
        if(tc_strcmp(pszInputString, "") != 0)
        {
            int idx        = 0;
            int iNewStrLen = 0;

            iNewStrLen = iStrEndPosition - iStrStartPosition + 2;

            (*ppszCopyedStr) = (char*) MEM_alloc(sizeof(char*) * iNewStrLen);

            while(iStrStartPosition <= iStrEndPosition)
            {
                (*ppszCopyedStr)[idx] = pszInputString[iStrStartPosition-1];
                idx++;
                iStrStartPosition++;
            }

            (*ppszCopyedStr)[idx] = '\0';
        }
    }
    else
    {
        iRetCode = INVALID_INPUT_TO_FUNCTION;
    }

    return iRetCode;
}


/**
 * Function checks if given string exists in string of array or not 
 *
 * @param[in]  pszStrVal     String to be looked into string array
 * @param[in]  iTotalStrVals String Array Size 
 * @param[in]  pppszStrVals  String Array
 *
 * @return ITK_ok if string is found in the array else return 1
 */
int T4_contains_string_value
(
    char*  pszStrVal, 
    int    iTotalStrVals, 
    char** pppszStrVals)
{
    int iRetCode (ITK_ok);

    if( iTotalStrVals > 0 )
    {
         for ( int inx = 0; inx < iTotalStrVals; inx++ )
         {
             if(tc_strcmp( pszStrVal, pppszStrVals[ inx ]) == 0)
             {
                return iRetCode;
             }
         } 
        
    }

    return 1;
}


/**
 * Function tokenize the given string with provided delimited
 *
 * @param[in]  pszInputStr      String to be tokenized
 * @param[in]  pszDelim         Delimiter
 * @param[out] iTokenCnt        Total number of tokens
 * @param[out] pppszAllTokens   String array of tokens
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
void  T4_get_string_tokens
(
    const char* pszInputStr,
    char*       pszDelim, 
    int*        iTokenCnt,
    char***     pppszAllTokens
)
{
    char* pszTempStr = NULL;
    char* pszToken   = NULL;
 
    T4_STRCPY(pszTempStr, pszInputStr);
   
    if(NULL != (pszToken = tc_strtok(pszTempStr, pszDelim)))
    {
        T4_UPDATE_STRING_ARRAY(iTokenCnt, pppszAllTokens, pszToken);
    }

    while(NULL != (pszToken = tc_strtok(NULL, pszDelim)))
    {
        T4_UPDATE_STRING_ARRAY(iTokenCnt,pppszAllTokens, pszToken);
    }

    T4_MEM_TCFREE(pszToken);
    T4_MEM_TCFREE(pszTempStr);
}





