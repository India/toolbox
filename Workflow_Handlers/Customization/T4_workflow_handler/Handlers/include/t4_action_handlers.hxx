/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_action_handlers.hxx

    Description:  Header File for t4_action_handlers.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_ACTION_HANDLERS_HXX
#define T4_ACTION_HANDLERS_HXX
    
#include <epm/epm.h>

int T4_register_action_handlers
(
    void
);
    
#endif

