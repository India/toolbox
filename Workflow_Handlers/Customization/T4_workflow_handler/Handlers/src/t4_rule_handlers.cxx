/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_rule_handlers.cxx

    Description: This file contains function that registers all Rule Handlers for T4 custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <t4_library.hxx>
#include <t4_rule_handlers.hxx>

#ifdef __cplusplus
}
#endif


/**
 * Registers the rule handlers for workflows.
 * 
 * @return Status of registration of rule handlers.
 */
int T4_register_rule_handlers
(
    void
)
{
    int iRetCode      = ITK_ok;
    
    //TC_write_syslog("In T4 Register Rule Handlers Operation\n");   
   
    return iRetCode;
    
}


