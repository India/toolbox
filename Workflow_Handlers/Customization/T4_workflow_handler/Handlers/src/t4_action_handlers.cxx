/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_action_handlerss.cxx

    Description: This file contains function that registers all Action Handlers for T4 custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <t4_action_handlers.hxx>
#include <t4_library.hxx>
#include <t4_dispatcher_request.hxx>

#ifdef __cplusplus
}
#endif


/**
 * Functon to register the action handlers to be used in the workflows.
 *
 * @return Status of registration of action handlers. 
 */
int T4_register_action_handlers
(
    void
)
{
    int iRetCode = ITK_ok;
    
    TC_write_syslog("In T4 Register Action Handlers Operation\n");
    
    T4_ITKCALL( iRetCode, EPM_register_action_handler( "T4-create-dispatcher-request", "", T4_create_dispatcher_request));

        
    return iRetCode;
}
