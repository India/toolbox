/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_register_callbacks.hxx

    Description:  Header File for t4_register_callbacks.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_REGISTER_CALLBACKS_HXX
#define T4_REGISTER_CALLBACKS_HXX
    
#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>


extern DLLAPI int T4_workflow_handler_register_callbacks(void);

extern DLLAPI int T4_register_custom_handlers
(
    int *decision,
    va_list args
);


#endif

