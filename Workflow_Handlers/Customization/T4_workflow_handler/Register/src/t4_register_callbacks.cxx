/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_register_callbacks.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <t4_const.hxx>
#include <t4_register_callbacks.hxx>
#include <t4_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Description:
 *
 * Function to register Callback function. Entry point for custom T$ custom Code.
 *
 * Returns:
 *
 * Status of execution.
 */
extern DLLAPI int T4_workflow_handler_register_callbacks () 
{
    int  iRetCode   = ITK_ok;
        
    printf("INFO: Register Custom Exits. \n");

    iRetCode = CUSTOM_register_exit ("T4_workflow_handler", "USER_gs_shell_init_module", (CUSTOM_EXIT_ftn_t)  T4_register_custom_handlers);
    if(iRetCode!=ITK_ok)
    {
        printf("ERROR: Unable to register custom handlers.");
        return iRetCode;
    }

    return iRetCode;
}




