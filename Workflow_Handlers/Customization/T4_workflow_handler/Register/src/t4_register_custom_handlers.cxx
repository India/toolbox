/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_register_custom_handlers.cxx

    Description:  This file contains function registering custom handlers to be used by the workflow.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <t4_library.hxx>
#include <t4_register_callbacks.hxx>
#include <t4_rule_handlers.hxx>
#include <t4_action_handlers.hxx>
#include <t4_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Registers custom handlers to be used by the workflow.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
extern DLLAPI int T4_register_custom_handlers(int *decision, va_list args)
{
    int iRetCode = ITK_ok;

    TC_write_syslog( "Registering custom handlers ...\n");
    *decision  = ALL_CUSTOMIZATIONS;

        
    iRetCode = T4_register_rule_handlers();
    if(iRetCode != ITK_ok)
    {
        printf("Unable to register: T4_register_rule_handlers\n");
    }
    
    iRetCode = T4_register_action_handlers();
    if(iRetCode != ITK_ok)
    {
        printf("Unable to register: T4_register_action_handlers\n");
    }
    
    return iRetCode;
}
