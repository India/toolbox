/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_dispatcher_request.cxx

    Description: This File contains functions/handlers which are responsible for sending a dispatcher request depending on the
                 argument or parameter values.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <t4_action_handlers.hxx>
#include <t4_library.hxx>
#include <t4_errors.hxx>
#include <t4_dispatcher_request.hxx>

#ifdef __cplusplus
}
#endif

/**
 * The handler creates a dispatcher request using the translation service "T4-create-dispatcher-request" with the provider.
 * Request is created for each PDF dataset attached to the target item revision(s) one by one. 
 * Following arguments are expected by this handler:
 *
 *  '-priority'(O)       Defines the priority assigned to the new translation request. If not provided the default value is
 *                       "2". 1 = low priority, 2 = medium priority, 3 = high priority
 *  '-provider_name'(M)  Specifies the provide name that defines the translation service. The service has to be a valid
 *                       service name as specified in site preference.
 *  '-service_name'(M)   Specifies the translation service name for which the request will be created. The service has to
 *                       be a valid service name as specified in translator.xml.
 *  '-object_types(M)    Specifies the object type in the targets for which a request will be created. Multiple object types
 *                       can be defined separated by a ",".Valid values are any object of the class of ItemRevision or Dataset.
 *  '-request_args(M)    Specifies the request arguments used when creating the request. Multiple arguments can be
 *                       passed separated by a ",". The following pattern has to be used for each argument:
 *                       ARGUMENT_NAME=ARGUMENT_VALUE
 *                       As a value for "ARGUMENT_VALUE" the following notation can be used as well:
 *                       ($TARGET|$REFERENCE).OBJECT_TYPE.PROPERTY_NAME (e.g.
 *                       $REFERENCE.MYFORM.t4_value)
 *
 * (O)  Optional Argument
 * (M)  Mandatory Argument
 *
 * @param[in] msg  Handler Message
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int T4_create_dispatcher_request
(
    EPM_action_message_t msg
)
{
    int      iRetCode                = ITK_ok;
    int      iTargetCount            = 0;
    int      iObjTypeCnt             = 0;
    tag_t    tRootTask               = NULLTAG;
    tag_t*   ptTargets               = NULL;
    char*    pszOriginalArgVal       = NULL;
    char**   ppszObjType             = NULL;

    /* Get the target attachments   */
    T4_ITKCALL(iRetCode, EPM_ask_root_task( msg.task, &tRootTask));
    
        /* Getting the "object_types" argument value. */ 
    T4_ITKCALL(iRetCode, T4_arg_get_arguments( msg.arguments, msg.task, T4_WF_ARG_OBJECT_TYPES, T4_CHAR_COMMA,
                                                   false, &iObjTypeCnt, &ppszObjType, &pszOriginalArgVal ));

    T4_ITKCALL(iRetCode, T4_arg_get_attached_obj_of_input_type_list(tRootTask, EPM_target_attachment, ppszObjType, iObjTypeCnt,
                                                                    &iTargetCount, &ptTargets));
    if(iTargetCount > 0)
    {
        int      iPriority               = 0;
        int      iReqArgCnt              = 0;
        int      iRelTypeCnt             = 0;
        char*    pszPriority             = NULL;
        char*    pszProviderName         = NULL;
        char*    pszServiceName          = NULL;
        char*    pszRequestArgs          = NULL;
        char*    pszOriginalReqArgVal    = NULL;
        char*    pszOrigRelArgVal        = NULL;
        char**   ppszReqArg              = NULL;
        char**   ppszETSArgVal           = NULL;
        char**   ppszRelType             = NULL;

        /* Getting the "priority" argument value. */ 
        T4_ITKCALL(iRetCode, T4_arg_get_wf_argument_value(msg.arguments, msg.task, T4_WF_ARG_PRIORITY, true,
                                                              &pszPriority));

        /* Getting the "provider_name" argument value. */ 
        T4_ITKCALL(iRetCode, T4_arg_get_wf_argument_value(msg.arguments, msg.task, T4_WF_ARG_PROVIDER_NAME, false,
                                                              &pszProviderName));

        /* Getting the "service_name" argument value. */ 
        T4_ITKCALL(iRetCode, T4_arg_get_wf_argument_value(msg.arguments, msg.task, T4_WF_ARG_SERVICE_NAME, false,
                                                              &pszServiceName));

        /* Getting the "relation" argument value. */ 
        T4_ITKCALL(iRetCode, T4_arg_get_arguments( msg.arguments, msg.task, T4_WF_ARG_RELATION, T4_CHAR_COMMA,
                                                       false, &iRelTypeCnt, &ppszRelType, &pszOrigRelArgVal ));
        
        /* Getting the "request_args" argument value. */ 
        T4_ITKCALL(iRetCode, T4_arg_get_arguments( msg.arguments, msg.task, T4_WF_ARG_REQUEST_ARGS, T4_CHAR_COMMA,
                                                       false, &iReqArgCnt, &ppszReqArg, &pszOriginalReqArgVal ));

        if(pszPriority == NULL)
        {
            iPriority = 2;
        }
        else
        {
            iPriority = atoi ( pszPriority );
        }
        
        T4_ITKCALL(iRetCode, T4_generate_disp_request(tRootTask, iTargetCount, ptTargets, iRelTypeCnt, ppszRelType, 
                                                        iReqArgCnt, ppszReqArg, iPriority, pszProviderName, pszServiceName));

        T4_MEM_TCFREE( pszPriority );
        T4_MEM_TCFREE( pszProviderName );
        T4_MEM_TCFREE( pszServiceName );
        T4_MEM_TCFREE( pszRequestArgs );
        T4_MEM_TCFREE( pszOriginalReqArgVal );
        T4_MEM_TCFREE( pszOrigRelArgVal );
        T4_MEM_TCFREE( ppszReqArg );
        T4_MEM_TCFREE( ppszRelType );
    }

    T4_MEM_TCFREE( ptTargets );
    T4_MEM_TCFREE( ppszObjType );
    T4_MEM_TCFREE( pszOriginalArgVal );
    return iRetCode;
}

/**
 * This function will creates translation request with input argument information.
 *
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int T4_generate_disp_request 
( 
    tag_t   tRootTask,
    int     iObjectCnt,
    tag_t*  ptObject,
    int     iRelTypeCnt,
    char**  ppszRelType,
    int     iReqArgCnt, 
    char**  ppszReqArg,
    int     iPriority, 
    char*   pszProviderName, 
    char*   pszServiceName
)
{
    int    iRetCode      = ITK_ok;
    int    iNameCount    = 0;
    int    iAttrCnt      = 0;
    int    iETSArgCnt    = 0;
    char** ppszNameOfArg = NULL;
    char** ppszAttrNames = NULL;
    char** ppszETSArgVal = NULL;

    for(int iJx = 0; iJx < iReqArgCnt && iRetCode == ITK_ok; iJx++)
    {
        char*  pszPosOfDot = NULL; 

        /* Do not free variable 'pszPosOfDot' otherwise system will crash */
        pszPosOfDot = STRNG_find_first_char(ppszReqArg[iJx], T4_CHAR_DOT);

        if(pszPosOfDot == NULL)
        {
            if(iETSArgCnt == 0)
            {
                T4_INITIALIZE_STRING_ARRAY(&iETSArgCnt, &ppszETSArgVal, ppszReqArg[iJx]);
            }
            else
            {        
                T4_UPDATE_STRING_ARRAY(&iETSArgCnt, &ppszETSArgVal, ppszReqArg[iJx]);
            }
        }
        else
        {
            int    iCount     = 0;
            char** ppszVal    = NULL;

            T4_ITKCALL(iRetCode, T4_arg_parse_multipleVal(ppszReqArg[iJx], T4_CHAR_EQUAL, &iCount, &ppszVal));

            if(iCount == 2)
            {
                int    iInfoCnt = 0;
                char** ppszInfo = NULL;

                T4_ITKCALL(iRetCode, T4_arg_parse_multipleVal(ppszVal[1], T4_CHAR_DOT, &iInfoCnt, &ppszInfo));
                
                if(iInfoCnt == 3)
                {
                    if(tc_strcmp(ppszInfo[0], T4_KW_REFERENCE) == 0 ||  tc_strcmp(ppszInfo[0], T4_KW_TARGET) == 0)
                    {
                        int    iRefObjCnt      = 0;
                        int    iAttachmentType = 0;
                        tag_t* ptRefObjects    =  NULL;

                        if(tc_strcmp(ppszInfo[0], T4_KW_REFERENCE) == 0)
                        {
                            iAttachmentType = EPM_reference_attachment;
                        }
                        else
                        {
                            iAttachmentType = EPM_target_attachment;
                        }

                        T4_ITKCALL(iRetCode, T4_arg_get_attached_objects_of_type(tRootTask, iAttachmentType, ppszInfo[1],
                                                                                  &iRefObjCnt, &ptRefObjects));

                        if(iRefObjCnt > 0)
                        {
                            char*  pszAttrVal = NULL;

                            T4_ITKCALL(iRetCode, AOM_ask_value_string(ptRefObjects[0], ppszInfo[2], &pszAttrVal));
                        
                            if(iRetCode == ITK_ok && pszAttrVal != NULL)
                            {
                                int   iLen    = 0;
                                char* pszTemp = NULL;
                                
                                iLen =(int)(tc_strlen(ppszVal[0]) + tc_strlen(T4_STRING_EQUAL) + tc_strlen(pszAttrVal) +1); 
                                
                                pszTemp = (char*) MEM_alloc((int)(sizeof(char)*iLen)); 

                                tc_strcpy(pszTemp, ppszVal[0]);
                                tc_strcat(pszTemp, T4_STRING_EQUAL);
                                tc_strcat(pszTemp, pszAttrVal);
                                tc_strcat(pszTemp, '\0');

                                if(iETSArgCnt == 0)
                                {
                                    T4_INITIALIZE_STRING_ARRAY(&iETSArgCnt, &ppszETSArgVal, pszTemp);
                                }
                                else
                                {        
                                    T4_UPDATE_STRING_ARRAY(&iETSArgCnt, &ppszETSArgVal, pszTemp);
                                }

                                T4_MEM_TCFREE( pszTemp );
                            }

                            T4_MEM_TCFREE( pszAttrVal );
                        }
                        
                        T4_MEM_TCFREE( ptRefObjects );
                    }
                    else
                    {
                        /*Store error into error stack */
                        iRetCode = UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES;
                        EMH_store_error(EMH_severity_error, iRetCode);
                        break;
                    }
                }
                else
                {
                    if(iInfoCnt == 2)
                    {
                        if(iAttrCnt == 0)
                        {
                            T4_INITIALIZE_STRING_ARRAY(&iNameCount, &ppszNameOfArg, ppszVal[0]);
                            T4_INITIALIZE_STRING_ARRAY(&iAttrCnt, &ppszAttrNames, ppszInfo[1]);
                        }
                        else
                        {        
                            T4_UPDATE_STRING_ARRAY(&iNameCount, &ppszNameOfArg, ppszVal[0]);
                            T4_UPDATE_STRING_ARRAY(&iAttrCnt, &ppszAttrNames, ppszInfo[1]);
                        }
                    }
                    else
                    {
                        /*Store error into error stack */
                        iRetCode = UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES;
                        EMH_store_error(EMH_severity_error, iRetCode);
                        break;
                    }
                }

                T4_MEM_TCFREE( ppszInfo );
            }
            else
            {
                /*Store error into error stack */
                iRetCode = UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES;
                EMH_store_error(EMH_severity_error, iRetCode);
                break;
            }

            T4_MEM_TCFREE( ppszVal );
        }
    }

    for (int iDx = 0; iDx < iObjectCnt && iRetCode == ITK_ok; iDx++)
    {
        tag_t   tTransReq      = NULLTAG;
        tag_t   tPrimaryObj    = NULLTAG;
        logical bIsTypeOfIR    = false;

        T4_ITKCALL(iRetCode, T4_obj_is_type_of(ptObject[iDx], T4_CLASS_ITEMREVISION, &bIsTypeOfIR ));
        
        if(bIsTypeOfIR == true)
        {
            T4_ITKCALL(iRetCode, AOM_ask_value_tag( ptObject[iDx], T4_ATTR_ITEM_TAG, &tPrimaryObj));
        }
        else
        {
            int    iObjCnt   = 0;
            tag_t* ptObjects = NULL;

            for(int iZx = 0; iZx < iRelTypeCnt && iRetCode == ITK_ok; iZx++)
            {
                T4_ITKCALL(iRetCode, T4_grm_get_primary_obj_of_type(ptObject[iDx], ppszRelType[iZx], T4_CLASS_ITEMREVISION,
                                                                    NULL, &ptObjects, &iObjCnt ));
                if(iObjCnt > 0)
                {
                    break;
                }
            }

            if(iRetCode == ITK_ok && iObjCnt > 0)
            {
                tPrimaryObj = ptObjects[0];
            }

            T4_MEM_TCFREE( ptObjects );
        }

        for(int iMx = 0; iMx < iAttrCnt && iRetCode == ITK_ok; iMx++)
        {
            char*  pszPropValue = NULL;

            T4_ITKCALL(iRetCode, AOM_ask_value_string(ptObject[iDx], ppszAttrNames[iMx], &pszPropValue));
       
            if(iRetCode == ITK_ok && pszPropValue != NULL)
            {
                int   iLen    = 0;
                char* pszTemp = NULL;
                
                iLen =(int)(tc_strlen(ppszNameOfArg[iMx]) + tc_strlen(T4_STRING_EQUAL) + tc_strlen(pszPropValue) +1); 
                
                pszTemp = (char*) MEM_alloc((int)(sizeof(char)*iLen)); 

                tc_strcpy(pszTemp, ppszNameOfArg[iMx]);
                tc_strcat(pszTemp, T4_STRING_EQUAL);
                tc_strcat(pszTemp, pszPropValue);
                tc_strcat(pszTemp, '\0');

                if(iETSArgCnt == 0)
                {
                    T4_INITIALIZE_STRING_ARRAY(&iETSArgCnt, &ppszETSArgVal, pszTemp);
                }
                else
                {        
                    T4_UPDATE_STRING_ARRAY(&iETSArgCnt, &ppszETSArgVal, pszTemp);
                }

                T4_MEM_TCFREE( pszTemp );
            }

            T4_MEM_TCFREE( pszPropValue );
        }

        /*  Create Dispatcher Request  */
        T4_ITKCALL(iRetCode, ETS_create_request2( 1, &(ptObject[iDx]), &tPrimaryObj, iPriority, pszProviderName, pszServiceName,
                                                  T4_ETS_ARG_VALUE_TRIGGER_WF, iETSArgCnt, (const char**)ppszETSArgVal, &tTransReq ));
    }

    T4_MEM_TCFREE( ppszNameOfArg );
    T4_MEM_TCFREE( ppszNameOfArg );
    T4_MEM_TCFREE( ppszETSArgVal );
    return iRetCode;
}


 