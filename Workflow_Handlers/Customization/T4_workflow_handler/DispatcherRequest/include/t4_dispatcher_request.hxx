/*======================================================================================================================================================================
                Copyright 2013
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: t4_dispatcher_request.hxx

    Description:  Header File for t4_dispatcher_request.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-March-13   Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef T4_DISPATCHER_REQUEST_HXX
#define T4_DISPATCHER_REQUEST_HXX
    
#include <epm/epm.h>
#include <dispatcher/dispatcher_itk.h>

int T4_create_dispatcher_request
(
    EPM_action_message_t msg
);

int T4_generate_disp_request 
( 
    tag_t   tRootTask,
    int     iObjectCnt,
    tag_t*  ptObject,
    int     iRelTypeCnt,
    char**  ppszRelType,
    int     iReqArgCnt, 
    char**  ppszReqArg,
    int     iPriority, 
    char*   pszProviderName, 
    char*   pszServiceName
);

#endif

