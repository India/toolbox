/*!
 * @file     timeseries_upload.c
 * @brief    Example custom agent code which uploads timeseries.
 *
 * It is assumed that data source configuration is already uploaded which complies with the timeseries you want to exchange.
 *
 * @copyright Copyright (C) 2019 Siemens Aktiengesellschaft.\n
 *            All rights reserved.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "mcl_connectivity/mcl_connectivity.h"
#include "callbacks.h"

#if defined(WIN32) || defined(WIN64)
#include "Windows.h"
#else
#include <unistd.h>
#endif

#define VALUE_LIST_PER_EXCHANGE 4
#define VALUE_BUFFER_LENGTH 8

// Number of data points.
#define DATA_POINT_COUNT 7

// Quality code for "Good".
#define QUALITY_CODE_GOOD "00000000"

// Definition of a function pointer type to create a function list by using it. This function list will be used to call and add new functions easily.
typedef mcl_bool_t (*read_data_function_template)(char **buffer);

// Functions to read sensor values from Motor A.
mcl_bool_t read_voltage_motor_a(char **buffer);
mcl_bool_t read_current_motor_a(char **buffer);

// Functions to read sensor values from Motor B.
mcl_bool_t read_voltage_motor_b(char **buffer);
mcl_bool_t read_current_motor_b(char **buffer);

// Functions to read sensor values from Environmental Sensor.
mcl_bool_t read_temperature(char **buffer);
mcl_bool_t read_humidity(char **buffer);
mcl_bool_t read_pressure(char **buffer);

// List consisting of the functions declared above to call them in a loop.
read_data_function_template read_data_function_list[DATA_POINT_COUNT] = { read_voltage_motor_a, read_current_motor_a, read_voltage_motor_b, read_current_motor_b, read_temperature, read_humidity, read_pressure };

// Data point id list.
const char *data_point_id_list[DATA_POINT_COUNT] =
{
    "59c27254-6987-407e-b292-acbffcda20dc",
    "da8a3fac-cd38-4b27-9412-8530f47f7cbe",
    "0736c966-8968-44c6-b5a5-5306ee8bc79e",
    "bfde1367-eec0-4fe4-a2c8-73046fd2bee1",
    "aaf8b3de-3f53-47ae-b021-ea38c2ad1577",
    "c5205d57-e74a-4cb3-b7fd-12a0cf187768",
    "b48e88ef-822a-4e48-8db4-628b803eb560"
};

// Timeseries configuration id comes from data source configuration upload.
const char *configuration_id = "229c923a-f91e-489e-9a3c-f1b10ac2d835";

// Static functions for initialization and parameter setting process of timeseries_value and timeseries_value_list handles.
static mcl_error_t add_value_to_value_list(const char *data_point_id, const char *value, const char *quality_code, mcl_timeseries_value_list_t *timeseries_value_list);
static mcl_error_t add_value_list_to_timeseries(mcl_timeseries_t *timeseries);

// This function tries to exchange, renews access token or rotates key if necessary.
static mcl_error_t safe_exchange(mcl_connectivity_t *connectivity, mcl_core_t *core, void *item);

// Sleep function.
static void sleep_in_seconds(mcl_uint32_t seconds);

int upload_timeseries(mcl_t* mcl)
{
	mcl_error_t code = MCL_OK;

    // Exchange timeseries for each loop.
    while (MCL_OK == code)
    {
        mcl_timeseries_t *timeseries = NULL;

        // Create timeseries.
        code = mcl_timeseries_initialize(MCL_TIMESERIES_VERSION_1_0, &timeseries);
        printf("Timeseries initialization : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));

        if (MCL_OK == code)
        {
            // Set configuration id.
            code = mcl_timeseries_set_parameter(timeseries, MCL_TIMESERIES_PARAMETER_CONFIGURATION_ID, configuration_id);
            printf("Setting configuration id : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
        }

        // Timeseries will be prepared with four timeseries value list before exchange.
        if (MCL_OK == code)
        {
            for (mcl_size_t i = 0; i < VALUE_LIST_PER_EXCHANGE; i++)
            {
                // Set parameters for timeseries value list.
                code = add_value_list_to_timeseries(timeseries);

                // Sleep to get timeseries_values in 1 second period.
                sleep_in_seconds(1);
            }
        }
        if (MCL_OK == code)
        {
            // Exchange timeseries.
            code = safe_exchange(connectivity, core, timeseries);
            printf("Timeseries upload: %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
        }

        mcl_timeseries_destroy(&timeseries);
    }

    if (MCL_OK != code)
    {
        return -1;
    }

    return 0;
}

static mcl_error_t add_value_to_value_list(const char *data_point_id, const char *quality_code, const char *value, mcl_timeseries_value_list_t *timeseries_value_list)
{
    mcl_timeseries_value_t *timeseries_value = NULL;

    // Initialize timeseries value.
    mcl_error_t code = mcl_timeseries_value_initialize(&timeseries_value);
    printf("Timeseries value creation : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));

    if (MCL_OK == code)
    {
        // Set parameter data point id.
        code = mcl_timeseries_value_set_parameter(timeseries_value, MCL_TIMESERIES_VALUE_PARAMETER_DATA_POINT_ID, data_point_id);
        printf("|-->Adding data point id to timeseries value : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
    }

    if (MCL_OK == code)
    {
        // Set parameter quality code.
        code = mcl_timeseries_value_set_parameter(timeseries_value, MCL_TIMESERIES_VALUE_PARAMETER_QUALITY_CODE, quality_code);
        printf("|-->Adding quality code to timeseries value : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
    }

    if (MCL_OK == code)
    {
        // Set parameter value.
        code = mcl_timeseries_value_set_parameter(timeseries_value, MCL_TIMESERIES_VALUE_PARAMETER_VALUE, value);
        printf("|-->Adding value to timeseries value : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
    }

    if (MCL_OK == code)
    {
        // Add timeseries value to timeseries value list.
        code = mcl_timeseries_value_list_add_value(timeseries_value_list, timeseries_value);
        printf("|-->Adding timeseries value to timeseries value list : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
    }

    if (MCL_OK != code)
    {
        mcl_timeseries_value_destroy(&timeseries_value);
    }

    return code;
}

static mcl_error_t add_value_list_to_timeseries(mcl_timeseries_t *timeseries)
{
    char timestamp[MCL_TIMESTAMP_LENGTH];
    mcl_timeseries_value_list_t *timeseries_value_list = NULL;

    // Get local time to create timestamp.
    time_t timer;
    time(&timer);

    mcl_error_t code = mcl_time_util_convert_to_iso_8601_format(&timer, timestamp);
    printf("Converting time_t to iso 8601 format : %s.\n", MCL_CORE_CODE_TO_STRING(code));

    if (MCL_OK == code)
    {
        // Initialize timeseries value list.
        code = mcl_timeseries_value_list_initialize(&timeseries_value_list);
        printf("Timeseries value list creation : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
    }

    if (MCL_OK == code)
    {
        // Set timestamp for timeseries value list.
        code = mcl_timeseries_value_list_set_parameter(timeseries_value_list, MCL_TIMESERIES_VALUE_LIST_PARAMETER_TIMESTAMP_ISO8601, timestamp);
        printf("|-->Setting timestamp of timeseries value list : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
    }

    // Data point parameters will be added to timeseries value list.
    if (MCL_OK == code)
    {
        for (mcl_size_t data_point = 0; data_point < DATA_POINT_COUNT; data_point++)
        {
            char *value = NULL;

            // Read values from related sensors.
            read_data_function_list[data_point](&value);

            // Set parameters for timeseries value.
            code = add_value_to_value_list(data_point_id_list[data_point], QUALITY_CODE_GOOD, value, timeseries_value_list);

            free(value);
        }
    }
    if (MCL_OK == code)
    {
        code = mcl_timeseries_add_value_list(timeseries, timeseries_value_list);
        printf("|-->Adding timeseries value list to timeseries : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
    }

    if (MCL_OK != code)
    {
        mcl_timeseries_value_list_destroy(&timeseries_value_list);
    }

    return code;
}

static mcl_error_t safe_exchange(mcl_connectivity_t *connectivity, mcl_core_t *core, void *item)
{
    // Exchange item.
    mcl_error_t code = mcl_connectivity_exchange(connectivity, item);
    printf("Uploading item : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));

    if (MCL_UNAUTHORIZED == code || MCL_NO_ACCESS_TOKEN_EXISTS == code)
    {
        // Get access token.
        code = mcl_core_get_access_token(core);
        printf("Getting new access token : %s.\n", MCL_CORE_CODE_TO_STRING(code));

        if (MCL_BAD_REQUEST == code)
        {
            // Rotate key.
            code = mcl_core_rotate_key(core);
            printf("Calling rotate key : %s.\n", MCL_CORE_CODE_TO_STRING(code));

            // If key rotation is successful, then try to get access token.
            if (MCL_OK == code)
            {
                // Get access token.
                code = mcl_core_get_access_token(core);
                printf("Getting new access token : %s.\n", MCL_CORE_CODE_TO_STRING(code));
            }
        }

        if (MCL_OK == code)
        {
            // Exchange item.
            code = mcl_connectivity_exchange(connectivity, item);
            printf("Retrying exchange : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
        }
    }

    return code;
}

mcl_bool_t read_temperature(char **buffer)
{
    (*buffer) = (char*)malloc(VALUE_BUFFER_LENGTH * sizeof(*buffer));
    float temperature = (float) (25.0 + ((rand() % 32) / 32.0));
    snprintf(*buffer, VALUE_BUFFER_LENGTH, "%2.4f", temperature);

    return MCL_TRUE;
}

mcl_bool_t read_humidity(char **buffer)
{
    (*buffer) = (char*)malloc(VALUE_BUFFER_LENGTH * sizeof(*buffer));
    float humidity = (float) (70.0 + ((rand() % 256) / 32.0));
    snprintf(*buffer, VALUE_BUFFER_LENGTH, "%2.4f", humidity);

    return MCL_TRUE;
}

mcl_bool_t read_pressure(char **buffer)
{
    (*buffer) = (char*)malloc(VALUE_BUFFER_LENGTH * sizeof(*buffer));
    float pressure = (float) (3000.0 + ((rand() % 256) / 64.0));
    snprintf(*buffer, VALUE_BUFFER_LENGTH, "%4.2f", pressure);

    return MCL_TRUE;
}

mcl_bool_t read_voltage_motor_a(char **buffer)
{
    (*buffer) = (char*)malloc(VALUE_BUFFER_LENGTH * sizeof(*buffer));
    float voltage_motor_a = (float) (220.0 + ((rand() % 32) / 64.0));
    snprintf(*buffer, VALUE_BUFFER_LENGTH, "%3.3f", voltage_motor_a);

    return MCL_TRUE;
}

mcl_bool_t read_current_motor_a(char **buffer)
{
    (*buffer) = (char*)malloc(VALUE_BUFFER_LENGTH * sizeof(*buffer));
    float current_motor_a = (float) (5.0 + ((float)(rand() % 32) / 64.0));
    snprintf(*buffer, VALUE_BUFFER_LENGTH, "%1.5f", current_motor_a);

    return MCL_TRUE;
}

mcl_bool_t read_voltage_motor_b(char **buffer)
{
    (*buffer) = (char*)malloc(VALUE_BUFFER_LENGTH * sizeof(*buffer));
    float voltage_motor_b = (float) (380.0 + ((float)(rand() % 32) / 64.0));
    snprintf(*buffer, VALUE_BUFFER_LENGTH, "%3.3f", voltage_motor_b);

    return MCL_TRUE;
}

mcl_bool_t read_current_motor_b(char **buffer)
{
    (*buffer) = (char*)malloc(VALUE_BUFFER_LENGTH * sizeof(*buffer));
    float current_motor_b = (float) (60.0 + ((rand() % 32) / 64.0));
    snprintf(*buffer, VALUE_BUFFER_LENGTH, "%2.4f", current_motor_b);

    return MCL_TRUE;
}

static void sleep_in_seconds(mcl_uint32_t seconds)
{
#if defined(WIN32) || defined(WIN64)
    Sleep(seconds * 1000);
#else
    sleep(seconds);
#endif
}
