#include "utils.h"

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include "config.h"

#define READ_BUFFER 1024

static mcl_error_t save_data_source_configuration_string(char* file_name, char *configuration_string)
{
	mcl_error_t code = MCL_OK;
	FILE *fd = fopen(file_name, "w");
	if (NULL == fd) {
		int err = fputs(configuration_string, fd);
		if (err < 0) {
			code = MCL_FAIL;
			printf("ERROR: Unable to write file %s\n", file_name);
		}
		fclose(fd);
	}
	else
	{
		code = MCL_FAIL;
		printf("ERROR: unable to open file for writing: %s\n", file_name);
	}

	return code;
}


static mcl_error_t load_configuration_file(const char* file_name, char** configuration_string) {
	mcl_error_t code = MCL_OK;
	FILE *fd = NULL;

	if (NULL == file_name) {
		code = MCL_TRIGGERED_WITH_NULL;
		printf("ERROR: expected filename is empty!\n");
		return code;
	}

	if (NULL == configuration_string) {
		code = MCL_TRIGGERED_WITH_NULL;
		printf("ERROR: expected buffer is empty!\n");
		return code;
	}

	fd = fopen(file_name, "r");
	
	if (NULL != fd) {
		char buffer[READ_BUFFER];
		*configuration_string = NULL;
		while (feof(fd) == 0 && MCL_OK == code) {
			if (fgets(buffer, READ_BUFFER, fd)) {
				if (NULL == *configuration_string) {
					*configuration_string = (char*)malloc(strlen(buffer) + 1);
					if(NULL != *configuration_string)
						(*configuration_string)[0] = 0;
					else {
						code = MCL_OUT_OF_MEMORY;
						printf("ERROR: Unable to alocate memory for configuration string.\n");
					}
				}
				else {
					*configuration_string = (char*)realloc(*configuration_string, strlen(*configuration_string) + strlen(buffer) + 1);
					if (NULL == *configuration_string) {
						code = MCL_OUT_OF_MEMORY;
						printf("ERROR: Unable to alocate memory for configuration string.\n");
					}
				}

				if(code == MCL_OK && NULL != *configuration_string)
					strcat(*configuration_string, buffer);
			}
			else {
				code = MCL_FAIL;
				printf("ERROR reading file %\n", file_name);
			}
		}
		fclose(fd);
	}
	else {
		code = MCL_FAIL;
		printf("ERROR: Unable to open file %s\n", file_name);
	}

	return code;
}

mcl_error_t save_data_source_configuration(mcl_json_t *configuration_json) {
	mcl_error_t code;
	char *configuration_string = NULL;

	code = mcl_json_util_to_string(configuration_json, &configuration_string);
		printf("Configuration json to string conversion : %s.\n", MCL_CORE_CODE_TO_STRING(code));

	if (MCL_OK == code)
	{
		printf("Saving configuration.\n");
		code = save_data_source_configuration_string(CONFIGURATION_FILE_NAME, configuration_string);
	}

	if (NULL != configuration_string)
		free(configuration_string);

	return code;
}

mcl_error_t load_json_configuration(const char* file_name, mcl_json_t **configuration_json) {
	mcl_error_t code = MCL_OK;
	char* configuration_string = NULL;

	code = load_configuration_file(file_name, &configuration_string);

	if(MCL_OK == code && NULL != configuration_string) {
		code = mcl_json_util_parse(configuration_string, 0, configuration_json);
	}

	if (NULL != configuration_string)
		free(configuration_string);

	return code;
}

mcl_error_t load_data_source_configuration(mcl_json_t **configuration_json) {
	return load_json_configuration(CONFIGURATION_FILE_NAME, configuration_json);
}


		