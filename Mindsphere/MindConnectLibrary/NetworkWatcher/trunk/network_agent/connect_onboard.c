#include "connect_onboard.h"
#include <mcl_connectivity/mcl_connectivity.h>
#include <mcl_core/mcl_random.h>
#include <memory.h>
#include <stdio.h>
#include "callbacks.h"
#include "config.h"

void init_mcl(mcl_t* mcl_data) {
	memset(mcl_data, 0, sizeof(mcl_t));
}

int login_onboard(mcl_t* mcl) {
	mcl_error_t code;

	// Core configuration and related parameters.
	mcl_uint16_t mindsphere_port = 443;
//	mcl_uint16_t proxy_port = 3128;
//	E_MCL_PROXY mcl_proxy = MCL_PROXY_HTTP;
	E_MCL_SECURITY_PROFILE security_profile = MCL_SECURITY_RSA_3072;
	mcl_size_t maximum_payload_size = 65536;

	// Set log level. Note that default log level is the level set during build.
	mcl_log_util_set_output_level(MCL_LOG_LEVEL_INFO);

	// Initialize core configuration.
	code = mcl_core_configuration_initialize(&(mcl->core_configuration));
	printf("Core configuration initialization : %s.\n", MCL_CORE_CODE_TO_STRING(code));

	if (MCL_OK == code)
	{
		// Set core configuration parameters.
		mcl_core_configuration_set_parameter(mcl->core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_MDSP_HOST, "https://southgate.eu1.mindsphere.io");
		mcl_core_configuration_set_parameter(mcl->core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_MDSP_PORT, &mindsphere_port);
		mcl_core_configuration_set_parameter(mcl->core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_CERTIFICATE, mindsphere_certificate);
		mcl_core_configuration_set_parameter(mcl->core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_SECURITY_PROFILE, &security_profile);
		mcl_core_configuration_set_parameter(mcl->core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_USER_AGENT, "network agent v1.0");
		//        mcl_core_configuration_set_parameter(core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_PROXY_HOST, "192.168.0.1");
		//        mcl_core_configuration_set_parameter(core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_PROXY_PORT, &proxy_port);
		//        mcl_core_configuration_set_parameter(core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_PROXY_TYPE, &mcl_proxy);

		mcl_core_configuration_set_parameter(mcl->core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_TENANT, "lmtecdev");
		mcl_core_configuration_set_parameter(mcl->core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_CREDENTIALS_LOAD_CALLBACK, custom_load_function_rsa);
		mcl_core_configuration_set_parameter(mcl->core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_CREDENTIALS_SAVE_CALLBACK, custom_save_function_rsa);

		// Initialize mcl core with the core configuration.
		code = mcl_core_initialize(mcl->core_configuration, &mcl->core);
		printf("MCL core initialization : %s.\n", MCL_CORE_CODE_TO_STRING(code));
	}

	if (MCL_NO_ACCESS_TOKEN_PROVIDED == code) {
		mcl_core_configuration_set_parameter(mcl->core_configuration, MCL_CORE_CONFIGURATION_PARAMETER_IAT, initial_access_token);
		code = mcl_core_initialize(mcl->core_configuration, &mcl->core);
	}

	// If mcl core is successfully initialized, try to onboard.
	if (MCL_OK == code)
	{
		code = mcl_core_onboard(mcl->core);
		printf("Onboarding : %s.\n", MCL_CORE_CODE_TO_STRING(code));
	}

	// If onboarding is successful or the agent is already onboarded, get access token.
	if (MCL_OK == code || MCL_ALREADY_ONBOARDED == code)
	{
		code = mcl_core_get_access_token(mcl->core);
		printf("Getting access token : %s.\n", MCL_CORE_CODE_TO_STRING(code));
	}



	// Initialize connectivity configuration.
	if (MCL_OK == code)
	{
		code = mcl_connectivity_configuration_initialize(&mcl->connectivity_configuration);
	}

	// Set core for connectivity configuration.
	if (MCL_OK == code)
	{
		code = mcl_connectivity_configuration_set_parameter(mcl->connectivity_configuration, MCL_CONNECTIVITY_CONFIGURATION_PARAMETER_CORE, mcl->core);
	}

	// Set max HTTP payload size for connectivity configuration.
	if (MCL_OK == code)
	{
		code = mcl_connectivity_configuration_set_parameter(mcl->connectivity_configuration, MCL_CONNECTIVITY_CONFIGURATION_PARAMETER_MAX_HTTP_PAYLOAD_SIZE, &maximum_payload_size);
	}

	// Initialize mcl connectivity.
	if (MCL_OK == code)
	{
		code = mcl_connectivity_initialize(mcl->connectivity_configuration, &mcl->connectivity);
	}

	// Initialize store.
	if (MCL_OK == code)
	{
		code = mcl_store_initialize(&mcl->store);
	}

	return code;
}

void shutdown_mcl(mcl_t* mcl_data) {
	mcl_store_destroy(&mcl_data->store);

	mcl_core_destroy(&(mcl_data->core));
	mcl_core_configuration_destroy(&(mcl_data->core_configuration));
	mcl_connectivity_destroy(&(mcl_data->connectivity));
	mcl_connectivity_configuration_destroy(&(mcl_data->connectivity_configuration));

	init_mcl(mcl_data);
}