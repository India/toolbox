#pragma once

#include <mcl_connectivity/mcl_connectivity.h>
#include <mcl_core/mcl_random.h>

typedef struct {
	mcl_core_t *core;
	mcl_core_configuration_t *core_configuration;
	mcl_connectivity_t *connectivity;
	mcl_connectivity_configuration_t *connectivity_configuration;
	mcl_store_t *store;
} mcl_t;

void init_mcl(mcl_t* mcl_data);
int login_onboard(mcl_t* mcl_data);
void shutdown_mcl(mcl_t* mcl_data);