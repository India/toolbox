//
// Created by karsten.meier on 14.10.2019.
//

#ifndef NETWORK_AGENT_UPLOAD_DSC_H
#define NETWORK_AGENT_UPLOAD_DSC_H

#include "connect_onboard.h"

int update_dsc(mcl_t* mcl);

#endif //NETWORK_AGENT_UPLOAD_DSC_H
