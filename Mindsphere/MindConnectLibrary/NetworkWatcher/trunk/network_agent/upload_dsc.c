//
// Created by karsten.meier on 14.10.2019.
//

/*!
 * @file     dsc_upload.c
 * @brief    Example custom agent code which uploads data source configuration.
 *
 * @copyright Copyright (C) 2019 Siemens Aktiengesellschaft.\n
 *            All rights reserved.
 */
#include "upload_dsc.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mcl_connectivity/mcl_connectivity.h>
#include <mcl_core/mcl_random.h>
#include "callbacks.h"
#include "config.h"
#include "utils.h"


// DATA_SOURCE_COUNT is used in main function: keep the name, change the value and update the tables accordingly if necessary.
#define DATA_SOURCE_COUNT 1

// These definitions are not used in main function: add/change/remove as long as the tables are kept consistent.
#define DATA_POINT_COUNT_MOTOR 2
#define DATA_POINT_COUNT_NETWORK 5
#define DATA_POINT_COUNT_ENVIRONMENTAL_SENSOR 3

// Only the tables with size DATA_SOURCE_COUNT are directly accessed in main.
// Data points for motor source: Name, Type, Unit, Description.

const char *data_point_ip[4]      = { "ip",   "STRING", "-", "IP of target" }; 
const char *data_point_minimum[4] = { "minimum", "INT", "ms", "Minimum Ping" };
const char *data_point_maximum[4] = { "maximum", "INT", "ms", "Maximum ping" };
const char *data_point_average[4] = { "average", "INT", "ms", "Avg of ping" };
const char *data_point_lost[4]    = { "lost", "INT", "pkg", "Lost Packages" };

const char **data_points_network[DATA_POINT_COUNT_NETWORK] = {
        data_point_minimum,
        data_point_maximum,
        data_point_average,
        data_point_lost,
        data_point_ip
};

// Data point table.
const char ***data_point_table[DATA_SOURCE_COUNT] = { data_points_network };
const mcl_size_t data_point_count_table[DATA_SOURCE_COUNT] = { DATA_POINT_COUNT_NETWORK};

// Data sources: Name, Description.
const char *data_source_network[2] = {"Network Data", "Network Ping Data" };

// Data source table.
const char **data_source_table[DATA_SOURCE_COUNT] = { data_source_network };


// There is no custom data field for environmental sensor for this example.
const char **data_source_custom_fields[DATA_SOURCE_COUNT] = { NULL };
const char **data_source_custom_values[DATA_SOURCE_COUNT] = { NULL };
const mcl_size_t data_source_custom_field_count[DATA_SOURCE_COUNT] = {0};

// There is no data point custom field for any data point in environmental sensor source for this example.
const char ***data_point_custom_fields[DATA_SOURCE_COUNT] = {NULL};
const char ***data_point_custom_values[DATA_SOURCE_COUNT] = {NULL};
mcl_size_t *data_point_custom_field_counts[DATA_SOURCE_COUNT] = {NULL};

int update_dsc(mcl_t* mcl)
{
	mcl_error_t code;

    printf("***** CUSTOM AGENT using MCL *****\n");
	// Data source configuration.
	mcl_data_source_configuration_t *data_source_configuration = NULL;
	
	// These will be used to save data source configuration.
	mcl_json_t *configuration_json = NULL;
	mcl_json_t *data_sources_json = NULL;

	// Data source configuration id.
	char *configuration_id = NULL;
   
    // Create data source configuration.
    code = mcl_data_source_configuration_initialize(MCL_DATA_SOURCE_CONFIGURATION_1_0, &data_source_configuration);
    printf("Data source configuration creation : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));

    if (MCL_OK == code)
    {
        code = mcl_random_generate_guid(&configuration_id);
        printf("Data source configuration id creation : %s.\n", MCL_CORE_CODE_TO_STRING(code));
    }

    if (MCL_OK == code)
    {
        code = mcl_data_source_configuration_set_parameter(data_source_configuration, MCL_DATA_SOURCE_CONFIGURATION_PARAMETER_ID, configuration_id);
        printf("Data source configuration id setting : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
    }

    if (MCL_OK == code)
    {
        code = mcl_json_util_initialize(MCL_JSON_OBJECT, &configuration_json);
        printf("Configuration json initialization : %s.\n", MCL_CORE_CODE_TO_STRING(code));
    }

    if (MCL_OK == code)
    {
        code = mcl_json_util_start_object(configuration_json, configuration_id, &data_sources_json);
        printf("Data sources json initialization : %s.\n", MCL_CORE_CODE_TO_STRING(code));
    }

    for (mcl_size_t i = 0; i < DATA_SOURCE_COUNT && MCL_OK == code; i++)
    {
        mcl_data_source_t *data_source = NULL;
        mcl_json_t *data_source_json = NULL;

        code = mcl_data_source_initialize(&data_source);
        printf("\nData source creation for \"%s\" : %s.\n", data_source_table[i][0], MCL_CONNECTIVITY_CODE_TO_STRING(code));

        if (MCL_OK == code)
        {
            code = mcl_json_util_start_object(data_sources_json, data_source_table[i][0], &data_source_json);
            printf("|-->Data source json initialization : %s.\n", MCL_CORE_CODE_TO_STRING(code));
        }

        if (MCL_OK == code)
        {
            code = mcl_data_source_set_parameter(data_source, MCL_DATA_SOURCE_PARAMETER_NAME, data_source_table[i][0]);
            printf("|-->Data source name setting : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
        }

        if (MCL_OK == code)
        {
            code = mcl_data_source_set_parameter(data_source, MCL_DATA_SOURCE_PARAMETER_DESCRIPTION, data_source_table[i][1]);
            printf("|-->Data source description setting to \"%s\" : %s.\n", data_source_table[i][1], MCL_CONNECTIVITY_CODE_TO_STRING(code));
        }

        if (MCL_OK == code && 0 != data_source_custom_field_count[i])
        {
            mcl_json_t *data_source_custom_data = NULL;

            code = mcl_json_util_initialize(MCL_JSON_OBJECT, &data_source_custom_data);
            printf("|-->Custom data json for data source creation : %s.\n", MCL_CORE_CODE_TO_STRING(code));

            for (mcl_size_t j = 0; j < data_source_custom_field_count[i] && MCL_OK == code; j++)
            {
               code = mcl_json_util_add_string(data_source_custom_data, data_source_custom_fields[i][j], data_source_custom_values[i][j]);
               printf("    |-->Custom field adding \"%s\" with value \"%s\" : %s.\n", data_source_custom_fields[i][j], data_source_custom_values[i][j], MCL_CORE_CODE_TO_STRING(code));
            }

            if (MCL_OK == code)
            {
                code = mcl_data_source_set_parameter(data_source, MCL_DATA_SOURCE_PARAMETER_CUSTOM_DATA, data_source_custom_data);
                printf("|-->Custom data field setting : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
            }

            mcl_json_util_destroy(&data_source_custom_data);
        }

        for (mcl_size_t j = 0; j < data_point_count_table[i] && MCL_OK == code; j++)
        {
            mcl_data_point_t *data_point = NULL;
            char *data_point_id = NULL;

            code = mcl_data_point_initialize(&data_point);
            printf("|\n|-->Data point creation for \"%s\" : %s.\n", data_point_table[i][j][0], MCL_CONNECTIVITY_CODE_TO_STRING(code));

            if (MCL_OK == code)
            {
                code = mcl_random_generate_guid(&data_point_id);
                printf("    |-->Data point id creation : %s.\n", MCL_CORE_CODE_TO_STRING(code));
            }

            if (MCL_OK == code)
            {
                code = mcl_data_point_set_parameter(data_point, MCL_DATA_POINT_PARAMETER_ID, data_point_id);
                printf("    |-->Data point id setting : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
            }

            if (MCL_OK == code)
            {
                code = mcl_data_point_set_parameter(data_point, MCL_DATA_POINT_PARAMETER_NAME, data_point_table[i][j][0]);
                printf("    |-->Data point name setting : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
            }

            if (MCL_OK == code)
            {
                code = mcl_data_point_set_parameter(data_point, MCL_DATA_POINT_PARAMETER_TYPE, data_point_table[i][j][1]);
                printf("    |-->Data point type setting to \"%s\" : %s.\n", data_point_table[i][j][1], MCL_CONNECTIVITY_CODE_TO_STRING(code));
            }

            if (MCL_OK == code)
            {
                code = mcl_data_point_set_parameter(data_point, MCL_DATA_POINT_PARAMETER_UNIT, data_point_table[i][j][2]);
                printf("    |-->Data point unit setting to \"%s\" : %s.\n", data_point_table[i][j][2], MCL_CONNECTIVITY_CODE_TO_STRING(code));
            }

            if (MCL_OK == code)
            {
                code = mcl_data_point_set_parameter(data_point, MCL_DATA_POINT_PARAMETER_DESCRIPTION, data_point_table[i][j][3]);
                printf("    |-->Data point description setting to \"%s\" : %s.\n", data_point_table[i][j][3], MCL_CONNECTIVITY_CODE_TO_STRING(code));
            }

            if (MCL_OK == code && NULL != data_point_custom_field_counts[i] && 0 != data_point_custom_field_counts[i][j])
            {
                mcl_json_t *data_point_custom_data = NULL;

                code = mcl_json_util_initialize(MCL_JSON_OBJECT, &data_point_custom_data);
                printf("    |-->Custom data json creation for data point : %s.\n", MCL_CORE_CODE_TO_STRING(code));

                for (mcl_size_t k = 0; k < data_point_custom_field_counts[i][j] && MCL_OK == code; k++)
                {
                    code = mcl_json_util_add_string(data_point_custom_data, data_point_custom_fields[i][j][k], data_point_custom_values[i][j][k]);
                    printf("        |-->Custom field adding \"%s\" with value \"%s\" : %s.\n", data_point_custom_fields[i][j][k], data_point_custom_values[i][j][k], MCL_CONNECTIVITY_CODE_TO_STRING(code));
                }

                if (MCL_OK == code)
                {
                    code = mcl_data_point_set_parameter(data_point, MCL_DATA_POINT_PARAMETER_CUSTOM_DATA, data_point_custom_data);
                    printf("    |-->Custom data field setting : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
                }

                mcl_json_util_destroy(&data_point_custom_data);
            }

            if (MCL_OK == code)
            {
                code = mcl_data_source_add_data_point(data_source, data_point);
                printf("    |-->Data point to data source adding : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
            }

            if (MCL_OK == code)
            {
                code = mcl_json_util_add_string(data_source_json, data_point_table[i][j][0], data_point_id);
                printf("    |-->Data point id to data source json adding : %s.\n", MCL_CORE_CODE_TO_STRING(code));
            }

            if (MCL_OK != code)
            {
                mcl_data_point_destroy(&data_point);
            }

            free(data_point_id);
        }

        if (MCL_OK == code)
        {
            code = mcl_data_source_configuration_add_data_source(data_source_configuration, data_source);
            printf("|-->Data source to data source configuration adding : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
        }

        if (MCL_OK != code)
        {
            mcl_data_source_destroy(&data_source);
        }
    }

    if (MCL_OK == code)
    {
        // Upload the data source configuration.
        code = mcl_connectivity_exchange(mcl->connectivity, data_source_configuration);
        printf("Data source configuration upload: %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
    }

    if (MCL_OK == code)
    {
		code = save_data_source_configuration(configuration_json);
    }

    // Clean up.
    free(configuration_id);
    mcl_json_util_destroy(&configuration_json);
	mcl_data_source_configuration_destroy(&data_source_configuration);


    return 0;
}

