#pragma once
#ifndef NETWORK_AGENT_UTILS_H
#define NETWORK_AGENT_UTILS_H

#include <mcl_core\mcl_core.h>

/**
* This function saves the given @p json object to a configuration file.
* The file name is defines by the macro CONFIGURATION_FILE_NAME
* defined in config.h
*
* @param [in] configuration_json Json object to be saved.
* @return
* <ul>
* <li>#MCL_OK in case of success.</li>
* <li>#MCL_FAIL if CONFIGURATION_FILE_NAME cannot be opened/created</li>
* <li>#MCL_TRIGGERED_WITH_NULL if @p configuration_json is NULL
* </ul>
*/
mcl_error_t save_data_source_configuration(mcl_json_t *configuration_json);

/**
* This function loads the configuration (JSON-) file described by
* macro CONFIGURATION_FILE_NAME defined in config.h
*
* @param [out] root @p configuration_json is going to be parsed to this root object.
* @return
* <ul>
* <li>#MCL_OK in case of success.</li>
* <li>#MCL_FAIL if CONFIGURATION_FILE_NAME cannot be opened/read orcontains invalid json format</li>
* <li>#MCL_TRIGGERED_WITH_NULL if @p configuration_json is NULL
* <li>#MCL_OUT_OF_MEMORY If memory cannot be allocated for root.</li>
* <li>#MCL_INVALID_PARAMETER If json does not end within @p buffer size.</li>
* </ul>
*/
mcl_error_t load_data_source_configuration(mcl_json_t **configuration_json);

/**
* This function loads and parses a JSON-file.
*
* @param [in] @p file_name complete path to json file.
* @param [out] root @p configuration_json is going to be parsed to this root object.
* @return
* <ul>
* <li>#MCL_OK in case of success.</li>
* <li>#MCL_FAIL if CONFIGURATION_FILE_NAME cannot be opened/read orcontains invalid json format</li>
* <li>#MCL_TRIGGERED_WITH_NULL if @p configuration_json is NULL
* <li>#MCL_OUT_OF_MEMORY If memory cannot be allocated for root.</li>
* <li>#MCL_INVALID_PARAMETER If json does not end within @p buffer size.</li>
* </ul>
*/
mcl_error_t load_json_configuration(const char* file_name, mcl_json_t **configuration_json);

#endif
