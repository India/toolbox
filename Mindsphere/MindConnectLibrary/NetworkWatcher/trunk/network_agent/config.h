//
// Created by karsten.meier on 14.10.2019.
//

#ifndef NETWORK_AGENT_CONFIG_H
#define NETWORK_AGENT_CONFIG_H

extern char *initial_access_token;
extern const char *mindsphere_certificate;
// Name of the configuration file which is used to save data point ids.
#define CONFIGURATION_FILE_NAME "configuration.txt"
#define PING_DATA "c:\\projects\\99_misc\\Mindsphere\\network_watcher\\dumps"

#endif //NETWORK_AGENT_CONFIG_H
