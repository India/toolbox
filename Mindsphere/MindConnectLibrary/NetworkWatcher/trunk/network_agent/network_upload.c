#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <windows.h>
#include <malloc.h>

#include "config.h"
#include "utils.h"
#include "cJSON.h"
#include "connect_onboard.h"
#include <mcl_core/mcl_core.h>
#include <mcl_connectivity/mcl_connectivity.h>

#define MAX_JSON_BY_CYCLE 20

static void safe_str_cpy(char** dst, const char* src);
static void read_config(char** config_id, char*** dp_names, char*** dp_ids, int *n);
static mcl_error_t safe_exchange(mcl_connectivity_t *connectivity, mcl_core_t *core, void *item);
static void get_entries(const char* ping_server, mcl_json_t*** entries, int* n);
static void upload_timeseries(mcl_t* mcl, char* config_id, char** dp_names, char** dp_ids, int dps, mcl_json_t** json_entries, int jsons);
static void delete_json_array(mcl_json_t** entries, int n);
static mcl_error_t add_ping_data(mcl_timeseries_t *timeseries, mcl_json_t* json, char** dp_names, char** dp_ids, int dps);
static void get_dp_id(char* name, char** dp_names, char** dp_ids, int dps, char** id);

void upload(mcl_t* mcl, const char* server) {

	char* config_id = NULL;
	char** dp_names = NULL;
	char** dp_ids = NULL;
	int dps = 0;

	mcl_json_t** json_entries = NULL;
	int jsons = 0;


	read_config(&config_id, &dp_names, &dp_ids, &dps);
	if (NULL != config_id && NULL != dp_names && NULL != dp_ids) {
		while (1) {
			get_entries(server, &json_entries, &jsons);

			if (jsons > 0) {
				upload_timeseries(mcl, config_id, dp_names, dp_ids, dps, json_entries, jsons);
			}
			//    m    s     ms
			Sleep(5 * 60 * 1000);
		}
	}

	delete_json_array(json_entries, jsons);
	free(dp_names);
	free(dp_ids);
}

static void delete_json_array(mcl_json_t** entries, int n) {
	if (0 >= n || entries == NULL)
		return;

	for (int i = 0; i < n; i++) {
		mcl_json_util_destroy(entries[i]);
	}
	free(entries);
}

static void get_entries(const char* ping_server, mcl_json_t*** entries, int* n) {
	mcl_error_t code = MCL_OK;
	WIN32_FIND_DATA file_data;
	HANDLE hFind = NULL;

	char directory[MAX_PATH + 1];
	char filter[MAX_PATH + 1];

	*n = 0;
	*entries = NULL;
	
	sprintf(directory, "%s\\%s", PING_DATA, ping_server);
	sprintf(filter, "%s\\*.json", directory);

	if ((hFind = FindFirstFile(filter, &file_data)) == INVALID_HANDLE_VALUE)
	{
		printf("Path not found: [%s]\n", directory);
		return;
	}

	do
	{
		char ping_file[MAX_PATH + 1];
		++(*n);
		if (1 == *n)
			*entries = (mcl_json_t**) malloc(sizeof(mcl_json_t*));
		else
			*entries = (mcl_json_t**) realloc(*entries, (*n) * sizeof(mcl_json_t*) );
		
		(*entries)[*n - 1] = NULL;
		sprintf(ping_file, "%s\\%s", directory, file_data.cFileName);
		code = load_json_configuration(ping_file, &((*entries)[*n - 1]));
		if (MCL_OK != code) {
			--(*n);
			printf("Error parding %s : %s.\n", ping_file, MCL_CORE_CODE_TO_STRING(code));
		}
		else {
			char cpy_file[MAX_PATH + 1];
			sprintf(cpy_file, "%s.read", ping_file);
			MoveFile(ping_file, cpy_file);
		}

	} while (*n < MAX_JSON_BY_CYCLE && FindNextFile(hFind, &file_data));

	FindClose(hFind);
}

static void get_dp_id(char* name, char** dp_names, char** dp_ids, int dps, char** id) {
	*id = NULL;

	for (int i = 0; i < dps; i++) {
		if (strcmp(name, dp_names[i]) == 0) {
			*id = dp_ids[i];
			break;
		}
	}
}

static mcl_error_t get_json_string_value(mcl_json_t* json, const char* name, char value[128]) {
	mcl_error_t code = MCL_OK;
	mcl_json_t *entry = NULL;

	code = mcl_json_util_get_object_item(json, name, &entry);
	if (MCL_OK == code) {
		char* json_value = NULL;
		code = mcl_json_util_get_string(entry, &json_value);
		strcpy(value, json_value);
		free(json_value);
	}
	return code;
}

static mcl_error_t get_json_int_value(mcl_json_t* json, const char* name, char value[128]) {
	mcl_error_t code = MCL_OK;
	mcl_json_t *entry = NULL;

	code = mcl_json_util_get_object_item(json, name, &entry);
	if (MCL_OK == code) {
		mcl_error_t json_value = 0;
		code = mcl_json_util_get_number_value(entry, &json_value);
		_itoa(json_value, value, 10);
	}
	return code;
}

static mcl_error_t set_timestamp(mcl_timeseries_value_list_t *timeseries_value_list, char value[128]) {
	mcl_error_t code = MCL_OK;
	char day[3];
	char mon[4];
	char year[5];
	char hour[3];
	char min[3];
	char sec[3];
	char ms[7];
	char* cmon = NULL;
	char timestamp[MCL_TIMESTAMP_LENGTH];

//	"14-Oct-2019_110704_174589"
	sscanf(value, "%2s-%3s-%4s_%2s%2s%2s_%6s", &day, &mon, &year, &hour, &min, &sec, &ms);

	if (strcmp(mon, "Jan") == 0)
		cmon = "01";
	if (strcmp(mon, "Feb") == 0)
		cmon = "02";
	if (strcmp(mon, "Mar") == 0)
		cmon = "03";
	if (strcmp(mon, "Apr") == 0)
		cmon = "04";
	if (strcmp(mon, "May") == 0)
		cmon = "05";
	if (strcmp(mon, "Jun") == 0)
		cmon = "06";
	if (strcmp(mon, "Jul") == 0)
		cmon = "07";
	if (strcmp(mon, "Aug") == 0)
		cmon = "08";
	if (strcmp(mon, "Sep") == 0)
		cmon = "09";
	if (strcmp(mon, "Oct") == 0)
		cmon = "10";
	if (strcmp(mon, "Nov") == 0)
		cmon = "11";
	if (strcmp(mon, "Dec") == 0)
		cmon = "12";

	ms[3] = 0;

	// yyyy-MM-ddTHH:mm:ss.SSSZ length including null character.
	sprintf(timestamp, "%s-%s-%sT%s:%s:%s.%sZ", year, cmon, day, hour, min, sec, ms);

	if (MCL_OK == code)
	{
		// Set timestamp for timeseries value list.
		code = mcl_timeseries_value_list_set_parameter(timeseries_value_list, MCL_TIMESERIES_VALUE_LIST_PARAMETER_TIMESTAMP_ISO8601, timestamp);
		printf("|-->Setting timestamp of timeseries value list : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}

	return code;
}

static mcl_error_t add_data(char* id, char* value, mcl_timeseries_value_list_t *timeseries_value_list) {
	mcl_timeseries_value_t *timeseries_value = NULL;

	// Initialize timeseries value.
	mcl_error_t code = mcl_timeseries_value_initialize(&timeseries_value);
	printf("Timeseries value creation : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));

	if (MCL_OK == code)
	{
		// Set parameter data point id.
		code = mcl_timeseries_value_set_parameter(timeseries_value, MCL_TIMESERIES_VALUE_PARAMETER_DATA_POINT_ID, id);
		printf("|-->Adding data point id to timeseries value : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}

	if (MCL_OK == code)
	{
		// Set parameter quality code.
		code = mcl_timeseries_value_set_parameter(timeseries_value, MCL_TIMESERIES_VALUE_PARAMETER_QUALITY_CODE, "00000000");
		printf("|-->Adding quality code to timeseries value : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}

	if (MCL_OK == code)
	{
		// Set parameter value.
		code = mcl_timeseries_value_set_parameter(timeseries_value, MCL_TIMESERIES_VALUE_PARAMETER_VALUE, value);
		printf("|-->Adding value to timeseries value : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}

	if (MCL_OK == code)
	{
		// Add timeseries value to timeseries value list.
		code = mcl_timeseries_value_list_add_value(timeseries_value_list, timeseries_value);
		printf("|-->Adding timeseries value to timeseries value list : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}

	if (MCL_OK != code)
	{
		mcl_timeseries_value_destroy(&timeseries_value);
	}

	return code;
}

static mcl_error_t add_ping_data(mcl_timeseries_t *timeseries, mcl_json_t* json, char** dp_names, char** dp_ids, int dps) {
	mcl_error_t code = MCL_OK;
	char buffer[128];
	char* id;
	mcl_timeseries_value_list_t *timeseries_value_list = NULL;

	code = get_json_string_value(json, "time", buffer);
	
	if (MCL_OK == code)
	{
		// Initialize timeseries value list.
		code = mcl_timeseries_value_list_initialize(&timeseries_value_list);
		printf("Timeseries value list creation : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}

	if (MCL_OK == code) {
		code = set_timestamp(timeseries_value_list, buffer);
		printf("set time stamp : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}


	code = get_json_string_value(json, "ip", buffer);
	get_dp_id("ip", dp_names, dp_ids, dps, &id);
	add_data(id, buffer, timeseries_value_list);

	code = get_json_int_value(json, "lost", buffer);
	get_dp_id("lost", dp_names, dp_ids, dps, &id);
	add_data(id, buffer, timeseries_value_list);

	code = get_json_int_value(json, "minimum", buffer);
	get_dp_id("minimum", dp_names, dp_ids, dps, &id);
	add_data(id, buffer, timeseries_value_list);

	code = get_json_int_value(json, "maximum", buffer);
	get_dp_id("maximum", dp_names, dp_ids, dps, &id);
	add_data(id, buffer, timeseries_value_list);
	
	code = get_json_int_value(json, "average", buffer);
	get_dp_id("average", dp_names, dp_ids, dps, &id);
	add_data(id, buffer, timeseries_value_list);



	if (MCL_OK == code)
	{
		code = mcl_timeseries_add_value_list(timeseries, timeseries_value_list);
		printf("|-->Adding timeseries value list to timeseries : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}

	if (MCL_OK != code)
	{
		mcl_timeseries_value_list_destroy(&timeseries_value_list);
	}

	return code;
}

static void upload_timeseries(mcl_t* mcl, char* config_id, char** dp_names, char** dp_ids, int dps, mcl_json_t** json_entries, int jsons) {
	mcl_error_t code = MCL_OK;
	mcl_timeseries_t *timeseries = NULL;

	// Create timeseries.
	code = mcl_timeseries_initialize(MCL_TIMESERIES_VERSION_1_0, &timeseries);
	printf("Timeseries initialization : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));

	if (MCL_OK == code)
	{
		// Set configuration id.
		code = mcl_timeseries_set_parameter(timeseries, MCL_TIMESERIES_PARAMETER_CONFIGURATION_ID, config_id);
		printf("Setting configuration id : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}

	for (int i = 0; i < jsons; i++) {
		add_ping_data(timeseries, json_entries[i], dp_names, dp_ids, dps);
	}

	if (MCL_OK == code)
	{
		mcl_store_add(mcl->store, timeseries);

		// Exchange timeseries.
		code = safe_exchange(mcl->connectivity, mcl->core, mcl->store);
		printf("Timeseries upload: %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
	}

//	if(MCL_OK != code)
//		mcl_timeseries_destroy(&timeseries);
}

static mcl_error_t safe_exchange(mcl_connectivity_t *connectivity, mcl_core_t *core, void *item)
{
	// Exchange item.
	mcl_error_t code = mcl_connectivity_exchange(connectivity, item);
	printf("Uploading item : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));

	if (MCL_UNAUTHORIZED == code || MCL_NO_ACCESS_TOKEN_EXISTS == code)
	{
		// Get access token.
		code = mcl_core_get_access_token(core);
		printf("Getting new access token : %s.\n", MCL_CORE_CODE_TO_STRING(code));

		if (MCL_BAD_REQUEST == code)
		{
			// Rotate key.
			code = mcl_core_rotate_key(core);
			printf("Calling rotate key : %s.\n", MCL_CORE_CODE_TO_STRING(code));

			// If key rotation is successful, then try to get access token.
			if (MCL_OK == code)
			{
				// Get access token.
				code = mcl_core_get_access_token(core);
				printf("Getting new access token : %s.\n", MCL_CORE_CODE_TO_STRING(code));
			}
		}

		if (MCL_OK == code)
		{
			// Exchange item.
			code = mcl_connectivity_exchange(connectivity, item);
			printf("Retrying exchange : %s.\n", MCL_CONNECTIVITY_CODE_TO_STRING(code));
		}
	}

	return code;
}

void read_config(char** config_id, char*** dp_names, char*** dp_ids, int *n) {
	mcl_error_t code = MCL_OK;
	mcl_json_t *data_source_config = NULL;
	mcl_json_t *config_item = NULL;
	mcl_json_t *ds_item = NULL;

	*n = 0;
	code = load_data_source_configuration(&data_source_config);

	if (MCL_OK == code) {
		char* name = NULL;
		mcl_size_t objects = 0;

		mcl_json_util_get_array_size(data_source_config, &objects);
		if(1 == objects) {
			mcl_json_util_get_array_item(data_source_config, 0, &config_item);
			safe_str_cpy(config_id, ((cJSON*)config_item)->string);
			printf("Config ID: %s\n", *config_id);
		}

		if (NULL != config_item) {
			mcl_json_util_get_array_size(config_item, &objects);
			if (1 == objects) {
				mcl_json_util_get_array_item(config_item, 0, &ds_item);
				printf("DS Name: %s\n", ((cJSON*)ds_item)->string);
			}
		}

		if (NULL != ds_item) {
			mcl_json_util_get_array_size(ds_item, &objects);
			*n = (int) objects;
			if (0 < objects) {
				*dp_names = (char**)malloc(objects * sizeof(char*));
				*dp_ids = (char**)malloc(objects * sizeof(char*));
				memset(*dp_names, 0, objects * sizeof(char*));
				memset(*dp_ids, 0, objects * sizeof(char*));

				for (int i = 0; i < objects; i++) {
					mcl_json_t *item = NULL;
					mcl_json_util_get_array_item(ds_item, i, &item);

					safe_str_cpy(&((*dp_names)[i]), ((cJSON*)item)->string);
					safe_str_cpy(&((*dp_ids)[i]), ((cJSON*)item)->valuestring);
					
					printf("Datapoint: %s : %s\n", (*dp_names)[i], (*dp_ids)[i]);
				}
			}
		}

		if (NULL != data_source_config)
			mcl_json_util_destroy(&data_source_config);
	}
}

static void safe_str_cpy(char** dst, const char* src) {
	if (NULL != *dst) {
		free(*dst);
		*dst = NULL;
	}

	if (NULL == src)
		return;
	*dst = (char*)malloc(strlen(src));
	strcpy(*dst, src);
}
