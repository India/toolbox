import subprocess
import os
import threading
import time
import re
import json
from config import output_base
from datetime import datetime, timezone


class PingHandler(threading.Thread):
    _server = ""
    _intv = 10
    _pings = 10
    _stopEvent = None

    def __init__(self, server, intv=10):
        super(PingHandler, self).__init__()
        self._server = server
        self._intv = intv
        self._stopEvent = threading.Event()

    def run(self):
        while not self._stopEvent.is_set():
            self._doMeasure()
            time.sleep(self._intv)

    def _doMeasure(self):
        data = {}
        now = datetime.now(timezone.utc)
        timestamp = now.strftime("%d-%b-%Y_%H%M%S_%f")

        data["time"] = timestamp
        data['url'] = self._server

        minimum = float('Inf')
        maximum = float('Inf')
        average = float('Inf')
        lost = float('Inf')

        count_flag = '-n'

        try:
            ping = subprocess.Popen(['ping', self._server, count_flag, str(self._pings)],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            (out, err) = ping.communicate()
            if out:
                try:
                    out = out.decode()
                    # Windows-specific output parsing
                    data["minimum"] = int(re.findall(r'Minimum = (\d+)', out)[0])
                    data["maximum"] = int(re.findall(r'Maximum = (\d+)', out)[0])
                    data["average"] = int(re.findall(r'Average = (\d+)', out)[0])
                    data["lost"] = int(re.findall(r'Lost = (\d+)', out)[0])
                    data["ip"] = re.findall(r'Reply from (\d+\.\d+\.\d+\.\d+)', out)[0]
                    self.writeLog(data)
                except:
                    print('No data for one of minimum/maximum/average/lost')

        except subprocess.CalledProcessError:
            print('Could not get a ping!')


        print("Pinging {} with timestamp {}".format(self._server, timestamp))

    def writeLog(self, data):
        try:
            path = output_base + "\\" + self._server
            if not os.path.exists(path):
                os.makedirs(path)
            filename = path + "\\" + data['time'] + ".json"
            f = open(filename, "w")
            f.write(json.dumps(data))
            f.close()
        except OSError as err:
            print("Unexpected error:", err.strerror)


    def doStop(self):
        self._stopEvent.set()
