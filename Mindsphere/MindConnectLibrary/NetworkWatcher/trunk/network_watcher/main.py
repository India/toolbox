from PingHandler import PingHandler
import time
import json
from config import servers, ping_interval, max_time_to_run

def main():
    # TODO: Read config files of servers to ping

    # spawn jobs
    print(json.dumps(servers))
    handler = []
    i = 0
    for server in servers:
        current = PingHandler(server['server'], intv=ping_interval)
        handler.append(current)
        current.start()

    if max_time_to_run < 0:
        while True:
            time.sleep(3600)
    else:
        time.sleep(max_time_to_run)

    for h in handler:
        h.doStop()

    for h in handler:
        h.join()


if __name__ == '__main__':
    main()
