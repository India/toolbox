# max time of application to run in [s].
# -1: infinite run
max_time_to_run = -1

# Interval between pings (in [s])
ping_interval = 60

# base of output path
output_base = ".\\dumps\\"

# servers to ping
servers = [
    {'server': "www.heise.de"}
#    ,{'server': "twitter.com"}
    ,{'server': "www.lmtec.eu"}
]