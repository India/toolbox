This tool constantly pings the configured servers and writes down the timing in a json file.

In config.py the times and the main directory for the json files are configured.

For each pinged server a subdirectory is created below the "dump" path.