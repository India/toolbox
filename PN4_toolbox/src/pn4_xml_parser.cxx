#include <pn4_itk_mem_free.hxx>
#include <pn4_xml_parser.hxx>
#include <pn4_const.hxx>

DOMElement*   rootElement       = NULL;
/**
 * This function returns the size of the input file 
 *
 * @param[in]  pszfile           The complete path of the file whose size is to be determined.
 *
 * @return  file size on successful execution, else returns -1.
 */
int PN4_get_file_size
(
    char* pszfile
)
{
    struct stat info;
    
    if(0==stat( pszfile, &info))
        return info.st_size;
    else
        return -1;
}

/**
 * It gives the data from the specified XML element.
 *
 * @param[in]  elementId    DOMElement Object.
 * @param[in]  elementName  Name of the XML element for which the data is enquired.
 *
 * @return  return a pointer to the first non-space character.
 */
char* PN4_get_element_data
(
    DOMElement  *elementId,
    char        *elementName
)
{
    char            *pszContent     = NULL;
    
    DOMNodeList     *nl             = NULL;
    DOMElement      *currElm        = NULL;

    nl = elementId->getElementsByTagName(XMLString::transcode(elementName));
     
    if(nl != NULL && nl->getLength() > 0)
    {
        currElm = (DOMElement *) nl->item(0);

        if( tc_strlen( XMLString::transcode(currElm->getTextContent()) ) > 0 )
        {
            pszContent = (char *) MEM_alloc( sizeof(char) *
                (int)tc_strlen( XMLString::transcode(currElm->getTextContent()))+1 );
            tc_strcpy( pszContent, XMLString::transcode(currElm->getTextContent()) );
        }
    }
    return pszContent != NULL ? pszContent : (char *)"";
}

/**
 * This function parses the input xml file and 
 *
 * @param[in]  xmlfile          The input xml file path.
 * @param[out] piItemCount      Total items count in the xml file.
 * @param[out] piEBOMCount      The EBOM count in the xml file.
 * @param[out] piPBOMCount      The PBOM count in the xml file.
 * @param[out] ppstrBOMHeaderData The Data structure of the whole xml.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int PN4_run_xml_parser
(    
    char*  pszxmlfile,
    map<string, vector<string>> &mAttrData
)
{
	int           itemCount      = 0;	
	DOMElement*   currItemElm    = NULL;    
	DOMNodeList*  RuleNodeList   = NULL;
	xercesc::DOMDocument *doc    = NULL; 
    
    try 
    {
        XMLPlatformUtils::Initialize();
         
        XercesDOMParser *parser = new XercesDOMParser();

        parser->parse(pszxmlfile);
        
        doc = parser->getDocument();

        rootElement = doc->getDocumentElement();
 
        RuleNodeList = rootElement->getElementsByTagName(XMLString::transcode("rule"));

        itemCount = (int)(RuleNodeList->getLength());

		for (int inx = 0; inx < itemCount; inx++) 
        {
			DOMElement*   RuleNodeElement     = NULL;
			DOMNodeList*  primaryNodeList     = NULL;
			DOMNodeList*  secondaryNodeList   = NULL;
			char* pszPrimRuleStr    = NULL;
			char* pszPrimProp       = NULL;
			char* pszOperator       = NULL;
			char* pszSecRuleStr     = NULL;
			char* pszSecProp        = NULL;

			RuleNodeElement =  (DOMElement *) RuleNodeList->item(0);

			primaryNodeList = RuleNodeElement->getElementsByTagName(XMLString::transcode("primary"));
			currItemElm     = (DOMElement *) primaryNodeList->item(0);
			pszPrimRuleStr  = PN4_get_element_data(currItemElm, "objects");
			pszPrimProp		= PN4_get_element_data(currItemElm, "properties");

			currItemElm = (DOMElement *) RuleNodeList->item(0);
			pszOperator = PN4_get_element_data(currItemElm, "operator");

			secondaryNodeList = RuleNodeElement->getElementsByTagName(XMLString::transcode("secondary"));	
			currItemElm       = (DOMElement *) secondaryNodeList->item(0);
			pszSecRuleStr     = PN4_get_element_data(currItemElm, "objects");
			pszSecProp		  = PN4_get_element_data(currItemElm, "properties");
			
			PN4_MEM_TCFREE(pszPrimRuleStr);
			PN4_MEM_TCFREE(pszPrimProp);
			PN4_MEM_TCFREE(pszOperator);
			PN4_MEM_TCFREE(pszSecRuleStr);
			PN4_MEM_TCFREE(pszSecProp);
		}
	}
	catch (const XMLException& toCatch) 
    {
        char* message = XMLString::transcode(toCatch.getMessage());
        printf( "Error during initialization of XML Parser! \n %s\n",message);
        XMLString::release(&message);
        return 1;
    }
    catch (const DOMException& toCatch)
    {
        char* message = XMLString::transcode(toCatch.msg);
        printf("Exception message is: \n %s\n",message);
        XMLString::release(&message);
        return -1;
    }
    catch (...) 
    {
        printf( "Unexpected Exception during XML Parsing\n" );
        return -1;
    }

	return ITK_ok;
}