/*=========================================================================================================================================
                Copyright 2012 Prion
                Unpublished - All rights reserved
===========================================================================================================================================
File Description:

    Filename: pn4_register_callbacks.cxx

    Description:

===========================================================================================================================================

Date          Name                Description of Change
-------------------------------------------------------------------------------------------------------------------------------------------
26-Nov-12     Tarun Kumar Singh    Initial Release

===========================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <pn4_register_callbacks.hxx>

/**
 * Description:
 *
 * Function to register Callback function.
 * Entry point for custom PN4 Code.
 *
 * Returns:
 *
 * Status of execution.
 */
int PN4_toolbox_register_callbacks ()
{

    int  iRetCode   = ITK_ok;

    printf("INFO: Registered PN4_toolbox services\n");


    iRetCode = CUSTOM_register_exit ("PN4_toolbox", "USER_gs_shell_init_module", (CUSTOM_EXIT_ftn_t)PN4_register_custom_handlers);
    if(iRetCode!=ITK_ok)
    {
        printf("ERROR: Unable to register custom handlers.");
        return iRetCode;
    }

    return iRetCode;
}


#ifdef __cplusplus
}
#endif



