/*=========================================================================================================================================
                Copyright 2012  Prion
                Unpublished - All rights reserved
===========================================================================================================================================

File Description:

    Filename: pn4_const.hxx

    Description:  Header File for constants used during scope of the DLL.

===========================================================================================================================================

Date          Name                Description of Change
-------------------------------------------------------------------------------------------------------------------------------------------
26-Nov-12    Tarun Kumar Singh   Initial Release

===========================================================================================================================================*/


#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item.h>
#include <tccore/item_msg.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tcinit/tcinit.h>
#include <tccore/aom.h>
#include <tccore/aom_prop.h>
#include <ps/ps.h>
#include <pom/enq/enq.h>
#include <bom/bom.h>
#include <cfm/cfm_item.h>
#include <me/me.h>
#include <fclasses/tc_string.h>
#include <form.h>
#include <tc/preferences.h>

/* Date Time Format */
#define PN4_DATE_TIME_FORMAT_FILENAME                            "%y%m%d%H%M%S"

/* Relation    */
#define PN4_IMAN_SPECIFICATION_RELATION                          "IMAN_specification"
#define PN4_IMAN_BASED_ON                                        "IMAN_based_on"
#define PN4_IMAN_MASTER_FORM                                     "IMAN_master_form"

/* Dataset Named References */
#define PN4_DATASET_XML_REFERENCE                                "XML"

/* Attributes  */
#define PN4_ATTR_DATE_RELEASED                                   "date_released"
#define PN4_ATTR_RELEASE_STATUS_LIST                             "release_status_list"
#define PN4_ATTR_RELEASE_STATUS_NAME                             "name"
#define PN4_ATTR_OBJECT_NAME_ATTRIBUTE                           "object_name"
#define PN4_ATTR_DESCRIPTION_ATTRIBUTE                           "object_desc"
#define PN4_ATTR_ITEM_ID                                         "item_id"
#define PN4_ATTR_ITEM_REVISION_ID                                "item_revision_id"
#define PN4_ATTR_OBJECT_TYPE_ATTRIBUTE                           "object_type"
#define PN4_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define PN4_ATTR_OBJECT_CREATION_DATE                            "creation_date"


/* Class Name */
#define PN4_CLASS_RELEASE_STATUS                                 "releasestatus"
#define PN4_CLASS_WORKSPACEOBJECT                                "WorkspaceObject"
#define PN4_CLASS_FORM                                           "Form"
#define PN4_CLASS_STATUS                                         "ReleaseStatus"
#define PN4_CLASS_ITEMREVISION                                   "ItemRevision"
#define PN4_CLASS_ITEM                                           "Item"
#define PN4_CLASS_DATASET                                        "Dataset"


/*  Wildcard  */
#define PN4_WILDCARD_STAR                                        "*"

/*  Reference Type */
#define PN4_FOLDER                                               "Folder"
#define PN4_NEWSTUFF_FOLDER                                      "Newstuff Folder"
#define PN4_ENVELOPE                                             "Envelope"

/* variable Separators */
#define PN4_NAME_SEPARATOR_UNDERSCORE                            "_"
#define PN4_NAME_SEPARATOR_UNDERSCORE_CHAR                       '_'
#define PN4_NAME_SEPARATOR_DASH                                  "-"
#define PN4_NAME_SEPARATOR_DASH_CHAR                             '-'
#define PN4_NAME_SEPARATOR_HASH_CHAR                             '#'
#define PN4_NAME_SEPARATOR_HASH_STRING                           "#"
#define PN4_VALUE_SEPARATOR_SEMICOLON_CHAR                       ';'
#define PN4_VALUE_SEPARATOR_SEMICOLON                            ";"
#define PN4_VALUE_SEPARATOR_COMMA                                ","
#define PN4_VALUE_SEPARATOR_COLON                                ":"
#define PN4_VALUE_SEPARATOR_FORWARD_SLASH                        "/"
#define PN4_VALUE_SEPARATOR_TILDE                                "~"
#define PN4_VALUE_SEPARATOR_ARROW                                "-->"

#define PN4_MAX_LINE_LEN                                         256
#define PN4_NUMERIC_ERROR_LENGTH                                 12

