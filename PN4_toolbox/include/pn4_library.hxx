/*=========================================================================================================================================
                Copyright 2012 Prion
                Unpublished - All rights reserved
===========================================================================================================================================

File Description:

    Filename: pn4_library.hxx

    Description:  Header File containing declarations of function spread across all pn4_<UTILITY NAME>_utilities.cxx 
                  For e.g. pn4_lib_dataset_utilities.cxx/pn4_lib_form_utilities.cxx

===========================================================================================================================================

Date          Name                Description of Change
-------------------------------------------------------------------------------------------------------------------------------------------
26-Nov-12    Tarun Kumar Singh   Initial Release

===========================================================================================================================================*/
#ifndef PN4_LIBRARY_HXX
#define PN4_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me.h>
#include <form.h>
#include <tc/tc_util.h>
#include <pn4_const.hxx>
#include <pn4_itk_errors.hxx>
#include <pn4_errors.hxx>
#include <pn4_itk_mem_free.hxx>
#include <pn4_exports.hxx>


/*        Utility Functions For Error Logging        */
int PN4_err_write_to_logfile
(
    char*       pszErrorFunction,
    int         iErrorNumber,
    char*       pszFileName,
    int         iLineNumber
);

int PN4_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
);


#include <pn4_undef.hxx>
#endif




