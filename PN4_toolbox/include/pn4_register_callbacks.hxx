/*======================================================================================================================================
                Copyright 2012 Prion
                Unpublished - All rights reserved
========================================================================================================================================

File Description:

    Filename: PN4_register_callbacks.hxx

    Description:  Header File for PN4_register_callbacks.cxx

========================================================================================================================================

Date          Name                Description of Change
----------------------------------------------------------------------------------------------------------------------------------------
26-Nov-12     Tarun Kumar Singh   Initial Release

========================================================================================================================================*/
#ifndef PN4_REGISTER_CALLBACKS_HXX
#define PN4_REGISTER_CALLBACKS_HXX
    
#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <itk/bmf.h>


extern DLLAPI int PN4_toolbox_register_callbacks(void);

extern DLLAPI int PN4_register_custom_handlers(int *decision, va_list args);

#endif
