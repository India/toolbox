/*======================================================================================================================================
                Copyright 2012 Prion
                Unpublished - All rights reserved
========================================================================================================================================

File Description:

    Filename: pn4_errors.hxx

    Description:  Header File for containing macros for custom errors

========================================================================================================================================

Date          Name                Description of Change
----------------------------------------------------------------------------------------------------------------------------------------
23-Nov-12     Tarun Kumar Singh   Initial Release

========================================================================================================================================*/

#ifndef PN4_ERRORS_HXX
#define PN4_ERRORS_HXX

#include <tc/emh_const.h>


#define ERROR_ALLOCATING_MEMORY                                     (EMH_USER_error_base + 400)
#define ERROR_CODE_NOT_STORED_ON_STACK                              (EMH_USER_error_base + 401)



#endif




