/*=========================================================================================================================================
                Copyright 2012 Prion
                Unpublished - All rights reserved
===========================================================================================================================================

File Description:

    Filename: PN4_itk_errors.hxx

    Description:  This file contains macros for handling ITK errors, macros for general errors, different Trace macros, and a function
                  TextEMHErrors which encapsulates EMH_store_error, EMH_store_error_s1...

===========================================================================================================================================

Date          Name                Description of Change
-------------------------------------------------------------------------------------------------------------------------------------------
26-Nov-12    Tarun Kumar Singh   Initial Release

===========================================================================================================================================*/
#ifndef PN4_ITK_ERRORS_HXX
#define PN4_ITK_ERRORS_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <pn4_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef PN4_ITKCALL
#undef PN4_ITKCALL
#endif

// This macro works for functions which return integer. The function
// return value is an input argument and return value in the same time.
// f is either ITK function, or a function which uses only ITK calls
// and therefore has to return ITK integer return code

#define PN4_ITKCALL(iErrorCode,f) {\
           if(iErrorCode == ITK_ok) {\
               (iErrorCode) = (f);\
               PN4_err_start_debug(#f, iErrorCode,__FILE__, __LINE__);\
               if(iErrorCode != ITK_ok) {\
                PN4_err_write_to_logfile(#f, iErrorCode,__FILE__, __LINE__);\
                }\
             }\
          }

#endif




