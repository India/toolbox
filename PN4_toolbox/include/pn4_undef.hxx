/*=========================================================================================================================================
                Copyright 2012 Prion
                Unpublished - All rights reserved
===========================================================================================================================================

File Description:

    Filename: pn4_errors.hxx

    Description:  Header File for containing macros for custom errors

===========================================================================================================================================

Date          Name                Description of Change
-------------------------------------------------------------------------------------------------------------------------------------------
23-Nov-12     Tarun Kumar Singh   Initial Release

===========================================================================================================================================*/


#include <common/library_indicators.h>


#if !defined(EXPORTLIBRARY)
#   error EXPORTLIBRARY is not defined
#endif

#undef EXPORTLIBRARY

#if !defined(IPLIB)
#   error IPLIB is not defined
#endif

#undef PN4_TOOLBOX_API
#undef PN4_TOOLBOXEXPORT
#undef PN4_TOOLBOXGLOBAL
#undef PN4_TOOLBOXPRIVATE

