/*=============================================================================
                Copyright 2010  Northrop Grumman Shipbuilding - Newport News
                Unpublished - All rights reserved
===============================================================================

File Description:

    Filename: PN4_lib_itk_mem_free.hxx

    Description:  This file contains macros for freeing portion of memory 
                  allocated with TCe 'MEM_' functions and by TCe itself.

===============================================================================

Date          Name                Description of Change
-------------------------------------------------------------------------------
18-Aug-10    Tarun Kumar Singh   Initial Release

==============================================================================*/
#ifndef PN4_LIB_ITK_MEM_FREE_HXX
#define PN4_LIB_ITK_MEM_FREE_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <PN4_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef PN4_MEM_TCFREE
#undef PN4_MEM_TCFREE
#endif

// Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc

#define PN4_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }

#endif








