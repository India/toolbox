/*=========================================================================================================================================
                Copyright (c) 2012 Prion
                Unpublished - All rights reserved
===========================================================================================================================================
File Description:

    Filename: pn4_xml_parser.hxx

    Description: This Header file contains all the header files that are 
	             required for parsing the XML files.
===========================================================================================================================================

Date          Name                Description of Change
-------------------------------------------------------------------------------------------------------------------------------------------
28-Nov-11     Tarun Kumar Singh   Intial Creation
===========================================================================================================================================*/

#ifndef PN4_XML_PARSER_HXX
#define PN4_XML_PARSER_HXX
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <time.h>
#include <unidefs.h>
#include <map>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <tccore/tctype.h>
#include <tcinit/tcinit.h>
#include <tccore/aom.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <ps/ps.h>
#include <pom/enq/enq.h>
#include <bom/bom.h>
#include <cfm/cfm_item.h>
#include <me/me.h>
#include <fclasses/tc_string.h>
#include <user_exits/epm_toolkit_utils.h>
#include <form.h>
#include <tc/preferences.h>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>   /* The header file <dom/DOM.hpp> includes all the individual headers for the DOM API classes. */
 
using namespace std;

using namespace xercesc;

typedef map<string, vector<string>> AttrNvaluesMap;
int PN4_get_file_size
(
    char* pszfile
);

char* PN4_get_element_data
(
    DOMElement  *elementId,
    char        *elementName
);

int PN4_run_xml_parser
(    
    char*  pszxmlfile,
    map<string, vector<string>> &mAttrData
);


#endif