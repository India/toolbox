/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_library.hxx

    Description:  Header File containing declarations of function spread across all LM5_lib_<UTILITY NAME>_utilities.cxx 
                  For e.g. lm5_lib_dataset_utilities.cxx/lm5_lib_form_utilities.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/
#ifndef LM5_LIB_LIBRARY_HXX
#define LM5_LIB_LIBRARY_HXX

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unidefs.h>
/* Teamcenter OOTB Header Files */
#include <ae/dataset_msg.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <bom/bom.h>
#include <cfm/cfm.h>
#include <epm/epm.h>
#include <epm/epm_task_template_itk.h>
#include <form/form.h>
#include <fclasses/tc_string.h>
#include <fclasses/tc_date.h>
#include <pom/pom/pom.h>
#include <me/me.h>
#include <ps/ps.h>
#include <pom/enq/enq.h>
#include <res/res_itk.h>
#include <sa/am.h>
#include <sa/user.h>
#include <sa/role.h>
#include <tc/iman.h>
#include <tc/folder.h>
#include <tc/preferences.h>
#include <tc/tc_util.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <tccore/item_msg.h>
#include <tccore/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <tccore/project.h>
#include <tcinit/tcinit.h>
/* Custom Header Files */
#include <lm5_lib_const.hxx>
#include <lm5_lib_itk_errors.hxx>
#include <lm5_lib_itk_mem_free.hxx>
#include <lm5_lib_string_macros.hxx>

struct sStringArray
{
    int    iArrayLen;
    char** ppszValue;
};

/*        Utility Functions For Item/Item Revision        */
int LM5_itemrev_get_previous_rev
(
    tag_t     tItemRev,
    tag_t*    ptPreviousItemRev,
    logical*  pbIsOnlyRevision,
    logical*  pbIsFirstRevision
);

int LM5_itemrev_get_all_previous_rev
(
    tag_t     tItemRev,
    int*      piPrevIRCnt,
    tag_t**   pptPreviousIR,
    logical*  pbIsOnlyRevision,
    logical*  pbIsFirstRevision
);

int LM5_itemrev_construct_name
(
    tag_t   tItemRev,
    char**  ppszName
);

/*        Utility Functions For Folder        */
int LM5_get_user_folder
(
    char*    pszUserFLType,
    tag_t*   ptFolder
);

int LM5_create_custom_folder
(
    char*    pszClassName,
    char*    pszFolderName,
    char*    pszFolderDesc,
    int      iAttrCount,
    char**   ppszAttrNames,
    char**   pszAttrValues,
    tag_t*   ptFolder
);

int LM5_insert_multiple_content_to_folder
(
    tag_t  tFolder,
    int    iObjCnt,
    tag_t* ptObjects,
    int    iPosition
);

/*        Utility Functions For GRM        */
int LM5_grm_get_secondary_obj_and_rel_info
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    char*   pszSecondaryObjType,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount,
    tag_t** pptRelation
);

int LM5_grm_create_relation
(
    tag_t   tPrimaryObj,
    tag_t   tSecondaryObj,
    char*   pszRelationType,
    tag_t*  ptRelation
);

int LM5_grm_get_related_obj
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    char*    pszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary,
    tag_t**  pptValidRelatedObj,
    int*     piRelatedObjCnt
);

int LM5_grm_get_sec_obj_attach_with_rel_type
(
    tag_t   tPrimaryObj,
    int     iRelCount,
    char**  ppszRelTypeList,
    int     iSecObjCnt,
    char**  ppszSecObjTypeList,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
);

int LM5_grm_load_and_get_related_obj
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    char*    pszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary,
    tag_t**  pptValidRelatedObj,
    int*     piRelatedObjCnt
);

/*        Utility Functions For Objects        */
int LM5_obj_ask_status_list
(
    tag_t       tObject,
    char***     pppszStatusNameList,
    int*        piStatusCount,
    tag_t**     pptStatusTagList
);

int LM5_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
);

int LM5_obj_ask_type_display_name
(
    char*   pszType,
    char**  ppszTypeDisplayName
);

int LM5_obj_ask_parent_class
(
    tag_t       tObject,
    char**      ppszObjParentTypeName
);

int LM5_obj_save_and_unlock
(
    tag_t       tObject
);

int LM5_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
);

int LM5_obj_get_owning_user
(
    tag_t   tObject,
    char**  ppszAttrValue,
    tag_t*  ptOwningUser
);

int LM5_obj_set_tag_value
(
    tag_t tObject,
    char* pszAttribute,
    tag_t tObjValue
);

int LM5_obj_set_logical_value
(
    tag_t       tObject,
    char*       pszPropertyName,
    logical     bAttrVal
);

int LM5_obj_ask_status_tag
(
    tag_t      tObject,
    char*      pszStatusName,
    tag_t*     ptStatusTag
);

int LM5_obj_is_any_input_status_attached
(
    tag_t      tObject,
    int        iValidStatus,
    char**     ppszStatusName,
    tag_t*     ptStatus,
    logical*   pbFound
);

int LM5_obj_is_any_input_status_attached_info
(
    tag_t      tObject,
    int        iValidStatus,
    char**     ppszStatusName,
    tag_t*     ptStatus,
    logical*   pbStatusAttached
);

int LM5_create_folder
( 
    const char* folder_name, 
    const char* folder_type,
    tag_t *folder_tag
);

int LM5_initiate_workflow
(
    char* pszWorkflowTemplate,
    tag_t tTargetObject
);

int LM5_obj_pom_set_multiple_str_attr_without_saving
(
    tag_t   tObject,
    char*   pszClassName,
    int     iAttrCount,
    char**  pszAttrNames,
    char**  pszAttrValues
);

int LM5_obj_is_type_of
(
    tag_t    tObject,
    char*    pszTypeName,
    logical* pbBelongsToInputClass 
);

int LM5_check_privilege
(
    tag_t    tObject,
    logical* pbWriteAccessDenied,
    logical* pbObjCheckedOut,
    logical* pbModifiable
);

int LM5_obj_assign_project
(
    int    iObjCnt,
    tag_t* ptObjects,
    int    iProjCnt,
    tag_t* ptProjects
);

int LM5_obj_load_related
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    char*    pszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary
);

int LM5_obj_set_uom
(
    tag_t tObject,
    tag_t tUOM
);

/*        Utility Functions For Error Logging        */
int LM5_err_write_to_logfile
(
    char* pszErrorFunction,
    int   iErrorNumber,
    char* pszFileName,
    int   iLineNumber
);

int LM5_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
);



/*        Utility Functions For Date & Time        */
int LM5_get_date
(
    date_t *dReleaseDate
);

int LM5_dt_get_current_date_time
(
    const char* pszFormat,
    char        szDateTime[IMF_filename_size_c + 1]
);


/*        Argument Utility Functions        */
int LM5_arg_get_attached_obj_of_input_type_list
(
    tag_t   task,
    int     iAttachmentType,
    char**  ppszObjTypeList,
    int     iTypeCnt,
    int*    piCount,
    tag_t** pptObject
);

int LM5_arg_get_attached_objects_of_type
(
    tag_t   task,
    int     iAttachmentType,
    char*   pszAttachedObjType,
    int*    piCount,
    tag_t** pptObject
);

int LM5_arg_get_arguments
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    const char*         pszArgument,
    char*               pszSeparator,
    logical             bOptional,
    int*                piValCount,
    char***             pppszArgValue,
    char**              ppszOriginalArgVal
);

int LM5_arg_get_wf_argument_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    const char*         pszArgument,
    logical             bOptional,
    char**              ppszArgValue
);

int LM5_arg_get_wf_argument_info_and_value
(
    TC_argument_list_t* pArgList,
    tag_t               tTask,
    const char*         pszArgument,
    logical             bOptional,
    logical*            pbFoundArg,
    char**              ppszArgValue
);

/*        String Utility Functions        */
int LM5_str_parse_string
(
    const char*  pszList,
    char*        pszSeparator,
    int*         iCount,
    char***      pppszValuelist
);

int LM5_str_check_for_word_wrap
(
    int      iMaxStrLenPerLine,
    char*    pszPropStrVal,
    logical* pbChanged,
    char**   ppszNewVal
);

int LM5_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
);

int LM5_contains_string_value
(
    char*  pszStrVal,
    int    iTotalStrVals,
    char** pppszStrVals
);

int LM5_parse_str_array
(
    int      iListLen, 
    char**   ppszStrArrayList,
    char*    pszSeparator,
    int*     piStructSize,
    sStringArray** ppsStringInfoList
);

/*        Utility Functions For Preferences        */
int LM5_pref_get_string_value
(
    char*   pszPrefName, 
    char**  ppszPrefValue
);

int LM5_pref_get_string_values
(
    char*   pszPrefName, 
    int*    iPrefValCnt, 
    char*** pppszPrefVals
);
    
int LM5_pref_get_integer_value
(
    char*   pszPrefName, 
    int*    piPrefValue
);

#endif




