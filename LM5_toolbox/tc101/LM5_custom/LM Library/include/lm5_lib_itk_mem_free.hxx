/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_itk_mem_free.hxx

    Description:  This file contains macros for freeing portion of memory allocated with TCe 'MEM_' functions and by TCe itself.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/
#ifndef LM5_LIB_ITK_MEM_FREE_HXX
#define LM5_LIB_ITK_MEM_FREE_HXX

//#include <base_utils/Mem.h>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef LM5_MEM_TCFREE
#undef LM5_MEM_TCFREE
#endif

// Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc

#define LM5_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }



#ifdef LM5_ALLOCATE_TAG_MEM
#undef LM5_ALLOCATE_TAG_MEM
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation if done for the very first time
*/
#define LM5_ALLOCATE_TAG_MEM(iRetCode, ptMem, iSize){\
            if(iSize > 0){\
                   if (ptMem == NULL || iSize == 1){\
                        ptMem = (tag_t*) MEM_alloc((int)(sizeof(tag_t)*iSize));}\
                   else{\
                        ptMem = (tag_t*) MEM_realloc(ptMem, (int)((sizeof(tag_t)*iSize)));}\
            }\
            else{\
                iRetCode = INVALID_INPUT_TO_FUNCTION;\
                EMH_store_error(EMH_severity_error, iRetCode);}\
}

#ifdef LM5_UPDATE_TAG_ARRAY
#undef LM5_UPDATE_TAG_ARRAY
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation if done for the very first time
*/
#define LM5_UPDATE_TAG_ARRAY(iCurrentArrayLen, ptArray, tValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ptArray = (tag_t*) MEM_alloc(iCurrentArrayLen * sizeof(tag_t));\
            }\
            else{\
                ptArray = (tag_t*) MEM_realloc(ptArray, iCurrentArrayLen * sizeof(tag_t));\
            }\
            ptArray[iCurrentArrayLen-1]=  tValue;\
}


#ifdef LM5_ALLOCATE_INT_MEM
#undef LM5_ALLOCATE_INT_MEM
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation if done for the very first time
*/
#define LM5_ALLOCATE_INT_MEM(iRetCode, piMem, iSize){\
            if(iSize > 0){\
                   if (piMem == NULL || iSize == 1){\
                        piMem = (int*) MEM_alloc((int)(sizeof(int)*iSize));}\
                   else{\
                        piMem = (int*) MEM_realloc(piMem, (int)((sizeof(int)*iSize)));}\
            }\
            else{\
                iRetCode = INVALID_INPUT_TO_FUNCTION;\
                EMH_store_error(EMH_severity_error, iRetCode);}\
}


#ifdef LM5_TCFREE_STRING_INFO_STRUCT
#undef LM5_TCFREE_STRING_INFO_STRUCT
#endif
/* This macro is used to free the memory of structure. */
#define LM5_TCFREE_STRING_INFO_STRUCT(psStringInfoList, iStructSize) {\
          for(int iDx = 0; iDx < iStructSize; iDx++) {\
                for(int iFx = 0; iFx < (psStringInfoList[iDx].iArrayLen); iFx++){\
                        LM5_MEM_TCFREE(psStringInfoList[iDx].ppszValue[iFx]);\
                }\
          }\
          LM5_MEM_TCFREE(psStringInfoList);\
}


#ifdef LM5_TCFREE_PREF_REL_INFO_STRUCT
#undef LM5_TCFREE_PREF_REL_INFO_STRUCT
#endif
/* This macro is used to free the memory of structure. */
#define LM5_TCFREE_PREF_REL_INFO_STRUCT(psStringPrefValInfo, iPrefInfoCnt) {\
          for(int iDx = 0; iDx < iPrefInfoCnt; iDx++) {\
            LM5_MEM_TCFREE(psStringPrefValInfo[iDx].pszPrimaryType);\
            LM5_MEM_TCFREE(psStringPrefValInfo[iDx].pszRelationType);\
            LM5_MEM_TCFREE(psStringPrefValInfo[iDx].pszSecondaryType);\
          }\
          LM5_MEM_TCFREE(psStringPrefValInfo);\
}

#endif








