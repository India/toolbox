/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_const.hxx

    Description:  Header File for constants used during scope of the DLL.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LM5_LIB_CONST_HXX
#define LM5_LIB_CONST_HXX


/* Date Time Format */
#define LM5_DATE_TIME_FORMAT                                     "%d.%m.%Y"

/* Prefixes */

/* Preferences */
#define LM5_PREF_SMAPROJECT_REL_PROPAGATION                      "LM5_Project_Relation_Propagation_Rules"
#define LM5_PREF_FL_CONTENT_PROPAGATE_OBJ                        "LM5_Folder_Content_Propagate_Objects"
#define LM5_PREF_PROPAGATION_INFORMATION                         "LM5_Propagation_Information"

/* Relation    */
#define LM5_IMAN_SPECIFICATION_RELATION                          "IMAN_specification"
#define LM5_IMAN_BASED_ON                                        "IMAN_based_on"
#define LM5_IMAN_MASTER_FORM                                     "IMAN_master_form"
#define LM5_IMAN_REFERENCE                                       "IMAN_reference"

/* Status Names*/
#define LM5_STATUS_RELEASE                                       "LM5_Released"


/* Propagation Information */
#define LM5_CONST_PROJECT                                        "Project"
#define LM5_CONST_LICENSE                                        "License"
#define LM5_CONST_IP_CLASSIFICATION                              "IP Classification"

/* Attributes  */
#define LM5_ATTR_DATE_RELEASED                                   "date_released"
#define LM5_ATTR_RELEASE_STATUS_LIST                             "release_status_list"
#define LM5_ATTR_RELEASE_STATUS_NAME                             "name"
#define LM5_ATTR_OBJECT_NAME_ATTRIBUTE                           "object_name"
#define LM5_ATTR_DESCRIPTION_ATTRIBUTE                           "object_desc"
#define LM5_ATTR_ITEM_ID                                         "item_id"
#define LM5_ATTR_ITEM_REVISION_ID                                "item_revision_id"
#define LM5_ATTR_OBJECT_TYPE_ATTRIBUTE                           "object_type"
#define LM5_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define LM5_ATTR_OBJECT_CREATION_DATE                            "creation_date"
#define LM5_ATTR_PROJ_LIST                                       "project_list"
#define LM5_ATTR_LICENSE_LIST                                    "license_list"
#define LM5_ATTR_IP_CLASSIFICATION                               "ip_classification"
#define LM5_ATTR_PROCESS_STAGE_LIST                              "process_stage_list"
#define LM5_ATTR_PSOCC_CHILD_ITEM                                "child_item"
#define LM5_ATTR_PSOCC_PARENT_BVR                                "parent_bvr"
#define LM5_ATTR_USER_DATA_1                                     "user_data_1"
#define LM5_ATTR_SIGNOFF_ATTACHMENTS                             "signoff_attachments"
#define LM5_ATTR_PARENT_PROCESSES                                "parent_processes"
#define LM5_ATTR_ALL_TASKS                                       "all_tasks"
#define LM5_ATTR_ACTIVE_SEQ                                      "active_seq"
#define LM5_ATTR_EPM_TASK_ATTACHMENTS                            "attachments"
#define LM5_ATTR_PRIMARY_OBJ                                     "primaryObjects"
#define LM5_ATTR_SECONDARY_OBJ                                   "secondaryObjects"
#define LM5_ATTR_STATE                                           "state"
#define LM5_ATTR_UOM_TAG                                         "uom_tag"

/* Class Name */
#define LM5_CLASS_RELEASE_STATUS                                 "releasestatus"
#define LM5_CLASS_WORKSPACEOBJECT                                "WorkspaceObject"
#define LM5_CLASS_FORM                                           "Form"
#define LM5_CLASS_STATUS                                         "ReleaseStatus"
#define LM5_CLASS_ITEMREVISION                                   "ItemRevision"
#define LM5_CLASS_ITEM                                           "Item"
#define LM5_CLASS_DATASET                                        "Dataset"
#define LM5_CLASS_EPM_TASK                                       "EPMTask"
#define LM5_CLASS_IMANRELATION                                   "ImanRelation"
#define LM5_CLASS_PSOCCURRENCE                                   "PSOccurrence"
#define LM5_CLASS_EFFECTIVITY                                    "Effectivity"
#define LM5_CLASS_PDF_DATASET                                    "PDF"
#define LM5_CLASS_FOLDER                                         "Folder"
#define LM5_CLASS_ENVELOPE                                       "Envelope"
#define LM5_CLASS_DISPATCHERREQUEST                              "DispatcherRequest"

/* Type Name */
#define LM5_TYPE_NEWSTUFF_FOLDER                                 "Newstuff Folder"

/* BOM View & BOM View Revision Type  */
#define LM5_BOMVIEW_REV_TYPE_VIEW                                "view"

/*  Revision Rule  */
#define LM5_REVISION_RULE_PRECISE_ONLY                           "Precise Only"

/* Workflow Action Handler Names  */
#define LM5_WF_AH_ASSIGN_PROJ                                    "LM5-assign-project"
#define LM5_WF_AH_PROPAGATE_SECURITY_INFO                        "LM5-propagate-security-info"


/* Workflow Rule Handler Names  */
#define LM5_WF_RH_VALIDATE_TARGET_OBJ_OWNER                      "LM5-validate-target-object-owner"

/* Workflow Argument Name  */
#define LM5_WF_ARG_STATUS                                        "status"
#define LM5_WF_ARG_STATUS_ALLOW                                  "status_allow"
#define LM5_WF_ARG_STATUS_DISALLOW                               "status_disallow"
#define LM5_WF_ARG_INCLUDE_TYPES                                 "include_types"
#define LM5_WF_ARG_EXCLUDE_TYPES                                 "exclude_types"
#define LM5_WF_ARG_VAL_TRUE                                      "true"
#define LM5_WF_ARG_PRIORITY                                      "priority"
#define LM5_WF_ARG_RELATION_TYPE                                 "relation_types"
#define LM5_WF_ARG_SERVICE                                       "Service"
#define LM5_WF_ARG_TEXT                                          "text"
#define LM5_WF_ARG_STAMP_PREVIOUS                                "stamp_previous"
#define LM5_WF_ARG_DATASET_REL_TYPE                              "dst_relation_type"
#define LM5_WF_ARG_FOLDER_TYPE                                   "folder_type"
#define LM5_WF_ARG_OBJECT_CLASS                                  "object_class"
#define LM5_WF_ARG_OBJECT_TYPES                                  "object_types"
#define LM5_WF_ARG_OBJ_TYPE                                      "object_type"
#define LM5_WF_ARG_ATTACH_TO                                     "attach_to"
#define LM5_WF_ARG_READ_SIGNOFFS_FROM                            "read_signoffs_from"
#define LM5_WF_ARG_TARGET_TYPE                                   "target_type"
#define LM5_WF_ARG_REL_TYPE                                      "relation_type"
#define LM5_WF_ARG_ATTR_LIST                                     "attr_list"
#define LM5_WF_ARG_FROM_ATT_TYPE                                 "from_att_type"
#define LM5_WF_ARG_TO_ATT_TYPE                                   "to_att_type"
#define LM5_WF_ARG_MOVE_SRC                                      "move_src"
#define LM5_WF_ARG_FOLDER_TYPE                                   "folder_type"
#define LM5_WF_ARG_FOLDER_NAME                                   "folder_name"
#define LM5_WF_ARG_FL_NAME_LIST                                  "folder_names"
#define LM5_WF_ARG_VALIDATE_OBJ_TYPES                            "validate_object_types"
#define LM5_WF_ARG_RELATE_OBJECTS                                "relate_objects"
#define LM5_WF_ARG_CUSTOM_ITEM_ID_FOR_TYPES                      "custom_item_id_for_types"
#define LM5_WF_ARG_DATASET_TYPE                                  "dataset_type"
#define LM5_WF_ARG_TRANS_PROVIDER                                "provider_name"
#define LM5_WF_ARG_TRANS_SERVICE                                 "service_name"
#define LM5_WF_ARG_VALIDATE_OBJECTS                              "validate_objects"
#define LM5_WF_ARG_BASELINE_TEMPLATE                             "baseline_template"
#define LM5_WF_ARG_DESCRIPTION                                   "description"
#define LM5_WF_ARG_FOR_TRANS_REQ                                 "arg"
#define LM5_WF_ARG_LOCATION                                      "location"
#define LM5_WF_ARG_PROPERTY_NAME                                 "prop"
#define LM5_WF_ARG_SIGNOFF_FORMAT                                "signoff_format"
#define LM5_WF_ARG_REVISION_RULE                                 "rev_rule"
#define LM5_WF_ARG_UOM_SYMBOL                                    "uom_symbol"
#define LM5_WF_ARG_UOM_NAME                                      "uom_name"
#define LM5_WF_ARG_ITEM_TYPE                                     "item_type"

/* Workflow Argument Value  */
#define LM5_WF_ARG_VAL_SINGLE                                    "single"
#define LM5_WF_ARG_VAL_ALL                                       "all"
#define LM5_WF_ARG_VAL_CURRENT                                   "current"
#define LM5_WF_ARG_VAL_TOP                                       "top"
#define LM5_WF_ARG_VAL_PREVIOUS                                  "previous"
#define LM5_WF_ARG_VAL_TARGET                                    "target"
#define LM5_WF_ARG_VAL_REFERENCE                                 "reference"
#define LM5_WF_ARG_VAL_REMOVE                                    "remove"
#define LM5_WF_ARG_VAL_USER                                      "user"
#define LM5_WF_ARG_VAL_PERSON                                    "person"

/* Name Of Privileges  */
#define LM5_WRITE_ACCESS                                         "WRITE"

/* Organisation */
#define LM5_GROUP_ADMIN                                          "ADMIN"
#define LM5_ROLE_DATA_ADMIN                                      "Data Administrator"

/* System Administrator  */
#define LM5_APPROVER                                             "Approver"
#define LM5_REVIEWER                                             "Reviewer"

/* Folder Names String */
#define LM5_NEWSTUFF_FOLDER                                      "newstuff"
#define LM5_HOME_FOLDER                                          "home"

/* Separators */
#define LM5_STRING_UNDERSCORE                                    "_"
#define LM5_STRING_COMMA                                         ","
#define LM5_STRING_SEMICOLON                                     ";"
#define LM5_CHAR_SEMICOLON                                       ';'
#define LM5_STRING_FORWARD_SLASH                                 "/"
#define LM5_STRING_SPACE                                         " "
#define LM5_STRING_COLON                                         ":"
#define LM5_STRING_DOT                                           "."

#define LM5_SEPARATOR_LENGTH                                     1
#define LM5_NUMERIC_ERROR_LENGTH                                 12

#endif

