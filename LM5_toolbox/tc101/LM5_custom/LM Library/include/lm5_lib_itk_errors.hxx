/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_itk_errors.hxx

    Description:  This file contains macros for handling ITK errors, macros for general errors, different Trace macros, and a function TextEMHErrors 
                  which encapsulates EMH_store_error, EMH_store_error_s1...

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-13    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/
#ifndef LM5_LIB_ITK_ERRORS_HXX
#define LM5_LIB_ITK_ERRORS_HXX

#include <lm5_lib_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef LM5_ITKCALL
#undef LM5_ITKCALL
#endif

// This macro works for functions which return integer. The function
// return value is an input argument and return value in the same time.
// f is either ITK function, or a function which uses only ITK calls
// and therefore has to return ITK integer return code

#define LM5_ITKCALL(iErrorCode,f) {\
           if(iErrorCode == ITK_ok) {\
               (iErrorCode) = (f);\
               LM5_err_start_debug(#f, iErrorCode,__FILE__, __LINE__);\
               if(iErrorCode != ITK_ok) {\
                LM5_err_write_to_logfile(#f, iErrorCode,__FILE__, __LINE__);\
                }\
             }\
          }

#endif






