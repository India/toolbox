/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_string_utilities.cxx

    Description: This File contains functions for manipulating string. These functions are part of SMA library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Singh          Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_errors.hxx>

/**
 * Parses a string containing values seperated by a given separator
 * into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in]  pszList         Input list to be parsed
 * @param[in]  pszSeparator    Separator 
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by LM5_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 */
int LM5_str_parse_string
(
    const char*  pszList,
    char*        pszSeparator,
    int*         iCount,
    char***      pppszValuelist
)
{
    int    iRetCode     = ITK_ok;
    int    iCounter     = 0;
    char*  pszToken     = NULL;
    char*  pszTempList  = NULL;

     /* Validate input */
    if(pszList == NULL || pszSeparator == NULL)
    {
        TC_write_syslog( "ERROR: Missing input data: List passed or seprator is null\n");
    }
    else
    {
        /* Copying the input string to Null terminated string */
        LM5_CUST_STRCPY(pszTempList, pszList);

        /* Get the first token */
        pszToken = tc_strtok( pszTempList, pszSeparator );

        *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
        /* Tokenize the input string */
        while( pszToken != NULL )
        {
            /* Copy the token to the output string pointer */
            LM5_CUST_STRCPY((*pppszValuelist)[iCounter], pszToken);

            /* Count the number of elements in the string */
            iCounter++;
                
            /* Allocating memory to pointer of string (output) as per the number of tokens found */
            *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));

            /* Get next token: */
            pszToken = tc_strtok( NULL, pszSeparator );
        }

        (*iCount) = iCounter;
    }

    /* Free the temporary memory used for tokenizing */
    LM5_MEM_TCFREE(pszTempList);
    return iRetCode;
}

/**
 * This function word wrap (insert '\n' to) the input string after every fixed number of characters. Maximum number 
 * of character per line is specified by input argument 'iMaxStrLenPerLine'. If character at the position immediate 
 * after maximum number of characters per line happens to be a character from a word then the whole word is moved to
 * next line. Exception to this process is that if a single word is long enough to exceed maximum number of characters
 * as specified by input argument 'iMaxStrLenPerLine' then the word is not moved to next line. In this case '\n' is 
 * inserted after number of characters as specified by 'iMaxStrLenPerLine'.
 *
 * @param[in]  iMaxStrLenPerLine   Number of characters allowed per line
 * @param[in]  pszPropStrVal       String to be word wrap
 * @param[in]  pbChanged           'false' if input string is changed else 'true'
 * @param[out] ppszNewVal          Modified string.
 *
 * @retval ITK_ok on successful execution
 */
int LM5_str_check_for_word_wrap
(
    int      iMaxStrLenPerLine,
    char*    pszPropStrVal,
    logical* pbChanged,
    char**   ppszNewVal
)
{
    int   iRetCode       = ITK_ok;
    int   iLen           =  0;
    char* pszTempNewStr  = NULL;

    iLen  = (int)tc_strlen(pszPropStrVal);

    if(iLen > iMaxStrLenPerLine)
    {
        int  iCharCount        = 0;
        int  iLastSpaceLoc     = 0;
        int  iNewStrLen        = 0;
        int  iLocOfNewLineChar = 0;

        iLocOfNewLineChar = iMaxStrLenPerLine +1;

        pszTempNewStr = (char*) MEM_alloc( sizeof(char)*(iLen + 1));

        for(int iDx = 0; iDx < iLen; iDx++)
        {
            iCharCount++;

            if(iNewStrLen < iLen)
            {
                iNewStrLen++;
            }
            else
            {
                iNewStrLen++;
                pszTempNewStr = (char*) MEM_realloc(pszTempNewStr, (sizeof(char)*(iNewStrLen+1)));
            }

            if(pszPropStrVal[iDx] == ' ')
            {
                iLastSpaceLoc = iDx+1;
            }

            if( iCharCount == iLocOfNewLineChar )
            {
                if(pszPropStrVal[iDx] == '\n')
                {
                    iCharCount = 0; 
                    pszTempNewStr[iNewStrLen-1] = pszPropStrVal[iDx];
                    pszTempNewStr[iNewStrLen] = '\0';
                }
                else
                {
                    if(pszPropStrVal[iDx] == ' ')
                    {
                        pszTempNewStr[iNewStrLen-1] = '\n';

                        if(iNewStrLen < iLen)
                        {
                            iNewStrLen++;
                        }
                        else
                        {
                            iNewStrLen++;
                            pszTempNewStr = (char*) MEM_realloc(pszTempNewStr, (sizeof(char)*(iNewStrLen+1)));
                        }

                        pszTempNewStr[iNewStrLen-1] = pszPropStrVal[iDx];
                        pszTempNewStr[iNewStrLen] = '\0';
                        iCharCount = 1;;
                    }
                    else
                    {    /* To Check */
                        if(iLastSpaceLoc > (iDx+1 -iLocOfNewLineChar))
                        {
                            int   iSubStrLen = 0;
                            char* pszSubStr  = NULL;

                            LM5_ITKCALL(iRetCode, LM5_str_copy_substring(pszPropStrVal, (iLastSpaceLoc+1), (iDx+1), &pszSubStr));
                            
                            iSubStrLen = (int)tc_strlen(pszSubStr);
                            pszTempNewStr[iNewStrLen-iSubStrLen] = '\n';
                            iCharCount = iSubStrLen;

                            if(iNewStrLen < iLen)
                            {
                                iNewStrLen++;
                            }
                            else
                            {
                                iNewStrLen++;
                                pszTempNewStr = (char*) MEM_realloc(pszTempNewStr, (sizeof(char)*(iNewStrLen+1)));
                            }

                            for(int iJx = 0; iJx < iSubStrLen; iJx++)
                            {
                                pszTempNewStr[iNewStrLen-(iSubStrLen-iJx)]= pszSubStr[iJx];
                            }

                            pszTempNewStr[iNewStrLen] = '\0';
                            LM5_MEM_TCFREE(pszSubStr);
                        }
                        else
                        {
                            pszTempNewStr[iNewStrLen-1] = '\n';
                            if(iNewStrLen < iLen)
                            {
                                iNewStrLen++;
                            }
                            else
                            {
                                iNewStrLen++;
                                pszTempNewStr = (char*) MEM_realloc(pszTempNewStr, (sizeof(char)*(iNewStrLen+1)));
                            }

                            pszTempNewStr[iNewStrLen-1] = pszPropStrVal[iDx];
                            pszTempNewStr[iNewStrLen] = '\0';
                            iCharCount = 1;
                        }
                    }
                }
            }
            else
            {
                if(pszPropStrVal[iDx] == '\n')
                {
                    iCharCount = 0;
                }

                pszTempNewStr[iNewStrLen-1] = pszPropStrVal[iDx];
                pszTempNewStr[iNewStrLen] = '\0';
            }
        }
    }

    if(tc_strcmp(pszTempNewStr, pszPropStrVal) == 0 || iLen <= iMaxStrLenPerLine)
    {
        (*pbChanged) = false;
    }
    else
    {
        (*ppszNewVal) = (char*) MEM_alloc((int)(sizeof(char)* (tc_strlen(pszTempNewStr)+1)));
        tc_strcpy((*ppszNewVal), pszTempNewStr);
        (*pbChanged) = true;
    }

    LM5_MEM_TCFREE(pszTempNewStr);
    return iRetCode;
}

/**
 * This function returns the substring when provided with start and end position on the 
 * the input string. Substring is the character sequence that starts at character position 
 * 'iStrStartPosition' and ends at character position 'iStrEndPosition'
 *
 * For example If 'pszInputString' = 'Application'
 *                'iStrStartPosition' = 4
 *                'iStrEndPosition'   = 9
 *             then Output Will be: 'licati'
 *
 * Function will copy the characters starting 4th character to the 9th character of
 * input string.
 * 
 * @param[in]  pszInputString    Actual input string
 * @param[in]  iStrStartPosition Position of a character in the current string to be
 *                               used as starting character for the substring.
 * @param[in]  iStrEndPosition   Position of a character in the current string to be
 *                               used as last character for the substring.
 *
 * @param[out] ppszCopyedStr     Copied string.
 *
 * @retval INVALID_INPUT_TO_FUNCTION if input not correct
 * @retval ITK_ok on successful execution
 */
int LM5_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
)
{
    int  iRetCode    = ITK_ok;
    int  iStrLen     = 0;

    /* Initializing the Out Parameter to this function. */
    (*ppszCopyedStr) = NULL;

    iStrLen = (int) tc_strlen(pszInputString);
    
    if ((pszInputString != NULL) && (iStrStartPosition >= 0) && (iStrEndPosition <=  iStrLen) && 
        (iStrStartPosition <= iStrEndPosition))
    {
        if(tc_strcmp(pszInputString, "") != 0)
        {
            int idx        = 0;
            int iNewStrLen = 0;

            iNewStrLen = iStrEndPosition - iStrStartPosition + 2;

            (*ppszCopyedStr) = (char*) MEM_alloc(sizeof(char*) * iNewStrLen);

            while(iStrStartPosition <= iStrEndPosition)
            {
                (*ppszCopyedStr)[idx] = pszInputString[iStrStartPosition-1];
                idx++;
                iStrStartPosition++;
            }

            (*ppszCopyedStr)[idx] = '\0';
        }
    }
    else
    {
        iRetCode = INVALID_INPUT_TO_FUNCTION;
    }

    return iRetCode;
}


/**
 * Function checks if given string exists in string of array or not 
 *
 * @param[in]  pszStrVal     String to be looked into string array
 * @param[in]  iTotalStrVals String Array Size 
 * @param[in]  pppszStrVals  String Array
 *
 * @return ITK_ok if string is found in the array else return 1
 */
int LM5_contains_string_value
(
    char*  pszStrVal, 
    int    iTotalStrVals, 
    char** pppszStrVals)
{
    int iRetCode (ITK_ok);

    if( iTotalStrVals > 0 )
    {
         for ( int inx = 0; inx < iTotalStrVals; inx++ )
         {
             if(tc_strcmp( pszStrVal, pppszStrVals[ inx ]) == 0)
             {
                return iRetCode;
             }
         } 
        
    }

    return 1;
}

/**
 * This function receives the array of strings. Function parses every string values separated by a given separator and
 * construct an array of structure containing multiple array of string values.
 *
 * @param[in]  iListLen          Length of String Array
 * @param[in]  ppszStrArrayList  Input array of string to be parsed
 * @param[in]  pszSeparator      Separator
 * @param[out] piStructSize      Size of Structure Array
 * @param[out] ppsStringInfoList Array of custom defined structure
 * @return 
 */
int LM5_parse_str_array
(
    int      iListLen, 
    char**   ppszStrArrayList,
    char*    pszSeparator,
    int*     piStructSize,
    sStringArray** ppsStringInfoList
)
{
    int iRetCode   = ITK_ok;

    /* Validate input */
    if((pszSeparator == NULL) || (tc_strlen(pszSeparator) == 0))
    {
        TC_write_syslog( "ERROR: Missing input data:List passed or seprator is null\n");
    }
    else
    {
        for(int iDx = 0; iDx < iListLen && iRetCode == ITK_ok; iDx++)
        {
            int   iCounter    = 0;
            char* pszToken  = NULL; /* Do not free the memory of this variable. */

            if(iDx == 0)
            {
                (*piStructSize) = iListLen;
                (*ppsStringInfoList) = (sStringArray*) MEM_alloc(sizeof(sStringArray) * iListLen);
                /* Initializing the data members of structure */
                for(int iCx = 0; iCx < iListLen; iCx++)
                {
                    (*ppsStringInfoList)[iCx].iArrayLen = 0;
                    (*ppsStringInfoList)[iCx].ppszValue = NULL; 
                }
            }

            /* Get the first token */
            pszToken = tc_strtok( ppszStrArrayList[iDx], pszSeparator );

            /* Tokenize the input string */
            while( pszToken != NULL )
            {
                /* Count the number of elements in the string */
                iCounter++;

                if(iCounter == 1)
                {
                    (*ppsStringInfoList)[iDx].ppszValue = (char**) MEM_alloc(sizeof(char*)*iCounter);
                }
                else
                {
                    (*ppsStringInfoList)[iDx].ppszValue = (char**) MEM_realloc(((*ppsStringInfoList)[iDx].ppszValue), sizeof(char*)*iCounter);
                }

                LM5_CUST_STRCPY(((*ppsStringInfoList)[iDx].ppszValue[iCounter-1]), pszToken);

                /* Get next token: */
                pszToken = tc_strtok( NULL, pszSeparator );
            }
      
            (*ppsStringInfoList)[iDx].iArrayLen = iCounter;
        }   
    }

    return iRetCode;
}



#ifdef __cplusplus
}
#endif



