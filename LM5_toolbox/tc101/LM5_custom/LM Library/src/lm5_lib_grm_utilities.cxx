/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_grm_utilities.cxx

    Description:  This File contains custom functions for GRM to perform generic operations. These functions are part of LM5 library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh    Initial Release
========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_errors.hxx>

/**
 * This function returns a list and count of all relation tags and the secondary objects with a specified
 * relation for specified secondary object Type to the specified primary_object.
 *
 * e.g. Suppose 'P_Katalogteil' is a type of 'Item'. Then object type for 'P_Katalogteil'
 *      is 'P_Katalogteil'
 *
 * @note This function returns the value depending on Secondary Object Type as well as tags of the relation
 *       object that binds secondary objects to primary objects
 * 
 * @param[in] tPrimaryObj              The object.
 * @param[in] pszRelationTypeName      Name of relation type. Can be NULL.
 * @param[in] pszSecondaryObjType      Type of the primary object.
 * @param[out] pptSecondaryObjects     List of Primary object of specified type.
 * @param[out] piObjectCount           Primary Object count.
 * @param[out] pptRelation             List of tags of the relation object that binds primary and
 *                                     secondary objects
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LM5_grm_get_secondary_obj_and_rel_info
(
    tag_t   tPrimaryObj,
    char*   pszRelationTypeName,
    char*   pszSecondaryObjType,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount,
    tag_t** pptRelation
)
{
    int     iRetCode                   = ITK_ok;
    int     iCount                     = 0;
    int     iIterator                  = 0;
    int     iSecondaryCount            = 0;
    tag_t   tRelationType              = NULLTAG;
    char*   pszObjectType              = NULL;
    GRM_relation_t *ptSecondaryObjs    = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount) = 0;
    (*pptSecondaryObjects) = NULL;

    /* get the relation type tag e.g. "IMAN_specification"  */
    LM5_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName,
                                                   &tRelationType)); 
    
    LM5_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, tRelationType, 
                                                       &iSecondaryCount, &ptSecondaryObjs));
                                       
    for(iIterator = 0; iIterator < iSecondaryCount && iRetCode == ITK_ok; iIterator++)
    {        
        LM5_ITKCALL(iRetCode, LM5_obj_ask_type(ptSecondaryObjs[iIterator].secondary,
                                                  &pszObjectType));
            
        if (tc_strcmp(pszObjectType, pszSecondaryObjType) == 0)
        {
            iCount++;
            LM5_ALLOCATE_TAG_MEM(iRetCode, (*pptSecondaryObjects), iCount);
            (*pptSecondaryObjects)[iCount-1] = ptSecondaryObjs[iIterator].secondary;

            LM5_ALLOCATE_TAG_MEM(iRetCode, (*pptRelation), iCount);
            (*pptRelation)[iCount-1] = ptSecondaryObjs[iIterator].the_relation;
        }
    }

    (*piObjectCount ) = iCount;

    LM5_MEM_TCFREE(pszObjectType);
    LM5_MEM_TCFREE(ptSecondaryObjs);
    return iRetCode;
}

/**
 * This function creates specification relationship between primary and secondary object.
 * 
 * @param[in] tPrimaryObj      Tag of the primary object.
 * @param[in] tSecondaryObj    Tag of the secondary object.
 * @param[in] pszRelationType  Name of relation type.
 * @param[out] ptRelation      Tag to newly created relation type.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LM5_grm_create_relation
(
    tag_t   tPrimaryObj,
    tag_t   tSecondaryObj,
    char*   pszRelationType,
    tag_t*  ptRelation
)
{
    int    iRetCode        = ITK_ok;
    tag_t  tRelationType   = NULLTAG;
    tag_t  tNewRelation    = NULLTAG;

    /* Initializing the Out Parameter to this function. */
    *ptRelation = NULLTAG;

    /* Get relation type */
    LM5_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationType, &tRelationType));

    if(tRelationType == NULLTAG)
        iRetCode = GRM_invalid_relation_type;
    if(tPrimaryObj == NULLTAG)
        iRetCode = GRM_invalid_primary_object;
    if(tSecondaryObj == NULLTAG)
        iRetCode = GRM_invalid_secondary_object;

     /*     Find relation to check if relation already exists   */
    LM5_ITKCALL(iRetCode, GRM_find_relation(tPrimaryObj, tSecondaryObj, tRelationType, &tNewRelation));

    if(tNewRelation == NULLTAG)
    {
        LM5_ITKCALL(iRetCode, GRM_create_relation(tPrimaryObj, tSecondaryObj, tRelationType, NULLTAG, &tNewRelation));

        if(tNewRelation != NULLTAG)
        {
            LM5_ITKCALL(iRetCode, GRM_save_relation(tNewRelation));
        }
    }

    if(iRetCode == ITK_ok)
    {
        ( *ptRelation ) = tNewRelation;
    }
    
    return iRetCode;
}

/**
 * This function get's the object's related to input business object 'tObject'. Related objects are retrieved based on various 
 * input criteria such as:
 *
 *   <b> Relation Type between business objects </b>
 *   <b> Type of related object </b>
 *   <b> Related objects can be excluded based on status </b>
 * 
 * @note: This function is generic function and is capable of retrieving Primary and Secondary relation objects based on value
 *        of input arguments 'bGetPrimary' If argument 'pszRelationTypeName' is NULL then all related objects irrespective
 *        of relation type will be retrieved. If 'pszObjType' is NOT NULL then related objects of type 'pszObjType' will be 
 *        retrieved.
 *
 * @param[in]  tObject              Tag of Object
 * @param[in]  pszRelationTypeName  Relation Type. NULLTAG means all relation types.
 * @param[in]  pszObjType           Specific type of object which is related to input object 'tObject'.
 *                                  NULL means any type of related object.
 * @param[in]  pszExcludeStatus     Related objects with this status will be excluded from valid related objects list
 * @param[in]  bGetPrimary          'true' of Primary related objects needs to be retrieved from input objects else  
 *                                  secondary objects will be retrieved. 
 * @param[out] pptValidRelatedObj   List of valid related objects
 * @param[out] piRelatedObjCnt      Count of valid related objects.
 *
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LM5_grm_get_related_obj
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    char*    pszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary,
    tag_t**  pptValidRelatedObj,
    int*     piRelatedObjCnt
)
{
    int     iRetCode              = ITK_ok;
    int     iCount                = 0;
    int     iTempCnt              = 0;
    int     iRelatedObjCnt        = 0;
    int     iLoadedObjCnt         = 0;    
    tag_t   tRelationType         = NULLTAG;
    tag_t*  ptTempObj             = NULL;
    tag_t*  ptLoadedObj           = NULL;
    GRM_relation_t *ptRelatedObj  = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piRelatedObjCnt)    = 0;
    (*pptValidRelatedObj) = NULL;

    if(pszRelationTypeName != NULL)
    {
        /* Get the relation type tag e.g. "IMAN_specification"  */
        LM5_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    }
    
    if(bGetPrimary == true)
    {
        LM5_ITKCALL(iRetCode, GRM_list_primary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }
    else
    {
        LM5_ITKCALL(iRetCode, GRM_list_secondary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }

    /* Related objects are deliberately collected in this loop so that they can be loaded using POM API in one shot */                 
    for(int iNx = 0; iNx < iRelatedObjCnt && iRetCode == ITK_ok; iNx++)
    {
        int     iActiveSeq  = 0;
        tag_t   tObject     = NULLTAG;
        logical bIsValid    = false;
        logical bIsWSObj    = false;
        
        if(bGetPrimary == true)
        {
            tObject = ptRelatedObj[iNx].primary;
        }
        else
        {
            tObject = ptRelatedObj[iNx].secondary;
        }

        LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(tObject, LM5_CLASS_WORKSPACEOBJECT, &bIsWSObj));

        if(iRetCode == ITK_ok)
        {
            if(bIsWSObj == true)
            {
                LM5_ITKCALL(iRetCode, AOM_ask_value_int(tObject, LM5_ATTR_ACTIVE_SEQ, &iActiveSeq));
            }
            else
            {
                iActiveSeq = 1;
            }
        }
    
        /* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0 && iRetCode == ITK_ok)
        {
            logical bBelongsToClass = false;

            if(pszObjType != NULL && tc_strlen(pszObjType) > 0)
            {
                LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(tObject, pszObjType, &bBelongsToClass));
            }
            else
            {
                bBelongsToClass = true;
            }

            if( bBelongsToClass ==  true && iRetCode == ITK_ok)
            {
                tag_t  tStatus  = NULLTAG;

                if(pszExcludeStatus != NULL && tc_strlen(pszExcludeStatus) > 0)
                {
                    LM5_ITKCALL(iRetCode, LM5_obj_ask_status_tag(tObject, pszExcludeStatus, &tStatus));
                }

                if(tStatus == NULLTAG && iRetCode == ITK_ok)
                {
                    LM5_UPDATE_TAG_ARRAY(iCount, (*pptValidRelatedObj), tObject);
                }
            }
        }
    }
  
    (*piRelatedObjCnt) = iCount;

    LM5_MEM_TCFREE(ptLoadedObj);
    LM5_MEM_TCFREE(ptTempObj);
    LM5_MEM_TCFREE(ptRelatedObj);
    return iRetCode;
}

/**
 * This function returns a list of secondary objects of requested types which are attached to input primary
 * object with any of the input list of relations.
 *
 * @note This function returns the value depending on secondary Object Type list.
 * 
 * @param[in]  tPrimaryObj           The object.
 * @param[in]  iRelCount             Count of Relation.  Can be 0.
 * @param[in]  ppszRelTypeList       Name of relation types. Can be NULL.
 * @param[in]  iSecObjCnt            Count of secondary object type
 * @param[in]  ppszSecObjTypeList    List of secondary object type.
 * @param[out] pptSecondaryObjects   List of Primary object of specified type.
 * @param[out] piObjectCount         Primary Object count.
 *  
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LM5_grm_get_sec_obj_attach_with_rel_type
(
    tag_t   tPrimaryObj,
    int     iRelCount,
    char**  ppszRelTypeList,
    int     iSecObjCnt,
    char**  ppszSecObjTypeList,
    tag_t** pptSecondaryObjects,
    int*    piObjectCount  
)
{
    int     iRetCode              = ITK_ok;
    int     iCount                = 0;
    int     iIterator             = 0;
    int     iSecondaryCount       = 0;
    tag_t   tRelationType         = NULLTAG;
    GRM_relation_t *ptSecObjects  = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piObjectCount) = 0;
    (*pptSecondaryObjects) = NULL;

    /* Get the relation type tag e.g. "IMAN_specification". If there is only one input relation then find the secondary objects
       with this specific relation else find all the secondary objects and validate the relation type from the input list.
    */
    if(iRelCount == 1 && ppszRelTypeList != NULL)
    {
        LM5_ITKCALL(iRetCode, GRM_find_relation_type(ppszRelTypeList[0], &tRelationType));
    }
    
    LM5_ITKCALL(iRetCode, GRM_list_secondary_objects(tPrimaryObj, tRelationType, &iSecondaryCount, &ptSecObjects));
                                       
    for(iIterator = 0; iIterator < iSecondaryCount && iRetCode == ITK_ok; iIterator++)
    {
        char    szRelTypeName[TCTYPE_name_size_c+1] = {""};
        logical bIsValidRel    = false;

        LM5_ITKCALL(iRetCode, TCTYPE_ask_name(ptSecObjects[iIterator].relation_type , szRelTypeName));

        for(int iDx = 0; iDx < iRelCount && iRetCode == ITK_ok; iDx++)
        {
            if(tc_strcmp(szRelTypeName, ppszRelTypeList[iDx]) == 0)
            {
                bIsValidRel = true;
                break;
            }
        }

        if(bIsValidRel == true)
        {
            char* pszType  = false;

            LM5_ITKCALL(iRetCode, LM5_obj_ask_type(ptSecObjects[iIterator].secondary, &pszType));

            for(int iJx = 0; iJx < iSecObjCnt && iRetCode == ITK_ok; iJx++)
            {
                if (tc_strcmp(pszType, ppszSecObjTypeList[iJx]) == 0)
                {
                    LM5_UPDATE_TAG_ARRAY(iCount, (*pptSecondaryObjects), ptSecObjects[iIterator].secondary);
                }
            }

            LM5_MEM_TCFREE(pszType);
        }
    }

	(*piObjectCount ) = iCount;

    LM5_MEM_TCFREE(ptSecObjects);
    return iRetCode;
}

/**
 * This function get's the object's related to input object 'tObject'. 
 * 
 * @note: Instances are deliberately loaded in this function using POM API because due to high latency some of related instances
 *        are loaded and while extracting attribute value of these related object error is prompted to GUI.
 *
 * @param[in]  tObject              Tag of Object
 * @param[in]  pszRelationTypeName  Relation Type. NULLTAG means all relation types.
 * @param[in]  pszObjType           Specific type of object which is related to input object 'tObject'.
 *                                  NULL means any type of related object.
 * @param[in]  pszExcludeStatus     Related objects with this status will be excluded from valid related objects list
 * @param[in]  bGetPrimary          'true' of Primary related objects needs to be retrieved from input objects else  
 *                                  secondary objects will be retrieved. 
 * @param[out] pptValidRelatedObj   List of valid related objects
 * @param[out] piRelatedObjCnt      Count of valid related objects.
 *
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LM5_grm_load_and_get_related_obj
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    char*    pszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary,
    tag_t**  pptValidRelatedObj,
    int*     piRelatedObjCnt
)
{
    int     iRetCode              = ITK_ok;
    int     iCount                = 0;
    int     iTempCnt              = 0;
    int     iRelatedObjCnt        = 0;
    int     iLoadedObjCnt         = 0;    
    tag_t   tRelationType         = NULLTAG;
    tag_t*  ptTempObj             = NULL;
    tag_t*  ptLoadedObj           = NULL;
    GRM_relation_t *ptRelatedObj  = NULL;
    
    /* Initializing the Out Parameter to this function. */
    (*piRelatedObjCnt)    = 0;
    (*pptValidRelatedObj) = NULL;

    if(pszRelationTypeName != NULL)
    {
        /* Get the relation type tag e.g. "IMAN_specification"  */
        LM5_ITKCALL(iRetCode, GRM_find_relation_type(pszRelationTypeName, &tRelationType)); 
    }
    
    if(bGetPrimary == true)
    {
        LM5_ITKCALL(iRetCode, GRM_list_primary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }
    else
    {
        LM5_ITKCALL(iRetCode, GRM_list_secondary_objects(tObject, tRelationType, &iRelatedObjCnt, &ptRelatedObj));
    }

    /* Related objects are deliberately collected in this loop so that they can be loaded using POM API in one shot */                 
    for(int iNx = 0; iNx < iRelatedObjCnt && iRetCode == ITK_ok; iNx++)
    {
        if(bGetPrimary == true)
        {
            LM5_UPDATE_TAG_ARRAY(iTempCnt, ptTempObj, (ptRelatedObj[iNx].primary));
        }
        else
        {
            LM5_UPDATE_TAG_ARRAY(iTempCnt, ptTempObj, (ptRelatedObj[iNx].secondary));
        }
    }
    
    /* Instances are loaded because due to high latency some of related instances are loaded and while extracting 
       attribute value of these related object error is prompted to GUI.
    */
    LM5_ITKCALL(iRetCode, POM_load_instances_possible(iTempCnt, ptTempObj, &iLoadedObjCnt, &ptLoadedObj));

    for(int iDx = 0; iDx < iRelatedObjCnt && iRetCode == ITK_ok; iDx++)
    {
        int     iActiveSeq   = 0;
        logical bIsWSObj     = false;

        LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(ptTempObj[iDx], LM5_CLASS_WORKSPACEOBJECT, &bIsWSObj));

        if(iRetCode == ITK_ok)
        {
            if(bIsWSObj == true)
            {
                LM5_ITKCALL(iRetCode, AOM_ask_value_int(ptTempObj[iDx], LM5_ATTR_ACTIVE_SEQ, &iActiveSeq));
            }
            else
            {
                iActiveSeq = 1;
            }
        }
    
        /* This check indicates that Item Revisions with Active sequence will be considered */
        if(iActiveSeq != 0 && iRetCode == ITK_ok)
        {
            logical bBelongsToClass = false;

            if(pszObjType != NULL && tc_strlen(pszObjType) > 0)
            {
                LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(ptTempObj[iDx], pszObjType, &bBelongsToClass));
            }
            else
            {
                bBelongsToClass = true;
            }

            if( bBelongsToClass ==  true && iRetCode == ITK_ok)
            {
                tag_t  tStatus  = NULLTAG;

                if(pszExcludeStatus != NULL && tc_strlen(pszExcludeStatus) > 0)
                {
                    LM5_ITKCALL(iRetCode, LM5_obj_ask_status_tag(ptTempObj[iDx], pszExcludeStatus, &tStatus));
                }

                if(tStatus == NULLTAG && iRetCode == ITK_ok)
                {
                    LM5_UPDATE_TAG_ARRAY(iCount, (*pptValidRelatedObj), ptTempObj[iDx]);
                }
            }
        }
    }
  
    (*piRelatedObjCnt) = iCount;

    LM5_MEM_TCFREE(ptLoadedObj);
    LM5_MEM_TCFREE(ptTempObj);
    LM5_MEM_TCFREE(ptRelatedObj);
    return iRetCode;
}

#ifdef __cplusplus
}
#endif