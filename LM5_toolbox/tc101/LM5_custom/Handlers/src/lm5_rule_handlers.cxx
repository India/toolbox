/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_rule_handlers.cxx

    Description: This file contains function that registers all Rule Handlers for SMA custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_rule_handlers.hxx>
#include <lm5_workflow_handlers.hxx>

/**
 * Registers the rule handlers for workflows.
 * 
 * @return Status of registration of rule handlers.
 */
int LM5_register_rule_handlers
(
    void
)
{
    int iRetCode      = ITK_ok;
    
    TC_write_syslog("In LM5 Register Rule Handlers Operation\n");
    
    LM5_ITKCALL(iRetCode, EPM_register_rule_handler( LM5_WF_RH_VALIDATE_TARGET_OBJ_OWNER, "", LM5_validate_target_object_owner_rh ));


    return iRetCode;    
}

#ifdef __cplusplus
}
#endif



