/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_action_handlerss.cxx

    Description: This file contains function that registers all Action Handlers for LMtech custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_action_handlers.hxx>
#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_tc_project_function.hxx>
#include <lm5_workflow_handlers.hxx>

/**
 * Functon to register the action handlers to be used in the workflows.
 *
 * @return Status of registration of action handlers. 
 */
int LM5_register_action_handlers
(
    void
)
{
    int iRetCode = ITK_ok;
    
    TC_write_syslog("In LM5 Register Action Handlers Operation\n");

    LM5_ITKCALL( iRetCode, EPM_register_action_handler( LM5_WF_AH_ASSIGN_PROJ, "", LM5_assign_project));

    LM5_ITKCALL( iRetCode, EPM_register_action_handler( LM5_WF_AH_PROPAGATE_SECURITY_INFO, "", LM5_propagate_security_info));
    
    return iRetCode;
}


#ifdef __cplusplus
}
#endif

