/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_tc_project_function.cxx

    Description: This File contains custom functions related to TC Project assignment, removal and manipulation.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Singh          Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_errors.hxx>
#include <lm5_tc_project_function.hxx>
    
/**
 * The handler is specifically designed to assign Project to Item, Revision and the attached business object to Item Revision.
 * The project is retrieved from Item revision which is attached to the workflow target. If no project is assigned to workflow 
 * target Item revision then project with Project ID same as Item ID of Item is searched and assigned to Item of the Item revision
 * which is present in the workflow traget.
 * TC Project is assigned to valid contents of the Folder which are attached to Item revision through 'IMAN_reference' relation.
 *
 * @note: Function also get all the contents from the folder through recursion if Folder exists as part of another Folder content.
 *
 * -object_types
 *    This argument specifies the object type that is used to be revised. Multiple object types can be specified separated by a
 *    "," (comma).
 *
 * @param[in]  msg  handler message
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LM5_assign_project
(
    EPM_action_message_t msg
)
{
    int      iRetCode              = ITK_ok;
    int      iObjTypeCnt           = 0;
    int      iTargetCount          = 0;
    int      iPropInfoCnt          = 0;
    int      iValidObjTypeCnt      = 0;
    int      iPrefValCnt           = 0;
    tag_t    tRootTask             = NULLTAG;
    tag_t*   ptTargetObjects       = NULL;
    char*    pszOrigArgValObjType  = NULL;
    char**   ppszObjTypeList       = NULL;
    char**   ppszPrefVals          = NULL;
    char**   ppszValidObjType      = NULL;
    sStringArray* psStringInfoList = NULL;
    
    /* Getting the root task. */
    LM5_ITKCALL(iRetCode, EPM_ask_root_task( msg.task, &tRootTask));

    /* Getting the "-object_types" argument value. */ 
    LM5_ITKCALL(iRetCode, LM5_arg_get_arguments(msg.arguments, msg.task, LM5_WF_ARG_OBJECT_TYPES,
                                                  LM5_STRING_COMMA, false, &iObjTypeCnt, &ppszObjTypeList,
                                                  &pszOrigArgValObjType));
    /* Getting the target attachement. */
    if(iObjTypeCnt == 0)
    {
        iRetCode = INVALID_ARG_VAL_SUPPLIED_TO_WF_HANDLER;
        EMH_store_error_s2(EMH_severity_error, iRetCode, LM5_WF_ARG_OBJECT_TYPES, LM5_WF_AH_ASSIGN_PROJ );
        return iRetCode;
    }

    LM5_ITKCALL(iRetCode, LM5_arg_get_attached_obj_of_input_type_list(tRootTask, EPM_target_attachment, ppszObjTypeList, 
                                                                        iObjTypeCnt, &iTargetCount, &ptTargetObjects));

    LM5_ITKCALL(iRetCode, LM5_pref_get_string_values(LM5_PREF_FL_CONTENT_PROPAGATE_OBJ, &iValidObjTypeCnt, &ppszValidObjType));

    LM5_ITKCALL(iRetCode, LM5_pref_get_string_values(LM5_PREF_SMAPROJECT_REL_PROPAGATION, &iPrefValCnt, &ppszPrefVals));

    LM5_ITKCALL(iRetCode, LM5_parse_str_array(iPrefValCnt, ppszPrefVals, LM5_STRING_COLON, &iPropInfoCnt,
                                                        &psStringInfoList));

    for(int iDx = 0; iDx < iTargetCount && iRetCode == ITK_ok; iDx++)
    {
        int     iProjCnt    = 0;
        tag_t   tItem       = NULLTAG;
        tag_t*  ptProjList  = NULL;
               
        LM5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(ptTargetObjects[iDx], &tItem));
        /* Project List is retrieved from Item Revision and will be assigned to Item so that project assigned on revision
           and item are in sync
        */
        LM5_ITKCALL(iRetCode, AOM_ask_value_tags(ptTargetObjects[iDx], LM5_ATTR_PROJ_LIST, &iProjCnt, &ptProjList));

        if(iProjCnt == 0 && iRetCode == ITK_ok)
        {
            char*  pszProjID  = NULL;
            tag_t  tProject   = NULLTAG;

            /* LM5_Phase_Plan 'item_id' is same as the Project ID that needs to be assigned to all the related Objects */
            LM5_ITKCALL(iRetCode, AOM_ask_value_string(tItem, LM5_ATTR_ITEM_ID, &pszProjID));

            LM5_ITKCALL(iRetCode, PROJ_find(pszProjID, &tProject));

            if(tProject != NULLTAG)
            {                
                iProjCnt++;

                LM5_ALLOCATE_TAG_MEM(iRetCode, ptProjList, iProjCnt);

                ptProjList[iProjCnt-1] = tProject;
            }

            LM5_MEM_TCFREE( pszProjID );
        }

        if(iProjCnt > 0)
        {
            int    iSecObjCnt     = 0;
            int    iObjCnt        = 0;
            tag_t* ptObjList      = NULL;
            tag_t* ptSecObj       = NULL;
            
            /* Secondary Objects attached to Item revision will get project through propagation rule hence will not be
               processed explicitly. If secondary object is of type folder then only the content of the folder will
               be processed because the content of folder does not receive project information through propagation rule.
            */
            LM5_ITKCALL(iRetCode, LM5_grm_get_related_obj(ptTargetObjects[iDx], LM5_IMAN_REFERENCE, NULL, NULL, false, 
                                                                &ptSecObj, &iSecObjCnt));
            
            for(int iJx = 0; iJx < iSecObjCnt && iRetCode == ITK_ok; iJx++)
            {
                logical bISTypeOfFL = false;

                LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(ptSecObj[iJx], LM5_CLASS_FOLDER, &bISTypeOfFL));

                if(bISTypeOfFL == true)
                {
                    int     iNoOfRef       = 0;
                    tag_t*  ptReferences   = NULL;

                    LM5_ITKCALL(iRetCode, FL_ask_references(ptSecObj[iJx], FL_fsc_as_ordered, &iNoOfRef, &ptReferences));

                    if(iNoOfRef > 0)
                    {
                        LM5_ITKCALL(iRetCode, LM5_get_valid_and_related_obj_list_to_propagate_proj_info(iNoOfRef, ptReferences, iValidObjTypeCnt, 
                                                                                                          ppszValidObjType, iPropInfoCnt, psStringInfoList,
                                                                                                          &iObjCnt, &ptObjList));
                    }

                    LM5_MEM_TCFREE( ptReferences );
                }
            }

            /*  Assign Project to Item of revision on which WF is initiated  */
            LM5_ITKCALL(iRetCode, PROJ_assign_objects ( iProjCnt, ptProjList, 1, &tItem ));

            if(iObjCnt > 0)
            {
                LM5_ITKCALL(iRetCode, PROJ_assign_objects ( iProjCnt, ptProjList, iObjCnt, ptObjList ));
            }

            
            LM5_MEM_TCFREE( ptObjList );
            LM5_MEM_TCFREE( ptSecObj );
        }

        LM5_MEM_TCFREE( ptProjList );
    }

    LM5_MEM_TCFREE( ptTargetObjects );
    LM5_MEM_TCFREE( ppszObjTypeList );
    LM5_MEM_TCFREE( pszOrigArgValObjType );
    LM5_MEM_TCFREE( ppszValidObjType );
    LM5_MEM_TCFREE( ppszPrefVals );
    LM5_TCFREE_STRING_INFO_STRUCT( psStringInfoList, iPropInfoCnt );
    return iRetCode;
}

/**
 * This function checks if the input primary objects are of valid type based on the preference 'LM5_Folder_Content_Propagate_Objects'. 
 * For the valid primary objects function gets the related secondary objects using the preference 'LM5_Project_Relation_Propagation_Rules'.
 * This preference specifies all relation types for any primary object that are considered for second level project propagation   
 * when assigning a project to the primary object.
 * The pattern is <b>"<PRIMARY OBJECT TYPE>:<RELATION TYPE 1>[,<RELATION TYPE N>]"</b>
 *
 * @note: This function will be called recursively for those input Primary objects which are of type Folders. Input argument
 *        'piRelatedObjCnt' should be carefully handlers from the calling function.
 *
 * @param[in]  iObjCnt          Count of Primary Objects
 * @param[in]  ptPrimaryObj     Tags of Primary objects
 * @param[in]  iValidObjTypeCnt Count of valid object Type List
 * @param[in]  ppszValidObjType List of Object types which will can are allowed for project assignment
 * @param[in]  iPropInfoCnt     Size of structure containing propagation information
 * @param[in]  psStringInfoList Structure containing relation name which are used to find attached secondary 
 *                              objects to primary object
 * @param[out] piRelatedObjCnt  Count of Secondary objects
 * @param[out] pptRelatedObj    Tags of Secondary objects
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LM5_get_valid_and_related_obj_list_to_propagate_proj_info
(
    int           iObjCnt,
    tag_t*        ptPrimaryObj,
    int           iValidObjTypeCnt,
    char**        ppszValidObjType,
    int           iPropInfoCnt,
    sStringArray* psStringInfoList,
    int*          piRelatedObjCnt,
    tag_t**       pptRelatedObj
)
{
    int  iRetCode    = ITK_ok;
    int  iObjectCnt  = 0;

    iObjectCnt = (*piRelatedObjCnt);

    for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
    {
        char*    pszObjType   = NULL;
        logical  bIsValidType = false;

        LM5_ITKCALL(iRetCode, LM5_obj_ask_type(ptPrimaryObj[iDx], &pszObjType));

        /* Validating if input primary object belongs to valid list of objects.  */
        for(int iZx = 0; iZx < iValidObjTypeCnt && iRetCode == ITK_ok; iZx++)
        {
            if(tc_strcmp(pszObjType, ppszValidObjType[iZx]) == 0)
            {
                logical bISTypeOfIR = false;

                LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(ptPrimaryObj[iDx], LM5_CLASS_ITEMREVISION, &bISTypeOfIR));

                if(iRetCode == ITK_ok)
                {
                    bIsValidType = true;
                    iObjectCnt++;
                    LM5_ALLOCATE_TAG_MEM(iRetCode, (*pptRelatedObj), iObjectCnt);

                    if( bISTypeOfIR == true )
                    {
                        tag_t tItem  = NULLTAG;

                        LM5_ITKCALL(iRetCode, ITEM_ask_item_of_rev ( ptPrimaryObj[iDx], &tItem ));

                        (*pptRelatedObj)[iObjectCnt-1] = tItem;
                    }
                    else
                    {
                         (*pptRelatedObj)[iObjectCnt-1] = ptPrimaryObj[iDx];
                    }
                }

                break;
            }
        }
        /* Retrieving and validating secondary object types associated with appropriate relation to valid primary objects  */
        if(bIsValidType == true)
        {
            for(int iYx = 0; iYx < iPropInfoCnt && iRetCode == ITK_ok; iYx++)
            {
                if(psStringInfoList[iYx].iArrayLen > 0)
                {
                    if(tc_strcmp(pszObjType, psStringInfoList[iYx].ppszValue[0]) == 0)
                    {
                        /* Getting all secondary objects that are associated with the list of following relations. */
                        for(int iBx = 1; iBx < (psStringInfoList[iYx].iArrayLen) && iRetCode == ITK_ok; iBx++)
                        {
                            int    iObjCount  = 0;
                            tag_t* ptSecObj   = NULL;

                            LM5_ITKCALL(iRetCode, LM5_grm_get_related_obj(ptPrimaryObj[iDx], psStringInfoList[iYx].ppszValue[iBx],
                                                                               NULL, NULL, false, &ptSecObj, &iObjCount));

                            for(int iVx = 0; iVx < iObjCount && iRetCode == ITK_ok; iVx++)
                            {
                                logical bISTypeOfIR = false;

                                LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(ptSecObj[iVx], LM5_CLASS_ITEMREVISION, &bISTypeOfIR));

                                if(iRetCode == ITK_ok)
                                {
                                    iObjectCnt++;
                                    LM5_ALLOCATE_TAG_MEM(iRetCode, (*pptRelatedObj), iObjectCnt);

                                    if( bISTypeOfIR == true )
                                    {
                                        tag_t tItem  = NULLTAG;

                                        LM5_ITKCALL(iRetCode, ITEM_ask_item_of_rev ( ptSecObj[iVx], &tItem ));
                                        (*pptRelatedObj)[iObjectCnt-1] = tItem;
                                    }
                                    else
                                    {
                                        (*pptRelatedObj)[iObjectCnt-1] = ptSecObj[iVx];
                                    }
                                }
                            }

                            LM5_MEM_TCFREE(ptSecObj);
                        }

                        break;
                    }
                }
            }   
        }

        /* If input primary object is any type of folder then process the contents of folder to project assignment based on criteria. */
        if(iRetCode == ITK_ok)
        {
            logical bISTypeOfFL = false;

            LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(ptPrimaryObj[iDx], LM5_CLASS_FOLDER, &bISTypeOfFL));

            if(bISTypeOfFL == true)
            {
                int     iNoOfRef       = 0;
                tag_t*  ptReferences   = NULL;

                LM5_ITKCALL(iRetCode, FL_ask_references(ptPrimaryObj[iDx], FL_fsc_as_ordered, &iNoOfRef, &ptReferences));

                if(iNoOfRef > 0)
                {
                    LM5_ITKCALL(iRetCode, LM5_get_valid_and_related_obj_list_to_propagate_proj_info( iNoOfRef, ptReferences, iValidObjTypeCnt,
                                                                                                       ppszValidObjType, iPropInfoCnt, psStringInfoList, 
                                                                                                       &iObjectCnt, pptRelatedObj ));
                }

                LM5_MEM_TCFREE(ptReferences);
            }
        }

        LM5_MEM_TCFREE(pszObjType);
    }

    (*piRelatedObjCnt) = iObjectCnt;

    return iRetCode;
}

/**
 * The handler 
 *
 * @param[in] msg  Handler Message
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LM5_propagate_security_info
(
    EPM_action_message_t msg
)
{
    int    iRetCode            = ITK_ok;
    int    iIRCount            = 0;
    int    iPropInfoCnt        = 0;
    tag_t  tRootTask           = NULLTAG;
    tag_t* ptItemRev           = NULL;
    char*  pszIRType           = NULL;
    char** ppszPropagationInfo = NULL;

    /* Get the target attachments   */
    LM5_ITKCALL(iRetCode, EPM_ask_root_task( msg.task, &tRootTask));
    
    /* Getting the "object_types" argument value. */
    LM5_ITKCALL(iRetCode, LM5_arg_get_wf_argument_value(msg.arguments, msg.task, LM5_WF_ARG_OBJECT_TYPES,
                                                              false, &pszIRType));

    if(pszIRType == NULL)
    {
        LM5_ITKCALL(iRetCode, LM5_arg_get_attached_objects_of_type(tRootTask, EPM_target_attachment, pszIRType, 
                                                                        &iIRCount, &ptItemRev));
    }
    else
    {
        LM5_ITKCALL(iRetCode, LM5_arg_get_attached_objects_of_type(tRootTask, EPM_target_attachment, LM5_CLASS_ITEMREVISION, 
                                                                        &iIRCount, &ptItemRev));
    }

    if(iIRCount > 0)
    {
        LM5_ITKCALL(iRetCode, LM5_pref_get_string_values( LM5_PREF_PROPAGATION_INFORMATION, &iPropInfoCnt, &ppszPropagationInfo ));

        if(iPropInfoCnt == 0)
        {
            TC_write_syslog("Preference \"%s\" for Propagation Information not present in the system. \n",  LM5_PREF_PROPAGATION_INFORMATION);
            return iRetCode;   
        }
    }

    for(int iDx = 0; iDx < iIRCount&& iRetCode == ITK_ok ; iDx++)
    {
        int    iObjCount      = 0;
        int    iProjectCount  = 0;
        int    iLicCount      = 0;
        tag_t* ptObject       = NULL;
        tag_t* ptProjects     = NULL;
        tag_t* ptLicense      = NULL;
        char*  pszIPClassific = NULL;

        LM5_ITKCALL(iRetCode, LM5_get_security_info(ptItemRev[iDx], iPropInfoCnt, ppszPropagationInfo, &iLicCount, &ptLicense, 
                                                        &iProjectCount, &ptProjects, &pszIPClassific));

        if(iLicCount > 0 || iProjectCount > 0 || pszIPClassific != NULL)
        {

        }

        for(int iZx = 0; iZx < iObjCount && iRetCode == ITK_ok; iZx++)
        {
            if(iProjectCount > 0)
            {
                /*  Assign Project to Object */
                LM5_ITKCALL(iRetCode, PROJ_assign_objects (iProjectCount, ptProjects, 1, &(ptObject[iZx])));
            }

            /*  Assign License to Object */
            for(int iFx = 0; iFx < iLicCount && iRetCode == ITK_ok; iFx++)
            {
                LM5_ITKCALL(iRetCode, ADA_add_license_object(ptLicense[iFx], ptObject[iZx]));
            }

            /*  Set IP Classification attribute of Object */
            if(iRetCode == ITK_ok && pszIPClassific != NULL)
            {
                LM5_ITKCALL(iRetCode, LM5_obj_set_str_attribute_val(ptObject[iZx], LM5_ATTR_IP_CLASSIFICATION, pszIPClassific) );
            }
        }

        LM5_MEM_TCFREE( ptProjects );
        LM5_MEM_TCFREE( ptLicense );
        LM5_MEM_TCFREE( pszIPClassific );
        LM5_MEM_TCFREE( ptObject );
    }

    LM5_MEM_TCFREE( pszIRType );
    LM5_MEM_TCFREE( ptItemRev );
    LM5_MEM_TCFREE( ppszPropagationInfo );
    return iRetCode;
}

/**
 * The function retrieves the security attribute information from the Item of the input revision. All the security information
 * follow the propagation rule hence this function assumes that security information is applied to Item and because of propagation
 * rules the information will get assigned to it associated objects as defined in propagation rule in Teamcenter.
 *
 * @param[in]  tItemRev          Tag of Item revision whose Item will be considered for retrieving security information
 * @param[in]  iSecurityAttrCnt  Security attribute count
 * @param[in]  ppszSecurityAttr  Names of Security attribute whose value needs to be fetched
 * @param[out] piLicCount        License Count
 * @param[out] pptLicense        Tag of License assigned to Item of input revision
 * @param[out] piProjectCount    Project count
 * @param[out] pptProjects       Tag of projects
 * @param[out] ppszIPClassific   IP Classification attribute value
 *
 * @retval ITK_ok is successful else ITK return code.
 */
int LM5_get_security_info
(
    tag_t   tItemRev,
    int     iSecurityAttrCnt,
    char**  ppszSecurityAttr,
    int*    piLicCount,
    tag_t** pptLicense,
    int*    piProjectCount,
    tag_t** pptProjects,
    char**  ppszIPClassific
)
{
    int   iRetCode     = ITK_ok;
    tag_t tItem        = NULLTAG;
 
    LM5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));

    for(int iJx = 0; iJx < iSecurityAttrCnt && iRetCode == ITK_ok; iJx++)
    {
        if(tc_strcmp(LM5_CONST_PROJECT, ppszSecurityAttr[iJx]) == 0)
        {   
            /* Get the project assigned to Item */
            LM5_ITKCALL(iRetCode, AOM_ask_value_tags(tItem, LM5_ATTR_PROJ_LIST, piProjectCount, pptProjects));
        }
        else
        {
            if(tc_strcmp(LM5_CONST_LICENSE, ppszSecurityAttr[iJx]) == 0)
            {  
                LM5_ITKCALL(iRetCode, AOM_ask_value_tags(tItem, LM5_ATTR_LICENSE_LIST, piLicCount, pptLicense));
            }
            else
            {
                if(tc_strcmp(LM5_CONST_IP_CLASSIFICATION, ppszSecurityAttr[iJx]) == 0)
                {
                    LM5_ITKCALL(iRetCode, AOM_ask_value_string(tItem, LM5_ATTR_IP_CLASSIFICATION, ppszIPClassific) );
                }
            }
        }
    }
    
    return iRetCode;
}


#ifdef __cplusplus
}
#endif