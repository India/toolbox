/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_workflow_handlers.cxx

    Description: This File contains functions/handlers implementation.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_action_handlers.hxx>
#include <lm5_lib_library.hxx>
#include <lm5_errors.hxx>
#include <lm5_workflow_handlers.hxx>
/**
 * This handler validates if the target object owner and the workflow initiator are same.
 * If the workflow initiator does not match the owning user of all target objects specified by "-object_type" argument
 * of workflow then an error has to be thrown. The validation stops after the first invalid object was found.
 *
 * @param [in] msg  EPM rule message.
 * 
 * @retval EPM_go if successful else EPM_nogo
 */
EPM_decision_t LM5_validate_target_object_owner_rh
(
    EPM_rule_message_t msg
)
{
    int      iRetCode               = ITK_ok;
    int      iTargetCount           = 0;
    int      iObjTypeCnt            = 0;
    tag_t    tRootTask              = NULLTAG;
    tag_t*   ptTargetObjects        = NULL;
    char**   ppszObjTypeList        = NULL;
    char*    pszOrigArgValObjType   = NULL;
    EPM_decision_t     decision     = EPM_go;
    
    /* Getting the root task. */
    LM5_ITKCALL(iRetCode, EPM_ask_root_task( msg.task, &tRootTask));

    /* Getting the "-object_type" argument value. */ 
    LM5_ITKCALL(iRetCode, LM5_arg_get_arguments(msg.arguments, msg.task, LM5_WF_ARG_OBJECT_TYPES,
                                                  LM5_STRING_COMMA, true, &iObjTypeCnt, &ppszObjTypeList,
                                                  &pszOrigArgValObjType));
     /* Getting the target attachement. */
    if(iObjTypeCnt == 0)
    {
        /* Get the target objects. */
        LM5_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, EPM_target_attachment, 
                                                   &iTargetCount, &ptTargetObjects));
    }
    else
    {
        LM5_ITKCALL(iRetCode, LM5_arg_get_attached_obj_of_input_type_list(tRootTask, EPM_target_attachment, ppszObjTypeList, iObjTypeCnt,
                                                                            &iTargetCount, &ptTargetObjects));

    }

    /* browse through the target attachments. */
    for(int iDx = 0; iDx < iTargetCount && iRetCode == ITK_ok; iDx++)
    {
        char*   pszOwningUserName       = NULL;
        char*   pszLoggedInUserName     = NULL;
        tag_t   tOwningUser             = NULLTAG;
        tag_t   tLoggedInUser           = NULLTAG;

        /* Get owning user of target object type against. */
        LM5_ITKCALL(iRetCode, LM5_obj_get_owning_user(ptTargetObjects[iDx], &pszOwningUserName, &tOwningUser));

        /* Get logged-in user. */
        LM5_ITKCALL(iRetCode, POM_get_user(&pszLoggedInUserName, &tLoggedInUser));

        if(tc_strcmp(pszOwningUserName, pszLoggedInUserName) != 0 )
        {
            char* pszObjNameVal  = NULL;

            LM5_ITKCALL(iRetCode, LM5_itemrev_construct_name(ptTargetObjects[iDx], &pszObjNameVal));

            EMH_store_error_s2(EMH_severity_error, WF_INITIATOR_IS_NOT_THE_OWNER_OF_ATTACHED_OBJECT,
                               pszLoggedInUserName, pszObjNameVal);

            LM5_MEM_TCFREE(pszObjNameVal);
            decision = EPM_nogo;
            break;
        }
       
        LM5_MEM_TCFREE(pszOwningUserName);
        LM5_MEM_TCFREE(pszLoggedInUserName);
            
   }

    LM5_MEM_TCFREE(ptTargetObjects);
    LM5_MEM_TCFREE(ppszObjTypeList);
    LM5_MEM_TCFREE(pszOrigArgValObjType);
    return decision;
}



#ifdef __cplusplus
}
#endif

 