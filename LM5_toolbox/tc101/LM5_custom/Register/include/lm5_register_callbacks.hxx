/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_register_callbacks.hxx

    Description:  Header File for lm5_register_callbacks.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/
#ifndef LM5_REGISTER_CALLBACKS_HXX
#define LM5_REGISTER_CALLBACKS_HXX
    
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>

extern DLLAPI int LM5_custom_register_callbacks(void);

extern DLLAPI int LM5_register_custom_handlers
(
    int*    piDecision, 
    va_list args
);


#endif

