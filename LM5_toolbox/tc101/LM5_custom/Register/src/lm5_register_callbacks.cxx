/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_register_callbacks.cxx

    Description:

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_const.hxx>
#include <lm5_register_callbacks.hxx>
#include <lm5_errors.hxx>

/**
 * Description:
 *
 * Function to register Callback function. Entry point for custom SMA custom Code.
 *
 * Returns:
 *
 * Status of execution.
 */
extern DLLAPI int LM5_custom_register_callbacks () 
{
    int  iRetCode   = ITK_ok;
        
    printf("INFO: Registered LM5 Register Custom Exits. \n");

    iRetCode = CUSTOM_register_exit ("LM5_custom", "USER_gs_shell_init_module", (CUSTOM_EXIT_ftn_t)  LM5_register_custom_handlers);
    if(iRetCode!=ITK_ok)
    {
        printf("ERROR: Unable to register custom handlers.");
        return iRetCode;
    }

    return iRetCode;
}

#ifdef __cplusplus
}
#endif



