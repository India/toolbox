/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_register_custom_handlers.cxx

    Description:  This file contains function registering custom handlers to be used by the workflow.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/


#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_register_callbacks.hxx>
#include <lm5_rule_handlers.hxx>
#include <lm5_action_handlers.hxx>
#include <lm5_errors.hxx>

/**
 * Registers custom handlers to be used by the workflow.
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
extern DLLAPI int LM5_register_custom_handlers
(
    int*    piDecision, 
    va_list args
)
{
    int iRetCode = ITK_ok;

    TC_write_syslog( "Registering custom handlers ...\n");
    
    (*piDecision)  = ALL_CUSTOMIZATIONS;

        
    iRetCode = LM5_register_rule_handlers();
    if(iRetCode != ITK_ok)
    {
        printf("Unable to register: LM5_register_rule_handlers\n");
    }
    
    iRetCode = LM5_register_action_handlers();
    if(iRetCode != ITK_ok)
    {
        printf("Unable to register: LM5_register_action_handlers\n");
    }
    
    return iRetCode;
}


#ifdef __cplusplus
}
#endif
