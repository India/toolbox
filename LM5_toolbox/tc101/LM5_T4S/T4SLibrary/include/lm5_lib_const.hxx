/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_const.hxx

    Description:  Header File for constants used during scope of the DLL.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-Sep-14    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LM5_LIB_CONST_HXX
#define LM5_LIB_CONST_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>

/* Date Time Format */
#define LM5_DATE_TIME_FORMAT                                     "%d.%m.%Y"

/* Relation    */
#define LM5_IMAN_SPECIFICATION_RELATION                          "IMAN_specification"
#define LM5_IMAN_BASED_ON                                        "IMAN_based_on"
#define LM5_IMAN_MASTER_FORM                                     "IMAN_master_form"

/* Prefixes */
#define LM5_LIBRARY_PREFIX                                       "LM5_"

/* Postfixes */
#define LM5_PREF_SET_PROPERTIES_POSTFIX                          "_Set_Properties"

/* Status Names*/
#define LM5_STATUS_RELEASE                                       "LM5_Released"
#define LM5_STATUS_PRE_RELEASE                                   "LM5_Invalid"


/* Attributes  */
#define LM5_ATTR_DATE_RELEASED                                   "date_released"
#define LM5_ATTR_RELEASE_STATUS_LIST                             "release_status_list"
#define LM5_ATTR_RELEASE_STATUS_NAME                             "name"
#define LM5_ATTR_OBJECT_NAME_ATTRIBUTE                           "object_name"
#define LM5_ATTR_DESCRIPTION_ATTRIBUTE                           "object_desc"
#define LM5_ATTR_ITEM_ID                                         "item_id"
#define LM5_ATTR_ITEM_REVISION_ID                                "item_revision_id"
#define LM5_ATTR_OBJECT_TYPE_ATTRIBUTE                           "object_type"
#define LM5_ATTR_OBJECT_STRING_ATTRIBUTE                         "object_string"
#define LM5_ATTR_OBJECT_CREATION_DATE                            "creation_date"
#define LM5_ATTR_PROCESS_STAGE_LIST                              "process_stage_list"
#define LM5_ATTR_PSOCC_CHILD_ITEM                                "child_item"
#define LM5_ATTR_PSOCC_PARENT_BVR                                "parent_bvr"
#define LM5_ATTR_USER_DATA_1                                     "user_data_1"
#define LM5_ATTR_SIGNOFF_ATTACHMENTS                             "signoff_attachments"
#define LM5_ATTR_PARENT_PROCESSES                                "parent_processes"
#define LM5_ATTR_ALL_TASKS                                       "all_tasks"

/* Attributes Value */
#define LM5_ATTR_VAL_TRUE                                        "true"
#define LM5_ATTR_VAL_FALSE                                       "false"

/* Class Name */
#define LM5_CLASS_RELEASE_STATUS                                 "releasestatus"
#define LM5_CLASS_WORKSPACEOBJECT                                "WorkspaceObject"
#define LM5_CLASS_FORM                                           "Form"
#define LM5_CLASS_STATUS                                         "ReleaseStatus"
#define LM5_CLASS_ITEMREVISION                                   "ItemRevision"
#define LM5_CLASS_ITEM                                           "Item"
#define LM5_CLASS_DATASET                                        "Dataset"
#define LM5_CLASS_EPM_TASK                                       "EPMTask"
#define LM5_CLASS_IMANRELATION                                   "ImanRelation"
  
/* Workflow Argument Name  */
#define LM5_WF_ARG_TYPES_LIST                                    "type"

/* Name Of Privileges  */
#define LM5_WRITE_ACCESS                                         "WRITE"

/* Organisation */
#define LM5_GROUP_ADMIN                                          "ADMIN"
#define LM5_ROLE_DATA_ADMIN                                      "Data Administrator"

/* System Administrator  */
#define LM5_APPROVER                                             "Approver"
#define LM5_REVIEWER                                             "Reviewer"

/* Folder Names String */
#define LM5_NEWSTUFF_FOLDER                                      "newstuff"
#define LM5_HOME_FOLDER                                          "home"

/* Separators */
#define LM5_STRING_UNDERSCORE                                    "_"
#define LM5_STRING_COMMA                                         ","
#define LM5_STRING_SEMICOLON                                     ";"
#define LM5_STRING_COLON                                         ":"
#define LM5_STRING_FORWARD_SLASH                                 "/"
#define LM5_STRING_SPACE                                         " "

#define LM5_SEPARATOR_LENGTH                                     1
#define LM5_NUMERIC_ERROR_LENGTH                                 12

#endif

