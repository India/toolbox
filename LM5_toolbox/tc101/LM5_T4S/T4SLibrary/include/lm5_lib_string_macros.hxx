/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_string_macros.hxx

    Description:  This file contains macros for string's basic operation such as copy, concatination etc.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
24-Oct-13     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LM5_LIB_STRING_MACROS_HXX
#define LM5_LIB_STRING_MACROS_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <lm5_lib_library.hxx>

#ifdef LM5_CUST_STRCPY
#undef LM5_CUST_STRCPY
#endif

#define LM5_cust_strcpy(pszDestiStr, pszSourceStr){\
            pszDestiStr = (char*)MEM_alloc((int)(sizeof(char)* ( tc_strlen(pszSourceStr) + 1 )));\
            tc_strcpy(pszDestiStr, pszSourceStr);\
}

#define LM5_cust_strcat(pszDestiStr, pszSourceStr){\
    int iSourceStrLen = (int) tc_strlen(pszSourceStr);\
    int iDestiStrLen = 0;\
    if(pszDestiStr != NULL){\
        iDestiStrLen = (int) tc_strlen (pszDestiStr);\
        pszDestiStr = (char*)MEM_realloc(pszDestiStr, (int)((iSourceStrLen + iDestiStrLen + 1)* sizeof(char)));\
    }\
    else{\
        pszDestiStr = (char*)MEM_alloc((int)(sizeof(char) * (iSourceStrLen + 1)));\
    }\
    if(iDestiStrLen == 0){\
       tc_strcpy(pszDestiStr, pszSourceStr);\
    }\
    else{\
       tc_strcat(pszDestiStr, pszSourceStr);\
       tc_strcat(pszDestiStr, '\0');\
       }\
}

#define LM5_cust_update_string_array(iCurrentArrayLen, ppszStrArray, pszNewValue){\
            iCurrentArrayLen++;\
            if(iCurrentArrayLen == 1){\
                ppszStrArray = (char**) MEM_alloc(iCurrentArrayLen * sizeof(char*));\
            }\
            else{\
                ppszStrArray = (char**) MEM_realloc(ppszStrArray, iCurrentArrayLen * sizeof(char*));\
            }\
            ppszStrArray[iCurrentArrayLen - 1] = (char*) MEM_alloc((int)((tc_strlen(pszNewValue) + 1)* sizeof(char)));\
            tc_strcpy(ppszStrArray[iCurrentArrayLen - 1], pszNewValue);\
}

#endif //LM5_LIB_STRING_MACROS_HXX







