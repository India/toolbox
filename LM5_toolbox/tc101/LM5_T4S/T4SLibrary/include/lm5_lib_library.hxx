/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_library.hxx

    Description:  Header File containing declarations of function spread across all lm5_lib_<UTILITY NAME>_utilities.cxx 
                  For e.g. lm5_lib_dataset_utilities.cxx/lm5_lib_form_utilities.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/
#ifndef LM5_LIB_LIBRARY_HXX
#define LM5_LIB_LIBRARY_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <epm/epm_task_template_itk.h>
#include <tccore/tctype.h>
#include <tc/folder.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <epm/signoff.h>
#include <sa/user.h>
#include <sa/role.h>
#include <bom/bom.h>
#include <pom/pom/pom.h>
#include <ps/ps.h>
#include <cfm/cfm.h>
#include <ctype.h>
#include <pom/enq/enq.h>
#include <me/me.h>
#include <form/form.h>
#include <tccore/project.h>
#include <tc/folder.h>
#include <tccore/tctype.h>
#include <fclasses/tc_date.h>
#include <res/res_itk.h>
#include <sa/am.h>
#include <lm5_lib_itk_errors.hxx>
#include <lm5_lib_itk_mem_free.hxx>
#include <lm5_lib_string_macros.hxx>

/*        Utility Functions For Error Logging        */
int LM5_err_write_to_logfile
(
    char*       pszErrorFunction,
    int         iErrorNumber,
    char*       pszFileName,
    int         iLineNumber
);

int LM5_err_start_debug
(
    char*  pszCallingFunction,
    int    iErrorNumber,
    char*  pszFileName,
    int    iLineNumber
);

/*        Argument Utility Functions        */
int LM5_arg_get_arguments
(
    TC_argument_list_t*   pArgList,
    tag_t                 tTask,
    const char*           pszArgument,
    char*                 pszSep,
    logical               bOptional,
    int*                  piValCount,
    char***               pppszArgValue,
    char**                ppszOriginalArgVal
);

int LM5_arg_get_attached_obj_of_input_type_list
(
    tag_t   task,
    int     iAttachmentType,
    char**  ppszObjTypeList,
    int     iTypeCnt,
    int*    piCount,
    tag_t** pptObject
);

/*        String Utility Functions        */

int LM5_str_parse_string
(
    const char*  pszList,
    char*        pszSeparator,
    int*         iCount,
    char***      pppszValuelist
);


int LM5_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
);

int LM5_contains_string_value
(
    char*  pszStrVal,
    int    iTotalStrVals,
    char** pppszStrVals
);

/*        Utility Functions For Preferences        */
int LM5_pref_get_string_values
(
    char*   pszPrefName, 
    int*    iPrefValCnt, 
    char*** pppszPrefVals
);
    
int LM5_pref_get_integer_value
(
    char*   pszPrefName, 
    int*    piPrefValue
);

/*        Utility Functions For Objects        */

int LM5_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
);

int LM5_obj_save_and_unlock
(
    tag_t       tObject
);

int LM5_obj_is_type_of
(
    tag_t    tObject,
    char*    pszTypeName,
    logical* pbBelongsToInputClass 
);

int LM5_check_privilege
(
    tag_t    tObject,
    logical* pbWriteAccessDenied,
    logical* pbObjCheckedOut,
    logical* pbModifiable
);

int LM5_obj_set_attribute_val
(
    tag_t   tObject,
    char*   pszAttributeName,
    char*   pszStrAttrVal,
    char    szCharAttrVal,
    date_t  dtDateAttrVal,
    tag_t   tTagAttrVal,
    logical bBoolAttrVal,
    int     iIntAttrVal,
    double  iDoubleAttrVal
);

int LM5_set_attribute_value
(
    tag_t  tObject,
    char*  pszAttributeName,
    char*  pszAttributeValue,
    tag_t  tTagAttrVal
);

#endif




