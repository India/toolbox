/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_object_utilities.cxx

    Description:  This File contains custom functions for POM classes to perform generic operations. These functions are part of LM5 library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
24-Oct-13    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * This function gets the name of type for the input business
 * objet.
 * e.g. Consider Item type 'P_KatalogteilRevision'. If we pass tag to
 *      an instance of 'P_KatalogteilRevision' this function will return string
 *      'P_KatalogteilRevision' and not 'ItemRevision'. Consider Dataset of type 'catia'.
 *      If we pass tag to an instance of 'catia' this function will
 *      return 'catia' and not 'Dataset'.
 *
 * @param[in]  tObject       Business Object tag
 * @param[out] ppszObjType   Object type
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
)
{
    int    iRetCode       = ITK_ok;
    tag_t  tObjectType    = NULLTAG;
	char   szObjectType[TCTYPE_name_size_c+1] = {""};

    /* Initializing the Out Parameter to this function. */
    (*ppszObjType) = NULL;
    
    LM5_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject,&tObjectType));
    
    LM5_ITKCALL(iRetCode, TCTYPE_ask_name(tObjectType, szObjectType));
    
    if(iRetCode == ITK_ok)
    {
        LM5_cust_strcpy(*ppszObjType, szObjectType);
    }
    
    return iRetCode;
}

/**
 * Save and unlock the given object
 *
 * @param[in] tObject tag of object to be saved and unloaded
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_save_and_unlock
(
    tag_t       tObject
)
{
    int iRetCode  = ITK_ok;

    /*  Save the object.  */
    LM5_ITKCALL(iRetCode, AOM_save( tObject));
   
    /* Unlock the object. */
    LM5_ITKCALL(iRetCode, AOM_unlock( tObject));
    
    return iRetCode;
}

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int LM5_obj_is_type_of
(
    tag_t    tObject,
    char*    pszTypeName,
    logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
    tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    LM5_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    LM5_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    LM5_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}


/**
 * This function validated if input object has write access and object is not checked-out. If object does not full fill the validation criteria
 * then function will return the modifiable information as 'false'
 * 
 * @param[in]   tObject              Tag of Object to be validated
 * @param[out]  pbWriteAccessDenied  'true' if Object has write access  
 * @param[out]  pbObjCheckedOut      'true' if Object is not checked out
 * @param[out]  pbModifiable         'true' if object is Modifiable i.e. Object has write access and is not checked-out
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LM5_check_privilege
(
    tag_t    tObject,
    logical* pbWriteAccessDenied,
    logical* pbObjCheckedOut,
    logical* pbModifiable
)
{
    int     iRetCode    = ITK_ok;
    logical bAccess     = false;

    (*pbModifiable)        = false;
    (*pbWriteAccessDenied) = false;
    (*pbObjCheckedOut)     = false;

    LM5_ITKCALL(iRetCode, RES_assert_object_modifiable (tObject));

    LM5_ITKCALL(iRetCode, AM_check_privilege (tObject, LM5_WRITE_ACCESS, &bAccess));

    if(bAccess == false)
    {
        (*pbWriteAccessDenied) = true;
        (*pbModifiable) = false;
    }
    else
    {
        logical bCheckedOut = false;

        LM5_ITKCALL(iRetCode, RES_is_checked_out (tObject, &bCheckedOut));

        if(bCheckedOut == false)
        {
            (*pbModifiable)    = true;
        }
        else
        {
            char* pszUser          = NULL;
            tag_t tReservationObj  = NULLTAG;
            tag_t tCheckOutUser    = NULLTAG;
            tag_t tUser            = NULLTAG;

            LM5_ITKCALL(iRetCode, RES_ask_reservation_object (tObject, &tReservationObj));

            LM5_ITKCALL(iRetCode, RES_ask_user (tReservationObj, &tCheckOutUser));

            LM5_ITKCALL(iRetCode, POM_get_user (&pszUser, &tUser));

            if ((tUser != NULLTAG) &&    (tUser == tCheckOutUser))
            {
                char* pszObjAttrVal = NULL;

                LM5_ITKCALL(iRetCode, AOM_ask_value_string(tObject, LM5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjAttrVal));

                TC_write_syslog ("%s object is checked-out by same user\n", pszObjAttrVal);
                (*pbModifiable) = true;
                LM5_MEM_TCFREE(pszObjAttrVal);
            } 
            else 
            {
                (*pbModifiable)    = false;
                (*pbObjCheckedOut) = true;
            }

            LM5_MEM_TCFREE (pszUser);
        }
    }

    return iRetCode;
}

/**
 * This function sets the input attribute with value which is passed as input argument to this function.
 *
 * @note This function does not saves the object whose attribute is reset hence it is the responsibility of
 *       calling function to save the Object in order to commit the changes to database.
 * 
 * @param[in] tObject           Tag of object whose attribute needs to be set
 * @param[in] pszAttributeName  Name of Attribute of input object
 * @param[in] pszAttrVal        String Value of the attribute to be set else 'NULL'
 * @param[in] szCharAttrVal     Character Value of the attribute to be set else '\0'
 * @param[in] dtDateAttrVal     Date value of the attribute to be set else 'NULLDATE'
 * @param[in] tTagAttrVal       Tag value of the attribute to be set else 'NULLTAG'
 * @param[in] bBoolAttrVal      Boolean value of the attribute to be set else 'false'
 * @param[in] iIntAttrVal       Integer value of the attribute to be set else '0'
 * @param[in] iDoubleAttrVal    Double value of the attribute to be set else '0.'
 *
 * @return ITK_ok if successful else ITK return code. 
 */
int LM5_obj_set_attribute_val
(
    tag_t   tObject,
    char*   pszAttributeName,
    char*   pszStrAttrVal,
    char    szCharAttrVal,
    date_t  dtDateAttrVal,
    tag_t   tTagAttrVal,
    logical bBoolAttrVal,
    int     iIntAttrVal,
    double  iDoubleAttrVal
)
{
    int    iRetCode          = ITK_ok;
    char*  pszNameOfValType  = NULL;
    PROP_value_type_t ValueTypeOfProp = PROP_untyped;

    /* Get the type of the property value */
    LM5_ITKCALL(iRetCode, AOM_ask_value_type(tObject, pszAttributeName, &ValueTypeOfProp, &pszNameOfValType));

    /* Reset the property based on its value type */
    if (iRetCode == ITK_ok)
    {
        LM5_ITKCALL(iRetCode, AOM_refresh(tObject, true));

        switch(ValueTypeOfProp)
        {
            case PROP_char :
                 LM5_ITKCALL(iRetCode, AOM_set_value_char(tObject, pszAttributeName, szCharAttrVal));
                 break;

            case PROP_date :
                 LM5_ITKCALL(iRetCode, AOM_set_value_date(tObject, pszAttributeName, dtDateAttrVal));
                 break;

            case PROP_float  :
            case PROP_double :
                 LM5_ITKCALL(iRetCode, AOM_set_value_double(tObject, pszAttributeName, iDoubleAttrVal));
                 break;

            case PROP_int   :
            case PROP_short :
                 LM5_ITKCALL(iRetCode, AOM_set_value_int(tObject, pszAttributeName, iIntAttrVal));
                 break;

            case PROP_logical :
                 LM5_ITKCALL(iRetCode, AOM_set_value_logical(tObject, pszAttributeName, bBoolAttrVal));
                 break;

            case PROP_string :
            case PROP_note   :
                 LM5_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszAttributeName, pszStrAttrVal));
                 break;

            case PROP_typed_reference    :
            case PROP_untyped_reference  :
            case PROP_external_reference :
            case PROP_typed_relation     :
            case PROP_untyped_relation   :
                 LM5_ITKCALL(iRetCode, AOM_set_value_tag(tObject, pszAttributeName, tTagAttrVal));
                 break;

            case PROP_untyped :
            default :
                 TC_write_syslog("LM5_T4S: Unknown property type [%s] could not be reset\n", pszAttributeName);
                 break;
        }

        LM5_ITKCALL(iRetCode, AOM_refresh(tObject, false));
    }

    LM5_MEM_TCFREE(pszNameOfValType);
    return iRetCode;
}

/**
 * Sets the attribute value of the input object based on the type of attribute.
 *  
 * @note: This function does not set Date and Character attribute values
 * @note: This function does not saves the object whose attribute is reset hence it is the responsibility of
 *        calling function to save the Object in order to commit the changes to database.
 *
 * @param[in] tObject           Tag of the object.
 * @param[in] pszAttributeName  Attribute name which is to be set.
 * @param[in] pszAttributevalue Attribute Value to be set.
 * 
 * @retval iRetCode Status of execution of the function. 
 */
int LM5_set_attribute_value
(
    tag_t  tObject,
    char*  pszAttributeName,
    char*  pszAttributeValue,
    tag_t  tTagAttrVal
)
{
    int     iRetCode          = ITK_ok;
    char*   pszNameOfValType  = NULL;
    int     iValue            = 0;
    double  fValue            = 0.0;
    logical lValue            = true;
    PROP_value_type_t ValueTypeOfProp = PROP_untyped;

    /* Get the type of the property value */
    LM5_ITKCALL(iRetCode, AOM_ask_value_type(tObject, pszAttributeName, &ValueTypeOfProp, &pszNameOfValType));

    /* Reset the property based on its value type */
    if (iRetCode == ITK_ok)
    {
        switch(ValueTypeOfProp)
        {
            case PROP_float  :
            case PROP_double :
                fValue = atof(pszAttributeValue);
                LM5_ITKCALL(iRetCode, AOM_set_value_double(tObject, pszAttributeName, fValue));
                break;

            case PROP_int   :
            case PROP_short :
                iValue = atoi(pszAttributeValue);
                LM5_ITKCALL(iRetCode, AOM_set_value_int(tObject, pszAttributeName, iValue));
                break;
            
            case PROP_logical :
                if (tc_strcasecmp(pszAttributeValue, LM5_ATTR_VAL_TRUE) == 0)
                {
                    lValue = true;
                }
                else if (tc_strcasecmp(pszAttributeValue, LM5_ATTR_VAL_FALSE) == 0)
                {
                    lValue = false;
                }
                LM5_ITKCALL(iRetCode, AOM_set_value_logical(tObject, pszAttributeName, lValue));
                break;

            case PROP_string :
            case PROP_note   :
                LM5_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszAttributeName, pszAttributeValue));
                break;

            case PROP_typed_reference    :
            case PROP_untyped_reference  :
            case PROP_external_reference :
            case PROP_typed_relation     :
            case PROP_untyped_relation   :
                if(tTagAttrVal == NULLTAG)
                {
                    LM5_ITKCALL( iRetCode, POM_string_to_tag(pszAttributeValue, &tTagAttrVal));
                }

                LM5_ITKCALL(iRetCode, AOM_set_value_tag(tObject, pszAttributeName, tTagAttrVal));
                break;

            case PROP_untyped :
            default :
                TC_write_syslog("LM5_T4S: Unknown property type [%s] could not be reset\n", pszAttributeName);
                break;
        }
    }

    LM5_MEM_TCFREE(pszNameOfValType);

    return iRetCode;
}



