/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_preference_utilities.cxx

    Description: This File contains functions for Retrieving and Manipulating Preference. These functions are part of PNG library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
24-Oct-13    Tarun Singh          Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Function to read Teamcenter preference with Multiple string value
 *
 * @param[in]  pszPrefName    Preference Name
 * @param[out] piPrefValCnt   Total number of preference values
 * @param[out] pppszPrefVals  String array of preference values
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LM5_pref_get_string_values
(
    char*   pszPrefName, 
    int*    piPrefValCnt, 
    char*** pppszPrefVals
)
{
    int iRetCode   = ITK_ok;
    int iPrefCnt   = 0;

    TC_preference_search_scope_t prefOldScope;
    
    LM5_ITKCALL(iRetCode, PREF_initialize());

    LM5_ITKCALL(iRetCode, PREF_ask_search_scope( &prefOldScope));

    LM5_ITKCALL(iRetCode, PREF_set_search_scope( TC_preference_site ));

    LM5_ITKCALL(iRetCode, PREF_ask_value_count( pszPrefName, &iPrefCnt ));

    if( iPrefCnt != 0 )
        LM5_ITKCALL(iRetCode, PREF_ask_char_values( pszPrefName, piPrefValCnt, pppszPrefVals ));

    LM5_ITKCALL(iRetCode, PREF_set_search_scope (prefOldScope));

    return iRetCode;
}

/**
 * Function to read Teamcenter Preference for Retrieving integer value
 *
 * @param[in]  pszPrefName  Preference Name
 * @param[out] piPrefValue  Integer value of preference            
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LM5_pref_get_integer_value
(
    char*   pszPrefName, 
    int*    piPrefValue
)
{
    int iRetCode  = ITK_ok;
    int iPrefCnt   = 0;

    TC_preference_search_scope_t prefOldScope;
    
    LM5_ITKCALL(iRetCode, PREF_initialize());

    LM5_ITKCALL(iRetCode, PREF_ask_search_scope( &prefOldScope));

    LM5_ITKCALL(iRetCode, PREF_set_search_scope( TC_preference_site ));

    LM5_ITKCALL(iRetCode, PREF_ask_value_count( pszPrefName, &iPrefCnt ));

    if(iPrefCnt != 0 )
        LM5_ITKCALL(iRetCode, PREF_ask_int_value( pszPrefName, 0, piPrefValue ));

    LM5_ITKCALL(iRetCode, PREF_set_search_scope (prefOldScope));

    return iRetCode;
}




