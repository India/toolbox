/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_string_utilities.cxx

    Description: This File contains functions for manipulating string. These functions are part of PNG library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
24-Oct-13    Tarun Singh          Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_errors.hxx>


#ifdef __cplusplus
}
#endif
/**
 * Parses a string containing values seperated by a given separator
 * into an array with values. \n
 * Example: input string <b>item;document;eco</b> will return an
 * array of three strings: <b>item</b>, <b>document</b> and <b>eco</b>
 *
 * @param[in]  pszList         Input list to be parsed
 * @param[in]  pszSeparator    Separator 
 * @param[out] iCount          Number of values in the returned string list
 * @param[out] pppszValuelist  Returned list of values
 *
 * @note The returned array should be freed by LM5_MEM_TCFREE
 *
 * @retval ITK_ok If everything went okay.
 */
int LM5_str_parse_string
(
    const char*  pszList,
    char*        pszSeparator,
    int*         iCount,
    char***      pppszValuelist
)
{
    int    iRetCode     = ITK_ok;
    int    iCounter     = 0;
    char*  pszToken     = NULL;
    char*  pszTempList  = NULL;

     /* Validate input */
    if(pszList == NULL || pszSeparator == NULL)
    {
        TC_write_syslog( "ERROR: Missing input data: List passed or seprator is null\n");
    }
    else
    {
        /* Copying the input string to Null terminated string */
        LM5_cust_strcpy(pszTempList, pszList);

        /* Get the first token */
        pszToken = tc_strtok( pszTempList, pszSeparator );

        *pppszValuelist = (char **)MEM_alloc(sizeof(char *));
        /* Tokenize the input string */
        while( pszToken != NULL )
        {
            /* Copy the token to the output string pointer */
            LM5_cust_strcpy((*pppszValuelist)[iCounter],pszToken);

            /* Count the number of elements in the string */
            iCounter++;
                
            /* Allocating memory to pointer of string (output) as per the number of tokens found */
            *pppszValuelist = (char**) MEM_realloc((void *)*pppszValuelist, (sizeof(char*) * (iCounter+1)));

            /* Get next token: */
            pszToken = tc_strtok( NULL, pszSeparator );
        }

        (*iCount) = iCounter;
    }

    /* Free the temporary memory used for tokenizing */
    LM5_MEM_TCFREE(pszTempList);
    return iRetCode;
}


/**
 * This function returns the substring when provided with start and end position on the 
 * the input string. Substring is the character sequence that starts at character position 
 * 'iStrStartPosition' and ends at character position 'iStrEndPosition'
 *
 * For example If 'pszInputString' = 'Application'
 *                'iStrStartPosition' = 4
 *                'iStrEndPosition'   = 9
 *             then Output Will be: 'licati'
 *
 * Function will copy the characters starting 4th character to the 9th character of
 * input string.
 * 
 * @param[in]  pszInputString    Actual input string
 * @param[in]  iStrStartPosition Position of a character in the current string to be
 *                               used as starting character for the substring.
 * @param[in]  iStrEndPosition   Position of a character in the current string to be
 *                               used as last character for the substring.
 *
 * @param[out] ppszCopyedStr     Copied string.
 *
 * @retval INVALID_INPUT_TO_FUNCTION if input not correct
 * @retval ITK_ok on successful execution
 */
int LM5_str_copy_substring
(
    char*  pszInputString,
    int    iStrStartPosition,
    int    iStrEndPosition,
    char** ppszCopyedStr
)
{
    int  iRetCode    = ITK_ok;
    int  iStrLen     = 0;

    /* Initializing the Out Parameter to this function. */
    (*ppszCopyedStr) = NULL;

    iStrLen = (int) tc_strlen(pszInputString);
    
    if ((pszInputString != NULL) && (iStrStartPosition >= 0) && (iStrEndPosition <=  iStrLen) && 
        (iStrStartPosition <= iStrEndPosition))
    {
        if(tc_strcmp(pszInputString, "") != 0)
        {
            int idx        = 0;
            int iNewStrLen = 0;

            iNewStrLen = iStrEndPosition - iStrStartPosition + 2;

            (*ppszCopyedStr) = (char*) MEM_alloc(sizeof(char*) * iNewStrLen);

            while(iStrStartPosition <= iStrEndPosition)
            {
                (*ppszCopyedStr)[idx] = pszInputString[iStrStartPosition-1];
                idx++;
                iStrStartPosition++;
            }

            (*ppszCopyedStr)[idx] = '\0';
        }
    }
    else
    {
        iRetCode = INVALID_INPUT_TO_FUNCTION;
    }

    return iRetCode;
}


/**
 * Function checks if given string exists in string of array or not 
 *
 * @param[in]  pszStrVal     String to be looked into string array
 * @param[in]  iTotalStrVals String Array Size 
 * @param[in]  pppszStrVals  String Array
 *
 * @return ITK_ok if string is found in the array else return 1
 */
int LM5_contains_string_value
(
    char*  pszStrVal, 
    int    iTotalStrVals, 
    char** pppszStrVals)
{
    int iRetCode (ITK_ok);

    if( iTotalStrVals > 0 )
    {
         for ( int inx = 0; inx < iTotalStrVals; inx++ )
         {
             if(tc_strcmp( pszStrVal, pppszStrVals[ inx ]) == 0)
             {
                return iRetCode;
             }
         } 
        
    }

    return 1;
}




