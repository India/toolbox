/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_errors.hxx

    Description:  Header File for containing macros for custom errors

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-Sep-14    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifndef LM5_ERRORS_HXX
#define LM5_ERRORS_HXX

#include <common/emh_const.h>

#define ERROR_ALLOCATING_MEMORY                                     (EMH_USER_error_base + 4)
#define ERROR_CODE_NOT_STORED_ON_STACK                              (EMH_USER_error_base + 5)
#define INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME                   (EMH_USER_error_base + 6)
#define ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND                       (EMH_USER_error_base + 7)
#define UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES                     (EMH_USER_error_base + 8)
#define UNABLE_TO_ADD_STATUS_TO_BUSINESS_OBJ                        (EMH_USER_error_base + 9)
#define INVALID_RELATION_PASSED_TO_HANDLER                          (EMH_USER_error_base + 12)
#define INVALID_INPUT_TO_FUNCTION                                   (EMH_USER_error_base + 14)

#endif

