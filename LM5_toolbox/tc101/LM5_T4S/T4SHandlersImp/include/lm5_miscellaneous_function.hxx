/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_miscellaneous_function.hxx

    Description:  Header File for 'lm5_miscellaneous_function.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Oct-13     Tarun Singh          Initial Release

========================================================================================================================================================================*/
#ifndef LM5_MISCELLANEOUS_FUNCTION_HXX
#define LM5_MISCELLANEOUS_FUNCTION_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>

int LM5_set_attribute_val_for_objects
( 
    char*  pszPrefPrefix,
    int    iObjCnt,
    tag_t* ptObjects
);

int LM5_get_pref_containing_prop_and_value_list
(
    char*    pszPrefPrefix,
    tag_t    tObject, 
    char**   ppszPrefName
);

#endif
