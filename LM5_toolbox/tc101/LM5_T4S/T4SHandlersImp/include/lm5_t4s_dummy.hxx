/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_t4s_dummy.hxx

    Description:  Header File for lm5_t4s_dummy.cxx

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-Sep-14    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifndef LM5_T4S_DUMMY_HXX
#define LM5_T4S_DUMMY_HXX
    
#include <unidefs.h>
#include <tc/iman.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <tccore/item.h>
#include <tccore/aom_prop.h>
#include <fclasses/tc_string.h>
#include <time.h>
#include <constants/constants.h>
#include <ae/dataset.h>
#include <ae/datasettype.h>
#include <ae/tool.h>
#include <epm/epm.h>
#include <dispatcher/dispatcher_itk.h>

int LM5_T4S_transfer_MaterialMaster
(
    EPM_action_message_t msg
);

int LM5_T4S_transfer_ChangeNumber
(
    EPM_action_message_t msg
);

int LM5_T4S_transfer_BillOfMaterial
(
    EPM_action_message_t msg
);

int LM5_T4S_transfer_DocumentInfoRecord
(
    EPM_action_message_t msg
);

EPM_decision_t LM5_t4x_attach_logfiledataset_rh
(
	EPM_rule_message_t msg
);
#endif

