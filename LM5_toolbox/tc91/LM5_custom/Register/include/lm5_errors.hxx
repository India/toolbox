/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_errors.hxx

    Description:  Header File for containing macros for custom errors

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-Jan-12    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifndef LM5_ERRORS_HXX
#define LM5_ERRORS_HXX

#include <common/emh_const.h>

#define ERROR_ALLOCATING_MEMORY                                     (EMH_USER_error_base + 4)
#define ERROR_CODE_NOT_STORED_ON_STACK                              (EMH_USER_error_base + 5)
#define INSUFF_BUFF_SIZE_FOR_GETTING_CUR_DATETIME                   (EMH_USER_error_base + 6)
#define ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND                       (EMH_USER_error_base + 7)
#define UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES                     (EMH_USER_error_base + 8)
#define UNABLE_TO_ADD_STATUS_TO_BUSINESS_OBJ                        (EMH_USER_error_base + 9)
#define INVALID_RELATION_PASSED_TO_HANDLER                          (EMH_USER_error_base + 12)
#define INVALID_INPUT_TO_FUNCTION                                   (EMH_USER_error_base + 14)
#define WF_INITIATOR_IS_NOT_THE_OWNER_OF_ATTACHED_OBJECT            (EMH_USER_error_base + 15)
#define OBJECTS_ATTACHED_TO_FL_DOES_NOT_HAVE_VALID_STATUS           (EMH_USER_error_base + 16)
#define TARGET_DOES_NOT_MATCH_ALLOWED_STATUS_LIST                   (EMH_USER_error_base + 18)
#define TARGET_MATCH_DISALLOWED_STATUS_LIST                         (EMH_USER_error_base + 19)
#define OBJECT_ALREADY_PART_OF_ANOTHER_WORKFLOW                     (EMH_USER_error_base + 22)
#define ATTRIBUTE_VALUE_MISMATCH                                    (EMH_USER_error_base + 26)
#define INVALID_ARG_VAL_SUPPLIED_TO_WF_HANDLER                      (EMH_USER_error_base + 29)
#define ITEM_REV_WITH_THIS_PATTERN_CANNOT_BE_VERSIONED              (EMH_USER_error_base + 31)
#define INVALID_WORKFLOW_TEMPLATE_NAME                              (EMH_USER_error_base + 32)
#define OBJECT_DOES_NOT_HAVE_WRITE_PRIVILEGE                        (EMH_USER_error_base + 33)
#define OBJECT_IS_CHECKED_OUT                                       (EMH_USER_error_base + 34)

#endif


