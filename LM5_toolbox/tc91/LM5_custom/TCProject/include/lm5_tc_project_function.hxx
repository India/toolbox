/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_tc_project_function.hxx

    Description:  Header File for 'lm5_tc_project_function.cxx'

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Singh          Initial Release

========================================================================================================================================================================*/
#ifndef LM5_TC_PROJECT_FUNCTION_HXX
#define LM5_TC_PROJECT_FUNCTION_HXX

#include <tccore/license.h>

int LM5_assign_project
(
    EPM_action_message_t msg
);

int LM5_get_valid_and_related_obj_list_to_propagate_proj_info
(
    int           iObjCnt,
    tag_t*        ptPrimaryObj,
    int           iValidObjTypeCnt,
    char**        ppszValidObjType,
    int           iPropInfoCnt,
    sStringArray* psStringInfoList,
    int*          piRelatedObjCnt,
    tag_t**       pptRelatedObj
);


int LM5_propagate_security_info
(
    EPM_action_message_t msg
);

int LM5_get_security_info
(
    tag_t   tItemRev,
    int     iSecurityAttrCnt,
    char**  ppszSecurityAttr,
    int*    piLicCount,
    tag_t** pptLicense,
    int*    piProjectCount,
    tag_t** pptProjects,
    char**  ppszIPClassific
);

#endif
