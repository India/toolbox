/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_object_utilities.cxx

    Description:  This File contains custom functions for POM classes to perform generic operations. These functions are part of LM5 library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_errors.hxx>

/**
 * Get the release status string and tag of an object.
 *
 * @note  Do not add any custom error code to this function as it will
 *        impact functionality for LM5 321-3. We can use custom error
 *        in the calling function.
 *
 * Get the attribute length of "release_status_list" attribute of the object
 * length == 0? no status
 * else (length > 0)
 * get the tag of all the status from the "release_status_list"
 * get the name of the release status with the found tags.
 *
 * @param[in]  tObject             The object to get the release status from
 * @param[out] pppszStatusNameList Listof  status names
 * @param[out] piStatusCount       Status Count
 * @param[out] pptStatusTagList    Status list of tags.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_ask_status_list
(
    tag_t       tObject,
    char***     pppszStatusNameList,
    int*        piStatusCount,
    tag_t**     pptStatusTagList
)
{
    int   iRetCode       = ITK_ok;
    int   iNoOfStatuses  = 0;

    /* Initializing the Out Parameter to this function. */
    (*piStatusCount)    = 0;
    (*pptStatusTagList) = NULL;
    (*pppszStatusNameList) = NULL;

    LM5_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses, pptStatusTagList));
    
    for (int iDx = 0; iDx < iNoOfStatuses && iRetCode == ITK_ok; iDx++)
	{
        char szRelStatusType[WSO_name_size_c+1] = {""};
        
        LM5_ITKCALL(iRetCode, CR_ask_release_status_type((*pptStatusTagList)[iDx], szRelStatusType));

        if(iRetCode == ITK_ok)
		{
            LM5_CUST_UPDATE_STRING_ARRAY((*piStatusCount), (*pppszStatusNameList), szRelStatusType);               
        }            
    }
   
    return iRetCode;;
}

/**
 * This function get's the tag to status of input status type(pszStatusName) out 
 * of the list of status attached to input Workspace Object.
 *
 * @param[in]  tObject        The object to get the release status from
 * @param[in]  pszStatusName  Name of status whose tag is to be recovered
 * @param[out] ptStatusTag    Tag to status of input type
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_ask_status_tag
(
    tag_t      tObject,
    char*      pszStatusName,
    tag_t*     ptStatusTag
)
{
    int     iRetCode        = ITK_ok;
    int     iNoOfStatuses   = 0;
    tag_t   item_rev        = NULLTAG;
    tag_t*  ptStatusList    = NULL;
    logical bFound          = false;
    char    szReleStatusType[WSO_name_size_c+1] = {""};

    /* Initializing the Out Parameter to this function. */
    (*ptStatusTag) = NULLTAG;


     LM5_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses, &ptStatusList));
    
    for (int iDx = 0; iDx < iNoOfStatuses && iRetCode == ITK_ok && bFound == false; iDx++)
    {
        
        LM5_ITKCALL(iRetCode, CR_ask_release_status_type(ptStatusList[iDx], szReleStatusType));
        
        if(tc_strcmp(pszStatusName, szReleStatusType) == 0)
        {
              (*ptStatusTag) = ptStatusList[iDx];
              bFound = true;
        }
            
    }

    LM5_MEM_TCFREE(ptStatusList);
    return iRetCode;

}

/**
 * This function retrieves all the status attached to input business object and validate list of 
 * status of business object with input list of statuses. Function will return the tag to the first
 * valid status present of business object
 *
 * Example: Suppose input business object has following status in this sequence only.
 *            1. LM5_Archive
 *            2. LM5_Concept_Phase
 *            3. LM5_Design_Phase
 *          And the input status list to the function 'ppszStatusName' has following values
 *            1. LM5_Concept_Phase
 *            2. LM5_Paused
 *          Function will return the tag to 'LM5_Concept_Phase' and will confirm that valid status is present.
 *
 * @param[in]  tObject         The object to get the release status from
 * @param[in]  iValidStatus    Count of input status in the list
 * @param[in]  ppszStatusName  Status name list
 * @param[out] ptStatus        Tag to first valid status
 * @param[out] pbFound         Logical value to indicate if any valid status is present
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_is_any_input_status_attached
(
    tag_t      tObject,
    int        iValidStatus,
    char**     ppszStatusName,
    tag_t*     ptStatus,
    logical*   pbFound
)
{
    int    iRetCode       = ITK_ok;
    int    iNoOfStatuses  = 0;
    tag_t* ptStatusList   = NULL;
    char   szRelStatusType[WSO_name_size_c+1] = {""};

    /* Initializing the Out Parameter to this function. */
    (*ptStatus) = NULLTAG;
    (*pbFound)  = false;

    LM5_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses, &ptStatusList));
    
    for (int idx = 0; idx < iNoOfStatuses && iRetCode == ITK_ok && (*pbFound) == false; idx++)
    {
        LM5_ITKCALL(iRetCode, CR_ask_release_status_type(ptStatusList[idx], szRelStatusType));

        for(int iCount = 0; iCount<iValidStatus && (*pbFound) == false; iCount++)
        {
            if(tc_strcmp(ppszStatusName[iCount], szRelStatusType) == 0)
            {
                  (*ptStatus)  = ptStatusList[idx];
                  (*pbFound)   = true;
            }
        }   
    }
    
    LM5_MEM_TCFREE(ptStatusList);
    return iRetCode;
}

/**
 * This function retrieves all the status attached to input business object and validate list of 
 * status of business object with input list of statuses. Function will return the tag to the first
 * valid status present of business object
 *
 * Example: Suppose input business object has following status in this sequence only.
 *            1. LM5_Archive
 *            2. LM5_Concept_Phase
 *            3. LM5_Design_Phase
 *          And the input status list to the function 'ppszStatusName' has following values
 *            1. LM5_Concept_Phase
 *            2. LM5_Paused
 *          Function will return the tag to 'LM5_Concept_Phase' and will confirm that valid status is present.
 *
 * @note: Output Variable 'pbFound' is true if input object has any status attached to it i.e. it can be valid or invalid status
 *
 * @param[in]  tObject           The object to get the release status from
 * @param[in]  iValidStatus      Count of input status in the list
 * @param[in]  ppszStatusName    Status name list
 * @param[out] ptStatus          Tag to first valid status
 * @param[out] pbStatusAttached  Logical value to indicate if any status is present
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_is_any_input_status_attached_info
(
    tag_t      tObject,
    int        iValidStatus,
    char**     ppszStatusName,
    tag_t*     ptStatus,
    logical*   pbStatusAttached
)
{
    int     iRetCode            = ITK_ok;
    int     iNoOfStatuses       = 0;
    tag_t*  ptStatusList        = NULL;
    logical bFoundValidStatus   = false;

    /* Initializing the Out Parameter to this function. */
    (*ptStatus)         = NULLTAG;
    (*pbStatusAttached) = false;

    LM5_ITKCALL(iRetCode, WSOM_ask_release_status_list(tObject, &iNoOfStatuses, &ptStatusList));

    if(iNoOfStatuses > 0)
    {
        (*pbStatusAttached) = true;
    }
    
    for (int iDx = 0; iDx < iNoOfStatuses && iRetCode == ITK_ok && bFoundValidStatus == false; iDx++)
    {
        char szRelStatusType[WSO_name_size_c+1] = {""};

        LM5_ITKCALL(iRetCode, CR_ask_release_status_type(ptStatusList[iDx], szRelStatusType));

        for(int iCount = 0; iCount<iValidStatus && bFoundValidStatus == false; iCount++)
        {
            if(tc_strcmp(ppszStatusName[iCount], szRelStatusType) == 0)
            {
                  (*ptStatus)  = ptStatusList[iDx];
                  bFoundValidStatus = true;
            }
        }   
    }
     
    LM5_MEM_TCFREE(ptStatusList);
    return iRetCode;
}

/**
 * This function gets the name of type for the input business
 * objet.
 * e.g. Consider Item type 'P_KatalogteilRevision'. If we pass tag to
 *      an instance of 'P_KatalogteilRevision' this function will return string
 *      'P_KatalogteilRevision' and not 'ItemRevision'. Consider Dataset of type 'catia'.
 *      If we pass tag to an instance of 'catia' this function will
 *      return 'catia' and not 'Dataset'.
 *
 * @param[in]  tObject       Business Object tag
 * @param[out] ppszObjType   Object type
 *
 * @return ITK_ok is successful else ITK return code.
 */

int LM5_obj_ask_type
(
    tag_t       tObject,
    char**      ppszObjType
)
{
    int     iRetCode       = ITK_ok;
    tag_t   tObjectType    = NULLTAG;
    char    szObjectType[TCTYPE_name_size_c+1]  = {""};

    /* Initializing the Out Parameter to this function. */
    (*ppszObjType) = NULL;
    
    LM5_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject,&tObjectType));
    
    LM5_ITKCALL(iRetCode, TCTYPE_ask_name(tObjectType, szObjectType));
    
    if(iRetCode == ITK_ok)
    {
        LM5_CUST_STRCPY(*ppszObjType, szObjectType);
    }
    
    return iRetCode;
}

/**
 * This function gets the Display name of input business object Type.
 *
 * e.g. Consider if ItemRevision type 'P_KatalogteilRevision' is pass to this function then the Function 
 *      will return the display name of this Item type i.e. 'Catalog Part Revision'
 *
 * @param[in]  pszType             Business object data model Type Name
 * @param[out] ppszTypeDisplayName Business object display name
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_ask_type_display_name
(
    char*   pszType,
    char**  ppszTypeDisplayName
)
{
    int     iRetCode    = ITK_ok;
    tag_t   tTypeTag    = NULLTAG;

    /* Initializing the Out Parameter to this function. */
    (*ppszTypeDisplayName) = NULL;
    
    LM5_ITKCALL(iRetCode, TCTYPE_find_type(pszType, NULL, &tTypeTag));
    
    LM5_ITKCALL(iRetCode, TCTYPE_ask_display_name(tTypeTag, ppszTypeDisplayName));
   
    return iRetCode;
}

/**
 * This function gets the Parent Class of the input business objet.
 *
 * e.g. Consider Item type 'P_Katalogteil'. If we pass tag to an instance of 'P_Katalogteil' this function will return string
 *      'Item' and not 'P_Katalogteil'. Consider Dataset of type 'catia'.If we pass tag to an instance of 
 *      'Dataset'. 'Email' is a of type 'Document' and 'Document' is of type 'Item'. If we pass  tag to an instance of 'Email'
*        this function will return string 'Document'  not 'Item' or 'Email.
 *
 * @param[in] tObject               Business Object tag
 * @param[out] ppszObjParentTypeName Object Class Name
 * 
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_ask_parent_class
(
    tag_t   tObject,
    char**  ppszObjParentTypeName
)
{
    int    iRetCode      = ITK_ok;
    tag_t  tObjectType   = NULLTAG;
    tag_t  tParentType   = NULLTAG;
    char   szClassName[TCTYPE_class_name_size_c+1]  = {""};

    /* Initializing the Out Parameter to this function. */
    (*ppszObjParentTypeName) = NULL;

    LM5_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tObjectType));

    LM5_ITKCALL(iRetCode, TCTYPE_ask_parent_type(tObjectType, &tParentType));
        
    LM5_ITKCALL(iRetCode, TCTYPE_ask_class_name(tParentType, szClassName));
           
    if(iRetCode == ITK_ok)
    {
        LM5_CUST_STRCPY(*ppszObjParentTypeName, szClassName);
    }
  
    return iRetCode;
}


/**
 * Save and unlock the given object
 *
 * @param[in] tObject tag of object to be saved and unloaded
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_save_and_unlock
(
    tag_t       tObject
)
{
    int iRetCode  = ITK_ok;

    /*  Save the object.  */
    LM5_ITKCALL(iRetCode, AOM_save( tObject));
   
    /* Unlock the object. */
    LM5_ITKCALL(iRetCode, AOM_unlock( tObject));
    
    return iRetCode;

}

/**
 * Set the string attribute value.
 *
 * @param[in] tObject         Tag of object to be saved and unloaded
 * @param[in] pszPropertyName Name of the property
 * @param[in] pszPropVal      Value of the property to be set
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_set_str_attribute_val
(
    tag_t       tObject,
    char*       pszPropertyName,
    char*       pszPropVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    LM5_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute string value.
     */    
    LM5_ITKCALL(iRetCode, AOM_set_value_string(tObject, pszPropertyName, pszPropVal));

    /*  Save the object.  */
    LM5_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     LM5_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}

/**
 * Function to get the owning user of the object.
 * 
 * tObject[in]        tObject       Workspace object whose owning user is to be retrived.
 * ppszAttrValue[out] ppszAttrValue The Owning user name to be returned.
 * ppszAttrValue[out] ptOwningUser  Tag of Owning user. 
 * 
 * @retval iRetCode Status of execution of function.
 */
int LM5_obj_get_owning_user
(
    tag_t   tObject,
    char**  ppszAttrValue,
    tag_t*  ptOwningUser
)
{
    int   iRetCode            = ITK_ok;
    tag_t tOwningUser         = NULLTAG;
    char  szOwningUser[SA_person_name_size_c + 1] = {""};
    
    LM5_ITKCALL(iRetCode, AOM_ask_owner(tObject, ptOwningUser));
        
    LM5_ITKCALL(iRetCode, SA_ask_user_person_name(*ptOwningUser, szOwningUser));
    
    if(iRetCode == ITK_ok)
    {
        LM5_CUST_STRCPY(*ppszAttrValue, szOwningUser);
    }
    
    return iRetCode;
}

/**
 * Function to set reference(tag) value on object
 *
 * @param[in] tObject         Tag of object 
 * @param[in] pszPropertyName Name of the property
 * @param[in] tAttrVal        Value of the property to be set
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_set_tag_value
(
    tag_t       tObject,
    char*       pszPropertyName,
    tag_t       tAttrVal
)
{
    int iRetCode  = ITK_ok;

    /*  Lock the object.  */
    LM5_ITKCALL(iRetCode, AOM_refresh( tObject, true));
   
    /*
     * Set the attribute Tag value.
     */    
    LM5_ITKCALL(iRetCode, AOM_set_value_tag(tObject, pszPropertyName, tAttrVal));

    /*  Save the object.  */
    LM5_ITKCALL(iRetCode, AOM_save( tObject ));

    /*  Unlock the object.  */
     LM5_ITKCALL(iRetCode, AOM_refresh( tObject, false));

    return iRetCode;
}

/**
 * Function to set Boolean (logical) value on object. This function checks if the existing attribute value is not the same as
 * value that is supplied to this function to set. If so then the attribute value is not set.
 *
 * @param[in] tObject         Tag of object 
 * @param[in] pszPropertyName Name of the property
 * @param[in] bAttrVal        Logical Value of the property to be set
 *
 * @note The object must be an instance of Application Object class
 *       or a subclass of that.
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_obj_set_logical_value
(
    tag_t       tObject,
    char*       pszPropertyName,
    logical     bAttrVal
)
{
    int     iRetCode     = ITK_ok;
    logical bExistingVal = false;

    LM5_ITKCALL(iRetCode, AOM_ask_value_logical(tObject, pszPropertyName, &bExistingVal));

    if(bExistingVal != bAttrVal)
    {
        /*  Lock the object.  */
        LM5_ITKCALL(iRetCode, AOM_refresh( tObject, true));
       
        /*
         * Set the attribute Logical value.
         */    
        LM5_ITKCALL(iRetCode, AOM_set_value_logical(tObject, pszPropertyName, bAttrVal));

        /*  Save the object.  */
        LM5_ITKCALL(iRetCode, AOM_save( tObject ));

        /*  Unlock the object.  */
        LM5_ITKCALL(iRetCode, AOM_refresh( tObject, false));
    }

    return iRetCode;
}

/**
 * Function to initiate workflow with provided target object
 *
 * @param[in]  pszWorkflowTemplate  Workflow template name
 * @param[in]  tTargetObject        Target object
 * @return ITK_ok if no errors occurred, else ITK error code
 */
extern int LM5_initiate_workflow
(
    char* pszWorkflowTemplate, 
    tag_t tTargetObject
)
{
    int   iRetCode         = ITK_ok;
    tag_t tProcessTemplate = NULLTAG;
    tag_t tNewProcess      = NULLTAG;
    int   iAttType         = EPM_target_attachment;
    char* pszProcessName = NULL;

    LM5_ITKCALL(iRetCode, EPM_find_process_template( pszWorkflowTemplate, &tProcessTemplate ) );

    if( tProcessTemplate != NULLTAG )
    {
        LM5_ITKCALL(iRetCode, WSOM_ask_object_id_string(tTargetObject, &pszProcessName));
        LM5_ITKCALL(iRetCode, EPM_create_process(pszProcessName, "", tProcessTemplate, 1, 
            &tTargetObject, &iAttType, &tNewProcess));
        
        LM5_MEM_TCFREE( pszProcessName );
    }

    return iRetCode;
}

/**
 * Function sets the value of the given list of string attributes from the given object with given class
 *
 * @param[in] tObject         Object whose attributes are to be set.
 * @param[in] pszClassName     The name of class whose object is to be created.
 * @param[in] iAttrCount     Number of attributes to be set.
 * @param[in] pszAttrNames     List of attribute names
 * @param[in] pszAttrValues List of attribute values
 *
 * @note The number of names and values in the string lists must be same and equal to iAttrCount
 * @return ITK_ok is successful else ITK return code. 
 */
int LM5_obj_pom_set_multiple_str_attr_without_saving
(
    tag_t   tObject,
    char*   pszClassName,
    int     iAttrCount,
    char**  pszAttrNames,
    char**  pszAttrValues
)
{
    int iRetCode = ITK_ok;
 
    /* Now fill the instance with values for each field. */
    for(int iDx = 0; iDx < iAttrCount && iRetCode == ITK_ok; iDx++)
    {
        tag_t tAttr  = NULLTAG;
        
        LM5_ITKCALL(iRetCode, POM_attr_id_of_attr( pszAttrNames[iDx], pszClassName, &tAttr));

        LM5_ITKCALL(iRetCode, POM_set_attr_string( 1, &tObject, tAttr, pszAttrValues[iDx]));
    }
    
    return iRetCode;
}

/**
 * This function validates if input Object is one of the sub type of the type that is supplied as input
 * argument 'pszTypeName' or if input object type is of the same type that is supplied to this function
 *
 * @param[in]  tObject               Object Tag
 * @param[in]  pszTypeName           Name of the Type
 * @param[out] pbBelongsToInputClass Flag indicating whether 'tObject' is the same as 'pszTypeName'
 *                                   or is a subtype of 'pszTypeName'
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int LM5_obj_is_type_of
(
    tag_t    tObject,
    char*    pszTypeName,
    logical* pbBelongsToInputClass 
)
{
    int    iRetCode       = ITK_ok;  
    tag_t  tType          = NULLTAG;
    tag_t  tInputObjType  = NULLTAG;

    LM5_ITKCALL(iRetCode, TCTYPE_find_type(pszTypeName, NULL, &tType));

    LM5_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tInputObjType));

    LM5_ITKCALL(iRetCode, TCTYPE_is_type_of(tInputObjType, tType, pbBelongsToInputClass));
    
    return iRetCode;
}


/**
 * This function validated if input object has write access and object is not checked-out. If object does not full fill the validation criteria
 * then function will return the modifiable information as 'false'
 * 
 * @param[in]   tObject              Tag of Object to be validated
 * @param[out]  pbWriteAccessDenied  'true' if Object has write access  
 * @param[out]  pbObjCheckedOut      'true' if Object is not checked out
 * @param[out]  pbModifiable         'true' if object is Modifiable i.e. Object has write access and is not checked-out
 *
 * @return ITK_ok if successful else ITK return code.
 */
int LM5_check_privilege
(
    tag_t    tObject,
    logical* pbWriteAccessDenied,
    logical* pbObjCheckedOut,
    logical* pbModifiable
)
{
    int     iRetCode    = ITK_ok;
    logical bAccess     = false;

    (*pbModifiable)        = false;
    (*pbWriteAccessDenied) = false;
    (*pbObjCheckedOut)     = false;

    LM5_ITKCALL(iRetCode, RES_assert_object_modifiable (tObject));

    LM5_ITKCALL(iRetCode, AM_check_privilege (tObject, LM5_WRITE_ACCESS, &bAccess));

    if(bAccess == false)
    {
        (*pbWriteAccessDenied) = true;
        (*pbModifiable) = false;
    }
    else
    {
        logical bCheckedOut = false;

        LM5_ITKCALL(iRetCode, RES_is_checked_out (tObject, &bCheckedOut));

        if(bCheckedOut == false)
        {
            (*pbModifiable)    = true;
        }
        else
        {
            char* pszUser          = NULL;
            tag_t tReservationObj  = NULLTAG;
            tag_t tCheckOutUser    = NULLTAG;
            tag_t tUser            = NULLTAG;

            LM5_ITKCALL(iRetCode, RES_ask_reservation_object (tObject, &tReservationObj));

            LM5_ITKCALL(iRetCode, RES_ask_user (tReservationObj, &tCheckOutUser));

            LM5_ITKCALL(iRetCode, POM_get_user (&pszUser, &tUser));

            if ((tUser != NULLTAG) &&    (tUser == tCheckOutUser))
            {
                char* pszObjAttrVal = NULL;

                LM5_ITKCALL(iRetCode, AOM_ask_value_string(tObject, LM5_ATTR_OBJECT_STRING_ATTRIBUTE, &pszObjAttrVal));

                TC_write_syslog ("%s object is checked-out by same user\n", pszObjAttrVal);
                (*pbModifiable) = true;
                LM5_MEM_TCFREE(pszObjAttrVal);
            } 
            else 
            {
                (*pbModifiable)    = false;
                (*pbObjCheckedOut) = true;
            }

            LM5_MEM_TCFREE (pszUser);
        }
    }

    return iRetCode;
}

/**
 * This function assigns the projects to the input list of Objects. If function find the object of class
 * Item revision then the projects are assigned to Item instead of Item revision. If project is assigned to Item 
 * then project will be assigned to Item revision and its related object based on configured propagation rule.
 *
 * @param[in] iObjCnt    Object Count
 * @param[in] ptObjects  Object tags to which project will be assigned 
 * @param[in] iProjCnt   Project Count
 * @param[in] ptProjects Tags of project to be assigned
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LM5_obj_assign_project
(
    int    iObjCnt,
    tag_t* ptObjects,
    int    iProjCnt,
    tag_t* ptProjects
)
{
    int    iRetCode   = ITK_ok;

    if(iObjCnt > 0 && iProjCnt > 0)
    {
        int    iObjWithOutPrjCnt = 0;
        tag_t* ptObjWithOutPrj   = NULL;

        for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
        {
            logical bISTypeOfIR = false;

            LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(ptObjects[iDx], LM5_CLASS_ITEMREVISION, &bISTypeOfIR));

            if(iRetCode == ITK_ok)
            {
                iObjWithOutPrjCnt++;

                LM5_ALLOCATE_TAG_MEM(iRetCode, ptObjWithOutPrj, iObjWithOutPrjCnt);

                /* If object type is Item Revision the get the item of that revision and assign project to it.*/
                if( bISTypeOfIR == true )
                {
                    tag_t  tItem = NULLTAG;

                    LM5_ITKCALL(iRetCode, ITEM_ask_item_of_rev (ptObjects[iDx], &tItem )); 
                    
                    ptObjWithOutPrj[iObjWithOutPrjCnt-1] = tItem;
                }
                else
                {
                    ptObjWithOutPrj[iObjWithOutPrjCnt-1] = ptObjects[iDx];
                }
            }
        }

        if(iObjWithOutPrjCnt > 0)
        {
            /*  Assign Project to contents of 'LM5_Project_Phase_Folder' */
            LM5_ITKCALL(iRetCode, PROJ_assign_objects ( iProjCnt, ptProjects, iObjWithOutPrjCnt, ptObjWithOutPrj));
        }

        LM5_MEM_TCFREE(ptObjWithOutPrj);
    }

    return iRetCode;
}

/**
 * This function loads the objects related to input object 'tObject'. Related objects can be primary or secondary objects
 * linked to input object. By providing appropriate arguments to this function we can load the desired related objects.
 *
 * @param[in]  tObject              Tag of Item revision
 * @param[in]  pszRelationTypeName  Relation Type. NULLTAG means all relation types.
 * @param[in]  pszObjType           Specific type of object which is related to input object 'tObject'.
 *                                  NULL means any type of related object.
 * @param[in]  pszExcludeStatus     Related objects with this status will be excluded from valid related objects list
 * @param[in]  bGetPrimary          'true' of Primary related objects needs to be retrieved from input objects else  
 *                                  secondary objects will be retrieved. 
 *
 * @return  ITK_ok on successful execution, else returns ITK  error code.
 */
int LM5_obj_load_related
(
    tag_t    tObject,
    char*    pszRelationTypeName,
    char*    pszObjType,
    char*    pszExcludeStatus,
    logical  bGetPrimary
)
{
    int    iRetCode           = ITK_ok;
    int    iCount             = 0;
    tag_t* ptValidRelatedObj  = NULL;

    LM5_ITKCALL(iRetCode, LM5_grm_load_and_get_related_obj(tObject, pszRelationTypeName, pszObjType, pszExcludeStatus, bGetPrimary,
                                                                &ptValidRelatedObj, &iCount));

    if(iCount > 0)
    {
        int    iLoadedObjCnt = 0;
        tag_t* ptLoadedObj   = NULL;

        LM5_ITKCALL(iRetCode, AOM_load_instances(iCount, ptValidRelatedObj, &iLoadedObjCnt, &ptLoadedObj));
       
        LM5_MEM_TCFREE(ptLoadedObj);
    }

    LM5_MEM_TCFREE(ptValidRelatedObj);
    return iRetCode;
}


/**
 * This function sets UOM attribute value for input Item. 
 *
 * @param[in]  tObject   Object Tag
 * @param[in]  tUOM      Tag of UOM to be set
 *
 * @return ITK_ok is successful else ITK return code. 
 */
int LM5_obj_set_uom
(
    tag_t tObject,
    tag_t tUOM
)
{
    int    iRetCode       = ITK_ok;  
    logical bIsTypeOfItem = false;           

    LM5_ITKCALL(iRetCode, LM5_obj_is_type_of(tObject, LM5_CLASS_ITEM, &bIsTypeOfItem));

    if(bIsTypeOfItem == true)
    {
        tag_t tType        = NULLTAG;
        tag_t tUOMAttrID   = NULLTAG;
        char  szClassName[TCTYPE_class_name_size_c+1] = {""};

        LM5_ITKCALL(iRetCode, TCTYPE_ask_object_type(tObject, &tType));

        LM5_ITKCALL(iRetCode, TCTYPE_ask_class_name(tType, szClassName));

        /* Get attribute tags for UOM of  Item class.*/
        LM5_ITKCALL(iRetCode, POM_attr_id_of_attr(LM5_ATTR_UOM_TAG, szClassName, &tUOMAttrID)); 

        LM5_ITKCALL(iRetCode, AOM_refresh(tObject, true));

        LM5_ITKCALL(iRetCode, POM_set_attr_tag( 1, &tObject, tUOMAttrID, tUOM));
        
        LM5_ITKCALL(iRetCode, AOM_save(tObject));

        LM5_ITKCALL(iRetCode, AOM_refresh(tObject, false));
    }

    return iRetCode;
}


#ifdef __cplusplus
}
#endif


