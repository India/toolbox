/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_item_itemrev_utilities.cxx

    Description: This File contains custom functions for Item & Item Revision to perform generic operations. These functions are part of SMA library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Sep-14    Tarun Singh          Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_errors.hxx>


/**
 * Function gets the previous Revision tag for the Input Item Revision. If input Item Revision is the 
 * only revision of the Item then function will return 'pbIsOnlyRevision' as 'true'.
 *
 * @note: Suppose the input revision is revision 'A' but the Item of input revision has more than one
 *        revision then this function will return 'pbIsFirstRevision' as 'true'.
 *
 * @param[in]  tItemRev           Tag to Item Revision .
 * @param[out] ptPreviousItemRev  Tag to Previous Item Revision
 * @param[out] pbIsOnlyRevision   'true' if input Item Revision is the only revision of Item
 * @param[out] pbIsFirstRevision  'true' if input Item Revision is the First revision for the Item
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int LM5_itemrev_get_previous_rev
(
    tag_t     tItemRev,
    tag_t*    ptPreviousItemRev,
    logical*  pbIsOnlyRevision,
    logical*  pbIsFirstRevision
)
{
    int     iRetCode                 = ITK_ok;
    int     iRevCount                = 0;
    tag_t   tItem                    = NULLTAG;
    tag_t   tTempPreviousIR          = NULLTAG;
    date_t  CreationDate             = POM_null_date();
    date_t  PreviousIRCreationDate   = POM_null_date();
    tag_t*  ptItemRevList            = NULL;

    /* Initializing the Out Parameter to this function. */
    (*ptPreviousItemRev)  = NULLTAG;
    (*pbIsOnlyRevision)   = false;
    (*pbIsFirstRevision)  = false;
   
    LM5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));

    LM5_ITKCALL(iRetCode, ITEM_list_all_revs(tItem, &iRevCount, &ptItemRevList));

    LM5_ITKCALL(iRetCode, AOM_ask_value_date(tItemRev, LM5_ATTR_OBJECT_CREATION_DATE, &CreationDate));

    if(iRevCount > 1)
    {
        int  iValidItemRev  = 0;
        (*pbIsOnlyRevision) = false;


        for(int iDx = 0; iDx < iRevCount && iRetCode == ITK_ok; iDx++)
        {
            int    iAnswer              = 0;
            date_t TempCreationDate    = POM_null_date();


            LM5_ITKCALL(iRetCode, AOM_ask_value_date(ptItemRevList[iDx], LM5_ATTR_OBJECT_CREATION_DATE,
                                                      &TempCreationDate));

            /* Compares two dates for equality. Function result is returned in 'iAnswer' as
               0 if dates are equal 
               1 if date1 is later than date2 (date1 > date2)
               -1 if date1 is earlier than date2 (date1 < date2)
            */  
            LM5_ITKCALL(iRetCode, POM_compare_dates(CreationDate, TempCreationDate,
                                                     &iAnswer));

            if(iRetCode == ITK_ok && iAnswer == 1)
            {
                if(iValidItemRev == 0)
                {
                    PreviousIRCreationDate = TempCreationDate;
                    tTempPreviousIR = ptItemRevList[iDx];
                }
                else
                {
                    int  iResult  = 0;

                    LM5_ITKCALL(iRetCode, POM_compare_dates(PreviousIRCreationDate, TempCreationDate,
                                                              &iResult));

                    if(iRetCode == ITK_ok && iResult == -1)
                    {
                        PreviousIRCreationDate = TempCreationDate;
                        tTempPreviousIR = ptItemRevList[iDx];
                    }
                }

                iValidItemRev++;
            }
        }

        if(iValidItemRev == 0)
        {
            (*pbIsFirstRevision)  = true;
        }
        else
        {
            (*ptPreviousItemRev) = tTempPreviousIR;
        }
    }
    else
    {
        (*pbIsOnlyRevision) = true;
    }

    LM5_MEM_TCFREE(ptItemRevList);
    return iRetCode;
}


/**
 * Function gets all Item Revisions that have creation date older to Input Item Revision. If input Item Revision is the 
 * only revision of the Item then function will return 'pbIsOnlyRevision' as 'true'.
 *
 * @note: Suppose the input revision is revision 'A' but the Item of input revision has more than one
 *        revision then this function will return 'pbIsFirstRevision' as 'true'.
 *
 * @param[in]  tItemRev           Tag to Item Revision .
 * @param[out] piPrevIRCnt        List Of Previous Item Revisions
 * @param[out] pptPreviousIR      Tag to Previous Item Revision
 * @param[out] pbIsOnlyRevision   'true' if input Item Revision is the only revision of Item
 * @param[out] pbIsFirstRevision  'true' if input Item Revision is the First revision for the Item
 *
 * @retval ITK_ok on successful execution of function, ITK error code otherwise
 */
int LM5_itemrev_get_all_previous_rev
(
    tag_t     tItemRev,
    int*      piPrevIRCnt,
    tag_t**   pptPreviousIR,
    logical*  pbIsOnlyRevision,
    logical*  pbIsFirstRevision
)
{
    int     iRetCode                 = ITK_ok;
    int     iRevCount                = 0;
    int     iPrevRevCnt              = 0;
    tag_t   tItem                    = NULLTAG;
    tag_t   tTempPreviousIR          = NULLTAG;
    date_t  CreationDate             = POM_null_date();
    tag_t*  ptItemRevList            = NULL;

    /* Initializing the Out Parameter to this function. */
    (*piPrevIRCnt)        = 0;
    (*pptPreviousIR)      = NULL;
    (*pbIsOnlyRevision)   = false;
    (*pbIsFirstRevision)  = false;
   
    LM5_ITKCALL(iRetCode, ITEM_ask_item_of_rev(tItemRev, &tItem));

    LM5_ITKCALL(iRetCode, ITEM_list_all_revs(tItem, &iRevCount, &ptItemRevList));

    LM5_ITKCALL(iRetCode, AOM_ask_value_date(tItemRev, LM5_ATTR_OBJECT_CREATION_DATE, &CreationDate));

    if(iRevCount > 1)
    {
        (*pbIsOnlyRevision) = false;


        for(int iDx = 0; iDx < iRevCount && iRetCode == ITK_ok; iDx++)
        {
            int    iAnswer              = 0;
            date_t TempCreationDate    = POM_null_date();


            LM5_ITKCALL(iRetCode, AOM_ask_value_date(ptItemRevList[iDx], LM5_ATTR_OBJECT_CREATION_DATE,
                                                      &TempCreationDate));

            /* Compares two dates for equality. Function result is returned in 'iAnswer' as
               0 if dates are equal 
               1 if date1 is later than date2 (date1 > date2)
               -1 if date1 is earlier than date2 (date1 < date2)
            */  
            LM5_ITKCALL(iRetCode, POM_compare_dates(CreationDate, TempCreationDate, &iAnswer));

            if(iRetCode == ITK_ok && iAnswer == 1)
            {
                LM5_UPDATE_TAG_ARRAY(iPrevRevCnt, (*pptPreviousIR), ptItemRevList[iDx]);
            }
        }

        if(iPrevRevCnt == 0)
        {
            (*pbIsFirstRevision)  = true;
        }
        else
        {
            (*piPrevIRCnt) = iPrevRevCnt;
        }
    }
    else
    {
        (*pbIsOnlyRevision) = true;
    }

    LM5_MEM_TCFREE(ptItemRevList);
    return iRetCode;
}


/**
 * Construct Item Revision name in the given below pattern.
 *  'item_id'/'item_reision_id';'object_name'
 *
 * @param[in] tItemRev     Tag of Item Revision
 * @param[out] ppszName Value of the property to be set
 *
 * @return ITK_ok is successful else ITK return code.
 */
int LM5_itemrev_construct_name
(
    tag_t   tItemRev,
    char**  ppszName
)
{
    int    iRetCode       = ITK_ok;
    int    iLen           = 0;
    char*  pszItemID      = NULL;
    char*  pszItemRevID   = NULL;
    char*  pszObjName     = NULL;
           
    /* Initializing the Out Parameter to this function. */
    (*ppszName)  = NULL;

    LM5_ITKCALL(iRetCode, AOM_ask_value_string(tItemRev, LM5_ATTR_ITEM_ID, &pszItemID));

    LM5_ITKCALL(iRetCode, AOM_ask_value_string(tItemRev, LM5_ATTR_ITEM_REVISION_ID, &pszItemRevID));

    LM5_ITKCALL(iRetCode, AOM_ask_value_string(tItemRev, LM5_ATTR_OBJECT_NAME_ATTRIBUTE, &pszObjName));

    if(iRetCode == ITK_ok)
    {
        LM5_CUST_STRCPY(*ppszName, pszItemID);
        LM5_CUST_STRCAT(*ppszName, LM5_STRING_FORWARD_SLASH);
        LM5_CUST_STRCAT(*ppszName, pszItemRevID);
        LM5_CUST_STRCAT(*ppszName, LM5_STRING_SEMICOLON);
        LM5_CUST_STRCAT(*ppszName, pszObjName);  
    }
       
    LM5_MEM_TCFREE(pszItemID);
    LM5_MEM_TCFREE(pszItemRevID);
    LM5_MEM_TCFREE(pszObjName);
    return iRetCode;
}


#ifdef __cplusplus
}
#endif