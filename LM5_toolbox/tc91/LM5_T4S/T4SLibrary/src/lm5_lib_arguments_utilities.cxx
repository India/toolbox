/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_arguments_utilities.cxx

    Description:  This File contains functions for string parsing operations and functions for retrieving arguments passed in workflow action or rule handlers
                  These functions are part of LM5 library.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
24-Oct-13     Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_const.hxx>
#include <lm5_lib_library.hxx>
#include <lm5_errors.hxx>

#ifdef __cplusplus
}
#endif

/**
 * This function loops through the arguments given in a handler
 * definition list and searches for the given switch. It gives
 * back a list with values of the switch. \n
 * Depending on the lOptional the argument must be present in
 * the definition of the handler or not.
 *
 * @note Example: -type=item;document \n
 *       where -type is the switch and \n
 *       item and document are the two values which appear in the array.
 *
 * @note The separator for multiple values can be semi-colon (;) or coma(,). 
 *       This is generic function and takes input as character separator 
 *       on that basis function will process the value list.
 *       
 * @param[in]  pArgList           Argument list provided by the handler msg structure
 * @param[in]  tTask              Task tag of the workflow task
 * @param[in]  pszArgument        Name of the argument to be used
 * @param[in]  pszSep             Delimiter
 * @param[in]  bOptional          If FALSE the argument is required, else optional
 * @param[out] piValCount         Number of values returned
 * @param[out] pppszArgValue      Array of values returned
 * @param[out] ppszOriginalArgVal Value of the argument with separator as passed in the workflow 
 * @retval ITK_ok is successful else ITK return code.
 */
int LM5_arg_get_arguments
(
    TC_argument_list_t*   pArgList,
    tag_t                 tTask,
    const char*           pszArgument,
    char*                 pszSep,
    logical               bOptional,
    int*                  piValCount,
    char***               pppszArgValue,
    char**                ppszOriginalArgVal
)
{
    /* Use TC_init_argument_list and TC_number_of_arguments to get the argument list
       find the right argument using ITK_ask_argument_named_value
       if found, split the value to an array of values using the separator
    */
    
    int  iRetCode                = 0;
    int  iIterator               = 0;
    int  iArgCount               = 0;
    char* pszSwitch              = NULL;
    char* pszSValue              = NULL;
    char* pszNextArgument        = NULL;
    logical bFoundArg            = false;

    /* Initializing the Out Parameter to this function. */
    (*pppszArgValue) = NULL;
    (*piValCount)   = 0;
    (*ppszOriginalArgVal) = NULL;

    /* Validate input */
    if((pszArgument == NULL) || (pszSep == NULL))
    {
        TC_write_syslog( "ERROR: Missing input data:Name of the argument or seprator is null\n");
    }
    else
    {
        TC_init_argument_list(pArgList);
        
        iArgCount = TC_number_of_arguments(pArgList);

        for(iIterator = 0; iIterator < iArgCount && iRetCode == ITK_ok; iIterator++)
        {
            pszNextArgument = TC_next_argument(pArgList);

            LM5_ITKCALL(iRetCode, ITK_ask_argument_named_value(pszNextArgument, &pszSwitch, &pszSValue));
            
            if(tc_strcmp(pszSwitch, pszArgument) == 0)
            {
                int iLen  = 0;
                
                iLen = (int)tc_strlen(pszSValue);

                (*ppszOriginalArgVal) = (char*) MEM_alloc(sizeof(char)*(iLen +1));

                if(iLen > 0)
                {
                    tc_strcpy((*ppszOriginalArgVal), pszSValue);
                }

                LM5_ITKCALL(iRetCode, LM5_str_parse_string(pszSValue, pszSep, piValCount, pppszArgValue));
                if(iRetCode != ITK_ok)
                {
                    /*Store error into error stack */
                    iRetCode = UNABLE_TO_PARSE_WORKFLOW_HANDLER_VALUES;
                    EMH_store_error(EMH_severity_error, iRetCode);
                    break;
                }
                else
                {
                    bFoundArg = true;
                    break;
                }
            }
            
            LM5_MEM_TCFREE(pszSwitch);
            LM5_MEM_TCFREE(pszSValue);
        }

        /*
         * When the requested argument is not found: If lOptional flag is set to TRUE, Do Nothing.
         * Else If lOptional flag is set to FALSE. Throw an Error.
         */
        if(bOptional == false && bFoundArg == false)
        {
            iRetCode = ERROR_REQUIRED_ARGUMENT_WAS_NOT_FOUND;
            EMH_store_error(EMH_severity_error, iRetCode);
        }
    }

    LM5_MEM_TCFREE(pszSwitch);
    LM5_MEM_TCFREE(pszSValue);
    return iRetCode;
}


/**
 * This function retrieves tags of the attachments to the workflow whose types are 
 * one of the input Types which is given as an input parameter.
 *
 * @param[in]  task               Tag of the task on which action is triggered
 * @param[in]  iAttachmentType    Determines of the Valid objects needs to be retrieved from 
 *                                target or Reference of the workflow. Valid values of this parameter are
 *                                'EPM_reference_attachment' or 'EPM_target_attachment'
 * @param[in]  ppszObjTypeList    List of Type of the Object according to which attachment is retrieved.
 * @param[in]  iTypeCnt           Count of input types.
 * @param[out] piCount            Number of attachment that are retrieved.
 * @param[out] pptObject          Array of attachment tags.
 * 
 * @retval ITK_ok is always returned.
 */
int LM5_arg_get_attached_obj_of_input_type_list
(
    tag_t   task,
    int     iAttachmentType,
    char**  ppszObjTypeList,
    int     iTypeCnt,
    int*    piCount,
    tag_t** pptObject
)
{
    int  iRetCode     = ITK_ok;
    
    /* Initializing the Out Parameter to this function. */
    (*pptObject) = NULL;
    (*piCount)   = 0;

    /* Validate input */
    if(ppszObjTypeList != NULL && iTypeCnt > 0)
    {
        int     iTargetCount   = 0;
        int     iIterator      = 0;
        int     iObjCount      = 0;
        tag_t   tRootTask      = NULLTAG;
        tag_t*  ptTargets      = NULL;

        LM5_ITKCALL(iRetCode, EPM_ask_root_task(task, &tRootTask));
          
        /* Get the target objects. */
        LM5_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, iAttachmentType, &iTargetCount, &ptTargets));
        
        /* For each target, check if it is an Item Revision. If so, try to get the children.
           Check the status of the returned Item Revision and compare themn with the given list.
        */
        for (iIterator = 0; iIterator < iTargetCount && iRetCode == ITK_ok; iIterator++)
        {
            for(int idx = 0; idx < iTypeCnt && iRetCode == ITK_ok; idx++)
            {
                char* pszObjType = NULL;
            
                LM5_ITKCALL(iRetCode, LM5_obj_ask_type(ptTargets[iIterator], &pszObjType));
                        
                if (tc_strcmp(ppszObjTypeList[idx], pszObjType) == 0)
                { 
                    iObjCount++;

                    LM5_ALLOCATE_TAG_MEM(iRetCode, (*pptObject), iObjCount);

                    (*pptObject )[iObjCount-1] = ptTargets[iIterator];
                }

                LM5_MEM_TCFREE(pszObjType);
            }
        }

        (*piCount) = iObjCount;
        LM5_MEM_TCFREE(ptTargets); 
    }

    return iRetCode;
}



