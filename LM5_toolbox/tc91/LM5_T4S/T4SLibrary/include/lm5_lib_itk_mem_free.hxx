/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_lib_itk_mem_free.hxx

    Description:  This file contains macros for freeing portion of memory allocated with TCe 'MEM_' functions and by TCe itself.
========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
20-June-12    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/
#ifndef LM5_LIB_ITK_MEM_FREE_HXX
#define LM5_LIB_ITK_MEM_FREE_HXX

#include <stdlib.h>
#include <stdio.h>
#include <unidefs.h>
#include <tc/iman.h>
#include <ict/ict_userservice.h>
#include <tccore/custom.h>
#include <tccore/method.h>
#include <base_utils/Mem.h>
#include <tc/preferences.h>
#include <tccore/item_msg.h>
#include <epm/releasestatus.h>
#include <tccore/tctype.h>
#include <tccore/aom.h>
#include <ae/dataset_msg.h>
#include <tccore/grm.h>
#include <tccore/tc_msg.h>
#include <LM5_lib_library.hxx>

// We want to use our macros. If some others are defined under same
// name, undefine them

#ifdef LM5_MEM_TCFREE
#undef LM5_MEM_TCFREE
#endif

// Macro to free memory allocated using ITK function MEM_alloc or MEM_realloc

#define LM5_MEM_TCFREE(pMem) {\
           if (pMem != NULL) {\
               MEM_free(pMem);\
               pMem = NULL;\
              }\
          }


#ifdef LM5_ALLOCATE_TAG_MEM
#undef LM5_ALLOCATE_TAG_MEM
#endif
/* Macro to Allocating memory using ITK function MEM_alloc or MEM_realloc. 
   IMPORTANT: Always initialize the pointer to NULL if memory allocation if done for the very first time
*/
#define LM5_ALLOCATE_TAG_MEM(iRetCode, ptMem, iSize){\
            if(iSize > 0){\
                   if (ptMem == NULL || iSize == 1){\
                        ptMem = (tag_t*) MEM_alloc((int)(sizeof(tag_t)*iSize));}\
                   else{\
                        ptMem = (tag_t*) MEM_realloc(ptMem, (int)((sizeof(tag_t)*iSize)));}\
            }\
            else{\
                iRetCode = INVALID_INPUT_TO_FUNCTION;\
                EMH_store_error(EMH_severity_error, iRetCode);}\
}

#endif








