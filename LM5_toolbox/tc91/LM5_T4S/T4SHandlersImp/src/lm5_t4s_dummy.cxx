/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_t4s_dummy.cxx

    Description: 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-Sep-14    Tarun Kumar Singh      Changes Made to existing functions and new functions added.

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_action_handlers.hxx>
#include <lm5_errors.hxx>
#include <lm5_t4s_dummy.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_lib_library.hxx>
#include <lm5_miscellaneous_function.hxx>

#ifdef __cplusplus
}
#endif

/**
 * 
 *
 *  
 */
int LM5_T4S_transfer_MaterialMaster
(
    EPM_action_message_t msg
)
{
    int    iRetCode           = ITK_ok;
    int    iObjectCount       = 0;
    tag_t  tRootTask          = NULLTAG;
    tag_t* ptObject           = NULL;

    TC_write_syslog("T4S-transfer-MaterialMaster: Handler invoked from Custom DLL LM5_T4S.\n");  
    
    /* Get the target attachments   */
    LM5_ITKCALL(iRetCode, EPM_ask_root_task( msg.task, &tRootTask));

    /* Get the target objects. */
    LM5_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, EPM_target_attachment, &iObjectCount, &ptObject));

    LM5_ITKCALL(iRetCode, LM5_set_attribute_val_for_objects(__FUNCTION__, iObjectCount, ptObject));

    LM5_MEM_TCFREE(ptObject);
    return iRetCode;
}


/**
 * 
 *
 *  
 */

int LM5_T4S_transfer_ChangeNumber
(
    EPM_action_message_t msg
)
{
    int    iRetCode           = ITK_ok;
    int    iNoOfObjType       = 0;
    int    iObjectCount       = 0;
    tag_t  tRootTask          = NULLTAG;
    tag_t* ptObject           = NULL;

    TC_write_syslog("T4S-transfer-ChangeNumber: Handler invoked from Custom DLL LM5_T4S.\n");  
    
    /* Get the target attachments   */
    LM5_ITKCALL(iRetCode, EPM_ask_root_task( msg.task, &tRootTask));

    /* Get the target objects. */
    LM5_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, EPM_target_attachment, &iObjectCount, &ptObject));

    LM5_ITKCALL(iRetCode, LM5_set_attribute_val_for_objects(__FUNCTION__, iObjectCount, ptObject));

    LM5_MEM_TCFREE(ptObject);
    return iRetCode;
}

/**
 * 
 *
 *  
 */
int LM5_T4S_transfer_BillOfMaterial
(
    EPM_action_message_t msg
)
{
    int    iRetCode           = ITK_ok;
    int    iNoOfObjType       = 0;
    int    iObjectCount       = 0;
    tag_t  tRootTask          = NULLTAG;
    tag_t* ptObject           = NULL;

    TC_write_syslog("T4S-transfer-BillOfMaterial: Handler invoked from Custom DLL LM5_T4S.\n"); 
    
    /* Get the target attachments   */
    LM5_ITKCALL(iRetCode, EPM_ask_root_task( msg.task, &tRootTask));

    /* Get the target objects. */
    LM5_ITKCALL(iRetCode, EPM_ask_attachments(tRootTask, EPM_target_attachment, &iObjectCount, &ptObject));

    LM5_ITKCALL(iRetCode, LM5_set_attribute_val_for_objects(__FUNCTION__, iObjectCount, ptObject));

    LM5_MEM_TCFREE(ptObject);
    return iRetCode;
}

/**
 * 
 *
 *  
 */
EPM_decision_t LM5_t4x_attach_logfiledataset_rh
(
	EPM_rule_message_t msg
)
{
    int             iRetCode   = ITK_ok;
	EPM_decision_t  decision   = EPM_go;

    TC_write_syslog("T4X-attach-LogfileDataset: Handler invoked from Custom DLL LM5_T4S.\n");
    
    return decision;
}

