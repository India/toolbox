/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_miscellaneous_function.cxx

    Description: This File contains custom miscellaneous functions which are used in various operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
30-Oct-13     Tarun Singh          Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_lib_const.hxx>
#include <lm5_errors.hxx>
#include <lm5_miscellaneous_function.hxx>

#ifdef __cplusplus
}
#endif

/**
 * This function sets the properties with respective value for input list of objects. The property and value list is store in the
 * preference. Name of property and its respective value is separated by delimiter ':'
 * The name follows the pattern 
 *          LM5_<T4S Handler name with underscore instead of dash>_<OBJECT_TYPE>_Set_Properties
 * For e.g.
 *  Name Of Preference: 
 *                          LM5_T4S_transfer_BillOfMaterial_P_SMAspezTeilRevision_Set_Properties
 *  Values of Preference:
 *                          LM5_MMSENTTOSAP:Success
 *                          LM5_EPLAN_EXISTS:true
 *                          LM5_GEWBRUTTO:9.01
 *
 * @param[in] pszPrefPrefix   Prefix that will be used in constructing the Preference name
 *                            which contains property and value list
 * @param[in] iObjCnt         Count of object
 * @param[in] ptObjects       Tags of Objects whose properties will be set based on preference.
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LM5_set_attribute_val_for_objects
( 
    char*  pszPrefPrefix,
    int    iObjCnt,
    tag_t* ptObjects
)
{
    int iRetCode     = ITK_ok;

    for(int iDx = 0; iDx < iObjCnt && iRetCode == ITK_ok; iDx++)
    {
        char*  pszPrefName = NULL;

        LM5_ITKCALL(iRetCode, LM5_get_pref_containing_prop_and_value_list(pszPrefPrefix, ptObjects[iDx], &pszPrefName));
        
        if(pszPrefName != NULL)
        {
            int    iPrefValCnt  = 0;
            char** ppszPrefVals = NULL;

            LM5_ITKCALL(iRetCode, LM5_pref_get_string_values(pszPrefName, &iPrefValCnt, &ppszPrefVals));

            if(iPrefValCnt > 0)
            {
                /*  Lock the object.  */
                LM5_ITKCALL(iRetCode, AOM_refresh(ptObjects[iDx], TRUE));

                for(int iJx = 0; iJx < iPrefValCnt &&  iRetCode == ITK_ok; iJx++)
                {
                    int    iInfoCnt     = 0;
                    char** ppszAttrInfo = NULL;

                    LM5_ITKCALL(iRetCode, LM5_str_parse_string(ppszPrefVals[iJx], LM5_STRING_COLON, &iInfoCnt, &ppszAttrInfo));

                    if(iInfoCnt == 2)
                    {
                        LM5_ITKCALL(iRetCode, LM5_set_attribute_value(ptObjects[iDx], ppszAttrInfo[0], ppszAttrInfo[1], NULLTAG));
                    }
                    else
                    {
                        TC_write_syslog("LM5_T4S: Value[%s]of Preference [%s] does not comply with prescribed pattern. \n", ppszPrefVals[iJx], pszPrefName);
                    }

                    LM5_MEM_TCFREE(ppszAttrInfo);
                }

                /*  Save the object.  */
                LM5_ITKCALL(iRetCode, AOM_save(ptObjects[iDx]));
                
                /*  Lock the object.  */
                LM5_ITKCALL(iRetCode, AOM_refresh(ptObjects[iDx], FALSE));
            }
            
            LM5_MEM_TCFREE(ppszPrefVals);
        }

        LM5_MEM_TCFREE(pszPrefName);
    }
    
    return iRetCode;
}

/**
 * Function get the name of multi-value Preference which contains the Property and respective value to be set for input object.
 * Preference name to be constructed is of following pattern
 *                LM5_<T4S Handler name with underscore instead of dash>_<OBJECT_TYPE>_Set_Properties
 * For e.g.
 *  Name Of Preference: 
 *                          LM5_T4S_transfer_BillOfMaterial_P_SMAspezTeilRevision_Set_Properties
 *  Values of Preference:
 *                          LM5_MMSENTTOSAP:Success
 *                          LM5_EPLAN_EXISTS:true
 *                          LM5_GEWBRUTTO:9.01
 *
 * @param[in] pszPrefPrefix  Prefix that will be used in constructing the Preference name
 *                           which contains property and value list                 
 * @param[in]  tObject       Tag of TC Business object            
 * @param[out] ppszPrefName  Name of preference
 *
 * @return ITK_ok if no errors occurred, else ITK error code
 */
int LM5_get_pref_containing_prop_and_value_list
(
    char*    pszPrefPrefix,
    tag_t    tObject, 
    char**   ppszPrefName
)
{
    int    iRetCode           = ITK_ok;
    char*  pszObjectType      = NULL;
 
    /* Initializing the Out Parameter to this function. */
    (*ppszPrefName)  = NULL;

    /* Get Folder type */
    LM5_ITKCALL(iRetCode, LM5_obj_ask_type(tObject, &pszObjectType));

    LM5_cust_strcpy((*ppszPrefName), pszPrefPrefix);

    LM5_cust_strcat((*ppszPrefName), LM5_STRING_UNDERSCORE);

    LM5_cust_strcat((*ppszPrefName), pszObjectType);

    LM5_cust_strcat((*ppszPrefName), LM5_PREF_SET_PROPERTIES_POSTFIX);

    LM5_MEM_TCFREE( pszObjectType );

    return iRetCode;
}

