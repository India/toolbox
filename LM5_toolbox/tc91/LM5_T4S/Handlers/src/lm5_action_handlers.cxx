/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_action_handlerss.cxx

    Description: This file contains function that registers all Action Handlers for LM5_T4S custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-Sep-14    Tarun Kumar Singh   Initial Release

========================================================================================================================================================================*/
#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_action_handlers.hxx>
#include <lm5_t4s_dummy.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Functon to register the action handlers to be used in the workflows.
 *
 * @return Status of registration of action handlers. 
 */
int LM5_register_action_handlers
(
    void
)
{
    int iRetCode = ITK_ok;
    
    TC_write_syslog("In LM5 Register Action Handlers Operation\n");
    
    LM5_ITKCALL( iRetCode, EPM_register_action_handler( "T4S-transfer-MaterialMaster", "", LM5_T4S_transfer_MaterialMaster));

    LM5_ITKCALL( iRetCode, EPM_register_action_handler( "T4S-transfer-ChangeNumber", "", LM5_T4S_transfer_ChangeNumber));
  
    LM5_ITKCALL( iRetCode, EPM_register_action_handler( "T4S-transfer-BillOfMaterial", "", LM5_T4S_transfer_BillOfMaterial));

    return iRetCode;
}


