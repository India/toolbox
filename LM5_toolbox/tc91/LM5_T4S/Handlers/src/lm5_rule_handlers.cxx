/*======================================================================================================================================================================
                Copyright 2014  LMtec Group - Lifecycle Management Solutions
                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

    Filename: lm5_rule_handlers.cxx

    Description: This file contains function that registers all Rule Handlers for LM5 T4S custom DLL

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-Sep-14    Tarun Kumar Singh    Initial Release

========================================================================================================================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include <lm5_lib_library.hxx>
#include <lm5_rule_handlers.hxx>
#include <lm5_t4s_dummy.hxx>

#ifdef __cplusplus
}
#endif

/**
 * Registers the rule handlers for workflows.
 * 
 * @return Status of registration of rule handlers.
 */
int LM5_register_rule_handlers
(
    void
)
{
    int iRetCode      = ITK_ok;
    
    TC_write_syslog("In LM5 Register Rule Handlers Operation\n");

	LM5_ITKCALL( iRetCode, EPM_register_rule_handler( "T4X-attach-LogfileDataset", "", LM5_t4x_attach_logfiledataset_rh));
   
    return iRetCode;
    
}




