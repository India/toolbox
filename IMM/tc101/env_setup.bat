ECHO ON

REM INSTALL WORKFLOWS
plmxml_import -u=infodba -p=infodba -g=dba -xml_file=./workflows/material_workflows.xml -log=create_workflows.log -transfermode=workflow_template_overwrite

REM INSTALL PREFERENCES
preferences_manager -u=infodba -p=infodba -g=dba -mode=import -file=./preferences/TC10_IMM_preferences_site_to_merge.xml -scope=SITE -action=MERGE

REM INSTALL QUERRIES
plmxml_import -u=infodba -p=infodba -g=dba -xml_file=./queries/__MDSNewMaterials.xml -import_mode=overwrite -log=__MDSNewMaterials.log

REM INSTALL STYLESHEETS
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMCheckClassCreateSummary -ref=XMLRendering -f=./stylesheets/IMMCheckClassCreate.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMatCompCreate  -ref=XMLRendering -f=./stylesheets/IMMMatCompCreate.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMatCompRevProperties -ref=XMLRendering -f=./stylesheets/IMMMatCompRevProperties.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMatCompSummary  -ref=XMLRendering -f=./stylesheets/IMMMatCompSummary.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMaterialCreate -ref=XMLRendering -f=./stylesheets/IMMMaterialCreate.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMaterialRevProperties  -ref=XMLRendering -f=./stylesheets/IMMMaterialRevProperties.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMaterialRevSummary -ref=XMLRendering -f=./stylesheets/IMMMaterialRevSummary.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMatNoteFormCreate  -ref=XMLRendering -f=./stylesheets/IMMMatNoteFormCreate.xml -de=r
