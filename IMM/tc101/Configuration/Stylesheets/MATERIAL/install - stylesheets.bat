ECHO ON
cd %~DP0

REM INSTALL STYLESHEETS
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMCheckClassCreateSummary -ref=XMLRendering -f=%~DP0IMMCheckClassCreate.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMatCompCreate  -ref=XMLRendering -f=%~DP0IMMMatCompCreate.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMatCompRevProperties -ref=XMLRendering -f=%~DP0IMMMatCompRevProperties.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMatCompSummary  -ref=XMLRendering -f=%~DP0IMMMatCompSummary.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMaterialCreate -ref=XMLRendering -f=%~DP0IMMMaterialCreate.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMaterialRevProperties  -ref=XMLRendering -f=%~DP0IMMMaterialRevProperties.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMaterialRevSummary -ref=XMLRendering -f=%~DP0IMMMaterialRevSummary.xml -de=r
import_file -u=infodba -p=infodba -g=dba -type=XMLRenderingStylesheet -d=IMMMatNoteFormCreate  -ref=XMLRendering -f=%~DP0IMMMatNoteFormCreate.xml -de=r
