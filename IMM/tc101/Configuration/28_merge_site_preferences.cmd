@echo off

if not defined TC_ROOT goto ENV_MISSING
if x%SILENT_MODE%==xTRUE goto START_PROCESS

echo Type in password of "infodba"
set /P DBAPASS=Password: 
set DBAPASS_TMP=******

:START_PROCESS
REM Merge Teamcenter site preferences
echo.
echo preferences_manager -u=infodba -p=%DBAPASS_TMP% -g=dba -mode=import -file=Preferences\TC10_IMM_preferences_site_to_merge.xml -scope=SITE -action=MERGE
preferences_manager -u=infodba -p=%DBAPASS% -g=dba -mode=import -file=Preferences\TC10_IMM_preferences_site_to_merge.xml -scope=SITE -action=MERGE

goto :EOF

:ENV_MISSING
echo You need to run this script in a Teamcenter environment
