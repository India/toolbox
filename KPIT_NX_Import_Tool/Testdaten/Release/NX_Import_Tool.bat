@echo off
rem Pieron values
if not defined TC_ROOT set TC_ROOT=d:\splm\tc11
if not defined TC_DATA set TC_DATA=d:\splm\tc11data
if not defined UGII_BASE_DIR set UGII_BASE_DIR=D:\splm\NX100
if not defined UGII_ROOT_DIR set UGII_ROOT_DIR=D:\splm\NX100\UGII

rem LMtec values
rem if not defined TC_ROOT set TC_ROOT=C:\plm\tc112
rem if not defined TC_DATA set TC_DATA=C:\plmshare\tcdev\tcdata112
rem if not defined UGII_BASE_DIR set UGII_BASE_DIR=C:\plm\nx\NX10_SIEMENS_01040000
rem if not defined UGII_ROOT_DIR set UGII_ROOT_DIR=C:\plm\nx\NX10_SIEMENS_01040000\UGII

call %TC_DATA%\tc_profilevars.bat
if not defined FMS_HOME set FMS_HOME=%TC_ROOT%\tccs
set PATH=%UGII_ROOT_DIR%;%PATH%
set CURRENT_DIR=%~dp0

rem cd x:\
rem cd X:\Konstruktionsdaten\Normteile\Teamcenter\Bihlermaschinen
rem set UGII_LOAD_OPTIONS=X:\Konstruktionsdaten\Normteile\Teamcenter\Bihlermaschinen\bihlermaschinen.def

set TC_KEEP_SYSTEM_LOG=Y

::start cmd.exe /k

"%CURRENT_DIR%ImportTool.exe"
rem "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\devenv.exe"
