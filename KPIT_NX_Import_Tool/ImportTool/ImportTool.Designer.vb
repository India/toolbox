﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form_ImportTool
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_ImportTool))
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.btn_1_CreateMappingLog = New System.Windows.Forms.Button()
        Me.btn_TC_Import = New System.Windows.Forms.Button()
        Me.txt_Username = New System.Windows.Forms.TextBox()
        Me.txt_Passwort = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Group = New System.Windows.Forms.TextBox()
        Me.btn_partcleanup = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_1_CreateMappingLog
        '
        Me.btn_1_CreateMappingLog.BackColor = System.Drawing.SystemColors.Control
        Me.btn_1_CreateMappingLog.Location = New System.Drawing.Point(26, 86)
        Me.btn_1_CreateMappingLog.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btn_1_CreateMappingLog.Name = "btn_1_CreateMappingLog"
        Me.btn_1_CreateMappingLog.Padding = New System.Windows.Forms.Padding(1, 1, 0, 0)
        Me.btn_1_CreateMappingLog.Size = New System.Drawing.Size(195, 38)
        Me.btn_1_CreateMappingLog.TabIndex = 4
        Me.btn_1_CreateMappingLog.Text = "Create import.csv (2)"
        Me.btn_1_CreateMappingLog.UseVisualStyleBackColor = False
        '
        'btn_TC_Import
        '
        Me.btn_TC_Import.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_TC_Import.Location = New System.Drawing.Point(291, 192)
        Me.btn_TC_Import.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btn_TC_Import.Name = "btn_TC_Import"
        Me.btn_TC_Import.Size = New System.Drawing.Size(299, 29)
        Me.btn_TC_Import.TabIndex = 5
        Me.btn_TC_Import.Text = "Assembly Import in Teamcenter (3)"
        Me.btn_TC_Import.UseVisualStyleBackColor = True
        '
        'txt_Username
        '
        Me.txt_Username.Location = New System.Drawing.Point(106, 32)
        Me.txt_Username.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_Username.Name = "txt_Username"
        Me.txt_Username.Size = New System.Drawing.Size(167, 21)
        Me.txt_Username.TabIndex = 6
        '
        'txt_Passwort
        '
        Me.txt_Passwort.Location = New System.Drawing.Point(106, 69)
        Me.txt_Passwort.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_Passwort.Name = "txt_Passwort"
        Me.txt_Passwort.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_Passwort.Size = New System.Drawing.Size(167, 21)
        Me.txt_Passwort.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 35)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 15)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "TC-Username"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 72)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 15)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "TC-Password"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(20, 109)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 15)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "TC-Gruppe"
        '
        'txt_Group
        '
        Me.txt_Group.Location = New System.Drawing.Point(106, 106)
        Me.txt_Group.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_Group.Name = "txt_Group"
        Me.txt_Group.Size = New System.Drawing.Size(167, 21)
        Me.txt_Group.TabIndex = 11
        '
        'btn_partcleanup
        '
        Me.btn_partcleanup.BackColor = System.Drawing.SystemColors.Control
        Me.btn_partcleanup.Location = New System.Drawing.Point(26, 35)
        Me.btn_partcleanup.Name = "btn_partcleanup"
        Me.btn_partcleanup.Size = New System.Drawing.Size(195, 33)
        Me.btn_partcleanup.TabIndex = 12
        Me.btn_partcleanup.Text = "Part Cleanup (1)"
        Me.btn_partcleanup.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txt_Username)
        Me.GroupBox1.Controls.Add(Me.txt_Passwort)
        Me.GroupBox1.Controls.Add(Me.txt_Group)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(291, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(299, 151)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Login"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_1_CreateMappingLog)
        Me.GroupBox2.Controls.Add(Me.btn_partcleanup)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(22, 24)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(245, 151)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Utilities"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(22, 192)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(40, 29)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = "Exit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form_ImportTool
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(617, 247)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btn_TC_Import)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "Form_ImportTool"
        Me.Text = "Teamcenter NX Import Tool"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents btn_1_CreateMappingLog As Button
    Friend WithEvents btn_TC_Import As Button
    Friend WithEvents txt_Username As TextBox
    Friend WithEvents txt_Passwort As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_Group As TextBox
    Friend WithEvents btn_partcleanup As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Button1 As Button
End Class
