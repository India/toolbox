﻿Imports System.IO

''' <summary>
''' Moved some of the UI Code here to get a better overview of the code.
''' </summary>
Module ImportToolHelper
	Public Sub RemoveAttributesFromParts(Path_LoadOptions_def As String, File_LoadOptions_def As String, baseDir As String, count_Anzahl_Attributdateien As Integer)
		For n = 0 To count_Anzahl_Attributdateien - 1
			Dim cmd = baseDir & "\ugii\run_journal.exe"
			Dim args = Path_LoadOptions_def & "TC_IMPORT_TEMP\ATTRIBUTE\delete_AttributeValues_" & n & ".vb"

			Dim startInfo As ProcessStartInfo = New ProcessStartInfo(cmd, args)

			Dim ugiiLoadOptions = Path.Combine(Path_LoadOptions_def, File_LoadOptions_def)

			startInfo.EnvironmentVariables.Add("UGII_LOAD_OPTIONS", ugiiLoadOptions)
			startInfo.UseShellExecute = False

			Dim p = Process.Start(startInfo)

			'Dim p = Process.Start(cmd, Path_LoadOptions_def & "TC_IMPORT_TEMP\ATTRIBUTE\delete_AttributeValues_" & n & ".vb")
			p.WaitForExit()
		Next
	End Sub

	Public Sub DeletePartFamilyStatus(Path_LoadOptions_def As String, File_LoadOptions_def As String, count_Anzahl_PartFamily_Dateien As Integer)
		For n = 0 To count_Anzahl_PartFamily_Dateien - 1
			Dim cmd = "cmd"
			Dim args = "/C" & Path_LoadOptions_def & "TC_IMPORT_TEMP\PARTFAMILY\delete_partfamilyStatus_" & n & ".cmd"

			Dim startInfo As ProcessStartInfo = New ProcessStartInfo(cmd, args)

			Dim ugiiLoadOptions = Path.Combine(Path_LoadOptions_def, File_LoadOptions_def)

			startInfo.EnvironmentVariables.Add("UGII_LOAD_OPTIONS", ugiiLoadOptions)
			startInfo.UseShellExecute = False

			Dim p = Process.Start(startInfo)

			' p.WaitForExit()
			p.Close()
		Next
	End Sub


	Public Function AskUserForImportAssembly(initialDirectory As String) As String

		Dim fd As OpenFileDialog = New OpenFileDialog()

		fd.Title = "Select LoadOptions File"
		fd.InitialDirectory = initialDirectory

		'fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
		fd.Filter = "All files (*.prt)|*.prt"
		fd.FilterIndex = 2
		fd.RestoreDirectory = True

		If fd.ShowDialog() = DialogResult.OK Then
			Return fd.FileName
		End If 

		Return String.Empty
	End Function

End Module
