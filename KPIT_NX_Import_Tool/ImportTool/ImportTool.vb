﻿Imports System.IO
Imports System.Reflection
Imports System.Text.RegularExpressions

Public Class Form_ImportTool

	' globale Variablen deklarieren
	Shared PfadImportTool As String = Application.StartupPath

	Shared Dateityp1 As String
	Shared Dateityp2 As String

	Shared ItemType As String

	Shared str_TC_Username As String
	Shared str_TC_Password As String
	Shared str_TC_GROUP As String

	Shared InitialDir As String

	Shared ImportCloneZeile01 As String
	Shared ImportCloneZeile02 As String
	Shared ImportCloneZeile03 As String
	Shared ImportCloneZeile04 As String
	Shared ImportCloneZeile05 As String
	Shared ImportCloneZeile06 As String
	Shared ImportCloneZeile07 As String
	Shared ImportCloneZeile08 As String
	Shared ImportCloneZeile09 As String
	Shared ImportCloneZeile10 As String
	Shared ImportCloneZeile11 As String
	Shared ImportCloneZeile12 As String
	Shared ImportCloneZeile13 As String
	Shared ImportCloneZeile14 As String
	Shared ImportCloneZeile15 As String
	Shared ImportCloneZeile16 As String
	Shared ImportCloneZeile17 As String
	Shared ImportCloneZeile18 As String
	Shared ImportCloneZeile19 As String
	Shared ImportCloneZeile20 As String
	Shared ImportCloneZeile21 As String
	Shared ImportCloneZeile22 As String

	Shared ImportAssembly As String

	' Frank: Schreibe Dateinamen hier herein
	' Beispiel:
	'D:\test_30\_TC_IMPORT_TEMP\import.csv
	Shared ImportFileName As String

	Shared Configuration As Config

	Public Sub Form_ImportTool_Load(sender As Object, e As EventArgs) Handles MyBase.Load
#Region "Read Config and Set Variables"

		' #####################################
		' Config.ini lesen und Variablen setzen
		' #####################################

		Configuration = Config.Load(PfadImportTool & "\config.ini")
		
		txt_Username.Text = Configuration.TcUserName
		txt_Passwort.Text = Configuration.TcPassword
		txt_Group.Text = Configuration.TcGroup


		For Each Line As String In IO.File.ReadAllLines(PfadImportTool & "\config.ini")
			Dim strArr() As String
			Dim count As Integer
		
			' Deklaration von Dateityp1
			If Line.Contains("Dateityp1=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					Dateityp1 = strArr(1)
				Next
			End If

			' Deklaration von Dateityp2
			If Line.Contains("Dateityp2=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					Dateityp2 = strArr(1)
				Next
			End If

			' Deklaration von ItemType
			If Line.Contains("ItemType=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ItemType = strArr(1)
				Next
			End If
			
			' Deklaration von InitialDir, also der Startpfad wo die Suchmaske zur LoadOptions.def geöffnet wird
			If Line.Contains("InitialDir=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					InitialDir = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile01
			If Line.Contains("ImportCloneZeile01=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile01 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile02
			If Line.Contains("ImportCloneZeile02=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile02 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile03
			If Line.Contains("ImportCloneZeile03=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile03 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile04
			If Line.Contains("ImportCloneZeile04=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile04 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile05
			If Line.Contains("ImportCloneZeile05=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile05 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile06
			If Line.Contains("ImportCloneZeile06=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile06 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile07
			If Line.Contains("ImportCloneZeile07=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile07 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile08
			If Line.Contains("ImportCloneZeile08=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile08 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile09
			If Line.Contains("ImportCloneZeile09=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile09 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile10
			If Line.Contains("ImportCloneZeile10=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile10 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile11
			If Line.Contains("ImportCloneZeile11=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile11 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile12
			If Line.Contains("ImportCloneZeile12=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile12 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile13
			If Line.Contains("ImportCloneZeile13=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile13 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile14
			If Line.Contains("ImportCloneZeile14=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile14 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile15
			If Line.Contains("ImportCloneZeile15=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile15 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile16
			If Line.Contains("ImportCloneZeile16=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile16 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile17
			If Line.Contains("ImportCloneZeile17=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile17 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile18
			If Line.Contains("ImportCloneZeile18=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile18 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile19
			If Line.Contains("ImportCloneZeile19=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile19 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile20
			If Line.Contains("ImportCloneZeile20=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile20 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile21
			If Line.Contains("ImportCloneZeile21=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile21 = strArr(1)
				Next
			End If

			' Deklaration von ImportCloneZeile22
			If Line.Contains("ImportCloneZeile22=") Then
				strArr = Line.Split("=")
				For count = 0 To strArr.Length - 1
					ImportCloneZeile22 = strArr(1)
				Next
			End If




		Next

#End Region
	End Sub

	Private Sub btn_1_CreateMappingLog_Click(sender As Object, e As EventArgs) Handles btn_1_CreateMappingLog.Click

        If (MessageBox.Show("Do you want to create mapping CSV ?", "Create Mapping", MessageBoxButtons.YesNo) = DialogResult.No) Then
            Exit Sub
        End If

        ' Fehler-Abfangen falls User, PW, Grp nicht eingetragen wurde
        ' ###########################################################
        Dim bool_OK_NOK As Boolean
		bool_OK_NOK = True

		If txt_Username.Text = "" Then
			bool_OK_NOK = False
			MsgBox("TC Username fehlt")
		ElseIf txt_Passwort.Text = "" Then
			bool_OK_NOK = False
			MsgBox("TC Passwort fehlt")
		ElseIf txt_Group.Text = "" Then
			bool_OK_NOK = False
			MsgBox("TC Gruppe fehlt")
		Else
			' Variablen eintragen
			str_TC_Username = txt_Username.Text
			str_TC_Password = txt_Passwort.Text
			str_TC_GROUP = txt_Group.Text
		End If


		If bool_OK_NOK = True Then
			' Test der Logindaten

			'Der Funktion ein Rückgabewert (Array) definieren
			'Dim Login_OK(0) As Boolean

			' Rückgabewert der boolschen Variable zuweisen
			'bool_OK_NOK = Login_OK(0)
			'MsgBox(bool_OK_NOK)
		End If


		If bool_OK_NOK = True Then
			' Aufruf der Sub, wenn (bool_OK_NOK = True)
			CreateMapping()
		End If

	End Sub



	Function Logintest(ByVal bool_OK_Input As Boolean) As Array
		'Der Funktion ein Rückgabewert (Array) definieren
		Dim Login_OK(0) As Boolean

		' ####################################
		' Hier der Login Test
		' ####################################

		'str_TC_Username
		'str_TC_Password
		'str_TC_GROUP

		'Rückgabewert der Variablen/ dem Array(0) "Login_OK(0)" zur Rückgabe füllen
		Login_OK(0) = False
		Return Login_OK

	End Function

	Sub CreateMapping()
		' ##########################################
		'Schleife:
		' -Oeffnen der Odnerauswahl (Fensters)
		' -Auswahl der "LoadOption.def"
		' - Erzeugen des Ordners "TC_IMPORT_TEMP"
		' - Schreiben der Einstellung "LoadOptions" in den TEMP Ordner ins File "SETTINGS_TEMP.csv"

		' - Lesen der Ordnerstruktur "PRT Files"
		' - Schreiben der Datei "Mapping.log"
		' ##########################################


		' Starten der Auswahl
		'If import_Folder.ShowDialog = Windows.Forms.DialogResult.OK Then
		' Darstellen des Ausgewählten Ordners in einer MSG-Box
		'MessageBox.Show(it_Folder.SelectedPath)

		' Auswahl der "LoadOptions.def"
		Dim Path_LoadOptions_def As String = Nothing 
		Dim File_LoadOptions_def As String = Nothing

		' Auswahl der LoadOptions im Path
		' ##################################
		Dim fd As OpenFileDialog = New OpenFileDialog()
		Dim strFileName As String

		fd.Title = "Select LoadOptions File"
		fd.InitialDirectory = InitialDir

		'fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
		fd.Filter = "All files (*.def)|*.def"
		fd.FilterIndex = 2
		fd.RestoreDirectory = True

		If fd.ShowDialog() = DialogResult.OK Then
			strFileName = fd.FileName

			' Pfad extrahieren, ist ohne \ am Ende
			Path_LoadOptions_def = IO.Path.GetDirectoryName(strFileName)

			' Dateiname extrahieren
			File_LoadOptions_def = IO.Path.GetFileName(strFileName)

		Else
			' Do nothing
			Application.Exit()
		End If

		ImportAssembly = ImportToolHelper.AskUserForImportAssembly(Path_LoadOptions_def)
		If ImportAssembly.Count = 0
			Application.Exit()
		End If

		' xxx
		' Die Pfadangabe erweitern zur darunter erzeugten Datei "TC_IMPORT_TEMP\import.csv"
		' und in die globale SHARED Variable schreiben:
		'ImportFileName = "D:\test_30\TC_IMPORT_TEMP\import.csv"
		ImportFileName = Path_LoadOptions_def & "\TC_IMPORT_TEMP\import.csv"


		' Ordner "TC_IMPORT_TEMP" erstellen
		' ###################################

		If Not System.IO.Directory.Exists(Path_LoadOptions_def) = False Then
			' Ordner existiert noch nicht, also erstellen
			Try
				System.IO.Directory.CreateDirectory(Path_LoadOptions_def & "\TC_IMPORT_TEMP")
				System.IO.Directory.CreateDirectory(Path_LoadOptions_def & "\TC_IMPORT_TEMP\PARTFAMILY")
				System.IO.Directory.CreateDirectory(Path_LoadOptions_def & "\TC_IMPORT_TEMP\ATTRIBUTE")
			Catch ex As Exception
				' Ordner existiert schon und wurde nicht erstellt
			End Try
		End If

		' Schreiben der settings.ini mit "Path" + "LoadOptions"
		' ##################################################
		'Using swTMP As IO.StreamWriter = IO.File.AppendText(it_Folder.SelectedPath & "\_temp_prt.ini")
		'Using swTMP As IO.StreamWriter = IO.File.AppendText(Path_LoadOptions_def & "\TC_IMPORT_TEMP\settings.ini")
		'swTMP.WriteLine("Path_LoadOptions_def=" & Path_LoadOptions_def)
		'swTMP.WriteLine("File_LoadOptions_def=" & File_LoadOptions_def)
		'sw.WriteLine("Text")
		'End Using



		' Einlesen der *.PRT Files aus dem gewählten Verzeichnis
		' #########################################################
		' #########################################################
		Path_LoadOptions_def = Path_LoadOptions_def & "\"

		' Welcher Ordner und welcher Dateityp soll ausgelesen werden
		Dim filea() As String = IO.Directory.GetFiles(Path_LoadOptions_def, "*" & Dateityp1, IO.SearchOption.AllDirectories)


		For i As Integer = 0 To filea.Length - 1
			' Schreiben der neuen Zeile ans ENDE der "TC_IMPORT_TEMP\mapping.log"
			' ###################################################################
			Using swTMP As IO.StreamWriter = IO.File.AppendText(Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping.log")
				swTMP.WriteLine("[" & filea(i) & "]")
			End Using

			' Schreiben der neuen Zeile ans ENDE der "TC_IMPORT_TEMP\PARTFAMILY\delete_PartFamily_xxx.cmd"
			' ###############################################################################################
			Dim templateFileName_PartFamily = Path.Combine(Application.StartupPath, "template_partfamily.cmd")
			Dim fileContent_PartFamily = File.ReadAllText(templateFileName_PartFamily)
			fileContent_PartFamily = fileContent_PartFamily.Replace("<FILENAME>", filea(i))
			File.WriteAllText(Path_LoadOptions_def & "TC_IMPORT_TEMP\PARTFAMILY\delete_partfamilyStatus_" & i & ".cmd", fileContent_PartFamily)

			' Schreiben der neuen Zeile ans ENDE der "TC_IMPORT_TEMP\ATTRIBUTE\delete_AttributeValues_xxx.vb"
			' ###############################################################################################
			Dim templateFileName = Path.Combine(Application.StartupPath, "template.vb")
			Dim fileContent = File.ReadAllText(templateFileName)
			fileContent = fileContent.Replace("<FILENAME>", filea(i))
			File.WriteAllText(Path_LoadOptions_def & "TC_IMPORT_TEMP\ATTRIBUTE\delete_AttributeValues_" & i & ".vb", fileContent)
		Next

		' MAPPING.CLONE Datei erzeugen
		' und
		' Die Attribut-Leerungsdateien durcharbeiten
		' #########################################################
		' #########################################################

		' Laufwerksbuchstabe vom Pfad zu der LoadOptions.def extrahieren
		' Wird in der nächsten "using" Anwendung benötigt
		' ##############################################################
		Dim str_LW_LoadOptions
		Dim str_path As System.IO.Path
		str_LW_LoadOptions = str_path.GetPathRoot(Path_LoadOptions_def)
		' Letztes Zeichen "\" entfernen
		str_LW_LoadOptions = str_LW_LoadOptions.Substring(0, str_LW_LoadOptions.Length - 1)

		Dim baseDir = Environment.GetEnvironmentVariable("UGII_BASE_DIR")


		'##############################7

		' Zählen wieviele Dateien im Ordner "PARTFAMILY" sind
		Dim count_Anzahl_PartFamily_Dateien As Integer
		count_Anzahl_PartFamily_Dateien = My.Computer.FileSystem.GetFiles(Path_LoadOptions_def & "TC_IMPORT_TEMP\PARTFAMILY").Count

		' Variable hält den kompletten Text
		Dim Output_PartFamily As String = Nothing

		If Not Configuration.SkipDeletePartFamilyStatus
			ImportToolHelper.DeletePartFamilyStatus(Path_LoadOptions_def, File_LoadOptions_def, count_Anzahl_PartFamily_Dateien)
		End If


		' Zählen wieviele Dateien im Ordner "ATTRIBUTE" sind
		Dim count_Anzahl_Attributdateien As Integer = My.Computer.FileSystem.GetFiles(Path_LoadOptions_def & "TC_IMPORT_TEMP\ATTRIBUTE").Count

		If Not Configuration.SkipRemoveAttributes Then
			ImportToolHelper.RemoveAttributesFromParts(Path_LoadOptions_def, File_LoadOptions_def, baseDir, count_Anzahl_Attributdateien)
		End If


        '--------------------------------------------------------------------------
        'logFilename = Path_prt_file & "\partcleanuplog_" & System.DateTime.Now.ToString("yyyMMdd") & "_" & System.DateTime.Now.ToString("HHmmss") & ".log"

        'Dim ps = New Process
        'ps.StartInfo.FileName = baseDir & "\UGII\refile_part"
        'ps.StartInfo.Arguments = " " & strFileName & " -cleanup -g -y -s -d -f -us -co -r"
        'ps.StartInfo.WorkingDirectory = Path_prt_file
        'ps.StartInfo.WindowStyle = ProcessWindowStyle.Minimized
        'ps.StartInfo.RedirectStandardOutput = True
        'ps.StartInfo.UseShellExecute = False
        'ps.Start()

        'Dim logfile As System.IO.StreamWriter
        'logfile = My.Computer.FileSystem.OpenTextFileWriter(logFilename, False)
        'logfile.WriteLine("#############################################################################################################################")
        'logfile.WriteLine("                                                 PART CLEANUP LOG                                                            ")
        'logfile.WriteLine("Date and Time : " & Date.Now)
        'logfile.WriteLine("#############################################################################################################################")
        'logfile.Write(ps.StandardOutput.ReadToEnd)
        'logfile.Close()
        'ps.WaitForExit()
        'MessageBox.Show("Log is saved under " & logFilename)

        '--------------------------------------------------------------------------

        'Console.WriteLine("Start tcin")


        ' NEw code =======================****************************************************************============================================================================
        Dim Output As String

        Using P As New Process()
            P.StartInfo.FileName = "cmd.exe"
            P.StartInfo.Arguments = "/k echo on"
            P.StartInfo.UseShellExecute = False
            P.StartInfo.RedirectStandardOutput = True
            P.StartInfo.RedirectStandardInput = True
            P.Start()

            'Schreiben der verschiedenen Befehle als separate Zeilen in die cmd:
            P.StandardInput.WriteLine("set TC_ROOT=" & Configuration.TcRoot)
            P.StandardInput.WriteLine("set TC_DATA=" & Configuration.TcData)
            P.StandardInput.WriteLine(Configuration.TcData & "\tc_profilevars.bat")
            P.StandardInput.WriteLine("set PATH=%UGII_ROOT_DIR%;%PATH%")
            P.StandardInput.WriteLine("set FMS_HOME=%TC_ROOT%\tccs")
            P.StandardInput.WriteLine("X:")
            P.StandardInput.WriteLine("cd " & Path_LoadOptions_def)
            P.StandardInput.WriteLine("cd " & Path_LoadOptions_def)
            P.StandardInput.WriteLine("set UGII_LOAD_OPTIONS=" & Path_LoadOptions_def & "\" & File_LoadOptions_def)
            P.StandardInput.WriteLine("%UGII_BASE_DIR%\UGMANAGER\tcin_import -u=" & str_TC_Username & " -p=" & str_TC_Password & " -g=" & str_TC_GROUP & " -fam=error -s=" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping.clone  -asse=" & ImportAssembly & " -dr > fff.log")

            ' Wichtig: Am Ende ein "Exit" senden, sonst wird der Standart-Out (STDOUT) vom Stream nie geschlossen
            P.StandardInput.WriteLine("pause")

            ' Lesen des kompletten Streams
            '  Output = P.StandardOutput.ReadToEnd()
            ' Lesen des kompletten Streams
            ' Output = P.StandardOutput.ReadToEnd()

            ' Warten, bis die cmd / der Prozess geschlossen wurde
            P.WaitForExit()

            MessageBox.Show("Log is dddddddddddddddddddddddddddddddddd under ")
        End Using

        ' NEw code ============================================*******************************************************=======================================================



        'Dim tcinCmd = "D:\\Siemens\\NX11\\UGMANAGER\\tcin_import.exe"

        'Dim ps = New Process
        'ps.StartInfo.FileName = baseDir & "\UGMANAGER\tcin_import"
        'ps.StartInfo.Arguments = "-u=" & str_TC_Username & " -p=" & str_TC_Password & " -g=" & str_TC_GROUP & " -fam=strip_status -s=" & Path_LoadOptions_def & "TC_IMPORT_TEMP\  -asse=" & ImportAssembly & " -dr > fff.log"
        'ps.StartInfo.WorkingDirectory = Path_LoadOptions_def
        'ps.StartInfo.WindowStyle = ProcessWindowStyle.Minimized
        'ps.StartInfo.EnvironmentVariables.Add("UGII_LOAD_OPTIONS", Path.Combine(Path_LoadOptions_def, File_LoadOptions_def))
        'ps.StartInfo.RedirectStandardOutput = True
        'ps.StartInfo.UseShellExecute = False
        'ps.Start()

        ''      Dim startInfo = New ProcessStartInfo()
        ''startInfo.FileName = baseDir & "\UGMANAGER\tcin_import"
        ''startInfo.Arguments =  "-u=" & str_TC_Username & " -p=" & str_TC_Password & " -g=" & str_TC_GROUP & " -fam=strip_status -s=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping"" -asse=" & ImportAssembly & " -dr"
        '''startInfo.Arguments =  "-u=" & str_TC_Username & " -p=" & str_TC_Password & " -g=" & str_TC_GROUP & " -fam=strip_status -l=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping.log"" -s=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping"" -asse=" & ImportAssembly & " -dr"
        ''startInfo.EnvironmentVariables.Add("UGII_LOAD_OPTIONS", Path.Combine(Path_LoadOptions_def, File_LoadOptions_def))
        ''startInfo.UseShellExecute = False

        'Dim logFilename As String = Nothing
        'logFilename = Path_LoadOptions_def & "\part_importcsvlog_" & System.DateTime.Now.ToString("yyyMMdd") & "_" & System.DateTime.Now.ToString("HHmmss") & ".log"

        'Dim logfile As System.IO.StreamWriter
        'logfile = My.Computer.FileSystem.OpenTextFileWriter(logFilename, False)
        'logfile.WriteLine("#############################################################################################################################")
        'logfile.WriteLine("                                                 PART CLEANUP LOG                                                            ")
        'logfile.WriteLine("Date And Time : " & Date.Now)
        'logfile.WriteLine("#############################################################################################################################")
        'logfile.Write(ps.StandardOutput.ReadToEnd)
        'logfile.Close()
        'ps.WaitForExit()
        'MessageBox.Show("Log is saved under " & logFilename)

        'If Configuration.SkipRemoveAttributes OrElse Configuration.SkipDeletePartFamilyStatus Then
        '	Return
        'End If

#Region "Dead code"
        ' Neues Prozess Object erzeugen
        'Using P As New Process()
        '    ' Definition welches Programm gestartet werden soll, hier "cmd.exe"
        '    P.StartInfo.FileName = "cmd.exe"

        '    ' Da ich mehrere Commandozeilen an das CMD Uebergeben moechte lasse ich das Startargument mal weg
        '    ' Set Startargument

        '    ' Unterschied Schalter /c und /k .... hmmmmm noch klären
        '    'P.StartInfo.Arguments = "/c ping google.de"
        '    'P.StartInfo.Arguments = "/k echo on"

        '    ' Setze Startargument im cmd Fenster
        '    P.StartInfo.Arguments = "/k echo on"

        '    'Required to redirect output, don't both worrying what it means
        '    P.StartInfo.UseShellExecute = False

        '    ' Dem System mitteilen, dass ich lesen und schreiben möchte
        '    P.StartInfo.RedirectStandardOutput = True
        '    P.StartInfo.RedirectStandardInput = True

        '    '(Prozess) Start der cmd.exe
        '    P.Start()

        '    'Schreiben der verschiedenen Befehle als separate Zeilen in die cmd:
        '    P.StandardInput.WriteLine("set TC_ROOT=" & TC_ROOT)
        '    P.StandardInput.WriteLine("set TC_DATA=" & TC_DATA)
        '    P.StandardInput.WriteLine(TC_DATA & "\tc_profilevars.bat")
        '    'P.StandardInput.WriteLine("d: \splm\tc11data\tc_profilevars.bat")

        '    P.StandardInput.WriteLine("set PATH=%UGII_ROOT_DIR%;%PATH%")

        '    ' FMS_HOME setzen  ;-)
        '    P.StandardInput.WriteLine("set FMS_HOME=%TC_ROOT%\tccs")

        '    P.StandardInput.WriteLine(str_LW_LoadOptions)
        '    P.StandardInput.WriteLine("cd " & Path_LoadOptions_def)

        '    'P.StandardInput.WriteLine("D:")
        '    'P.StandardInput.WriteLine("cd D:\test_22")

        '    P.StandardInput.WriteLine("cd " & Path_LoadOptions_def)
        '    P.StandardInput.WriteLine("set UGII_LOAD_OPTIONS=" & Path_LoadOptions_def & "\LoadOptions.def")






        '    ' Attribut-Leerung durchgehen
        '    Dim n As Integer

        '    For n = 0 To count_Anzahl_Attributdateien - 1
        '        'P.StandardInput.WriteLine("%UGII_BASE_DIR%\ugii\run_journal.exe " & Path_LoadOptions_def & "TC_IMPORT_TEMP\ATTRIBUTE\delete_AttributeValues_" & n & ".vb")
        '        'P.StandardInput.Flush()
        '    Next





        '    'P.StandardInput.WriteLine("%UGII_BASE_DIR%\UGMANAGER\tcin_import -u=" & str_TC_Username & " -p=" & str_TC_Password & " -g=" & str_TC_GROUP & " –default_t=" & ItemType & " -l=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping.log"" -s=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping"" -dr")
        '    P.StandardInput.WriteLine("%UGII_BASE_DIR%\UGMANAGER\tcin_import -u=" & str_TC_Username & " -p=" & str_TC_Password & " -g=" & str_TC_GROUP & " -fam=strip_status -l=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping.log"" -s=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping"" -dr")

        '    'ok
        '    'P.StandardInput.WriteLine("%UGII_BASE_DIR%\UGMANAGER\tcin_import -u=" & str_TC_Username & " -p=" & str_TC_Password & " -g=" & str_TC_GROUP & " -l=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping.log"" -s=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping"" -dr")
        '    'P.StandardInput.WriteLine("ping google.com")

        '    ' Wichtig: Am Ende ein "Exit" senden, sonst wird der Standart-Out (STDOUT) vom Stream nie geschlossen
        '    P.StandardInput.WriteLine("exit")

        '    ' Lesen des kompletten Streams
        '    Output = P.StandardOutput.ReadToEnd()

        '    ' Warten, bis die cmd / der Prozess geschlossen wurde
        '    P.WaitForExit()
        'End Using

        ' Machen wir etwas mit dem "Output"
        'Trace.WriteLine(Output).
        'MsgBox(Output)
#End Region

        ' Schreiben der neuen Zeile ans Ende / der neuen Datei "TC_IMPORT_TEMP\mapping_result.log"
        ' Der komplette Inhalt des cmd-Fensters wird gespeichert ....
        ' ########################################################################################


        ' Erzeuge komplette import.csv Datei aller Importdaten
        ' ####################################################
        Dim strInhalt As String
		Dim strarr_Zeilen() As String
		Dim strFind_ASM As String
		Dim strFind_PRT As String
		Dim counter As Integer


		' Zeile suchen
		strFind_ASM = "&LOG TOP_ASSEMBLY Part: "
		strFind_PRT = "&LOG Part: "

		' Einlesen der Datei "mapping.clone"
		If IO.File.Exists(Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping.clone") Then strInhalt = IO.File.ReadAllText(Path_LoadOptions_def & "TC_IMPORT_TEMP\mapping.clone", System.Text.Encoding.Default) 'gesamte Textdatei einlesen
		'Zeilen in Array splitten
		strarr_Zeilen = Split(strInhalt, vbNewLine)

		' Arraygröße herausfinden (Anzahl Zeilen der Datei "mapping.clone")
		Dim int_arrGroesse As Integer
		int_arrGroesse = strarr_Zeilen.Length - 1


		' Schreiben der ERSTEN Zeile (HEADER) in der import.csv Datei
		' ###########################################################
		Using swTMP As IO.StreamWriter = IO.File.AppendText(Path_LoadOptions_def & "\TC_IMPORT_TEMP\import.csv")
			Dim sep = Configuration.TrennzeichenCSV
			swTMP.WriteLine("Path" & sep & "Name_PRT_File" & sep & "auto_Part_No" & sep & "auto_Part_Rev" & sep & "auto_Part_Name" & sep & "auto_Description" & sep & "auto_Folder" & sep & "Soll_Part_No" & sep & "Soll_Part_Rev" & sep & "Soll_Part_Name" & sep & "Soll_Description" & sep & "Soll_Folder" & sep & "Soll_B_N_K" & sep & "auto_ASM_or_Part")
		End Using


		' Die einzelnen Arrays / Zeilen durchsuchen (von der Datei "mapping.clone)
		' ########################################################################
		For counter = 0 To int_arrGroesse

			Dim currentLine = strarr_Zeilen(counter)

			If currentLine.Contains(strFind_ASM) Or currentLine.Contains(strFind_PRT) Then
				'Wenn ein *.prt File in der Zeile ist, also
				' wenn "&LOG TOP_ASSEMBLY Part:" oder "&LOG Part:" am Anfang steht
				Dim str_ASM_or_Part As String = String.Empty
				Dim str_Path_and_File As String = String.Empty
				Dim str_PathPRT As String = String.Empty
				Dim str_NamePRT As String = String.Empty
				Dim str_DBPartNo As String = String.Empty
				Dim str_DBPartRev As String = String.Empty
				'Dim str_ItemType As String = String.Empty
				Dim str_DBPartName As String = String.Empty
				Dim str_DBPartDescription As String = String.Empty
				Dim str_DBFolder As String = String.Empty

#Region "Dead Code"
				' liefert die letzten 6 zeichen von rechts
				'str_Path_and_File = Microsoft.VisualBasic.Right(strarr_Zeilen(counter), 6)

				' entfernt die ersten 2 stellen von links
				'str_Path_and_File = (strarr_Zeilen(counter).Remove(0, 2))

				'Liefert die ersten Zeichen mit Left$(String,AnzahlZeichen):
				'Dim s As String = "ABCDEFG"
				'MessageBox.Show(s.Substring(0, 1)) 'ergibt A
				'Messagebox.Show (s.SubString(0, 3)) 'ergibt ABC

				'Die letzten Zeichen mit der Right$(String, AnzahlZeichen)
				'Dim s As String = "ABCDEFG"
				'MessageBox.Show(s.Substring(s.Length - 1)) 'ergibt G
				'MessageBox.Show(s.Substring(s.Length - 3)) 'ergibt EFG

				' erkenne Anführungszeichen = Chr(34)
				'Quelltext = Quelltext.Replace(Chr(34), "")

				'Dim s As String = "Mein Text"
				'MessageBox.Show(s.Substring(0, s.Length - 4)) ' die vier letzten Zeichen entfernen
#End Region

				If currentLine.Contains(strFind_ASM) Then
					' Füllen der Variable zum erkennen ob Part oder ASM/DWG
					' Ergebnis hier: "&LOG TOP_ASSEMBLY Part:"
					str_ASM_or_Part = strFind_ASM
					str_Path_and_File = currentLine.Replace(strFind_ASM, String.Empty).Trim()

				ElseIf currentLine.Contains(strFind_PRT) Then
					' Füllen der Variable zum erkennen ob Part oder ASM/DWG
					str_ASM_or_Part = strFind_PRT
					str_Path_and_File = currentLine.Replace(strFind_PRT, String.Empty).Trim()

				End If

				str_Path_and_File = str_Path_and_File.Replace("""", String.Empty)

				' Path und Filenamen trennen
				' ############################
				' Pfad extrahieren (Hat am Ende kein \)
				str_PathPRT = IO.Path.GetDirectoryName(str_Path_and_File)

				' Dateiname extrahieren
				str_NamePRT = IO.Path.GetFileName(str_Path_and_File)

				' untersten Ordner ermitteln
				Dim str_array_Ordner() As String
				Dim int_array_Groesse As Integer
				'str_DBFolder
				str_array_Ordner = Split(str_PathPRT, "\", -1, CompareMethod.Text)
				int_array_Groesse = str_array_Ordner.Length
				str_DBFolder = str_array_Ordner(int_array_Groesse - 1)

				' ###########################################################################################
				' ###########################################################################################
				' Ab hier Unterteilung, mehrzeilig wie gehabt oder nur 2 Zeilig (!!!!!)
				' ###########################################################################################
				' ###########################################################################################

				Dim nextLine = strarr_Zeilen(counter + 1)
				Dim itemIdRevRegex = New Regex("^&LOG Naming_Technique:.*Clone_Name: ""?@DB/(.+)/([a-zA-Z0-9]+)""?.*")


				Dim lineWithDbName = String.Empty

				If nextLine.StartsWith("&LOG Cloning_Action:") Then
					lineWithDbName = strarr_Zeilen(counter + 2)

				ElseIf nextLine.StartsWith("&LOG Naming_Technique:") Then
					' Sonderfall: Wenn in File "mapping.clone" ein 2zeiliger Eintrag existiert
					' Das passiert dann, wenn
					' - eine PRT Datei zugeordnet wird, die in einem anderen / parallelen Ordner abgelegt ist, als dem aktuell zu importierenden
					lineWithDbName = nextLine
				Else
					' Es ist werde die mehrzeilige, noch die 2 zeilige Variante im File "mapping.clone" vorhanden.
					' Somit ein unbekannter neuer Sonderfall, auf den dieses Programm noch icht reagieren kann.
					' -> Ergo: Programmabbruch
				End If

				Dim match = itemIdRevRegex.Match(lineWithDbName)
				If match.Success AndAlso match.Groups.Count >= 3 Then
					str_DBPartNo = match.Groups(1).Value
					str_DBPartRev = match.Groups(2).Value
				End If

				' Neu PartName & Description
				' die letzten 4 Zeichen abschneiden
				str_DBPartName = Path.GetFileNameWithoutExtension(str_NamePRT)  '.Substring(0, str_NamePRT.Length - 4)
				str_DBPartDescription = str_DBPartName

				If str_DBPartNo.StartsWith("ZN") Then
					str_DBPartNo = RemoveZnPrefix(str_DBPartNo)
					str_DBPartName = str_DBPartNo
					str_DBPartDescription = str_DBPartNo
				End If

				' Schreiben der neuen Zeile in der import.csv Datei
				' #######################################################

				Using swTMP As IO.StreamWriter = IO.File.AppendText(Path_LoadOptions_def & "\TC_IMPORT_TEMP\import.csv")
					Dim sep = Configuration.TrennzeichenCSV
					swTMP.WriteLine(str_PathPRT & sep & str_NamePRT & sep & str_DBPartNo & sep & str_DBPartRev & sep & str_DBPartName & sep & str_DBPartDescription & sep & str_DBFolder & sep & sep & sep & sep & sep & sep & sep & str_ASM_or_Part)
				End Using

			End If

		Next

		' Patrik
		' Die vorbereitete Datei "TC_IMPORT_TEMP\import.csv" für den Anwendungsfall wurde erzeugt.
		' Jetzt müssen die schon importierten Daten aus den CSV-Dateien am Server ergänzt werden
		' Also die einzelnen Zellen der csv Überschrift "Soll_...." ergänzt werden
		' Programmcode ..... 

		' Bihler: Vergleich und Zellen kopieren über 2 Arrays
		' ###################################################
		Dim str_Pfad_und_Datei_Bihler As String ' Verbindet die Pfadangabe und den Dateinamen
		str_Pfad_und_Datei_Bihler = Configuration.PathBihlerCSV & "\" & Configuration.FileBihlerCSV

		' FileBihlerCSV,                      ' Pfad und Datei "Bihler" - Patricks erste Originalzeile
		Dim biehlerComparator As CsvComparison.CsvComparator = New CsvComparison.CsvComparator(
			str_Pfad_und_Datei_Bihler,          ' Pfad und Datei "Bihler"
			ImportFileName,                     ' CSV Datei vom aktuellen Import-Prozess
			New Integer() {1},                  ' Die Zellen welche Verglichen werden sollen
			New Integer() {7, 8, 9, 10, 11, 12},    ' Die Zellen die von Bihler zur import.csv kopiert werden sollen (Beginnt bei 0,1,2)
			Configuration.TrennzeichenCSV.Chars(0))           ' Das Trennzeichen
		biehlerComparator.Append = True
		'biehlerComparator.BNKValue = "b"
		biehlerComparator.Process()             ' Den Prozess starten
		biehlerComparator.SaveTargetFile(ImportFileName)    ' Speichern der Datei "import.csv"

		' Normteile: Vergleich und Zellen kopieren über 2 Arrays
		' ######################################################
		Dim str_Pfad_und_Datei_Norm As String ' Verbindet die Pfadangabe und den Dateinamen
		str_Pfad_und_Datei_Norm = Configuration.PathNormteilCSV & "\" & Configuration.FileNormteilCSV

		'str_Pfad_und_Datei_Norm,                      ' Pfad und Datei "Norm" - Patricks erste Originalzeile
		Dim normComparator As CsvComparison.CsvComparator = New CsvComparison.CsvComparator(
			str_Pfad_und_Datei_Norm,
			ImportFileName,
			New Integer() {0, 1},
			New Integer() {7, 8, 9, 10, 11, 12},
			Configuration.TrennzeichenCSV.Chars(0))
		normComparator.Append = True
		'normComparator.BNKValue = "n"
		normComparator.Process()
		normComparator.SaveTargetFile(ImportFileName)

		' Konstruktionsdaten: Vergleich und Zellen kopieren über 2 Arrays
		' ######################################################
		Dim str_Pfad_und_Datei_Konstruktion As String ' Verbindet die Pfadangabe und den Dateinamen
		str_Pfad_und_Datei_Konstruktion = Configuration.PathKonstruktionCSV & "\" & Configuration.FileKonstruktionCSV

		Dim konstruktionComparator As CsvComparison.CsvComparator = New CsvComparison.CsvComparator(
			str_Pfad_und_Datei_Konstruktion,
			ImportFileName,
			New Integer() {0, 1},
			New Integer() {7, 8, 9, 10, 11, 12},
			Configuration.TrennzeichenCSV.Chars(0))
		konstruktionComparator.Append = True
		'konstruktionComparator.BNKValue = "k"
		konstruktionComparator.Process()
		konstruktionComparator.SaveTargetFile(ImportFileName)


		MsgBox("Funktion abgeschlossen.")

	End Sub

	Private Sub btn_TC_Import_Click(sender As Object, e As EventArgs) Handles btn_TC_Import.Click

        If (MessageBox.Show("This action will import the selected NX Assmbly in Teamcenter. Do you really want to continue?", "NX Import", MessageBoxButtons.YesNo) = DialogResult.No) Then
            Exit Sub


        End If
        ' Fehler-Abfangen falls User, PW, Grp nicht eingetragen wurde
        ' ###########################################################

        If txt_Username.Text = "" Then
			' Username fehlt
			MsgBox("TC Username fehlt")
		ElseIf txt_Passwort.Text = "" Then
			' PW fehlt
			MsgBox("TC Passwort fehlt")
		ElseIf txt_Group.Text = "" Then
			' GROUP fehlt
			MsgBox("TC Gruppe fehlt")
		Else
			' Weiter -> ...
			' Variablen eintragen
			str_TC_Username = txt_Username.Text
			str_TC_Password = txt_Passwort.Text
			str_TC_GROUP = txt_Group.Text
			' Aufruf der Sub
			Ueberpruefungen()
		End If

	End Sub

	Public Sub Ueberpruefungen()

		' Auswahl der "LoadOptions.def"
		Dim Path_LoadOptions_def As String
		Dim File_LoadOptions_def As String

		' Auswahl der LoadOptions im Path
		' ##################################
		Dim fd As OpenFileDialog = New OpenFileDialog()
		Dim strFileName As String

		fd.Title = "Select LoadOptions File"
		fd.InitialDirectory = InitialDir

		'fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
		fd.Filter = "All files (*.def)|*.def"
		fd.FilterIndex = 2
		fd.RestoreDirectory = True

		If fd.ShowDialog() = DialogResult.OK Then
			strFileName = fd.FileName

			' Pfad extrahieren
			Path_LoadOptions_def = IO.Path.GetDirectoryName(strFileName)

			' Dateiname extrahieren
			File_LoadOptions_def = IO.Path.GetFileName(strFileName)
		End If

		' Prüfungen
		' #########
		' Prüfen ob "TC_IMPORT_TEMP\mapping.clone" existiert
		' ##################################################
		Dim bool_OK_NOK As Boolean
		bool_OK_NOK = True

		If Not System.IO.File.Exists(Path_LoadOptions_def & "\TC_IMPORT_TEMP\mapping.clone") Then
			bool_OK_NOK = False
			MessageBox.Show("Das Mapping-File wurde noch nicht erzeugt!")
		End If
		
		Dim filename As String = Path_LoadOptions_def & "\TC_IMPORT_TEMP\import.csv"

		If Not IO.File.Exists(filename) Then
			bool_OK_NOK = False
			MessageBox.Show("Das Datei -import.csv- existiert nicht!")
		Else
			' Vergleichsstring wie die erste Zeile aussehen muss
			Dim str_vorgabe_ersteZeile As String
			Dim sep = Configuration.TrennzeichenCSV
			str_vorgabe_ersteZeile = "Path" & sep & "Name_PRT_File" & sep & "auto_Part_No" & sep & "auto_Part_Rev" & sep & "auto_Part_Name" & sep & "auto_Description" & sep & "auto_Folder" & sep & "Soll_Part_No" & sep & "Soll_Part_Rev" & sep & "Soll_Part_Name" & sep & "Soll_Description" & sep & "Soll_Folder" & sep & "Soll_B_N_K" & sep & "auto_ASM_or_Part"

			' Einselen der Datei in ein Array
			Dim allLines() As String = System.IO.File.ReadAllLines(filename)

			'Die erste Zeile der vorhandenen und vom User geänderten Datei "import.csv" einlesen
			' Wichtig: Die erste Zeile muss die Headline sein ...
			For Each line As String In allLines
				Dim Line0 As String = allLines(0) ' Erste Zeile (Zeile "0") einlesen

				If str_vorgabe_ersteZeile <> Line0 Then
					bool_OK_NOK = False
					MessageBox.Show("Die Headline der Datei -import.csv- ist nicht in der ersten Zeile!")
				End If
			Next
		End If

		' Wenn alle Überprüfungen positiv
		If bool_OK_NOK = True Then
			TCImport(Path_LoadOptions_def, File_LoadOptions_def)
		End If

	End Sub



	Sub TCImport(Path_LoadOptions_def As String, File_LoadOptions_def As String)
		' ##########################################################################
		' Jetzt wird das "import.clone" File geschrieben und nach TC Importiert ....
		' ##########################################################################

		' Variable am Ende ein "\" ergänzen, damit diese als Path erkannt wird
		Path_LoadOptions_def = Path_LoadOptions_def & "\"

		' Laufwerksbuchstabe vom Pfad zu der LoadOptions.def extrahieren
		' Wird in der nächsten "using" Anwendung benötigt
		' ##############################################################
		Dim str_LW_LoadOptions
		Dim str_path As System.IO.Path
		str_LW_LoadOptions = str_path.GetPathRoot(Path_LoadOptions_def)
		' Letztes Zeichen "\" entfernen
		str_LW_LoadOptions = str_LW_LoadOptions.Substring(0, str_LW_LoadOptions.Length - 1)

		' Variablen eintragen
		str_TC_Username = txt_Username.Text
		str_TC_Password = txt_Passwort.Text
		str_TC_GROUP = txt_Group.Text


		' Schreiben der neuen Zeile ans ENDE der "TC_IMPORT_TEMP\import.clone"
		' ###################################################################
		' Vorbereitung: Header schreiben

		Using swTMP As IO.StreamWriter = IO.File.AppendText(Path_LoadOptions_def & "TC_IMPORT_TEMP\import.clone")
			swTMP.WriteLine(ImportCloneZeile01)
			swTMP.WriteLine(ImportCloneZeile02)
			swTMP.WriteLine(ImportCloneZeile03 & System.DateTime.Now.ToString())
			swTMP.WriteLine(ImportCloneZeile04)
			swTMP.WriteLine(ImportCloneZeile05)
			swTMP.WriteLine(ImportCloneZeile06)
			swTMP.WriteLine(ImportCloneZeile07)
			swTMP.WriteLine(ImportCloneZeile08)
			swTMP.WriteLine(ImportCloneZeile09)
			swTMP.WriteLine(ImportCloneZeile10)
			swTMP.WriteLine(ImportCloneZeile11)
			swTMP.WriteLine(ImportCloneZeile12)
			swTMP.WriteLine(ImportCloneZeile13 & ItemType)
			swTMP.WriteLine(ImportCloneZeile14)
			swTMP.WriteLine(ImportCloneZeile15)
			swTMP.WriteLine(ImportCloneZeile16)
			swTMP.WriteLine(ImportCloneZeile17 & str_TC_Username & " " & str_TC_GROUP)
			swTMP.WriteLine(ImportCloneZeile18)
			swTMP.WriteLine(ImportCloneZeile19)
			swTMP.WriteLine(ImportCloneZeile20)
			swTMP.WriteLine(ImportCloneZeile21)
			swTMP.WriteLine(ImportCloneZeile22)
		End Using

		' Lese Zeilenweise die Datei "import.csv" in ein Array ein
		' Danach wird die Datei "import.clone" erweitert
		' ########################################################
		Dim str_Inhalt As String
		Dim str_arr_Zeilen() As String
		Dim counter As Integer


		' Einlesen der Datei "import.csv"
		If IO.File.Exists(Path_LoadOptions_def & "TC_IMPORT_TEMP\import.csv") Then str_Inhalt = IO.File.ReadAllText(Path_LoadOptions_def & "TC_IMPORT_TEMP\import.csv", System.Text.Encoding.Default) 'gesamte Textdatei einlesen
		'Zeilen in Array splitten
		str_arr_Zeilen = Split(str_Inhalt, vbNewLine)

		' Arraygröße herausfinden (Anzahl Zeilen)
		Dim int_arrGroesse As Integer
		int_arrGroesse = str_arr_Zeilen.Length - 1

		' Die einzelnen Arrays / Zeilen durchsuchen
		For counter = 1 To int_arrGroesse - 1

			'Splitte das Zeilen Array anhand des Trennzeichens "Semikolon" in ein Unter-Array:
			' ################################################################################
			Dim str_arr_Zellen() As String
			str_arr_Zellen = Split(str_arr_Zeilen(counter), Configuration.TrennzeichenCSV)

			'ermittel Arraygröße des Unter-Arrays "Zellen"
			Dim int_arrGroesse_Zellen As Integer
			int_arrGroesse_Zellen = str_arr_Zellen.Length - 1

			'Dimensionieren der Variablen
			Dim str_ASM_or_Part As String
			Dim str_Path_and_File As String
			Dim str_PathPRT As String
			Dim str_NamePRT As String

			'Dim str_DBPartNo As String
			'Dim str_DBPartRev As String
			'Dim str_DBPartName As String
			'Dim str_DBPartDescription As String
			'Dim str_DBFolder As String

			Dim str_Soll_DBPartNo As String
			Dim str_Soll_DBPartName As String
			Dim str_Soll_DBPartRev As String

			Dim str_Soll_DBPartDescription As String
			Dim str_Soll_DBFolder As String


			' Die einzelnen Zellen des Unter-Array nach den Inhalten durchsuchen und befüllen
			' ###############################################################################

			' Lese str_PathPRT
			str_PathPRT = (str_arr_Zellen(0))
            ' Lese str_NamePRT
            str_NamePRT = (str_arr_Zellen(4))
            ' Lese str_Path_and_File
            str_Path_and_File = str_PathPRT & "\" & str_NamePRT
            ' Lese str_Soll_DBPartNo
            str_Soll_DBPartNo = (str_arr_Zellen(2))
            ' Lese str_Soll_DBPartRev
            str_Soll_DBPartRev = (str_arr_Zellen(3))
            ' Lese str_Soll_DBFolder
            str_Soll_DBFolder = (str_arr_Zellen(11))
            ' Lese str_Soll_DBPartName
            str_Soll_DBPartName = (str_arr_Zellen(4))
            ' Lese str_Soll_DBPartDescription
            str_Soll_DBPartDescription = (str_arr_Zellen(5))
            ' Lese str_ASM_or_Part
            str_ASM_or_Part = (str_arr_Zellen(13))

			' Schreiben der neuen Zeile ans ENDE der "TC_IMPORT_TEMP\import.clone"
			' ###################################################################
			' Schreiben der Part Specific Information

			Using swTMP As IO.StreamWriter = IO.File.AppendText(Path_LoadOptions_def & "TC_IMPORT_TEMP\import.clone")
				swTMP.WriteLine("&LOG ")
				swTMP.WriteLine("&Log Part: """ & str_Path_and_File & """")
				swTMP.WriteLine("&LOG Cloning_Action: USE_EXISTING ")

				' Wenn die ersten beiden Zeichen "ZN" sind (also eine Zeichnung sind), dann
				If str_NamePRT.Substring(0, 2) = "ZN" Then
					swTMP.WriteLine("&LOG Naming_Technique: USER_NAME Clone_Name: ""@DB/" & str_Soll_DBPartNo & "/" & str_Soll_DBPartRev & "/specification/" & str_Soll_DBPartNo & "-" & str_Soll_DBPartRev & "-Z01" & """")
				Else
					swTMP.WriteLine("&LOG Naming_Technique: USER_NAME Clone_Name: ""@DB/" & str_Soll_DBPartNo & "/" & str_Soll_DBPartRev & """")
				End If

				swTMP.WriteLine("&Log Container: """ & str_TC_Username & ":" & str_Soll_DBFolder & """")
				swTMP.WriteLine("&Log Part_Type: " & ItemType)
				swTMP.WriteLine("&Log Part_Name: """ & str_Soll_DBPartName & """")
				swTMP.WriteLine("&Log Part_Description: """ & str_Soll_DBPartDescription & """")
				swTMP.WriteLine("&Log Associated_Files_Directory: """"")
			End Using
		Next

		' Letzte Zeile schreiben
		Using swTMP As IO.StreamWriter = IO.File.AppendText(Path_LoadOptions_def & "TC_IMPORT_TEMP\import.clone")
			swTMP.WriteLine("&LOG ")
		End Using

		' Der eigentliche Import:
		' #########################################################
		' import.clone Datei an TC_Prompt übergeben
		' #########################################################
		' #########################################################
		' Variable hält den kompletten Text
		Dim Output As String

		' Neues Prozess Object erzeugen
		Using P As New Process()
			' Definition welches Programm gestartet werden soll, hier "cmd.exe"
			P.StartInfo.FileName = "cmd.exe"

			' Da ich mehrere Commandozeilen an das CMD Uebergeben moechte lasse ich das Startargument mal weg
			' Set Startargument

			' Unterschied Schalter /c und /k .... hmmmmm noch klären
			'P.StartInfo.Arguments = "/c ping google.de"
			'P.StartInfo.Arguments = "/k echo on"

			' Setze Startargument im cmd Fenster
			P.StartInfo.Arguments = "/k echo on"

			'Required to redirect output, don't both worrying what it means
			P.StartInfo.UseShellExecute = False

			' Dem System mitteilen, dass ich lesen und schreiben möchte
			P.StartInfo.RedirectStandardOutput = True
			P.StartInfo.RedirectStandardInput = True

			'(Prozess) Start der cmd.exe
			P.Start()

			'Schreiben der verschiedenen Befehle als separate Zeilen in die cmd:
			P.StandardInput.WriteLine("set TC_ROOT=" & Configuration.TcRoot)
			P.StandardInput.WriteLine("set TC_DATA=" & Configuration.TcData)
			P.StandardInput.WriteLine(Configuration.TcData & "\tc_profilevars.bat")
			'P.StandardInput.WriteLine("d:\splm\tc11data\tc_profilevars.bat")

			P.StandardInput.WriteLine("set PATH=%UGII_ROOT_DIR%;%PATH%")

			' FMS_HOME setzen  ;-)
			P.StandardInput.WriteLine("set FMS_HOME=%TC_ROOT%\tccs")

			P.StandardInput.WriteLine(str_LW_LoadOptions)
			P.StandardInput.WriteLine("cd " & Path_LoadOptions_def)

			'P.StandardInput.WriteLine("D:")
			'P.StandardInput.WriteLine("cd D:\test_22")

			P.StandardInput.WriteLine("cd " & Path_LoadOptions_def)
            P.StandardInput.WriteLine("set UGII_LOAD_OPTIONS=" & Path_LoadOptions_def & "\" & File_LoadOptions_def)

            ' Vorlage
            'D:\splm\NX100\UGMANAGER\tcin_import -u=infodba -p=infodba -g=dba                                                                              -l=D:\test_20\mapping.clone
            P.StandardInput.WriteLine("%UGII_BASE_DIR%\UGMANAGER\tcin_import -u=" & str_TC_Username & " -p=" & str_TC_Password & " -g=" & str_TC_GROUP & " -fam=strip_status -default_a=overwrite -l=""" & Path_LoadOptions_def & "TC_IMPORT_TEMP\import.clone""")

            MessageBox.Show(P.StandardOutput.ToString, "fdfd")

            'P.StandardInput.WriteLine("ping google.com")

            ' Wichtig: Am Ende ein "Exit" senden, sonst wird der Standart-Out (STDOUT) vom Stream nie geschlossen
            P.StandardInput.WriteLine("pause")

            ' Lesen des kompletten Streams
            Output = P.StandardOutput.ReadToEnd()

			' Warten, bis die cmd / der Prozess geschlossen wurde
			P.WaitForExit()
		End Using

		' Patrik
		' TC Import erledigt. Jetzt müssen noch die Informationen aus der Datei "TC_IMPORT_TEMP\import.csv"
		' auf die zugeordneten Dateien auf dem Server kopiert werden.
		' Dateizuordnung: N= Normteile, B= Bihlerdaten, K=Konstruktionsdaten
		' Die Server-CSV werden in der eingangs definierten "config.ini" definiert
		' Programmcode .....


		ImportFileName = Path_LoadOptions_def & "TC_IMPORT_TEMP\import.csv"

		' Bihler: Vergleich und Zellen kopieren über 2 Arrays
		' ###################################################
		Dim str_Pfad_und_Datei_Bihler As String ' Verbindet die Pfadangabe und den Dateinamen
		str_Pfad_und_Datei_Bihler = Configuration.PathBihlerCSV & Configuration.FileBihlerCSV
		'MsgBox(ImportFileName)

		Dim biehlerComparator As CsvComparison.CsvComparator = New CsvComparison.CsvComparator(
			ImportFileName,
			str_Pfad_und_Datei_Bihler,
			New Integer() {1, 12},
			New Integer() {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13},
			Configuration.TrennzeichenCSV.Chars(0))
		biehlerComparator.Append = True
		biehlerComparator.BNKValue = "b"
		biehlerComparator.Process()
		'biehlerComparator.SaveTargetFile(ImportFileName)
		biehlerComparator.SaveTargetFile(str_Pfad_und_Datei_Bihler)

		' Normteile: Vergleich und Zellen kopieren über 2 Arrays
		' ######################################################
		Dim str_Pfad_und_Datei_Norm As String ' Verbindet die Pfadangabe und den Dateinamen
		str_Pfad_und_Datei_Norm = Configuration.PathNormteilCSV & Configuration.FileNormteilCSV
		'MsgBox(str_Pfad_und_Datei_Norm)

		Dim normComparator As CsvComparison.CsvComparator = New CsvComparison.CsvComparator(
			ImportFileName,
			str_Pfad_und_Datei_Norm,
			New Integer() {0, 1, 12},
			New Integer() {0, 1, 7, 8, 9, 10, 11, 12},
			Configuration.TrennzeichenCSV.Chars(0))
		normComparator.Append = True
		normComparator.BNKValue = "n"
		normComparator.Process()
		normComparator.SaveTargetFile(str_Pfad_und_Datei_Norm)

		' Konstruktionsdaten: Vergleich und Zellen kopieren über 2 Arrays
		' ######################################################
		Dim str_Pfad_und_Datei_Konstruktion As String ' Verbindet die Pfadangabe und den Dateinamen
		str_Pfad_und_Datei_Konstruktion = Configuration.PathKonstruktionCSV & Configuration.FileKonstruktionCSV

		Dim konstruktionComparator As CsvComparison.CsvComparator = New CsvComparison.CsvComparator(
			ImportFileName,
			str_Pfad_und_Datei_Konstruktion,
			New Integer() {0, 1, 12},
			New Integer() {1, 7, 8, 9, 10, 11, 12},
			Configuration.TrennzeichenCSV.Chars(0))
		konstruktionComparator.Append = True
		konstruktionComparator.BNKValue = "k"
		konstruktionComparator.Process()
		konstruktionComparator.SaveTargetFile(str_Pfad_und_Datei_Konstruktion)

		MsgBox("Teamcenter-Import abgeschlossen.")

	End Sub

	Private Shared Function RemoveZnPrefix(str_DBPartNo As String)
		Try
			If str_DBPartNo.StartsWith("ZN-1_") Then
				' die ersten 5 Zeichen entfernen
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-1-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_1_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_1-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-2_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-2-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_2_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_2-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-3_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-3-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_3_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_3-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-4_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-4-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_4_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_4-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-5_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-5-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_5_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_5-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-6_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-6-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_6_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_6-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-7_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-7-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_7_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_7-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-8_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-8-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_8_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_8-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-9_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN-9-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_9_") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_9-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 5)
			ElseIf str_DBPartNo.StartsWith("ZN_") Then
				' die ersten 3 Zeichen entfernen
				str_DBPartNo = str_DBPartNo.Remove(0, 3)
			ElseIf str_DBPartNo.StartsWith("ZN-") Then
				str_DBPartNo = str_DBPartNo.Remove(0, 3)
			End If
		Catch ex As Exception 
			Console.WriteLine("Error while removing ""ZN"" prefix: " & ex.Message)
		End Try

		Return str_DBPartNo
	
	End Function

    Private Sub btn_partcleanup_Click(sender As Object, e As EventArgs) Handles btn_partcleanup.Click
        If (MessageBox.Show("This action will clean up parts in the directory and sub directories which will take more time depends on your assembly size. Do you really want to continue?", "Part Cleanup", MessageBoxButtons.YesNo) = DialogResult.No) Then
            Exit Sub
        End If

        Dim baseDir = Environment.GetEnvironmentVariable("UGII_BASE_DIR")
        Dim Path_prt_file As String = Nothing
        Dim part_file_name As String = Nothing
        Dim fd As OpenFileDialog = New OpenFileDialog()
        Dim strFileName As String = Nothing
        Dim strFileNamedef As String = Nothing
        Dim logFilename As String = Nothing
        Dim Path_LoadOptions_def As String = Nothing
        Dim File_LoadOptions_def As String = Nothing

        fd.Title = "Select Assembly File to cleanup"
        fd.InitialDirectory = InitialDir

        'fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.Filter = "All files (*.prt)|*.prt"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True

        If fd.ShowDialog() = DialogResult.OK Then
            strFileName = fd.FileName
            Path_prt_file = IO.Path.GetDirectoryName(strFileName)
            part_file_name = IO.Path.GetFileName(strFileName)
        Else
            Application.Exit()
        End If


        ' Dim fd As OpenFileDialog = New OpenFileDialog()


        fd.Title = "Select LoadOptions File"
        fd.InitialDirectory = InitialDir

        'fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.Filter = "All files (*.def)|*.def"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True

        If fd.ShowDialog() = DialogResult.OK Then
            strFileNamedef = fd.FileName

            ' Pfad extrahieren, ist ohne \ am Ende
            Path_LoadOptions_def = IO.Path.GetDirectoryName(strFileNamedef)

            ' Dateiname extrahieren
            File_LoadOptions_def = IO.Path.GetFileName(strFileNamedef)

        Else
            ' Do nothing
            Application.Exit()
        End If





        logFilename = Path_prt_file & "\partcleanuplog_" & System.DateTime.Now.ToString("yyyMMdd") & "_" & System.DateTime.Now.ToString("HHmmss") & ".log"

        'Dim ps = New Process
        'ps.StartInfo.FileName = baseDir & "\UGII\refile_part"
        'ps.StartInfo.Arguments = " " & strFileName & " -cleanup -g -y -s -d -l"
        ''  ps.StartInfo.Arguments = " " & strFileName & " -cleanup -g -y -s -d -f -us -r"

        'ps.StartInfo.WorkingDirectory = Path_prt_file
        'ps.StartInfo.WindowStyle = ProcessWindowStyle.Minimized
        'ps.StartInfo.EnvironmentVariables.Add("UGII_LOAD_OPTIONS", Path.Combine(Path_LoadOptions_def, File_LoadOptions_def))
        'ps.StartInfo.RedirectStandardOutput = True
        'ps.StartInfo.UseShellExecute = False
        'ps.Start()

        Dim Output As String

        Using P As New Process()
            P.StartInfo.FileName = "cmd.exe"
            P.StartInfo.Arguments = "/k echo on"
            P.StartInfo.UseShellExecute = False
            P.StartInfo.RedirectStandardOutput = True
            P.StartInfo.RedirectStandardInput = True
            P.Start()

            'Schreiben der verschiedenen Befehle als separate Zeilen in die cmd:
            P.StandardInput.WriteLine("set TC_ROOT=" & Configuration.TcRoot)
            P.StandardInput.WriteLine("set TC_DATA=" & Configuration.TcData)
            P.StandardInput.WriteLine(Configuration.TcData & "\tc_profilevars.bat")
            P.StandardInput.WriteLine("set PATH=%UGII_ROOT_DIR%;%PATH%")
            P.StandardInput.WriteLine("set FMS_HOME=%TC_ROOT%\tccs")
            P.StandardInput.WriteLine("X:")
            P.StandardInput.WriteLine("cd " & Path_LoadOptions_def)
            P.StandardInput.WriteLine("cd " & Path_LoadOptions_def)
            P.StandardInput.WriteLine("set UGII_LOAD_OPTIONS=" & Path_LoadOptions_def & "\" & File_LoadOptions_def)
            P.StandardInput.WriteLine("%UGII_BASE_DIR%\UGII\refile_part " & strFileName & " -cleanup -g -y -s -d -l > ddd.log")

            MessageBox.Show(P.StandardOutput.ToString, "fdfd")


            ' Wichtig: Am Ende ein "Exit" senden, sonst wird der Standart-Out (STDOUT) vom Stream nie geschlossen
            P.StandardInput.WriteLine("pause")

            ' Lesen des kompletten Streams
            '  Output = P.StandardOutput.ReadToEnd()
            ' Lesen des kompletten Streams
            ' Output = P.StandardOutput.ReadToEnd()

            ' Warten, bis die cmd / der Prozess geschlossen wurde
            P.WaitForExit()




            Dim logfile As System.IO.StreamWriter
            logfile = My.Computer.FileSystem.OpenTextFileWriter(logFilename, False)
            logfile.WriteLine("#############################################################################################################################")
            logfile.WriteLine("                                                 PART CLEANUP LOG                                                            ")
            logfile.WriteLine("Date and Time : " & Date.Now)
            logfile.WriteLine("#############################################################################################################################")
            logfile.Write(P.StandardOutput.ReadToEnd)
            logfile.Close()
            'ps.WaitForExit()
            MessageBox.Show("Log is saved under " & logFilename)
        End Using

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Application.Exit()
    End Sub
End Class


