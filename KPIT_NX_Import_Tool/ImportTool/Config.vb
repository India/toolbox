﻿Imports System.IO

Public Class Config

	Public Property TcUserName As String
	Public Property TcPassword As String 
	Public Property TcGroup As String 
	Public Property TcRoot As String 
	Public Property TcData As String 
	Public Property PathNormteilCSV As String
	Public Property FileNormteilCSV As String
	Public Property PathBihlerCSV As String
	Public Property FileBihlerCSV As String
	Public Property PathKonstruktionCSV As String
	Public Property FileKonstruktionCSV As String
	Public Property TrennzeichenCSV As String

	Public Property SkipRemoveAttributes As Boolean
	Public Property SkipDeletePartFamilyStatus As Boolean

	Public Shared Function Load(fileName As String) As Config
		
		Dim config = New Config
		
		Dim values = LoadValues(fileName)
		config.TcUserName = GetValueOrDefault("Username", String.Empty, values)
		config.TcPassword = GetValueOrDefault("Password", String.Empty, values)
		config.TcGroup = GetValueOrDefault("Group", String.Empty, values)

		config.TcRoot = GetValueOrDefault("TC_ROOT", String.Empty, values)
		config.TcData = GetValueOrDefault("TC_DATA", String.Empty, values)

		config.PathNormteilCSV = GetValueOrDefault("PathNormteilCSV", String.Empty, values)
		config.FileNormteilCSV = GetValueOrDefault("FileNormteilCSV", String.Empty, values)

		config.PathBihlerCSV = GetValueOrDefault("PathBihlerCSV", String.Empty, values)
		config.FileBihlerCSV = GetValueOrDefault("FileBihlerCSV", String.Empty, values)

		config.PathKonstruktionCSV = GetValueOrDefault("PathKonstruktionCSV", String.Empty, values)
		config.FileKonstruktionCSV = GetValueOrDefault("FileKonstruktionCSV", String.Empty, values)

		config.TrennzeichenCSV = GetValueOrDefault("TrennzeichenCSV", ";", values)
	
		config.SkipRemoveAttributes = GetBoolOrDefault("SkipRemoveAttributes", False, values)
		config.SkipDeletePartFamilyStatus = GetBoolOrDefault("SkipDeletePartFamilyStatus", False, values)

		Return config
	End Function

	Private Shared Function GetBoolOrDefault(key As String, defaultValue As Boolean, values As IDictionary(Of String, String)) As Boolean
		If values.ContainsKey(key) Then 
			Dim str = values(key)
			Return Boolean.Parse(str)
		Else
			Return defaultValue
		End If 
	End Function

	Private Shared Function GetValueOrDefault(key As String, defaultValue As String, values As  IDictionary(Of String, String)) As String 
		If values.ContainsKey(key) Then 
			Return values(key)
		Else
			Return defaultValue
		End If 
	End Function

	Private Shared Function LoadValues(fileName As String) As IDictionary(Of String, String)
		Dim values = New Dictionary(Of String, String)

		Dim lines = File.ReadAllLines(fileName)

		For Each line In lines
			If line.Contains("=") Then
				Dim kv = line.Split("=")

				If kv.Length = 2 Then
					values.Add(kv(0), kv(1))
				End If
			End If
		Next

		Return values
	End Function

End Class
