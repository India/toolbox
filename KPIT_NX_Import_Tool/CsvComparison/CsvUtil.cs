﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvComparison
{
    public static class CsvUtil
    {
		public static List<string[]> Read(string fileName, char separator = ';')
		{
			var content = System.IO.File.ReadAllLines(fileName);

			var lines = new List<string[]>();

			foreach (var line in content)
				lines.Add(line.Split(separator));
			
			return lines;
		}

		public static void Write(IEnumerable<string[]> lines, string fileName, char separator = ';')
		{
			var seperatorString = new string(separator, 1);
			var content = lines.Select(x => string.Join(seperatorString, x)).ToArray();
			System.IO.File.WriteAllLines(fileName, content);
		}
    }
}
