﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvComparison
{
	public class CsvComparator
	{
		private List<string[]> mSourceContent;
		
		private List<string[]> mTargetContent;
		private IDictionary<string, int> mTargetIndex;

		private int[] mKeyFields;
		private int[] mCopyFields;

		private char mSeparator;

		public bool Append { get; set; }
        public string BNKValue { get; set; }

		/// <summary>
		/// Create a new instance of CsvComparator
		/// </summary>
		/// <param name="sourceFile">The file from which values are copied.</param>
		/// <param name="targetFile">The file to which values are copied to.</param>
		/// <param name="keyFields">Indices of the fields which build the key.</param>
		/// <param name="copyFields">Indices of the fields whose values should be copied.</param>
		/// <param name="separator">The separator used in the CSV.</param>
		public CsvComparator(string sourceFile, string targetFile, int[] keyFields, int[] copyFields, char separator)
		{
			mSeparator = separator;
			mKeyFields = keyFields;
			mCopyFields = copyFields;

			mSourceContent = CsvUtil.Read(sourceFile, separator);
			mTargetContent = CsvUtil.Read(targetFile, separator);
			mTargetIndex = BuildKeyMap(mTargetContent);

			Append = false;
		}

		public void Process()
		{
			foreach (var sourceRow in mSourceContent)
			{
				var key = BuildKey(sourceRow);
				if (mTargetIndex.ContainsKey(key))
				{
					int index = mTargetIndex[key];
					var targetRow = mTargetContent[index];
					ProcessMatch(sourceRow, targetRow);
				}
				else if (Append && BNKValue != null && BNKValue.Equals(sourceRow[12]))
				{
					mTargetContent.Add(sourceRow);
					mTargetIndex.Add(BuildKey(sourceRow), mTargetContent.Count - 1);
				}
			}
		}

		protected void ProcessMatch(string[] sourceRow, string[] targetRow)
		{
			foreach (var copyIndex in mCopyFields)
			{
				var sourceValue = sourceRow[copyIndex];
				if (!string.IsNullOrWhiteSpace(sourceValue))
					targetRow[copyIndex] = sourceRow[copyIndex];
			}
		}

		private IDictionary<string, int> BuildKeyMap(List<string[]> lines)
		{
			var map = new Dictionary<string, int>();

			for (int i = 0; i < lines.Count; ++i)
			{
				var key = BuildKey(lines[i]);
				if (!map.ContainsKey(key))
					map.Add(key, i);
			}
			return map;
		}

		private string BuildKey(string[] line)
		{
			var sb = new StringBuilder();

			foreach (var k in mKeyFields)
				sb.Append(line[k]).Append(";");

			return sb.ToString();
		}

		public void SaveTargetFile(string fileName)
		{
			CsvUtil.Write(mTargetContent, fileName);
		}
	}
}